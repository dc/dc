# Data Controller
# Contributing

## Workflow guidelines
[Wiki Page](https://git.datacontroller.io/dc/dc/wiki/Git-Workflow)

## Dependencies that requires licences

[SheetJS Pro Version](https://www.npmjs.com/package/sheetjs)


To auth for SheetJS Pro version you need to use their private registry
Add `client/.npmrc` file with following content:
```
@sheet:registry=https://pylon.sheetjs.com:54111/
//pylon.sheetjs.com:54111/:_authToken="TOKEN-GOES-HERE"
```

[Handsontable](https://www.npmjs.com/package/handsontable)

Licence should be inserted in the `client/src/index.html` file:

```
 <sasjs
    ...
    hotLicenceKey="LICENCE-GOES-HERE"
>
</sasjs>
```
## Development
Update `client/src/index.html` so that it points to your SAS9, SASVIYA or SASJS backend.  
Be aware that VIYA can be configured in such way that it would not work with cross origin frontend.  
Follow this guide to disable CORS: https://sasjs.io/cors/ (NOTICE: Sometimes even this approach would fail to work, in such case it is imposible to set it up without reconfiguring the VIYA server)  

Start dev server:
```
cd client

npm start
```

## GUI Elements

For documentation on the Clarity Design System, including a list of components and example usage, see [our website](https://vmware.github.io/clarity).

## Code style
Run prettier fix:
```bash
npm run lint:fix
```

## Generate docs
Typedoc is used for generating typescript documentation based on the code.
That part is automated and beign done as a part of CI job.

# Release
Release is automated as a part of CI job. Workflow file: `.gitea/workflows/release.yaml`.  
It will run automatically when branch merged to the `main` branch.  
IMPORTANT!  
If release job fails, after it has been created empty release and a tag, we must not re-run the relase job until we removed the newly create GIT TAG and RELEASE.  
To remove the git tag run:  
```
git push -d origin vX.X.X
```
To remove the release, you need to do it with repo administration over at [https://git.datacontroller.io/dc/dc](https://git.datacontroller.io/dc/dc)

# Troubleshooting

## Makedata service "could not create directory" error

The dcpath folder should have its permissions set so that the system account (SYSUSERID) can both read and write to it.

Example:

If dcpath is: '/tmp/dc'

Run:
```
chmod 777 /tmp/dc
```