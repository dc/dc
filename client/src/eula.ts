/**
 * Recreate _eula.ts based on LICENCE.md an put it in environments folder so it can be used in the angular components
 */

const { readFileSync } = require('fs')
const { resolve, relative } = require('path')
const { writeFileSync } = require('fs-extra')

const file = resolve(__dirname, '..', 'src', 'environments', '_eula.ts')

const licenceFilePath = resolve(__dirname, '..', '..', 'LICENCE.md')

try {
  const licence = readFileSync(licenceFilePath).toString()

  writeFileSync(
    file,
    `//IMPORTANT: THIS FILE IS AUTO GENERATED BASED ON LICENCE.MD FILE!\nexport const EULA = \`\n${licence}\n\`\n`,
    { encoding: 'utf-8' }
  )

  console.log(
    `Created _eula.ts based on LICENCE.MD in: ${relative(
      resolve(__dirname, '..'),
      file
    )}`
  )
} catch (e) {
  console.log(e)
}
