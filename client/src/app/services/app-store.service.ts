import { Injectable } from '@angular/core'
import { SASjsConfig } from '@sasjs/adapter'
import { DcAdapterSettings } from '../models/DcAdapterSettings'

@Injectable({
  providedIn: 'root'
})
export class AppStoreService {
  private dcAdapterSettings: DcAdapterSettings | undefined
  public sasjsConfig: SASjsConfig | undefined

  constructor() {}

  public setDcAdapterSettings(dcAdapterSettings: DcAdapterSettings) {
    this.dcAdapterSettings = dcAdapterSettings
  }

  public getDcAdapterSettings(): DcAdapterSettings | undefined {
    return this.dcAdapterSettings
  }
}
