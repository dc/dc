import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'prettyjson'
})
export class PrettyjsonPipe implements PipeTransform {
  transform(rawJson: any): string {
    return JSON.stringify(rawJson, null, 2)
  }
}
