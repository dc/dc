import { Pipe, PipeTransform } from '@angular/core'
import * as moment from 'moment'
@Pipe({
  name: 'dateTimeFormatter'
})
export class DateTimeFormatterPipe implements PipeTransform {
  transform(value: Date | string, type: string): string {
    if (typeof value === 'string' && value.length < 1) return value

    switch (type) {
      case 'date': {
        return moment(value, 'DDMMMYYYY:hh:mm:ss').format('DD/MM/YYYY')
      }
      case 'time': {
        if (typeof value !== 'string')
          throw new Error('Error parsing time. Value is not string.')

        const hours = parseInt(value.split(':')[0])
        const minutes = parseInt(value.split(':')[1])
        const seconds = parseInt(value.split(':')[2])

        const hoursString = hours < 10 ? '0' + hours : hours
        const minutesString = minutes < 10 ? '0' + minutes : minutes
        const secondsString = seconds < 10 ? '0' + seconds : seconds

        return `${hoursString}:${minutesString}:${secondsString}`
      }
    }

    return typeof value === 'string' ? value : value.toString()
  }
}
