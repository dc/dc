import { Pipe, PipeTransform } from '@angular/core'
import { bytesToSize } from '@sasjs/utils/utils/bytesToSize'

@Pipe({
  name: 'convertSize'
})
export class ConvertSizePipe implements PipeTransform {
  transform(bytes: string | number, ...args: string[]): string {
    const decimals = parseInt(args[0]) || 2
    const bytesNum = typeof bytes !== 'number' ? parseInt(bytes) : bytes

    return bytesToSize(bytesNum, decimals)
  }
}
