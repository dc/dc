import { Pipe, PipeTransform } from '@angular/core'
import { HelperService } from '../services/helper.service'

@Pipe({
  name: 'secondsParser'
})
export class SecondsParserPipe implements PipeTransform {
  constructor(private helperService: HelperService) {}

  transform(value: number | string): string {
    if (value === undefined || value === null || value === '') return ''

    let hours
    let minutes
    let seconds

    // If it's datetime value (formatted) we parse it, othervise we calculate it
    if (typeof value === 'string' && value.split(':').length === 3) {
      const valueSplit = value.split(':')

      hours = valueSplit[0]
      minutes = valueSplit[1]
      seconds = valueSplit[2]
    } else {
      if (typeof value !== 'number') value = parseInt(value)

      hours = Math.floor(value / 3600)
      minutes = Math.floor((value % 3600) / 60)
      seconds = Math.floor((value % 3600) % 60)
    }

    return `${this.helperService.addLeadingZero(
      hours
    )}:${this.helperService.addLeadingZero(
      minutes
    )}:${this.helperService.addLeadingZero(seconds)}`
  }
}
