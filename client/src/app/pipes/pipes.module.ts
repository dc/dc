import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { DateTimeFormatterPipe } from './date-time-formatter.pipe'
import { sasToJsDatePipe } from './ms-to-date.pipe'
import { PkSpaceSeparatePipe } from './pk-space-separate.pipe'
import { SecondsParserPipe } from './seconds-parser.pipe'
import { ThousandSeparatorPipe } from './thousand-separator.pipe'
import { ToNumberPipe } from './to-number.pipe'
import { ConvertSizePipe } from './convert-size.pipe'
import { LinkinzePipe } from './linkinze.pipe'
import { PrettyjsonPipe } from './prettyjson.pipe'

@NgModule({
  declarations: [
    PkSpaceSeparatePipe,
    sasToJsDatePipe,
    ThousandSeparatorPipe,
    SecondsParserPipe,
    DateTimeFormatterPipe,
    ToNumberPipe,
    ConvertSizePipe,
    LinkinzePipe,
    PrettyjsonPipe
  ],
  imports: [CommonModule],
  exports: [
    PkSpaceSeparatePipe,
    sasToJsDatePipe,
    ThousandSeparatorPipe,
    SecondsParserPipe,
    DateTimeFormatterPipe,
    ToNumberPipe,
    ConvertSizePipe,
    LinkinzePipe,
    PrettyjsonPipe
  ]
})
export class PipesModule {}
