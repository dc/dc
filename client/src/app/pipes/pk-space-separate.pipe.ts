import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'pkSpaceSeparate'
})
export class PkSpaceSeparatePipe implements PipeTransform {
  transform(value: string): string {
    return value.replace(/\|/g, ' | ')
  }
}
