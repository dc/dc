import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'linkinze'
})
export class LinkinzePipe implements PipeTransform {
  /**
   * It makes path/url to be clicklable
   * @param path /example/path
   * @returns string to be used in `innerHTML`
   */
  transform(path: string): string {
    if (!path.includes('/')) return path

    let cascadingPath = ''

    const pathSplit = path.split('/')

    for (let i = 0; i < pathSplit.length; i++) {
      if (pathSplit[i] !== '') {
        const link = pathSplit[i]
        cascadingPath += '/' + link

        pathSplit[i] = `<a href=${cascadingPath}>${link}</a>`
      }
    }

    return pathSplit.join('/')
  }
}
