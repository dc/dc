import { Component, OnInit, OnDestroy } from '@angular/core'

@Component({
  selector: 'app-home-route',
  templateUrl: './home-route.component.html',
  styleUrls: ['./home-route.component.scss'],
  host: {
    class: 'content-container'
  }
})
export class HomeRouteComponent implements OnInit, OnDestroy {
  constructor() {}

  ngOnInit() {}

  ngOnDestroy() {}
}
