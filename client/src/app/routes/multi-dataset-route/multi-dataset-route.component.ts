import { Component, OnInit, OnDestroy } from '@angular/core'

@Component({
  selector: 'app-multi-dataset-route',
  templateUrl: './multi-dataset-route.component.html',
  host: {
    class: 'content-container'
  }
})
export class MultiDatasetRouteComponent implements OnInit, OnDestroy {
  constructor() {}

  ngOnInit() {}

  ngOnDestroy() {}
}
