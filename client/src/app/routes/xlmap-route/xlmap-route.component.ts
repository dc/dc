import { Component, OnInit, OnDestroy } from '@angular/core'

@Component({
  selector: 'app-xlmap-route',
  templateUrl: './xlmap-route.component.html',
  styleUrls: ['./xlmap-route.component.scss'],
  host: {
    class: 'content-container'
  }
})
export class XLMapRouteComponent implements OnInit, OnDestroy {
  constructor() {}

  ngOnInit() {}

  ngOnDestroy() {}
}
