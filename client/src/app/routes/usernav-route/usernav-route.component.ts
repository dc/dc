import { Component, OnInit } from '@angular/core'

@Component({
  selector: 'app-usernav-route',
  templateUrl: './usernav-route.component.html',
  styleUrls: ['./usernav-route.component.scss'],
  host: {
    class: 'content-container'
  }
})
export class UsernavRouteComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
