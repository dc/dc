import { Component, OnInit } from '@angular/core'

@Component({
  selector: 'app-review-route',
  templateUrl: './review-route.component.html',
  styleUrls: ['./review-route.component.scss'],
  host: {
    class: 'content-container'
  }
})
export class ReviewRouteComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
