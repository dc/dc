import { Component, OnInit, OnDestroy } from '@angular/core'

@Component({
  selector: 'app-view-route',
  templateUrl: './view-route.component.html',
  styleUrls: ['./view-route.component.scss'],
  host: {
    class: 'content-container'
  }
})
export class ViewRouteComponent implements OnInit, OnDestroy {
  constructor() {}

  ngOnInit() {}

  ngOnDestroy() {}
}
