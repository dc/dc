import { DcValidator } from '../shared/dc-validator/dc-validator'
import { FileUploadEncoding } from './FileUploadEncoding'
import { FileUploader } from './FileUploader.class'
import { ExcelRule } from './TableData'
import XLSX from 'xlsx'

export interface ParseParams {
  file: File
  password?: string
  dcValidator: DcValidator
  /**
   * If workbook is provided, parse function will not run a XLSX.read()
   * it will use this property instead. So the client must do a file read beforehand
   */
  workbook?: XLSX.WorkBook
  /**
   * Parse function will manipulate and return the uploader array which can be provided with files already in the queue
   * Otherwise new empty instance will be created.
   */
  uploader?: FileUploader
  headerPks: string[]
  headerArray: string[]
  headerShow: string[]
  timeHeaders: string[]
  dateHeaders: string[]
  dateTimeHeaders: string[]
  xlRules: ExcelRule[]
  encoding?: FileUploadEncoding
}
