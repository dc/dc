import { FileUploader } from './FileUploader.class'
import FoundRangeInfo from './RangeInfo'

export interface ParseResult {
  /**
   * In case of CSV file, won't be returned
   */
  data?: any[]
  /**
   * In case of CSV file, won't be returned
   */
  headerShow?: string[]
  rangeSheetRes?: FoundRangeInfo
  uploader: FileUploader
}
