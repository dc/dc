import { BaseSASResponse } from './common/BaseSASResponse'

export interface EditorsRestoreServiceResponse extends BaseSASResponse {
  restore_out: RestoreOut[]
}

export interface RestoreOut {
  LOADREF: string
}
