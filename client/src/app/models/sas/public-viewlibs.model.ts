import { BaseSASResponse } from './common/BaseSASResponse'

export interface PublicViewlibsServiceResponse extends BaseSASResponse {
  saslibs: Saslibs[]
}

export interface Saslibs {
  ENGINE: string
  LIBRARYID: string
  LIBRARYNAME: string
  LIBRARYREF: string
}
