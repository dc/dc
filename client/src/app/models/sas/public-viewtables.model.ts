import { BaseSASResponse } from './common/BaseSASResponse'
import { Libinfo } from './common/Libinfo'

export interface PublicViewtablesServiceResponse extends BaseSASResponse {
  mptables: { MEMNAME: string }[]
  libinfo: Libinfo[]
}
