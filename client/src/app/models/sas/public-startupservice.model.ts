import { BaseSASResponse } from './common/BaseSASResponse'

export interface PublicStartupserviceResponse extends BaseSASResponse {
  sasdatasets: any[]
  saslibs: any[]
  globvars: Globvar[]
}

export interface Globvar {
  DCLIB: string
  SAS9LINEAGE_ENABLED: number
  ISREGISTERED: number
  REGISTERCOUNT: number
  LICENCE_KEY: string
  ACTIVATION_KEY: string
  DC_ADMIN_GROUP: string
}
