import { BaseSASResponse } from './common/BaseSASResponse'

export interface ValidateFilterSASResponse extends BaseSASResponse {
  result: Result[]
}

export interface Result {
  FILTER_RK: number
  FILTER_HASH: string
  FILTER_TABLE: string
}
