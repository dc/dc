import { BaseSASResponse } from './common/BaseSASResponse'

export interface EditorsStageDataSASResponse extends BaseSASResponse {
  SYSDATE: string
  SYSTIME: string
  sasparams: Sasparam[]
  _DEBUG: string
  _PROGRAM: string
  AUTOEXEC: string
  MF_GETUSER: string
  SYSCC: string
  SYSENCODING: string
  SYSERRORTEXT: string
  SYSHOSTINFOLONG: string
  SYSHOSTNAME: string
  SYSPROCESSID: string
  SYSPROCESSMODE: string
  SYSPROCESSNAME: string
  SYSJOBID: string
  SYSSCPL: string
  SYSSITE: string
  SYSTCPIPHOSTNAME: string
  SYSUSERID: string
  SYSVLONG: string
  SYSWARNINGTEXT: string
  END_DTTM: string
  MEMSIZE: string
}

export interface Sasparam {
  STATUS: string | 'SUCCESS'
  DSID: string
  URL: string
}
