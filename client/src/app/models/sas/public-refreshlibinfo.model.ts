import { BaseSASResponse } from './common/BaseSASResponse'
import { Libinfo } from './common/Libinfo'

export interface PublicRefreshlibinfoServiceResponse extends BaseSASResponse {
  libinfo: Libinfo[]
}
