import { BaseSASResponse } from './common/BaseSASResponse'
import { DataFormat } from './common/DateFormat'

export interface PublicGetcolvalsSASResponse extends BaseSASResponse {
  vals: Val[]
  $vals: $Vals
  meta: Meta[]
}

export interface Val {
  FORMATTED: string
  UNFORMATTED: string | number
}

export interface Meta {
  COLUMN: string
  SASFORMAT: string
  STARTROW: string
  ROWS: string
  NOBS: number
}

export interface $Vals {
  vars: Vars
}

export interface Vars {
  FORMATTED: DataFormat
  UNFORMATTED: DataFormat
}
