import { BaseSASResponse } from './common/BaseSASResponse'

export interface PublicGetgroupsResponse extends BaseSASResponse {
  groups: any[]
}

export interface SASGroup {
  GROUPDESC: string
  GROUPNAME: string
  GROUPURI: string
}
