import { BaseSASResponse } from './common/BaseSASResponse'
import { Col } from '../../shared/dc-validator/models/col.model'
export interface AuditorsPostdataSASResponse extends BaseSASResponse {
  params: Param[]
  cols: Col[]
  submits: Submit[]
  deleted: any[]
  new: any[]
  updates: Update[]
  originals: Original[]
  fmt_deleted: any[]
  fmt_new: any[]
  fmt_updates: Update[]
  fmt_originals: Original[]
}

export interface Param {
  BASE_DS: string
  BASE_LIB: string
  DIFFS_CSV: string
  DIFFTIME: string
  FILESIZE_RAW: number
  FILESIZE: string
  INPUT_OBS: number
  INPUT_VARS: number
  ISAPPROVER: string
  LIBDS: string
  NUM_ADDED: number
  NUM_DELETED: number
  NUM_OF_APPROVALS_REMAINING: number
  NUM_OF_APPROVALS_REQUIRED: number
  NUM_UPDATED: number
  REVIEWED_BY_NM: string
  REVIEWED_ON_DTTM?: any
  REVIEWED_ON: string
  SUBMITTED_BY_NM: string
  SUBMITTED_ON_DTTM: number
  SUBMITTED_ON: string
  SUBMITTED_REASON_TXT: string
  SUBMIT_STATUS_CD: string
  TABLE_ID: string
  TECH_TO_DTTM: number
  TRUNCATED: string
  [key: string]: any
}

export interface Submit {
  TABLE_ID: string
  TECH_FROM_DTTM: number
  BASE_TABLE: string
  INPUT_VARS: number
  INPUT_OBS: number
  SUBMITTED_BY_NM: string
  SUBMITTED_ON_DTTM: number
  SUBMITTED_REASON_TXT: string
  APPROVER: string
  REVIEW_STATUS_ID: string
  REVIEWED_BY_NM: string
  REVIEWED_ON_DTTM?: any
  REVIEW_REASON_TXT: string
  TECH_TO_DTTM: number
  NUM_OF_APPROVALS_REQUIRED?: any
  [key: string]: any
}

export interface New {
  LIBREF: string
  DSN: string
  NUM_OF_APPROVALS_REQUIRED: number
  LOADTYPE: string
  BUSKEY: string
  VAR_TXFROM: string
  VAR_TXTO: string
  VAR_BUSFROM: string
  VAR_BUSTO: string
  VAR_PROCESSED: string
  CLOSE_VARS: string
  PRE_EDIT_HOOK: string
  POST_EDIT_HOOK: string
  PRE_APPROVE_HOOK: string
  POST_APPROVE_HOOK: string
  SIGNOFF_COLS: string
  SIGNOFF_HOOK: string
  NOTES: string
  RK_UNDERLYING: string
  HELPFUL_LINK: string
  [key: string]: any
}

export interface Update {
  LIBREF: string
  DSN: string
  NUM_OF_APPROVALS_REQUIRED: number
  LOADTYPE: string
  BUSKEY: string
  VAR_TXFROM: string
  VAR_TXTO: string
  VAR_BUSFROM: string
  VAR_BUSTO: string
  VAR_PROCESSED: string
  CLOSE_VARS: string
  PRE_EDIT_HOOK: string
  POST_EDIT_HOOK: string
  PRE_APPROVE_HOOK: string
  POST_APPROVE_HOOK: string
  SIGNOFF_COLS: string
  SIGNOFF_HOOK: string
  NOTES: string
  RK_UNDERLYING: string
  HELPFUL_LINK: string
  [key: string]: any
}

export interface Original {
  LIBREF: string
  DSN: string
  NUM_OF_APPROVALS_REQUIRED: number
  LOADTYPE: string
  BUSKEY: string
  VAR_TXFROM: string
  VAR_TXTO: string
  VAR_BUSFROM: string
  VAR_BUSTO: string
  VAR_PROCESSED: string
  CLOSE_VARS: string
  PRE_EDIT_HOOK: string
  POST_EDIT_HOOK: string
  PRE_APPROVE_HOOK: string
  POST_APPROVE_HOOK: string
  SIGNOFF_COLS: string
  SIGNOFF_HOOK: string
  NOTES: string
  RK_UNDERLYING: string
  HELPFUL_LINK: string
  [key: string]: any
}
