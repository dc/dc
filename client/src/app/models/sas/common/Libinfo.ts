export interface Libinfo {
  ENGINE: string
  LIBNAME: string
  PATHS: string
  PERMS: string
  OWNERS: string
  SCHEMAS: string
  LIBID: string
  LIBSIZE: number
  TABLE_CNT: number
}
