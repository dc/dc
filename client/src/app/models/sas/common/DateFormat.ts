export interface DataFormat {
  format: string
  label: string
  length: string
  type: string
}
