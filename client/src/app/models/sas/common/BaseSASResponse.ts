export interface BaseSASResponse {
  SYSDATE: string
  SYSTIME: string
  SYSUSERID: string
  MF_GETUSER: string
  SYS_JES_JOB_URI: string
  SYSJOBID: string
  _DEBUG: string
  _PROGRAM: string
  SYSCC: string
  SYSERRORTEXT: string
  SYSHOSTNAME: string
  SYSSCPL: string
  SYSSITE: string
  SYSVLONG: string
  SYSWARNINGTEXT: string
  END_DTTM: string
  MEMSIZE: string
}
