import { Col } from 'src/app/shared/dc-validator/models/col.model'
import { DQRule } from 'src/app/shared/dc-validator/models/dq-rules.model'
import { DQData, SASParam } from '../TableData'
import { BaseSASResponse } from './common/BaseSASResponse'
import { DataFormat } from './common/DateFormat'

export interface EditorsGetDataServiceResponse {
  data: EditorsGetDataSASResponse
  libds: string
}

export interface EditorsGetDataSASResponse extends BaseSASResponse {
  $sasdata: $DataFormats
  sasdata: Sasdata[]
  sasparams: SASParam[]
  approvers: Approver[]
  dqrules: DQRule[]
  dsmeta: DSMeta[]
  dqdata: DQData[]
  versions: Version[]
  cols: Col[]
  maxvarlengths: Maxvarlength[]
  xl_rules: any[]
  query: any[]
}

export interface DSMeta {
  ODS_TABLE: string
  NAME: string
  VALUE: string
  [key: string]: string
}

export interface Version {
  LOAD_REF: string
  USER_NM: string
  VERSION_DTTM: string
  VERSION_DESC: string
  CHANGED_RECORDS: number
  NEW_RECORDS: number
  DELETED_RECORDS: number
  [key: string]: string | number
}

export interface Sasdata {
  _____DELETE__THIS__RECORD_____: string
  PRIMARY_KEY_FIELD: number
  SOME_CHAR: string
  SOME_DROPDOWN: string
  SOME_NUM: number
  SOME_DATE: string
  SOME_DATETIME: string
  SOME_TIME: string
  SOME_SHORTNUM: number
  SOME_BESTNUM: number
}

export interface Vars {
  _____DELETE__THIS__RECORD_____: DataFormat
  PRIMARY_KEY_FIELD: DataFormat
  SOME_CHAR: DataFormat
  SOME_DROPDOWN: DataFormat
  SOME_NUM: DataFormat
  SOME_DATE: DataFormat
  SOME_DATETIME: DataFormat
  SOME_TIME: DataFormat
  SOME_SHORTNUM: DataFormat
  SOME_BESTNUM: DataFormat
  [key: string]: DataFormat
}
export interface $DataFormats {
  vars: Vars
}

export interface Approver {
  PERSONNAME: string
  EMAIL: string
  USERID: string
}

export interface Maxvarlength {
  NAME: string
  MAXLEN: number
}
