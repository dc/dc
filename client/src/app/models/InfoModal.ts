export class InfoModal {
  id?: number
  modalTitle?: string
  sasService: string | null = null
  message: string = ''
  details: AbortDetails | null = new AbortDetails()
}

export class AbortDetails {
  SYSWARNINGTEXT?: string
  SYSERRORTEXT?: string
  MAC?: string
  LOG?: string
}
