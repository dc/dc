export interface LicenseKeyData {
  valid_until: string
  users_allowed: number
  site_id: string
  site_id_multiple: string[]
  demo: boolean
  hot_license_key: string | undefined
  features?: string
}
