export interface FilterClause {
  ddtype: string
  invalidClause: boolean
  logic: any
  operator: string
  operators: string[]
  type: string
  value: any
  valueVariable: boolean
  values: { formatted: string; unformatted: any }[]
  variable: string
}

export interface FilterGroup {
  filterClauses: FilterClause[]
  clauseLogic?: string
}

export interface FilterQuery {
  groupLogic: string
  filterGroups: FilterGroup[]
}
