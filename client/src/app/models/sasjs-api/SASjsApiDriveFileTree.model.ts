export interface SASjsApiTree {
  name: string
  relativePath: string
  absolutePath: string
  children: SASjsApiTree[]
}

export interface SASjsApiDriveFileTree {
  status: string
  tree: SASjsApiTree
}
