export interface SASjsApiServerInfo {
  mode: string
  cors: string
  whiteList: string[]
  protocol: string
  runTimes: string[]
}
