export interface SASjsApiDriveFolderContents {
  folders: string[]
  files: string[]
}
