export interface RequestWrapperOptions {
  suppressSuccessAbortModal?: boolean
  suppressErrorAbortModal?: boolean
}
