export interface RequestWrapperResponse<responseType = any> {
  adapterResponse: responseType
  log?: string
}
