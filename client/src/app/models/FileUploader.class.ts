export class FileUploader {
  queue: File[] = []

  addToQueue(files: File[]): void {
    this.queue.push(...files)
  }
}
