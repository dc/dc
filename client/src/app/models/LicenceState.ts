export interface LicenceState {
  users_allowed?: number
  viewer_rows_allowed: number
  editor_rows_allowed: number
  stage_rows_allowed: number
  history_rows_allowed: number
  submit_rows_limit: number
  viewbox_limit: number
  lineage_daily_limit: number
  tables_in_library_limit: number //If limit is 5 that means first 5 tables in the list will be UNLOCKED, rest of it LOCKED
  viewbox: boolean
  fileUpload: boolean
  editRecord: boolean
  addRecord: boolean
}

/**
 * '-' means unset
 * '0' disabled
 * '1' enabled
 * '10' number when not toggling position
 * ',' separator
 *
 * Example:
 * coding: '10,15,15,-,15,5,35,1,1,3,1,1,1,1'
 */
export enum LicenceFeaturesMap {
  viewer_rows_allowed = 0,
  editor_rows_allowed = 1,
  stage_rows_allowed = 2,
  history_rows_allowed = 3,
  submit_rows_limit = 4,
  tables_in_library_limit = 5,
  viewbox = 6,
  viewbox_limit = 7,
  lineage_daily_limit = 8,
  fileUpload = 9,
  editRecord = 10,
  addRecord = 11
}
