export default interface SheetInfo {
  foundData: boolean
  sheetName: string
  startRow: number
  endRow: number
  csvArrayHeadersMap: any
  missingHeaders: string[]
  rangeStartRow: number
  rangeStartCol: number
  rangeAddress?: string
}
