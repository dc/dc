export interface ErrorBody {
  message: string
  details: any
  raw: any
}
