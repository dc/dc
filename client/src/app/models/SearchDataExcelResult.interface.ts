import { MissingHeaders } from './RangeInfo'

export interface SearchDataExcelResult {
  missing?: MissingHeaders[]
  found?: {
    data: any
    arrayData: any[]
    sheetName: string
    headers: string[]
    startAddress?: string
    endAddress?: string
  }
}
