export interface QueryClause {
  GROUP_LOGIC: string
  SUBGROUP_LOGIC: string
  SUBGROUP_ID: number
  VARIABLE_NM: any
  OPERATOR_NM: string
  RAW_VALUE: string
}

export interface ColumnDetail {
  DDTYPE: string
  FORMAT: string
  LABEL: string
  LENGTH: number
  NAME: string
  TYPE: string
  VARNUM: number
}

export interface Approver {
  PERSONNAME: string
  EMAIL: string
  USERID: string
}

export interface DQData {
  BASE_COL: string
  RULE_DATA: string
  RULE_VALUE: string
  SELECTBOX_ORDER: number
}

export interface MaxVarLength {
  MAXLEN: number
  NAME: string
}

export interface SASParam {
  COLHEADERS: string
  COLTYPE: string
  DTTMVARS: string
  DTVARS: string
  CLS_FLAG: number
  FILTER_TEXT: string
  LOADTYPE: string
  PK: string
  PKCNT: number
  RK_FLAG: number
  TMVARS: string
}

export interface ExcelRule {
  XL_COLUMN: string
  XL_RULE: string
}
