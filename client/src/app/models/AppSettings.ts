export interface AppSettings {
  persistSelectedTheme: boolean
  selectedTheme: AppThemes
}

export enum AppThemes {
  light = 'light',
  dark = 'dark'
}
