export interface CellValidationSource {
  col: number
  row: number
  strict: boolean
  values: any[]
  extended_values?: string[]
  hash: string
  count: number
}
