export default interface FoundRangeInfo {
  found: boolean
  sheetName: string
  rangeStartAddress: string
  rangeEndAddress: string
  rangeAddress: string
  missingHeaders: MissingHeaders[]
}

export interface MissingHeaders {
  sheetName: string
  missingHeaders: string[]
}
