import Handsontable from 'handsontable'

export interface HotTableInterface extends Handsontable.GridSettings {
  height?: number | string | (() => number | string)
  data: any
  hidden?: boolean
  settings: Handsontable.GridSettings
}
