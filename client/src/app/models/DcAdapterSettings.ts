import { SASjsConfig } from '@sasjs/adapter'

export interface DcAdapterSettings extends Partial<SASjsConfig> {
  adminGroup: string
  dcPath: string
  hotLicenceKey: string
}
