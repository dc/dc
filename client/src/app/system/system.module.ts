import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { SystemRoutingModule } from './system-routing.module'
import { SystemComponent } from './system.component'
import { ClarityModule } from '@clr/angular'
import { FormsModule } from '@angular/forms'

@NgModule({
  declarations: [SystemComponent],
  imports: [CommonModule, SystemRoutingModule, ClarityModule, FormsModule]
})
export class SystemModule {}
