export interface AppInfo {
  adapterVersion: string
  appVersion: string
  buildTimestamp: string
}
