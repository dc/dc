export interface EnvironmentInfo {
  SYSSITE?: string
  SYSSCPL?: string
  SYSTCPIPHOSTNAME?: string
  SYSVLONG?: string
  MEMSIZE?: string
  SYSPROCESSMODE?: string
  SYSHOSTNAME?: string
  SYSUSERID?: string
  SYSHOSTINFOLONG?: string
  SYSENCODING?: string
  AUTOEXEC?: string
  ISADMIN?: number
  DC_ADMIN_GROUP?: string
  APP_LOC?: string
}
