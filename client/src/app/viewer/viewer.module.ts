import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ViewerComponent } from './viewer.component'
import { ViewRouteComponent } from '../routes/view-route/view-route.component'
import { HotTableModule } from '@handsontable/angular'
import { ViewerRoutingModule } from './viewer-routing.module'
import { ClarityModule } from '@clr/angular'
import { FormsModule } from '@angular/forms'
import { ClipboardModule } from 'ngx-clipboard'
import { AppSharedModule } from '../app-shared.module'
import { PipesModule } from '../pipes/pipes.module'
import { SharedModule } from '../shared/shared.module'
import { ViewboxesModule } from '../shared/viewboxes/viewboxes.module'
import { QueryModule } from '../query/query.module'
import { DirectivesModule } from '../directives/directives.module'
import { UserComponent } from '../user/user.component'
import { RoleComponent } from '../role/role.component'
import { GroupComponent } from '../group/group.component'
import { LineageComponent } from '../lineage/lineage.component'
import { MetadataComponent } from '../metadata/metadata.component'

@NgModule({
  declarations: [
    ViewerComponent,
    ViewRouteComponent,
    UserComponent,
    RoleComponent,
    GroupComponent,
    LineageComponent,
    MetadataComponent
  ],
  imports: [
    ViewboxesModule,
    CommonModule,
    ViewerRoutingModule,
    ClipboardModule,
    FormsModule,
    ClarityModule,
    HotTableModule.forRoot(),
    AppSharedModule,
    SharedModule,
    PipesModule,
    QueryModule,
    DirectivesModule
  ]
})
export class ViewerModule {}
