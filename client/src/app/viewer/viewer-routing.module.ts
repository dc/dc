import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { GroupComponent } from '../group/group.component'
import { LineageComponent } from '../lineage/lineage.component'
import { MetadataComponent } from '../metadata/metadata.component'
import { RoleComponent } from '../role/role.component'
import { UsernavRouteComponent } from '../routes/usernav-route/usernav-route.component'
import { ViewRouteComponent } from '../routes/view-route/view-route.component'
import { UserComponent } from '../user/user.component'
import { ViyaApiExplorerComponent } from '../viya-api-explorer/viya-api-explorer.component'
import { ViewerComponent } from './viewer.component'

const routes: Routes = [
  {
    path: '',
    component: ViewRouteComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'data' },
      { path: 'data', component: ViewerComponent },
      { path: 'data', component: ViewerComponent },
      { path: 'data/:libMem/:filterId', component: ViewerComponent },
      { path: 'data/:libMem', component: ViewerComponent },
      { path: 'lineage', component: LineageComponent },
      { path: 'lineage/:tableid/:direction', component: LineageComponent },
      {
        path: 'lineage/column/:coluri/:direction',
        component: LineageComponent
      },
      {
        path: 'lineage/column/:coluri/:direction/:reload',
        component: LineageComponent
      },
      { path: 'viya-api-explorer', component: ViyaApiExplorerComponent },
      { path: 'metadata', component: MetadataComponent },
      { path: 'metadata/object/:objectID', component: MetadataComponent },
      {
        path: 'metadata/object/:objectID/:objectName',
        component: MetadataComponent
      },
      {
        path: 'usernav',
        component: UsernavRouteComponent,
        children: [
          { path: '', pathMatch: 'full', redirectTo: 'groups' },
          { path: 'users', component: UserComponent },
          { path: 'users/:uri', component: UserComponent },
          { path: 'groups', component: GroupComponent },
          { path: 'groups/:uri', component: GroupComponent },
          { path: 'roles', component: RoleComponent },
          { path: 'roles/:uri', component: RoleComponent }
        ]
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewerRoutingModule {}
