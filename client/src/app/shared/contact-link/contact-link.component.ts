import { Component, Input, OnInit } from '@angular/core'

@Component({
  selector: 'contact-link',
  templateUrl: './contact-link.component.html',
  styleUrls: ['./contact-link.component.scss']
})
export class ContactLinkComponent implements OnInit {
  @Input() classes: string = ''

  constructor() {}

  ngOnInit(): void {}
}
