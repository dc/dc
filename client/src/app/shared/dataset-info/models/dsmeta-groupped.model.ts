export interface Tab<T> {
  name: string
  title: string
  /**
   * Columns to be displayed in the the grid
   * If empty, all columns will be displayed
   */
  colsToDisplay: {
    colKey: string
    colName?: string
  }[]
  meta: T[]
  onRowClick?: (value: any) => void
}
