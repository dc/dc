import { Component } from '@angular/core'
import { Observable } from 'rxjs'
import { ExcelPasswordModalService } from './excel-password-modal.service'
import { Options } from './models/options.interface'

@Component({
  selector: 'app-excel-password-modal',
  styleUrls: ['./excel-password-modal.component.scss'],
  templateUrl: './excel-password-modal.component.html'
})
export class ExcelPasswordModalComponent {
  options$: Observable<Options> = this.excelPasswordModalService.optionsSubject$

  fileUnlockError: boolean = false

  passwordInput: string = ''

  constructor(private excelPasswordModalService: ExcelPasswordModalService) {}

  close(password?: string) {
    this.passwordInput = ''
    this.excelPasswordModalService.close(password)
  }
}
