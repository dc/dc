import { Injectable } from '@angular/core'
import { Subject, Observable } from 'rxjs'
import { OpenOptions, Options } from './models/options.interface'
import { Result } from './models/result.interface'

@Injectable({
  providedIn: 'root'
})
export class ExcelPasswordModalService {
  public optionsSubject$: Subject<Options> = new Subject()
  public resultChange$: Subject<Result> = new Subject()

  constructor() {}

  public open(openOptions?: OpenOptions): Observable<Result> {
    this.optionsSubject$.next({
      open: true,
      ...openOptions
    })

    this.resultChange$ = new Subject<Result>()
    return this.resultChange$.asObservable()
  }

  close(password?: string) {
    this.optionsSubject$.next({
      open: false
    })

    this.resultChange$.next({
      password
    })
    this.resultChange$.complete()
  }
}
