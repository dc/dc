export interface OpenOptions {
  error?: boolean
}

export interface Options extends OpenOptions {
  open: boolean
}
