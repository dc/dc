import Handsontable from 'handsontable'
import Core from 'handsontable/core'

export class CustomAutocompleteEditor extends Handsontable.editors
  .AutocompleteEditor {
  constructor(instance: Core) {
    super(instance)
  }

  createElements() {
    super.createElements()
  }

  // Listbox open
  open(event?: Event | undefined): void {
    super.open(event)

    if (this.isCellNumeric()) {
      this.htContainer.classList.add('numericListbox')
    } else {
      this.htContainer.classList.remove('numericListbox')
    }
  }

  isCellNumeric() {
    return this.cellProperties?.className?.includes('htNumeric')
  }
}
