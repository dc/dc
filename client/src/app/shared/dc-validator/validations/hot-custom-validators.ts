import { isSpecialMissing } from '@sasjs/utils/input/validators'

/**
 * Custom validator for numeric field that contains speical missings
 */
export function specialMissingNumericValidator(
  value: any,
  callback?: (valid: boolean) => void
) {
  if (value === undefined || value === null) {
    if (callback) callback(true)
    return true
  }

  if (!isNaN(value) && isFinite(value)) {
    if (callback) callback(true)
    return true
  }

  const valid = isSpecialMissing(value)

  if (callback) callback(valid)
  return valid
}
