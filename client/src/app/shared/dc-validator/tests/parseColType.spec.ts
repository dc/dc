import { HotColumnSettings } from '../models/dc-validation.model'
import { parseColType } from '../utils/parseColType'

describe('DC Validator - parse col type', () => {
  it('should return array of parsed json', () => {
    const colTypeString =
      '{"data":"test","test2":"test2"}, {"data":"test3","test4":"test4"}'
    const expected: HotColumnSettings[] = [
      { data: 'test', test2: 'test2' },
      { data: 'test3', test4: 'test4' }
    ]

    expect(parseColType(colTypeString)).toEqual(expected)
  })

  it('should return empty array for invalid json', () => {
    const colTypeString =
      '{"test":"test""test:"test2"}, {"test3":"test3","test4":"test4"}'
    const expected: HotColumnSettings[] = []

    expect(parseColType(colTypeString)).toEqual(expected)
  })
})
