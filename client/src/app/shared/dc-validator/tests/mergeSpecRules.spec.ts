import { Col } from '../models/col.model'
import { DcValidation } from '../models/dc-validation.model'
import { mergeColsRules } from '../utils/mergeColsRules'

describe('DC Validator - merge spec rules', () => {
  it('should return array of merged specs', () => {
    const rules: DcValidation[] = [
      {
        data: 'test_col'
      }
    ]
    const cols: Col[] = [
      {
        NAME: 'test_col',
        MEMLABEL: 'string',
        DESC: 'test_desc',
        LONGDESC: 'string',
        TYPE: '',
        CLS_RULE: 'cls_rule',
        VARNUM: 0,
        LABEL: 'string',
        FMTNAME: 'string',
        DDTYPE: 'string'
      }
    ]
    const $dataFormats: any = {
      vars: {
        test_col: {
          format: 'best.',
          label: 'PRIMARY_KEY_FIELD',
          length: '8',
          type: 'test_type'
        }
      }
    }
    const expected: DcValidation[] = [
      {
        data: 'test_col',
        desc: 'test_desc',
        clsRule: 'cls_rule',
        length: 8
      }
    ]

    expect(mergeColsRules(cols, rules, $dataFormats)).toEqual(expected)
    expect(cols[0].TYPE).toEqual('test_type')
  })
})
