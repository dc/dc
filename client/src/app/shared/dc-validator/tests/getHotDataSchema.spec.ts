import { getHotDataSchema } from '../utils/getHotDataSchema'

describe('DC Validator - hot data schema', () => {
  it('should return correct value for col type', () => {
    expect(getHotDataSchema('numeric')).toEqual('')
    expect(
      getHotDataSchema('autocomplete', { data: '', source: [1, 2, 3] })
    ).toEqual(1)
    expect(getHotDataSchema('missing')).toEqual('')
  })
})
