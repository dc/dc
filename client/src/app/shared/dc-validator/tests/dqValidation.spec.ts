import { DQRule } from '../models/dq-rules.model'
import { dqValidate } from '../validations/dq-validation'

describe('DC Validator - dq validation', () => {
  it('should validate MINVAL value', () => {
    const validValue = 5
    const validValue2 = 6
    const invalidValue = 2
    const invalidStringValue = '2'
    const numericStringValue = '5'
    const numericSpecialMissingValue = 's'

    const dqRules: DQRule[] = [
      {
        BASE_COL: 'test',
        RULE_TYPE: 'MINVAL',
        RULE_VALUE: '5',
        X: 0
      }
    ]

    expect(dqValidate(dqRules, validValue)).toBeTrue()
    expect(dqValidate(dqRules, validValue2)).toBeTrue()
    expect(dqValidate(dqRules, invalidValue)).toBeFalse()
    expect(dqValidate(dqRules, invalidStringValue)).toBeFalse()
    expect(dqValidate(dqRules, numericStringValue)).toBeTrue()
    expect(dqValidate(dqRules, numericSpecialMissingValue)).toBeFalse()
  })

  it('should validate MAXVAL value', () => {
    const validValue = 5
    const validValue2 = 4
    const invalidValue = 6
    const numericStringValue = '4'
    const numericSpecialMissingValue = 's'
    const invalidStringValue = '6'

    const dqRules: DQRule[] = [
      {
        BASE_COL: 'test',
        RULE_TYPE: 'MAXVAL',
        RULE_VALUE: '5',
        X: 0
      }
    ]

    expect(dqValidate(dqRules, validValue)).toBeTrue()
    expect(dqValidate(dqRules, validValue2)).toBeTrue()
    expect(dqValidate(dqRules, invalidValue)).toBeFalse()
    expect(dqValidate(dqRules, invalidStringValue)).toBeFalse()
    expect(dqValidate(dqRules, numericStringValue)).toBeTrue()
    expect(dqValidate(dqRules, numericSpecialMissingValue)).toBeTrue()
  })

  it('should validate UPCASE value', () => {
    const validValue = 'VAL'
    const invalidValue = 'val'
    const numberValue = 1

    const dqRules: DQRule[] = [
      {
        BASE_COL: 'test',
        RULE_TYPE: 'CASE',
        RULE_VALUE: 'UPCASE',
        X: 0
      }
    ]

    expect(dqValidate(dqRules, validValue)).toBeTrue()
    expect(dqValidate(dqRules, invalidValue)).toBeFalse()
    expect(dqValidate(dqRules, numberValue)).toBeFalse()
  })

  it('should validate LOWCASE value', () => {
    const validValue = 'val'
    const invalidValue = 'VAL'
    const numberValue = 1

    const dqRules: DQRule[] = [
      {
        BASE_COL: 'test',
        RULE_TYPE: 'CASE',
        RULE_VALUE: 'LOWCASE',
        X: 0
      }
    ]

    expect(dqValidate(dqRules, validValue)).toBeTrue()
    expect(dqValidate(dqRules, invalidValue)).toBeFalse()
    expect(dqValidate(dqRules, numberValue)).toBeFalse()
  })

  it('should validate NOTNULL value', () => {
    const validValue = '1'
    const validValue2 = 1
    const invalidValue = ''
    const invalidValue2 = null

    const dqRules: DQRule[] = [
      {
        BASE_COL: 'test',
        RULE_TYPE: 'NOTNULL',
        RULE_VALUE: ' ',
        X: 0
      }
    ]

    expect(dqValidate(dqRules, validValue)).toBeTrue()
    expect(dqValidate(dqRules, validValue2)).toBeTrue()
    expect(dqValidate(dqRules, invalidValue)).toBeFalse()
    expect(dqValidate(dqRules, invalidValue2)).toBeFalse()
  })

  it('should return true if rule not found', () => {
    const validValue = 5

    const dqRules: any[] = [
      {
        BASE_COL: 'test',
        RULE_TYPE: 'NOT_EXIST',
        RULE_VALUE: '5',
        X: 0
      }
    ]

    expect(dqValidate(dqRules, validValue)).toBeTrue()
  })
})
