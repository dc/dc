import Handsontable from 'handsontable'

export interface HotColumnSettings extends Handsontable.ColumnSettings {
  data: string
}

export interface DcColumnSettings {
  length?: number
  format?: number
  valid?: boolean
  desc?: string
  clsRule?: string
}

export interface DcValidation extends HotColumnSettings, DcColumnSettings {}

export interface DcValidationRuleUpdate
  extends Handsontable.ColumnSettings,
    DcColumnSettings {
  data?: string
}
