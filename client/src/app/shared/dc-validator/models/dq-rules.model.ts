export interface DQRule {
  BASE_COL: string
  RULE_TYPE: DQRuleTypes
  RULE_VALUE: string
  X: number
}

export type DQRuleTypes =
  | 'SOFTSELECT'
  | 'HARDSELECT'
  | 'SOFTSELECT_HOOK'
  | 'HARDSELECT_HOOK'
  | 'NOTNULL'
  | 'CASE'
  | 'MINVAL'
  | 'MAXVAL'
