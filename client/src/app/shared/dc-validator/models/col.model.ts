export interface Col {
  NAME: string
  VARNUM: number
  LABEL: string
  FMTNAME: string
  DDTYPE: string
  TYPE: string
  CLS_RULE: string
  MEMLABEL: string
  DESC: string
  LONGDESC: string
}
