import { DcValidation } from '../models/dc-validation.model'

const schemaTypeMap: { [key: string]: any } = {
  numeric: '',
  default: ''
}

/**
 * Schema defines the default values for given types. For example when new row is added.
 */
export const getHotDataSchema = (
  type: string | undefined,
  cellValidation?: DcValidation
): any => {
  if (!type) return schemaTypeMap.default

  switch (type) {
    case 'autocomplete': {
      return cellValidation && cellValidation.source
        ? (cellValidation.source as string[] | number[])[0]
        : []
    }
    default: {
      if (schemaTypeMap.hasOwnProperty(type)) return schemaTypeMap[type]

      return schemaTypeMap.default
    }
  }
}
