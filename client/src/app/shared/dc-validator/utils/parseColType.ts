import { HotColumnSettings } from '../models/dc-validation.model'

/**
 * From sas we get a string instead of array of objects, in that string `[]` are missing so
 * before parsing JSON we need to add them.
 *
 * @param coltype string (objects) that comes from sas
 * @returns JSON Handsontable.ColumnSettings[]
 */
export const parseColType = (coltype: string): HotColumnSettings[] => {
  try {
    return JSON.parse(`[${coltype}]`)
  } catch (err: any) {
    return []
  }
}
