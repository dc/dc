import { DcValidation } from '../models/dc-validation.model'
import * as languages from 'numbro/dist/languages.min'
/**
 * Applying the numeric formats based on the browser locale/language
 * So that correct decimal separators are applied.
 * For example european format (thousand dot, decimal comma): 1.000,00
 *
 * @param rules Cell Validation rules to be updated
 * Those rules are passed in the `columns` property Of handsontable settings.
 */
export const applyNumericFormats = (rules: DcValidation[]): DcValidation[] => {
  const lang = languages[window.navigator.language]

  if (!lang) return rules

  for (let rule of rules) {
    if (rule.type === 'numeric')
      rule.numericFormat = {
        pattern: '0,0',
        culture: window.navigator.language // use this for EUR (German),
        // more cultures available on http://numbrojs.com/languages.html
      }
  }

  return rules
}
