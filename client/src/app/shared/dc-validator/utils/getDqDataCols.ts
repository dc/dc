import { DQData } from 'src/app/models/TableData'

export const getDqDataCols = (dqData: DQData[]): string[] => {
  const cols: string[] = []

  dqData.forEach((element: DQData) => {
    if (!cols.includes(element.BASE_COL)) cols.push(element.BASE_COL)
  })

  return cols
}
