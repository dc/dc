import { NgModule, OnInit } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { ClarityModule } from '@clr/angular'
import { RouterModule } from '@angular/router'

import { LoadingIndicatorComponent } from './loading-indicator/loading-indicator.component'
import { LoginComponent } from './login/login.component'
import { UserService } from './user.service'
import { AlertsService } from './alerts/alerts.service'
import { HeaderActions } from './user-nav-dropdown/header-actions.component'
import { AlertsComponent } from './alerts/alerts.component'
import { TermsComponent } from './terms/terms.component'
import { DirectivesModule } from '../directives/directives.module'
import { DatasetInfoComponent } from './dataset-info/dataset-info.component'
import { ContactLinkComponent } from './contact-link/contact-link.component'
import { ExcelPasswordModalComponent } from './excel-password-modal/excel-password-modal.component'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ClarityModule,
    DirectivesModule
  ],
  declarations: [
    LoadingIndicatorComponent,
    LoginComponent,
    HeaderActions,
    AlertsComponent,
    TermsComponent,
    DatasetInfoComponent,
    ContactLinkComponent,
    ExcelPasswordModalComponent
  ],
  exports: [
    LoadingIndicatorComponent,
    LoginComponent,
    HeaderActions,
    AlertsComponent,
    TermsComponent,
    DatasetInfoComponent,
    ContactLinkComponent,
    ExcelPasswordModalComponent
  ],
  providers: [UserService, AlertsService]
})
export class SharedModule implements OnInit {
  ngOnInit(): void {}
}
