import { Saslibs } from 'src/app/models/sas/public-viewlibs.model'

export interface LibraryClickEmitter {
  libid: string
  library: Saslibs
  tablesLoaded: boolean
}
