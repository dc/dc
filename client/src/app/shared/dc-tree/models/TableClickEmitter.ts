import { Saslibs } from 'src/app/models/sas/public-viewlibs.model'

export interface TableClickEmitter {
  tableuri: string
  libTable: string
  columnsLoaded: boolean
  library: Saslibs
}
