import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { DcTreeComponent } from './dc-tree.component'
import { ClarityModule } from '@clr/angular'
import { FormsModule } from '@angular/forms'
import { DirectivesModule } from 'src/app/directives/directives.module'

@NgModule({
  declarations: [DcTreeComponent],
  imports: [
    CommonModule,
    ClarityModule,
    CommonModule,
    FormsModule,
    DirectivesModule
  ],
  exports: [DcTreeComponent]
})
export class DcTreeModule {}
