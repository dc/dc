import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ClarityModule } from '@clr/angular'
import { FormsModule } from '@angular/forms'
import { ViewboxesComponent } from './viewboxes.component'
import { QueryModule } from 'src/app/query/query.module'
import { HotTableModule } from '@handsontable/angular'
import { DragDropModule } from '@angular/cdk/drag-drop'
import { AutocompleteModule } from '../autocomplete/autocomplete.module'
import { DcTreeModule } from '../dc-tree/dc-tree.module'
import { DirectivesModule } from 'src/app/directives/directives.module'

@NgModule({
  declarations: [ViewboxesComponent],
  imports: [
    CommonModule,
    ClarityModule,
    CommonModule,
    FormsModule,
    QueryModule,
    HotTableModule,
    DragDropModule,
    AutocompleteModule,
    DcTreeModule,
    DirectivesModule
  ],
  exports: [ViewboxesComponent]
})
export class ViewboxesModule {}
