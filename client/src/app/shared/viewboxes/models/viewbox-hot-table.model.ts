import { HotTableInterface } from 'src/app/models/HotTable.interface'

export interface ViewboxHotTable extends HotTableInterface {
  allColHeaders: string[]
  $dataformats: any
  colHeadersHidden: string[]
  colHeadersVisible: string[]
  headerPks: string[]
  cols: any
}
