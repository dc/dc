import { QueryClause } from 'src/app/models/TableData'

export interface Viewbox {
  id: number
  library: string
  table: string
  width: number
  height: number
  x: number
  y: number
  focused?: boolean
  query?: QueryClause[]
  filterText?: string
  columns?: number[]
  filter_pk?: string
  minimized?: boolean
  collapsed?: boolean
  loadingData?: boolean
  searchNumeric?: boolean
  searchLoading?: boolean
}
