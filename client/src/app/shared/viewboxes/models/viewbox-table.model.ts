import { ViewboxHotTable } from './viewbox-hot-table.model'

export interface ViewboxTable {
  viewboxId: number
  viewboxLibDataset: string
  hotTable: ViewboxHotTable
}
