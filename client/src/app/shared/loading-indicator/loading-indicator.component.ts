import { Component, OnDestroy, OnInit } from '@angular/core'
import { Subscription } from 'rxjs'

import { Service } from '../service.interface'

@Component({
  selector: 'app-loading-indicator',
  templateUrl: './loading-indicator.component.html',
  styleUrls: ['./loading-indicator.component.scss']
})
export class LoadingIndicatorComponent implements OnInit, OnDestroy {
  public loading: boolean = false
  public requests: Service[] = []
  private _loadingSub: Subscription = new Subscription()

  constructor() {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this._loadingSub.unsubscribe()
  }
}
