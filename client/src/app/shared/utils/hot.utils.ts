import { globals } from '../../_globals'

/**
 * Function reused in HOT instances to add a class used for dark mode
 */
export const baseAfterGetColHeader = (
  column: number,
  TH: HTMLTableCellElement,
  headerLevel: number
) => {
  // Dark mode
  TH.classList.add(globals.handsontable.darkTableHeaderClass)
}
