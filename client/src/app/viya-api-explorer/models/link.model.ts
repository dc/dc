export interface Link {
  method: string
  rel: string
  href: string
  uri: string
  type: string
  itemType: string
  //Tree related properties
  tables?: any[]
  expanded: boolean
  hidden?: boolean
  loadingTables?: boolean
}
