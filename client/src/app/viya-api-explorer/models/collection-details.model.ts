import { Link } from './link.model'

export interface CollectionDetails {
  links: Link[]
  name: string
  accept: string
  start: number
  items: any[]
  limit: number
  version: number
}
