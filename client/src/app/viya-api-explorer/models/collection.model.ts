import { Link } from './link.model'

export interface Collection {
  links: Link[]
  version: number
  name?: string
  accept?: string
  start?: number
  items?: any[]
  limit?: number
}
