import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { ClarityModule } from '@clr/angular'
import { HotTableModule } from '@handsontable/angular'
import { registerAllModules } from 'handsontable/registry'
import { AppSharedModule } from '../app-shared.module'
import { DirectivesModule } from '../directives/directives.module'
import { PipesModule } from '../pipes/pipes.module'
import { SharedModule } from '../shared/shared.module'
import { EditRecordComponent } from './components/edit-record/edit-record.component'
import { UploadStaterComponent } from './components/upload-stater/upload-stater.component'
import { EditorRoutingModule } from './editor-routing.module'
import { EditorComponent } from './editor.component'
import { DcTreeModule } from '../shared/dc-tree/dc-tree.module'
import { DragDropModule } from '@angular/cdk/drag-drop'
import { ViewboxesModule } from '../shared/viewboxes/viewboxes.module'
import { QueryModule } from '../query/query.module'

// register Handsontable's modules
registerAllModules()

@NgModule({
  declarations: [EditRecordComponent, EditorComponent, UploadStaterComponent],
  imports: [
    ViewboxesModule,
    CommonModule,
    FormsModule,
    EditorRoutingModule,
    ClarityModule,
    HotTableModule.forRoot(),
    AppSharedModule,
    DirectivesModule,
    SharedModule,
    PipesModule,
    DcTreeModule,
    DragDropModule,
    QueryModule
  ]
})
export class EditorModule {}
