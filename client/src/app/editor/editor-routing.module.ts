import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { EditorComponent } from './editor.component'

const ROUTES: Routes = [
  { path: ':libMem', component: EditorComponent },
  { path: ':libMem/:filterId', component: EditorComponent },
  { path: 'edit-record/:libMem', component: EditorComponent },
  { path: 'edit-record/:libMem/:filterId', component: EditorComponent }
]

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(ROUTES)],
  exports: [RouterModule]
})
export class EditorRoutingModule {}
