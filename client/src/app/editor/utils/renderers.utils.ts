/**
 * Custom renderer for HOT cell
 * Used to show error icon
 */
export const errorRenderer = (
  instance: any,
  td: any,
  row: number,
  col: number,
  prop: string,
  value: any,
  cellProperties: any
) => {
  addDarkClass(td)

  td.innerHTML = `${
    value ? value.toString() : ''
  } <cds-icon shape="exclamation-triangle" status="warning"></cds-icon>`

  return td
}

/**
 * Custom renderer for HOT cell
 * Used to revert cell back to original state (no spinner, no error)
 */
export const noSpinnerRenderer = (
  instance: any,
  td: any,
  row: number,
  col: number,
  prop: string,
  value: any,
  cellProperties: any
) => {
  addDarkClass(td)

  td.innerHTML = value ? value : ''

  return td
}

/**
 * Custom renderer for HOT cell
 * Used to show loading spinner in the cell
 * (Spinner shown whilst waiting for SAS to respond)
 */
export const spinnerRenderer = (
  instance: any,
  td: any,
  row: number,
  col: number,
  prop: string,
  value: any,
  cellProperties: any
) => {
  addDarkClass(td)

  td.innerHTML = `${
    value ? value.toString() : ''
  } <span class="spinner spinner-sm vertical-align-middle"></span>`

  return td
}

/**
 * Adds a htDark class to a TD element if not existing
 */
const addDarkClass = (td: any) => {
  if (!td.classList.contains('htDark')) {
    td.classList.add('htDark')
  }
}
