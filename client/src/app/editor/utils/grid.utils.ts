import { Col } from 'src/app/shared/dc-validator/models/col.model'

/**
 * Converts excel date serial number to JS date
 */
export const excelDateToJSDate = (serial: number) => {
  return new Date(Math.round((serial - 25569) * 86400 * 1000))
}

/**
 * Parsing table columns for the HOT in editor
 * Converts array of objects into array of strings, every string is column name (key)
 * @param data array of objects (columns data)
 */
export const parseTableColumns = (data: Col[]): string[] => {
  const columns: string[] = []

  for (let specEntry of data) {
    if (specEntry.NAME !== '_____DELETE__THIS__RECORD_____') {
      columns.push(specEntry.NAME)
    }
  }

  return columns
}

/**
 * Captures headers that are not found in the current table but is found in the uploaded file data
 * @param data
 * @param headers
 * @returns string array of missing headers
 */
export const getMissingHeaders = (data: any, headers: any) => {
  const missingHeaders: string[] = []
  const remainingHeaders: string[] = []

  headers.forEach((element: string) => {
    if (data.indexOf(element) === -1) {
      missingHeaders.push(element)
    } else {
      remainingHeaders.push(element)
    }
  })

  return [missingHeaders, remainingHeaders]
}
