export const isStringNumber = (str: string) => {
  if (/^-{0,1}\d+$/.test(str)) {
    return true
  } else {
    return false
  }
}

export const isStringDecimal = (str: string) => {
  if (/^\d+\.\d+$/.test(str)) {
    return true
  } else {
    return false
  }
}
