/**
 * Converting date object to the UTC time string
 */
export const dateToUtcTime = (date: Date) => {
  let timeStr = ('0' + date.getUTCHours()).slice(-2) + ':'
  timeStr = timeStr + ('0' + date.getUTCMinutes()).slice(-2) + ':'
  timeStr = timeStr + ('0' + date.getUTCSeconds()).slice(-2)
  return timeStr
}

/**
 * Converts date object to the time string
 */
export const dateToTime = (date: Date) => {
  let timeStr = ('0' + date.getHours()).slice(-2) + ':'
  timeStr = timeStr + ('0' + date.getMinutes()).slice(-2) + ':'
  timeStr = timeStr + ('0' + date.getSeconds()).slice(-2)
  return timeStr
}

/**
 * Converts date object to the YYYY-MM-DD
 */
export const dateFormat = (date: Date) => {
  return (
    date.getFullYear() +
    '-' +
    ('0' + (date.getMonth() + 1)).slice(-2) +
    '-' +
    ('0' + date.getDate()).slice(-2)
  )
}
