import { DcValidation } from 'src/app/shared/dc-validator/models/dc-validation.model'

/**
 * Wrapper for DC Validation because we need `noLinkOption` property
 * to be used as a flag to show/hide button that generates link for the
 * edit record modal
 */
export interface EditRecordModal extends DcValidation {
  noLinkOption: boolean
  [key: string]: any
}
