/**
 * Editor restrictions model (based on the licencing)
 */
export interface EditorRestrictions {
  restrictEditRecord?: boolean // Feature is locked but edit/add record buttons are visible so when user clicks he gets the `locked feature modal`
  restrictAddRecord?: boolean // Same as editRecord, but for addRecord
  removeAddRecordButton?: boolean // Removes the buttons in cases as such when Column Level Security is enabled
  removeEditRecordButton?: boolean // Same as removeAddRecordButton
  restrictAddRow?: boolean // locks add row button
  restrictFileUpload?: boolean // locks file upload
}
