/**
 * Model for the dynamic cell validation in the editor
 * (sending whole row to the backend service and recieving data for the cell dropdown)
 */
export interface DynamicExtendedCellValidation {
  DISPLAY_INDEX: number
  DISPLAY_TYPE: string
  DISPLAY_VALUE: string
  EXTRA_COL_NAME: string
  RAW_VALUE_CHAR: string
  RAW_VALUE_NUM: number
  FORCE_FLAG: number
}
