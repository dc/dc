/**
 * Edit record modal - input has been focused event
 */
export interface EditRecordInputFocusedEvent {
  event: any
  colName: number
}

/**
 * Edit record modal - dropdown has been changed event
 */
export interface EditRecordDropdownChangeEvent {
  colName: string
  col: number
}
