import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { ClarityModule } from '@clr/angular'
import { AppSharedModule } from '../app-shared.module'
import { DirectivesModule } from '../directives/directives.module'
import { HomeRouteComponent } from '../routes/home-route/home-route.component'
import { DcTreeModule } from '../shared/dc-tree/dc-tree.module'
import { HomeRoutingModule } from './home-routing.module'
import { HomeComponent } from './home.component'

@NgModule({
  declarations: [HomeComponent, HomeRouteComponent],
  imports: [
    HomeRoutingModule,
    FormsModule,
    ClarityModule,
    AppSharedModule,
    CommonModule,
    DcTreeModule,
    DirectivesModule
  ],
  exports: [HomeComponent]
})
export class HomeModule {}
