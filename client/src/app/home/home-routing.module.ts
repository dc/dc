import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { HomeRouteComponent } from '../routes/home-route/home-route.component'
import { HomeComponent } from './home.component'
import { XLMapModule } from '../xlmap/xlmap.module'
import { MultiDatasetModule } from '../multi-dataset/multi-dataset.module'

const routes: Routes = [
  {
    path: '',
    component: HomeRouteComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'tables' },
      { path: 'tables', component: HomeComponent },
      { path: 'excel-maps', loadChildren: () => XLMapModule },
      { path: 'multi-load', loadChildren: () => MultiDatasetModule }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {}
