import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ApproveDetailsComponent } from './approve-details/approve-details.component'
import { ApproveComponent } from './approve/approve.component'
import { SubmitterComponent } from './submitter/submitter.component'
import { HistoryComponent } from './history/history.component'

const ROUTES: Routes = [
  { path: 'approve', component: ApproveComponent },
  { path: 'approveDet/:tableId', component: ApproveDetailsComponent },
  { path: 'submitted', component: SubmitterComponent },
  { path: 'submitted/:tableId', component: SubmitterComponent },
  { path: 'history', component: HistoryComponent }
]

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(ROUTES)],
  exports: [RouterModule]
})
export class ReviewRoutingModule {}
