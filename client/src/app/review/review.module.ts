import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { ClarityModule } from '@clr/angular'
import { HotTableModule } from '@handsontable/angular'
import { DirectivesModule } from '../directives/directives.module'
import { SharedModule } from '../shared/shared.module'
import { ApproveDetailsComponent } from './approve-details/approve-details.component'
import { ApproveComponent } from './approve/approve.component'
import { ReviewRoutingModule } from './review-routing.module'
import { SubmitterComponent } from './submitter/submitter.component'
import { HistoryComponent } from './history/history.component'

@NgModule({
  declarations: [
    ApproveComponent,
    ApproveDetailsComponent,
    SubmitterComponent,
    HistoryComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReviewRoutingModule,
    ClarityModule,
    HotTableModule.forRoot(),
    DirectivesModule,
    SharedModule
  ]
})
export class ReviewModule {}
