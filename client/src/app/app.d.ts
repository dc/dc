declare module 'save-svg-as-png'
declare module 'numbro/dist/languages.min'
declare interface Navigator {
  msSaveBlob: (blob: any, defaultName?: string) => boolean
}
