/*
 * Copyright (c) 2016 VMware, Inc. All Rights Reserved.
 * This software is released under MIT license.
 * The full license information can be found in LICENSE in the root directory of this project.
 */
import { ModuleWithProviders } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import { NotFoundComponent } from './not-found/not-found.component'

import { DeployModule } from './deploy/deploy.module'
import { EditorModule } from './editor/editor.module'
import { HomeModule } from './home/home.module'
import { LicensingModule } from './licensing/licensing.module'
import { ReviewModule } from './review/review.module'
import { ReviewRouteComponent } from './routes/review-route/review-route.component'
import { StageModule } from './stage/stage.module'
import { SystemModule } from './system/system.module'
import { ViewerModule } from './viewer/viewer.module'

/**
 * Defining routes
 */
export const ROUTES: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'view',
    loadChildren: () => ViewerModule
  },
  {
    /**
     * Load review module (approve, history, submitted)
     */
    path: 'review',
    component: ReviewRouteComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'toapprove' },
      {
        path: '',
        loadChildren: () => ReviewModule
      }
    ]
  },
  {
    path: 'licensing',
    loadChildren: () => LicensingModule
  },
  {
    path: 'home',
    loadChildren: () => HomeModule
  },
  {
    /**
     * Load editor module with subroutes
     */
    path: 'editor',
    loadChildren: () => EditorModule
  },
  {
    path: 'stage',
    loadChildren: () => StageModule
  },
  {
    path: 'system',
    loadChildren: () => SystemModule
  },
  {
    path: 'deploy',
    loadChildren: () => DeployModule
  },
  { path: '**', component: NotFoundComponent }
]

/**
 * Exporting routes
 */
export const ROUTING: ModuleWithProviders<RouterModule> = RouterModule.forRoot(
  ROUTES,
  { useHash: true }
)
