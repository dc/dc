import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { LicensingRoutingModule } from './licensing-routing.module'
import { ClarityModule } from '@clr/angular'
import { SharedModule } from '../shared/shared.module'
import { LicensingComponent } from './licensing.component'
import { FormsModule } from '@angular/forms'

@NgModule({
  declarations: [LicensingComponent],
  imports: [
    CommonModule,
    FormsModule,
    ClarityModule,
    LicensingRoutingModule,
    SharedModule
  ]
})
export class LicensingModule {}
