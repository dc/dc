import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { LicensingGuard } from '../routes/licensing.guard'
import { LicensingComponent } from './licensing.component'

const routes: Routes = [
  {
    path: ':action',
    component: LicensingComponent,
    canActivate: [LicensingGuard],
    canDeactivate: [LicensingGuard]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LicensingRoutingModule {}
