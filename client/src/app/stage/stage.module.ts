import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { StageComponent } from './stage.component'
import { HotTableModule } from '@handsontable/angular'
import { ClarityModule } from '@clr/angular'
import { RouterModule, Routes } from '@angular/router'

const routes: Routes = [{ path: ':tableId', component: StageComponent }]

@NgModule({
  declarations: [StageComponent],
  imports: [
    CommonModule,
    ClarityModule,
    RouterModule.forChild(routes),
    HotTableModule.forRoot()
  ]
})
export class StageModule {}
