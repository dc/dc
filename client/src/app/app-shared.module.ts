import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { SidebarComponent } from './shared/sidebar/sidebar.component'
import { SoftSelectComponent } from './shared/soft-select/soft-select.component'
import { ClarityModule } from '@clr/angular'
import { RouterModule } from '@angular/router'
import { SharedModule } from './shared/shared.module'
import { FormsModule } from '@angular/forms'
import { PipesModule } from './pipes/pipes.module'
import { DirectivesModule } from './directives/directives.module'
import { AutocompleteModule } from './shared/autocomplete/autocomplete.module'

@NgModule({
  declarations: [SidebarComponent, SoftSelectComponent],
  imports: [
    CommonModule,
    FormsModule,
    ClarityModule,
    RouterModule,
    SharedModule,
    PipesModule,
    DirectivesModule,
    AutocompleteModule
  ],
  exports: [SidebarComponent, SoftSelectComponent]
})
export class AppSharedModule {}
