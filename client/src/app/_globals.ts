import { QueryClause } from './models/TableData'

/**
 * Filtering cache info, to be reused when filtering modal is re-open
 */
interface FilterCache {
  cols: any[]
  vals: any[]
  groupLogic: string
  whereClause: string
  libds: string
  clauses: any[]
  query: QueryClause[]
}

/**
 * Filtering cache info in the open viewboxes, to be reused when filtering modal is re-open
 */
interface ViewboxCache {
  [key: number]: {
    filter: FilterCache
  }
}

/**
 * Initial values when no cached values stored
 */
export const initFilter: { filter: FilterCache } = {
  filter: {
    cols: <any[]>[],
    vals: <any[]>[],
    groupLogic: <string>'',
    whereClause: <string>'',
    libds: <string>'',
    clauses: <any>[],
    query: <QueryClause[]>[]
  }
}

export interface XLMapListItem {
  id: string
  description: string
  targetDS: string
}

export interface HandsontableStaticConfig {
  darkTableHeaderClass: string
}

/**
 * Cached filtering values across whole app (editor, viewer, viewboxes)
 * Cached lineage libraries, tables
 * Cached metadata tree
 * Cached usernav tree
 * Cached viyaApi collections, search and selected endpoint
 */
export const globals: {
  rootParam: string
  dcLib: string
  xlmaps: XLMapListItem[]
  editor: any
  viewer: any
  viewboxes: ViewboxCache
  lineage: any
  metadata: any
  viyaApi: any
  usernav: any
  operators: any
  handsontable: HandsontableStaticConfig
  [key: string]: any
} = {
  rootParam: <string>'',
  dcLib: '',
  xlmaps: [],
  editor: {
    startupSet: <boolean>false,
    treeNodeLibraries: <any[] | null>[],
    libsAndTables: <any[]>[],
    libraries: <string[] | undefined>[],
    library: <string>'',
    table: <string>'',
    filter: <FilterCache>{
      cols: <any[]>[],
      vals: <any[]>[],
      groupLogic: <string>'',
      whereClause: <string>'',
      libds: <string>'',
      clauses: <any>[],
      query: <QueryClause[]>[]
    }
  },
  viewer: {
    startupSet: <boolean>false,
    tablesSet: <boolean>false,
    libraries: <any[]>[],
    tables: <any>null,
    library: '',
    table: '',
    libinfo: [],
    librariesSearch: <string>'',
    filter: <FilterCache>{
      cols: <any[]>[],
      vals: <any[]>[],
      groupLogic: <string>'',
      whereClause: <string>'',
      libds: <string>'',
      clauses: <any>[],
      query: <QueryClause[]>[]
    },
    currentSelection: <string>''
  },
  viewboxes: {},
  lineage: {
    libraryList: <any[] | undefined>[],
    tablesList: <any[] | undefined>[],
    columnsList: <any[] | undefined>[],
    librariesSearch: <string>'',
    lib: <String | null>'',
    table: <String | undefined>'',
    column: <String | undefined>'',
    currentLineagePathLibTable: <String>'',
    currentLineagePathColumn: <String>''
  },
  metadata: {
    metaDataList: <any[] | undefined>undefined,
    metaDataSearch: <string>'',
    metaObjectList: <any[] | undefined>[],
    metaObjectSearch: <string>'',
    metaRepositories: <any[] | undefined>undefined,
    selectedRepository: <string>''
  },
  viyaApi: {
    collectionsList: <any[] | undefined>undefined,
    collectionsSearch: <string>'',
    selectedRepository: <string>''
  },
  usernav: {
    userList: <any[] | undefined>undefined,
    userSearch: <string>'',
    groupList: <any[] | undefined>undefined,
    groupSearch: <string>'',
    roleList: <any[] | undefined>undefined,
    roleSearch: <string>''
  },
  operators: {
    numOperators: ['=', '<', '>', '<=', '>=', 'BETWEEN', 'IN', 'NOT IN', 'NE'],
    charOperators: ['=', '<', '>', '<=', '>=', 'CONTAINS', 'IN', 'NOT IN', 'NE']
  },
  handsontable: {
    darkTableHeaderClass: 'darkTH'
  }
}
