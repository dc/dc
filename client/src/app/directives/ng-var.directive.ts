import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core'

/**
 * Purpose of this directive is ability to declare variables in html.
 * It helps writing cleaner code and prevents us from calling function is html
 * Calling functions in html is bad for performance
 */
@Directive({
  selector: '[ngVar]'
})
export class NgVarDirective {
  @Input()
  set ngVar(context: unknown) {
    this.context.$implicit = this.context.ngVar = context

    if (!this.hasView) {
      this.vcRef.createEmbeddedView(this.templateRef, this.context)
      this.hasView = true
    }
  }

  private context: {
    $implicit: unknown
    ngVar: unknown
  } = {
    $implicit: null,
    ngVar: null
  }

  private hasView: boolean = false

  constructor(
    private templateRef: TemplateRef<any>,
    private vcRef: ViewContainerRef
  ) {}
}
