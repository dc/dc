import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { NgVarDirective } from './ng-var.directive'
import { DragNdropDirective } from './drag-ndrop.directive'
import { FileDropDirective } from './file-drop.directive'
import { FileSelectDirective } from './file-select.directive'
import { StealFocusDirective } from './steal-focus.directive'

@NgModule({
  declarations: [
    NgVarDirective,
    DragNdropDirective,
    FileDropDirective,
    FileSelectDirective,
    StealFocusDirective
  ],
  imports: [CommonModule],
  exports: [
    NgVarDirective,
    DragNdropDirective,
    FileDropDirective,
    FileSelectDirective,
    StealFocusDirective
  ]
})
export class DirectivesModule {}
