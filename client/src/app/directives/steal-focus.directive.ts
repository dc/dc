import { Directive, HostListener } from '@angular/core'

@Directive({
  selector: '[appStealFocus]'
})
export class StealFocusDirective {
  constructor() {}

  /**
   * For some reason newest version of Clarity v17.0.1 is stealing focus when
   * clicking on the input inside of the clr-tree-view
   * This is workaround
   */
  @HostListener('click', ['$event']) onClick(event: any) {
    event.target.focus()
  }
}
