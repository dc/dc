/// <reference lib="webworker" />

/**
 * We use normal version of the XLSX (SheetJS)
 * Because at the moment "@sheet/crypto" can't work in the Web Worker environment
 * Because of the missing "global" variable.
 */
import * as XLSX from 'xlsx'

addEventListener('message', ({ data }) => {
  const { data: xldata, opts: xlopts } = data as {
    data: any
    opts?: any
  }

  try {
    const workbook = XLSX.read(xldata, xlopts)

    postMessage({
      event: 'reading_end',
      workbook: workbook
    })
  } catch (err: any) {
    if (err.message.toLowerCase().includes('password')) {
      postMessage({
        error: err
      })
    }
  }
})
