import { LicenceState } from './models/LicenceState'

/**
 * These properties Will be applied when NO KEY FREE TIER is active
 * and when key is FREE TIER it is used for limit values that are not set
 * props from the LICENCE KEY is will be used to override these values
 *
 * @param users_allowed - users allowed IF NO key is provided
 * @param viewer_rows_allowed ---------
 * @param editor_rows_allowed ---- Infinity is unlimited
 * @param stage_rows_allowed ----- positive number is rows limit
 * @param history_rows_allowed ---
 * @param submit_rows_limit -----------
 * @param viewbox_limit - number of viewboxes allowed to open
 * @param lineage_daily_limit - dailiy limit of opening lineage diagrams
 * @param tables_in_library_limit - limit of shown tables in library lists
 * @param viewbox - enable/disable Viewbox feature
 * @param fileUpload - enable/disable File Upload in table feature
 * @param editRecord - enable/disable Edit Record feature
 * @param addRecord - enable/disable Add Record feature
 */
export const freeTierConfig: LicenceState = {
  users_allowed: 1,
  viewer_rows_allowed: 15,
  editor_rows_allowed: 15,
  stage_rows_allowed: Infinity,
  history_rows_allowed: 15,
  submit_rows_limit: 5,
  viewbox_limit: 1,
  lineage_daily_limit: 3,
  tables_in_library_limit: 35,
  viewbox: true,
  fileUpload: true,
  editRecord: true,
  addRecord: true
}
