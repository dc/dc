import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { DeployComponent } from './deploy.component'
import { AutomaticComponent } from './sections/automatic/automatic.component'
import { ManualComponent } from './sections/manual/manual.component'
import { SasjsConfiguratorComponent } from './sections/sasjs-configurator/sasjs-configurator.component'
import { ClarityModule } from '@clr/angular'
import { FormsModule } from '@angular/forms'
import { DeployRoutingModule } from './deploy-routing.module'

@NgModule({
  declarations: [
    DeployComponent,
    AutomaticComponent,
    ManualComponent,
    SasjsConfiguratorComponent
  ],
  imports: [CommonModule, FormsModule, ClarityModule, DeployRoutingModule]
})
export class DeployModule {}
