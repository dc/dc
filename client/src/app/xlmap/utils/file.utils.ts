export const blobToFile = (blob: Blob, fileName: string): File => {
  const file = new File([blob], fileName, {
    lastModified: new Date().getTime()
  })
  return file
}

/**
 * Convert an array of bytes (Uint8Array) to a binary string.
 * @param {Uint8Array} res - The array of bytes to convert.
 * @returns {string} The binary string representation of the array of bytes.
 */
export const byteArrayToBinaryString = (res: Uint8Array): string => {
  // Create a Uint8Array from the input array (if it's not already)
  const bytes = new Uint8Array(res)

  // Initialize an empty string to store the binary representation
  let binary = ''

  // Get the length of the byte array
  const length = bytes.byteLength

  // Iterate through each byte in the array
  for (let i = 0; i < length; i++) {
    // Convert each byte to its binary representation and append to the string
    binary += String.fromCharCode(bytes[i])
  }

  // Return the binary string
  return binary
}
