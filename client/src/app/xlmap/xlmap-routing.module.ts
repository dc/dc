import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import { XLMapComponent } from '../xlmap/xlmap.component'
import { XLMapRouteComponent } from '../routes/xlmap-route/xlmap-route.component'

const routes: Routes = [
  {
    path: '',
    component: XLMapRouteComponent,
    children: [
      { path: '', component: XLMapComponent },
      { path: ':id', component: XLMapComponent }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class XLMapRoutingModule {}
