import {
  extractRowAndCol,
  getCellAddress,
  getFinishingCell,
  isBlankRow
} from '../utils/xl.utils'

describe('isBlankRow', () => {
  it('should return true for a blank row', () => {
    const blankRow = { __rowNum__: 1 }
    expect(isBlankRow(blankRow)).toBeTrue()
  })

  it('should return false for a non-blank row', () => {
    const nonBlankRow = {
      B: 3,
      C: 'some value',
      D: -203
    }
    expect(isBlankRow(nonBlankRow)).toBeFalse()
  })
})

describe('extractRowAndCol', () => {
  it('should extract row and column from "MATCH F R[2]C[0]: CASH BALANCE"', () => {
    const input = 'MATCH F R[2]C[0]: CASH BALANCE'
    const result = extractRowAndCol(input)
    expect(result).toEqual({ row: 2, column: 0 })
  })

  it('should extract row and column from "RELATIVE R[10]C[6]"', () => {
    const input = 'RELATIVE R[10]C[6]'
    const result = extractRowAndCol(input)
    expect(result).toEqual({ row: 10, column: 6 })
  })

  it('should return null for invalid input', () => {
    const invalidInput = 'INVALID INPUT'
    const result = extractRowAndCol(invalidInput)
    expect(result).toBeNull()
  })
})

describe('getCellAddress', () => {
  const arrayOfObjects = [
    { A: 'valueA1', B: 'valueB1' },
    { A: 'valueA2', B: 'valueB2' }
  ]

  it('should convert "ABSOLUTE D8" to A1-style address', () => {
    const input = 'ABSOLUTE D8'
    const result = getCellAddress(input, arrayOfObjects)
    expect(result).toBe('D8')
  })

  it('should convert "RELATIVE R[10]C[6]" to A1-style address', () => {
    const input = 'RELATIVE R[10]C[6]'
    const result = getCellAddress(input, arrayOfObjects)
    expect(result).toBe('F10')
  })

  it('should convert "MATCH 1 R[0]C[0]:valueA1" to A1-style address', () => {
    const input = 'MATCH 1 R[0]C[0]:valueA1'
    const result = getCellAddress(input, arrayOfObjects)
    expect(result).toBe('A1')
  })

  it('should convert "MATCH A R[0]C[0]:valueA1" to A1-style address', () => {
    const input = 'MATCH A R[0]C[0]:valueA1'
    const result = getCellAddress(input, arrayOfObjects)
    expect(result).toBe('A1')
  })

  it('should convert "MATCH 1 R[1]C[0]:valueA1" to A1-style address', () => {
    const input = 'MATCH 1 R[1]C[0]:valueA1'
    const result = getCellAddress(input, arrayOfObjects)
    expect(result).toBe('A2')
  })

  it('should convert "MATCH A R[0]C[1]:valueA1" to A1-style address', () => {
    const input = 'MATCH A R[0]C[1]:valueA1'
    const result = getCellAddress(input, arrayOfObjects)
    expect(result).toBe('B1')
  })

  it('should convert "MATCH 1 R[1]C[1]:valueA1" to A1-style address', () => {
    const input = 'MATCH 1 R[1]C[1]:valueA1'
    const result = getCellAddress(input, arrayOfObjects)
    expect(result).toBe('B2')
  })

  it('should convert "MATCH A R[1]C[1]:valueA1" to A1-style address', () => {
    const input = 'MATCH A R[1]C[1]:valueA1'
    const result = getCellAddress(input, arrayOfObjects)
    expect(result).toBe('B2')
  })
})

describe('getFinishingCell', () => {
  const arrayOfObjects = [
    { A: 'valueA1', B: 'valueB1' },
    { A: 'valueA2', B: 'valueB2' },
    { A: 'valueA3', B: 'valueB3' },
    { B: 'valueB4' },
    { A: 'valueA5' },
    { A: 'valueA6', B: 'valueB6' },
    {},
    { A: 'valueA8' }
  ]

  it('should return the start cell if finish is an empty string', () => {
    const start = 'A1'
    const finish = ''
    const result = getFinishingCell(start, finish, arrayOfObjects)
    expect(result).toBe(start)
  })

  it('should convert "ABSOLUTE D8" to A1-style address', () => {
    const start = 'A1'
    const finish = 'ABSOLUTE D8'
    const result = getFinishingCell(start, finish, arrayOfObjects)
    expect(result).toBe('D8')
  })

  it('should convert "RELATIVE R[2]C[1]" to A1-style address', () => {
    const start = 'A1'
    const finish = 'RELATIVE R[2]C[1]'
    const result = getFinishingCell(start, finish, arrayOfObjects)
    expect(result).toBe('B3')
  })

  it('should convert "MATCH A R[0]C[1]:valueA1" to A1-style address', () => {
    const start = 'A1'
    const finish = 'MATCH A R[0]C[1]:valueA1'
    const result = getFinishingCell(start, finish, arrayOfObjects)
    expect(result).toBe('B1')
  })

  it('should convert "MATCH 1 R[4]C[0]:valueB1" to A1-style address', () => {
    const start = 'A1'
    const finish = 'MATCH 1 R[4]C[0]:valueB1'
    const result = getFinishingCell(start, finish, arrayOfObjects)
    expect(result).toBe('B5')
  })

  it('should convert "LASTDOWN" to A1-style address of the last non-blank cell in column A', () => {
    const start = 'A1'
    const finish = 'LASTDOWN'
    const result = getFinishingCell(start, finish, arrayOfObjects)
    expect(result).toBe('A3')
  })

  it('should convert "BLANKROW" to A1-style address of the last row with blank cells', () => {
    const start = 'A1'
    const finish = 'BLANKROW'
    const result = getFinishingCell(start, finish, arrayOfObjects)
    expect(result).toBe('B6')
  })
})
