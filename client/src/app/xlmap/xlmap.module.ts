import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { ClarityModule } from '@clr/angular'
import { HotTableModule } from '@handsontable/angular'
import { registerAllModules } from 'handsontable/registry'
import { AppSharedModule } from '../app-shared.module'
import { DirectivesModule } from '../directives/directives.module'
import { XLMapRouteComponent } from '../routes/xlmap-route/xlmap-route.component'
import { DcTreeModule } from '../shared/dc-tree/dc-tree.module'
import { XLMapRoutingModule } from './xlmap-routing.module'
import { XLMapComponent } from './xlmap.component'

// register Handsontable's modules
registerAllModules()

@NgModule({
  declarations: [XLMapRouteComponent, XLMapComponent],
  imports: [
    HotTableModule,
    XLMapRoutingModule,
    FormsModule,
    ClarityModule,
    AppSharedModule,
    CommonModule,
    DcTreeModule,
    DirectivesModule
  ],
  exports: [XLMapComponent]
})
export class XLMapModule {}
