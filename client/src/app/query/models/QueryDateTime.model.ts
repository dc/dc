export interface QueryDateTime {
  clauseIndex: number
  queryIndex: number
  date: string
  time: string
}
