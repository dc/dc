import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ClarityModule } from '@clr/angular'
import { FormsModule } from '@angular/forms'
import { QueryComponent } from './query.component'
import { AppSharedModule } from '../app-shared.module'
import { PipesModule } from '../pipes/pipes.module'
import { DirectivesModule } from '../directives/directives.module'

@NgModule({
  declarations: [QueryComponent],
  imports: [
    CommonModule,
    ClarityModule,
    CommonModule,
    FormsModule,
    AppSharedModule,
    PipesModule,
    DirectivesModule
  ],
  exports: [QueryComponent]
})
export class QueryModule {}
