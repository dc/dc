export interface SpecObj {
  [propName: string]: {
    colType: string
    colLength: any
  }
}
