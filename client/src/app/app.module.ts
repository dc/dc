import { HttpClientModule } from '@angular/common/http'
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { ClarityModule } from '@clr/angular'

import { AppComponent } from './app.component'
import { ROUTING } from './app.routing'
import { NotFoundComponent } from './not-found/not-found.component'

import { SasStoreService } from './services/sas-store.service'
import { SharedModule } from './shared/shared.module'

import { AppSharedModule } from './app-shared.module'
import { PipesModule } from './pipes/pipes.module'
import { ReviewRouteComponent } from './routes/review-route/review-route.component'
import { LicensingGuard } from './routes/licensing.guard'
import { UsernavRouteComponent } from './routes/usernav-route/usernav-route.component'
import { AppService } from './services/app.service'
import { InfoModalComponent } from './shared/abort-modal/info-modal.component'
import { RequestsModalComponent } from './shared/requests-modal/requests-modal.component'
import { DirectivesModule } from './directives/directives.module'
import { ViyaApiExplorerComponent } from './viya-api-explorer/viya-api-explorer.component'
import { NgxJsonViewerModule } from 'ngx-json-viewer'
import { AppSettingsService } from './services/app-settings.service'

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    ReviewRouteComponent,
    ReviewRouteComponent,
    UsernavRouteComponent,
    RequestsModalComponent,
    InfoModalComponent,
    ViyaApiExplorerComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ROUTING,
    SharedModule,
    ClarityModule,
    AppSharedModule,
    PipesModule,
    DirectivesModule,
    NgxJsonViewerModule
  ],
  providers: [AppService, SasStoreService, LicensingGuard, AppSettingsService],
  bootstrap: [AppComponent]
})
export class AppModule {}
