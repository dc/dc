import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import { MultiDatasetRouteComponent } from '../routes/multi-dataset-route/multi-dataset-route.component'
import { MultiDatasetComponent } from './multi-dataset.component'

const routes: Routes = [
  {
    path: '',
    component: MultiDatasetRouteComponent,
    children: [{ path: '', component: MultiDatasetComponent }]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MultiDatasetRoutingModule {}
