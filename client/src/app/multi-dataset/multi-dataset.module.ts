import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { ClarityModule } from '@clr/angular'
import { HotTableModule } from '@handsontable/angular'
import { registerAllModules } from 'handsontable/registry'
import { AppSharedModule } from '../app-shared.module'
import { DirectivesModule } from '../directives/directives.module'
import { DcTreeModule } from '../shared/dc-tree/dc-tree.module'
import { MultiDatasetComponent } from './multi-dataset.component'
import { MultiDatasetRoutingModule } from './multi-dataset-routing.module'
import { MultiDatasetRouteComponent } from '../routes/multi-dataset-route/multi-dataset-route.component'

// register Handsontable's modules
registerAllModules()

@NgModule({
  declarations: [MultiDatasetRouteComponent, MultiDatasetComponent],
  imports: [
    HotTableModule,
    MultiDatasetRoutingModule,
    FormsModule,
    ClarityModule,
    AppSharedModule,
    CommonModule,
    DcTreeModule,
    DirectivesModule
  ],
  exports: [MultiDatasetComponent]
})
export class MultiDatasetModule {}
