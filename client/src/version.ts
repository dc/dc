const { readFileSync } = require('fs')
const { gitDescribeSync } = require('git-describe')
const { resolve, relative } = require('path')
const { writeFileSync } = require('fs-extra')
const momentObj = require('moment')

const gitInfo = gitDescribeSync({
  dirtyMark: false,
  dirtySemver: false
})

//Reading the installed adapter version
const adapterPackagePath = resolve(
  __dirname,
  '..',
  'node_modules',
  '@sasjs',
  'adapter',
  'package.json'
)
const adapterPackageFile = readFileSync(adapterPackagePath)
const adapterPackageFileJson = JSON.parse(adapterPackageFile)

gitInfo.timestamp = momentObj().format('x')
gitInfo.adapterVersion = adapterPackageFileJson.version

const file = resolve(__dirname, '..', 'src', 'environments', 'version.ts')
writeFileSync(
  file,
  `// IMPORTANT: THIS FILE IS AUTO GENERATED! DO NOT MANUALLY EDIT OR CHECKIN!
/* tslint:disable */
export const VERSION = ${JSON.stringify(gitInfo, null, 4)};
/* tslint:enable */
`,
  { encoding: 'utf-8' }
)

console.log(
  `Wrote version info ${gitInfo.raw} to ${relative(
    resolve(__dirname, '..'),
    file
  )}`
)
