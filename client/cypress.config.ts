import { defineConfig } from "cypress";

export default defineConfig({
  reporter: "mochawesome",

  reporterOptions: {
    reportDir: "cypress/results",
    overwrite: false,
    html: true,
    json: false,
  },
  viewportHeight: 900,
  viewportWidth: 1600,

  chromeWebSecurity: false,
  defaultCommandTimeout: 30000,

  env: {
    hosturl: "http://localhost:4200",
    appLocation: "",
    site_id_SAS9: "70221618",
    site_id_SASVIYA: "70253615",
    site_id_SASJS: "123",
    serverType: "SASJS",
    libraryToOpenIncludes_SASVIYA: "viya",
    libraryToOpenIncludes_SAS9: "dc",
    libraryToOpenIncludes_SASJS: "dc",
    debug: false,
    screenshotOnRunFailure: false,
    longerCommandTimeout: 50000,
    testLicenceUserLimits: false,
  },

  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
