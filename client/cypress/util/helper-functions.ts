export const base64ToArrayBuffer = (base64: string) => {
  return new Promise(async (resolve, reject) => {
    const dataUrl = "data:application/octet-binary;base64," + base64;
  
    fetch(dataUrl)
      .then(res => res.arrayBuffer())
      .then(buffer => {
        resolve(new Uint8Array(buffer))
      }).catch((err) => {
        reject(err)
      })
  })
}

export const arrayBufferToBase64 = (arrayBuffer: any) => {
  return new Promise((resolve, reject) => {
    const blob = new Blob([arrayBuffer])

    const reader = new FileReader();

    reader.onload = async function(event){
      if (event.target) {
        var base64: any = event.target.result
        base64 = base64.substring(37, base64.length)

        resolve(base64)
      }
    };

    reader.readAsDataURL(blob);
  })
}