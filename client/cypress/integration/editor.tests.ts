const username = Cypress.env('username')
const password = Cypress.env('password')
const hostUrl = Cypress.env('hosturl')
const appLocation = Cypress.env('appLocation')
const longerCommandTimeout = Cypress.env('longerCommandTimeout')
const serverType = Cypress.env('serverType')
const libraryToOpenIncludes = Cypress.env(`libraryToOpenIncludes_${serverType}`)
const fixturePath = 'excels_general/'

context('editor tests: ', function () {
  this.beforeAll(() => {
    cy.visit(`${hostUrl}/SASLogon/logout`)
    cy.loginAndUpdateValidKey()
  })

  this.beforeEach(() => {
    cy.visit(hostUrl + appLocation)

    visitPage('home')
  })

  it('1 | Submits duplicate primary keys', (done) => {
    openTableFromTree(libraryToOpenIncludes, 'mpe_datadictionary')

    attachExcelFile('MPE_DATADICTIONARY_duplicate_keys.xlsx', () => {
      clickOnUploadPreview(() => {
        confirmEditPreviewFile(() => {
          submitTable(() => {
            cy.get('.modal-body').then((modalBody: any) => {
              if (modalBody[0].innerText.includes(`Duplicates found:`)) {
                done()
              }
            })
          })
        })
      })
    })
  })

  it('2 | Submits null cells which must not be null', (done) => {
    openTableFromTree(libraryToOpenIncludes, 'mpe_x_test')

    clickOnEdit(() => {
      cy.get('.btn.btn-sm.btn-icon.btn-outline-danger', {
        timeout: longerCommandTimeout
      }).then(() => {
        cy.get('.ht_master tbody tr').then((rows: any) => {
          cy.get(rows[1].childNodes[2])
            .dblclick({ force: true })
            .then(() => {
              cy.focused()
                .clear()
                .type('{enter}')
                .then(() => {
                  submitTable(() => {
                    cy.get('.modal-body').then((modalBody: any) => {
                      if (
                        modalBody[0].innerHTML
                          .toLowerCase()
                          .includes(`invalid values are present`)
                      ) {
                        done()
                      }
                    })
                  })
                })
            })
        })
      })
    })
  })

  it('3 | Gets basic dynamic cell validation', () => {
    openTableFromTree(libraryToOpenIncludes, 'mpe_x_test')

    clickOnEdit(() => {
      cy.get('.btn.btn-sm.btn-icon.btn-outline-danger', {
        timeout: longerCommandTimeout
      }).then(() => {
        cy.get('.ht_master tbody tr').then((rows: any) => {
          cy.get(rows[1].childNodes[5])
            .click({ force: true })
            .then(($td) => {
              cy.get('.htAutocompleteArrow', { withinSubject: $td }).should(
                'exist'
              )
            })
        })
      })
    })
  })

  it('4 | Gets advanced dynamic cell validation', () => {
    openTableFromTree(libraryToOpenIncludes, 'mpe_tables')

    clickOnEdit(() => {
      cy.get('.btn.btn-sm.btn-icon.btn-outline-danger', {
        timeout: longerCommandTimeout
      }).then(() => {
        cy.get('.ht_master tbody tr').then((rows: any) => {
          cy.get(rows[1].childNodes[3])
            .click({ force: true })
            .then(($td) => {
              cy.get('.htAutocompleteArrow', { withinSubject: $td }).should(
                'exist'
              )
              cy.get('.htAutocompleteArrow', {
                withinSubject: rows[1].childNodes[7]
              }).should('exist')
              cy.get('.htAutocompleteArrow', {
                withinSubject: rows[1].childNodes[8]
              }).should('exist')
            })
        })
      })
    })
  })
})

const clickOnEdit = (callback?: any) => {
  cy.get('.btnCtrl button.btn-primary', { timeout: longerCommandTimeout })
    .click()
    .then(() => {
      if (callback) callback()
    })
}

const openTableFromTree = (libNameIncludes: string, tablename: string) => {
  cy.get('.app-loading', { timeout: longerCommandTimeout })
    .should('not.exist')
    .then(() => {
      cy.get('.nav-tree clr-tree > clr-tree-node', {
        timeout: longerCommandTimeout
      }).then((treeNodes: any) => {
        let viyaLib

        for (let node of treeNodes) {
          if (node.innerText.toLowerCase().includes(libNameIncludes)) {
            viyaLib = node
            break
          }
        }

        cy.get(viyaLib).within(() => {
          cy.get('.clr-tree-node-content-container > button').click()

          cy.get('.clr-treenode-link').then((innerNodes: any) => {
            for (let innerNode of innerNodes) {
              if (innerNode.innerText.toLowerCase().includes(tablename)) {
                innerNode.click()
                break
              }
            }
          })
        })
      })
    })
}

const attachExcelFile = (excelFilename: string, callback?: any) => {
  cy.get('.buttonBar button:last-child')
    .click()
    .then(() => {
      cy.get('input[type="file"]#file-upload')
        .attachFile(`/${fixturePath}/${excelFilename}`)
        .then(() => {
          cy.get('.modal-footer .btn.btn-primary').then((modalBtn) => {
            modalBtn.click()
            if (callback) callback()
          })
        })
    })
}

const clickOnUploadPreview = (callback?: any) => {
  cy.get('.buttonBar button.btn-primary.btn-upload-preview')
    .click()
    .then(() => {
      if (callback) callback()
    })
}

const confirmEditPreviewFile = (callback?: any) => {
  cy.get('.modal-footer button.btn-success-outline')
    .click()
    .then(() => {
      if (callback) callback()
    })
}

const submitTable = (callback?: any) => {
  cy.get('.btnCtrl button.btn-primary')
    .click()
    .then(() => {
      if (callback) callback()
    })
}

const submitTableMessage = (callback?: any) => {
  cy.get('.modal-footer .btn.btn-sm.btn-success-outline')
    .click()
    .then(() => {
      if (callback) callback()
    })
}

const submitExcel = (callback?: any) => {
  cy.get('.buttonBar button.preview-submit')
    .click()
    .then(() => {
      if (callback) callback()
    })
}

const rejectExcel = (callback?: any) => {
  cy.get('button', { timeout: longerCommandTimeout })
    .should('contain', 'Approve')
    .then((allButtons: any) => {
      for (let approvalButton of allButtons) {
        if (approvalButton.innerText.toLowerCase().includes('approve')) {
          approvalButton.click()
          break
        }
      }

      cy.get('button.btn-danger')
        .should('exist')
        .should('not.be.disabled')
        .click()
        .then(() => {
          cy.get('.modal-footer button.btn-success-outline')
            .click()
            .then(() => {
              cy.get('app-history')
                .should('exist')
                .then(() => {
                  if (callback) callback()
                })
            })
        })
    })
}

const visitPage = (url: string) => {
  cy.visit(`${hostUrl}${appLocation}/#/${url}`)
}
