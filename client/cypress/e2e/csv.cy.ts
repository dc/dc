import { Callbacks } from 'cypress/types/jquery/index'

const username = Cypress.env('username')
const password = Cypress.env('password')
const hostUrl = Cypress.env('hosturl')
const appLocation = Cypress.env('appLocation')
const longerCommandTimeout = Cypress.env('longerCommandTimeout')
const serverType = Cypress.env('serverType')
const libraryToOpenIncludes = Cypress.env(`libraryToOpenIncludes_${serverType}`)
const fixturePath = 'csvs/'

context('excel tests: ', function () {
  this.beforeAll(() => {
    cy.visit(`${hostUrl}/SASLogon/logout`)
    cy.loginAndUpdateValidKey(true)
  })

  this.beforeEach(() => {
    cy.visit(hostUrl + appLocation)

    visitPage('home')

    colorLog(
      `TEST START ---> ${
        Cypress.mocha.getRunner().suite.ctx.currentTest.title
      }`,
      '#3498DB'
    )
  })

  it('1 | Uploads regular csv file', () => {
    openTableFromTree(libraryToOpenIncludes, 'mpe_x_test')

    attachExcelFile('regular.csv', () => {
      cy.get('#approval-btn', { timeout: 60000 })
        .should('be.visible')
        // .then(() => {
        //   cy.get('#hotInstance', { timeout: 30000 })
        //     .find('div.ht_master.handsontable')
        //     .find('div.wtHolder')
        //     .find('div.wtHider')
        //     .find('div.wtSpreader')
        //     .find('table.htCore')
        //     .find('tbody')
        //     .then((data) => {
        //       let cell: any = data[0].children[0].children[1]
        //       expect(cell.innerText).to.equal('0')
        //       cell = data[0].children[0].children[2]
        //       expect(cell.innerText).to.equal('44')
        //       cell = data[0].children[0].children[3]
        //       expect(cell.innerText).to.equal('abc')
        //       cell = data[0].children[0].children[6]
        //       expect(cell.innerText).to.equal('Option abc')
        //     })
        // })
    })
  })

  this.afterEach(() => {
    colorLog(`TEST END -------------`, '#3498DB')
  })
})

const openTableFromTree = (libNameIncludes: string, tablename: string) => {
  cy.get('.app-loading', { timeout: longerCommandTimeout })
    .should('not.exist')
    .then(() => {
      cy.get('.nav-tree clr-tree > clr-tree-node', {
        timeout: longerCommandTimeout
      }).then((treeNodes: any) => {
        let viyaLib

        for (let node of treeNodes) {
          if (node.innerText.toLowerCase().includes(libNameIncludes)) {
            viyaLib = node
            break
          }
        }

        cy.get(viyaLib).within(() => {
          cy.get('.clr-tree-node-content-container > button').click()

          cy.get('.clr-treenode-link').then((innerNodes: any) => {
            for (let innerNode of innerNodes) {
              if (innerNode.innerText.toLowerCase().includes(tablename)) {
                innerNode.click()
                break
              }
            }
          })
        })
      })
    })
}

const attachExcelFile = (excelFilename: string, callback?: any) => {
  cy.get('.buttonBar button:last-child')
    .should('exist')
    .click()
    .then(() => {
      cy.get('input[type="file"]#file-upload')
        .attachFile(`/${fixturePath}/${excelFilename}`)
        .then(() => {
          if (callback) callback()
        })
    })
}

const visitPage = (url: string) => {
  cy.visit(`${hostUrl}${appLocation}/#/${url}`)
}

const colorLog = (msg: string, color: string) => {
  console.log('%c' + msg, 'color:' + color + ';font-weight:bold;')
}
