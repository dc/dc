/**
  @file
  @brief test setup

  <h4> SAS Macros </h4>
  @li dc_getsettings.sas
  @li mf_getapploc.sas
  @li mf_getplatform.sas
  @li mf_getuniquefileref.sas
  @li mf_mkdir.sas
  @li mm_createstp.sas
  @li mm_createlibrary.sas
  @li mm_deletelibrary.sas
  @li mm_deletestp.sas
  @li mm_getstpcode.sas
  @li mp_assert.sas
  @li mp_coretable.sas
  @li mp_init.sas
  @li mp_testservice.sas
  @li ms_createfile.sas
  @li ms_deletefile.sas
  @li mx_getcode.sas


**/

%mp_init()

%global apploc _program;

%let defaultcontext=Datacontroller compute context;

data _null_;
  length _pgm $1000;
  _pgm=symget('_program');
  if _pgm='' then do;
    if "&SYSPROCESSMODE" = 'SAS Workspace Server'
    then apploc=
      '/30.SASApps/3030.Projects/303001.DataController/build2/DataController';
    else apploc='/Public/app/viya';
    call symputx('apploc',apploc);
    call symputx('_program',cats(apploc,'/tests/services/bottom'));
  end;
  else do;
    cnt=find(_pgm,'/tests/');
    if cnt=0 then cnt=find(_pgm,'/services/');
    if cnt=0 then cnt=find(_pgm,'/jobs/');
    apploc=substr(_pgm,1,cnt-1);
    put cnt= apploc=;
    call symputx('apploc',apploc);
  end;
run;

%dc_getsettings()


/**
  * Set up Test library
  */
%let testloc=%sysfunc(pathname(&DC_LIBREF))/fmt%mf_getuniquefileref();
%mf_mkdir(&testloc)
libname dctest "&testloc";

/* add formats */
PROC FORMAT library=dctest.dcfmts;
  picture MyMSdt other='%0Y-%0m-%0dT%0H:%0M:%0S' (datatype=datetime);
RUN;
data work.fmts;
  length fmtname $32;
  do fmtname='SMAUG','MORDOR','GOLLUM';
    do start=1 to 10;
      label= cats('Dragon ',start);
      output;
    end;
  end;
run;
proc sort data=work.fmts nodupkey;
  by fmtname;
run;
proc format cntlin=work.fmts library=dctest.dcfmts;
run;
proc format library=dctest.dcfmts;
  invalue indays (default=13) other=42;
run;
proc sql;
delete from &DC_LIBREF..mpe_tables
  where libref="DCTEST" and dsn='DCFMTS-FC';
data work.append;
  if 0 then set &DC_LIBREF..mpe_tables;
  tx_from=0;
  tx_to='31DEC9999:23:59:59'dt;
  libref="DCTEST";
  dsn='DCFMTS-FC';
  buskey='TYPE FMTNAME FMTROW';
  loadtype='FORMAT_CAT';
  num_of_approvals_required=1;
  output;
run;
proc append base=&dc_libref..mpe_tables data=work.append;
run;

/* add some other tables */
%mp_coretable(LOCKTABLE,libds=dctest.locktable)
%mp_coretable(DIFFTABLE,libds=dctest.difftable)


/**
  * Update settings service
  */
%let root=%mf_getapploc(&_program);
%put &=root;
filename setngs temp;
options mprint;

%macro update_settings();

%if "&_debug"="2477" or "&_debug"="fields,log,trace" %then %do;
  %put debug mode activated;
  options mprint;
  %put _global_;
%end;

%let pf=%mf_getplatform();
%if &pf=SASVIYA %then %do;
  filename jobout filesrvc folderpath="&root/services";
  data _null_;
    file setngs;
    infile jobout('settings.sas') end=last;
    input;
    put _infile_;
    if last then do;
      put "libname dctest '&testloc';";
    end;
  run;
  data _null_;
    file jobout('settings.sas');
    infile setngs;
    input;
    put _infile_;
    putlog _infile_;
  run;
%end;
%else %if &pf=SASJS %then %do;
  %mx_getcode(&root/services/public/settings,outref=stngs)

  filename sendback "&sasjswork/settings2.txt" lrecl=1000;
  data _null_;
    file sendback;
    infile stngs end=last lrecl=1000;
    input;
    put _infile_;
    if last then do;
      put "libname dctest '&testloc';";
    end;
  run;
  %ms_deletefile(&root/services/public/settings.sas)

  %ms_createfile(&root/services/public/settings.sas, inref=sendback,mdebug=1)
%end;
%else %do;
  %let libname=DC Test;
  %mm_deletelibrary(name=&libname)
  %mm_createlibrary(
    libname=&libname
    ,libref=dctest
    ,libdesc=Testing the DC
    ,engine=BASE
    ,tree=/30.SASApps/3030.Projects/303001.DataController
    ,servercontext=SASApp
    ,directory=&testloc
    ,mDebug=1
  )

  %mm_getstpcode(tree=&root/services/public
    ,name=Data_Controller_Settings
    ,outloc=&sasjswork/settings.txt
  )
  data _null_;
    file "&sasjswork/settings2.txt";
    infile "&sasjswork/settings.txt" end=last;
    input;
    put _infile_;
    if last then do;
      put "libname dctest '&testloc';";
    end;
  run;
  %mm_deletestp(target=&root/services/public/Data_Controller_Settings)
  %mm_createstp(stpname=Data_Controller_Settings
    ,filename=settings2.txt
    ,directory=&sasjswork
    ,tree=&root/services/public
    ,Server=SASApp
    ,stptype=2
    ,mdebug=1
    ,stpdesc=Data Controller Configuration
    ,minify=NO
  )
%end;
%mend update_settings;

%update_settings()

%mp_assert(
  iftrue=(&syscc=0),
  desc=Makedata ran without errors,
  outds=work.test_results
)

