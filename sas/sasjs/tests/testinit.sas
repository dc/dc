/**
  @file
  @brief testinit.sas

  @details for SAS 9 you can run:

  <h4> SAS Macros </h4>
  @li dc_getsettings.sas
  @li mf_getplatform.sas
  @li mpeinit2.sas
  @li mp_abort.sas
  @li mp_init.sas
  @li mp_testservice.sas


REMOVE THAT LAST MACRO
**/

%mp_init()

%let syscc=0;
%global apploc _program dclib defaultcontext _debug sasjs_mdebug dc_dttmtfmt;

%let defaultcontext=Datacontroller compute context;
%let sasjs_mdebug=0;

options mprint mprintnest nobomfile lrecl=32767;

%mpeinit2()

data _null_;
  length _pgm $1000;
  _pgm=symget('_program');
  if _pgm='' then do;
    if "&SYSPROCESSMODE" = 'SAS Workspace Server'
    then apploc=
      '/30.SASApps/3030.Projects/303001.DataController/build2/DataController';
    else apploc='/Public/app/viya';
    call symputx('apploc',apploc);
    call symputx('_program',cats(apploc,'/tests/services/bottom'));
  end;
  else do;
    cnt=find(_pgm,'/tests/');
    if cnt=0 then cnt=find(_pgm,'/services/');
    if cnt=0 then cnt=find(_pgm,'/jobs/');
    put cnt= apploc= _pgm=;
    apploc=substr(_pgm,1,cnt-1);
    call symputx('apploc',apploc);
  end;
run;

%macro conditional();
%if %scan(&_program,-1,/) ne testsetup %then %do;
  %dc_getsettings()
  /* this should be tidied up */
  %global dclib mpelib dc_libref;
  %let dclib=&dc_libref;
  %let mpelib=&dc_libref;
  %put &=dclib &=mpelib;
%end;

%if "&_debug"="2477" or "&_debug"="fields,log,trace" or "&_debug"="131"
%then %do;
  %let sasjs_mdebug=1;
%end;

%mend conditional;

%conditional()



