/**
  @file getcols.sas
  @brief Retrieves column info to enable population of dropdowns
  @details

  <h4> SAS Macros </h4>
  @li dc_assignlib.sas
  @li mf_getvalue.sas
  @li mp_abort.sas

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/
%mpeinit()


%let ds=%mf_getvalue(work.iwant,libds);
%dc_assignlib(READ,%scan(&ds,1,.))

proc contents noprint data=&ds
  out=droplist1 (keep=name type length label varnum format:);
run;
data cols(keep=name type length varnum format label);
  set droplist1(rename=(format=format2 type=type2));
  name=upcase(name);
  if type2=2 then do;
    length format $49.;
    if format2='' then format=cats('$',length,'.');
    else if formatl=0 then format=cats(format2,'.');
    else format=cats(format2,formatl,'.');
    type='C';
    ddtype='CHARACTER';
  end;
  else do;
    if format2='' then format=cats(length,'.');
    else if formatl=0 then format=cats(format2,'.');
    else if formatd=0 then format=cats(format2,formatl,'.');
    else format=cats(format2,formatl,'.',formatd);
    type='N';
    if format=:'DATETIME' then ddtype='DATETIME';
    else if format=:'DATE' then ddtype='DATE';
    else if format=:'TIME' then ddtype='TIME';
    else ddtype='NUMERIC';
  end;
  if label='' then label=name;
run;

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program..sas
  ,msg=%str(syscc=&syscc)
)

%webout(OPEN)
%webout(OBJ,cols)
%webout(CLOSE)


%mpeterm()
