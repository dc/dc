/**
  @file registeruser.sas
  @brief Registers a new user in Data Controller
  @details New users are logged after accepting EULA terms.

  <h4> SAS Macros </h4>
  @li mf_getuser.sas
  @li mp_abort.sas
  @li mpeinit.sas

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%mpeinit()

%let userid=%mf_getuser();

/* confirm the user is not registered */
%let isRegistered=0;
proc sql noprint;
select count(*) into: isregistered
  from &mpelib..mpe_users
  where user_id="&userid";

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program..sas
  ,msg=%str(Problem accessing &mpelib..mpe_users table)
)
%mp_abort(iftrue= (&isregistered > 0)
  ,mac=&_program..sas
  ,msg=%str(User &userid is already registered on &mpelib..mpe_users!)
)

data work.append;
  if 0 then set &mpelib..mpe_users;
  user_id=symget('userid');
  registered_dt=today();
  last_seen_dt=today();
run;

proc append base=&mpelib..mpe_users data=work.append;

%let isRegistered=0;
proc sql noprint;
select count(*) into: isregistered
  from &mpelib..mpe_users
  where user_id="&userid";

%mp_abort(iftrue= (&syscc ne 0 or &isregistered ne 1)
  ,mac=&_program..sas
  ,msg=%str(Problem appending to &mpelib..mpe_users table)
)

data work.return;
  msg='SUCCESS';
run;

%webout(OPEN)
%webout(OBJ,return)
%webout(CLOSE)
