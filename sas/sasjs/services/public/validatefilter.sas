/**
  @file
  @brief Validates a filter clause before it gets hashified, returns the RK
  @details Used to generate a FILTER_RK from an input query dataset.
  Raw values are stored in dc.mpe_filtersource and the meta values are stored
  in dc.mpe_filteranytable

  <h4> Service Inputs </h4>
  <h5> IWANT </h5>
  |FILTER_TABLE:$41.|
  |---|
  |DC258467.MPE_X_TEST|

  <h5> FILTERQUERY </h5>
  |GROUP_LOGIC:$3|SUBGROUP_LOGIC:$3|SUBGROUP_ID:8.|VARIABLE_NM:$32|OPERATOR_NM:$10|RAW_VALUE:$32767|
  |---|---|---|---|---|---|
  |AND|AND|1|SOME_BESTNUM|>|1|
  |AND|AND|1|SOME_TIME|=|77333|

  <h4> Service Outputs </h4>

  <h5> result </h5>
  @li FILTER_HASH
  @li FILTER_RK
  @li FILTER_TABLE

  <h4> SAS Macros </h4>
  @li dc_assignlib.sas
  @li mf_getvalue.sas
  @li mp_filterstore.sas
  @li removecolsfromwork.sas

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/

%mpeinit()

%let ds=%upcase(%mf_getvalue(work.iwant,filter_table));
%dc_assignlib(WRITE,%scan(&ds,1,.))

%mp_filterstore(
  libds=&ds,
  queryds=work.filterquery,
  filter_summary=&dc_libref..mpe_filteranytable,
  filter_detail=&dc_libref..mpe_filtersource,
  lock_table=&dc_libref..mpe_lockanytable,
  maxkeytable=&dc_libref..mpe_maxkeyvalues,
  outresult=work.result,
  outquery=work.query,  /* not used */
  mdebug=1
)

%removecolsfromwork(___TMP___MD5)

proc sql;
alter table work.result drop PROCESSED_DTTM;

%webout(OPEN)
%webout(OBJ,result)
%webout(CLOSE)

%mpeterm()
