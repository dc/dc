/**
  @file getversion.sas
  @brief get a specific (previous) version of a particular table
  @details Used to fetch a version of a table as at a previous point in time
  Delivered as part of this issue: https://git.datacontroller.io/dc/dc/issues/84

  <h4> Service Inputs </h4>
  <h5> getversion_input </h5>

  |LIBREF:$char8.|DS:$char32.|TS: 8.|
  |---|---|---|
  |SOMELIB|SOMEDS|1341344.804|

  <h4> Service Outputs </h4>
  <h5> work.getversion_output </h5>

  The data for a particular version

  <h4> SAS Macros </h4>
  @li mf_getuser.sas
  @li mpeinit.sas
  @li mpe_getvars.sas

  @version 9.2
  @author 4GL Apps Ltd

**/

%mpeinit()

%global LIBREF DS;

/* load parameters */
%mpe_getvars(getversion_input, getversion_input)

%mp_abort(iftrue= (&syscc ne 0 )
  ,mac=&_program
  ,msg=%str(Issue on startup)
)

/* todo */

%webout(OPEN)
%webout(OBJ,getversion_output)
%webout(CLOSE)


%mpeterm()
