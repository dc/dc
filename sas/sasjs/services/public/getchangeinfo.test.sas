/**
  @file
  @brief testing getchangeinfo service

  <h4> SAS Macros </h4>
  @li mp_assertcolvals.sas
  @li mf_getuniquefileref.sas

**/

%let _program=&appLoc/services/public/getchangeinfo;

/**
  * First part - stage some data (for diffing)
  */
data work.sascontroltable;
  action='LOAD';
  message="getdiffs prep";
  libds="&dclib..MPE_X_TEST";
  output;
  stop;
run;
proc sql noprint;
select max(primary_key_field) into: maxpk from &dclib..mpe_x_test;
data work.jsdata;
  set &dclib..mpe_x_test(rename=(
    some_date=dt2 SOME_DATETIME=dttm2 SOME_TIME=tm2)
  );
  /* for now, the adapter sends these as strings */
  some_date=put(dt2,date9.);
  SOME_DATETIME=put(dttm2,datetime19.);
  some_time=put(tm2,time.);
  drop dt2 dttm2 tm2;
  _____DELETE__THIS__RECORD_____='No';
  if _n_=1 then do;
    primary_key_field=sum(&maxpk,1);
    some_char='   leadingblanks';
    some_num=._;
    output;
  end;
  else if _n_<3 then do;
    SOME_NUM=ranuni(0);
  end;
  else stop;
run;

%mx_testservice(&appLoc/services/editors/stagedata,
  viyacontext=&defaultcontext,
  inputdatasets=work.jsdata work.sascontroltable,
  outlib=web1,
  mdebug=&sasjs_mdebug
)

%let status=0;
data work.sasparams;
  set web1.sasparams;
  putlog (_all_)(=);
  if status='SUCCESS' then call symputx('status',1);
  call symputx('dsid',dsid);
run;
%mp_assert(
  iftrue=(&status=1 and &syscc=0),
  desc=Checking successful submission
)


/* now call getchangeinfo */
%let f3=%mf_getuniquefileref();
data _null_;
  file &f3 termstr=crlf;
  put 'TABLE:$43.';
  put "&dsid";
run;
%mp_testservice(&_program,
  viyacontext=&defaultcontext,
  inputfiles=&f3:sascontroltable,
  outlib=web3,
  mdebug=&sasjs_mdebug
)

data work.jsparams;
  set web3.jsparams;
  putlog (_all_)(=);
  call symputx('ALLOW_RESTORE',ALLOW_RESTORE);
run;
%mp_assert(
  iftrue=(&syscc=0),
  desc=Checking successful execution
)
%mp_assert(
  iftrue=(%mf_nobs(work.jsparams)=1),
  desc=Checking data was returned
)
%mp_assert(
  iftrue=(&ALLOW_RESTORE=NO),
  desc=Checking admin user cannot restore - as table was not approved
)
