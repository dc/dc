/**
  @file
  @brief testing getddl service

  <h4> SAS Macros </h4>
  @li mp_assertdsobs.sas
  @li mx_testservice.sas

**/


%let _program=&appLoc/services/public/getddl;

/* test 1 */
data work.params;
  length name $32 value $1000;
  name='libref';value="&dclib";output;
  name='ds';value='';output;
  name='flavour';value='TSQL';output;
run;
%mx_testservice(&_program,
  viyacontext=&defaultcontext,
  inputparams=work.params,
  outref=webout,
  viyaresult=WEBOUT_TXT,
  mdebug=&sasjs_mdebug
)

data work.results;
  infile webout;
  input;
  putlog _infile_;
  if _n_=2 then do;
    if _infile_=:'/* TSQL Flavour DDL for' then do;
      putlog 'test passed';
      output;
      output;
    end;
    stop;
  end;
  else if _n_>2 then stop;
run;

%mp_assertdsobs(work.results,
  desc=DDL file is successfully returned,
  test=EQUALS 2,
  outds=work.test_results
)

/* test 2 - format catalog */
data work.params;
  length name $32 value $1000;
  name='libref';value="DCTEST";output;
  name='ds';value='DCFMTS-FC';output;
run;
%mx_testservice(&_program,
  viyacontext=&defaultcontext,
  inputparams=work.params,
  outref=web2,
  viyaresult=WEBOUT_TXT,
  mdebug=&sasjs_mdebug
)

data work.results2;
  infile web2;
  input;
  putlog _infile_;
  if index(upcase(_infile_),'WORK.FMTEXTRACT') then do;
    putlog 'test passed';
    output;
    output;
    stop;
  end;
  else if _n_>50 then stop;
run;

%mp_assertdsobs(work.results2,
  desc=DDL file is successfully returned from format catalog,
  test=EQUALS 2,
  outds=work.test_results
)
