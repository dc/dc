/**
  @file getchangeinfo.sas
  @brief Returns the details for an approval diff
  @details

  <h4> SAS Macros </h4>
  @li mf_getengine.sas
  @li mp_abort.sas
  @li mpe_checkrestore.sas

  <h4> Service Inputs </h4>
  <h5> sascontroltable </h5>
  @li table table ID or LOAD_REF used to uniquely identify a staged change

  <h4> Service Outputs </h4>

  <h5> work.jsparams </h5>
  Mainly sourced from MPE_SUBMIT plus some extra cols:

  @li LIB_ENGINE Library engine
  @li allow_restore YES if a user can restore, else NO
  @li REASON reason why a restore is / is no possible

  <h4> Data Inputs </h4>
  @li MPE_AUDIT
  @li MPE_COLUMN_LEVEL_SECURITY
  @li MPE_ROW_LEVEL_SECURITY
  @li MPE_SUBMIT

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/

%mpeinit()

%let table=;
data _null_;
  set SASControlTable;
  call symputx('table',table);
run;

data APPROVE1;
  set &mpelib..mpe_submit
    (rename=(SUBMITTED_ON_DTTM=submitted_on REVIEWED_ON_DTTM=REVIEWED_ON));
  where TABLE_ID="&TABLE";
  TABLE_NM=cats(base_lib,'.',base_ds);
  BASE_TABLE=table_nm;
  call symputx('base_lib',base_lib);
  REVIEWED_ON_DTTM=put(reviewed_on,datetime19.);
  SUBMITTED_ON_DTTM=put(submitted_on,datetime19.);
run;

/**
  * Check if user has basic access permission to RESTORE the table
  */
%put checking access;
%global allow_restore reason;
%mpe_checkrestore(&table,outresult=ALLOW_RESTORE,outreason=REASON)

data work.jsParams;
  set approve1;
  LIB_ENGINE="%mf_getEngine(&base_lib)";
  allow_restore="&allow_restore";
  REASON="&reason";
run;

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program..sas
  ,msg=%str(syscc=&syscc)
)

%webout(OPEN)
%webout(OBJ,jsParams,missing=STRING)
%webout(CLOSE)


%mpeterm()
