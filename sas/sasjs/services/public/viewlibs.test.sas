/**
  @file
  @brief testing public/viewlibs service

  <h4> SAS Macros </h4>
  @li mp_assertdsobs.sas
  @li mp_testservice.sas

  <h4> Related Programs </h4>
  @li viewlibs.sas

**/

%let _program=&appLoc/services/public/viewlibs;

data work.sascontroltable;
  mplib="&dclib";
run;

%mp_testservice(&_program,
  viyacontext=&defaultcontext,
  outlib=web1
)
data work.saslibs;
  set web1.saslibs;
run;

%mp_assertdsobs(work.saslibs,
  desc=saslibs contains rows,
  test=ATLEAST 2
)




