/**
  @file
  @brief testing public/viewtables service

  <h4> SAS Macros </h4>
  @li dc_refreshcatalog.sas
  @li mf_getengine.sas
  @li mp_assert.sas
  @li mp_assertdsobs.sas
  @li mx_testservice.sas

  <h4> Related Programs </h4>
  @li viewtables.sas

**/

%let _program=&appLoc/services/public/viewtables;

/* ensure catalog is up to date */
%dc_refreshcatalog(DCTEST)

data work.sascontroltable;
  mplib="DCTEST";
run;

%mx_testservice(&_program,
  viyacontext=&defaultcontext,
  inputdatasets=work.sascontroltable,
  outlib=web1,
  mdebug=&sasjs_mdebug
)
data work.mptables;
  set web1.mptables;
  putlog (_all_)(=);
run;

%mp_assertdsobs(work.mptables,
  desc=DCTEST contains tables,
  test=ATLEAST 2
)

%let engine=0;
data work.libinfo;
  set web1.libinfo;
  putlog (_all_)(=);
  call symputx('engine',engine);
run;

%mp_assertdsobs(work.libinfo,
  desc=LIBINFO contains one row,
  test=EQUALS 1
)
%mp_assert(
  iftrue=(&engine=%mf_getengine(DCTEST)),
  desc=Checking returned engine matches the one we put in
)


%let catcnt=0;
proc sql noprint;
select count(*) into: catcnt
  from work.mptables
  where substr(reverse(cats(upcase(memname))),1,3)='CF-';

%mp_assert(
  iftrue=(&catcnt=1),
  desc=Checking dclib cat was created
)



