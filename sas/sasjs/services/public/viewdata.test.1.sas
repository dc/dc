/**
  @file
  @brief testing public/viewdata service for filtered rowcount
  @details We had a situation where the row count for a filtered view was
  the count for the entire table, not the filtered view.

  This test first makes a filter, then applies to to a table, and checks the
  rowcount.

  <h4> SAS Macros </h4>
  @li mp_assert.sas
  @li mp_testservice.sas
  @li mf_getuniquefileref.sas

**/

/* first, ensure the table has the 3 records we need */
proc sql;
delete from &dclib..mpe_x_test where PRIMARY_KEY_FIELD in (1,2,3);
data work.append;
  set &dclib..mpe_x_test;
  do PRIMARY_KEY_FIELD=1,2,3;
    output;
  end;
  stop;
run;
proc append base=&dclib..mpe_x_test data=work.append;
run;

/* now, validate the filter and get the RK */
%let _program=&appLoc/services/public/validatefilter;

/* create filter */
%let f1=%mf_getuniquefileref();
data _null_;
  file &f1 termstr=crlf;
  put 'filter_table:$41.';
  put "&dclib..MPE_X_TEST";
run;
%let f2=%mf_getuniquefileref();
data _null_;
  file &f2 termstr=crlf;
  infile datalines4 dsd;
  input;
  put _infile_;
datalines4;
GROUP_LOGIC:$3. SUBGROUP_LOGIC:$3. SUBGROUP_ID:8. VARIABLE_NM:$32. OPERATOR_NM:$10. RAW_VALUE:$4000.
AND,AND,1,PRIMARY_KEY_FIELD,IN,"(1,2,3)"
;;;;
run;
%mp_testservice(&_program,
  viyacontext=&defaultcontext,
  inputfiles=&f1:iwant &f2:filterquery,
  outlib=web1
)
data result;
  set web1.result;
  putlog (_all_)(=);
  call symputx('filter_rk',filter_rk);
run;


/* now use this to filter a viewtable for three records */

%let _program=&appLoc/services/public/viewdata;

/* filter for one record */
%let f3=%mf_getuniquefileref();
data _null_;
  file &f3 termstr=crlf;
  put 'LIBDS:$char19. FILTER_RK:best.';
  put "&dclib..MPE_X_TEST,&filter_rk";
run;

%mp_testservice(&_program,
  viyacontext=&defaultcontext,
  inputfiles=&f3:SASControlTable ,
  outlib=web2,
  outref=rawfile,
  mdebug=1
)
%let nobs=0;
data sasparams;
  set web2.sasparams;
  putlog (_all_)(=);
  call symputx('nobs',nobs);
run;

%mp_assert(
  iftrue=(&nobs=3),
  desc=Checking the view table has 3 records returned,
  outds=work.test_results
)

%put viewdata raw:;
data _null_;
  infile rawfile;
  input;
  putlog _infile_;
run;


