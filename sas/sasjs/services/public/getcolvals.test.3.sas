/**
  @file
  @brief testing getcolvals service with FORMATS

  <h4> SAS Macros </h4>
  @li mp_assertcolvals.sas
  @li mf_getuniquefileref.sas

**/

%let _program=&appLoc/services/public/getcolvals;


data work.iwant;
  LIBDS="DCTEST.DCFMTS-FC";
  COL="FMTNAME";
run;
%let f2=%mf_getuniquefileref();
data _null_;
  file &f2 termstr=crlf;
  infile datalines4 dsd;
  input;
  if _n_=1 then do;
    put 'GROUP_LOGIC:$char3. SUBGROUP_LOGIC:$char3. SUBGROUP_ID:8. '@;
    put 'VARIABLE_NM:$char32. OPERATOR_NM:$char10. RAW_VALUE:$char4000.';
  end;
  put _infile_;
  putlog _infile_;
datalines4;
AND,OR,1,FMTNAME,CONTAINS,"'MOR'"
;;;;
run;
%mp_testservice(&_program,
  viyacontext=&defaultcontext,
  inputfiles=&f2:filterquery,
  inputdatasets=work.iwant,
  outlib=web2
)

data work.vals2;
  set web2.vals;
  putlog (_all_)(=);
run;

data work.check;
  type='MORDOR'; output;
run;
%mp_assertcolvals(work.vals2.unformatted,
  checkvals=work.check.type,
  desc=Checking fmtname for filtered format query(DCTEST.DCFMTS-FC),
  test=ALLVALS
)

