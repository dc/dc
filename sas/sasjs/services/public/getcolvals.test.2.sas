/**
  @file
  @brief testing getcolvals service

  <h4> SAS Macros </h4>
  @li mp_assertcolvals.sas
  @li mf_getuniquefileref.sas

**/

%let _program=&appLoc/services/public/getcolvals;

%let f1=%mf_getuniquefileref();
%let f2=%mf_getuniquefileref();
data _null_;
  file &f1 termstr=crlf;
  put 'LIBDS:$43. COL:$32.';
  put "&dclib..MPE_TABLES,LOADTYPE";
run;
data _null_;
  file &f2 termstr=crlf;
  infile datalines4 dsd;
  input;
  if _n_=1 then do;
    put 'GROUP_LOGIC:$char3. SUBGROUP_LOGIC:$char3. SUBGROUP_ID:8. '@;
    put 'VARIABLE_NM:$char32. OPERATOR_NM:$char10. RAW_VALUE:$char4000.';
  end;
  put _infile_;
  putlog _infile_;
datalines4;
AND,OR,1,LIBREF,CONTAINS,"'DC'"
AND,OR,1,LIBREF,CONTAINS,"'VIYA'"
AND,OR,2,DSN,=,"'MPE_LOCK_ANYTABLE'"
AND,OR,2,DSN,=,"'MPE_X_TEST'"
;;;;
run;
%mp_testservice(&_program,
  viyacontext=&defaultcontext,
  inputfiles=&f1:iwant &f2:filterquery,
  outlib=web2
)

data work.vals2;
  set web2.vals;
  putlog (_all_)(=);
run;

data work.check;
  type='UPDATE'; output;
run;
%mp_assertcolvals(work.vals2.unformatted,
  checkvals=work.check.type,
  desc=Checking loadtype for DCLIB in MPE_TABLES,
  test=ALLVALS
)

