/**
  @file
  @brief testing public/refreshlibinfo service

  <h4> SAS Macros </h4>
  @li mp_assert.sas
  @li mx_testservice.sas

  <h4> Related Programs </h4>
  @li refreshlibinfo.sas

**/

%let _program=&appLoc/services/public/refreshlibinfo;

data work.lib2refresh;
  libref="&dclib";
run;

%mx_testservice(&_program,
  viyacontext=&defaultcontext,
  inputdatasets=work.lib2refresh,
  outlib=web1,
  mdebug=&sasjs_mdebug
)

%let libref=0;
data work.libinfo;
  set web1.libinfo;
  putlog (_all_)(=);
  call symputx('libref',libname);
run;

%mp_assert(
  iftrue=(&libref=&dclib),
  desc=Ensure the service ran without errors
)



