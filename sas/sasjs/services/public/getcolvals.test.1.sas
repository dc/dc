/**
  @file
  @brief testing getcolvals service

  <h4> SAS Macros </h4>
  @li mp_assertcolvals.sas
  @li mf_getuniquefileref.sas

**/

%let _program=&appLoc/services/public/getcolvals;

%let f3=%mf_getuniquefileref();
data _null_;
  file &f3 termstr=crlf;
  put 'LIBDS:$43. COL:$32.';
  put "&dclib..MPE_X_TEST,SOME_TIME";
run;
%mp_testservice(&_program,
  viyacontext=&defaultcontext,
  inputfiles=&f3:iwant,
  outlib=web3
)

data work.vals;
  set web3.vals;
  putlog (_all_)(=);
run;
data work.check;
  val=1;output;
  val=2;output;
run;
%mp_assertcolvals(work.vals.unformatted,
  checkvals=work.check.val,
  desc=DCLIB found in unfiltered getgolvals response,
  test=ANYVAL
)
