/**
  @file
  @brief Download DDL for a table or entire library in a particular flavour.
  @details

  <h4> SAS Macros </h4>
  @li mddl_sas_cntlout.sas
  @li dc_assignlib.sas
  @li mf_existds.sas
  @li mp_abort.sas
  @li mp_getddl.sas
  @li mp_streamfile.sas


  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/
%global libref ds flavour;

%let flavour=%sysfunc(coalescec(&flavour,SAS));

%mpeinit()

%dc_assignlib(READ,&libref)

data _null_;
  /* check if the request is for a format catalog */
  ds=symget('ds');
  if subpad(cats(reverse(ds)),1,3)=:'CF-' then do;
    ds=scan(ds,1,'-');
    libds=cats(symget('libref'),'.',ds);
    putlog "Format Catalog Captured";
    call execute('%mddl_sas_cntlout(libds=work.fmtextract)');
    call symputx('libref','work');
    call symputx('ds','fmtextract');
  end;
  putlog (_all_)(=);
run;

%mp_abort(iftrue=("exist&ds" ne "exist" and %mf_existds(libds=&libref..&ds)<1)
  ,mac=&_program
  ,msg=%str(Dataset &libref..&ds was not found)
)

%let tmploc=%sysfunc(pathname(work))/temp.txt;
filename tmp "&tmploc";
%mp_getddl(&libref,&ds,flavour=&flavour, fref=tmp, applydttm=YES)

%mp_streamfile(contenttype=TEXT
  ,inloc=%str(&tmploc)
  ,outname=&libref._&ds..ddl
)
