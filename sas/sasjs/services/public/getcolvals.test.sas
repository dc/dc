/**
  @file
  @brief testing gethistory service

  <h4> SAS Macros </h4>
  @li mp_assertcolvals.sas
  @li mf_getuniquefileref.sas

**/

%let _program=&appLoc/services/public/getcolvals;

%let f1=%mf_getuniquefileref();
data _null_;
  file &f1 termstr=crlf;
  put 'LIBDS:$43. COL:$32.';
  put "&dclib..MPE_TABLES,LIBREF";
run;
%mp_testservice(&_program,
  viyacontext=&defaultcontext,
  inputfiles=&f1:iwant,
  outlib=web1
)

data work.vals1;
  set web1.vals;
  putlog (_all_)(=);
run;


data work.check;
  val="&dclib";
run;
%mp_assertcolvals(work.vals1.unformatted,
  checkvals=work.check.val,
  desc=DCLIB found in unfiltered getgolvals response,
  test=ANYVAL
)
