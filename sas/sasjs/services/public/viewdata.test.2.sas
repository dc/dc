/**
  @file
  @brief testing public/viewdata service for serving formats
  @details Since v4 of DC we can support formats in VIEW and EDIT mode.

  <h4> SAS Macros </h4>
  @li mp_assert.sas
  @li mp_testservice.sas

**/

%let _program=&appLoc/services/public/viewdata;

data work.sascontroltable;
  libds='DCTEST.DCFMTS-FC';
  FILTER_RK=0;
run;

%mp_testservice(&_program,
  viyacontext=&defaultcontext,
  inputdatasets=work.SASControlTable ,
  outlib=web2,
  mdebug=1
)
%let nobs=0;
data _null_;
  set web2.viewdata nobs=nobs;
  putlog (_all_)(=);
  if _n_=1 then call symputx('nobs',nobs);
  else if _n_>10 then stop;
run;

%mp_assert(
  iftrue=(&nobs>2),
  desc=Checking the format table has records returned,
  outds=work.test_results
)
