/**
  @file refreshlibinfo.sas
  @brief Refresh the Data Catalog for a particular library
  @details When showing library info in the VIEW menu, the data is taken from
  the Data Catalog tables.  These may be empty or outdated, and so this service
  allows end users to run a refresh of the data.

  <h4> Service Inputs </h4>
  <h5> lib2refresh </h5>
  Should contain the libref to be refreshed.
  |libref:$8.|
  |---|
  |SOMELIB|

  <h4> Service Outputs </h4>

  <h5> libinfo </h5>

  |engine $|libname $|paths $|perms $|owners $|schemas $ |libid $|libsize $|table_cnt |
  |---|---|---|---|---|---|---|---|---|
  |V9|SOMELIB|"some/path"|rwxrwxr-x|sassrv|` `|` `|636MB|33|

  <h4> SAS Macros </h4>
  @li dc_assignlib.sas
  @li dc_refreshcatalog.sas
  @li mp_abort.sas

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/


%mpeinit()

%webout(FETCH)

%mp_abort(iftrue= (&syscc ne 0)
  ,msg=%str(syscc=&syscc Problem on startup)
)

%let libref=;
data _null_;
  set work.lib2refresh;
  call symputx('libref',libref);
run;

%mp_abort(iftrue= (&syscc ne 0)
  ,msg=%str(syscc=&syscc Problem with inputs - was lib2refresh object sent?)
)

%dc_assignlib(WRITE,&libref)

%mp_abort(iftrue= (&syscc ne 0)
  ,msg=%str(syscc=&syscc after lib assignment)
)

%dc_refreshcatalog(&libref)

%mp_abort(iftrue= (&syscc ne 0)
  ,msg=%str(syscc=&syscc Problem when running the catalog refresh)
)

/* get libinfo */
proc sql;
create table work.libinfo as
  select a.engine,
    a.libname,
    a.paths,
    a.perms,
    a.owners,
    a.schemas,
    a.libid,
    b.libsize,
    b.table_cnt
  from &mpelib..mpe_datacatalog_libs(where=(&dc_dttmtfmt. lt tx_to)) a
  inner join &mpelib..mpe_datastatus_libs(where=(&dc_dttmtfmt. lt tx_to)) b
  on a.libref=b.libref
  where a.libref="&libref";

%webout(OPEN)
%webout(OBJ,libinfo)
%webout(CLOSE)
