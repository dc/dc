/**
  @file
  @brief testing gethistory service

  <h4> SAS Macros </h4>
  @li mp_assertdsobs.sas
  @li mp_testservice.sas

**/

%let _program=&appLoc/services/public/startupservice;

%mp_testservice(&_program,
  viyacontext=&defaultcontext,
  outlib=webout
)


data work.globvars;
  set webout.globvars;
  putlog (_all_)(=);
run;

data work.xlmaps;
  set webout.xlmaps;
  putlog (_all_)(=);
run;

%mp_assertdsobs(work.globvars,
  desc=Fromsas table returned,
  test=HASOBS,
  outds=work.test_results
)

%mp_assertdsobs(work.xlmaps,
  desc=xlmaps table returned,
  test=HASOBS,
  outds=work.test_results
)