/**
  @file
  @brief testing getrawdata service

  <h4> SAS Macros </h4>
  @li mp_assertdsobs.sas
  @li mx_testservice.sas

**/


%let _program=&appLoc/services/public/getrawdata;

/* test1 - regular table */
data work.params;
  length name $32 value $1000;
  name='type';value='CSV';output;
  name='table';value="&dclib..MPE_X_TEST";output;
  name='filter';value='0';output;
run;

%mx_testservice(&_program,
  viyacontext=&defaultcontext,
  inputparams=work.params,
  outref=webout,
  viyaresult=WEBOUT_TXT,
  mdebug=&sasjs_mdebug
)

data work.results;
  infile webout;
  input;
  if _infile_=:'PRIMARY_KEY_FIELD,SOME_CHAR' then do;
    output;
    output;
    stop;
  end;
run;

%mp_assertdsobs(work.results,
  desc=CSV file is successfully returned,
  test=EQUALS 2,
  outds=work.test_results
)

/* test 2 - format table */
data work.params2;
  length name $32 value $1000;
  name='type';value='SAS';output;
  name='table';value="DCTEST.DCFMTS-FC";output;
  name='filter';value='0';output;
run;

%mx_testservice(&_program,
  viyacontext=&defaultcontext,
  inputparams=work.params2,
  outref=web2,
  viyaresult=WEBOUT_TXT
)

data work.results2;
  infile web2;
  input;
  putlog _infile_;
  if _infile_=:'datalines4;' then do;
    output;
    output;
    output;
    stop;
  end;
  if _n_>100 then stop;
run;

%mp_assertdsobs(work.results2,
  desc=datalines file is successfully returned for format catalog,
  test=EQUALS 3,
  outds=work.test_results
)
