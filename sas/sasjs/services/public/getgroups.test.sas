/**
  @file
  @brief testing getgroups service.

  <h4> SAS Macros </h4>
  @li mf_getuser.sas
  @li mp_assertcols.sas
  @li mp_testservice.sas

**/

%let _program=&appLoc/services/public/getgroups;

/* add user */
%put &=sasjs_mdebug;
%mp_testservice(&_program,
  viyacontext=&defaultcontext,
  outlib=webout,
  mdebug=1,
  debug=131
)


data work.return;
  set webout.groups;
  putlog (_all_)(=);
run;

%mp_assertcols(work.return,
  cols=groupuri groupname groupdesc,
  test=ALL
)

