/**
  @file startupservice.sas
  @brief List the libraries and tables the mp-editor user can access
  @details If user is in a control group (&mpeadmins, configured in mpeinit.sas)
    then they have access to all libraries / tables.  Otherwise a join is made
    to the &mpelib..mpe_security table.


  <h4> SAS Macros </h4>
  @li mf_getuser.sas
  @li mpe_getgroups.sas
  @li mp_abort.sas
  @li mpeinit.sas

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%mpeinit()
%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program..sas
  ,msg=%str(Issue on startup in startupService)
)

%let userid=%mf_getuser();
%put userid is &userid;
%mpe_getgroups(user=&userid,outds=groups)
%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program..sas
  ,msg=%str(Issue with Groups syscc=&syscc for user &userid)
)

/* check if user is an admin */
%let admin_check=0;
proc sql noprint;
select count(*) into: admin_check
  from groups
  where groupname="&mpeadmins";

/* check if user is registered or not */
%let isRegistered=0;
select count(*) into: isregistered
  from &mpelib..mpe_users
  where user_id="&userid";

/* get number of registered users */
%let registerCount=0;
select count(*) into: registercount
  from &mpelib..mpe_users
  where last_seen_dt>%sysfunc(today())-365;

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program..sas
  ,msg=%str(Problem accessing &mpelib..mpe_users table)
)

%global dc_restrict_editrecord;
data work.globvars;
  dclib="&mpelib";
  sas9lineage_enabled=1;
  isadmin=&admin_check;
  isregistered=&isregistered;
  registercount=&registerCount;
  dc_admin_group="&mpeadmins";
  /* fetched from mpe_config in dc_getsettings */
  licence_key="&dc_licence_key";
  activation_key="&dc_activation_key";
  dc_restrict_editrecord="&dc_restrict_editrecord";
run;


%macro mstp_mpeditorstartup();
  data _null_;
    putlog "entering &sysmacroname";
  run;
  proc sql noprint;
  /* update last seen, if seen */
  %if &isregistered>0 %then %do;
    update &mpelib..mpe_users
      set last_seen_dt=%sysfunc(today())
      where user_id="&userid";
  %end;
  %mp_abort(iftrue= (&syscc ne 0)
    ,mac=&_program..sas
    ,msg=%str(Problem updating &mpelib..mpe_users table)
  )

  %local all_cnt;
  select count(*) into: all_cnt
      from &mpelib..mpe_security
      where &dc_dttmtfmt. lt tx_to
        and ACCESS_LEVEL in ('EDIT')
        and libref='*ALL*'
        and SAS_GROUP in (select groupname from groups);
  %if &admin_check >0 or &all_cnt>0 %then %do;
    create table sasDatasets as
      select distinct libref, dsn
      from &mpelib..mpe_tables
      where &dc_dttmtfmt. lt tx_to
      order by 1;
  %end;
  %else %do;
    create table sasDatasets as
      select distinct a.libref,a.dsn
      from &mpelib..mpe_tables a
      left join &mpelib..mpe_security b
      on a.libref=b.libref
        and a.dsn=b.dsn
      where &dc_dttmtfmt. lt a.tx_to
        and &dc_dttmtfmt. lt b.tx_to
        and b.ACCESS_LEVEL in ('EDIT')
        and b.SAS_GROUP in (select groupname from groups)
      order by 1;
  %end;
%mend mstp_mpeditorstartup;
%mstp_mpeditorstartup()

create table saslibs as
  select distinct libref
  from &syslast;

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program..sas
  ,msg=%str(issue with security validation)
)

proc sql;
create table work.xlmaps as
  select distinct a.XLMAP_ID
    ,b.XLMAP_DESCRIPTION
    ,coalescec(b.XLMAP_TARGETLIBDS,"&mpelib..MPE_XLMAP_DATA")
      as XLMAP_TARGETLIBDS
  from &mpelib..MPE_XLMAP_RULES a
  left join &mpelib..MPE_XLMAP_INFO(where=(&dc_dttmtfmt. lt tx_to)) b
  on a.XLMAP_ID=b.XLMAP_ID
  where &dc_dttmtfmt. lt a.tx_to;

/* we don't want the XLMAP target datasets to be directly editable */
delete from sasdatasets
  where cats(libref,'.',dsn) in (select XLMAP_TARGETLIBDS from xlmaps);

%webout(OPEN)
%webout(OBJ,sasDatasets)
%webout(OBJ,saslibs)
%webout(OBJ,globvars)
%webout(ARR,xlmaps)
%webout(CLOSE)

%mpeterm()
