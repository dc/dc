/**
  @file
  @brief testing public/validatefilter service

  <h4> SAS Macros </h4>
  @li mp_assertdsobs.sas
  @li mp_assertcolvals.sas
  @li mf_getuniquefileref.sas

**/

%let _program=&appLoc/services/public/validatefilter;

/* filter for one record */
%let f1=%mf_getuniquefileref();
data _null_;
  file &f1 termstr=crlf;
  put 'filter_table:$41.';
  put "&dclib..MPE_TABLES";
run;
%let f2=%mf_getuniquefileref();
data _null_;
  file &f2 termstr=crlf;
  infile datalines4 dsd;
  input;
  put _infile_;
datalines4;
GROUP_LOGIC:$3. SUBGROUP_LOGIC:$3. SUBGROUP_ID:8. VARIABLE_NM:$32. OPERATOR_NM:$10. RAW_VALUE:$4000.
AND,AND,1,LIBREF,CONTAINS,"'DC'"
AND,OR,2,DSN,=,"'MPE_LOCK_ANYTABLE'"
;;;;
run;
%mp_testservice(&_program,
  viyacontext=&defaultcontext,
  inputfiles=&f1:iwant &f2:filterquery,
  outlib=web1
)
data result;
  set web1.result;
run;
%mp_assertdsobs(work.result,
  desc=Test1 result filter,
  test=EQUALS 1
)

/* filter for one record */

data work.iwant;
  filter_table='DCTEST.DCFMTS-FC';
run;
data work.filterquery;
GROUP_LOGIC='AND';
SUBGROUP_LOGIC='AND';
SUBGROUP_ID=1;
VARIABLE_NM='FMTNAME';
OPERATOR_NM='CONTAINS';
RAW_VALUE="'MORD'";
;;;;
run;
%mp_testservice(&_program,
  viyacontext=&defaultcontext,
  inputdatasets=work.iwant work.filterquery,
  outlib=web2,
  outref=web2ref
)
data _null_;
  infile web2ref;
  input;
  putlog _infile_;
run;
data result;
  set web2.result;
  putlog (_all_)(=);
run;
%mp_assertdsobs(work.result,
  desc=Test2 result filter,
  test=EQUALS 1
)
