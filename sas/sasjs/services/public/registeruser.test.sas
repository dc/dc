/**
  @file
  @brief testing registeruser service.  Test will only work once (after deploy)

  <h4> SAS Macros </h4>
  @li mf_getuser.sas
  @li mp_assertcolvals.sas
  @li mx_testservice.sas

**/

%let _program=&appLoc/services/public/registeruser;

/* remove user */
%let userid=%mf_getuser();
proc sql noprint;
delete from &dc_libref..mpe_users
  where user_id="&userid";

/* add user */
%mx_testservice(&_program,
  viyacontext=&defaultcontext,
  outlib=webout,
  mdebug=&sasjs_mdebug
)

data _null_;
  infile "%sysfunc(pathname(webout))" lrecl=999999;
  input;
  putlog _infile_;
run;

data work.return;
  set webout.return;
  putlog (_all_)(=);
run;

data work.check;
  msg='SUCCESS';
run;
%mp_assertcolvals(work.return.msg,
  checkvals=work.check.msg,
  desc=User successfully registered,
  test=ALLVALS
)
