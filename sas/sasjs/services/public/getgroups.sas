/**
  @file getgroups.sas
  @brief List all SAS Groups
  @details Gets a list of all SAS Groups.  Runs without mpeinit() so that it
  can be available to the sasjs/server configurator

  <h4> SAS Macros </h4>
  @li dc_getgroups.sas

  <h4> Data Outputs </h4>
  <h5> groups </h5>
  |NAME:$32.|DESCRIPTION:$64.|GROUPID:best.|
  |---|---|---|
  |`SomeGroup `|`A group `|`1`|
  |`Another Group`|`this is a different group`|`2`|
  |`admin`|`Administrators `|`3`|

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%dc_getgroups(outds=groups)

%webout(OPEN)
%webout(OBJ,groups)
%webout(CLOSE)

