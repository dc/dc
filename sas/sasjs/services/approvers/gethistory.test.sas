/**
  @file
  @brief testing gethistory service

  <h4> SAS Macros </h4>
  @li mp_assertdsobs.sas

**/

%let _program=&appLoc/services/approvers/gethistory;

filename fref1 temp;

data _null_;
  file fref1 termstr=crlf;
  put 'HIST:best.';
  put '50';
run;

%mp_testservice(&_program,
  viyacontext=&defaultcontext,
  inputfiles=fref1:BrowserParams ,
  outlib=webout,
  debug=log
)


data fromsas;
  set webout.fromsas;
run;

%mp_assertdsobs(work.fromsas,
  desc=Fromsas table returned,
  test=HASOBS,
  outds=work.test_results
)