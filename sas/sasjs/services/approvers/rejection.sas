/**
  @file
  @brief Removes a staged data package from approval screen
  @details

  <h4> SAS Macros </h4>
  @li mf_getuser.sas
  @li mf_getvarlist.sas
  @li mf_verifymacvars.sas
  @li mp_abort.sas
  @li mp_lockanytable.sas
  @li mpe_accesscheck.sas
  @li mpe_alerts.sas
  @li mpe_getvars.sas
  @li removecolsfromwork.sas

  <h4> Service Outputs </h4>
  <h5> fromsas </h5>

  @li TABLE_ID
  @li SUBMITTED_REASON_TXT
  @li RESPONSE

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/
%global STP_ACTION TABLE STP_REASON;
%mpeinit()

%mpe_getvars(BrowserParams, BrowserParams)

PROC FORMAT;
  picture yymmddhhmmss other='%0Y-%0m-%0d %0H:%0M:%0S' (datatype=datetime);
RUN;

/* get current status and base table */
data _null_;
  set &mpelib..mpe_submit(where=(TABLE_ID="&TABLE"));
  call symputx('BASE_TABLE',cats(base_lib,'.',base_ds));
  call symputx('submit_status_cd',submit_status_cd);
run;

%mp_abort(
  iftrue=(%mf_verifymacvars(base_table)=0)
  ,mac=&_program
  ,msg=%str(Missing: base_table)
)

%mp_abort(
  iftrue=(%quote(&submit_status_cd)=%quote(REJECTED))
  ,mac=&_program
  ,msg=%str(&table is already rejected!)
)

%mp_abort(iftrue= (&syscc ge 4)
  ,mac=&_program
  ,msg=%str(Issue on setup)
)

/**
  * determine if user is authorised to reject table
  */
%let user=%mf_getuser();
%global authcheck; %let authcheck=0;
%mpe_accesscheck(&base_table,outds=authAPP,user=&user,access_level=APPROVE)
%let authcheck=%mf_getattrn(work.authAPP,NLOBS);

%mp_abort(iftrue= (&authcheck=0)
  ,mac=&_program..sas
  ,msg=%str(User &user does not have APPROVE rights on &base_table and is not
    in the &mpeadmins group)
)

/* update the control table to show table as rejected (and why) */
%let now=%sysfunc(datetime());
data work.reject;
  if 0 then set &mpelib..mpe_review;
  TABLE_ID="&table";
  BASE_TABLE="&base_table";
  REVIEW_STATUS_ID="REJECTED";
  REVIEWED_BY_NM="&user";
  REVIEWED_ON_DTTM=&now;
  REVIEW_REASON_TXT=symget('STP_REASON');
run;

%mp_lockanytable(LOCK,
  lib=&mpelib,ds=mpe_review,ref=%str(&table rejection),
  ctl_ds=&mpelib..mpe_lockanytable
)
proc append base=&mpelib..mpe_review data=work.reject;
run;
%mp_lockanytable(UNLOCK,
  lib=&mpelib,ds=mpe_review,
  ctl_ds=&mpelib..mpe_lockanytable
)

%mp_lockanytable(LOCK,
  lib=&mpelib,ds=mpe_submit,ref=%str(&table rejection),
  ctl_ds=&mpelib..mpe_lockanytable
)
proc sql;
update &mpelib..mpe_submit
  set submit_status_cd='REJECTED',
    num_of_approvals_remaining=0,
    reviewed_by_nm="&user",
    reviewed_on_dttm=&now
  where table_id="&table";
%mp_lockanytable(UNLOCK,
  lib=&mpelib,ds=mpe_submit,
  ctl_ds=&mpelib..mpe_lockanytable
)


%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program..sas
  ,msg=%str(syscc=&syscc AFTER update...)
)

%mpe_alerts(alert_event=REJECTED
  , alert_lib=%scan(&BASE_TABLE,1,.)
  , alert_ds=%scan(&BASE_TABLE,2,.)
  , dsid=&TABLE
)

data fromSAS;
  RESPONSE='SUCCESS!';
  set REJECT;
run;

%removecolsfromwork(___TMP___MD5)

%webout(OPEN)
%webout(OBJ,fromSAS)
%webout(CLOSE)


%mpeterm()
