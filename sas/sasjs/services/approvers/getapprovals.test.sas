/**
  @file
  @brief testing gethistory service

  <h4> SAS Macros </h4>
  @li mp_assertdsobs.sas

**/

%let _program=&appLoc/services/approvers/getapprovals;

%mp_testservice(&_program,
  viyacontext=&defaultcontext,
  outlib=webout
)


data fromsas;
  set webout.fromsas;
run;

%mp_assertdsobs(work.fromsas,
  desc=Fromsas table returned,
  test=HASOBS,
  outds=work.test_results
)