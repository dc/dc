/**
  @file
  @brief Downloads zip file of DC customer configurations
  @details Zip contains several excel files, containing the customer specific
    (non-DC) configurations.  Useful when migrating to a new instance of
    Data Controller.


  <h4> SAS Macros </h4>
  @li mf_getuser.sas
  @li mf_nobs.sas
  @li mp_ds2cards.sas
  @li mp_abort.sas
  @li mp_binarycopy.sas
  @li mp_streamfile.sas

  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/

%mpeinit()
%let work=%sysfunc(pathname(work));

/* excel does not work in all envs */
%let mime=application/vnd.ms-excel;
%let dbms=EXCEL;

%let mime=application/csv;
%let dbms=CSV;
%let ext=csv;

%macro conditional_export(ds);
%if %mf_nobs(&ds)>0 %then %do;
  PROC EXPORT DATA= &ds OUTFILE= "&work/&ds..&ext"
    DBMS=&dbms REPLACE;
  RUN;
  ods package(ProdOutput) add file="&work/&ds..&ext" mimetype="&mime";
%end;
%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program
  ,msg=%nrstr(syscc=&syscc after &ds prep)
)
%mend conditional_export;


ods package(ProdOutput) open nopf;

data MPE_ALERTS;
  set &mpelib..MPE_ALERTS;
  where &dc_dttmtfmt. le tx_to;
  drop tx_: ;
run;
%conditional_export(MPE_ALERTS)


data MPE_COLUMN_LEVEL_SECURITY;
  set &mpelib..MPE_COLUMN_LEVEL_SECURITY;
  where &dc_dttmtfmt. le tx_to;
  where also CLS_LIBREF ne "&mpelib";
  drop tx_: ;
run;
%conditional_export(MPE_COLUMN_LEVEL_SECURITY)

data MPE_CONFIG;
  set &mpelib..MPE_CONFIG;
  where &dc_dttmtfmt. le tx_to;
  drop tx_: ;
run;
%conditional_export(MPE_CONFIG)

data MPE_DATADICTIONARY;
  set &mpelib..MPE_DATADICTIONARY;
  where &dc_dttmtfmt. le tx_to;
  drop tx_: ;
  if DD_SOURCE=:"&mpelib" then do;
    /* nothing */
  end;
  else output;
run;
%conditional_export(MPE_DATADICTIONARY)

data MPE_EMAILS;
  set &mpelib..MPE_EMAILS;
  where &dc_dttmtfmt. le tx_to;
  drop tx_: ;
run;
%conditional_export(MPE_EMAILS)

data MPE_EXCEL_CONFIG;
  set &mpelib..MPE_EXCEL_CONFIG;
  where &dc_dttmtfmt. le tx_to;
  drop tx_: ;
run;
%conditional_export(MPE_EXCEL_CONFIG)

data MPE_GROUPS;
  set &mpelib..MPE_GROUPS;
  where &dc_dttmtfmt. le tx_to;
  drop tx_: ;
run;
%conditional_export(MPE_GROUPS)

data MPE_ROW_LEVEL_SECURITY;
  set &mpelib..MPE_ROW_LEVEL_SECURITY;
  where &dc_dttmtfmt. le tx_to;
  drop tx_: ;
run;
%conditional_export(MPE_ROW_LEVEL_SECURITY)


data MPE_SECURITY;
  set &mpelib..MPE_SECURITY;
  where &dc_dttmtfmt. le TX_TO;
  drop tx_: ;
run;
%conditional_export(MPE_SECURITY)

data MPE_SELECTBOX;
  set &mpelib..MPE_SELECTBOX;
  where &dc_dttmtfmt. le ver_to_dttm;
  where also select_lib ne "&mpelib";
  drop ver_: selectbox_rk;
run;
%conditional_export(MPE_SELECTBOX)

data MPE_TABLES;
  set &mpelib..MPE_TABLES;
  where &dc_dttmtfmt. le TX_TO;
  where also LIBREF ne "&mpelib";
  drop tx_: ;
run;
%conditional_export(MPE_TABLES)

data MPE_VALIDATIONS;
  set &mpelib..MPE_VALIDATIONS;
  where &dc_dttmtfmt. le TX_TO;
  where also BASE_LIB ne "&mpelib";
  drop tx_: ;
run;
%conditional_export(MPE_VALIDATIONS)

/* finish up zip file */
ods package(ProdOutput) publish archive properties
        (archive_name="DCBACKUP.zip" archive_path="&work");
ods package(ProdOutput) close;

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program..sas
  ,msg=%nrstr(syscc=&syscc after zip prep)
)

/* now serve zip file to client */
%mp_streamfile(contenttype=ZIP
  ,inloc=%str(&work/DCBACKUP.zip)
  ,outname=DCBACKUP.zip
)
