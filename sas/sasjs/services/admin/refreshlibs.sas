/**
  @file refreshlibs.sas
  @brief Refreshes the library data catalog
  @details

  <h4> SAS Macros </h4>
  @li mpeinit.sas
  @li mpe_refreshlibs.sas

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/

%mpeinit()

%mpe_refreshlibs()
