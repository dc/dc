/**
  @file refreshcatalog.sas
  @brief Refreshes the library data catalog
  @details  A library may be passed in a LIBREF url param.

  <h4> SAS Macros </h4>
  @li mpeinit.sas
  @li dc_refreshcatalog.sas

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/
%global libref;
%mpeinit()

%dc_refreshcatalog(&libref)


data _null_;
  file _webout;
  put '<h1> Catalog Refresh Complete </h1>';
run;
