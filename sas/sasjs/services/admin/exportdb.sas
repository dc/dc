/**
  @file
  @brief Exports the data controller library in DB specific DDL
  @details If user is in the administrator group, they can call this
  service directly adding the following URL params:

    @li &flavour= (only PGSQL supported at this time)
    @li &schema= (optional, if target schema is needed)

  <h4> SAS Macros </h4>
  @li mf_getuser.sas
  @li mp_abort.sas
  @li mp_getddl.sas
  @li mp_lib2inserts.sas
  @li mp_streamfile.sas
  @li mpe_getgroups.sas

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/

%mpeinit()

%global flavour schema;
/* if no flavour is specified, default to SAS */
%let flavour=%sysfunc(coalescec(&flavour,SAS));

/* if no schema var provided, DC Libref is used */
%let schema=%sysfunc(coalescec(&schema,&dc_libref));

/* check user is in admin group */
%mpe_getgroups(user=%mf_getuser(),outds=work.usergroups)

data work.admins;
  set work.usergroups;
  put (_all_)(=);
run;

%let cnt=0;
proc sql noprint;
select count(*) into:cnt
  from usergroups
  where groupname="&mpeadmins";
%put &=cnt;
%mp_abort(iftrue= (&cnt=0)
  ,mac=&_program
  ,msg=%str(The &DC_LIBREF library can only be exported by &mpeadmins members)
)

%mp_getddl(&DC_LIBREF
  ,flavour=&flavour
  ,schema=&schema
  ,applydttm=YES
  ,fref=tmpref
)

%mp_lib2inserts(&DC_LIBREF,flavour=&flavour,schema=&schema,  outref=tmpref)

%mp_streamfile(contenttype=TEXT
  ,inref=tmpref
  ,outname=&dc_libref..ddl
)
