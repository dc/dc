/**
  @file getxlmaps.sas
  @brief Returns a list of rules and other info for a specific xlmap_id

  <h4> Service Inputs </h4>

  <h5> getxlmaps_in </h5>

  |XLMAP_ID|
  |---|
  |Sample|

  <h4> Service Outputs </h4>

  <h5> xlmaprules </h5>

  Filtered output of the dc.MPE_XLMAP_RULES table

  |XLMAP_ID|XLMAP_RANGE_ID|XLMAP_SHEET|XLMAP_START|XLMAP_FINISH|
  |---|---|---|---|---|
  |Sample|Range1|Sheet1|ABSOLUTE A1| |
  |Sample|Range2|Sheet1|RELATIVE R[2]C[2]|ABSOLUTE H11|

  <h5> xlmapinfo </h5>
  Extra info for a map id

  |TARGET_DS|
  |---|
  |DCXXX.MPE_XLMAP_DATA|

  <h4> SAS Macros </h4>
  @li mp_abort.sas
  @li mpeinit.sas

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/

%mpeinit()

data _null_;
  set work.getxlmaps_in;
  putlog (_all_)(=);
  call symputx('xlmap_id',xlmap_id);
run;

proc sql noprint;
create table work.xlmaprules as
  select xlmap_id
    ,XLMAP_RANGE_ID
    ,XLMAP_SHEET
    ,XLMAP_START
    ,XLMAP_FINISH
  from &mpelib..MPE_XLMAP_RULES
  where &dc_dttmtfmt. lt tx_to and xlmap_id="&xlmap_id"
  order by xlmap_sheet, xlmap_range_id;

%global target_ds;
select XLMAP_TARGETLIBDS into: target_ds
  from &mpelib..MPE_XLMAP_INFO
  where &dc_dttmtfmt. lt tx_to and xlmap_id="&xlmap_id";

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program..sas
  ,msg=%str(syscc=&syscc)
)

data work.xlmapinfo;
  target_ds=coalescec("&target_ds","&mpelib..MPE_XLMAP_DATA");
  output;
  stop;
run;

%webout(OPEN)
%webout(OBJ,xlmaprules)
%webout(OBJ,xlmapinfo)
%webout(CLOSE)

