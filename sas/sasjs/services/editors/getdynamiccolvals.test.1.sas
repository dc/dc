/**
  @file
  @brief testing getdynamiccolvals service

  <h4> SAS Macros </h4>
  @li mp_assertcols.sas
  @li mp_assertcolvals.sas
  @li mp_assertdsobs.sas
  @li mf_getuniquefileref.sas
  @li mx_testservice.sas

**/

%let _program=&appLoc/services/editors/getdynamiccolvals;

/**
  * Test 1 - basic fetch
  */

%let f1=%mf_getuniquefileref();
data _null_;
  file &f1 termstr=crlf;
  put 'libds:$41. variable_nm:$32.';
  put "&dclib..MPE_SECURITY,SAS_GROUP";
run;
%let f2=%mf_getuniquefileref();
data _null_;
  file &f2 termstr=crlf;
  put '_____DELETE__THIS__RECORD_____:$2. LIBREF:$8. DSN:$10. ACCESS_LEVEL:$7.';
  put 'No,DC265453,MPE_X_TEST,APPROVE';
run;
%mx_testservice(&_program,
  viyacontext=&defaultcontext,
  inputfiles=&f1:sascontroltable &f2:source_row,
  outlib=web1,
  mdebug=&sasjs_mdebug
)

data work.DYNAMIC_VALUES;
  set web1.DYNAMIC_VALUES;
  if _n_=1 then do;
    putlog '>>TEST1<<';
    putlog (_all_)(=);
  end;
run;

/* results are sent as ARRAY so there are no column names.  Check for table,
  and the three array alements
*/
%mp_assertcols(work.DYNAMIC_VALUES,
  cols=element1 element2 element3,
  test=ALL,
  desc=Check three columns exist in DYNAMIC_VALUES
)

/**
  * Test 2 - check libds
  */

%let f1=%mf_getuniquefileref();
data _null_;
  file &f1 termstr=crlf;
  put 'libds:$41. variable_nm:$32.';
  put "&dclib..MPE_TABLES,LIBREF";
run;
%let f2=%mf_getuniquefileref();
data _null_;
  file &f2 termstr=crlf;
  put 'LIBREF:$char8. DSN:$char16. NUM_OF_APPROVALS_REQUIRED:best. LOADTYPE:$char10. '@;
  put 'BUSKEY:$char35. VAR_TXFROM:$char7. VAR_TXTO:$char5. VAR_BUSFROM:$char1. '@;
  put 'VAR_BUSTO:$char1. VAR_PROCESSED:$char1. CLOSE_VARS:$char1. PRE_EDIT_HOOK:$char1. '@;
  put 'POST_EDIT_HOOK:$char1. PRE_APPROVE_HOOK:$char1. POST_APPROVE_HOOK:$char1. '@;
  put 'SIGNOFF_COLS:$char1. SIGNOFF_HOOK:$char1. NOTES:$char55. RK_UNDERLYING:$char1. '@;
  put 'AUDIT_LIBDS:$char1. _____DELETE__THIS__RECORD_____:$char2.';
  put 'DC266708,MPE_VALIDATIONSF,14,TXTEMPORAL,BASE_LIB BASE_DS BASE_COL RULE_TYPE,TX_FROM'@;
  put ',TX_TO, , , , , , , , ,,,Configuration of data quality rules in Editor component, , ,No';
run;
%mx_testservice(&_program,
  viyacontext=&defaultcontext,
  inputfiles=&f1:sascontroltable &f2:source_row,
  outlib=web2,
  mdebug=&sasjs_mdebug
)

data DYNAMIC_VALUES;
  set web2.DYNAMIC_VALUES;
  if _n_=1 then do;
    putlog '>>TEST2<<';
    putlog (_all_)(=);
  end;
run;

data work.check;
  val="&dclib";
run;
%mp_assertcolvals(work.dynamic_values.element2,
  checkvals=work.check.val,
  desc=DCLIB found in getdynamicgolvals DISPLAY_VALUE response,
  test=ANYVAL
)
%mp_assertcolvals(work.dynamic_values.element3,
  checkvals=work.check.val,
  desc=DCLIB found in getdynamicgolvals RAW_VALUE response,
  test=ANYVAL
)
