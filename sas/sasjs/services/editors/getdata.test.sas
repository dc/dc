/**
  @file
  @brief testing gethistory service

  <h4> SAS Macros </h4>
  @li mp_assertcols.sas
  @li mp_assertcolvals.sas
  @li mp_assertdsobs.sas
  @li mf_getuniquefileref.sas
  @li mf_getuniquename.sas
  @li mp_filterstore.sas
  @li mx_testservice.sas

**/

%let _program=&appLoc/services/editors/getdata;

/**
  * Test 1 - basic fetch
  */

%let f1=%mf_getuniquefileref();
data _null_;
  file &f1 termstr=crlf;
  put 'LIBDS:$43.';
  put "&dclib..MPE_TABLES";
run;
%mx_testservice(&_program,
  viyacontext=&defaultcontext,
  inputfiles=&f1:sascontroltable,
  outlib=web1,
  mdebug=&sasjs_mdebug
)
data _null_;
  infile "%sysfunc(pathname(WEB1))" lrecl=32767;
  input;
  putlog _infile_;
run;

data sasdata;
  set web1.sasdata;
data sasparams;
  set web1.sasparams;
data approvers;
    set web1.approvers;
data dqrules;
  set web1.dqrules;
data dqdata;
  set web1.dqdata;
data cols;
  set web1.cols;
data maxvarlengths;
  set web1.maxvarlengths;
data xl_rules;
  set web1.xl_rules;
data query;
  set web1.query;
run;

%mp_assertcols(work.query,
  desc=Query dataset is empty (test cols not obs as table has one row),
  cols=VARIABLE_NM RAW_VALUE SUBGROUP_ID,
  test=NONE,
  outds=work.test_results
)
%mp_assertdsobs(work.sasdata,
  desc=Test1 - data is returned,
  test=HASOBS,
  outds=work.test_results
)


/**
  * Test 2 - filtered view
  */

/* first, make filter */
data work.inquery;
  infile datalines4 dsd;
  input GROUP_LOGIC:$3. SUBGROUP_LOGIC:$3. SUBGROUP_ID:8. VARIABLE_NM:$32.
    OPERATOR_NM:$10. RAW_VALUE:$4000.;
datalines4;
AND,AND,1,LIBREF,CONTAINS,"'DC'"
AND,OR,2,DSN,=,"'MPE_LOCK_ANYTABLE'"
;;;;
run;
%mp_filterstore(
  libds=&dc_libref..MPE_TABLES,
  filter_summary=&dc_libref..mpe_filteranytable,
  filter_detail=&dc_libref..mpe_filtersource,
  lock_table=&dc_libref..mpe_lockanytable,
  maxkeytable=&dc_libref..mpe_maxkeyvalues,
  queryds=work.inquery,
  outresult=work.result,
  outquery=work.query
)
data _null_;
  set work.result;
  call symputx('filter_rk',filter_rk);
run;

%let f2=%mf_getuniquefileref();
data _null_;
  file &f2 termstr=crlf;
  put 'LIBDS:$43. FILTER_RK:8.';
  put "&dclib..MPE_TABLES,&filter_rk";
run;

%mx_testservice(&_program,
  viyacontext=&defaultcontext,
  inputfiles=&f2:sascontroltable,
  outlib=web2
)

data sasdata;
  set web2.sasdata;
data query;
  set web2.query;
run;

%mp_assertdsobs(work.query,
  desc=Test2 - query has rows,
  test=HASOBS,
  outds=work.test_results
)
%mp_assertdsobs(work.sasdata,
  desc=Test2 - data has just one row,
  test=EQUALS 1,
  outds=work.test_results
)


/**
  * Test 3 - format catalog
  */

/* first, make sure format data exists */
%let fmtname=%upcase(%substr(%mf_getuniquename(prefix=FMT),1,15))NAME;

/* add formats */
data work.fmts;
  length fmtname $32;
  fmtname="&fmtname";
  type='N';
  do start=1 to 10;
    label= cats("&fmtname",start);
    end=start;
    output;
  end;
run;
proc sort data=work.fmts nodupkey;
  by fmtname type start;
run;
proc format cntlin=work.fmts library=dctest.dcfmts;
run;

/* now, make filter */
data work.inquery3;
  infile datalines4 dsd;
  input GROUP_LOGIC:$3. SUBGROUP_LOGIC:$3. SUBGROUP_ID:8. VARIABLE_NM:$32.
    OPERATOR_NM:$10. RAW_VALUE:$4000.;
  RAW_VALUE="'&fmtname'";
datalines4;
AND,AND,1,FMTNAME,CONTAINS,placeholder (see line above)
;;;;
run;
%mp_filterstore(
  libds=DCTEST.DCFMTS-FC,
  filter_summary=&dc_libref..mpe_filteranytable,
  filter_detail=&dc_libref..mpe_filtersource,
  lock_table=&dc_libref..mpe_lockanytable,
  maxkeytable=&dc_libref..mpe_maxkeyvalues,
  queryds=work.inquery3,
  outresult=work.result3,
  outquery=work.query
)
data _null_;
  set work.result3;
  call symputx('filter_rk3',filter_rk);
run;

%let f3=%mf_getuniquefileref();
data _null_;
  file &f3 termstr=crlf;
  put 'LIBDS:$43. FILTER_RK:8.';
  put "DCTEST.DCFMTS-FC,&filter_rk3";
run;

%mx_testservice(&_program,
  viyacontext=&defaultcontext,
  inputfiles=&f3:sascontroltable,
  outlib=web3,
  mdebug=&sasjs_mdebug
)

data sasdata3;
  set web3.sasdata;
  putlog (_all_)(=);
data query3;
  set web3.query;
run;

%mp_assertdsobs(work.query3,
  desc=Test3 - format query has rows,
  test=HASOBS,
  outds=work.test_results
)
%mp_assertdsobs(work.sasdata3,
  desc=Test3 - format data has rows,
  test=EQUALS 10,
  outds=work.test_results
)
