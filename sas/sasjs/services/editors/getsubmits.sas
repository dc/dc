/**
  @file getsubmits.sas
  @brief Returns a list of staged data items that need to be approved
  @details

  <h4> SAS Macros </h4>
  @li mp_abort.sas
  @li mf_getuser.sas
  @li mpeinit.sas

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/

%mpeinit()

PROC FORMAT;
  picture yymmddhhmmss other='%0Y-%0m-%0d %0H:%0M:%0S' (datatype=datetime);
RUN;

proc sql noprint;
create table work.fromsas (rename=(SUBMITTED_ON=SUBMITTED_ON_DTTM)) as
  select table_id
    ,cats(base_lib,'.',base_ds) as base_table
    ,input_vars
    ,input_obs
    ,submitted_by_nm
    ,submitted_reason_txt
    ,'DEPRECATED' as approve_group
    ,submit_status_cd as review_status_id
    ,reviewed_by_nm
    ,reviewed_on_dttm
    ,cats(put(SUBMITTED_ON_DTTM,yymmddhhmmss.)) as SUBMITTED_ON
  from &mpelib..mpe_submit
  where submitted_by_nm="%mf_getuser()" and submit_status_cd='SUBMITTED'
  order by submitted_on_dttm desc;

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program..sas
  ,msg=%str(syscc=&syscc)
)

%webout(OPEN)
%webout(OBJ,fromSAS,missing=STRING)
%webout(CLOSE)

