/**
  @file
  @brief testing getdynamiccolvals service

  <h4> SAS Macros </h4>
  @li mp_assertcolvals.sas
  @li mf_getuniquefileref.sas
  @li mx_testservice.sas

**/

%let _program=&appLoc/services/editors/getdynamiccolvals;

/**
  * check extended values
  */

%let f1=%mf_getuniquefileref();
data _null_;
  file &f1 termstr=crlf;
  put 'libds:$41. variable_nm:$32.';
  put "&dclib..MPE_TABLES,DSN";
run;
%let f2=%mf_getuniquefileref();
data _null_;
  file &f2 termstr=crlf;
  put 'LIBREF:$char8. DSN:$char16. NUM_OF_APPROVALS_REQUIRED:best. LOADTYPE:$char10. '@;
  put 'BUSKEY:$char35. VAR_TXFROM:$char7. VAR_TXTO:$char5. VAR_BUSFROM:$char1. '@;
  put 'VAR_BUSTO:$char1. VAR_PROCESSED:$char1. CLOSE_VARS:$char1. PRE_EDIT_HOOK:$char1. '@;
  put 'POST_EDIT_HOOK:$char1. PRE_APPROVE_HOOK:$char1. POST_APPROVE_HOOK:$char1. '@;
  put 'SIGNOFF_COLS:$char1. SIGNOFF_HOOK:$char1. NOTES:$char55. RK_UNDERLYING:$char1. '@;
  put 'AUDIT_LIBDS:$char1. _____DELETE__THIS__RECORD_____:$char2.';
  put "&dclib,MPE_VALIDATIONS,14,TXTEMPORAL,BASE_LIB BASE_DS BASE_COL RULE_TYPE,TX_FROM"@;
  put ',TX_TO, , , , , , , , ,,,Configuration of data quality rules in Editor component, , ,No';
run;
%mx_testservice(&_program,
  viyacontext=&defaultcontext,
  inputfiles=&f1:sascontroltable &f2:source_row,
  outlib=web2,
  mdebug=&sasjs_mdebug
)

data DYNAMIC_VALUES;
  set web2.DYNAMIC_VALUES;
  if _n_<2 then putlog (_all_)(=);
run;

data DYNAMIC_EXTENDED_VALUES;
  set web2.DYNAMIC_EXTENDED_VALUES;
  if _n_<5 then putlog (_all_)(=);
run;

data work.check;
  val='VAR_PROCESSED';output;
  val='VAR_TXFROM';output;
  val='VAR_TXTO';output;
  val='VAR_BUSFROM';output;
  val='VAR_BUSTO';output;
run;
%mp_assertcolvals(work.DYNAMIC_EXTENDED_VALUES.element2,
  checkvals=work.check.val,
  desc=Correct values found in EXTRA_COL_NAME response,
  test=ALLVALS
)
