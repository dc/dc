/**
  @file
  @brief testing stagedata with row_level_security table

  <h4> SAS Macros </h4>
  @li mf_getuniquefileref.sas
  @li mx_testservice.sas
  @li mp_assert.sas


**/

%let _program=&appLoc/services/editors/stagedata;

/**
  * Test 1 - basic send
  */

%let f1=%mf_getuniquefileref();
data _null_;
  file &f1 termstr=crlf;
  put 'ACTION:$char4. MESSAGE:$char40. LIBDS:$char38.';
  put "LOAD,Testing upload of RLS table,&dclib..MPE_ROW_LEVEL_SECURITY";
run;

%let f2=%mf_getuniquefileref();
data _null_;
  file &f2 termstr=crlf;
  x=round(ranuni(0)*100000,1);
  y=cats(x);
  z=cats('"',"'",x,"'",'"');
  put 'RLS_SCOPE:$char3. RLS_GROUP:$char9. RLS_LIBREF:$char8. '@;
  put 'RLS_TABLE:$char32. RLS_GROUP_LOGIC:$char3. RLS_SUBGROUP_LOGIC:$char2. '@;
  put 'RLS_SUBGROUP_ID:best. RLS_VARIABLE_NM:$char32. RLS_OPERATOR_NM:$char2. '@;
  put 'RLS_RAW_VALUE:$char40. RLS_ACTIVE:best. _____DELETE__THIS__RECORD_____:$char2.';
  put "ALL,dcviewers,&dclib,MPE_ROW_LEVEL_SECURITY,AND,OR,0,RLS_ACTIVE,NE,"@;
  put y ",0,No";
  put "ALL,SASAdministrators,&dclib,MPE_ROW_LEVEL_SECURITY,AND,OR,0,RLS_SCOPE,NE,"@;
  put z ",0,No";
run;
%mx_testservice(&_program,
  viyacontext=&defaultcontext,
  inputfiles=&f1:sascontroltable &f2:jsdata,
  outlib=web1,
  mdebug=&sasjs_mdebug
)

%let status=0;
data work.sasparams;
  set web1.sasparams;
  putlog (_all_)(=);
  if status='SUCCESS' then call symputx('status',1);
run;


%mp_assert(
  iftrue=(&status=1),
  desc=Checking successful submission of a two row RLS table,
  outds=work.test_results
)

