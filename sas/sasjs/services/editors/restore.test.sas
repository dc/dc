/**
  @file
  @brief testing restore process

  <h4> SAS Macros </h4>
  @li mf_getuniquefileref.sas
  @li mx_testservice.sas
  @li mp_assert.sas


**/

%let _program=&appLoc/services/editors/restore;

/* take a snapshot of the table for later comparison */
proc sort data=&mpelib..mpe_x_test out=work.origds;
by primary_key_field;
run;

/* run an update */
%mx_testservice(&appLoc/tests/services/auditors/postdata.test.1,
  viyacontext=&defaultcontext
)

/* grab the loadref for later reversion */
%let loadref=0;
data APPROVE1;
  set &mpelib..mpe_submit end=last;
  if last then call symputx('loadref',table_id);
run;

/* run another update for good measure */
%mx_testservice(&appLoc/tests/services/auditors/postdata.test.1,
  viyacontext=&defaultcontext
)

/* now we are ready to revert */
data work.restore_in;
  load_ref="&loadref";
  output;
  stop;
run;
%mx_testservice(&_program,
  viyacontext=&defaultcontext,
  inputdatasets=work.restore_in,
  outlib=web1,
  mdebug=&sasjs_mdebug
)

/* check for success */
%let loadref=0;
data work.restore_out;
  set web1.restore_out;
  putlog (_all_)(=);
  call symputx('newref',loadref);
run;
%mp_assert(
  iftrue=(&newref ne 0),
  desc=Checking successful submission of a reversion,
  outds=work.test_results
)

/* approve the reversion */
data work.sascontroltable;
  ACTION='APPROVE_TABLE';
  TABLE="&newref";
  /* difftime is numeric for approve action */
  DIFFTIME="%sysfunc(datetime())";
  output;
  stop;
run;
%mx_testservice(&appLoc/services/auditors/postdata,
  viyacontext=&defaultcontext,
  inputdatasets=work.sascontroltable,
  outlib=web3,
  outref=wb3,
  mdebug=&sasjs_mdebug
)

%let status=0;
data _null_;
  set web3.apparams;
  putlog (_all_)(=);
  if response='SUCCESS!' then call symputx('status',1);
run;

%mp_assert(
  iftrue=(&status=1 and &syscc=0),
  desc=Checking successful submission of reversion
)

/* compare snapshot with latest data */
proc sort data=&mpelib..mpe_x_test out=work.compareds;
by primary_key_field;
run;

proc compare base=work.origds compare=work.compareds
  out=work.resultds outnoequal;
run;
data _null_;
  set work.resultds;
  if &sasjs_mdebug=1 then putlog (_all_)(=);
  if _n_>10 then stop;
run;
%mp_assert(
  iftrue=(&sysinfo le 41),
  desc=Checking compare of MPE_X_TEST,
  outds=work.test_results
)