/**
  @file
  @brief testing getxlmaps service

  <h4> SAS Macros </h4>
  @li mf_getuniquefileref.sas
  @li mx_testservice.sas
  @li mp_assert.sas
  @li mp_assertdsobs.sas


**/

%let _program=&appLoc/services/editors/getxlmaps;

/**
  * Test 1 - basic send
  */

%let f1=%mf_getuniquefileref();
data _null_;
  file &f1 termstr=crlf;
  put 'XLMAP_ID:$char12.';
  put "BASEL-KM1";
run;

%mx_testservice(&_program,
  viyacontext=&defaultcontext,
  inputfiles=&f1:getxlmaps_in,
  outlib=web1,
  mdebug=&sasjs_mdebug
)

data work.xlmaprules;
  set web1.xlmaprules;
  putlog (_all_)(=);
run;

%mp_assertdsobs(work.xlmaprules,
  test=ATLEAST 2,
  desc=Checking successful return of at least 2 rules for the BASEL-KM1 map,
  outds=work.test_results
)


/**
  * Test 2 - info returned
  */
data work.xlmapinfo;
  set web1.xlmapinfo;
  putlog (_all_)(=);
  call symputx('tgtds',target_ds);
run;
%mp_assert(
  iftrue=(&tgtds=&dclib..MPE_XLMAP_DATA),
  desc=Checking correct target table is returned,
  outds=work.test_results
)