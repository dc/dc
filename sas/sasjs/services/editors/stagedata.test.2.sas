/**
  @file
  @brief testing stagedata with format table

  <h4> SAS Macros </h4>
  @li mx_testservice.sas
  @li mp_assert.sas


**/

%let _program=&appLoc/services/editors/stagedata;

/**
  * Test 1 - basic send
  */
data work.sascontroltable;
  action='LOAD';
  MESSAGE='Testing upload of a format catalog';
  LIBDS="DCTEST.DCFMTS-FC";
run;

proc format lib=DCTEST.DCFMTS cntlout=work.fmtextract;
run;
data work.jsdata;
  set work.fmtextract;
  fmtrow=_n_;
  if _n_<5 then _____DELETE__THIS__RECORD_____='Yes';
  else _____DELETE__THIS__RECORD_____='No';
  if _n_>20 then stop;
run;

%mx_testservice(&_program,
  viyacontext=&defaultcontext,
  inputdatasets=work.sascontroltable work.jsdata,
  outlib=web1,
  mdebug=&sasjs_mdebug
)

%let status=0;
data work.sasparams;
  set web1.sasparams;
  putlog (_all_)(=);
  if status='SUCCESS' then call symputx('status',1);
run;

%mp_assert(
  iftrue=(&status=1),
  desc=Checking successful submission of a format catalog table,
  outds=work.test_results
)

