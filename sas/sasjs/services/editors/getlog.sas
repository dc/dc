/**
  @file getlog.sas
  @brief Downloads the submission, useful if there is an error
  @details

  <h4> SAS Macros </h4>
  @li mf_verifymacvars.sas
  @li mf_getuser.sas
  @li mp_abort.sas
  @li mp_dirlist.sas
  @li mp_binarycopy.sas
  @li mp_streamfile.sas

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/

%mpeinit()

%mp_abort(
  iftrue=(%mf_verifymacvars(table)=0)
  ,mac=&_program
  ,msg=%str(Missing: table)
)

/* security checks */
%let user=%mf_getuser();
%let check_access=0;

proc sql noprint;
select count(*) into: check_access from &mpelib..mpe_loads
  where csv_dir="&table" and user_nm="&user";

%mp_abort(iftrue= (&check_access=0 )
  ,msg=%str(&user not authorised to download audit data for &table)
  ,mac=mpestp_getlog.sas
)


ods package(ProdOutput) open nopf;

options notes source2 mprint;
%mp_dirlist(outds=dirs, path=&mpelocapprovals/&TABLE)
data _null_;
  set dirs;
  if scan(filename,-1,'.') not in ('sas7bdat','wpd');
  retain str1
    "ods package(ProdOutput) add file='&mpelocapprovals/&TABLE/";
  retain str2 "' mimetype='text/plain' path='contents/';";
  call execute(cats(str1,filename,str2));
run;

%let archive_path=%sysfunc(pathname(work));

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program..sas
  ,msg=%str(syscc=&syscc)
)
ods package(ProdOutput) publish archive  properties
      (archive_name=  "&table..zip" archive_path="&archive_path");

ods package(ProdOutput) close;

/* now serve zip file to client */
%mp_streamfile(contenttype=ZIP
  ,inloc=%str(&archive_path/&table..zip)
  ,outname=&table..zip
)

%mpeterm()
