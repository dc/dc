/**
  @file
  @brief Generic validator for editable libraries
  @details The input table is simply one row from the target table in table
  called "work.source_row".

  Available macro variables:
    @li MPELIB - The DC control library
    @li LIBDS - The library.dataset being filtered
    @li VARIABLE_NM - The column being filtered


  <h4> Service Outputs </h4>
  The values provided below are generic samples - we encourage you to replace
  these with realistic values in your own deployments.

  <h5>DYNAMIC_VALUES</h5>
  The RAW_VALUE column may be charactor or numeric.  If DISPLAY_INDEX is not
  provided, it is added automatically.

  |DISPLAY_INDEX:best.|DISPLAY_VALUE:$|RAW_VALUE|
  |---|---|---|
  |1|$77.43|77.43|
  |2|$88.43|88.43|

  <h5>DYNAMIC_EXTENDED_VALUES</h5>
  This table is optional.  If provided, it will map the DISPLAY_INDEX from the
  DYNAMIC_VALUES table to additional column/value pairs, that will be used to
  populate dropdowns for _other_ cells in the _same_ row.

  Should be used sparingly!  The use of large tables here can slow down the
  browser.

  |DISPLAY_INDEX:best.|EXTRA_COL_NAME:$32.|DISPLAY_VALUE:$|DISPLAY_TYPE:$1.|RAW_VALUE_NUM|RAW_VALUE_CHAR:$5000|
  |---|---|---|
  |1|DISCOUNT_RT|"50%"|N|0.5||
  |1|DISCOUNT_RT|"40%"|N|0.4||
  |1|DISCOUNT_RT|"30%"|N|0.3||
  |1|CURRENCY_SYMBOL|"GBP"|C||"GBP"|
  |1|CURRENCY_SYMBOL|"RSD"|C||"RSD"|
  |2|DISCOUNT_RT|"50%"|N|0.5||
  |2|DISCOUNT_RT|"40%"|N|0.4||
  |2|CURRENCY_SYMBOL|"EUR"|C||"EUR"|
  |2|CURRENCY_SYMBOL|"HKD"|C||"HKD"|


**/

/* send back the raw and formatted values */
proc sql;
create table work.DYNAMIC_VALUES as
  select distinct libref as display_value,
    upcase(libref) as raw_value
  from &mpelib..mpe_tables
  where &dc_dttmtfmt. < tx_to
  order by 1;

