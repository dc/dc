/**
  @file
  @brief fetch extended values for alert_lib
  @details Fetches libraries from mpe_tables, creates extended values for
  alert_ds, and marks "*ALL*" as the forced (default) value.

  Available macro variables:
    @li DC_LIBREF - The DC control library
    @li LIBDS - The library.dataset being filtered
    @li VARIABLE_NM - The column being filtered


  <h4> Service Outputs </h4>
  Output should be a single table called "work.dynamic_values" in the format
  below.  display_value should always be character, raw_value is unformatted
  character/numeric.

  <h5>DYNAMIC_VALUES</h5>
  The RAW_VALUE column may be charactor or numeric.  If DISPLAY_INDEX is not
  provided, it is added automatically.

  |DISPLAY_INDEX:best.|DISPLAY_VALUE:$|RAW_VALUE|
  |---|---|---|
  |1|$77.43|77.43|
  |2|$88.43|88.43|

  <h5>DYNAMIC_EXTENDED_VALUES</h5>
  This table is optional.  If provided, it will map the DISPLAY_INDEX from the
  DYNAMIC_VALUES table to additional column/value pairs, that will be used to
  populate dropdowns for _other_ cells in the _same_ row.

  Should be used sparingly!  The use of large tables here can slow down the
  browser.

  The FORCED_VALUE column can be used to force an extended value to be selected
  by default when a particular value is chosen.

  |DISPLAY_INDEX:best.|EXTRA_COL_NAME:$32.|DISPLAY_VALUE:$|DISPLAY_TYPE:$1.|RAW_VALUE_NUM|RAW_VALUE_CHAR:$5000|FORCED_VALUE|
  |---|---|---|---|
  |1|DISCOUNT_RT|"50%"|N|0.5||.|
  |1|DISCOUNT_RT|"40%"|N|0.4||0|
  |1|DISCOUNT_RT|"30%"|N|0.3||1|
  |1|CURRENCY_SYMBOL|"GBP"|C||"GBP"|.|
  |1|CURRENCY_SYMBOL|"RSD"|C||"RSD"|.|
  |2|DISCOUNT_RT|"50%"|N|0.5||.|
  |2|DISCOUNT_RT|"40%"|N|0.4||1|
  |2|CURRENCY_SYMBOL|"EUR"|C||"EUR"|.|
  |2|CURRENCY_SYMBOL|"HKD"|C||"HKD"|1|

  <h4> SAS Macros </h4>
  @li dc_getlibs.sas


**/

%mp_abort(iftrue= ("%upcase(&libds)" ne "&DC_LIBREF..MPE_ALERTS" )
  ,mac=&_program
  ,msg=%str(
    Invalid validation, expected MPE_ALERTS.ALERT_LIB, got %superq(libds)
  )
)

proc sql;
create table work.source as
  select libref,dsn
  from &DC_LIBREF..MPE_TABLES
  where tx_to > &dc_dttmtfmt.
  order by 1,2;

data work.DYNAMIC_VALUES (keep=display_index raw_value display_value);
  set work.source end=last;
  by libref;
  if last.libref then do;
    display_index+1;
    raw_value=libref;
    display_value=libref;
    output;
  end;
  if last then do;
    display_index+1;
    raw_value='*ALL*';
    display_value='*ALL*';
    output;
  end;
run;


data work.dynamic_extended_values(keep=display_index extra_col_name display_type
    display_value RAW_VALUE_CHAR raw_value_num forced_value);
  set work.source end=last;
  by libref dsn;
  retain extra_col_name 'ALERT_DS';
  retain display_type 'C';
  retain raw_value_num .;
  raw_value_char=dsn;
  display_value=dsn;
  forced_value=0;

  if first.libref then display_index+1;
  if last.libref then do;
    display_value='*ALL*';
    raw_value_char='*ALL*';
    forced_value=1;
    output;
  end;
  else output;

run;
