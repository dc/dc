/**
  @file
  @brief Generic validator for table columns
  @details The input table is simply one row from the target table in table
  called "work.source_row".

  Available macro variables:
    @li MPELIB - The DC control library
    @li LIBDS - The library.dataset being filtered
    @li VARIABLE_NM - The column being filtered

  <h4> Service Inputs </h4>
  <h5> work.sourcerow </h5>
  Has source table structure.

  <h4> Service Outputs </h4>

  The values provided below are generic samples - we encourage you to replace
  these with realistic values in your own deployments.

  <h5>DYNAMIC_VALUES</h5>
  The RAW_VALUE column may be charactor or numeric.  If DISPLAY_INDEX is not
  provided, it is added automatically.

  |DISPLAY_INDEX:best.|DISPLAY_VALUE:$|RAW_VALUE|
  |---|---|---|
  |1|$77.43|77.43|
  |2|$88.43|88.43|

  <h5>DYNAMIC_EXTENDED_VALUES</h5>
  This table is optional.  If provided, it will map the DISPLAY_INDEX from the
  DYNAMIC_VALUES table to additional column/value pairs, that will be used to
  populate dropdowns for _other_ cells in the _same_ row.

  Should be used sparingly!  The use of large tables here can slow down the
  browser.

  |DISPLAY_INDEX:best.|EXTRA_COL_NAME:$32.|DISPLAY_VALUE:$|DISPLAY_TYPE:$1.|RAW_VALUE_NUM|RAW_VALUE_CHAR:$5000|
  |---|---|---|
  |1|DISCOUNT_RT|"50%"|N|0.5||
  |1|DISCOUNT_RT|"40%"|N|0.4||
  |1|DISCOUNT_RT|"30%"|N|0.3||
  |1|CURRENCY_SYMBOL|"GBP"|C||"GBP"|
  |1|CURRENCY_SYMBOL|"RSD"|C||"RSD"|
  |2|DISCOUNT_RT|"50%"|N|0.5||
  |2|DISCOUNT_RT|"40%"|N|0.4||
  |2|CURRENCY_SYMBOL|"EUR"|C||"EUR"|
  |2|CURRENCY_SYMBOL|"HKD"|C||"HKD"|

  <h4> SAS Macros </h4>
  @li dc_assignlib.sas
  @li mf_getuniquename.sas
  @li mp_abort.sas
  @li mp_validatecol.sas


**/

/* send back the raw and formatted values */
%let tgtlibds=0;
%let varlibds=%mf_getuniquename();
%let vartgtlibds=%mf_getuniquename();
%let var_is_libds=%mf_getuniquename();

data _null_;
  length xl_libref base_lib select_lib rls_libref cls_libref libref $8
    xl_table base_ds select_ds rls_table cls_table dsn $32;
  if _n_=1 then call missing(of _all_);
  set work.source_row;
  &varlibds=symget('libds');
  if &varlibds="&mpelib..MPE_EXCEL_CONFIG"
  then &vartgtlibds=cats(xl_libref,'.',xl_table);
  else if &varlibds="&mpelib..MPE_VALIDATIONS"
  then &vartgtlibds=cats(BASE_LIB,'.',BASE_DS);
  else if &varlibds="&mpelib..MPE_SELECTBOX"
  then &vartgtlibds=cats(select_lib,'.',select_ds);
  else if &varlibds="&mpelib..MPE_ROW_LEVEL_SECURITY"
  then &vartgtlibds=cats(RLS_LIBREF,'.',RLS_TABLE);
  else if &varlibds="&mpelib..MPE_COLUMN_LEVEL_SECURITY"
  then &vartgtlibds=cats(CLS_LIBREF,'.',CLS_TABLE);
  else if &varlibds="&mpelib..MPE_TABLES"
  then &vartgtlibds=cats(LIBREF,'.',DSN);

  /* validate libds */
  %mp_validatecol(&vartgtlibds,LIBDS,&var_is_libds)

  if &var_is_libds=1 then call symputx('tgtlibds',&vartgtlibds);
  putlog (_all_)(=);
run;

%mp_abort(iftrue= ("&tgtlibds" ="0" )
  ,mac=&_program..sas
  ,msg=%str(Unable to extract libds vars from &libds inputs for &variable_nm)
)

%dc_assignlib(READ,%scan(&tgtlibds,1,.))

proc contents noprint data=&tgtlibds
  out=work.DYNAMIC_VALUES (keep=name rename=(name=display_value) );
run;

data work.DYNAMIC_VALUES;
  set work.DYNAMIC_VALUES;
  raw_value=upcase(display_value);
  format raw_value;
run;


