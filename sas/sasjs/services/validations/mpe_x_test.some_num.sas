/**
  @file
  @brief Generic validator for libraries
  @details The input table is simply one row from the target table in table
  called "work.source_row".

  Available macro variables:
    @li DC_LIBREF - The DC control library
    @li LIBDS - The library.dataset being filtered
    @li VARIABLE_NM - The column being filtered


  <h4> Service Outputs </h4>
  Output should be a single table called "work.dynamic_values" in the format
  below.  display_value should always be character, raw_value is unformatted
  character/numeric.

  |DISPLAY_VALUE:$|RAW_VALUE:??|
  |---|---|
  |$44.00|44|

  <h4> SAS Macros </h4>
  @li dc_getlibs.sas


**/

proc sql;
create table work.DYNAMIC_VALUES as
  select distinct cats(some_num) as display_value,
    some_num as raw_value
  from &libds
  order by 1;


