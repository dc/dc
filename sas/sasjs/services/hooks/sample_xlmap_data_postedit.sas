/**
  @file
  @brief Sample XLMAP Data hook program
  @details This hook script should NOT be modified in place, as the changes
    would be lost in your next Data Controller deployment.
    Instead, create a copy of this hook script and place it OUTSIDE the
    Data Controller metadata folder.

  Available macro variables:
    @li DC_LIBREF - The DC control library
    @li LIBREF - The library of the dataset being edited (is assigned)
    @li DS - The target dataset being loaded

**/

%let abort=0;
%let errmsg=;

data work.staging_ds;
  set work.staging_ds;
  length errmsg $1000;
  drop err:;
  /* KM1 validations */
  if XLMAP_ID='BASEL-KM1' then do;
    if XLMAP_RANGE_ID='KM1:a' & input(value_txt,8.)<100 then do;
      errmsg='Should be greater than 100';
      err=1;
    end;
  end;
  /* CR2 Validations */
  if XLMAP_ID='BASEL-CR2' then do;
    if XLMAP_RANGE_ID='CR2-sec1' & row_no=3 & input(value_txt,8.)>0 then do;
      errmsg='Should be negative';
      err=1;
    end;
  end;

  /* publish error message */
  if err=1 then do;
    errmsg=catx(' ',xlmap_range_id,':',value_txt,'->',errmsg);
    call symputx('errmsg',errmsg);
    call symputx('abort',1);
  end;
run;

%mp_abort(iftrue=(&abort ne 0)
  ,mac=xlmap_data_postedit
  ,msg=%superq(errmsg)
)
