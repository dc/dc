/**
  @file
  @brief Sample XLMAP Data hook program (sample_xlmap_data_postapprove)
  @details This hook script should NOT be modified in place, as the changes
    would be lost in your next Data Controller deployment.
    Instead, create a copy of this hook script and place it OUTSIDE the
    Data Controller metadata folder.

  Available macro variables:
    @li LOAD_REF - The Load Reference (unique upload id)
    @li ORIG_LIBDS - The target library.dataset that was just loaded

**/


data _null_;
  set work.staging_ds;
  putlog 'load ref is in the staged data: ' load_ref;
  stop;
run;

%put the unique identifier (LOAD_REF) is also a macro variable:  &LOAD_REF;