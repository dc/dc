/**
  @file
  @brief Post Edit Hook script for the MPE_COLUMN_LEVEL_SECURITY table
  @details Post edit hooks provide additional backend validation for user
  provided data.  The incoming dataset is named `work.staging_ds` and is
  provided in mpe_loader.sas.

  Available macro variables:
    @li DC_LIBREF - The DC control library
    @li LIBREF - The library of the dataset being edited (is assigned)
    @li DS - The dataset being edited

  This validation checks the incoming column_level_security settings to ensure
  each individual filter is valid


**/

/* check scope values and ensure uppercasing */
%let errflag=0;
%let errmsg=;
data work.staging_ds;
  set work.staging_ds;
  cls_scope=upcase(cls_scope);
  CLS_LIBREF=upcase(CLS_LIBREF);
  cls_table=upcase(CLS_TABLE);
  CLS_VARIABLE_NM=upcase(CLS_VARIABLE_NM);

  if cls_scope not in ('ALL','VIEW','EDIT') then do;
    call symputx('errflag',1);
    call symputx('errmsg',"Invalid scope: "!!cls_scope);
    stop;
  end;

  if cls_hide<1 then cls_hide=0;
  else cls_hide=1;
  if CLS_ACTIVE<1 then CLS_ACTIVE=0;
  else CLS_ACTIVE=1;

run;

%mp_abort(iftrue=(&errflag=1)
  ,mac=mpe_column_level_security_postedit
  ,msg=%superq(errmsg)
)
