/**
  @file
  @brief Post Edit Hook script for the MPE_XLMAP_RULES table
  @details Post edit hooks provide additional backend validation for user
  provided data.  The incoming dataset is named `work.staging_ds` and is
  provided in mpe_loader.sas.

  Available macro variables:
    @li DC_LIBREF - The DC control library
    @li LIBREF - The library of the dataset being edited (is assigned)
    @li DS - The dataset being edited


**/

data work.staging_ds;
  set work.staging_ds;

  /* ensure uppercasing */
  XLMAP_ID=upcase(XLMAP_ID);

run;

