/**
  @file
  @brief Post Edit Hook script for the MPE_SECURITY table
  @details Post edit hooks provide additional backend validation against
  user-sourced data.  The incoming dataset is always `work.staging_ds` and this
  file is included from the mpe_loader.sas macro.

  Available (at runtime) macro variables:
    @li DC_LIBREF - The DC control library for your site
    @li LIBREF - The library of the dataset being edited (is assigned)
    @li DS - The dataset being edited


**/

/* ensure upcase and check access level values*/
%let errval=0;
%let errmsg=;
data work.staging_ds;
  set work.staging_ds;
  LIBREF=upcase(LIBREF);
  DSN=upcase(DSN);
  ACCESS_LEVEL=upcase(ACCESS_LEVEL);
  if ACCESS_LEVEL not in ('EDIT','APPROVE','VIEW','SIGNOFF','AUDIT') then do;
    putlog "ERR" +(-1) "OR: invalid ACCESS_LEVEL - " access_level;
    call symputx('errval',1);
    call symputx('errmsg',"Invalid ACCESS_LEVEL: "!!access_level);
  end;
run;

%mp_abort(iftrue=(&errval=1)
  ,mac=mpe_security_postedit.sas
  ,msg=%str(&errmsg)
)