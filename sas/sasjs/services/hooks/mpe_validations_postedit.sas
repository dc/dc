/**
  @file
  @brief Post Edit Hook script for the MPE_VALIDATIONS table
  @details Post edit hooks provide additional backend validation for user
  provided data.  The incoming dataset is named `work.staging_ds` and is
  provided in mpe_loader.sas.

  Available macro variables:
    @li DC_LIBREF - The DC control library
    @li LIBREF - The library of the dataset being edited (is assigned)
    @li DS - The dataset being edited

  This validation checks the incoming mpe_validations settings to ensure
  there are no columns that have both HARDSELECT_HOOK and SOFTSELECT_HOOK.

  <h4> SAS Macros </h4>
  @li mf_nobs.sas

  <h4> Related Macros </h4>
  @li mpe_loader.sas

**/


/** check to avoid a colum having both HARDSELECT_HOOK and SOFTSELECT_HOOK */
/* need to merge with base table in the case of a single row being added */
%global src_list1 src_list2;
%let src_list1='';
proc sql noprint;
create table work.check1 as
  select quote(catx('.',base_lib,base_ds,base_col)) as source
    ,rule_type
  from work.staging_ds
  where rule_type in ('SOFTSELECT_HOOK','HARDSELECT_HOOK')
  and upcase(_____DELETE__THIS__RECORD_____) ne "YES";

select distinct cats(source) into: src_list1 separated by ','
  from work.check1;

create table work.check2 as
  select quote(catx('.',base_lib,base_ds,base_col)) as source
    ,rule_type
  from &DC_LIBREF..MPE_VALIDATIONS
  where rule_type in ('SOFTSELECT_HOOK','HARDSELECT_HOOK')
    and &dc_dttmtfmt. lt tx_to
    and catx('.',base_lib,base_ds,base_col) in (&src_list1);

create table work.check3 as
  select * from work.check1
union
  select * from work.check2;

create table work.validation_checker as
  select source
    ,count(*) as cnt
  from work.check3
  group by 1
  having cnt>1;

select distinct source into: src_list2 from work.validation_checker;

data _null_;
  set work.validation_checker;
  putlog (_all_)(=);
run;


%mp_abort(iftrue= (%mf_nobs(work.validation_checker)>0)
  ,mac=mpe_validations_postedit
  ,msg=%str(The following vars have duplicate HOOKS:  &src_list2)
)

