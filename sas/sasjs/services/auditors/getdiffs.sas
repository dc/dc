/**
  @file getdiffs.sas
  @brief Retrieves the diff file for viewing
  @details

  <h4> SAS Macros </h4>
  @li mpe_getvars.sas
  @li mpe_accesscheck.sas
  @li mf_getattrn.sas
  @li mp_abort.sas
  @li mp_binarycopy.sas
  @li mp_streamfile.sas

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/

%mpeinit()
%mpe_getvars(BrowserParams, BrowserParams);

/* security checks */
%let user=%mf_getuser();
%mpe_accesscheck(&libds,outds=authEDIT,user=&user,access_level=EDIT)
%mpe_accesscheck(&libds,outds=authAPP,user=&user,access_level=APPROVE)

%macro mpestp_diffs();
  %if %mf_getattrn(work.authEDIT,NLOBS)=0 & %mf_getattrn(work.authAPP,NLOBS)=0
  %then %do;
    %mp_abort(msg=%str(
        &user not authorised to download diffs data for &stp_table)
      ,mac=mpestp_diffs.sas);
    %return;
  %end;

  %mp_abort(iftrue= (&syscc ne 0)
    ,mac=&_program..sas
    ,msg=%str(syscc=&syscc)
  )

  /* stream diffs csv to client */
  %mp_streamfile(contenttype=EXCEL
    ,inloc=%str(&mpelocapprovals/&TABLE/&STP_DIFFS_CSV)
    ,outname=&STP_DIFFS_CSV
  )

%mend mpestp_diffs;
%mpestp_diffs()



%mpeterm()
