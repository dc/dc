/**
  @file getstagetable.sas
  @brief Retrieves the actual table that is being sent for update
  @details

  <h4> SAS Macros </h4>
  @li mf_getvalue.sas
  @li mp_abort.sas

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/
%mpeinit()

%let table_id=%mf_getvalue(work.iwant,table_id);
libname loc "&mpelocapprovals/&table_id";
data stagetable;
  set loc.&table_id;
run;

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program..sas
  ,msg=%str(syscc=&syscc)
)

%webout(OPEN)
%webout(OBJ,stagetable,missing=STRING)
%webout(CLOSE)


%mpeterm()
