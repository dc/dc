/**
  @file
  @brief testing postdata with format table

  <h4> SAS Macros </h4>
  @li mp_testservice.sas
  @li mp_assert.sas


**/


/**
  * First part - stage some data (for diffing)
  */
%let _program=&appLoc/services/editors/stagedata;

data work.sascontroltable;
  action='LOAD';
  MESSAGE='Testing upload of a format catalog';
  LIBDS="DCTEST.DCFMTS-FC";
run;

proc format lib=DCTEST.DCFMTS cntlout=work.fmtextract;
run;
data work.jsdata;
  set work.fmtextract;
  if _n_<5 then _____DELETE__THIS__RECORD_____='Yes';
  else _____DELETE__THIS__RECORD_____='No';
  if _n_>12 then label=cats('new!',label);
  if _n_>20 then stop;
run;

%mp_testservice(&_program,
  viyacontext=&defaultcontext,
  inputdatasets=work.sascontroltable work.jsdata,
  outlib=web1,
  mdebug=&sasjs_mdebug
)

%let status=0;
data work.sasparams;
  set web1.sasparams;
  putlog (_all_)(=);
  if status='SUCCESS' then call symputx('status',1);
  call symputx('dsid',dsid);
run;

%mp_assert(
  iftrue=(&status=1 and &syscc=0),
  desc=Checking successful submission
)

/**
  * Now run postdata with SHOW_DIFFS
  */
data work.sascontroltable;
  ACTION='SHOW_DIFFS';
  TABLE="&dsid";
  DIFFTIME="%sysfunc(datetime(),B8601DT19.3)";
  output;
  stop;
run;
%mp_testservice(&appLoc/services/auditors/postdata,
  viyacontext=&defaultcontext,
  inputdatasets=work.sascontroltable,
  outlib=web2,
  outref=wbout
)


data _null_;
  infile wbout;
  input;
  putlog _infile_;
run;

