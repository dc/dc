/**
  @file buildtermsas9.sas
  @brief Creates the library and staging area etc
  @details Creates a unique library, staging area, initial settings stp etc.

  The DCPATH should be configured in the tgtBuildVars attribute.


  <h4> SAS Macros </h4>
  @li dc_assignlib.sas
  @li mf_deletefile.sas
  @li mf_getapploc.sas
  @li mf_mkdir.sas
  @li mm_createdocument.sas
  @li mm_createlibrary.sas
  @li mm_createstp.sas
  @li mm_deletedocument.sas
  @li mm_deletestp.sas
  @li mm_spkexport.sas
  @li mp_abort.sas


  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/

%global dcpath;

%let dclib=%upcase(DC%substr(%sysevalf(%sysfunc(datetime())/60),3,6));
%let dclibname=Data Controller(&dclib);
%let work=%sysfunc(pathname(work));
%let dcpath=/tmp/dcsas9/&dclib;
%let apploc=%mf_getapploc();

%mf_mkdir(&dcpath)
%mf_mkdir(&work)
%put &=dcpath;

/* check we have physical permissions to the DCLIB folder */
data _null_;
  file "&dcpath/permTest.txt";
  put "something";
run;
%mp_abort(iftrue= (&syscc ne 0)
  ,mac=buildtermsas9
  ,msg=%str(
    User &sysuserid needs WRITE permissions on physical directory: &dcpath
  )
)
%mp_abort(iftrue= (%sysfunc(fileexist(&dcpath/permTest.txt)) ne 1)
  ,mac=buildtermsas9
  ,msg=%str(
    User &sysuserid could not create &dcpath/permTest.txt
  )
)
filename delfile "&dcpath/permTest.txt";
data _null_;
  rc=fdelete('delfile');
run;
%mp_abort(iftrue= (&syscc ne 0)
  ,mac=buildtermsas9
  ,msg=%str(User &sysuserid could create (but not delete) &dcpath/permTest.txt )
)

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=buildtermsas9
  ,msg=%str(Unable to write to &dcpath)
)

/* check we have the admin rights to update the items in the Admin folder */
%mm_createdocument(tree=&appLoc/services/admin,name=permTest)
%mm_deletedocument(target=&appLoc/services/admin/permTest)

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=buildtermsas9
  ,msg=%str(
    User &_metaperson does not have WriteMetadata on SAS folder: &appLoc
  )
)

/**
  * Create library and load data
  */
%let mpelibname=Data Controller (&dclib);
%mm_createlibrary(
  libname=&mpelibname
  ,libref=&dclib
  ,libdesc=Configuration tables for the MacroPeople Data Controller application
  ,engine=BASE
  ,tree=&appLoc/data
  ,servercontext=SASApp
  ,directory=&dcpath
  ,mDebug=1
)
%mp_abort(iftrue= (&syscc ne 0)
  ,mac=buildtermsas9
  ,msg=%str(Unable to create &dclib library)
)

/* get direct libref */
%dc_assignlib(READ,&dclib)

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=buildtermsas9
  ,msg=%str(Unable to assign &dclib library)
)

/* create an initial settings service (this will be overwritten in the
  data update step)
*/
%let temploc=&work/temp.txt;
data _null_;
  file "&temploc" ;
  put '/* Data Controller Precode */' / / ;
  put ' ';
  put 'options noquotelenmax ps=max;';
  put '%global DC_LIBREF DC_LIBNAME DC_LIBLOC DC_STAGING_AREA DC_ADMIN_GROUP;';
  put ' ';
  put '/* This metadata library (libref) contains control datasets for DC */';
  put '/* If a different libref must be used, configure it below */';
  put '%let DC_LIBREF=' "&dclib;";
  put '%let DC_LIBNAME=' "&mpelibname;";
  put ' ';
  put '/* get physical path for direct libname - needed to track requests */';
  put 'data _null_;';
  put '  length lib_uri up_uri path $256;';
  put '  call missing (of _all_);';
  put '  rc=metadata_getnobj("omsobj:SASLibrary?@Libref =''&dc_libref''",1,lib_uri);';
  put '  rc=metadata_getnasn(lib_uri,"UsingPackages",1,up_uri);';
  put '  rc=metadata_getattr(up_uri,"DirectoryName",path);';
  put '  call symputx("dc_libloc",path);';
  put 'run;';
  put 'libname &DC_LIBREF "&dc_libloc";';
  put ' ';
run;

%mm_deletestp(target=&appLoc/services/public/Data_Controller_Settings)

%mm_createstp(stpname=Data_Controller_Settings
  ,filename=temp.txt
  ,directory=&work
  ,tree=&appLoc/services/public
  ,Server=SASApp
  ,stptype=2
  ,mdebug=1
  ,stpdesc=Data Controller Configuration
  ,minify=NO
)
%mp_abort(iftrue= (&syscc ne 0)
  ,mac=buildtermsas9
  ,msg=%str(Issue creating settings stp)
)

/* create script for exporting SPK */
%let spkfile=/tmp/dcspk.sh;

%mf_deletefile(&spkfile)

filename myref "&spkfile" permission='A::u::rwx,A::g::r-x,A::o::---';
%let ignore1=tests;
%let ignore2=%scan(&_program,-1,/);
%mm_spkexport(metaloc=%str(&apploc)
  ,outref=myref
  ,excludevars=ignore1 ignore2
  ,cmdoutloc=%str(/tmp)
  ,cmdoutname=dc
)

data _null_;
  file _webout;
  put "<p> Library &dclib successfully assigned to &dcpath on ";
  put "%sysfunc(datetime(),datetime19.)</p>";
  put "<p> Now <a href='&_url?_program=&apploc/services/admin/configurator'>Configurate!</a></p>";
  put "Or skip it and <a href=";
  put "'&_url?_program=&apploc/services/admin/makedata%str(&)admin=sec-sas9-prd-ext-%trim(
    )sasplatform-300114sasjs%str(&)repo=foundation'";
  put "> make tables </a></p>";
run;


%mp_abort(iftrue= (&syscc ne 0)
  ,mac=buildtermsas9
  ,msg=%str(Some final error in &_program )
)
/* delete if successful */
%mm_deletestp(target=&_program)
