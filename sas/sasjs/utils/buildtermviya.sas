/**
  @file buildviyaterm.sas
  @brief build term script - prepare to call service
  @details services have been built, now to build the DB.

  <h4> SAS Macros </h4>
  @li mx_testservice.sas

  @version 3.5
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/

/* launch makedata with provided params */
%global dcpath adminGroup;
data work.params;
  length name $6;
  name='ADMIN';
  value=coalescec("&adminGroup","SASAdministrators");
  output;
  name='DCPATH';
  value=coalescec("&dcpath","/tmp/dc");
  output;
run;

%mx_testservice(&appLoc/services/admin/makedata,
  inputparams=work.params,
  outlib=webout,
  debug=log
)

data _null_;
  if symexist('_baseurl') then do;
    url=symget('_baseurl');
    if subpad(url,length(url)-9,9)='SASStudio'
      then url=substr(url,1,length(url)-11);
    else url="&systcpiphostname";
  end;
  else url="&systcpiphostname";
  call symputx('url',url);
run;

%put now call configurator:;
%put x;
%put x;
%put x http://&url/SASJobExecution?_program=&appLoc/services/clickme;
%put x;
%put x;
