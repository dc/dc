#!/usr/bin/env bash
####################################################################
# PROJECT: Data Controller                                         #
# The SAS project should be checked out from
# git@gitlab.com:macropeople/dcfrontend.git
####################################################################

cd ~/git/data-controller/sas
echo "copying viya deploy script up to sas.4gl.io:/tmp/dcviya/buildviya.sas"
scp sasjsbuild/myviyadeploy.sas allbow@sas.4gl.io:/tmp/dcviya/buildviya.sas

mkdir ../client/dist/sasbuild
cp sasjsbuild/viya.json ../client/dist/sasbuild

rsync -avhe ssh ../client/dist/* --delete allbow@sas.4gl.io:/var/www/html/dc

echo "Now open: https://sas.4gl.io/dc"
