/**
  @file serviceinit.sas
  @brief this file is called with every service
  @details  This file is included in *every* service, *after* the macros and
    *before* the service code.

  <h4> SAS Macros </h4>
  @li mpeinit.sas
  @li mpeterm.sas

**/

options noquotelenmax ps=max;

cas dcsession sessopts=(caslib=casuser);
caslib _all_ assign;

libname casuser cas caslib=casuser;


/*caslib casmusic path='/opt/sas/viya/cascache/tracks' libref=casmusic ;*/

%let syscc=0;

%put _global_;