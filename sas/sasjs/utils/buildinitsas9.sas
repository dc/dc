/**
  @file
  @brief Initialise build program for SAS 9 DC

  <h4> SAS Macros </h4>
**/
options nomprint;

%global _metaperson _url dcpath;

/* set webout if not running in STP mode */
data _null_;
  if "&sysprocessmode" ne "SAS Stored Process Server" then do;
    call execute('filename _webout temp;');
  end;
run;