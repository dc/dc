/**
  @file
  @brief this file is called with every service
  @details  This file is included in *every* service, *after* the macros and
    *before* the service code.

  <h4> SAS Macros </h4>
  @li mpeinit.sas
  @li mpeterm.sas

**/

options noquotelenmax ps=max nobomfile;

