/**
  @file serviceinit.sas
  @brief this file is called with every service
  @details  This file is included in *every* service, *after* the macros and
    *before* the service code.

  <h4> SAS Macros </h4>
  @li mpeinit.sas
  @li mpeterm.sas

**/

options noquotelenmax ps=max;

/* create dummy macros to prevent stpbegin / stpend from corrupting sessions */

%macro stpbegin();
%put NOTE: the STPBEGIN macro should not be used for web apps!;
%mend stpbegin;

%macro stpend();
%put NOTE: the STPEND macro should not be used for web apps!;
%mend stpend;