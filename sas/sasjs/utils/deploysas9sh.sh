#!/usr/bin/env bash
####################################################################
# PROJECT: Data Controller                                         #
# The SAS project should be checked out from
# git@gitlab.com:macropeople/dcfrontend.git
####################################################################
cd ~/git/datacontroller/sas/sasjs/utils
# appref should also be manully updated in the delete step below
APPREF="build2"
ADMINGROUP="sec-sas9-prd-ext-sasplatform-300114sasjs"
ADMINGROUP="dc-admin"


BUILDSERVER="http://sas.4gl.io:7980"
BUILDSERVER="https://sas.4gl.io:8343" # without trailing slash

STPSVR="$BUILDSERVER/SASStoredProcess/do"
BUILDSTP="/User%20Folders/allbow/My%20Folder/testJob"
APPROOT="/30.SASApps/3030.Projects/303001.DataController"

#  GET CREDENTIALS
#read -s -p "enter creds? " -i "N" -e answer
#echo "you answered: $answer"
if [ "0" != "0" ] # [ "$answer" != "N" ]
then
    # get SAS creds for running the build STP
    echo -n What is your SAS username? :
    read USER
    echo -n What is your SAS password? :
    stty -echo
    read PASS
    stty echo
    echo "username=$USER&password=$PASS">~/.ssh/sascred
    echo "_username=$USER&_password=$PASS">~/.ssh/sascredurl
fi
CREDS=$(cat ~/.ssh/sascredurl)
STPURI="&STPSVR?_program=$BUILDSTP"

mkdir -p /tmp/dc
cd ../..

echo current directory: $(pwd)


echo "copying deploy script up"
scp sasjsbuild/mysas9deploy.sas allbow@sas.4gl.io:/tmp/mysas9deploy.sas

# pass the folder to be DELETED here (will come from the root below)
# /30.Projects/3001.Internal/300115.DataController/dc
echo "SASJS: deleting old metadata folder"
ssh allbow@sas.4gl.io <<'ENDSSH'
    ./delfolder.sh build2 > ~/delfolder.txt
ENDSSH
scp allbow@sas.4gl.io:delfolder.txt /tmp/dc/delfolder.txt
cat /tmp/dc/delfolder.txt


##!/usr/bin/env bash
#: ${1?Need a value}
#cd "/opt/sas/sas9/SASHome/SASPlatformObjectFramework/9.4/tools"
#./sas-delete-objects -host sas.4gl.io  -port 8561 -user 'allbow' -password 'xxx!' \
#      "/30.Projects/3001.Internal/300115.DataController/$1/DataController"  -deleteContents 2>&1

# Create all the SAS services and the SPK shell script
echo "SASJS: now running $STPSVR?_program=$BUILDSTP"

curl -v -L -k  -b cookiefile -c cookiefile "$STPSVR&$CREDS" # dummy request to populate cookies
curl -v -L -k  -c cookiefile -b cookiefile  "$STPSVR?_program=$BUILDSTP&$CREDS"

# Run the spkexport shell script

#cd /tmp/dc
#./dcspk.sh USER PASS
#cp /tmp/dc/dc.spk /var/www/html/dc
echo "SASJS: executing dcspk.log"
ssh allbow@sas.4gl.io <<'ENDSSH'
    ./spkexport.sh > /tmp/dcspk.log
ENDSSH
scp allbow@sas.4gl.io:/tmp/dc.spk /tmp/dc.spk
scp allbow@sas.4gl.io:/tmp/dc.log /tmp/dc.log
cat /tmp/dc/dc.log


# Make the tables and configurate with Admin group
BASEURL="$STPSVR?_program=$APPROOT/$APPREF/DataController"
URL="$BASEURL/services/admin/makedata&admin=$ADMINGROUP&repo=foundation&_debug=2477"

echo "SASJS: now running $URL"

curl -v -L -k  -b cookiefile -c cookiefile "$URL&$CREDS"

URL2="$BASEURL/services/admin/refreshcatalog"
curl -v -L -k  -b cookiefile -c cookiefile "$URL2&$CREDS"

URL3="$BASEURL/services/admin/refreshtablelineage"
curl -v -L -k  -b cookiefile -c cookiefile "$URL3&$CREDS"

echo "SASJS: Make Catalog:  $URL2";
echo "SASJS: Refresh Table Lineage:  $URL3";
echo "SASJS: DC is ready:  $STPSVR?_program=$APPROOT/$APPREF/DataController/services/clickme"
