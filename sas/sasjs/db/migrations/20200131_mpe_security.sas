/**
  @file
**/
libname DC "/YOUR/DC/LIB";

proc sql;
create table backup as select * from dc.mpe_security;

proc sql;
alter table dc.mpe_security
  add libref char(8), dsn char(32);
update dc.mpe_security
  set libref=put(scan(base_table,1,'.'),$8.)
    , dsn=put(scan(base_table,2,'.'),$8.);
drop index pk from dc.mpe_security;
alter table dc.mpe_security drop base_table;
create unique index PK on dc.mpe_security
  (libref,dsn,access_level,sas_group,tx_from);

/**
  THERE IS NOW A MANUAL STEP - open MPE_TABLES
  and change the MPE_SECURITY entry:
    - FROM:  BASE_TABLE ACCESS_LEVEL SAS_GROUP
    - TO:  LIBREF DSN ACCESS_LEVEL SAS_GROUP
**/
