/**
  @file
**/

proc sql;
create table &curlib..mpe_groups(
      tx_from num not null,
      group_name char(100) not null,
      group_desc char(256),
      user_name char(50) not null,
      tx_to num not null,
  constraint pk
      primary key(tx_from,group_name,user_name));

insert into &syslast set
    tx_from=0
    ,group_name="DC Demo Group"
    ,group_desc="Custom Group for Data Controller Purposes"
    ,user_name="allbow"
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &syslast set
    tx_from=0
    ,group_name="DC Demo Group"
    ,group_desc="Custom Group for Data Controller Purposes"
    ,user_name="mobeen@saspw"
    ,tx_to='31DEC5999:23:59:59'dt;