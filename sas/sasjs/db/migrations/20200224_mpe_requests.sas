/**
  @file
**/


libname DC "/YOUR/DC/LIB";

  /* no PK defined as it is a transaction table */
proc sql;
  create table dc.mpe_requests(
    request_dttm num not null  format=datetime19.,
    request_user char(64) not null,
    request_service char(64) not null,
    request_params char(128)
  );

/* ALSO - make sure there is a DIRECT libname assigned in the
data_controller_settings stp */