/**
  @file
**/
libname DC "/YOUR/DC/LIB";

proc sql;
create table DC.MPE_VALIDATIONS
(
    TX_FROM float format=datetime19.,
    BASE_LIB varchar(8),
    BASE_DS varchar(32),
    BASE_COL varchar(32),
    RULE_TYPE varchar(32),
    RULE_VALUE varchar(128),
    RULE_ACTIVE int ,
    TX_TO float not null format=datetime19.,
    constraint pk
        primary key(tx_from, base_lib,base_ds,base_col,rule_type));

create table DC.mpe_datadictionary
(
    TX_FROM float format=datetime19.,
    DD_TYPE char(16),
    DD_SOURCE char(2048),
    DD_SHORTDESC char(256),
    DD_LONGDESC char(32767),
    DD_OWNER char(128),
    DD_RESPONSIBLE char(128),
    DD_SENSITIVITY char(64),
    TX_TO float not null format=datetime19.,
    constraint pk
        primary key(tx_from, dd_type,dd_source));