/**
  @file
**/

libname DC "/YOUR/DC/LIB";

/* incorrect format was applied (date.) */
proc datasets lib=DC;
  MODIFY mpe_review;
  FORMAT reviewed_on_dttm datetime19.;
quit;

proc sql;
alter table dc.mpe_config add var_active num;

update dc.mpe_config set var_active=1;