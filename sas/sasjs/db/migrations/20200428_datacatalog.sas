/**
  @file
**/
libname DC "/YOUR/DC/LIB";
proc sql;
create table dc.mpe_datacatalog_libs(
    TX_FROM float format=datetime19.,
    TX_TO float format=datetime19.,
    libref char(8) label='Library Ref',
    engine char(32) label='Library Engine',
    libname char(256) format=$256. label='Library Name',
    paths char(8192) label='Library Paths',
    perms char(500) label='Library Permissions (if BASE)',
    owners char(500) label='Library Owners (if BASE)',
    schemas char(500) label='Library Schemas (if DB)',
    libid char(17) label='LibraryId',
  constraint pk
      primary key(libref,tx_to));

create table dc.mpe_datacatalog_TABS(
    TX_FROM float format=datetime19.,
    TX_TO float format=datetime19.,
    libref char(8) label='Library Name',
    dsn char(64) label='Member Name',
    memtype char(8) label='Member Type',
    dbms_memtype char(32) label='DBMS Member Type',
    memlabel char(512) label='Data Set Label',
    typemem char(8) label='Data Set Type',
    nvar num label='Number of Variables',
    compress char(8) label='Compression Routine',
    pk_fields char(512)
      label='Primary Key Fields (identified by being in a constraint that is both Unique and Not Null)',
  constraint pk
      primary key(libref,dsn,tx_to));


create table dc.mpe_datacatalog_vars(
    TX_FROM float format=datetime19.,
    TX_TO float format=datetime19.,
    libref char(8) label='Library Name',
    dsn char(64) label='Table Name',
    memtype char(8) label='Member Type',
    name char(64) label='Column Name',
    type char(16) label='Column Type',
    length num label='Column Length',
    varnum num label='Column Number in Table',
    label char(512) label='Column Label',
    format char(49) label='Column Format',
    idxusage char(9) label='Column Index Type',
    notnull char(3) label='Not NULL?',
    pk_ind num label="Primary Key Indicator (1=Primary Key field)",
  constraint pk
      primary key(libref,dsn,name,tx_to));

  create table dc.mpe_datastatus_libs(
    TX_FROM float format=datetime19.,
    TX_TO float format=datetime19.,
    libref char(8) label='Library Name',
    libsize num format=SIZEKMG. label='Size of file',
    table_cnt num label='Number of Tables',
  constraint pk
      primary key(libref,tx_to));

  create table dc.mpe_datastatus_tabs(
    TX_FROM float format=datetime19.,
    TX_TO float format=datetime19.,
    libref char(8) label='Library Name',
    dsn char(64) label='Member Name',
    filesize num format=SIZEKMG. label='Size of file',
    crdate num format=DATETIME. informat=DATETIME. label='Date Created',
    modate num format=DATETIME. informat=DATETIME. label='Date Modified',
    nobs num label='Number of Physical (Actual, inc. deleted) Observations',
  constraint pk
      primary key(libref,dsn,tx_to));