/**
  @file
**/
libname DC "/YOUR/DC/LIB";


proc sql;
alter table DC . MPE_DATADICTIONARY drop constraint pk ;
alter table dc.mpe_datadictionary modify DD_SOURCE char(1024);
alter table DC . MPE_DATADICTIONARY
  add constraint pk_mpe_datadictionary PRIMARY KEY (
    TX_FROM, DD_SOURCE, DD_TYPE);

