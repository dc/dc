/**
  @file
  @brief migration script to move from v6.5 to 6.8.2 of data controller

**/

%let dclib=YOURDCLIB;

libname &dclib "/your/dc/path";

/**
  * Change 1
  * New MPE_SUBMIT table
  */
proc sql;
insert into &dclib..mpe_config set
    tx_from=0
    ,tx_to='31DEC9999:23:59:59'dt
    ,var_scope="DC"
    ,var_name="DC_REQUEST_LOGS"
    ,var_value="YES"
    ,var_active=1
    ,var_desc='Setting to NO will prevent each request being logged to the'
      !!' MPE_REQUESTS table  Default=YES.';