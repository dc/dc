/**
  @file
  @brief migration script to move from v3.12 to v3.13 of data controller


  <h4> SAS Macros </h4>
  @li mm_assigndirectlib.sas

**/

filename mc url "https://raw.githubusercontent.com/sasjs/core/main/all.sas";
%inc mc;

%let dclib=YOURDCLIB;
%let lib=&dclib;

%mm_assigndirectlib(&dclib)

proc sql;
update &dclib..MPE_VALIDATIONS set tx_to=%sysfunc(datetime())
  where base_lib="&dclib"
    and base_ds="MPE_TABLES"
    and base_col="DSN";
insert into &dclib..MPE_VALIDATIONS set tx_from=%sysfunc(datetime())
  ,tx_to='31DEC5999:23:59:59'dt
  ,base_lib="&dclib"
  ,base_ds="MPE_TABLES"
  ,base_col="DSN";
  ,rule_value="services/validations/mpe_tables.dsn"
  ,rule_active=1
  ,rule_type='SOFTSELECT_HOOK';