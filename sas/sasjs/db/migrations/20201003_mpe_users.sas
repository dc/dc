/**
  @file
**/
libname DC "/YOUR/DC/LIB";


proc sql;
create table dc.mpe_users(
        user_id char(50) not null,
        last_seen_dt num not null format=date9.,
        registered_dt num not null format=date9.,
    constraint pk_mpe_users
        primary key(user_id));

