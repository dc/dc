/**
  @file
  @brief ddl file
  @details


  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

proc sql;
create table &curlib..dim_product(
        tx_from num not null format=datetime19.3,
        tx_to num not null format=datetime19.3,
        product_cd char(40) not null,
        product_desc char(100) not null,
    constraint pk
        primary key(tx_from, product_cd));

insert into &syslast
  set tx_from=&low_date,tx_to=&high_date
    ,product_cd='WIDG'
    ,product_desc='The most amazing widget';
insert into &syslast
  set tx_from=&low_date,tx_to=&high_date
    ,product_cd='FLFY'
    ,product_desc='A fluffy bear';
insert into &syslast
  set tx_from=&low_date,tx_to=&high_date
    ,product_cd='DRNE'
    ,product_desc='Technological Drone Marvel';
insert into &syslast
  set tx_from=&low_date,tx_to=&high_date
    ,product_cd='BGGY'
    ,product_desc='Powerful Sand Buggy';
insert into &syslast
  set tx_from=&low_date,tx_to=&high_date
    ,product_cd='RBOT'
    ,product_desc='Deadly Robot';
