/**
  @file
  @brief ddl file
  @details


  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/


proc sql;
create table &curlib..mpe_security(
        tx_from num not null,
        libref char(8) not null,
        dsn char(32) not null,
        access_level char(10) not null,
        sas_group char(100) not null,
        tx_to num not null,
    constraint pk_mpe_security
        primary key(libref,dsn,access_level,sas_group,tx_from));
