/**
  @file
  @brief ddl file
  @details


  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/

proc sql;
create table &curlib..mpe_signoffs(
        tech_from_dttm num not null,
        signoff_table varchar(50) not null,
        signoff_section_rk num not null,
        signoff_version_rk num not null,
        signoff_name varchar(100) not null,
        tech_to_dttm num not null,
    constraint pk_mpe_signoffs
        primary key(tech_from_dttm, signoff_table, signoff_section_rk));

