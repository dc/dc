/**
  @file
  @brief DDL for mpe_xlmap_rules

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/

create table &curlib..mpe_xlmap_rules(
        tx_from num not null,
        XLMAP_ID char(32) not null,
        XLMAP_RANGE_ID char(32) not null,
        XLMAP_SHEET char(32) not null,
        XLMAP_START char(1000) not null,
        XLMAP_FINISH char(1000),
        tx_to num not null,
    constraint pk_mpe_xlmap_rules
        primary key(tx_from,XLMAP_ID,XLMAP_RANGE_ID));
