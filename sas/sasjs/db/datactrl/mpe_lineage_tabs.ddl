/**
  @file mpe_lineage_tabs.ddl
  @brief ddl file
  @details


  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/

proc sql;
create table &curlib..MPE_LINEAGE_TABS
 (
    tx_from num not null,
    jobid char(17),
    jobname char(128),
    srctableid char(17),
    srctabletype char(16),
    srctablename char(64),
    srclibref char(8),
    tgttableid char(17),
    tgttabletype char(16),
    tgttablename char(64),
    tgtlibref char(8),
    tx_to num not null,
    constraint pk_MPE_LINEAGE_TABS primary key
      (
        tx_to,jobid,srctableid,tgttableid
      )
  );
