/**
  @file
  @brief DDL for MPE_XLMAP_DATA

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/

create table &curlib..MPE_XLMAP_DATA(
        LOAD_REF char(32) not null,
        XLMAP_ID char(32) not null,
        XLMAP_RANGE_ID char(32) not null,
        ROW_NO num not null,
        COL_NO num not null,
        VALUE_TXT char(4000),
    constraint pk_MPE_XLMAP_DATA
      primary key(LOAD_REF, XLMAP_ID, XLMAP_RANGE_ID, ROW_NO, COL_NO));
