/**
  @file
  @brief DDL for Test Table
  @details


  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/


  create table &curlib..mpe_x_test(
          primary_key_field num,
          some_char char(32767) ,
          some_dropdown char(128),
          some_num num ,
          some_date num  format=date9.,
          some_datetime num  format=datetime19. informat=ANYDTDTM19.,
          some_time num format=time8.,
          some_shortnum num length=4,
          some_bestnum num format=best.,
      constraint pk_mpe_x_test
          primary key(primary_key_field));
