/**
  @file
  @brief ddl file
  @details


  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/

proc sql;
create table &curlib..mpe_dataloads(
        libref varchar(8) not null,
        dsn varchar(32) not null,
        etlsource varchar(100) not null,
        loadtype varchar(20) not null,
        changed_records int,
        new_records int,
        deleted_records int,
        duration num,
        user_nm varchar(50) not null,
        processed_dttm num format=datetime19.3,
        mac_ver varchar(5),
    constraint pk_mpe_dataloads
        primary key(processed_dttm, libref,dsn,etlsource));
