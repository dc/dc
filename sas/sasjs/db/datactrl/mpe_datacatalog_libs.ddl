/**
  @file mpe_datacatalog_libs.ddl
  @brief ddl file
  @details

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/

create table &curlib..mpe_datacatalog_libs(
    TX_FROM float format=datetime19.,
    TX_TO float format=datetime19.,
    libref char(8) label='Library Ref',
    engine char(32) label='Library Engine',
    libname char(256) format=$256. label='Library Name',
    paths char(8192) label='Library Paths',
    perms char(500) label='Library Permissions (if BASE)',
    owners char(500) label='Library Owners (if BASE)',
    schemas char(500) label='Library Schemas (if DB)',
    libid char(17) label='LibraryId',
  constraint pk_mpe_datacatalog_libs
      primary key(libref,tx_to));
