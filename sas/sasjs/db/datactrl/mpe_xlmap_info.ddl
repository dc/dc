/**
  @file
  @brief DDL for mpe_xlmap_info

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/

create table &curlib..mpe_xlmap_info(
        tx_from num not null,
        XLMAP_ID char(32) not null,
        XLMAP_DESCRIPTION char(1000) not null,
        XLMAP_TARGETLIBDS char(41) not null,
        tx_to num not null,
    constraint pk_mpe_xlmap_info
        primary key(tx_from,XLMAP_ID));
