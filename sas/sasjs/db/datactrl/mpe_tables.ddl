/**
  @file
  @brief ddl file
  @details


  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/

proc sql;
create table &curlib..mpe_tables(
        tx_from num not null format=datetime19.3,
        tx_to num not null format=datetime19.3,
        libref char(8) not null,
        dsn char(32) not null,
        num_of_approvals_required int,
        loadtype char(12) ,
        buskey char(1000) ,
        var_txfrom char(32) ,
        var_txto char(32) ,
        var_busfrom char(32) ,
        var_busto char(32) ,
        var_processed char(32) ,
        close_vars varchar(500),
        pre_edit_hook char(200),
        post_edit_hook char(200),
        pre_approve_hook char(200) ,
        post_approve_hook char(200) ,
        signoff_cols varchar(500),
        signoff_hook varchar(200),
        notes char(1000) ,
        rk_underlying char(1000) ,
        audit_libds char(41),
    constraint pk_mpe_tables
        primary key(tx_from, libref, dsn));

insert into &syslast
  set tx_from=0
    ,tx_to='31DEC5999:23:59:59'dt
    ,libref="&mpelib"
    ,dsn='MPE_LOCKANYTABLE'
    ,num_of_approvals_required=1
    ,loadtype='UPDATE'
    ,buskey='LOCK_LIB LOCK_DS'
    ,notes='Should not normally be edited, means a process failed and was left hung'
  ;

insert into &syslast
  set tx_from=0
    ,tx_to='31DEC5999:23:59:59'dt
    ,libref="&mpelib"
    ,dsn='MPE_TABLES'
    ,num_of_approvals_required=1
    ,loadtype='TXTEMPORAL'
    ,buskey='LIBREF DSN'
    ,var_txfrom='TX_FROM'
    ,var_txto='TX_TO'
    ,notes='This entry allows the MP Editor to edit itself!'
  ;
insert into &syslast
  set tx_from=0
    ,tx_to='31DEC5999:23:59:59'dt
    ,libref="&mpelib"
    ,dsn='MPE_SECURITY'
    ,num_of_approvals_required=1
    ,loadtype='TXTEMPORAL'
    ,buskey='LIBREF DSN ACCESS_LEVEL SAS_GROUP'
    ,var_txfrom='TX_FROM'
    ,var_txto='TX_TO'
    ,notes='Shows which metadata groups can edit which tables'
  ;
insert into &syslast
  set tx_from=0
    ,tx_to='31DEC5999:23:59:59'dt
    ,libref="&mpelib"
    ,dsn='MPE_SELECTBOX'
    ,num_of_approvals_required=1
    ,loadtype='TXTEMPORAL'
    ,buskey='SELECTBOX_RK'
    ,var_txfrom='VER_FROM_DTTM'
    ,var_txto='VER_TO_DTTM'
    ,notes='Can configure dropdowns for the front end'
    ,rk_underlying='SELECT_LIB SELECT_DS BASE_COLUMN SELECTBOX_VALUE'
  ;
insert into &mpelib..mpe_tables
  set tx_from=0
    ,tx_to='31DEC5999:23:59:59'dt
    ,libref="&mpelib"
    ,dsn='MPE_X_TEST'
    ,num_of_approvals_required=1
    ,loadtype='UPDATE'
    ,buskey='PRIMARY_KEY_FIELD'
    ,notes='Test table for controller'
  ;
  insert into &mpelib..mpe_tables
    set tx_from=0
      ,tx_to='31DEC5999:23:59:59'dt
      ,libref="&mpelib"
      ,dsn='MPE_VALIDATIONS'
      ,num_of_approvals_required=1
      ,loadtype='TXTEMPORAL'
      ,buskey='BASE_LIB BASE_DS BASE_COL RULE_TYPE'
      ,notes='Configuration of data quality rules in Editor component'
      ,var_txfrom='TX_FROM'
      ,var_txto='TX_TO'
    ;
  insert into &mpelib..mpe_tables
    set tx_from=0
      ,tx_to='31DEC5999:23:59:59'dt
      ,libref="&mpelib"
      ,dsn='MPE_DATADICTIONARY'
      ,num_of_approvals_required=1
      ,loadtype='TXTEMPORAL'
      ,buskey='DD_TYPE DD_SOURCE'
      ,notes='Configuration of data dictionary'
      ,var_txfrom='TX_FROM'
      ,var_txto='TX_TO'
    ;
