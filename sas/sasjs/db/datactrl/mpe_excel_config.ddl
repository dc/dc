/**
  @file
  @brief DDL for MPE_EXCEL_CONFIG (pk: tx_from,xl_libref,xl_table,xl_column)

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/

create table &curlib..mpe_excel_config(
        tx_from num,
        xl_libref char(8),
        xl_table char(32),
        xl_column char(32),
        xl_rule char(32),
        xl_active num,
        tx_to num not null,
    constraint pk_mpe_excel_config
        primary key(tx_from,xl_libref,xl_table,xl_column));
