/**
  @file mpe_datastatus_libs.ddl
  @brief ddl file
  @details

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/

create table &curlib..mpe_datastatus_libs(
    TX_FROM float format=datetime19.,
    TX_TO float format=datetime19.,
    libref char(8) label='Library Name',
    libsize num format=SIZEKMG. label='Size of file',
    table_cnt num label='Number of Tables',
  constraint pk_mpe_datastatus_libs
      primary key(libref,tx_to));
