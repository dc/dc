/**
  @file
  @brief ddl file
  @details


  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/

proc sql;
create table &curlib..mpe_maxkeyvalues(
    keytable varchar(41) label='Base table in libref.dataset format',
    keycolumn char(32) format=$32.
      label='The Surrogate / Retained key field containing the key values.',
    max_key num label=
      'Integer value representing current max RK or SK value in the KEYTABLE',
    processed_dttm num format=E8601DT26.6
      label='Datetime this value was last updated',
  constraint pk_mpe_maxkeyvalues
      primary key(keytable));
