/**
  @file
  @brief ddl file
  @details


  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/

proc sql;
create table &curlib..mpe_lockanytable(
        lock_status_cd varchar(10) not null,
        lock_lib varchar(8) not null ,
        lock_ds varchar(32)  not null,
        lock_user_nm varchar(100) not null ,
        lock_ref varchar(200),
        lock_pid varchar(10),
        lock_start_dttm num format=datetime19.3,
        lock_end_dttm num format=datetime19.3,
    constraint pk_mpe_lockanytable
        primary key(lock_lib,lock_ds));
