/**
  @file
  @brief ddl file
  @details


  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/

proc sql;
create table &curlib..mpe_review(
        table_id varchar(32) not null,
        base_table varchar(41) not null,
        review_status_id varchar(10) not null,
        reviewed_by_nm varchar(100) not null,
        reviewed_on_dttm date not null,
        review_reason_txt varchar(400),
    constraint pk_mpe_review
        primary key(table_id, reviewed_by_nm));

