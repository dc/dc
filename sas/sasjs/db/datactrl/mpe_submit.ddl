/**
  @file
  @brief ddl file
  @details Captures the submit process lifecycle

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/

proc sql;
create table &curlib..mpe_submit(
    table_id varchar(32) not null,
    submit_status_cd varchar(10) not null,
    base_lib char(8) not null,
    base_ds char(32) not null,
    submitted_by_nm varchar(100) not null,
    submitted_on_dttm num not null format=datetime19.2,
    submitted_reason_txt varchar(400),
    input_obs num,
    input_vars num,
    num_of_approvals_required num not null,
    num_of_approvals_remaining num not null,
    reviewed_by_nm char(100),
    reviewed_on_dttm num,
  constraint pk_mpe_submit
    primary key(table_id)
);

