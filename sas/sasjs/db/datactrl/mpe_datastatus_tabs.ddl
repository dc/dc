/**
  @file mpe_datastatus_tabs.ddl
  @brief ddl file
  @details

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/

create table &curlib..mpe_datastatus_tabs(
    TX_FROM float format=datetime19.,
    TX_TO float format=datetime19.,
    libref char(8) label='Library Name',
    dsn char(64) label='Member Name',
    filesize num format=SIZEKMG. label='Size of file',
    crdate num format=DATETIME. informat=DATETIME. label='Date Created',
    modate num format=DATETIME. informat=DATETIME. label='Date Modified',
    nobs num label='Number of Physical (Actual, inc. deleted) Observations',
  constraint pk_mpe_datastatus_tabs
      primary key(libref,dsn,tx_to));
