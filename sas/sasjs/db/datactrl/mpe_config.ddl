/**
  @file
  @brief Provides configuration by scope, name & value

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/

proc sql;
create table &curlib..mpe_config(
        tx_from num not null format=datetime19.3
        ,tx_to num not null format=datetime19.3
        ,var_scope varchar(10) not null
        ,var_name varchar(32) not null
        ,var_value varchar(5000)
        ,var_active num
        ,var_desc varchar(300)
    ,constraint pk_mpe_config
        primary key(tx_from, var_scope, var_name));
