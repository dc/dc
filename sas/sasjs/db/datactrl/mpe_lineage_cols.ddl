/**
  @file mpe_lineage_cols.ddl
  @brief ddl file
  @details


  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/

proc sql;
create table &curlib..mpe_lineage_cols
  (
      col_id char(32),
      direction char(1),
      jobname char(256),
      sourcetablename char(256),
      sourcecolname char(256),
      sourcecoluri char(256),
      map_type char(256),
      map_transform char(256),
      targettablename char(256),
      targetcolname char(256),
      targetcoluri char(256),
      Derived_Rule char(500),
      level int,
      modified_dttm float,
      modified_by char(64),
    constraint pk_mpe_lineage_cols primary key
      (
        col_id,direction,sourcecoluri,targetcoluri,map_type,map_transform
      )
  );
