/**
  @file
  @brief ddl file
  @details


  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/

proc sql;
create table &curlib..mpe_selectbox(
    selectbox_rk int not null, /* surrogate key */
    ver_from_dttm num not null, /* timestamp for versioning */
    select_lib varchar(17) not null, /* libref (big enough for uri)*/
    select_ds varchar(32) not null,
    base_column varchar(36) not null, /* variable name against which to apply selectbox */
    selectbox_value varchar(500) not null, /* selectbox value */
    selectbox_order int , /* optional ordering (1 comes before 2) */
    selectbox_type varchar(32) , /* column type (blank for default, else
                               sas or js to indicate relevant system functions*/
    ver_to_dttm num not null /* timestamp for versioning */
  );

create unique index pk_mpe_selectbox on &syslast(ver_from_dttm, selectbox_rk);

insert into &syslast set
    selectbox_rk=1
    ,ver_from_dttm=0
    ,select_lib="&mpelib"
    ,select_ds="MPE_LOCKANYTABLE"
    ,base_column="LOCK_STATUS_CD"
    ,selectbox_value='LOCKED'
    ,selectbox_order=1
    ,ver_to_dttm='31DEC5999:23:59:59'dt;
insert into &syslast set
    selectbox_rk=2
    ,ver_from_dttm=0
    ,select_lib="&mpelib"
    ,select_ds="MPE_LOCKANYTABLE"
    ,base_column="LOCK_STATUS_CD"
    ,selectbox_value='UNLOCKED'
    ,selectbox_order=2
    ,ver_to_dttm='31DEC5999:23:59:59'dt;
insert into &syslast set
    selectbox_rk=3
    ,ver_from_dttm=0
    ,select_lib="&mpelib"
    ,select_ds="MPE_SECURITY"
    ,base_column="ACCESS_LEVEL"
    ,selectbox_value='EDIT'
    ,selectbox_order=1
    ,ver_to_dttm='31DEC5999:23:59:59'dt;
insert into &syslast set
    selectbox_rk=4
    ,ver_from_dttm=0
    ,select_lib="&mpelib"
    ,select_ds="MPE_SECURITY"
    ,base_column="ACCESS_LEVEL"
    ,selectbox_value='APPROVE'
    ,selectbox_order=2
    ,ver_to_dttm='31DEC5999:23:59:59'dt;
insert into &syslast set
    selectbox_rk=5
    ,ver_from_dttm=0
    ,select_lib="&mpelib"
    ,select_ds="MPE_SECURITY"
    ,base_column="ACCESS_LEVEL"
    ,selectbox_value='SIGNOFF'
    ,selectbox_order=3
    ,ver_to_dttm='31DEC5999:23:59:59'dt;
insert into &syslast set
    selectbox_rk=6
    ,ver_from_dttm=0
    ,select_lib="&mpelib"
    ,select_ds="MPE_TABLES"
    ,base_column="LOADTYPE"
    ,selectbox_value='UPDATE'
    ,selectbox_order=1
    ,ver_to_dttm='31DEC5999:23:59:59'dt;
insert into &syslast set
    selectbox_rk=7
    ,ver_from_dttm=0
    ,select_lib="&mpelib"
    ,select_ds="MPE_TABLES"
    ,base_column="LOADTYPE"
    ,selectbox_value='REPLACE'
    ,selectbox_order=2
    ,ver_to_dttm='31DEC5999:23:59:59'dt;
insert into &syslast set
    selectbox_rk=8
    ,ver_from_dttm=0
    ,select_lib="&mpelib"
    ,select_ds="MPE_TABLES"
    ,base_column="LOADTYPE"
    ,selectbox_value='TXTEMPORAL'
    ,selectbox_order=3
    ,ver_to_dttm='31DEC5999:23:59:59'dt;
insert into &syslast set
    selectbox_rk=9
    ,ver_from_dttm=0
    ,select_lib="&mpelib"
    ,select_ds="MPE_TABLES"
    ,base_column="LOADTYPE"
    ,selectbox_value='BITEMPORAL'
    ,selectbox_order=4
    ,ver_to_dttm='31DEC5999:23:59:59'dt;
insert into &syslast set
    selectbox_rk=10
    ,ver_from_dttm=0
    ,select_lib="&mpelib"
    ,select_ds="MPE_TABLES"
    ,base_column="LOADTYPE"
    ,selectbox_value='FORMAT_CAT'
    ,selectbox_order=5
    ,ver_to_dttm='31DEC5999:23:59:59'dt;

