/**
  @file
  @brief ddl file
  @details


  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/

proc sql;
create table &curlib..mpe_loads(
        csv_dir char(255),
        user_nm char(50) ,
        status char(15) ,
        duration num ,
        processed_dttm num format=datetime19.3,
        reason_txt char(2048) ,
        approvals char(64),
    constraint pk_mpe_loads
        primary key(csv_dir));
