/**
  @file mpe_validations.ddl
  @brief ddl file
  @details


  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/


create table &curlib..MPE_VALIDATIONS
   (
    TX_FROM float format=datetime19.,
    BASE_LIB varchar(8),
    BASE_DS varchar(32),
    BASE_COL varchar(32),
    RULE_TYPE varchar(32),
    RULE_VALUE varchar(128),
    RULE_ACTIVE int ,
    TX_TO float not null format=datetime19.,
    constraint pk_mpe_validations
        primary key(tx_from, base_lib,base_ds,base_col,rule_type));

insert into &syslast set
    tx_from=0
    ,base_lib="&mpelib"
    ,base_ds="MPE_TABLES"
    ,base_col="LIBREF"
    ,rule_type='CASE'
    ,rule_value='UPCASE'
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;

insert into &syslast set
    tx_from=0
    ,base_lib="&mpelib"
    ,base_ds="MPE_TABLES"
    ,base_col="DSN"
    ,rule_type='CASE'
    ,rule_value='UPCASE'
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;

insert into &syslast set
    tx_from=0
    ,base_lib="&mpelib"
    ,base_ds="MPE_TABLES"
    ,base_col="LIBREF"
    ,rule_type='NOTNULL'
    ,rule_value=' '
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;

insert into &syslast set
    tx_from=0
    ,base_lib="&mpelib"
    ,base_ds="MPE_TABLES"
    ,base_col="DSN"
    ,rule_type='NOTNULL'
    ,rule_value=' '
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;

insert into &syslast set
    tx_from=0
    ,base_lib="&mpelib"
    ,base_ds="MPE_TABLES"
    ,base_col="NUM_OF_APPROVALS_REQUIRED"
    ,rule_type='MINVAL'
    ,rule_value='1'
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
