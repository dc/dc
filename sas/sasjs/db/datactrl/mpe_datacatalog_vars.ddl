/**
  @file
  @brief DDL for MPE_DATACATALOG_VARS
  @details

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/

create table &curlib..mpe_datacatalog_vars(
    TX_FROM float format=datetime19.,
    TX_TO float format=datetime19.,
    libref char(8) label='Library Name',
    dsn char(64) label='Table Name',
    memtype char(8) label='Member Type',
    name char(64) label='Column Name',
    type char(16) label='Column Type',
    length num label='Column Length',
    varnum num label='Column Number in Table',
    label char(512) label='Column Label',
    format char(49) label='Column Format',
    idxusage char(9) label='Column Index Type',
    notnull char(3) label='Not NULL?',
    pk_ind num label="Primary Key Indicator (1=Primary Key field)",
  constraint pk_mpe_datacatalog_vars
      primary key(libref,dsn,name,tx_to));
