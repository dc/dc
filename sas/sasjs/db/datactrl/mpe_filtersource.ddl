/**
  @file
  @brief ddl file
  @details


  @version 9.2
  @author 4GL Apps Ltd
**/

create table &curlib..mpe_filtersource(
      filter_hash char(32) not null,
      filter_line num not null,
      group_logic char(3) not null,
      subgroup_logic char(3) not null,
      subgroup_id num not null,
      variable_nm varchar(32) not null,
      operator_nm varchar(12) not null,
      raw_value varchar(4000) not null,
      processed_dttm num not null format=datetime19.,
  constraint pk_mpe_filteranytable
      primary key(filter_hash,filter_line));
