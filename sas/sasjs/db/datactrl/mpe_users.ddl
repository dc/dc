/**
  @file mpe_users.ddl
  @brief ddl file
  @details used to capture the actual users of the app

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/

proc sql;
create table &curlib..mpe_users(
        user_id char(50) not null,
        last_seen_dt num not null format=date9.,
        registered_dt num not null format=date9.,
    constraint pk_mpe_users
        primary key(user_id));
