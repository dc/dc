/**
  @file mpe_datadictionary.ddl
  @brief ddl file
  @details


  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/


create table &curlib..mpe_datadictionary
   (
    TX_FROM float format=datetime19.,
    DD_TYPE char(16),
    DD_SOURCE char(1024),
    DD_SHORTDESC char(256),
    DD_LONGDESC char(32767),
    DD_OWNER char(128),
    DD_RESPONSIBLE char(128),
    DD_SENSITIVITY char(64),
    TX_TO float not null format=datetime19.,
    constraint pk_mpe_datadictionary
        primary key(tx_from, dd_type,dd_source));

insert into &syslast set
    tx_from=0
    ,DD_TYPE='LIBRARY'
    ,DD_SOURCE="&mpelib"
    ,DD_SHORTDESC="Data Controller Control Tables"
    ,DD_LONGDESC="# The Data Controller Library"
    ,DD_OWNER="&sysuserid"
    ,DD_RESPONSIBLE="&sysuserid"
    ,DD_SENSITIVITY="Low"
    ,tx_to='31DEC5999:23:59:59'dt;

insert into &syslast set
    tx_from=0
    ,DD_TYPE='TABLE'
    ,DD_SOURCE="&mpelib..MPE_TABLES"
    ,DD_SHORTDESC="Configuration of new tables for Data Controller"
    ,DD_LONGDESC="# MPE_TABLES - adding new tabels to Data Controller"
    ,DD_OWNER="&sysuserid"
    ,DD_RESPONSIBLE="&sysuserid"
    ,DD_SENSITIVITY="Low"
    ,tx_to='31DEC5999:23:59:59'dt;

insert into &syslast set
    tx_from=0
    ,DD_TYPE='COLUMN'
    ,DD_SOURCE="&mpelib..MPE_TABLES.DSN"
    ,DD_SHORTDESC="Dataset Name to be edited"
    ,DD_LONGDESC="_DSN_ - must be UPCASE"
    ,DD_OWNER="&sysuserid"
    ,DD_RESPONSIBLE="&sysuserid"
    ,DD_SENSITIVITY="Low"
    ,tx_to='31DEC5999:23:59:59'dt;

insert into &syslast set
    tx_from=0
    ,DD_TYPE='DIRECTORY'
    ,DD_SOURCE="/some/directory"
    ,DD_SHORTDESC="Directory for some purpose"
    ,DD_LONGDESC="This directory is great.  It's great directory.  It trumps all other directories."
    ,DD_OWNER="&sysuserid"
    ,DD_RESPONSIBLE="&sysuserid"
    ,DD_SENSITIVITY="Low"
    ,tx_to='31DEC5999:23:59:59'dt;
