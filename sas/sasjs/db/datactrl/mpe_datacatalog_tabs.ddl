/**
  @file mpe_datacatalog_TABS.ddl
  @brief ddl file
  @details

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/

create table &curlib..mpe_datacatalog_TABS(
    TX_FROM float format=datetime19.,
    TX_TO float format=datetime19.,
    libref char(8) label='Library Name',
    dsn char(64) label='Member Name',
    memtype char(8) label='Member Type',
    dbms_memtype char(32) label='DBMS Member Type',
    memlabel char(512) label='Data Set Label',
    typemem char(8) label='Data Set Type',
    nvar num label='Number of Variables',
    compress char(8) label='Compression Routine',
    pk_fields char(512) label='Primary Key Fields (identified by being in a constraint that is both Unique and Not Null)',
  constraint pk_mpe_datacatalog_TABS
      primary key(libref,dsn,tx_to));
