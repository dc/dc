/**
  @file
  @brief DDL for MPE_GROUPS (pk: tx_from,group_name,user_name)

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd
**/

create table &curlib..mpe_groups(
      tx_from num not null,
      group_name char(100) not null,
      group_desc char(256),
      user_name char(50) not null,
      tx_to num not null,
  constraint pk_mpe_groups
      primary key(tx_from,group_name,user_name));
