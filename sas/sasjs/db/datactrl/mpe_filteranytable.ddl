/**
  @file
  @brief ddl file
  @details


  @version 9.2
  @author 4GL Apps Ltd
**/

create table &curlib..mpe_filteranytable(
        filter_rk num not null,
        filter_hash char(32) not null,
        filter_table char(41) not null,
        processed_dttm num not null format=datetime19.,
    constraint pk_mpe_filteranytable
        primary key(filter_rk));

