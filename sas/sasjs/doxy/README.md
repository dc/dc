# SAS Code Docs

This site contains the SAS code used in Data Controller for SAS.  The pages were generated using [`sasjs doc`](https://cli.sasjs.io/doc).

You can download Data Controller from [here](https://git.datacontroller.io/dc/dc/releases).

The main website is [https://datacontroller.io](https://datacontroller.io) and the user guide is [here](https://docs.datacontroller.io).

If you have questions or would like support, we can be contacted in our [Matrix Channel](https://matrix.to/#/%23dc:4gl.io).


