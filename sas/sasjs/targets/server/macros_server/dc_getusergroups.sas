/**
  @file
  @brief Gets all groups
  @details There are two versions of this macro - a META and a VIYA version (in
  different folders).  The interfaces is the same.  This version is VIYA.

  Returns the group memberships of a particular user.

  <h4> SAS Macros </h4>
  @li mf_getuser.sas
  @li ms_getgroups.sas

  @version 3.5
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro dc_getusergroups(user=,outds=ms_getgroups);

  %ms_getgroups(outds=work.groups,user=%mf_getuser())

  data &outds;
    set work.groups;
    rename name=groupname
      description=groupdesc;
  run;
%mend dc_getusergroups;
