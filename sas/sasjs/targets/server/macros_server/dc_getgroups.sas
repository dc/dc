/**
  @file dc_getgroups.sas
  @brief Gets all groups

  <h4> SAS Macros </h4>
  @li ms_getgroups.sas

  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro dc_getgroups(outds=ms_getgroups);

  %ms_getgroups(outds=&outds)

  data &outds;
    length groupuri groupname $32 groupdesc $128 ;
    if _n_=1 then call missing (of _all_);
    if nobs=0 then do;
      groupuri='1';
      groupname='DCDEFAULT';
      groupdesc='DC Default';
      output;
    end;
    set &outds nobs=nobs;
    keep groupuri groupname groupdesc;
    groupuri=cats(groupid);
    groupname=name;
    groupdesc=description;
    output;
  run;
%mend dc_getgroups;
