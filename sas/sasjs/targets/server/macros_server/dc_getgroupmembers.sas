/**
  @file dc_getgroupmembers.sas
  @brief Gets all the memers of a group
  @details There are two versions of this macro - a META and a VIYA version (in
  different folders).  The interfaces is the same.  This version is VIYA.

  <h4> SAS Macros </h4>

  @version 3.4
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro dc_getgroupmembers(group,outds=dc_getgroupmembers);
  data &outds ;
    length membername $64;
    call missing(membername);
    stop;
  run;
%mend dc_getgroupmembers;
