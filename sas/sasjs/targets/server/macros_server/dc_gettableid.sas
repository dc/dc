/**
  @file dc_gettableid.sas
  @brief Gets the  table id
  @details There are two versions of this macro - a META and a VIYA version (in
  different folders).  The interfaces is the same.  This version is VIYA.


  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro dc_gettableid(libref=
    ,ds=
    ,outds=);

data &outds;
  tableuri='';
  tablename="&ds";
run;

%mend dc_gettableid;
