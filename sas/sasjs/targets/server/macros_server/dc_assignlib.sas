/**
  @file
  @brief Assigns library and opens pass through
  @details There are two versions of this macro - a META and a VIYA version (in
  different folders).  The interfaces is the same.  This version is VIYA.

  This macro will source library definitions from a secure location on the
  filesystem.

  @version 3.4
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro dc_assignlib(type,libref,passthru=);
  %if %length(&passthru)>0 %then %do;
    proc sql;
    connect using &libref as &passthru;
  %end;
%mend dc_assignlib;
