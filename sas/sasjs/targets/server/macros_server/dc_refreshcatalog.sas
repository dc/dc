/**
  @file dc_refreshcatalog.sas
  @brief Refresh catalog
  @details There are two versions of this macro - a META and a VIYA version (in
  different folders).  The interfaces are the same.  This version is VIYA.

  The MPELIB should be pre-assigned

  <h4> SAS Macros </h4>
  @li mpe_refreshlibs.sas
  @li dc_assignlib.sas
  @li mpe_refreshtables.sas


  @version 3.4
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro dc_refreshcatalog(libref);

%mpe_refreshlibs()

filename executor catalog 'work.code.code.source';
data libraries;
  set &mpelib..mpe_datacatalog_libs;
  where &dc_dttmtfmt. le TX_TO;
%if %length(&libref)>0 %then %do;
  where also libref="&libref";
%end;
  file executor;
  str=cats('%mpe_refreshtables(',libref,')');
  put str;
  putlog str;
run;
%inc executor/source2;

%mend dc_refreshcatalog;
