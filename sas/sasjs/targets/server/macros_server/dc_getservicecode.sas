/**
  @file
  @brief Gets service code

  <h4> SAS Macros </h4>
  @li ms_getfile.sas

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro dc_getservicecode(loc=,outref=);

  %ms_getfile(&loc..sas, outref=&outref)

%mend dc_getservicecode;
