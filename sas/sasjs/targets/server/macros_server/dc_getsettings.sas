/**
  @file
  @brief Get settings from SASjs Drive
  @details gets and runs the code from services/public/settings.sas

  <h4> SAS Macros </h4>
  @li mp_abort.sas
  @li mf_getapploc.sas
  @li ms_getfile.sas

  @version 3.5
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro dc_getsettings();
%global DC_LIBNAME DC_LIBREF;

/* get settings code */
%ms_getfile(%mf_getapploc()/services/public/settings.sas, outref=settings)

/* run it */
%inc settings/source2;

%let DC_LIBNAME=&dc_libref;
%let mpelib=&DC_LIBREF;

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=dc_getsettings
  ,msg=%str(syscc=&syscc after dc_getsettings)
)

%mend dc_getsettings;
