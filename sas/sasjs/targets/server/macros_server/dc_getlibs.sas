/**
  @file dc_getlibs.sas
  @brief Gets all available libraries
  @details There are two versions of this macro - a META and a VIYA version (in
  different folders).  The interfaces is the same.  This version is VIYA.

  <h4> SAS Macros </h4>

  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro dc_getlibs(outds=mm_getlibs);
proc sql;
create table &outds as
  select distinct libname as LibraryRef
    ,libname as LibraryName length=256
    ,engine
    ,'' as libraryid length=17
  from dictionary.libnames
  where libname not in ('WORK','SASUSER');
insert into &syslast values ("&DC_LIBREF", "&DC_LIBNAME",'','V9');

%mend dc_getlibs;
