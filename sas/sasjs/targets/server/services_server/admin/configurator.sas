/**
  @file
  @brief provides the web service to create the control tables
  @details

  <h4> SAS Macros </h4>
  @li dc_getusergroups.sas
  @li mf_getuser.sas
  @li mf_getapploc.sas
  @li mp_base64copy.sas

  <h4> Binary Files </h4>
  @li dcsquare.png dcsq

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/

%let root=%mf_getapploc();

/* create web page */
data _null_;
  file _webout;
  infile datalines ;
  input;
  put _infile_;
datalines4;
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" http-equiv="X-UA-Compatible" content="IE=edge" />
  <title>Data Controller</title>
  <link rel="stylesheet"
    href="https://unpkg.com/@clr/ui@5.5.3/clr-ui.min.css">
  <style>
  .button {
    display: block;
    height: 35px;
    background: #4E9CAF;
    padding: 10px;
    text-align: center;
    border-radius: 5px;
    color: white !important;
    font-weight: bold;
  }
  </style>
;;;;
run;

/* convert favicon to base64 */
%mp_base64copy(inref=dcsq, outref=b64dcsq, action=ENCODE)
/* send */
/*
data _null_;
  file _webout mod lrecl=32767;
  infile b64dcsq end=last lrecl=1 recfm=n;
  if _n_=1 then do;
    put '<link rel="icon" type="image/x-icon"';
    put ' href="data:image/x-icon;base64,' @@;
  end;

  input sourcechar $char1. @@;
  put sourcechar +(-1) @@;

  if last then do;
    put '"';
    put '>';
  end;
run;
*/

/* get the folder root (depends where the app was installed) */
data _null_;
  length root $512;
  pgm="&_program";
  root="&root";
  putlog root=;
  file _webout mod;
  root=quote(trim(root));
  put '<script type="text/javascript">';
  put 'function mkData(dclib,path,groupname){';
  put '  const _debug=" ";//"&_debug=131";';
  put '  const root=' root ';';
  put "  window.location = location.pathname + '?_program='";
  put '  + root + "/services/admin/makedata&admin="';
  put '  + groupname + "&path=" + path + _debug';
  put '  + "&dclib=" + dclib;';
  put '  return false;';
  put '}';
  put 'function onLoad(){';
  put "  document.getElementById('mainbody').style.display='none';";
  put "  document.getElementById('dclib').value='DCCONFIG';";
  put "  document.getElementById('sasdata').value='/tmp/dcserver';";
  put '}';
run;

data _null_;
  file _webout mod;
  infile datalines ;
  input;
  put _infile_;
datalines4;
  </script>
</head>
<body cds-text="body" onload="onLoad()">
<header class="header-6">
    <div class="branding">
      <img style="width:40px;height:40px;margin-top:8px;margin-right:20px;"
        src="https://docs.datacontroller.io/img/dc.jpg">
        <span class="title">Data Controller</span>
    </div>
</header>
<div class="main-container">
  <div class="content-container">
    <div class="content-area">
      <div class="clr-row">
<!-- T&C section -->
<div id=TCS class="card">
<div class="card-header">
Terms and Conditions
</div>
<div class="card-block">
<div class="card-text">
<p>
  Before proceeding with configuration, please confirm that you have read,
  understood, and agreed to the
  <a href=https://docs.datacontroller.io/evaluation-agreement/
  target=_blank>Data Controller for SAS&#169; Evaluation Agreement</a>.
</p>
</div>
<div class="clr-checkbox-wrapper">
  <input type="checkbox" id="checkbox1" name="checkbox-basic" value="option1"
  class="clr-checkbox"
    onchange="document.getElementById('mainbody').style.display='';
    document.getElementById('TCS').style.display='none';">
  <label for="checkbox1">I have read and agree to the terms of the
    <a href=https://docs.datacontroller.io/evaluation-agreement
    target=_blank> Data Controller for SAS&#169; Evaluation Agreement</a>
  </label>
</div>
</div>
</div>
<!-- T&C section end -->
<div id=mainbody class="card" style="display:none; padding: 5px 20px;">
  <div class="card-header">
      Configurator
  </div>
  <div class="card-text">
      <p>To configure Data Controller for SAS&#169;, please provide the
      following details:</p>
  <cds-form-group layout="vertical">
;;;;
run;

/* get list of groups */
%dc_getusergroups(user=%mf_getuser(),outds=groups)

data _null_;
  file _webout;
  set work.groups;
  if _n_=1 then do;
    put "<p>Please specify a valid LIBREF to be used as the DC control";
    put "library.</p></br>";
    put '  <cds-input control-width="shrink">';
    put '    <label>DC Libref: </label><input style="border: 1px solid #a5a5a5; ';
    put 'border-radius: 3px; padding: 4px;" id=dclib';
    put 'placeholder="DCCONFIG" required /></cds-input>';
    put "<p>Please specify a physical directory below, to which user <code>";
    put "&sysuserid </code> can write, on behalf of Data Controller:</p></br>";
    put '  <cds-input control-width="shrink">';
    put '    <label>DC Directory: </label><input style="border: 1px solid #a5a5a5; ';
    put 'border-radius: 3px; padding: 4px;" id=sasdata';
    put 'placeholder="/tmp/dcserver" required /></cds-input>';
    put '<p>Below are the list of groups to which you belong. The group you ';
    put 'select will become the <a href=';
    put '"https://docs.datacontroller.io/dcc-groups"> Data Controller Admin ';
    put 'Group</a>, and everyone in it will have unrestricted access to ';
    put ' Data Controller.</p></br>';
    put '<cds-select layout="vertical">';
    put ' <label>Data Controller Admin Group: </label><select style="border-radius: 3px; ';
    put 'padding: 4px; background: white;" id=groupname>';
  end;
  put '<option>' groupname '</option>';
run;

data _null_;
  file _webout mod;
  infile datalines ;
  input;
  put _infile_;
datalines4;
  </select></cds-select></br>
  <p>Now, create the target directory and deploy the control library using the
  admin group selected above:</p></br>
  <a class="button"
  onclick="mkData(
    document.getElementById('dclib').value,
    document.getElementById('sasdata').value,
    document.getElementById('groupname').value
  )"
  href="javascript:void(0);">SUBMIT</a>
</form>
</cds-form-group>
;;;;
run;

data _null_;
  file _webout mod;
  put '</div><div class="card-block"><div class="card-title">';
  put " Configuration Information ";
  put '</div><div class="card-text">';
  metaperson=symget('_metaperson');
  metauser=symget('_metauser');
  sysuserid=symget('sysuserid');
  put "<p> You are connected with the following credentials:<ul>";
  put "<li> METAPERSON: " metaperson "</li>";
  put "<li> METAUSER: " metauser "</li>";
  put "<li> SYSUSERID: " sysuserid "</li>";
  put "<li> SYSHOSTNAME: &syshostname</li>";
  put "<li> SYSVLONG: &sysvlong</li>";
  put "<li> SYSSITE: &syssite</li>";
run;

data _null_;
  file _webout;
  infile datalines ;
  input;
  put _infile_;
datalines4;
          </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
;;;;
run;
