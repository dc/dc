/**
  @file usermembersbygroup.sas
  @brief List the members of a group

  <h4> SAS Macros </h4>
  @li mp_abort.sas
  @li mpeinit.sas
  @li ms_getusers.sas

  <h4> Data Inputs </h4>
  <h5> iwant </h5>
  |groupid:$|
  |---|
  |1|

  <h4> Data Outputs </h4>
  <h5> sasmembers </h5>
|DISPLAYNAME:$60.|USERNAME:$30.|ID:best.|
|---|---|---|
|`Super Admin `|`secretuser `|`1`|
|`Sabir Hassan`|`sabir`|`2`|
|`Mihajlo Medjedovic `|`mihajlo `|`3`|
|`Ivor Townsend `|`ivor `|`4`|
|`New User `|`newuser `|`5`|

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/
%mpeinit()

data _null_;
  set work.iwant;
  call symputx('gid',groupid);
run;

%ms_getusers(outds=sasMembers, gid=&gid)

%webout(OPEN)
%webout(OBJ,sasMembers)
%webout(CLOSE)
