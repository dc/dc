/**
  @file usergroupsbymember.sas
  @brief List the groups a member is in
  @details Runs without \%mpeinit() - this enables the dropdown to be populated
  during configuration, when the settings service does not yet exist.

  <h4> SAS Macros </h4>
  @li mp_abort.sas
  @li mpeinit.sas
  @li ms_getgroups.sas

  <h4> Data Inputs </h4>
  <h5> iwant </h5>
  |uri:$|
  |---|
  |1|

  <h4> Data Outputs </h4>
  <h5> groups </h5>
  |NAME:$32.|DESCRIPTION:$64.|GROUPID:best.|
  |---|---|---|
  |`SomeGroup `|`A group `|`1`|
  |`Another Group`|`this is a different group`|`2`|
  |`admin`|`Administrators `|`3`|

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

/* enable service to run without an input */
%let exist=%sysfunc(exist(work.iwant));
%let inds=%sysfunc(ifc(&exist=1,iwant,_null_));
%let uid=&_sasjs_userid;
data _null_;
  set &inds;
  call symputx('uid',uri);
run;

%ms_getgroups(outds=work.groups,uid=&uid)

data work.groups;
  set work.groups(rename=(groupid=uri name=groupname description=groupdesc));
run;

%webout(OPEN)
%webout(OBJ,groups)
%webout(CLOSE)

