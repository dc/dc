/**
  @file usermembers.sas
  @brief List all SAS users
  @details Gets a list of all SAS users

  <h4> SAS Macros </h4>
  @li dc_getusers.sas
  @li mpeinit.sas

  <h4> Data Outputs </h4>
  <h5> users </h5>
|DISPLAYNAME:$60.|USERNAME:$30.|ID:best.|
|---|---|---|
|`Super Admin `|`secretuser `|`1`|
|`Sabir Hassan`|`sabir`|`2`|
|`Mihajlo Medjedovic `|`mihajlo `|`3`|
|`Ivor Townsend `|`ivor `|`4`|
|`New User `|`newuser `|`5`|

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%mpeinit()
%dc_getusers(outds=users)

%webout(OPEN)
%webout(OBJ,users)
%webout(CLOSE)


