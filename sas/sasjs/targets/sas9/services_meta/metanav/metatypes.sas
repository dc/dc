/**
  @file metatypes.sas
  @brief Retrieves list of metadata types
  @details

  <h4> SAS Macros </h4>
  @li mm_gettypes.sas

  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/
%mpeinit()
%mm_gettypes(outds=work.types)

%webout(OPEN)
%webout(OBJ,types)
%webout(CLOSE)
%mpeterm()
