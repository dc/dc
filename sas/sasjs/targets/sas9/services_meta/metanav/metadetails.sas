/**
  @file metadetails.sas
  @brief Retrieves metadata attributes and associations for a particular object
  @details

  <h4> SAS Macros </h4>
  @li mm_getdetails.sas
  @li mpe_getvars.sas
  @li mpeinit.sas

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/

%mpeinit()

%mpe_getvars(SASControlTable, SASControlTable)

%mm_getdetails(&objecturi
  ,outattrs=work.attributes
  ,outassocs=work.associations
  ,sortoptions=SORTSEQ=LINGUISTIC
)

%webout(OPEN)
%webout(OBJ,attributes, missing=STRING)
%webout(OBJ,associations)
%webout(CLOSE)
%mpeterm()
