/**
  @file metaobjects.sas
  @brief Retrieves list of objects for a particular metadata type
  @details

  <h4> SAS Macros </h4>
  @li mm_getobjects.sas
  @li mpe_getvars.sas
  @li mpeinit.sas

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/

%mpeinit()

%mpe_getvars(SASControlTable, SASControlTable)
options metarepository=&repo;
%mm_getobjects(
  type=&metatype
  ,outds=work.objects
)

%webout(OPEN)
%webout(OBJ,objects,missing=STRING)
%webout(CLOSE)
%mpeterm()
