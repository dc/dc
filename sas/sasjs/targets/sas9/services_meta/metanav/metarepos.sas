/**
  @file metarepos.sas
  @brief Retrieves list of metadata types
  @details

  <h4> SAS Macros </h4>
  @li mm_getrepos.sas

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/
%mpeinit()
%mm_getrepos(outds=work.repos)

data outrepos;
  set repos;
  if repositorytype in ('CUSTOM','FOUNDATION');
  keep id name description;
run;
%webout(OPEN)
%webout(OBJ,outrepos)
%webout(CLOSE)
%mpeterm()
