/**
  @file usermembers.sas
  @brief List all SAS users
  @details Gets a list of all SAS users

  <h4> SAS Macros </h4>
  @li dc_getusers.sas
  @li mpeinit.sas

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%mpeinit()
%dc_getusers(outds=users)

%webout(OPEN)
%webout(OBJ,users)
%webout(CLOSE)


