/**
  @file userroles.sas
  @brief List all SAS Roles
  @details Gets a list of all SAS Roles

  <h4> SAS Macros </h4>
  @li dc_getroles.sas
  @li mpeinit.sas

  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/
%mpeinit()
%dc_getroles(outds=roles)

%webout(OPEN)
%webout(OBJ,roles)
%webout(CLOSE)
