/**
  @file getmetatables.sas
  @brief List the tables as defined in metadata
  @details Provide a library uri and get list of tables

  <h4> SAS Macros </h4>
  @li mm_gettables.sas
  @li mp_abort.sas

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%mpeinit()

%global liburi;
%let exist=%sysfunc(exist(work.SASControlTable));
%let inds=%sysfunc(ifc(&exist=1,SASControlTable,_null_));

%put &=inds;

data _null_;
  set &inds;
  call symputx('liburi',liburi);
run;

/* load parameters */
%mm_gettables(uri=&liburi,outds=metatables,getauth=NO)

data out;
  set metatables;
  keep table:;
run;

%webout(OPEN)
%webout(OBJ,metatables)
%webout(CLOSE)


%mpeterm()
