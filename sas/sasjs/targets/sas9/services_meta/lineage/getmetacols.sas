/**
  @file getmetacols.sas
  @brief List the cols as defined in metadata
  @details Provide a table uri and get list of columns

  <h4> SAS Macros </h4>
  @li mm_getcols.sas
  @li mp_abort.sas

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%mpeinit()

%global tableuri;
%let exist=%sysfunc(exist(work.SASControlTable));
%let inds=%sysfunc(ifc(&exist=1,SASControlTable,_null_));

%put &=inds;

data _null_;
  set &inds;
  call symputx('tableuri',scan(tableuri,-1,'\'));
run;
%put &=tableuri;
/* load parameters */
%mm_getcols(tableuri=&tableuri,outds=metacols)

data out;
  set metacols;
  keep col:;
run;

%webout(OPEN)
%webout(OBJ,out,dslabel=metacols)
%webout(CLOSE)


%mpeterm()
