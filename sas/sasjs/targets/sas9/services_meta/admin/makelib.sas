/**
  @file
  @brief Create a demo (base engine) library
  @details
    Creates the library

  @warning This STP self destructs! It will delete itself after a successful run
    to avoid being executed twice (and overwriting actual data)


  <h4> SAS Macros </h4>
  @li mm_createlibrary.sas
  @li mm_createdocument.sas
  @li mm_deletedocument.sas
  @li mp_abort.sas
  @li dc_assignlib.sas
  @li mm_deletestp.sas
  @li mm_createstp.sas
  @li mf_mkdir.sas


  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/

%global dcpath;

%let dclib=%upcase(DC%substr(%sysevalf(%sysfunc(datetime())/60),3,6));
%let dclibname=Data Controller(&dclib);
%let work=%sysfunc(pathname(work));
%let dcpath=&dcpath/&dclib;

%mf_mkdir(&dcpath)
%mf_mkdir(&work)
%put &=dcpath;

/* check we have physical permissions to the DCLIB folder */
data _null_;
  file "&dcpath/permTest.txt";
run;
%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program..sas
  ,msg=%str(User &sysuserid does not have WRITE permissions on physical
    directory: &dcpath )
)
filename delfile "&dcpath/permTest.txt";
data _null_;
  rc=fdelete('delfile');
run;
%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program..sas
  ,msg=%str(User &sysuserid could create (but not delete) &dcpath/permTest.txt )
)



%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program..sas
  ,msg=%str(Unable to write to &dcpath)
)


data _null_;
  pgm="&_program";
  rootlen=length(trim(pgm))-length("/services/admin/makelib");
  root=substr(pgm,1,rootlen);
  putlog root=;
  call symputx('deploy_dir',root);
run;

options noquotelenmax ps=max;

/* check we have the admin rights to update the items in the Admin folder */
%mm_createdocument(tree=&deploy_dir/services/admin,name=permTest)
%mm_deletedocument(target=&deploy_dir/services/admin/permTest)

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program..sas
  ,msg=%str(User &_metaperson does not have WriteMetadata on SAS folder:
    &deploy_dir )
)

/**
  * Create library and load data
  */
%let mpelibname=Data Controller (&dclib);
%mm_createlibrary(
  libname=&mpelibname
  ,libref=&dclib
  ,libdesc=Configuration tables for the MacroPeople Data Controller application
  ,engine=BASE
  ,tree=&deploy_dir/data
  ,servercontext=SASApp
  ,directory=&dcpath
  ,mDebug=1)

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program..sas
  ,msg=%str(Unable to create &dclib library)
)

/* get direct libref */
%dc_assignlib(READ,&dclib)

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program..sas
  ,msg=%str(Unable to assign &dclib library)
)


/* create an initial settings service (this will be overwritten in the
  data update step)
*/

%let temploc=&work/temp.txt;
data _null_;
  file "&temploc" ;
  put '/* Data Controller Precode */' / / ;
  put ' ';
  put 'options noquotelenmax ps=max;';
  put '%global DC_LIBREF DC_LIBNAME;';
  put ' ';
  put '/* This metadata library (libref) contains control datasets for DC */';
  put '/* If a different libref must be used, configure it below */';
  put '%let DC_LIBREF=' "&dclib;";
  put '%let DC_LIBNAME=' "&mpelibname;";
  put ' ';
  put '/* get physical path for direct libname - needed to track requests */';
  put 'data _null_;';
  put '  length lib_uri up_uri path $256;';
  put '  call missing (of _all_);';
  put '  rc=metadata_getnobj("omsobj:SASLibrary?@Libref=''&dc_libref''",1,lib_uri);';
  put '  rc=metadata_getnasn(lib_uri,"UsingPackages",1,up_uri);';
  put '  rc=metadata_getattr(up_uri,"DirectoryName",path);';
  put '  call symputx("dc_libloc",path);';
  put 'run;';
  put 'libname &DC_LIBREF "&dc_libloc";';
  put ' ';
run;

%mm_deletestp(target=&deploy_dir/services/public/Data_Controller_Settings)

%mm_createstp(stpname=Data_Controller_Settings
  ,filename=temp.txt
  ,directory=&work
  ,tree=&deploy_dir/services/public
  ,Server=SASApp
  ,stptype=2
  ,mdebug=1
  ,stpdesc=Data Controller Configuration
  ,minify=NO)

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program..sas
  ,msg=%str(Issue creating settings stp)
)

data _null_;
  file _webout;
  put "<p> Library &dclib successfully assigned</p>";
run;
