/**
  @file
  @brief provides the web service to create the control tables
  @details

  <h4> SAS Macros </h4>
  @li dc_getusergroups.sas
  @li mf_getuser.sas
  @li mf_getapploc.sas

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/

%let root=%mf_getapploc();

/* create web page */
data _null_;
  file _webout;
  infile datalines ;
  input;
  put _infile_;
datalines4;
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" http-equiv="X-UA-Compatible" content="IE=edge" />
  <title>Data Controller</title>
  <link rel="stylesheet"
    href="https://unpkg.com/@clr/ui@1.0.5/clr-ui.min.css">
  <link rel="icon" type="image/x-icon"
    href="https://datacontroller.io/wp-content/uploads/2018/04/logo-2.png">
  <script type="text/javascript">
;;;;
run;
/* get the folder root (depends where the app was installed) */
data _null_;
  length root $512;
  pgm="&_program";
  root="&root";
  putlog root=;
  file _webout;
  root=quote(trim(root));
  put 'let root=' root ';';
  put 'let repo="foundation"';
run;

data _null_;
  file _webout;
  infile datalines ;
  input;
  put _infile_;
datalines4;
  var _debug='';/* '&_debug=131' */
  function mkUrl(root,groupname,repo='foundation'){
      window.location = location.pathname + '?_program='
        + root + '/services/admin/makedata&admin='
        + groupname + '&repo=' + repo + _debug;
      return false;
  }
  function makeLib(){
    var loc=location.pathname + '?_program='
        + root + '/services/admin/makelib&dcpath='
        + document.getElementById('dcloc').value
        + '&repo=' + repo + _debug;
    console.log(loc);
    window.open(loc,'_blank') ;
      return false;
  }
  </script>
</head>
<body onload="document.getElementById('mainbody').style.display='none';">
<header class="header-6">
    <div class="branding">
      <img style="width:40px;height:40px;margin-top:8px;margin-right:20px;"
        src=
        "https://datacontroller.io/wp-content/uploads/2018/04/pic-1-300x300.png"
        alt="">
        <span class="title">Data Controller</span>
    </div>
</header>
<div class="main-container">
  <div class="content-container">
    <div class="content-area">
      <div class="clr-row">
<!-- T&C section -->
<div id=TCS class="card">
<div class="card-header">
Terms and Conditions
</div>
<div class="card-block">
<div class="card-text">
<p>Due to the way the Demo version is compiled (in an easy-to-deploy but
  inefficient-to-run format), it should not be deployed to production servers.
  Before proceeding with configuration, please confirm that you have read,
  understood, and agreed to the
  <a href=https://docs.datacontroller.io/evaluation-agreement/
  target=_blank>Data Controller for SAS&#169; Evaluation Agreement</a>.
</p>
</div>
<div class="clr-checkbox-wrapper">
  <input type="checkbox" id="checkbox1" name="checkbox-basic" value="option1"
  class="clr-checkbox"
    onchange="document.getElementById('mainbody').style.display='';
    document.getElementById('TCS').style.display='none';">>
  <label for="checkbox1">I have read and agree to the terms of the
    <a href=https://docs.datacontroller.io/evaluation-agreement/
    target=_blank> Data Controller for SAS&#169; Evaluation Agreement</a>
  </label>
</div>
</div>
</div>
<!-- T&C section end -->
<div id=mainbody class="card" style="display:none">
  <div class="card-header">
      Configurator
  </div>
  <div class="card-block">
      <div class="card-title">
          What is this?
      </div>
      <div class="card-text">
          <p>The Data Controller for SAS&#169; requires a set of configuration
          tables to be in place.  These are stored in a library called
;;;;
run;
/* get the folder root (depends where the app was installed) */
data _null_;
  file _webout;
  put "'&DC_LIBNAME'. For the demo version, a physical directory is used";
  put " to store these tables.  For &syshostname this physical directory ";
  put "(full directory path) is <code>&DC_LIBLOC</code>.</p>";
data _null_;
  file _webout;
  infile datalines ;
  input;
  put _infile_;
datalines4;
          <p>Configuration is made using a dedicated SAS <b>
          group</b>.  Below are the list of SAS groups to which you
          belong.  Clicking one of these groups will both create
          the configuration tables (in the Data Controller library)
          and ensure that <b>everyone in that group</b> will
          subsequently be able to configure those tables.</p>

          <p>To clarify - everyone in the group you choose below will have
          <b>full access</b> to Data Controller.  This will effectively be the
          Data Controller "administrator" group.</p>
      </div>
  </div>
  <div class="card-footer">
;;;;
run;

/* get list of groups */
%dc_getusergroups(user=%mf_getuser(),outds=groups)

data _null_;
  file _webout;
  if nobs=0 then do;
    put "<h6> YOU (%mf_getuser()) ARE NOT IN ANY SAS GROUPS</h6>";
    put "<p>Data Controller requires a SAS group to be designated";
    put "as admin. </p>";
  end;
  set groups end=last nobs=nobs;
  if _n_=1 then put "<ul>";
  length str $2000;
  str=cats('<li><a onclick="mkUrl(',"'&root','",groupname
    ,"')",'" href="javascript:void(0);">',groupname,"</a></li>");
  put str;
  if last then put "</ul>";
run;

data _null_;
  file _webout;
  put '</div><div class="card-block"><div class="card-title">';
  put " Configuration Information ";
  put '</div><div class="card-text">';
  if symexist('_metaperson') then do;
    metaperson=symget('_metaperson');
    metauser=symget('_metauser');
    sysuserid=symget('sysuserid');
    put "<p> You are connected with the following credentials:<ul>";
    put "<li> METAPERSON: " metaperson "</li>";
    put "<li> METAUSER: " metauser "</li>";
    put "<li> SYSUSERID: " sysuserid "</li>";
    put "<li> SYSSITE: &syssite</li>";
    url='/SASStoredProcess/do';
  end;
  else do;
    sysuserid=symget('sysuserid');
    put "<p> You are connected with the following credentials:<ul>";
    put "<li> SYSUSERID: " sysuserid "</li>";
    url='/SASJobExecution';
  end;
  put "</ul>For info on config settings, options and system variables click ";
  length str $2000;
  str=cats('<a href="',url,'?_program='
    ,"&root/services/admin/adminInfo"
    ,'" target=_blank>here</a>.');
  put str;
  if symexist('_metaperson') then do;
    put "Be sure to check the number of multibridge connections - this should ";
    put "be greater than the default (3)!";
  end;
run;

data _null_;
  file _webout;
  infile datalines ;
  input;
  put _infile_;
datalines4;
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
;;;;
run;
