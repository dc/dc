/**
  @file
  @brief service for creating the configuration tables in DEMO mode.
  @details
    STP for creating the configuration tables in DEMO mode.
        It also adds the STAGING directory as subdirectory to the BASE
        library location.
    Note - the CURLIB var is added during the build process.

  @warning This STP self destructs! It will delete itself after a successful run
    to avoid being executed twice (and overwriting actual data)


  <h4> SAS Macros </h4>
  @li mf_getapploc.sas
  @li mf_getuser.sas
  @li mf_mkdir.sas
  @li mm_createdocument.sas
  @li mm_createlibrary.sas
  @li mm_createstp.sas
  @li mm_deletedocument.sas
  @li mm_deletestp.sas
  @li mm_getstpcode.sas
  @li mm_getstpinfo.sas
  @li mp_abort.sas
  @li mp_init.sas
  @li mpe_makedata.sas
  @li mpe_makedatamodel.sas

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/

%mp_init()

%global admin repo path;  /* URL params from configurator */
%let repo=%sysfunc(coalescec(&repo,Foundation));
%let admin=%sysfunc(coalescec(&admin,dc-admin));
%let dcpath=%sysfunc(coalescec(&path,NOTFOUND));

%mp_abort(iftrue= ("&dcpath" = "NOTFOUND")
  ,mac=&_program
  ,msg=%str(PATH variable was not provided)
)

%let root=%mf_getapploc();
%let work=%sysfunc(pathname(work));
%let dc_libref=%upcase(DC%substr(%sysevalf(%sysfunc(datetime())/60),3,6));
%let DC_LIBNAME=Data Controller(&dc_libref);
%let dcpath=&dcpath/&dc_libref;
%put _all_;

%mf_mkdir(&dcpath/dc_staging)

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program
  ,msg=%str(Unable to create &dcpath using &sysuserid)
)


/* check we have the admin rights to update the items in the Admin folder */
%mm_createdocument(tree=&root/services/admin,name=permTest)
%mm_deletedocument(target=&root/services/admin/permTest)

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program
  ,msg=%str(User &_metaperson does not have WriteMetadata on metadata folder:
    &root )
)

/* check we have physical permissions to the DCLIB folder */
data _null_;
  putlog "dcpath=&dcpath/permTest.txt";
  putlog "sysuserid=&sysuserid";
data _null_;
  file "&dcpath/permTest.txt";
run;
%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program
  ,msg=%str(User &sysuserid does not have WRITE permissions on physical
    directory: &dcpath )
)
filename delfile "&dcpath/permTest.txt";
data _null_;
  rc=fdelete('delfile');
run;
%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program..sas
  ,msg=%str(User &sysuserid could create (but not delete) &dcpath/permTest.txt )
)

/* get the server context from the current STP */
%mm_getstpinfo(&_program,outds=work.stpinfo)
data _null_;
  set work.stpinfo;
  call symputx('serverContext',serverContext);
run;

/* create the library */
%mm_createlibrary(
  libname=&DC_LIBNAME
  ,libref=&dc_libref
  ,libdesc=Data Controller for SAS configuration tables
  ,engine=BASE
  ,tree=&root/data
  ,servercontext=&serverContext
  ,directory=&dcpath
  ,mDebug=1
)
%mp_abort(iftrue= (&syscc ne 0)
  ,mac=buildtermsas9
  ,msg=%str(Unable to create &dc_libref library)
)

/* assign the library */
libname &dc_libref "&dcpath";
%mp_abort(iftrue= (&syscc ne 0)
  ,mac=buildtermsas9
  ,msg=%str(Unable to assign the &dc_libref library)
)

/* make the tables */

/* SASAdministrators */
%mpe_makedatamodel(lib=&dc_libref)
%mpe_makedata(lib=&DC_LIBREF,mpeadmins=&admin,path=%str(&dcpath))

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program..sas
  ,msg=%str(Problem creating tables in &DC_LIBREF library\n
    SYSERRORTEXT=&SYSERRORTEXT \n
    SYSWARNINGTEXT=&SYSWARNINGTEXT)
)

/* register tables in metadata */
proc metalib;
  omr (library="&DC_LIBNAME");
  folder="&root/data";
  update_rule=(delete);
run;

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program..sas
  ,msg=%str(Problem registering metadata for tables in &DC_LIBREF library \n
    os user=&sysuserid \n
    metaperson=%mf_getuser() \n
    SYSERRORTEXT=&SYSERRORTEXT \n
    SYSWARNINGTEXT=&SYSWARNINGTEXT
  )
)


/* finally, update the app component. The user will need WM perms for this. */
data _null_;
  file "&work/settings.sas" mod ;
  put ' ';
  put '%global DC_LIBREF DC_LIBNAME DC_LIBLOC DC_STAGING_AREA DC_ADMIN_GROUP';
  put '  DC_REPO_USERS DC_DTTMTFMT DC_MACROS;';
  put ' ';
  put '/* This metadata library (libref) contains control datasets for DC */';
  put '/* If a different libref must be used, configure it below */';
  put '%let DC_LIBREF=' "&DC_LIBREF;";
  put '%let DC_LIBNAME=' "&DC_LIBNAME;";
  put '%let DC_LIBLOC=' "&dcpath;";
  put ' ';
  put 'libname &DC_LIBREF "&dc_libloc";';
  put ' ';
  put '/* This metadata group has unrestricted access to Data Controller */';
  put '%let dc_admin_group=' "&admin;";
  put ' ';
  put '/* This repository is used to query for users and groups */';
  put '%let dc_repo_users=' "&repo;";
  put ' ';
  put '/* This physical location is used for staging data and audit history */';
  put '%let dc_staging_area=&dc_libloc/dc_staging;';
  put ' ';
  put 'data _null_;';
  put '  set &DC_LIBREF..mpe_config(where=(';
  put '    var_scope="DC" ';
  put '    and &dc_dttmtfmt. lt tx_to';
  put '    and var_active=1';
  put "  ));";
  put "  call symputx(var_name,var_value,'G');";
  put '  putlog var_name "=" var_value;';
  put "run;";
  put ' ';
  put '/* to override any DC macros with client versions, place them below */';
  put 'options insert=(sasautos=("&dc_macros"));';
run;
/*
  put 'data _null_;';
  put '  length lib_uri up_uri path $256;';
  put '  call missing (of _all_);';
  put '  rc=metadata_getnobj("omsobj:SASLibrary?@Libref =''&dc_libref''",1,lib_uri);';
  put '  rc=metadata_getnasn(lib_uri,"UsingPackages",1,up_uri);';
  put '  rc=metadata_getattr(up_uri,"DirectoryName",path);';
  put '  call symputx("dc_libloc",path);';
  put 'run;';
*/
%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program
  ,msg=%str(syscc=syscc=&syscc when preparing data_controller_settings)
)

%mm_createstp(stpname=Data_Controller_Settings
  ,filename=settings.sas
  ,directory=&work
  ,tree=&root/services/public
  ,Server=&serverContext
  ,stptype=2
)

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program..sas
  ,msg=%str(
    Problem updating the dc_staging area in
      &root/services/public/DataController Settings \n
    SYSERRORTEXT=&SYSERRORTEXT \n
    SYSWARNINGTEXT=&SYSWARNINGTEXT
  )
)

data _null_;
  file _webout;
  put '<h3>Data Controller Config</h3>';
  put '<p>The following items have been successfully configured:</p>';
  put "<ul><li>Library Location (&dcpath)</li>";
  put "<li>Table Creation (&DC_LIBREF library) using the <b>&sysuserid</b> identity</li>";
  put "<li>Logs Location (%trim(&dcpath)/logs)</li>";
  put "<li>Repo for users & groups (&repo)</li>";
  put "<li>Data Controller Admin Group (&admin)</li>";
  put "</ul>";
  put "<p> Next Steps: </p>";
  put "<ol><li>Populate <a target='_blank' href='/SASStoredProcess/do?_program=&root/services/admin/refreshtablelineage'>";
  put "Table Lineage</a></li>";
  put "<li>Populate<a target='_blank' href='/SASStoredProcess/do?_program=&root/services/admin/refreshcatalog'>";
  put "Data Catalog</a></li>";
  put "<li>Now, <a target='_blank' href='/SASStoredProcess/do?_program=&root/services/clickme'>";
  put "here</a>Launch!</li></ol>";
run;

/* We ran successfully, now remove configurator and makedata STPs */
%mm_deletestp(target=&root/services/admin/configurator)
%mm_deletestp(target=&_program)
