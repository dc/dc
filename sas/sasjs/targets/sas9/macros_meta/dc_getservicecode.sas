/**
  @file
  @brief Gets service code

  <h4> SAS Macros </h4>
  @li mm_getstpcode.sas

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro dc_getservicecode(loc=,outref=);

  %mm_getstpcode(tree=&loc
    ,outref=&outref
  )

%mend dc_getservicecode;
