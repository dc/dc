/**
  @file dc_getgroupmembers.sas
  @brief Gets all the memers of a group
  @details There are two versions of this macro - a META and a VIYA version (in
  different folders).  The interfaces is the same.  This version is META.

  <h4> SAS Macros </h4>
  @li mm_getgroupmembers.sas

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro dc_getgroupmembers(group,outds=dc_getgroupmembers);

  %mm_getgroupmembers(&group,outds=&outds)

%mend dc_getgroupmembers;
