/**
  @file
  @brief Get settings
  @details There are two versions of this macro - a META and a VIYA version (in
  different folders).  The interfaces is the same.  This version is META.

  <h4> SAS Macros </h4>
  @li mf_getapploc.sas
  @li mm_getstpcode.sas
  @li mp_abort.sas

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro dc_getsettings();
%global _program;

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&sysmacroname
  ,msg=%str(syscc=&syscc on entry (&syswarningtext &syserrortext))
)

%if %length(&_PROGRAM)>1 %then %let root=&_program;
%else %do;
  %global _metauser;
  %let _metauser=&sysuserid;
  /* to mimic a "real" _program we need to give a dummy role and stp name */
  %let root=/dummyRole/dummyName;
%end;


/* the DC precode is stored in the Admin folder in the root of
  the project.  Lets find that root. */
%put &=root;
%let root=%mf_getapploc();
%put &=root;

/* Now we know the root location we can retrieve the params */
%let temploc=%sysfunc(pathname(work))/settings.txt;
%mm_getstpcode(tree=&root/services/public
  ,name=Data_Controller_Settings
  ,outloc=&temploc
)
%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&sysmacroname
  ,msg=%str(Unable to run getstpcode)
)
filename _getsets "&temploc" lrecl=2000;
/*
  Do not use mp_include here - this puts a copy in every service, which creates
  compilation problems when calling services from mp_include
*/
%inc _getsets/source2;

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&sysmacroname
  ,msg=%str(Problem running &sysmacroname (&syswarningtext &syserrortext))
)

%mend dc_getsettings;
