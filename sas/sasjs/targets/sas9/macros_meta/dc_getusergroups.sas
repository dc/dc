/**
  @file dc_getgroups.sas
  @brief Gets all groups
  @details There are two versions of this macro - a META and a VIYA version (in
  different folders).  The interfaces is the same.  This version is META.

  <h4> SAS Macros </h4>
  @li mm_getgroups.sas

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro dc_getusergroups(user=,outds=mm_getgroups);
  %global dc_repo_users;
  %if &dc_repo_users= %then %let dc_repo_users=foundation;
  %mm_getgroups(user=&user,outds=&outds,repo=&dc_repo_users)
%mend dc_getusergroups;
