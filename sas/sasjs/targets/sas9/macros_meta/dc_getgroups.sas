/**
  @file dc_getgroups.sas
  @brief Gets all groups
  @details There are two versions of this macro - a META and a VIYA version (in
  different folders).  The interfaces is the same.  This version is META.

  <h4> SAS Macros </h4>
  @li mm_getgroups.sas
  @li mm_getrepos.sas

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro dc_getgroups(outds=mm_getlibs);

%local repo repocnt x;
%let repo=%sysfunc(getoption(metarepository));

/* get list of repositories and filter */
%mm_getrepos(outds=repos)
%let repocnt=0;
data repos;
  set repos;
  where repositorytype in('CUSTOM','FOUNDATION')
    & upcase(name) ne "%upcase(&repo)";
  keep id name ;
  call symputx(cats('repo',_n_),name,'l');
  call symputx('repocnt',_n_,'l');
run;
%put _local_;

%mm_getgroups(outds=&outds,repo=foundation)
%do x=1 %to &repocnt;
  %mm_getgroups(outds=&outds.a, repo=&&repo&x)
  proc append base=&outds data=&outds.a;
  run;
%end;

proc sort data=&outds noduprec;
by groupname;
run;

%mend dc_getgroups;
