/**
  @file dc_refreshcatalog.sas
  @brief Refresh catalog
  @details There are two versions of this macro - a META and a VIYA version (in
  different folders).  The interfaces are the same.  This version is META.

  The MPELIB should be pre-assigned

  <h4> SAS Macros </h4>
  @li mpe_refreshlibs.sas
  @li dc_assignlib.sas
  @li mpe_refreshtables.sas
  @li mm_getrepos.sas


  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro dc_refreshcatalog(libref);

/* take current repository */
%local repo repocnt xx;
%let repo=%sysfunc(getoption(metarepository));

/* get list of repositories and filter */
%mm_getrepos(outds=repos)
%let repocnt=0;
data repos;
  set repos;
  put (_all_)(=);
  where repositorytype in('CUSTOM','FOUNDATION');
  keep id name ;
  call symputx(cats('repo',_n_),name,'l');
  call symputx('repocnt',_n_,'l');
run;
%put &sysmacroname #&libref#;

%if #&libref# ne ## %then %do;
  %put &sysmacroname: assigning specific libref, &libref;
  %dc_assignlib(WRITE,&libref) /* write just in order to assign direct lib */
  %mpe_refreshlibs(lib=&libref)
  %mpe_refreshtables(&libref)
%end;
%else %do xx=1 %to &repocnt;
  options metarepository=&&repo&xx;
  %mpe_refreshlibs()

  /* get libs to be ignored */
  %local ignorelist;
  proc sql noprint;
  select var_value into: ignorelist
    from &mpelib..MPE_CONFIG
    where var_scope='DC_CATALOG'
      and var_name="DC_IGNORELIBS"
      and &dc_dttmtfmt. < TX_TO
      and var_active=1;

  /* get all libs */
  %let libcnt=0;
  data libraries;
    set &mpelib..mpe_datacatalog_libs;
    where &dc_dttmtfmt. le TX_TO;
    if index("&ignorelist",'|'!!upcase(trim(libref))!!'|')=0;
    i+1;
    call symputx(cats('lib',i),libref);
    call symputx('libcnt',i);
  run;
  %local i;
  %do i=1 %to &libcnt;
    %dc_assignlib(WRITE,&&lib&i)
    %mpe_refreshtables(&&lib&i)
  %end;
%end;

options metarepository=&repo;
%mend dc_refreshcatalog;
