/**
  @file
  @brief Gets service code

  <h4> SAS Macros </h4>
  @li mv_getjobcode.sas

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro dc_getservicecode(loc=,outref=);

%local name;
%let name=%scan(&loc,-1,/);
%mv_getjobcode(path=%substr(&loc,1,%length(&loc)-%length(&name)-1)
  ,name=&name
  ,outref=&outref
)

%mend dc_getservicecode;
