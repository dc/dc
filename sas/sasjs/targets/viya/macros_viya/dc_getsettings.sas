/**
  @file dc_getsettings.sas
  @brief Get settings
  @details There are two versions of this macro - a META and a VIYA version (in
  different folders).  The interfaces is the same.  This version is VIYA.

  <h4> SAS Macros </h4>
  @li mp_abort.sas
  @li mf_getapploc.sas

  @version 3.5
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro dc_getsettings();
%global DC_LIBNAME DC_LIBREF;

%if %symexist(_PROGRAM) %then %let root=&_program;
%else %do;
  %global _metauser;
  %let _metauser=&sysuserid;
  /* to mimic a "real" _program we need to give a dummy role and stp name */
  %let root=/dummyRole/dummyName;
%end;


/* the DC precode is stored in the Admin folder in the root of
  the project.  Lets find that root. */
%put &=root;
%let root=%mf_getapploc();
%put &=root;

/* Now we know the root location we can retrieve the params */
/* only do this if the lib is not assigned - this is an expensive Viya call */

%if x&dc_libref.x=xx %then %do;
  %put fetching settings from API - this is an expensive call;
  %put it is recommended to put these values in the autoexec;
  filename __dc filesrvc folderpath="&root/services";
  %inc __dc(settings)/source2;
%end;


%let DC_LIBNAME=&dc_libref;
%let mpelib=&DC_LIBREF;

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program
  ,msg=%str(Problem running &sysmacroname (&syswarningtext &syserrortext))
)

%mend dc_getsettings;
