/**
  @file dc_createdataset.sas

  <h4> SAS Macros </h4>

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro dc_createdataset(libds=mm_getlibs);
data viewdata;
  var1='Table';
  var2="&libds";
  var3="does not exist!";
run;

%mend dc_createdataset;
