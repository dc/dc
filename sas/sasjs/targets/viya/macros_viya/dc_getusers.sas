/**
  @file dc_getusers.sas
  @brief Gets all available libraries
  @details There are two versions of this macro - a META and a VIYA version (in
  different folders).  The interfaces is the same.  This version is VIYA.

  <h4> SAS Macros </h4>
  @li mv_getusers.sas

  @version 3.5
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro dc_getusers(outds=mm_getlibs);
  %mv_getusers(outds=&outds)
  proc sort data=&outds(rename=(id=uri)) out=&outds(keep=uri name);
    by name;
  run;
%mend dc_getusers;
