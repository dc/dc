/**
  @file
  @brief Gets all groups
  @details There are two versions of this macro - a META and a VIYA version (in
  different folders).  The interfaces is the same.  This version is VIYA.

  Returns the group memberships of a particular user.

  <h4> SAS Macros </h4>
  @li mv_getusergroups.sas

  @version 3.5
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro dc_getusergroups(user=,outds=mm_getgroups);
  %mv_getusergroups(&user,outds=&outds)
  data &outds;
    length groupname groupdesc $256;
    set &outds(rename=(id=groupname name=groupdesc));
  run;
%mend dc_getusergroups;
