/**
  @file dc_getgroups.sas
  @brief Gets all groups
  @details There are two versions of this macro - a META and a VIYA version (in
  different folders).  The interfaces is the same.  This version is META.

  <h4> SAS Macros </h4>
  @li mv_getgroups.sas

  @version 3.4
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro dc_getgroups(outds=mm_getgroups);
  %mv_getgroups(outds=&outds)
  proc sort
      data=&outds(rename=(id=groupuri name=groupname description=groupdesc))
      out=&outds (keep=groupuri groupname groupdesc);
    by groupname;
  run;
%mend dc_getgroups;
