/**
  @file tokenauth.sas
  @brief Get initial tokens using an auth code - DEPRECATED

  <h4> SAS Macros </h4>
  @li mv_tokenauth.sas

  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%webout(FETCH)

data _null_;
  set work.fromjs;
  call symputx('viyasettings',viyasettings);
  call symputx('client_id',client_id);
  call symputx('auth_code',auth_code);
run;

data authme;
  /* get client info from special location */
  infile "&viyasettings" dsd;
  input client_secret:$100.;
  client_id="&client_id";
  auth_code="&auth_code";
run;
/* get tokens */
%mv_tokenauth(inds=authme, outds=fromSAS(keep=access_token refresh_token))

/* send back to frontend */
%webout(OPEN)
%webout(OBJ,fromSAS)
%webout(CLOSE)
