/**
  @file
  @brief self destructing setup service
  @details  Will create the database and perform config activities

  <h4> SAS Macros </h4>
  @li mf_getapploc.sas
  @li mf_mkdir.sas
  @li mf_trimstr.sas
  @li mpe_getvars.sas
  @li mpe_makedata.sas
  @li mpe_makedatamodel.sas
  @li mpe_makesampledata.sas
  @li mv_deletejes.sas

  @version 3.5
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/


%global dcpath ADMIN   ;

%webout(FETCH)

/* enable vars to be passed as url params */
%let exist=%sysfunc(exist(work.fromjs));
%let inds=%sysfunc(ifc(&exist=1,fromjs,_null_));
data _null_;
  set &inds;
  call symputx('dcpath',dcpath);
  call symputx('ADMIN',ADMIN);
run;


options noquotelenmax;

%let dclib=%upcase(VIYA%substr(%sysevalf(%sysfunc(datetime())/60),3,4));
%let dclibname=Data Controller (&dclib);
%let DC_LIBREF=&dclib;
%let work=%sysfunc(pathname(work));


%let dcpath=%mf_trimstr(&dcpath,/)/&dclib;
%put &=sysuserid;
%put &=dcpath;
%put &=admin;

%mf_mkdir(&dcpath)
%mf_mkdir(&dcpath/secret)
%mf_mkdir(&dcpath/dc_staging)


libname &dclib "&dcpath";
%global admin;
%let admin=%sysfunc(coalescec(&admin,All Users));
%mpe_makedatamodel(lib=&dclib)
%mpe_makedata(lib=&dclib,mpeadmins=&admin,path=%str(&dcpath))

/* sample data library */
%mf_mkdir(&dcpath/dc_demo)
libname dcdemo "&dcpath/dc_demo";
%mpe_makesampledata(outlib=DCDEMO)

/* the DC precode is stored in the root of the project */
%let root=%mf_getapploc(&_program)/services;
%put &=root;

filename jobout filesrvc folderpath="&root";
data _null_;
  file jobout('settings.sas');
  put '/* these values are ignored if DC_LIBREF was declared in autoexec */';
  put ' ';
  put '%global DC_LIBREF dc_admin_group dc_staging_area ;';
  put '/* This library (libref) contains the control datasets for DC */';
  put '/* If a different libref must be used, configure it below */';
  put '%let DC_LIBREF=' "&dclib;";
  put ' ';
  put "libname &dclib '&dcpath' ;";
  put ' ';
  put '/* This group has unrestricted access to Data Controller */';
  put '%let dc_admin_group=' "&admin;";
  put ' ';
  put '/* This physical location is used for staging data and audit history */';
  put '%let dc_staging_area=' "&dcpath/dc_staging;";
  put ' ';
  if &syssite in (70221618,70253615) then do;
    put "libname dcdemo '&dcpath/dc_demo';";
  end;
run;


/* create demo data

cas dcsession;
caslib _all_ assign;
caslib casmusic path='/opt/sas/viya/cascache/tracks' libref=casmusic GLOBAL;

proc casutil;
  LOAD DATA=dcdemo.cars
  CASOUT="cars"
  OUTCASLIB="casmusic" PROMOTE ;
run;
*/

/*
cas mysess;
caslib _all_ assign;
data casmusic.artists(promote=yes);
  length name varchar(30);
  do tracks=1 to 100;
    name='Phil Collins'!!cats(tracks);
    output;
  end;
run;
*/
/*
%let url=http://millionsongdataset.com/sites/default/files/AdditionalFiles%trim(
  )/unique_tracks.txt;
filename test url "&url" lrecl=3000 ;

proc sql;
drop table casmusic.tunes;

data tracks;
  infile test dlmstr='<SEP>' dsd  end=lastobs;
  input track_id:$32. song_id:$32. artist_nm:$128. title:$256.;
  output;
  if lastobs then do;
    track_id='dummyrecords';
    title='none';
    artist_nm='none';
    do x=1 to 4000000;
      drop x;
      song_id=cats(x);
      output;
    end;
    stop;
  end;
run;

proc casutil;
  LOAD DATA=tracks
  CASOUT="tunes"
  OUTCASLIB="casmusic" PROMOTE ;
run;
/*
data append;
  if 0 then set &dclib..MPE_TABLES;
  libref="CASMUSIC";
  dsn='TUNES';
  num_of_approvals_required=1;
  loadtype='UPDATE';
  buskey='TRACK_ID SONG_ID';
  tx_from=0;
  tx_to='31DEC9999:23:59:59'dt;
  output;
  dsn='ARTISTS';
  buskey='NAME';
  output;
run;
proc append base=&dclib..MPE_tABLES data=append;
run;
*/
%mp_abort(iftrue=(&syscc ne 0)
  ,mac=&sysmacroname
  ,msg=%str(Err during DB build)
)


%webout(OPEN)
data result;
  dclib="&dclib";
  admingroup="&admin";
  dcpath="&dcpath";
run;
%webout(OBJ,result)
%webout(CLOSE)

%mv_deletejes(path=&root/admin, name=makedata)
