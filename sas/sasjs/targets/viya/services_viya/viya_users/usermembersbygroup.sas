/**
  @file usermembersbygroup.sas
  @brief List the members of a group

  <h4> SAS Macros </h4>
  @li mp_abort.sas
  @li mpeinit.sas
  @li dc_getgroupmembers.sas

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/
%mpeinit()

data _null_;
  set iwant;
  call symputx('groupid',groupid);
run;

%dc_getgroupmembers(%str(&groupid),outds=sasMembers)

proc sort data=sasMembers;
  by membername;
run;

%webout(OPEN)
%webout(OBJ,sasMembers)
%webout(CLOSE)
