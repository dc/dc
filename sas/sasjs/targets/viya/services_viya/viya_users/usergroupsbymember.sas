/**
  @file usergroupsbymember.sas
  @brief List the groups a member is in
  @details Runs without \%mpeinit() - this enables the dropdown to be populated
  during configuration, when the settings service does not yet exist.

  <h4> SAS Macros </h4>
  @li mv_getusergroups.sas
  @li mf_getuser.sas

  @version 3.4
  @author 4GL Apps Ltd
**/

%mv_getusergroups(%mf_getuser(),outds=groups)

proc sort data=groups(rename=(id=uri name=groupname providerid=groupdesc))
  out=groups;
  by groupname;
run;

%webout(OPEN)
%webout(OBJ,groups)
%webout(CLOSE)

