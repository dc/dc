/**
  @file
  @brief Testing mpe_checkrestore macro
  @details Checking functionality of mpe_checkrestore.sas macro

  <h4> SAS Macros </h4>
  @li dc_getsettings.sas
  @li mpe_checkrestore.sas
  @li mp_assert.sas
  @li mp_assertscope.sas
  @li mp_testservice.sas


  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/

/* first, run a data update */
%mp_testservice(&appLoc/tests/services/auditors/postdata.test.1,
  viyacontext=&defaultcontext
)

/* now grab the latest update */
%let loadref=0;
data APPROVE1;
  set &mpelib..mpe_submit end=last;
  if last then call symputx('loadref',table_id);
run;

%global dc_repo_users dc_macros dc_libref dc_staging_area dc_admin_group
  mpelib dc_dttmtfmt;
%dc_getsettings()

%put checking it is restorable;
%global allow_restore reason;
%mp_assertscope(SNAPSHOT)
%mpe_checkrestore(&loadref,outresult=ALLOW_RESTORE,outreason=REASON)
%mp_assertscope(COMPARE,
  desc=Checking macro variables against previous snapshot,
  ignorelist=ALLOW_RESTORE REASON
    MCLIB0_JADP1LEN MCLIB0_JADP2LEN MCLIB0_JADPNUM MCLIB0_JADVLEN
    MCLIB2_JADP1LEN MCLIB2_JADP2LEN MCLIB2_JADPNUM MCLIB2_JADVLEN
)

%mp_assert(
  iftrue=(&syscc=0),
  desc=Checking successful submission
)
%mp_assert(
  iftrue=(&ALLOW_RESTORE=YES),
  desc=Checking restoring is possible
)
%mp_assert(
  iftrue=(&REASON=IS ADMIN),
  desc=Checking reason code returned
)