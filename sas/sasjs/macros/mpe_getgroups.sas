/**
  @file mpe_getgroups.sas
  @brief Gets groups from metadata and augments with DC groups
  @details  Fetches groups using relevant logic (Viya, EBI, Base) and augments
  with the contents of the MPE_GROUPS table.

      %mpe_getgroups(user=&user,outds=work.usergroups)

  <h4> SAS Macros </h4>
  @li dc_getusergroups.sas

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro mpe_getgroups(user=,outds=);

  %if not %symexist(dc_repo_users) %then %let dc_repo_users=foundation;

  %dc_getusergroups(user=&user,outds=&outds)

  data;
    length groupname groupdesc $256;
    set &dc_libref..mpe_groups;
    where &dc_dttmtfmt. lt tx_to;
    where also upcase(user_name)="%upcase(&user)";
    groupname=group_name;
    groupdesc=group_desc;
    keep groupname groupdesc;
  run;

  data &outds;
    set &syslast &outds(keep=groupname groupdesc);
  run;

%mend mpe_getgroups;
