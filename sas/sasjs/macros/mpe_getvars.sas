/**
  @file
  @brief Takes a input table and makes every variable a macro variable
  @details If table has multiple records, only first record is used.

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro mpe_getvars(injs,outds);
  /* load parameters */
  data _null_;
    __dummychar='';__dummynum=0;
    set &outds;
    array __charvals _character_;
    do over __charvals;
      call symputx(vname(__charvals),__charvals,'g');
    end;
    array __numvals _numeric_;
    do over __numvals;
      call symputx(vname(__numvals),__numvals,'g');
    end;
  run;
%mend mpe_getvars;
