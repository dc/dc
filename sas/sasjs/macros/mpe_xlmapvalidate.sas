/**
  @file
  @brief Validates excel map structure and adds load ref
  @details Used in staging, prior to the post edit hook

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro mpe_xlmapvalidate(mperef,inds,dclib,tgtds);

  %local ismap;
  proc sql noprint;
  select count(*) into: ismap
    from &dclib..mpe_xlmap_info
    where XLMAP_TARGETLIBDS="&tgtds" and &dc_dttmtfmt. le TX_TO ;

  %if "&tgtds"="&dclib..MPE_XLMAP_DATA" or &ismap>0 %then %do;
    data &inds;
      set &inds;
      LOAD_REF="&mperef";
    run;
  %end;

%mend mpe_xlmapvalidate;
