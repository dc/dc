/**
  @file
  @brief Termination file for the 4GL Apps Editor
  @details
    enables tracking and performance logging

  <h4> SAS Macros </h4>
  @li mp_abort.sas
  @li mf_getplatform.sas

  @version 9.4
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/


%macro mpeterm();

%local oldloc;
data _null_;
  if symexist('SYSPRINTTOLOG') then oldloc=symget('SYSPRINTTOLOG');
  else oldloc=getoption('LOG');
  if subpad(oldloc,1,1) not in ('"',"'",' ') then oldloc=quote(cats(oldloc));
  call symputx('oldloc',oldloc,'l');
run;

%if %length(&oldloc)>0 %then %do;
  proc printto log=log;
  run;
  data _null_;
    infile &oldloc;
    input; putlog _infile_;
  run;
%end;
%if %sysfunc(exist(&dc_libref..mpe_requests)) and %mf_getplatform() ne SASVIYA
and &DC_REQUEST_LOGS ne NO
%then %do;
  data ;
    if 0 then set &dc_libref..mpe_requests;
    request_dttm=%sysfunc(datetime());
    request_user="%mf_getuser()";
    request_service="%scan(&_program,-2,/)/%scan(&_program,-1,/)";
    request_params='';
    /* sleep random amount to avoid parallel update attempts */
    call sleep(ranuni(0)*0.1,1);
    output;stop;
  proc append base=&dc_libref..mpe_requests data=&syslast force nowarn;
  run;
%end;

%mend mpeterm;
