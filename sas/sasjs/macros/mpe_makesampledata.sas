/**
  @file mpe_makesampledata.sas
  @brief Creates sample data for DC and updates MPE_TABLES
  @details Creates sample data for DC.

  Usage:

      %mpe_makesampledata(outlib=DCxxxx)

  <h4> SAS Macros </h4>

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro mpe_makesampledata(outlib=);
%if &syscc ne 0 %then %do;
  %put syscc=&syscc exiting &sysmacroname;
  %return;
%end;
%if &syssite ne 70221618 and &syssite ne 70253615 %then %do;
  %put syssite=&syssite, exiting &sysmacroname;
  %return;
%end;

data &outlib..class(index=(name /unique));
  set sashelp.class;
run;

data &outlib..cars(index=(carspk=(make model drivetrain) /unique));
  set sashelp.cars;
run;

data &outlib..springs(index=(springspk=(name area latitude) /unique));
  set sashelp.springs;
run;

data &outlib..fmt_checks;;
  pk=1; E8601DA=date();
  format E8601DA E8601DA10.;
run;

data append;
  if 0 then set &dc_libref..mpe_tables;
  TX_FROM=0;
  TX_TO='31DEC9999:23:59:59'dt;
  LIBREF=%upcase("&outlib");
  LOADTYPE='UPDATE';
  NUM_OF_APPROVALS_REQUIRED=1;
  DSN='SPRINGS'; BUSKEY='NAME AREA LATITUDE'; output;
  DSN='CARS'; BUSKEY='MAKE MODEL DRIVETRAIN'; output;
  DSN='CLASS'; BUSKEY='NAME'; output;
  DSN='FMT_CHECKS'; BUSKEY='PK'; output;
run;
proc append base=&dc_libref..MPE_TABLES data=&syslast;
run;

/**
  * DC data extra
  */
%local lib;
%let lib=&dc_libref;
proc sql;
insert into &lib..mpe_row_level_security set
    tx_from=0
    ,tx_to='31DEC5999:23:59:59'dt
    ,RLS_RK=4
    ,RLS_SCOPE='ALL'
    ,RLS_GROUP='sec-sas9-prd-int-sasplatform-300114sasjs'
    ,RLS_LIBREF="&lib."
    ,RLS_TABLE="MPE_TABLES"
    ,RLS_GROUP_LOGIC='AND'
    ,RLS_SUBGROUP_LOGIC='OR'
    ,RLS_SUBGROUP_ID=0
    ,RLS_VARIABLE_NM='NUM_OF_APPROVALS_REQUIRED'
    ,RLS_OPERATOR_NM='>'
    ,RLS_RAW_VALUE='0'
    ,RLS_ACTIVE=1;
insert into &lib..mpe_row_level_security set
    tx_from=0
    ,tx_to='31DEC5999:23:59:59'dt
    ,RLS_RK=5
    ,RLS_SCOPE='ALL'
    ,RLS_GROUP='sec-sas9-prd-int-sasplatform-300114sasjs'
    ,RLS_LIBREF="&lib."
    ,RLS_TABLE="MPE_ROW_LEVEL_SECURITY"
    ,RLS_GROUP_LOGIC='AND'
    ,RLS_SUBGROUP_LOGIC='OR'
    ,RLS_SUBGROUP_ID=1
    ,RLS_VARIABLE_NM='RLS_GROUP_LOGIC'
    ,RLS_OPERATOR_NM='NOT IN'
    ,RLS_RAW_VALUE="('N/A1','N/A2')"
    ,RLS_ACTIVE=1;
insert into &lib..mpe_row_level_security set
    tx_from=0
    ,tx_to='31DEC5999:23:59:59'dt
    ,RLS_RK=6
    ,RLS_SCOPE='ALL'
    ,RLS_GROUP='sec-sas9-prd-int-sasplatform-300114sasjs'
    ,RLS_LIBREF="&lib."
    ,RLS_TABLE="MPE_ROW_LEVEL_SECURITY"
    ,RLS_GROUP_LOGIC='AND'
    ,RLS_SUBGROUP_LOGIC='OR'
    ,RLS_SUBGROUP_ID=1
    ,RLS_VARIABLE_NM='RLS_GROUP_LOGIC'
    ,RLS_OPERATOR_NM='NOT IN'
    ,RLS_RAW_VALUE="('N/A1','N/A2','N/A3')"
    ,RLS_ACTIVE=1;
insert into &lib..mpe_row_level_security set
    tx_from=0
    ,tx_to='31DEC5999:23:59:59'dt
    ,RLS_RK=7
    ,RLS_SCOPE='ALL'
    ,RLS_GROUP='sec-sas9-prd-int-sasplatform-300114sasjs'
    ,RLS_LIBREF="&lib."
    ,RLS_TABLE="MPE_ROW_LEVEL_SECURITY"
    ,RLS_GROUP_LOGIC='AND'
    ,RLS_SUBGROUP_LOGIC='OR'
    ,RLS_SUBGROUP_ID=2
    ,RLS_VARIABLE_NM='RLS_GROUP_LOGIC'
    ,RLS_OPERATOR_NM='NOT IN'
    ,RLS_RAW_VALUE="('N/A1','N/A2')"
    ,RLS_ACTIVE=1;
insert into &lib..mpe_row_level_security set
    tx_from=0
    ,tx_to='31DEC5999:23:59:59'dt
    ,RLS_RK=8
    ,RLS_SCOPE='ALL'
    ,RLS_GROUP='sec-sas9-prd-int-sasplatform-300114sasjs'
    ,RLS_LIBREF="&lib."
    ,RLS_TABLE="MPE_ROW_LEVEL_SECURITY"
    ,RLS_GROUP_LOGIC='AND'
    ,RLS_SUBGROUP_LOGIC='OR'
    ,RLS_SUBGROUP_ID=3
    ,RLS_VARIABLE_NM='RLS_GROUP_LOGIC'
    ,RLS_OPERATOR_NM='NOT IN'
    ,RLS_RAW_VALUE="('N/A1','N/A2')"
    ,RLS_ACTIVE=1;
insert into &lib..mpe_row_level_security set
    tx_from=0
    ,tx_to='31DEC5999:23:59:59'dt
    ,RLS_RK=9
    ,RLS_SCOPE='ALL'
    ,RLS_GROUP='sec-sas9-prd-int-sasplatform-300114sasjs'
    ,RLS_LIBREF="&lib."
    ,RLS_TABLE="MPE_ROW_LEVEL_SECURITY"
    ,RLS_GROUP_LOGIC='AND'
    ,RLS_SUBGROUP_LOGIC='OR'
    ,RLS_SUBGROUP_ID=4
    ,RLS_VARIABLE_NM='RLS_GROUP_LOGIC'
    ,RLS_OPERATOR_NM='NOT IN'
    ,RLS_RAW_VALUE="('N/A1','N/A2')"
    ,RLS_ACTIVE=1;
insert into &lib..mpe_row_level_security set
    tx_from=0
    ,tx_to='31DEC5999:23:59:59'dt
    ,RLS_RK=10
    ,RLS_SCOPE='ALL'
    ,RLS_GROUP='sec-sas9-prd-int-sasplatform-300114sasjs'
    ,RLS_LIBREF="&lib."
    ,RLS_TABLE="MPE_ROW_LEVEL_SECURITY"
    ,RLS_GROUP_LOGIC='AND'
    ,RLS_SUBGROUP_LOGIC='OR'
    ,RLS_SUBGROUP_ID=5
    ,RLS_VARIABLE_NM='RLS_GROUP_LOGIC'
    ,RLS_OPERATOR_NM='NOT IN'
    ,RLS_RAW_VALUE="('N/A1','N/A2','N/A3','N/A4','N/A5','N/A6','N/A7')"
    ,RLS_ACTIVE=1;
insert into &lib..mpe_row_level_security set
    tx_from=0
    ,tx_to='31DEC5999:23:59:59'dt
    ,RLS_RK=11
    ,RLS_SCOPE='ALL'
    ,RLS_GROUP='sec-sas9-prd-int-sasplatform-300114sasjs'
    ,RLS_LIBREF="&lib."
    ,RLS_TABLE="MPE_ROW_LEVEL_SECURITY"
    ,RLS_GROUP_LOGIC='AND'
    ,RLS_SUBGROUP_LOGIC='OR'
    ,RLS_SUBGROUP_ID=6
    ,RLS_VARIABLE_NM='RLS_GROUP_LOGIC'
    ,RLS_OPERATOR_NM='NOT IN'
    ,RLS_RAW_VALUE="('N/A1','N/A2','N/A3','N/A4','N/A5','N/A6','N/A7')"
    ,RLS_ACTIVE=1;
insert into &lib..mpe_row_level_security set
    tx_from=0
    ,tx_to='31DEC5999:23:59:59'dt
    ,RLS_RK=12
    ,RLS_SCOPE='ALL'
    ,RLS_GROUP='sec-sas9-prd-int-sasplatform-300114sasjs'
    ,RLS_LIBREF="&lib."
    ,RLS_TABLE="MPE_ROW_LEVEL_SECURITY"
    ,RLS_GROUP_LOGIC='AND'
    ,RLS_SUBGROUP_LOGIC='OR'
    ,RLS_SUBGROUP_ID=7
    ,RLS_VARIABLE_NM='RLS_GROUP_LOGIC'
    ,RLS_OPERATOR_NM='NOT IN'
    ,RLS_RAW_VALUE="('N/A1','N/A2','N/A3','N/A4','N/A5','N/A6','N/A7')"
    ,RLS_ACTIVE=1;
insert into &lib..mpe_row_level_security set
    tx_from=13
    ,tx_to='31DEC5999:23:59:59'dt
    ,RLS_RK=5
    ,RLS_SCOPE='ALL'
    ,RLS_GROUP='sec-sas9-prd-ext-sasplatform-300114sasjs'
    ,RLS_LIBREF="&lib."
    ,RLS_TABLE="MPE_ROW_LEVEL_SECURITY"
    ,RLS_GROUP_LOGIC='AND'
    ,RLS_SUBGROUP_LOGIC='OR'
    ,RLS_SUBGROUP_ID=1
    ,RLS_VARIABLE_NM='RLS_GROUP_LOGIC'
    ,RLS_OPERATOR_NM='NOT IN'
    ,RLS_RAW_VALUE="('N/A1','N/A2')"
    ,RLS_ACTIVE=1;
insert into &lib..mpe_row_level_security set
    tx_from=14
    ,tx_to='31DEC5999:23:59:59'dt
    ,RLS_RK=6
    ,RLS_SCOPE='ALL'
    ,RLS_GROUP='sec-sas9-prd-ext-sasplatform-300114sasjs'
    ,RLS_LIBREF="&lib."
    ,RLS_TABLE="MPE_ROW_LEVEL_SECURITY"
    ,RLS_GROUP_LOGIC='AND'
    ,RLS_SUBGROUP_LOGIC='OR'
    ,RLS_SUBGROUP_ID=1
    ,RLS_VARIABLE_NM='RLS_GROUP_LOGIC'
    ,RLS_OPERATOR_NM='NOT IN'
    ,RLS_RAW_VALUE="('N/A1','N/A2','N/A3')"
    ,RLS_ACTIVE=1;
insert into &lib..mpe_row_level_security set
    tx_from=15
    ,tx_to='31DEC5999:23:59:59'dt
    ,RLS_RK=7
    ,RLS_SCOPE='ALL'
    ,RLS_GROUP='sec-sas9-prd-ext-sasplatform-300114sasjs'
    ,RLS_LIBREF="&lib."
    ,RLS_TABLE="MPE_ROW_LEVEL_SECURITY"
    ,RLS_GROUP_LOGIC='AND'
    ,RLS_SUBGROUP_LOGIC='OR'
    ,RLS_SUBGROUP_ID=2
    ,RLS_VARIABLE_NM='RLS_GROUP_LOGIC'
    ,RLS_OPERATOR_NM='NOT IN'
    ,RLS_RAW_VALUE="('N/A1','N/A2')"
    ,RLS_ACTIVE=1;
insert into &lib..mpe_row_level_security set
    tx_from=16
    ,tx_to='31DEC5999:23:59:59'dt
    ,RLS_RK=8
    ,RLS_SCOPE='ALL'
    ,RLS_GROUP='sec-sas9-prd-ext-sasplatform-300114sasjs'
    ,RLS_LIBREF="&lib."
    ,RLS_TABLE="MPE_ROW_LEVEL_SECURITY"
    ,RLS_GROUP_LOGIC='AND'
    ,RLS_SUBGROUP_LOGIC='OR'
    ,RLS_SUBGROUP_ID=3
    ,RLS_VARIABLE_NM='RLS_GROUP_LOGIC'
    ,RLS_OPERATOR_NM='NOT IN'
    ,RLS_RAW_VALUE="('N/A1','N/A2')"
    ,RLS_ACTIVE=1;
insert into &lib..mpe_row_level_security set
    tx_from=17
    ,tx_to='31DEC5999:23:59:59'dt
    ,RLS_RK=9
    ,RLS_SCOPE='ALL'
    ,RLS_GROUP='sec-sas9-prd-ext-sasplatform-300114sasjs'
    ,RLS_LIBREF="&lib."
    ,RLS_TABLE="MPE_ROW_LEVEL_SECURITY"
    ,RLS_GROUP_LOGIC='AND'
    ,RLS_SUBGROUP_LOGIC='OR'
    ,RLS_SUBGROUP_ID=4
    ,RLS_VARIABLE_NM='RLS_GROUP_LOGIC'
    ,RLS_OPERATOR_NM='NOT IN'
    ,RLS_RAW_VALUE="('N/A1','N/A2')"
    ,RLS_ACTIVE=1;
insert into &lib..mpe_row_level_security set
    tx_from=18
    ,tx_to='31DEC5999:23:59:59'dt
    ,RLS_RK=10
    ,RLS_SCOPE='ALL'
    ,RLS_GROUP='sec-sas9-prd-ext-sasplatform-300114sasjs'
    ,RLS_LIBREF="&lib."
    ,RLS_TABLE="MPE_ROW_LEVEL_SECURITY"
    ,RLS_GROUP_LOGIC='AND'
    ,RLS_SUBGROUP_LOGIC='OR'
    ,RLS_SUBGROUP_ID=5
    ,RLS_VARIABLE_NM='RLS_GROUP_LOGIC'
    ,RLS_OPERATOR_NM='NOT IN'
    ,RLS_RAW_VALUE="('N/A1','N/A2','N/A3','N/A4','N/A5','N/A6','N/A7')"
    ,RLS_ACTIVE=1;
insert into &lib..mpe_row_level_security set
    tx_from=0
    ,tx_to='31DEC5999:23:59:59'dt
    ,RLS_RK=19
    ,RLS_SCOPE='ALL'
    ,RLS_GROUP='sec-sas9-prd-ext-sasplatform-300114sasjs'
    ,RLS_LIBREF="&lib."
    ,RLS_TABLE="MPE_ROW_LEVEL_SECURITY"
    ,RLS_GROUP_LOGIC='AND'
    ,RLS_SUBGROUP_LOGIC='OR'
    ,RLS_SUBGROUP_ID=6
    ,RLS_VARIABLE_NM='RLS_GROUP_LOGIC'
    ,RLS_OPERATOR_NM='NOT IN'
    ,RLS_RAW_VALUE="('N/A1','N/A2','N/A3','N/A4','N/A5','N/A6','N/A7')"
    ,RLS_ACTIVE=1;
insert into &lib..mpe_row_level_security set
    tx_from=0
    ,tx_to='31DEC5999:23:59:59'dt
    ,RLS_RK=20
    ,RLS_SCOPE='ALL'
    ,RLS_GROUP='sec-sas9-prd-ext-sasplatform-300114sasjs'
    ,RLS_LIBREF="&lib."
    ,RLS_TABLE="MPE_ROW_LEVEL_SECURITY"
    ,RLS_GROUP_LOGIC='AND'
    ,RLS_SUBGROUP_LOGIC='OR'
    ,RLS_SUBGROUP_ID=7
    ,RLS_VARIABLE_NM='RLS_GROUP_LOGIC'
    ,RLS_OPERATOR_NM='NOT IN'
    ,RLS_RAW_VALUE="('N/A1','N/A2','N/A3','N/A4','N/A5','N/A6','N/A7')"
    ,RLS_ACTIVE=1;


/** create excel config */
insert into &lib..MPE_EXCEL_CONFIG set
    tx_from=0
    ,xl_libref="&lib"
    ,xl_table="MPE_DATADICTIONARY"
    ,xl_column="DD_LONGDESC"
    ,xl_rule="FORMULA"
    ,xl_active=1
    ,tx_to='31DEC5999:23:59:59'dt;

/** mpe_security table */
insert into &lib..mpe_security set
    tx_from=0
    ,libref="*ALL*"
    ,dsn="*ALL*"
    ,access_level="APPROVE"
    ,sas_group="303001.DataController"
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..mpe_security set
    tx_from=0
    ,libref="*ALL*"
    ,dsn="*ALL*"
    ,access_level="EDIT"
    ,sas_group="303001.DataController"
    ,tx_to='31DEC5999:23:59:59'dt;

data append;
  if 0 then set &dc_libref..mpe_tables;
  TX_FROM=0;
  TX_TO='31DEC9999:23:59:59'dt;
  LIBREF=%upcase("&dc_libref");
  LOADTYPE='UPDATE';
  NUM_OF_APPROVALS_REQUIRED=1;
  DSN='MPE_USERS'; BUSKEY='USER_ID'; output;
run;
proc append base=&dc_libref..MPE_TABLES data=&syslast;
run;

%mend mpe_makesampledata;
