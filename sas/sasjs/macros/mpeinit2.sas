/**
  @file mpeinit2.sas
  @brief Helper macro to enable interactive debugging
  @details

  Just need two variables:

      %let apploc=
      %let mpelib=

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

  <h4> SAS Macros </h4>
  @li mf_fmtdttm.sas

**/

%macro mpeinit2(lib);
%global mpeinit
        mpeadmins /* group with unrestricted Meditor access */
        mpelocapprovals /* location for landing and staging files */
        mpelib /* location of configuration tables for DC */
        dc_repo_users /* location of user / group metadata */
        ;
  %global _metauser;
  %let _metauser=&sysuserid;
  /* to mimic a "real" _program we need to give a dummy role and stp name */
  %let root=/dummyRole/dummyName;

  %global dc_repo_users;
  %let dc_repo_users=foundation;

  %let dc_dttmtfmt="%sysfunc(datetime(),%mf_fmtdttm())"dt;
  %put &=dc_dttmtfmt;

%mend mpeinit2;
