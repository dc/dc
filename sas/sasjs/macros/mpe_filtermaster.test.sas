/**
  @file
  @brief setup mpe_filtermaster macro test
  @details requires some filter rks, so step 1 is to trigger the validatefilter
  STP

  <h4> SAS Macros </h4>
  @li mp_abort.sas
  @li mp_assertdsobs.sas
  @li mpe_filtermaster.sas
  @li mp_filterstore.sas

  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/
/*
proc printto log="/tmp/dcviya/%scan(&_program,-1,/)_%sysfunc(datetime()).log";
run;
*/

/**
  * STEP 1 - first, make filter
  */
data work.inquery;
  infile datalines4 dsd;
  input GROUP_LOGIC:$3. SUBGROUP_LOGIC:$3. SUBGROUP_ID:8. VARIABLE_NM:$32.
    OPERATOR_NM:$10. RAW_VALUE:$4000.;
datalines4;
AND,AND,1,LIBREF,CONTAINS,"'DC'"
AND,OR,2,DSN,=,"'MPE_LOCK_ANYTABLE'"
;;;;
run;
%mp_filterstore(
  libds=&dc_libref..MPE_TABLES,
  filter_summary=&dc_libref..mpe_filteranytable,
  filter_detail=&dc_libref..mpe_filtersource,
  lock_table=&dc_libref..mpe_lockanytable,
  maxkeytable=&dc_libref..mpe_maxkeyvalues,
  queryds=work.inquery,
  outresult=work.result,
  outquery=work.query
)
data _null_;
  set work.result;
  call symputx('filter_rk',filter_rk);
  putlog (_all_)(=);
run;


/**
  * begin testing!
  */

/*
  Test 1 - Expect 5 records:

    ((
        LIBREF CONTAINS 'DC'
    ) AND (
        DSN = 'MPE_LOCK_ANYTABLE'
    ))

*/
%mpe_filtermaster(VIEW,&dclib..MPE_TABLES,
  dclib=&dclib,
  filter_rk=&filter_rk,
  outref=qref
)
data work.out;
  infile qref;
  input;
  put _infile_;
run;
%mp_assertdsobs(work.out,
  desc=Test 1 - Five records from simple query,
  test=EQUALS 5,
  outds=work.test_results
)

/*
  Test 2 - Expect 6 records from an EDIT query

    ((
        LIBREF CONTAINS 'DC'
    ) AND (
        DSN = 'MPE_LOCK_ANYTABLE'
    ))
    AND ( %sysfunc(datetime()) < TX_TO )

*/

%mpe_filtermaster(EDIT,&dclib..MPE_TABLES,
  dclib=&dclib,
  filter_rk=&filter_rk,
  outref=qref
)
data work.out;
  infile qref;
  input;
  put _infile_;
run;
%mp_assertdsobs(work.out,
  desc=Test 2 - Six records from EDIT query,
  test=EQUALS 6,
  outds=work.test_results
)

/*
  Test 3 - Empty query


*/
data work.class;
  do age=1 to 19;
    weight=age*1.3;
    output;
  end;
run;
%mpe_filtermaster(VIEW,work.class,
  dclib=&dclib,
  filter_rk=-1,
  outref=qref3
)
data _null_;
  infile qref3;
  input;
  putlog _infile_;
run;
data work.out3;
  set work.class;
  where %inc qref3;;
run;
%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&sysmacroname
  ,msg=%str(syscc=&syscc in Test 3)
)
%mp_assertdsobs(work.out3,
  desc=Test 3 - all records returned due to empty query,
  test=EQUALS 19,
  outds=work.test_results
)
