/**
  @file
  @brief Testing mpe_getversions macro
  @details Checking functionality of mpe_getversions.sas macro

  <h4> SAS Macros </h4>
  @li mf_nobs.sas
  @li mp_assert.sas
  @li mp_assertscope.sas
  @li mpe_getversions.sas
  @li mpe_targetloader.sas

**/

/* run the macro*/
%mp_assertscope(SNAPSHOT)
%mpe_getversions(&mpelib,&mpelib,MPE_DATADICTIONARY, outds=ds0)
%mp_assertscope(COMPARE,
  desc=Checking macro variables against previous snapshot
)


/* now stage some data */

%let f1=%mf_getuniquefileref();
data _null_;
  file &f1 termstr=crlf;
  put 'ACTION:$char4. MESSAGE:$char40. LIBDS:$char38.';
  put "LOAD,staging some data,&dclib..MPE_DATADICTIONARY";
run;
data work.jsdata;
  set &mpelib..MPE_DATADICTIONARY;
  _____DELETE__THIS__RECORD_____='No';
  dd_source=cats(ranuni(0));
  output;
  stop;
run;
%mx_testservice(&appLoc/services/editors/stagedata,
  viyacontext=&defaultcontext,
  inputfiles=&f1:sascontroltable,
  inputdatasets=jsdata,
  outlib=web1,
  mdebug=&sasjs_mdebug
)

%let status=0;
data work.sasparams;
  set web1.sasparams;
  putlog (_all_)(=);
  if status='SUCCESS' then call symputx('status',1);
  call symputx('dsid',dsid);
run;
%mp_assert(
  iftrue=(&status=1),
  desc=Checking staged data component,
  outds=work.test_results
)

/* now approve the data so the change is applied */
data work.sascontroltable;
  ACTION='APPROVE_TABLE';
  TABLE="&dsid";
  DIFFTIME="%sysfunc(datetime(),datetime19.)";
  output;
  stop;
run;
%mx_testservice(&appLoc/services/auditors/postdata,
  viyacontext=&defaultcontext,
  inputdatasets=work.sascontroltable,
  outlib=web2,
  outref=wbout,
  mdebug=&sasjs_mdebug
)


/* finally - check that we have an extra version! */

%mpe_getversions(&mpelib,&mpelib,MPE_DATADICTIONARY, outds=ds1)

%mp_assert(
  iftrue=(%mf_nobs(ds0) = %mf_nobs(ds1)-1),
  desc=Checking one extra version was created
)

