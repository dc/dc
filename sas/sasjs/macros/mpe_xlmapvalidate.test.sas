/**
  @file
  @brief Testing mpe_xlmapvalidate macro

  <h4> SAS Macros </h4>
  @li mpe_xlmapvalidate.sas
  @li mp_assert.sas
  @li mp_assertscope.sas

  <h4> SAS Includes </h4>
  @li mpe_xlmap_data.ddl ul

  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/

/* create the table */
%let curlib=work;
proc sql;
%inc ul;

data work.test1;
  if 0 then set work.MPE_XLMAP_DATA;
  LOAD_REF='0';
  XLMAP_ID='Sample';
  XLMAP_RANGE_ID='Range 1';
  ROW_NO=1;
  COL_NO=2;
  VALUE_TXT='something';
run;

%mp_assertscope(SNAPSHOT)
%mpe_xlmapvalidate(DCTEST1,work.test1,&dclib,NOT.MAP)
%mp_assertscope(COMPARE,
  desc=Checking macro variables against previous snapshot
)

data _null_;
  set work.test1;
  call symputx('test1',load_ref);
run;
%mp_assert(
  iftrue=(&test1=0),
  desc=Checking load ref was not applied
)

%mpe_xlmapvalidate(DCTEST2,work.test1,&dclib,&dclib..MPE_XLMAP_DATA)

data _null_;
  set work.test1;
  call symputx('test2',load_ref);
run;
%mp_assert(
  iftrue=(&test2=DCTEST2),
  desc=Checking load ref was applied for default case
)