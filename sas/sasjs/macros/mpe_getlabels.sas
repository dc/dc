/**
  @file mpe_getlabels.sas
  @brief Gets the table and column labels for a particular table
  @details Takes labels first from mpe_datadictionary then from table metadata.

  <h4> SAS Macros </h4>
  @li mf_getuniquename.sas

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro mpe_getlabels(type,source,outds=mpe_getlabels);
%local tmpds;

%if &type=COLUMNS %then %do;
    %let tmpds=%mf_getuniquename();
    proc contents noprint data=&source
      out=&tmpds(keep=name memlabel label);
    run;
    proc sql ;
    create table &outds as
    select upcase(a.name) as name
        ,a.memlabel
        ,coalesce(b.dd_shortdesc,a.label) as desc
        ,b.dd_longdesc as longdesc
    from &tmpds a
    left join &mpelib..mpe_datadictionary
        (where=(&dc_dttmtfmt. < tx_to
        and dd_source ? %upcase("&source")
        and dd_type='COLUMN')) b
    on scan(b.dd_source,-1,'.')=upcase(a.name);
%end;

%mend mpe_getlabels;
