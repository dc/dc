/**
  @file mpeinit.sas
  @brief Initialisation file for Data Controller- sets SASAUTOs etc
  @details Runs at the beginning of every service.  Used to configure settings,
  options and macro variables.  Requires the staging area and DC library to be
  preconfigured.

  <h4> SAS Macros </h4>
  @li dc_getsettings.sas
  @li mf_fmtdttm.sas
  @li mf_getuser.sas
  @li mp_abort.sas
  @li mp_init.sas

  @param fetch= Fetch the SASjs tables.  Use NO if not loading SASjs content,
  eg for file upload.

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/


%macro mpeinit(fetch=YES);
%global mpeinit
        mpeadmins /* group with unrestricted Meditor access */
        mpelocapprovals /* location for landing and staging files */
        mpelib /* location of configuration tables for DC */
        dc_repo_users /* location of user / group metadata */
        /* extracted in dc_getsettings */
        dc_activation_key
        dc_licence_key
        dc_locale
        dc_request_logs
        dc_restrict_viewer
        dc_dttmtfmt /* can be overridden in dc_getsettings */
        _debug /* automatic variable when provided in URL */
        sasjs_mdebug /* used to show extra info when _debug is enabled */
        ;

%if &mpeinit=1 %then %return;
%else %let mpeinit=1;
%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program
  ,msg=%str(Problem on service startup (&syswarningtext &syserrortext))
)

%mp_init()

%if &fetch=YES %then %do;
  %webout(FETCH)
%end;

%global _CLIENTNAME;
%mp_abort(iftrue= (&_CLIENTNAME=SAS Enterprise Guide)
  ,mac=&_program..sas
  ,msg=%str(Data Controller is a web app and should not be executed from EG)
)

options urlencoding=utf8 nobomfile lrecl=32767;

%let perf=%sysfunc(datetime());
%put perfdiff: 0;

%let dc_locale=SYSTEM; /* default if not set */

/**
  * E8601DT26.6 has widest database support - but not all SAS flavours can
  * handle it.  Override in the settings STP if needed.
  */
data _null_;
  dc_dttmtfmt='"%sysfunc(datetime(),'!!"%mf_fmtdttm()"!!')"dt';
  call symputx('dc_dttmtfmt',dc_dttmtfmt);
  put dc_dttmtfmt=;
run;

%put &=dc_dttmtfmt;

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program
  ,msg=%str(syscc=&syscc prior to dc_getsettings)
)

%dc_getsettings()

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program
  ,msg=%str(syscc=&syscc after dc_getsettings)
)

data _null_;
  set &DC_LIBREF..mpe_config(where=(
    var_scope="DC"
    and &dc_dttmtfmt lt tx_to
    and var_active=1
  ));
  call symputx(var_name,var_value,'G');
  putlog var_name "=" var_value;
run;

%let mpelib=&dc_libref;
%let mpeadmins=&dc_admin_group;
%let mpelocapprovals=&dc_staging_area;
%let dc_repo_users=&dc_repo_users;

%if &dc_locale ne SYSTEM %then %do;
  options locale=&dc_locale;
%end;

%mp_abort(iftrue= (&syscc ne 0)
  ,mac=&_program..sas
  ,msg=%str(Problem during compilation or with STP precode (&syswarningtext))
)

%if "&_debug"="2477" or "&_debug"="fields,log,trace" or "&_debug"="131"
%then %do;
  %let sasjs_mdebug=1;
%end;

%mend mpeinit;
