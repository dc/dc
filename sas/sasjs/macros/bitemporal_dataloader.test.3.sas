/**
  @file
  @brief Test Harness for bitemporal dataloader
  @details testing special numerics

  <h4> SAS Macros </h4>
  @li bitemporal_dataloader.sas
  @li mp_assert.sas
  @li mf_nobs.sas

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/


%let syscc=0;
%let now=%sysfunc(datetime());
%let high_date='31DEC8888:23:59:59'dt;

/* create base table */
data basetable;
  PK='PK1';field1='somevalue';numvar=.; output;
  PK='PK2';field1='newvalue';numvar=0; output;
  PK='PK3';field1='somevalue';numvar=._; output;
  PK='PK4';field1='newvalue';numvar=.z; output;
run;

data appendtable;
  set basetable;
  if numvar=._ then numvar=.a;
  if numvar=.z then numvar=.y;
run;
libname work2(work);

proc sql;
create table work.mpe_dataloads(
          libref varchar(8) ,
          dsn varchar(32) ,
          etlsource varchar(100) ,
          loadtype varchar(20) ,
          changed_records int,
          new_records int,
          deleted_records int,
          duration num,
          user_nm varchar(50) ,
          processed_dttm num format=datetime19.3,
          mac_ver varchar(5)
);quit;
proc datasets lib=work noprint;
  modify mpe_dataloads;
  index create
    pk_mpe_dataloads=(processed_dttm libref dsn etlsource)
    /nomiss unique;
quit;

%bitemporal_dataloader(dclib=work2
    ,PK=pk
    ,ETLSOURCE=&_program
    ,base_dsn=BASETABLE
    ,LOG=1
    ,outds_mod=work.changes
    ,loadtype=UPDATE
)

%mp_assert(iftrue=(&syscc=0),
  desc=Testing long ETLSOURCE value
)

%mp_assert(iftrue=(%mf_nobs(work.changes)=2),
  desc=Testing special missings
)


