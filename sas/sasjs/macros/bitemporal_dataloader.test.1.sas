/**
  @file
  @brief Test Harness for bitemporal dataloader
  @details see below for usage:

      options mprint;
      options insert=(sasautos="/pub/programs/macrocore/base");
      options insert=(sasautos="/pub/programs/macrocore/meta");
      options insert=(sasautos="/pub/programs/datacontroller/macros");

      %mpeinit()

      %bitemporal_tester(test=1)

  TODO - add short numerics!!!


  <h4> SAS Macros </h4>
  @li bitemporal_dataloader.sas
  @li mp_assert.sas

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/


%let syscc=0;
%let now=%sysfunc(datetime());
%let high_date='31DEC8888:23:59:59'dt;

/* create base table */
data basetable;
  tx_from_dttm=0;tx_to_dttm=&high_date; PK='PK1';
  eff_from_dttm=0; eff_to_dttm=&now-10000; field1='somevalue';
  field2='someothervalue'; output;
  eff_from_dttm=&now-10000; eff_to_dttm=&high_date; PK='PK1'; field1='newvalue';
  field2='somenewvalue'; output;
run;

data appendtable;
  if 0 then set basetable;
  eff_from_dttm=&now-500; eff_to_dttm=&now+500; pk='PK1'; field1='blah';
  field2='blah';output;
  call symputx('x',repeat('x',200));
run;
libname work2(work);

/* create mpe_dataloads table */
proc sql;
create table work.mpe_dataloads(
          libref varchar(8) ,
          dsn varchar(32) ,
          etlsource varchar(100) ,
          loadtype varchar(20) ,
          changed_records int,
          new_records int,
          deleted_records int,
          duration num,
          user_nm varchar(50) ,
          processed_dttm num format=datetime19.3,
          mac_ver varchar(5)
);quit;
proc datasets lib=work noprint;
  modify mpe_dataloads;
  index create
    pk_mpe_dataloads=(processed_dttm libref dsn etlsource)
    /nomiss unique;
quit;

%bitemporal_dataloader(dclib=work2
    ,processed=
    ,PK=pk
    ,ETLSOURCE=&x
    ,base_dsn=BASETABLE
    ,bus_from=eff_from_dttm, bus_to=eff_to_dttm
    ,tech_from=tx_from_dttm, tech_to=tx_to_dttm
    ,LOG=1
)

%mp_assert(iftrue=(&syscc=0),
  desc=Testing long ETLSOURCE value
)


