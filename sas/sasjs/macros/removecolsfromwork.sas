/**
  @file removecolsfromwork.sas
  @brief Temp fix to remove md5 content from tables
  @details
proc json cannot handle the special characters in the md5 hash

  <h4> SAS Macros </h4>
  @li mp_dropmembers.sas
  @li mf_isblank.sas

  @version 9.4
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro removecolsfromwork(col);
/* only an issue if debug mode enabled */
%global _debug;
%if &_debug ge 131 %then %do;
  %let col=%upcase(&col);

  %local memlist;
  proc sql noprint;
  select distinct memname into: memlist
    separated by ' '
    from dictionary.columns
    where libname='WORK' and upcase(name)="&col";

  %if %mf_isblank(&memlist) %then %return;

  %mp_dropmembers(list=&memlist)

%end;
%mend removecolsfromwork;

