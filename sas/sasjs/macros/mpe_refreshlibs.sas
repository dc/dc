/**
  @file mpe_refreshlibs.sas
  @brief Refreshes the library data catalog
  @details

  <h4> SAS Macros </h4>
  @li dc_getlibs.sas
  @li mf_getplatform.sas
  @li bitemporal_dataloader.sas

  @version 9.3
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/

%macro mpe_refreshlibs(lib=0);

%dc_getlibs(outds=work.mm_getLibs)
proc sort data=mm_getlibs;
  by libraryref libraryname;
run;

data libs0;
  set mm_getlibs;
  by libraryref;
%if &lib ne 0 %then %do;
  where upcase(libraryref)="%upcase(&lib)";
%end;
  if "%mf_getplatform()"="SASMETA" then do;
  /* note - invalid libraries can result in exception errors.  If this happens,
    configure the dc_viewlib_check variable to NO in Data Controller Settings */
    rc=libname(libraryref,,'meta',cats('library="',libraryname,'";'));
    drop rc;
    if rc ne 0 then do;
      putlog "NOTE:  Library " libraryname " does not exist!!";
      putlog (_all_) (=);
      delete;
    end;
  end;
  if not first.libraryref then delete;
run;

proc sql;
create table libs1 as
  select distinct libname
  ,engine
  ,path
    ,level
    ,sysname
  ,sysvalue
  from dictionary.libnames
  order by libname, level,engine,path;

data libs2;
  set libs1;
  length tran $1024;
  if missing(sysname) then sysname='Missing';
  select(sysname);
    when('Access Permission') tran='Permissions';
    when('Owner Name') tran='Owner';
    when('Schema/Owner') tran='schema';
    otherwise tran=sysname;
  end;
run;

proc transpose data=libs2 out=libs3;
by libname  level engine path;
var sysvalue;
id tran;
run;

data libs4(rename=(libname=libref));
  length paths $8192 perms owners schemas $500 permissions owner schema $1024;
  if _n_=1 then call missing (of _all_);
  set libs3;
  by libname;
  if engine='V9' then engine='BASE';
  if first.libname then do;
    retain paths perms owners schemas;
    paths='('!!quote(trim(path));
    perms=permissions;
    owners=owner;
    schemas=schema;
  end;
  else do;
    paths=trim(paths)!!' '!!quote(trim(path));
    perms=trim(perms)!!','!!trim(permissions);
    owners=trim(owners)!!','!!trim(owner);
    schemas=trim(schemas)!!' '!!trim(schema);
  end;
  if last.libname then do;
    paths=trim(paths)!!')';
    schemas=cats(schemas);
    output;
  end;
  keep libname engine paths perms owners schemas;
run;

proc sql;
create table libs5 as
  select a.libref
    ,coalescec(b.engine,a.engine) as engine length=32
    ,b.libraryname as libname
    ,a.paths
    ,a.perms
    ,a.owners
    ,a.schemas
    ,b.libraryid as libid
  from libs4 a
  left join libs0 b
  on upcase(a.libref)=upcase(b.libraryref)
  where libref not in ('SASWORK','WORK','SASUSER','CASUSER','TEMP','STPSAMP'
    ,'MAPSGFK');

%bitemporal_dataloader(base_lib=&dc_libref
  ,base_dsn=MPE_DATACATALOG_LIBS
  ,append_dsn=libs5
  ,PK=LIBREF
  ,etlsource=&_program
  ,loadtype=TXTEMPORAL
  ,tech_from=TX_FROM
  ,tech_to=TX_TO
  ,dclib=&dc_libref
)

%mend mpe_refreshlibs;
