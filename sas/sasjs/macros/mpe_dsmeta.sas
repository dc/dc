/**
  @file
  @brief Gets table metadata
  @details Runs mp_dsmeta and adds datadictionary info

  <h4> SAS Macros </h4>
  @li mp_dsmeta.sas

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/

%macro mpe_dsmeta(libds, outds=dsmeta);
  %local ddsd ddld notes lenstmt;
  %let lenstmt=length ods_table $18 name $100 value $1000;
  %let libds=%upcase(&libds);
  %mp_dsmeta(&libds, outds=&outds)

  data _null_;
    set &mpelib..mpe_datadictionary;
    where &dc_dttmtfmt < tx_to & dd_source=%upcase("&libds") & dd_type='TABLE';
    call symputx('ddsd',dd_shortdesc,'l');
    call symputx('ddld',dd_longdesc,'l');
  run;

  data &outds;
    &lenstmt;
    if last then do;
      ODS_TABLE='MPE_DATADICTIONARY';
      NAME='DD_SHORTDESC';
      VALUE="&ddsd";
      output;
      NAME='DD_LONGDESC';
      VALUE="&ddld";
      output;
    end;
    set &outds end=last;
    output;
  run;

  data _data_;
    set &mpelib..mpe_tables;
    where libref="%scan(&libds,1,.)"
        & dsn="%scan(&libds,2,.)"
        & &dc_dttmtfmt<tx_to;
    &lenstmt;
    ODS_TABLE='MPE_TABLES';
    array c _character_;
    array n _numeric_;
    do over c;
      name=upcase(vname(c));
      value=c;
      output;
    end;
    do over n;
      name=upcase(vname(n));
      value=cats(n);
      output;
    end;
    keep ods_table name value;
  run;

  proc append base=&outds data=&syslast;
  run;


%mend mpe_dsmeta;
