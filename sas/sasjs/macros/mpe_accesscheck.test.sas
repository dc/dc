/**
  @file
  @brief Testing mpe_accesscheck macro
  @details Checking functionality of mpe_accesscheck.sas macro


  <h4> SAS Macros </h4>
  @li mf_getuniquename.sas
  @li mf_getuser.sas
  @li mp_assertdsobs.sas
  @li mpe_getgroups.sas
  @li mpe_accesscheck.sas


  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/

/* get the groups this user is actually a member of */
%mpe_getgroups(user=%mf_getuser(),outds=work.groups)

data _null_;
  set work.groups;
  call symputx('groupname',groupname);
run;

/* create demo MPE_SECURITY table */
data work.mpe_security;
  if 0 then set &dc_libref..mpe_security;
  do access_level='EDIT','APPROVE','VIEW','SIGNOFF','AUDIT';
    LIBREF='SOMELIB';
    DSN='SOMEDS';
    sas_group="&groupname";
    tx_from=0;
    tx_to='31dec4999:23:59:59'dt;
    output;
  end;
run;

%let WRK=WORK;

%mpe_accesscheck(
  SOMELIB.SOMEDS
  ,outds=work.test1
  ,access_level=APPROVE
  ,cntl_lib_var=WRK
)
%mp_assertdsobs(work.test1,
  desc=Test 1 - One record returned,
  test=EQUALS 1,
  outds=work.test_results
)

%mpe_accesscheck(
  SOMELIB.INVALID
  ,outds=work.test2
  ,access_level=APPROVE
  ,cntl_lib_var=WRK
)
%mp_assertdsobs(work.test2,
  desc=Test 12 - 0 records returned,
  test=EQUALS 0,
  outds=work.test_results
)

