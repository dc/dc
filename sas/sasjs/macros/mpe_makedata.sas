/**
  @file
  @brief Populates the Data Controller tables with sample data
  @details

  Usage:

      %mpe_makedata(lib=DC869651
        ,mpeadmins=SASAdministrators
        ,path=/opt/data/dc/VIYA8698
      )

  <h4> SAS Macros </h4>
  @li mf_increment.sas

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd - this is a licensed product and NOT FOR RESALE
  OR DISTRIBUTION.
**/

%macro mpe_makedata(lib=,mpeadmins=,path=);
%if &syscc ne 0 %then %do;
  %put syscc=&syscc exiting &sysmacroname;
  %return;
%end;

proc sql;
insert into &lib..mpe_column_level_security set
    tx_from=0
    ,tx_to='31DEC9999:23:59:59'dt
    ,CLS_SCOPE='EDIT'
    ,CLS_GROUP='AllUsers'
    ,CLS_LIBREF="&lib"
    ,CLS_TABLE='MPE_LOCKANYTABLE'
    ,CLS_VARIABLE_NM='LOCK_STATUS_CD'
    ,CLS_ACTIVE=1
    ,CLS_HIDE=0;

insert into &lib..mpe_config set
    tx_from=0
    ,tx_to='31DEC9999:23:59:59'dt
    ,var_scope="DC"
    ,var_name="DC_EMAIL_ALERTS"
    ,var_value='NO'
    ,var_active=1
    ,var_desc='YES or NO to enable email alerts. Note - this requires email '
        !!'options to be preconfigured! They can be configured in the '
        !!'settings stp if needed.';
insert into &lib..mpe_config set
    tx_from=0
    ,tx_to='31DEC9999:23:59:59'dt
    ,var_scope="DC"
    ,var_name="DC_VIEWLIB_CHECK"
    ,var_value='NO'
    ,var_active=1
    ,var_desc=
          'Set to YES to enable library validity checking in viewLibs service.'
        !!'  Note: this can make the service very slow if there are lots of '
        !!'external libraries.  If enabled, this removes empty libraries from '
        !!'the viewer library dropdown. To switch off, set to NO.';
insert into &lib..mpe_config set
    tx_from=0
    ,tx_to='31DEC9999:23:59:59'dt
    ,var_scope="DC"
    ,var_name="DC_MACROS"
    ,var_value=cats(symget('path'),"/dc_macros")
    ,var_active=1
    ,var_desc='Location of underlying macros - EUC feature.';
insert into &lib..mpe_config set
    tx_from=0
    ,tx_to='31DEC9999:23:59:59'dt
    ,var_scope="DC"
    ,var_name="DC_MAXOBS_WEBEDIT"
    ,var_value="100"
    ,var_active=1
    ,var_desc='This sets the maximum number of observations that can be loaded'
      !!' into the browser for editing in the EDIT screen.  A higher number'
      !!' will require a decent browser (ie, not IE) and more memory on the'
      !!' client side.';
insert into &lib..mpe_config set
    tx_from=0
    ,tx_to='31DEC9999:23:59:59'dt
    ,var_scope="DC"
    ,var_name="DC_REQUEST_LOGS"
    ,var_value="YES"
    ,var_active=1
    ,var_desc='Setting to NO will prevent each request being logged to the'
      !!' MPE_REQUESTS table  Default=YES.';
insert into &lib..mpe_config set
    tx_from=0
    ,tx_to='31DEC9999:23:59:59'dt
    ,var_scope="DC"
    ,var_name="DC_RESTRICT_VIEWER"
    ,var_value="NO"
    ,var_active=1
    ,var_desc='YES will restrict the list of libraries and tables in VIEWER to'
      !!' those explicitly set to VIEW in the MPE_SECURITY table.  Default=NO.';
insert into &lib..mpe_config set
    tx_from=0
    ,tx_to='31DEC9999:23:59:59'dt
    ,var_scope="DC"
    ,var_name="DC_RESTRICT_EDITRECORD"
    ,var_value="NO"
    ,var_active=1
    ,var_desc='Setting YES will prevent the EDIT RECORD dialog appearing in the'
      !!' EDIT screen by removing the "Edit Row" option in the right click menu'
      !!', and the "ADD RECORD" button in the bottom left.  Default=NO.';
insert into &lib..mpe_config set
    tx_from=0
    ,tx_to='31DEC9999:23:59:59'dt
    ,var_scope="DC_CATALOG"
    ,var_name="DC_IGNORELIBS"
    ,var_value="|MAPSSAS|MAPS|"
    ,var_active=1
    ,var_desc='Pipe seperated list of librefs (uppercase) to be ignored when'
      !!' running the Data Catalog refresh process.  This can enable a clean'
      !!' run when invalid librefs are returned by the mpe_refreshlibs macro.';
insert into &lib..mpe_config set
    tx_from=0
    ,tx_to='31DEC9999:23:59:59'dt
    ,var_scope="DC"
    ,var_name="DC_LOCALE"
    ,var_value="SYSTEM"
    ,var_active=1
    ,var_desc='Set to a locale (such as en_gb or en_be) to override the system'
      !!' value (which can be driven from the browser settings).  This is '
      !!'useful when importing ambiguous dates from CSV or Excel (eg 1/2/20 vs '
      !!'2/1/20) as DC uses the anydtdtm informats for import. Default=SYSTEM.';
insert into &lib..mpe_config set
    tx_from=0
    ,tx_to='31DEC9999:23:59:59'dt
    ,var_scope="DCBL_REDSH"
    ,var_name="BULKLOAD"
    ,var_value="YES"
    ,var_active=0
    ,var_desc='Set to YES to enable BULKLOAD=YES in redshift';
insert into &lib..mpe_config set
    tx_from=0
    ,tx_to='31DEC9999:23:59:59'dt
    ,var_scope="DCBL_REDSH"
    ,var_name="BL_BUCKET"
    ,var_value="'your-aws-bucket/Exchange'"
    ,var_active=0
    ,var_desc='Set to the (quoted) value of the AWS bucket to'
      !!'  use for s3 uploads in redshift';
insert into &lib..mpe_config set
    tx_from=0
    ,tx_to='31DEC9999:23:59:59'dt
    ,var_scope="DCBL_REDSH"
    ,var_name="BL_AWS_CREDENTIALS_FILE"
    ,var_value="'/path/to/your/aws/s3/.credentials'"
    ,var_active=0
    ,var_desc='Set to the (quoted) value of the AWS creds file';
insert into &lib..mpe_config set
    tx_from=0
    ,tx_to='31DEC9999:23:59:59'dt
    ,var_scope="DCBL_REDSH"
    ,var_name="BL_REGION"
    ,var_value="'eu-west-1'"
    ,var_active=0
    ,var_desc='Set to the (quoted) AWS region in use';
insert into &lib..mpe_config set
    tx_from=0
    ,tx_to='31DEC9999:23:59:59'dt
    ,var_scope="DCBL_REDSH"
    ,var_name="BL_COMPRESS"
    ,var_value="YES"
    ,var_active=0
    ,var_desc='Set to YES to perform compression ahead of the COPY command';
insert into &lib..mpe_config set
    tx_from=0
    ,tx_to='31DEC9999:23:59:59'dt
    ,var_scope="DCBL_REDSH"
    ,var_name="BL_USE_SSL"
    ,var_value="YES"
    ,var_active=0
    ,var_desc='Set to YES to use SSL encryption';
insert into &lib..mpe_config set
    tx_from=0
    ,tx_to='31DEC9999:23:59:59'dt
    ,var_scope="DC_REVIEW"
    ,var_name="HISTORY_ROWS"
    ,var_value='100'
    ,var_active=1
    ,var_desc='Number of rows (or additional rows) to return in the HISTORY '
        !!'page';
insert into &lib..mpe_config set
    tx_from=0
    ,tx_to='31DEC9999:23:59:59'dt
    ,var_scope="DC"
    ,var_name="DC_LICENCE_KEY"
    ,var_value=' '
    ,var_active=1
    ,var_desc='Licence Key';
insert into &lib..mpe_config set
    tx_from=0
    ,tx_to='31DEC9999:23:59:59'dt
    ,var_scope="DC"
    ,var_name="DC_ACTIVATION_KEY"
    ,var_value=' '
    ,var_active=1
    ,var_desc='Activation Key';


insert into &lib..mpe_datadictionary set
    tx_from=0
    ,DD_TYPE='LIBRARY'
    ,DD_SOURCE="&lib"
    ,DD_SHORTDESC="Data Controller Control Tables"
    ,DD_LONGDESC="# The Data Controller Library"
    ,DD_OWNER="&sysuserid"
    ,DD_RESPONSIBLE="&sysuserid"
    ,DD_SENSITIVITY="Low"
    ,tx_to='31DEC5999:23:59:59'dt;

insert into &lib..mpe_datadictionary set
    tx_from=0
    ,DD_TYPE='TABLE'
    ,DD_SOURCE="&lib..MPE_TABLES"
    ,DD_SHORTDESC="Configuration of new tables for Data Controller"
    ,DD_LONGDESC="# MPE_TABLES - adding new tabels to Data Controller"
    ,DD_OWNER="&sysuserid"
    ,DD_RESPONSIBLE="&sysuserid"
    ,DD_SENSITIVITY="Low"
    ,tx_to='31DEC5999:23:59:59'dt;

insert into &lib..mpe_datadictionary set
    tx_from=0
    ,DD_TYPE='COLUMN'
    ,DD_SOURCE="&lib..MPE_TABLES.DSN"
    ,DD_SHORTDESC="Dataset Name to be edited"
    ,DD_LONGDESC="_DSN_ - must be UPCASE"
    ,DD_OWNER="&sysuserid"
    ,DD_RESPONSIBLE="&sysuserid"
    ,DD_SENSITIVITY="Low"
    ,tx_to='31DEC5999:23:59:59'dt;

insert into &lib..mpe_datadictionary set
    tx_from=0
    ,DD_TYPE='DIRECTORY'
    ,DD_SOURCE="/some/directory"
    ,DD_SHORTDESC="Directory for some purpose"
    ,DD_LONGDESC="This directory is great.  It's great directory.
      It trumps all other directories."
    ,DD_OWNER="&sysuserid"
    ,DD_RESPONSIBLE="&sysuserid"
    ,DD_SENSITIVITY="Low"
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..mpe_datadictionary set
    tx_from=0
    ,DD_TYPE='TABLE'
    ,DD_SOURCE="&lib"
    ,DD_SHORTDESC="Transaction table for capturing Data Controller users"
    ,DD_LONGDESC="After a user accepts the Data Controller EULA they are "
      !!"registered as a user in this table."
    ,DD_OWNER="&sysuserid"
    ,DD_RESPONSIBLE="&sysuserid"
    ,DD_SENSITIVITY="Low"
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..mpe_datadictionary set
    tx_from=0
    ,DD_TYPE='COLUMN'
    ,DD_SOURCE="&lib..MPE_CONFIG.VAR_ACTIVE"
    ,DD_SHORTDESC="Set to 1 to make an option active"
    ,DD_LONGDESC="This value is used as a filter by data controller whenever "
      !!"querying for option settings."
    ,DD_OWNER="&sysuserid"
    ,DD_RESPONSIBLE="&sysuserid"
    ,DD_SENSITIVITY="Low"
    ,tx_to='31DEC5999:23:59:59'dt;

/**
  * mpe_xlmap_info
  */
insert into &lib..mpe_xlmap_info set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='BASEL-KM1'
  ,xlmap_description='Basel 3 Key Metrics report'
  ,XLMAP_TARGETLIBDS="&lib..MPE_XLMAP_DATA";

/**
  * mpe_xlmap_rules
  */
insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='BASEL-KM1'
  ,xlmap_range_id='KM1:a'
  ,xlmap_sheet='KM1'
  ,xlmap_start='MATCH 4 R[2]C[0]:a';
insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='BASEL-KM1'
  ,xlmap_range_id='KM1:b'
  ,xlmap_sheet='KM1'
  ,xlmap_start='MATCH 4 R[2]C[0]:b';
insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='BASEL-KM1'
  ,xlmap_range_id='KM1:c'
  ,xlmap_sheet='KM1'
  ,xlmap_start='MATCH 4 R[2]C[0]:c';
insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='BASEL-KM1'
  ,xlmap_range_id='KM1:d'
  ,xlmap_sheet='KM1'
  ,xlmap_start='MATCH 4 R[2]C[0]:d';
insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='BASEL-KM1'
  ,xlmap_range_id='KM1:e'
  ,xlmap_sheet='KM1'
  ,xlmap_start='MATCH 4 R[2]C[0]:e';
insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='BASEL-KM1'
  ,xlmap_range_id='KM1:f'
  ,xlmap_sheet='KM1'
  ,xlmap_start='MATCH 4 R[2]C[0]:f';
insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='BASEL-KM1'
  ,xlmap_range_id='KM1:1/a'
  ,xlmap_sheet='KM1'
  ,xlmap_start='MATCH C R[0]C[1]:Common Equity Tier 1 (CET1)';
insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='BASEL-KM1'
  ,xlmap_range_id='KM1:1/b'
  ,xlmap_sheet='KM1'
  ,xlmap_start='MATCH C R[0]C[2]:Common Equity Tier 1 (CET1)';
  insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='BASEL-KM1'
  ,xlmap_range_id='KM1:1/c'
  ,xlmap_sheet='KM1'
  ,xlmap_start='MATCH C R[0]C[3]:Common Equity Tier 1 (CET1)';
  insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='BASEL-KM1'
  ,xlmap_range_id='KM1:1/d'
  ,xlmap_sheet='KM1'
  ,xlmap_start='MATCH C R[0]C[4]:Common Equity Tier 1 (CET1)';
  insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='BASEL-KM1'
  ,xlmap_range_id='KM1:1/e'
  ,xlmap_sheet='KM1'
  ,xlmap_start='MATCH C R[0]C[5]:Common Equity Tier 1 (CET1)';
insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='BASEL-KM1'
  ,xlmap_range_id='KM1:1/f'
  ,xlmap_sheet='KM1'
  ,xlmap_start='MATCH C R[0]C[6]:Common Equity Tier 1 (CET1)';
insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='BASEL-KM1'
  ,xlmap_range_id='KM1:1a/e'
  ,xlmap_sheet='KM1'
  ,xlmap_start='MATCH C R[1]C[5]:Common Equity Tier 1 (CET1)';
insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='BASEL-KM1'
  ,xlmap_range_id='KM1:1a/f'
  ,xlmap_sheet='KM1'
  ,xlmap_start='MATCH C R[1]C[6]:Common Equity Tier 1 (CET1)';
insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='BASEL-KM1'
  ,xlmap_range_id='KM1:2/a'
  ,xlmap_sheet='KM1'
  ,xlmap_start='ABSOLUTE D10';
insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='BASEL-KM1'
  ,xlmap_range_id='KM1:2/b'
  ,xlmap_sheet='/3'
  ,xlmap_start='ABSOLUTE E10';
insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='BASEL-KM1'
  ,xlmap_range_id='KM1:2/c'
  ,xlmap_sheet='/3'
  ,xlmap_start='RELATIVE R[10]C[6]';
insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='BASEL-KM1'
  ,xlmap_range_id='KM1:2/d'
  ,xlmap_sheet='/3'
  ,xlmap_start='RELATIVE R[10]C[8]';
insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='BASEL-KM1'
  ,xlmap_range_id='KM1:2/e'
  ,xlmap_sheet='/3'
  ,xlmap_start='RELATIVE R[10]C[9]';
insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='BASEL-KM1'
  ,xlmap_range_id='KM1:2/f'
  ,xlmap_sheet='/3'
  ,xlmap_start='RELATIVE R[10]C[10]';
insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='BASEL-KM1'
  ,xlmap_range_id='KM1:2a'
  ,xlmap_sheet='KM1'
  ,xlmap_start='ABSOLUTE H11'
  ,xlmap_finish='RELATIVE R[0]C[1]';
insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='BASEL-KM1'
  ,xlmap_range_id='KM1:3'
  ,xlmap_sheet='KM1'
  ,xlmap_start='RELATIVE R[12]C[4]'
  ,xlmap_finish='ABSOLUTE I13';
insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='BASEL-CR2'
  ,xlmap_range_id='CR2-sec1'
  ,xlmap_sheet='CR2'
  ,xlmap_start='ABSOLUTE D8'
  ,xlmap_finish='BLANKROW';
insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='BASEL-CR2'
  ,xlmap_range_id='CR2-sec2'
  ,xlmap_sheet='CR2'
  ,xlmap_start='ABSOLUTE D18'
  ,xlmap_finish='LASTDOWN';
insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='SAMPLE'
  ,xlmap_range_id='header'
  ,xlmap_sheet='/1'
  ,xlmap_start='ABSOLUTE B3'
  ,xlmap_finish='ABSOLUTE B8';
insert into &lib..mpe_xlmap_rules set
  tx_from=0
  ,tx_to='31DEC5999:23:59:59'dt
  ,xlmap_id='SAMPLE'
  ,xlmap_range_id='data'
  ,xlmap_sheet='/1'
  ,xlmap_start='ABSOLUTE B13'
  ,xlmap_finish='ABSOLUTE E16';



/**
  * MPE_GROUPS
  */
insert into &lib..mpe_groups set
    tx_from=0
    ,group_name="dc-admin"
    ,group_desc="Custom Group for Data Controller Purposes"
    ,user_name="allbow"
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..mpe_groups set
    tx_from=0
    ,group_name="dc-admin"
    ,group_desc="Custom Group for Data Controller Purposes"
    ,user_name="dctestuser1"
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..mpe_groups set
    tx_from=0
    ,group_name="dc-admin"
    ,group_desc="Custom Group for Data Controller Purposes"
    ,user_name="mihmed"
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..mpe_groups set
    tx_from=0
    ,group_name="sec-sas9-prd-ext-sasplatform-300115datacontroller"
    ,group_desc="Custom Group for Data Controller Purposes"
    ,user_name="DCTest"
    ,tx_to='31DEC5999:23:59:59'dt;

/**
  * MPE_ROW_LEVEL_SECURITY
  */
insert into &lib..mpe_row_level_security set
    tx_from=0
    ,tx_to='31DEC5999:23:59:59'dt
    ,RLS_RK=1
    ,RLS_SCOPE='ALL'
    ,RLS_GROUP='sec-sas9-prd-int-sasplatform-300114sasjs'
    ,RLS_LIBREF="&lib."
    ,RLS_TABLE="MPE_GROUPS"
    ,RLS_GROUP_LOGIC='AND'
    ,RLS_SUBGROUP_LOGIC='OR'
    ,RLS_SUBGROUP_ID=0
    ,RLS_VARIABLE_NM='GROUP_NAME'
    ,RLS_OPERATOR_NM='NE'
    ,RLS_RAW_VALUE="'-1'"
    ,RLS_ACTIVE=1;
insert into &lib..mpe_row_level_security set
    tx_from=0
    ,tx_to='31DEC5999:23:59:59'dt
    ,RLS_RK=2
    ,RLS_SCOPE='ALL'
    ,RLS_GROUP='sec-sas9-prd-int-sasplatform-300114sasjs'
    ,RLS_LIBREF="&lib"
    ,RLS_TABLE="MPE_ROW_LEVEL_SECURITY"
    ,RLS_GROUP_LOGIC='AND'
    ,RLS_SUBGROUP_LOGIC='OR'
    ,RLS_SUBGROUP_ID=0
    ,RLS_VARIABLE_NM='RLS_RK'
    ,RLS_OPERATOR_NM='>'
    ,RLS_RAW_VALUE='0'
    ,RLS_ACTIVE=1;
insert into &lib..mpe_row_level_security set
    tx_from=0
    ,tx_to='31DEC5999:23:59:59'dt
    ,RLS_RK=3
    ,RLS_SCOPE='ALL'
    ,RLS_GROUP='DC Demo Group'
    ,RLS_LIBREF="&lib"
    ,RLS_TABLE="MPE_SECURITY"
    ,RLS_GROUP_LOGIC='AND'
    ,RLS_SUBGROUP_LOGIC='OR'
    ,RLS_SUBGROUP_ID=0
    ,RLS_VARIABLE_NM='ACCESS_LEVEL'
    ,RLS_OPERATOR_NM='NE'
    ,RLS_RAW_VALUE="'N/A'"
    ,RLS_ACTIVE=1;


/**
  * MPE_SECURITY
  */
insert into &lib..mpe_security set
    tx_from=0
    ,libref="*ALL*"
    ,dsn="*ALL*"
    ,access_level="APPROVE"
    ,sas_group="sec-sas9-prd-int-sasplatform-300114sasjs"
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..mpe_security set
    tx_from=0
    ,libref="*ALL*"
    ,dsn="*ALL*"
    ,access_level="EDIT"
    ,sas_group="sec-sas9-prd-int-sasplatform-300114sasjs"
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..mpe_security set
    tx_from=0
    ,libref="*ALL*"
    ,dsn="*ALL*"
    ,access_level="APPROVE"
    ,sas_group="sec-sas9-prd-ext-sasplatform-300114sasjs"
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..mpe_security set
    tx_from=0
    ,libref="*ALL*"
    ,dsn="*ALL*"
    ,access_level="EDIT"
    ,sas_group="sec-sas9-prd-ext-sasplatform-300114sasjs"
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..mpe_security set
    tx_from=0
    ,libref="*ALL*"
    ,dsn="*ALL*"
    ,access_level="EDIT"
    ,sas_group="dc-admin"
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..mpe_security set
    tx_from=0
    ,libref="*ALL*"
    ,dsn="*ALL*"
    ,access_level="APPROVE"
    ,sas_group="dc-admin"
    ,tx_to='31DEC5999:23:59:59'dt;


  /* mpe_selectbox */
  %let rk=1;
  insert into &lib..mpe_selectbox set
      selectbox_rk=&rk
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_LOCKANYTABLE"
      ,base_column="LOCK_STATUS_CD"
      ,selectbox_value='LOCKED'
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_LOCKANYTABLE"
      ,base_column="LOCK_STATUS_CD"
      ,selectbox_value='UNLOCKED'
      ,selectbox_order=2
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_SECURITY"
      ,base_column="ACCESS_LEVEL"
      ,selectbox_value='EDIT'
      ,selectbox_order=0
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_SECURITY"
      ,base_column="ACCESS_LEVEL"
      ,selectbox_value='APPROVE'
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_SECURITY"
      ,base_column="ACCESS_LEVEL"
      ,selectbox_value='VIEW'
      ,selectbox_order=2
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_SECURITY"
      ,base_column="ACCESS_LEVEL"
      ,selectbox_value='SIGNOFF'
      ,selectbox_order=3
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_TABLES"
      ,base_column="LOADTYPE"
      ,selectbox_value='UPDATE'
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_TABLES"
      ,base_column="LOADTYPE"
      ,selectbox_value='REPLACE'
      ,selectbox_order=2
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_TABLES"
      ,base_column="LOADTYPE"
      ,selectbox_value='TXTEMPORAL'
      ,selectbox_order=3
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_TABLES"
      ,base_column="LOADTYPE"
      ,selectbox_value='BITEMPORAL'
      ,selectbox_order=4
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_TABLES"
      ,base_column="LOADTYPE"
      ,selectbox_value='FORMAT_CAT'
      ,selectbox_order=5
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_ALERTS"
      ,base_column="ALERT_EVENT"
      ,selectbox_value='*ALL*'
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_ALERTS"
      ,base_column="ALERT_EVENT"
      ,selectbox_value='SUBMITTED'
      ,selectbox_order=2
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_ALERTS"
      ,base_column="ALERT_EVENT"
      ,selectbox_value='APPROVED'
      ,selectbox_order=3
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_ALERTS"
      ,base_column="ALERT_EVENT"
      ,selectbox_value='REJECTED'
      ,selectbox_order=4
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_X_TEST"
      ,base_column="SOME_DROPDOWN"
      ,selectbox_value='Option 1'
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_X_TEST"
      ,base_column="SOME_DROPDOWN"
      ,selectbox_value='Option 2'
      ,selectbox_order=2
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_X_TEST"
      ,base_column="SOME_DROPDOWN"
      ,selectbox_value='Option 3'
      ,selectbox_order=2
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_X_TEST"
      ,base_column="SOME_DROPDOWN"
      ,selectbox_value="This is a long option.  This option is very long.  "
        !!"It is optional, though."
      ,selectbox_order=3
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_VALIDATIONS"
      ,base_column="RULE_TYPE"
      ,selectbox_value="CASE"
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_VALIDATIONS"
      ,base_column="RULE_TYPE"
      ,selectbox_value="MINVAL"
      ,selectbox_order=2
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_VALIDATIONS"
      ,base_column="RULE_TYPE"
      ,selectbox_value="MAXVAL"
      ,selectbox_order=3
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_VALIDATIONS"
      ,base_column="RULE_TYPE"
      ,selectbox_value="HARDSELECT"
      ,selectbox_order=4
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
    insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_VALIDATIONS"
      ,base_column="RULE_TYPE"
      ,selectbox_value="SOFTSELECT"
      ,selectbox_order=5
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_VALIDATIONS"
      ,base_column="RULE_TYPE"
      ,selectbox_value="NOTNULL"
      ,selectbox_order=6
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_SECURITY"
      ,base_column="DSN"
      ,selectbox_value="SOME_DATASET"
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_SECURITY"
      ,base_column="DSN"
      ,selectbox_value="EXAMPLE"
      ,selectbox_order=2
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_DATADICTIONARY"
      ,base_column="DD_TYPE"
      ,selectbox_value="COLUMN"
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_DATADICTIONARY"
      ,base_column="DD_TYPE"
      ,selectbox_value="TABLE"
      ,selectbox_order=2
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_DATADICTIONARY"
      ,base_column="DD_TYPE"
      ,selectbox_value="LIBRARY"
      ,selectbox_order=3
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_DATADICTIONARY"
      ,base_column="DD_TYPE"
      ,selectbox_value="CATALOG"
      ,selectbox_order=3
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_DATADICTIONARY"
      ,base_column="DD_TYPE"
      ,selectbox_value="FORMAT"
      ,selectbox_order=3
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_SECURITY"
      ,base_column="LIBREF"
      ,selectbox_value='*ALL*'
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_SECURITY"
      ,base_column="ACCESS_LEVEL"
      ,selectbox_value='AUDIT'
      ,selectbox_order=4
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_VALIDATIONS"
      ,base_column="RULE_TYPE"
      ,selectbox_value="HARDSELECT_HOOK"
      ,selectbox_order=7
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_VALIDATIONS"
      ,base_column="RULE_TYPE"
      ,selectbox_value="SOFTSELECT_HOOK"
      ,selectbox_order=7
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_ROW_LEVEL_SECURITY"
      ,base_column="RLS_SCOPE"
      ,selectbox_value="ALL"
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_ROW_LEVEL_SECURITY"
      ,base_column="RLS_SCOPE"
      ,selectbox_value="EDIT"
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_ROW_LEVEL_SECURITY"
      ,base_column="RLS_SCOPE"
      ,selectbox_value="VIEW"
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_ROW_LEVEL_SECURITY"
      ,base_column="RLS_GROUP_LOGIC"
      ,selectbox_value="AND"
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_ROW_LEVEL_SECURITY"
      ,base_column="RLS_GROUP_LOGIC"
      ,selectbox_value="OR"
      ,selectbox_order=2
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_ROW_LEVEL_SECURITY"
      ,base_column="RLS_SUBGROUP_LOGIC"
      ,selectbox_value="AND"
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_ROW_LEVEL_SECURITY"
      ,base_column="RLS_SUBGROUP_LOGIC"
      ,selectbox_value="OR"
      ,selectbox_order=2
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_ROW_LEVEL_SECURITY"
      ,base_column="RLS_OPERATOR_NM"
      ,selectbox_value="="
      ,selectbox_order=0
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_ROW_LEVEL_SECURITY"
      ,base_column="RLS_OPERATOR_NM"
      ,selectbox_value=">"
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_ROW_LEVEL_SECURITY"
      ,base_column="RLS_OPERATOR_NM"
      ,selectbox_value="<"
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_ROW_LEVEL_SECURITY"
      ,base_column="RLS_OPERATOR_NM"
      ,selectbox_value="<="
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_ROW_LEVEL_SECURITY"
      ,base_column="RLS_OPERATOR_NM"
      ,selectbox_value=">="
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_ROW_LEVEL_SECURITY"
      ,base_column="RLS_OPERATOR_NM"
      ,selectbox_value="BETWEEN"
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_ROW_LEVEL_SECURITY"
      ,base_column="RLS_OPERATOR_NM"
      ,selectbox_value="IN"
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_ROW_LEVEL_SECURITY"
      ,base_column="RLS_OPERATOR_NM"
      ,selectbox_value="NOT IN"
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_ROW_LEVEL_SECURITY"
      ,base_column="RLS_OPERATOR_NM"
      ,selectbox_value="NE"
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_ROW_LEVEL_SECURITY"
      ,base_column="RLS_OPERATOR_NM"
      ,selectbox_value="CONTAINS"
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_EXCEL_CONFIG"
      ,base_column="XL_RULE"
      ,selectbox_value="FORMULA"
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_ROW_LEVEL_SECURITY"
      ,base_column="RLS_ACTIVE"
      ,selectbox_value="1"
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_ROW_LEVEL_SECURITY"
      ,base_column="RLS_ACTIVE"
      ,selectbox_value="0"
      ,selectbox_order=2
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_COLUMN_LEVEL_SECURITY"
      ,base_column="CLS_ACTIVE"
      ,selectbox_value="1"
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_COLUMN_LEVEL_SECURITY"
      ,base_column="CLS_ACTIVE"
      ,selectbox_value="0"
      ,selectbox_order=2
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_COLUMN_LEVEL_SECURITY"
      ,base_column="CLS_SCOPE"
      ,selectbox_value="EDIT"
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_COLUMN_LEVEL_SECURITY"
      ,base_column="CLS_SCOPE"
      ,selectbox_value="VIEW"
      ,selectbox_order=2
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_COLUMN_LEVEL_SECURITY"
      ,base_column="CLS_SCOPE"
      ,selectbox_value="ALL"
      ,selectbox_order=3
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_COLUMN_LEVEL_SECURITY"
      ,base_column="CLS_HIDE"
      ,selectbox_value="0"
      ,selectbox_order=1
      ,ver_to_dttm='31DEC5999:23:59:59'dt;
  insert into &lib..mpe_selectbox set
      selectbox_rk=%mf_increment(rk)
      ,ver_from_dttm=0
      ,select_lib="&lib"
      ,select_ds="MPE_COLUMN_LEVEL_SECURITY"
      ,base_column="CLS_HIDE"
      ,selectbox_value="1"
      ,selectbox_order=2
      ,ver_to_dttm='31DEC5999:23:59:59'dt;

/**
  * MPE_TABLES
  */
  insert into &lib..mpe_tables
    set tx_from=0
      ,tx_to='31DEC5999:23:59:59'dt
      ,libref="&lib"
      ,dsn='MPE_COLUMN_LEVEL_SECURITY'
      ,num_of_approvals_required=1
      ,loadtype='TXTEMPORAL'
      ,var_txfrom='TX_FROM'
      ,var_txto='TX_TO'
      ,buskey='CLS_SCOPE CLS_GROUP CLS_LIBREF CLS_TABLE CLS_VARIABLE_NM'
      ,notes='Docs: https://docs.datacontroller.io/column-level-security'
      ,post_edit_hook='services/hooks/mpe_column_level_security_postedit'
    ;
  insert into &lib..mpe_tables
    set tx_from=0
      ,tx_to='31DEC5999:23:59:59'dt
      ,libref="&lib"
      ,dsn='MPE_XLMAP_INFO'
      ,num_of_approvals_required=1
      ,loadtype='TXTEMPORAL'
      ,var_txfrom='TX_FROM'
      ,var_txto='TX_TO'
      ,buskey='XLMAP_ID'
      ,notes='Docs: https://docs.datacontroller.io/complex-excel-uploads'
      ,post_edit_hook='services/hooks/mpe_xlmap_info_postedit'
    ;
  insert into &lib..mpe_tables
    set tx_from=0
      ,tx_to='31DEC5999:23:59:59'dt
      ,libref="&lib"
      ,dsn='MPE_XLMAP_RULES'
      ,num_of_approvals_required=1
      ,loadtype='TXTEMPORAL'
      ,var_txfrom='TX_FROM'
      ,var_txto='TX_TO'
      ,buskey='XLMAP_ID XLMAP_RANGE_ID'
      ,notes='Docs: https://docs.datacontroller.io/complex-excel-uploads'
      ,post_edit_hook='services/hooks/mpe_xlmap_rules_postedit'
    ;
  insert into &lib..mpe_tables
    set tx_from=0
      ,tx_to='31DEC5999:23:59:59'dt
      ,libref="&lib"
      ,dsn='MPE_XLMAP_DATA'
      ,num_of_approvals_required=1
      ,loadtype='UPDATE'
      ,buskey='LOAD_REF XLMAP_ID XLMAP_RANGE_ID ROW_NO COL_NO'
      ,notes='Docs: https://docs.datacontroller.io/complex-excel-uploads'
    ;
  insert into &lib..mpe_tables
    set tx_from=0
      ,tx_to='31DEC5999:23:59:59'dt
      ,libref="&lib"
      ,dsn='MPE_LOCKANYTABLE'
      ,num_of_approvals_required=1
      ,loadtype='UPDATE'
      ,buskey='LOCK_LIB LOCK_DS'
      ,notes='This table may be edited when a process failed and left a lock'
    ;
  insert into &lib..mpe_tables
    set tx_from=0
      ,tx_to='31DEC5999:23:59:59'dt
      ,libref="&lib"
      ,dsn='MPE_TABLES'
      ,num_of_approvals_required=1
      ,loadtype='TXTEMPORAL'
      ,buskey='LIBREF DSN'
      ,var_txfrom='TX_FROM'
      ,var_txto='TX_TO'
      ,notes='This entry allows the MP Editor to edit itself!'
      ,post_edit_hook='services/hooks/mpe_tables_postedit'
    ;
  insert into &lib..mpe_tables
    set tx_from=0
      ,tx_to='31DEC5999:23:59:59'dt
      ,libref="&lib"
      ,dsn='MPE_SECURITY'
      ,num_of_approvals_required=1
      ,loadtype='TXTEMPORAL'
      ,buskey='LIBREF DSN ACCESS_LEVEL SAS_GROUP'
      ,var_txfrom='TX_FROM'
      ,var_txto='TX_TO'
      ,notes='Determines which groups can view/edit/approve which tables'
      ,post_edit_hook='services/hooks/mpe_security_postedit'
    ;
  insert into &lib..mpe_tables
    set tx_from=0
      ,tx_to='31DEC5999:23:59:59'dt
      ,libref="&lib"
      ,dsn='MPE_SELECTBOX'
      ,num_of_approvals_required=1
      ,loadtype='TXTEMPORAL'
      ,buskey='SELECTBOX_RK'
      ,var_txfrom='VER_FROM_DTTM'
      ,var_txto='VER_TO_DTTM'
      ,notes='Can configure dropdowns for the front end'
      ,rk_underlying='SELECT_LIB SELECT_DS BASE_COLUMN SELECTBOX_VALUE'
    ;
  insert into &lib..mpe_tables
    set tx_from=0
      ,tx_to='31DEC5999:23:59:59'dt
      ,libref="&lib"
      ,dsn='MPE_X_TEST'
      ,num_of_approvals_required=1
      ,loadtype='UPDATE'
      ,buskey='PRIMARY_KEY_FIELD'
      ,notes='Test table for controller'
    ;
  insert into &lib..mpe_tables
    set tx_from=0
      ,tx_to='31DEC5999:23:59:59'dt
      ,libref="&lib"
      ,dsn='MPE_EMAILS'
      ,num_of_approvals_required=1
      ,loadtype='TXTEMPORAL'
      ,buskey='USER_NAME'
      ,notes='Primary Emails Table (backup is metadata)'
      ,var_txfrom='TX_FROM'
      ,var_txto='TX_TO'
    ;
  insert into &lib..mpe_tables
    set tx_from=0
      ,tx_to='31DEC5999:23:59:59'dt
      ,libref="&lib"
      ,dsn='MPE_CONFIG'
      ,num_of_approvals_required=1
      ,loadtype='TXTEMPORAL'
      ,buskey='VAR_SCOPE VAR_NAME'
      ,notes='Configuration variables for Data Controller'
      ,var_txfrom='TX_FROM'
      ,var_txto='TX_TO'
    ;
  insert into &lib..mpe_tables
    set tx_from=0
      ,tx_to='31DEC5999:23:59:59'dt
      ,libref="&lib"
      ,dsn='MPE_ALERTS'
      ,num_of_approvals_required=1
      ,loadtype='TXTEMPORAL'
      ,buskey='ALERT_EVENT ALERT_LIB ALERT_DS ALERT_USER'
      ,notes='Configuration for alert email events'
      ,var_txfrom='TX_FROM'
      ,var_txto='TX_TO'
    ;
  insert into &lib..mpe_tables
    set tx_from=0
      ,tx_to='31DEC5999:23:59:59'dt
      ,libref="&lib"
      ,dsn='MPE_GROUPS'
      ,num_of_approvals_required=1
      ,loadtype='TXTEMPORAL'
      ,buskey='GROUP_NAME USER_NAME'
      ,notes='Configuration for additional groups within Data Controller'
      ,var_txfrom='TX_FROM'
      ,var_txto='TX_TO'
    ;
  insert into &lib..mpe_tables
    set tx_from=0
      ,tx_to='31DEC5999:23:59:59'dt
      ,libref="&lib"
      ,dsn='MPE_VALIDATIONS'
      ,num_of_approvals_required=1
      ,loadtype='TXTEMPORAL'
      ,buskey='BASE_LIB BASE_DS BASE_COL RULE_TYPE'
      ,notes='Configuration of data quality rules in Editor component'
      ,var_txfrom='TX_FROM'
      ,var_txto='TX_TO'
      ,post_edit_hook='services/hooks/mpe_validations_postedit'
    ;
  insert into &lib..mpe_tables
    set tx_from=0
      ,tx_to='31DEC5999:23:59:59'dt
      ,libref="&lib"
      ,dsn='MPE_DATADICTIONARY'
      ,num_of_approvals_required=1
      ,loadtype='TXTEMPORAL'
      ,buskey='DD_TYPE DD_SOURCE'
      ,notes='Configuration of data dictionary'
      ,var_txfrom='TX_FROM'
      ,var_txto='TX_TO'
    ;
  insert into &lib..mpe_tables
    set tx_from=0
      ,tx_to='31DEC5999:23:59:59'dt
      ,libref="&lib"
      ,dsn='MPE_EXCEL_CONFIG'
      ,num_of_approvals_required=1
      ,loadtype='TXTEMPORAL'
      ,buskey='XL_LIBREF XL_TABLE XL_COLUMN'
      ,notes='Configuration of the excel import rules'
      ,var_txfrom='TX_FROM'
      ,var_txto='TX_TO'
    ;
  insert into &lib..mpe_tables
    set tx_from=0
      ,tx_to='31DEC5999:23:59:59'dt
      ,libref="&lib"
      ,dsn='MPE_ROW_LEVEL_SECURITY'
      ,num_of_approvals_required=1
      ,loadtype='TXTEMPORAL'
      ,buskey='RLS_RK'
      ,notes='Configuration of Row Level Security'
      ,var_txfrom='TX_FROM'
      ,var_txto='TX_TO'
      ,rk_underlying='RLS_SCOPE RLS_GROUP RLS_LIBREF RLS_TABLE RLS_GROUP_LOGIC '
        !!'RLS_SUBGROUP_LOGIC RLS_SUBGROUP_ID RLS_VARIABLE_NM RLS_OPERATOR_NM '
        !!'RLS_RAW_VALUE '
      ,post_edit_hook='services/hooks/mpe_row_level_security_postedit'
    ;
  insert into &lib..mpe_tables
    set tx_from=0
      ,tx_to='31DEC5999:23:59:59'dt
      ,libref="&lib"
      ,dsn='MPE_X_CATALOG-FC'
      ,num_of_approvals_required=1
      ,loadtype='FORMAT_CAT'
      ,buskey='TYPE FMTNAME FMTROW'
      ,notes='Sample Format Catalog'
    ;

/* mpe_validations */
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_COLUMN_LEVEL_SECURITY"
    ,base_col="CLS_SCOPE"
    ,rule_type='CASE'
    ,rule_value='UPCASE'
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_COLUMN_LEVEL_SECURITY"
    ,base_col="CLS_LIBREF"
    ,rule_type='CASE'
    ,rule_value='UPCASE'
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_COLUMN_LEVEL_SECURITY"
    ,base_col="CLS_LIBREF"
    ,rule_type='SOFTSELECT_HOOK'
    ,rule_value="services/validations/libraries_all"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_COLUMN_LEVEL_SECURITY"
    ,base_col="CLS_TABLE"
    ,rule_type='CASE'
    ,rule_value='UPCASE'
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_COLUMN_LEVEL_SECURITY"
    ,base_col="CLS_TABLE"
    ,rule_type='SOFTSELECT_HOOK'
    ,rule_value="services/validations/tables_all"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_COLUMN_LEVEL_SECURITY"
    ,base_col="CLS_VARIABLE_NM"
    ,rule_type='CASE'
    ,rule_value='UPCASE'
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_COLUMN_LEVEL_SECURITY"
    ,base_col="CLS_VARIABLE_NM"
    ,rule_type='SOFTSELECT_HOOK'
    ,rule_value="services/validations/columns_in_libds"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_COLUMN_LEVEL_SECURITY"
    ,base_col="CLS_ACTIVE"
    ,rule_type='MAXVAL'
    ,rule_value='1'
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_COLUMN_LEVEL_SECURITY"
    ,base_col="CLS_HIDE"
    ,rule_type='MAXVAL'
    ,rule_value='1'
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_COLUMN_LEVEL_SECURITY"
    ,base_col="CLS_GROUP"
    ,rule_type='SOFTSELECT_HOOK'
    ,rule_value="services/validations/sas_groups"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;

insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_ALERTS"
    ,base_col="ALERT_LIB"
    ,rule_type='HARDSELECT_HOOK'
    ,rule_value="services/validations/mpe_alerts.alert_lib"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;

insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_XLMAP_INFO"
    ,base_col="XLMAP_ID"
    ,rule_type='CASE'
    ,rule_value='UPCASE'
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;

insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_XLMAP_RULES"
    ,base_col="XLMAP_ID"
    ,rule_type='CASE'
    ,rule_value='UPCASE'
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;

insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_TABLES"
    ,base_col="LIBREF"
    ,rule_type='CASE'
    ,rule_value='UPCASE'
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_TABLES"
    ,base_col="DSN"
    ,rule_type='CASE'
    ,rule_value='UPCASE'
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_TABLES"
    ,base_col="LIBREF"
    ,rule_type='NOTNULL'
    ,rule_value=' '
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_TABLES"
    ,base_col="DSN"
    ,rule_type='NOTNULL'
    ,rule_value=' '
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_TABLES"
    ,base_col="NUM_OF_APPROVALS_REQUIRED"
    ,rule_type='MINVAL'
    ,rule_value='1'
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_TABLES"
    ,base_col="BUSKEY"
    ,rule_type='CASE'
    ,rule_value='UPCASE'
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_TABLES"
    ,base_col="BUSKEY"
    ,rule_type='NOTNULL'
    ,rule_value=" "
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_TABLES"
    ,base_col="VAR_TXFROM"
    ,rule_type='CASE'
    ,rule_value='UPCASE'
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_TABLES"
    ,base_col="VAR_TXTO"
    ,rule_type='CASE'
    ,rule_value='UPCASE'
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_TABLES"
    ,base_col="VAR_BUSFROM"
    ,rule_type='CASE'
    ,rule_value='UPCASE'
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_TABLES"
    ,base_col="VAR_BUSTO"
    ,rule_type='CASE'
    ,rule_value='UPCASE'
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_SECURITY"
    ,base_col="LIBREF"
    ,rule_type='CASE'
    ,rule_value="UPCASE"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_TABLES"
    ,base_col="VAR_PROCESSED"
    ,rule_type='CASE'
    ,rule_value='UPCASE'
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_SECURITY"
    ,base_col="LIBREF"
    ,rule_type='HARDSELECT'
    ,rule_value="&lib..MPE_TABLES.LIBREF"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_SECURITY"
    ,base_col="DSN"
    ,rule_type='CASE'
    ,rule_value="UPCASE"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_SECURITY"
    ,base_col="DSN"
    ,rule_type='SOFTSELECT'
    ,rule_value="&lib..MPE_TABLES.DSN"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_SECURITY"
    ,base_col="SAS_GROUP"
    ,rule_type='SOFTSELECT_HOOK'
    ,rule_value="services/validations/sas_groups"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_VALIDATIONS"
    ,base_col="BASE_LIB"
    ,rule_type='SOFTSELECT_HOOK'
    ,rule_value="services/validations/libraries_editable"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_VALIDATIONS"
    ,base_col="BASE_DS"
    ,rule_type='SOFTSELECT_HOOK'
    ,rule_value="services/validations/tables_editable"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_VALIDATIONS"
    ,base_col="BASE_COL"
    ,rule_type='SOFTSELECT_HOOK'
    ,rule_value="services/validations/columns_in_libds"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_VALIDATIONS"
    ,base_col="RULE_ACTIVE"
    ,rule_type='MINVAL'
    ,rule_value="0"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_VALIDATIONS"
    ,base_col="RULE_ACTIVE"
    ,rule_type='MAXVAL'
    ,rule_value="1"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_EXCEL_CONFIG"
    ,base_col="XL_LIBREF"
    ,rule_type='SOFTSELECT_HOOK'
    ,rule_value="services/validations/libraries_editable"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_EXCEL_CONFIG"
    ,base_col="XL_TABLE"
    ,rule_type='SOFTSELECT_HOOK'
    ,rule_value="services/validations/tables_editable"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_EXCEL_CONFIG"
    ,base_col="XL_COLUMN"
    ,rule_type='SOFTSELECT_HOOK'
    ,rule_value="services/validations/columns_in_libds"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_TABLES"
    ,base_col="LIBREF"
    ,rule_type='SOFTSELECT_HOOK'
    ,rule_value="services/validations/libraries_all"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_TABLES"
    ,base_col="DSN"
    ,rule_type='SOFTSELECT_HOOK'
    ,rule_value="services/validations/mpe_tables.dsn"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_TABLES"
    ,base_col="VAR_TXFROM"
    ,rule_type='SOFTSELECT_HOOK'
    ,rule_value="services/validations/columns_in_libds"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_TABLES"
    ,base_col="VAR_TXTO"
    ,rule_type='SOFTSELECT_HOOK'
    ,rule_value="services/validations/columns_in_libds"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_TABLES"
    ,base_col="VAR_BUSFROM"
    ,rule_type='SOFTSELECT_HOOK'
    ,rule_value="services/validations/columns_in_libds"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_TABLES"
    ,base_col="VAR_BUSTO"
    ,rule_type='SOFTSELECT_HOOK'
    ,rule_value="services/validations/columns_in_libds"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_TABLES"
    ,base_col="VAR_PROCESSED"
    ,rule_type='SOFTSELECT_HOOK'
    ,rule_value="services/validations/columns_in_libds"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_SELECTBOX"
    ,base_col="SELECT_LIB"
    ,rule_type='SOFTSELECT_HOOK'
    ,rule_value="services/validations/libraries_editable"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_SELECTBOX"
    ,base_col="SELECT_DS"
    ,rule_type='SOFTSELECT_HOOK'
    ,rule_value="services/validations/tables_editable"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_SELECTBOX"
    ,base_col="BASE_COLUMN"
    ,rule_type='SOFTSELECT_HOOK'
    ,rule_value="services/validations/columns_in_libds"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_ROW_LEVEL_SECURITY"
    ,base_col="RLS_GROUP"
    ,rule_type='SOFTSELECT_HOOK'
    ,rule_value="services/validations/sas_groups"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_ROW_LEVEL_SECURITY"
    ,base_col="RLS_LIBREF"
    ,rule_type='SOFTSELECT_HOOK'
    ,rule_value="services/validations/libraries_all"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_ROW_LEVEL_SECURITY"
    ,base_col="RLS_TABLE"
    ,rule_type='SOFTSELECT_HOOK'
    ,rule_value="services/validations/tables_all"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_ROW_LEVEL_SECURITY"
    ,base_col="RLS_SUBGROUP_ID"
    ,rule_type='MINVAL'
    ,rule_value='0'
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_ROW_LEVEL_SECURITY"
    ,base_col="RLS_VARIABLE_NM"
    ,rule_type='SOFTSELECT_HOOK'
    ,rule_value="services/validations/columns_in_libds"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
/* test softselect on numeric var (should be ordered numerically) */
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_X_TEST"
    ,base_col="SOME_BESTNUM"
    ,rule_type='SOFTSELECT'
    ,rule_value="&lib..MPE_X_TEST.SOME_BESTNUM"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_X_TEST"
    ,base_col="SOME_NUM"
    ,rule_type='HARDSELECT_HOOK'
    ,rule_value="services/validations/mpe_x_test.some_num"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_EXCEL_CONFIG"
    ,base_col="XL_ACTIVE"
    ,rule_type='MINVAL'
    ,rule_value='0'
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_EXCEL_CONFIG"
    ,base_col="XL_ACTIVE"
    ,rule_type='MAXVAL'
    ,rule_value='1'
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;
insert into &lib..MPE_VALIDATIONS set
    tx_from=0
    ,base_lib="&lib"
    ,base_ds="MPE_XLMAP_INFO"
    ,base_col="XLMAP_ID"
    ,rule_type='SOFTSELECT'
    ,rule_value="&lib..MPE_XLMAP_RULES.XLMAP_ID"
    ,rule_active=1
    ,tx_to='31DEC5999:23:59:59'dt;


/**
  * MPE_X_TEST
  */
  insert into &lib..mpe_x_test
    set primary_key_field=0
      ,some_char='this is dummy data'
      ,some_dropdown='Option 1'
      ,some_num=42
      ,some_date=42
      ,some_datetime=42
      ,some_time=42
      ,some_shortnum=3
      ,some_bestnum=44;
  insert into &lib..mpe_x_test
    set primary_key_field=1
      ,some_char='more dummy data'
      ,some_dropdown='Option 2'
      ,some_num=42
      ,some_date=42
      ,some_datetime=42
      ,some_time=422
      ,some_shortnum=3
      ,some_bestnum=44;
  insert into &lib..mpe_x_test
    set primary_key_field=2
      ,some_char='even more dummy data'
      ,some_dropdown='Option 3'
      ,some_num=42
      ,some_date=42
      ,some_datetime=42
      ,some_time=142
      ,some_shortnum=3
      ,some_bestnum=44;
  insert into &lib..mpe_x_test
    set primary_key_field=3
      ,some_char=repeat('It was a dark and stormy night.  The wind was blowing'
        !!' a gale!  The captain said to his mate - mate, tell us a tale.  And'
        !!' this, is the tale he told: ',3)
      ,some_dropdown='Option 2'
      ,some_num=1613.001
      ,some_date=423
      ,some_datetime=423
      ,some_time=44
      ,some_shortnum=3
      ,some_bestnum=44;
  insert into &lib..mpe_x_test
    set primary_key_field=4
      ,some_char='if you can fill the unforgiving minute'
      ,some_dropdown='Option 1'
      ,some_num=1613.001123456
      ,some_date=4231
      ,some_datetime=423123123
      ,some_time=412
      ,some_shortnum=3
      ,some_bestnum=44;
%do x=10 %to 500;
  insert into &lib..mpe_x_test
    set primary_key_field=10&x
      ,some_char="&x bottles of beer on the wall"
      ,some_dropdown='Option 1'
      ,some_num=ranuni(0)
      ,some_date=round(ranuni(0)*1000,1)
      ,some_datetime=round(ranuni(0)*50000,1)
      ,some_time=round(ranuni(0)*100,1)
      ,some_shortnum=round(ranuni(0)*100,1)
      ,some_bestnum=round(ranuni(0)*100,1);
%end;


/* https://support.sas.com/resources/papers/proceedings/proceedings/sugi27/p056-27.pdf */
proc format library=&lib..mpe_x_catalog;
  value otdate
    .Z = 'Some Zs'
    .N = 'Some 9s'
    other = [date9.]
  ;
  invalue disc
    'ABC' = 0.20
    'DEF' = 0.25
    'XYZ' = 0.00
    other = 0.00
  ;
  invalue indate
    '00000000' = .Z
    '99999999' = .N
    other = [yymmdd8.]
  ;
  value age(multilabel)
    20 - 29 = '20 - 29'
    30 - 39 = '30 - 39'
    40 - 49 = '40 - 49'
    50 - 59 = '50 - 59'
    60 - high = '60 +++'
    20 - 35 = '20 - 35'
    36 - 55 = '36 - 55'
    55 - high = '55 +++'
    ;
/* https://libguides.library.kent.edu/SAS/UserDefinedFormats */
  VALUE $GENDERLABEL
    "M"   = "Male"
    "F"   = "Female"
  ;
  VALUE LIKERT_SEVEN
    1   = "Strongly Disagree"
    2   = "Disagree"
    3   = "Slightly Disagree"
    4   = "Neither Agree nor Disagree"
    5   = "Slightly Agree"
    6   = "Agree"
    7   = "Strongly Agree"
  ;
  VALUE LIKERT7_ELEVEN
    1,2,3   = "Disagree"
    4       = "Neither Agree nor Disagree"
    5,6,7   = "Agree"
  ;
  VALUE LIKERT7_SISTERS
    1-3     = "Disagree"
    4       = "Neither Agree nor Disagree"
    5-7     = "Agree"
  ;
  VALUE INCOME
    LOW   -< 20000 = "Low"
    20000 -< 60000 = "Middle"
    60000 - HIGH   = "High"
  ;
  VALUE RACE
    1     = "White"
    2     = "Black"
    OTHER = "Other"
  ;
  VALUE GENDERCODE
    0 = 'Male'
    1 = 'Female';
  VALUE ATHLETECODE
    0 = 'Non-athlete'
    1 = 'Athlete';
  VALUE SMOKINGCODE
    0 = 'Nonsmoker'
    1 = 'Past smoker'
    2 = 'Current smoker';
/* https://documentation.sas.com/doc/en/pgmsascdc/v_017/proc/p1upn25lbfo6mkn1wncu4dyh9q91.htm */
  value $state
    'Delaware'='DE'
    'Florida'='FL'
    'Ohio'='OH';
  value MYfmt
    /* Format dates prior to 31DEC2011 using only a year. */
    low-'31DEC2011'd=[year4.]

    /* Format 2012 dates using the month and year. */
    '01jan2012'd-'31DEC12'd=[monyy7.]

    /* Format dates 01JAN2013 and beyond using the day, month, and year. */
    '01JAN2013'd-high=[date9.]

    /* Catch missing values. */
    other='n/a';
  value newfmt .='N/A' other=[12.1];
/* https://www.lexjansen.com/nesug/nesug08/cc/cc14.pdf */
  value $genderml (multilabel)
    '1'='Male'
    '2'='Female'
    '1','2',' '='Total people';
  value agemla (multilabel)
    1-4='Preschool'
    1-18='Children'
    19-120='Adults';
  value agemlb (multilabel)
    19-120='Adults'
    1-18='Children'
    1-4='Preschool';
  value agemlc (multilabel notsorted)
    19-120='Adults'
    1-18='Children'
    1-4='Preschool';
%mend mpe_makedata;
