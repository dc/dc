/**
  @file
  @brief Updates the mpe_loads tracking table with success / failure info
  @details

  <h4> SAS Macros </h4>
  @li mp_abort.sas

  @version 9.2
  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/


%macro mpe_loadfail(
    status=FAILED - &syscc
    ,now=%sysfunc(datetime())
    ,approvals=
    ,mperef=
    ,reason_txt=
    ,mac=mpe_loadfail.sas
    ,dc_dttmtfmt=E8601DT26.6
  );
  /* do not perform duration calc in pass through */
  %local dur;
  data _null_;
    now=symget('now');
    dur=%sysfunc(datetime())-&now;
    call symputx('dur',dur,'l');
  run;
  proc sql;
  update &mpelib..mpe_loads
    set STATUS=symget('status')
        , duration=&dur
        , processed_dttm=&dc_dttmtfmt.
        , approvals = symget('approvals')
        , reason_txt= symget('reason_txt')
    where CSV_DIR="&mperef";
    %let syscc=666;
  %mp_abort(msg=%superq(status)\n%superq(reason_txt),mac=&mac)
%mend mpe_loadfail;
