/**
  @file
  @brief Testing mpe_dsmeta macro
  @details Checking functionality of mpe_dsmeta.sas macro

  <h4> SAS Macros </h4>
  @li mp_assertdsobs.sas
  @li mp_assertscope.sas
  @li mpe_dsmeta.sas

  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.

**/

/* run the macro*/
%mp_assertscope(SNAPSHOT)
%mpe_dsmeta(&mpelib..mpe_security, outds=test1)
%mp_assertscope(COMPARE,
  desc=Checking macro variables against previous snapshot
)

data work.test1;
  set work.test1;
  where ods_table in ('MPE_DATADICTIONARY','MPE_TABLES');
  putlog (_all_)(=);
run;

%mp_assertdsobs(work.test1,
  desc=Test 1 - 27 records returned,
  test=EQUALS 27,
  outds=work.test_results
)


