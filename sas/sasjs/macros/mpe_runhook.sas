/**
  @file
  @brief Runs a hook from MPE_TABLES
  @details Takes a hook script, identifies whether it is a
  SAS Program or Logical Job / STP and executes the corresponding
  code.

  <h4> SAS Macros </h4>
  @li dc_getservicecode.sas
  @li mf_getapploc.sas
  @li mp_abort.sas
  @li mp_include.sas

  @author 4GL Apps Ltd
  @copyright 4GL Apps Ltd.  This code may only be used within Data Controller
    and may not be re-distributed or re-sold without the express permission of
    4GL Apps Ltd.
**/


%macro mpe_runhook(hookvar);
%local pgmloc pgmtype;
%let pgmtype=0;
%put &sysmacroname: &=hookvar;
%if %length(&&&hookvar)>0 %then %do;
  %put &sysmacroname: Executing &&&hookvar;
  data _null_;
    rule_value=symget("&hookvar");
    if scan(upcase(rule_value),-1,'.')='SAS' then do;
      call symputx('pgmtype','PGM');
      call symputx('pgmloc',rule_value);
    end;
    else do;
      apploc="%mf_getapploc()";
      if substr(rule_value,1,1) ne '/'
      then rule_value=cats(apploc,'/',rule_value);
      call symputx('pgmloc',rule_value);
      call symputx('pgmtype','JOB');
    end;
  run;

  %if &pgmtype=PGM %then %do;
    filename sascode "&pgmloc";
  %end;
  %else %do;
    %dc_getservicecode(loc=&pgmloc
      ,outref=sascode
    )
  %end;

  /* the below script will need to modify work.STAGING_DS */
  %local x; %let x=; /* legacy feature */
  %mp_include(sascode)

%end;

%mend mpe_runhook;
