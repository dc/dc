_webout=`{"SYSDATE" : "26SEP22"
,"SYSTIME" : "08:51"
, "emails":
[

]
, "groups":
[
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000W",
"GROUPNAME": "Data Management Business Approvers",
"GROUPDESC": "Business approvers of the data management environment."
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000V",
"GROUPNAME": "Data Management Business Users",
"GROUPDESC": "Business users of the data management environment."
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001G",
"GROUPNAME": "Decision Manager Users",
"GROUPDESC": "Decision Manager Users Group"
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000Y",
"GROUPNAME": "Data Management Executives",
"GROUPDESC": "Executive users of the data management environment."
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000X",
"GROUPNAME": "Data Management Power Users",
"GROUPDESC": "Power users of the data management environment."
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000P",
"GROUPNAME": "BI Web Services Users",
"GROUPDESC": "Allows members to create and delete SAS BI Web Services."
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001O",
"GROUPNAME": "mdlmgradminusers",
"GROUPDESC": "Administrative Users for Model Manager"
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001F",
"GROUPNAME": "Decision Manager Common Administrators",
"GROUPDESC": "Decision Manager Administrative Group"
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001Q",
"GROUPNAME": "mdlmgrusers",
"GROUPDESC": "Limited write access users of Model Manager"
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000U",
"GROUPNAME": "Data Management Stewards",
"GROUPDESC": "Steward users of the data management environment."
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000T",
"GROUPNAME": "Data Management Administrators",
"GROUPDESC": "Administrative users of the data management environment."
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001C",
"GROUPNAME": "BI Dashboard Administrators",
"GROUPDESC": "The members of this group are allowed to administer content for the SAS BI Dashboard product. The SAS Trusted User is made a member during initial deployment."
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A5000013",
"GROUPNAME": "Visual Analytics Users",
"GROUPDESC": "Registered users of SAS Visual Analytics."
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000J",
"GROUPNAME": "Visual Data Builder Administrators",
"GROUPDESC": "Visual Data Builder Administrators"
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001D",
"GROUPNAME": "BI Dashboard Users",
"GROUPDESC": "The members of this group are allowed general viewing access of content for the SAS BI Dashboard product."
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000Q",
"GROUPNAME": "ThemeDesigner Administrators",
"GROUPDESC": ""
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001P",
"GROUPNAME": "mdlmgradvusers",
"GROUPDESC": "Advanced Users for Model Manager"
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A5000021",
"GROUPNAME": "Factory Miner Database Users",
"GROUPDESC": ""
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000I",
"GROUPNAME": "Visual Analytics Data Administrators",
"GROUPDESC": "Visual Analytics Data Administrators"
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001W",
"GROUPNAME": "SAS_EV_AppServer_Tier",
"GROUPDESC": "SAS Environment Manager App Server Tier Users"
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001U",
"GROUPNAME": "SAS_EV_Super_User",
"GROUPDESC": "SAS Environment Manager Super Users"
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001V",
"GROUPNAME": "SAS_EV_Guest",
"GROUPDESC": "SAS Environment Manager Guests"
}
]
, "roles":
[
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000B",
"ROLENAME": "Add-In for Microsoft Office: OLAP",
"ROLEDESC": "Supports viewing OLAP cubes in PivotTables and provides other capabilities."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000N",
"ROLENAME": "Job Execution: Job Designer",
"ROLEDESC": "Provides job and task definition capabilities."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000S",
"ROLENAME": "Fonts Administrator",
"ROLEDESC": "Font Administrator can reload fonts to update the fonts metadata."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A5000014",
"ROLENAME": "Visual Analytics: Report Viewing",
"ROLEDESC": "Provides report viewing capabilities in the Visual Analytics suite."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001R",
"ROLENAME": "mdlmgradminusage",
"ROLEDESC": "Provides ability to perform all Model Manager tasks"
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A5000016",
"ROLENAME": "Visual Analytics: Data Building",
"ROLEDESC": "Provides data preparation capabilities in the Visual Analytics suite."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001Z",
"ROLENAME": "Factory Miner: User",
"ROLEDESC": "Provides Factory Miner User capabilities to create projects and run models."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000M",
"ROLENAME": "Job Execution: Job Submitter",
"ROLEDESC": "Provides normal job submission capabilities."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001N",
"ROLENAME": "SAS Studio: Report Consumer",
"ROLEDESC": "Provides access to view or run existing SAS Studio reports without general access to SAS programming environment."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001M",
"ROLENAME": "SAS Studio: Usage",
"ROLEDESC": "Provides access to the SAS programming environment."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000K",
"ROLENAME": "Comments: Administrator",
"ROLEDESC": ""
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001A",
"ROLENAME": "Web Report Studio: Report Creation",
"ROLEDESC": "Provides report creation capabilities."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001Y",
"ROLENAME": "Time Series Studio: User",
"ROLEDESC": "Enables normal user access to the product and features."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A5000009",
"ROLENAME": "Add-In for Microsoft Office: Advanced",
"ROLEDESC": "Provides all capabilities in the SAS Add-In for Microsoft Office."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000C",
"ROLENAME": "Enterprise Guide: Advanced",
"ROLEDESC": "Provides all capabilities in SAS Enterprise Guide."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001E",
"ROLENAME": "BI Dashboard: Administration",
"ROLEDESC": "Provides SAS BI Dashboard administration capabilities."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000O",
"ROLENAME": "Job Execution: Job Scheduler",
"ROLEDESC": "Provides job scheduling capabilities."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000R",
"ROLENAME": "Theme Designer: Administration",
"ROLEDESC": "Provides Theme Designer administration capabilities."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001S",
"ROLENAME": "mdlmgradvusage",
"ROLEDESC": "Provides the ability to perform all SAS Model Manager tasks, except for administrative tasks"
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001H",
"ROLENAME": "Decision Manager Common: Administration",
"ROLEDESC": "Decision Manager Common Administrative role"
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A5000012",
"ROLENAME": "Home: Usage",
"ROLEDESC": "Provides all non-administrative capabilities for the home page (hub)."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001J",
"ROLENAME": "Forecast Server: Analyst",
"ROLEDESC": "Provides capabiliites for a Forecasting Analyst."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000L",
"ROLENAME": "Job Execution: Job Administrator",
"ROLEDESC": "Provides all capabilities for the Job Execution Service, a component of the Web Infra Platform Services."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A5000020",
"ROLENAME": "Factory Miner: Admin",
"ROLEDESC": "Provides Factory Miner Admin capabilities."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000E",
"ROLENAME": "Enterprise Guide: OLAP",
"ROLEDESC": "Supports viewing OLAP cubes in the OLAP Analyzer and provides other capabilities."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001I",
"ROLENAME": "Forecast Server: Browser",
"ROLEDESC": "Provides basic forecasting viewing and reporting capabilities."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001T",
"ROLENAME": "mdlmgrusage",
"ROLEDESC": "Provides the ability to perform all SAS Model Manager tasks, except for advanced or administrative tasks"
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001X",
"ROLENAME": "Time Series Studio: Administrator",
"ROLEDESC": "Enables administrator access to the product and features, including access to all product content regardless of ownership."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001K",
"ROLENAME": "Forecast Server: Forecaster",
"ROLEDESC": "Provides capabilities for a Forecaster."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A5000011",
"ROLENAME": "Home: Administration",
"ROLEDESC": "Provides all capabilities for the home page (hub)."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001B",
"ROLENAME": "Web Report Studio: Advanced",
"ROLEDESC": "Provides all capabilities in SAS Web Report Studio except the manage report distribution capability."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000D",
"ROLENAME": "Enterprise Guide: Analysis",
"ROLEDESC": "Provides basic data analysis, reporting, and other capabilities."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001L",
"ROLENAME": "Forecast Server: Administrator",
"ROLEDESC": "Provides capabilities for a Forecasting Administrator."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000Z",
"ROLENAME": "Lineage: Administration",
"ROLEDESC": "Provides all functionality related to administrative activities for the SAS Lineage application."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A5000017",
"ROLENAME": "Visual Analytics: Administration",
"ROLEDESC": "Provides administrative capabilities in the Visual Analytics suite."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A5000019",
"ROLENAME": "Web Report Studio: Report Viewing",
"ROLEDESC": "Provides report viewing capabilities."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000F",
"ROLENAME": "Enterprise Guide: Programming",
"ROLEDESC": "Provides SAS programming, stored process authoring, and other capabilities."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A5000018",
"ROLENAME": "Visual Analytics: Basic",
"ROLEDESC": "Provides functionality for guest access (if applicable) and entry-level users."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A5000010",
"ROLENAME": "Data Management: Lineage",
"ROLEDESC": "Provides default access to the SAS Lineage application."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000A",
"ROLENAME": "Add-In for Microsoft Office: Analysis",
"ROLEDESC": "Provides basic data analysis, reporting, and other capabilities."
},
{
"ROLEURI": "OMSOBJ:IdentityGroup\A59LNVZG.A5000015",
"ROLENAME": "Visual Analytics: Analysis",
"ROLEDESC": "If SAS Visual Statistics is licensed, provides the Build Analytical Model capability."
}
]
, "logins":
[
{
"DOMAIN": "DefaultAuth",
"USERID": "SAS\sasdemo"
}
]
, "info":
[
{
"NAME": "sasdemo",
"DISPLAYNAME": "SAS Demo User",
"METADATACREATED": "19Aug2020:06:22:00",
"METADATAUPDATED": "19Aug2020:06:22:00"
}
]
,"_DEBUG" : ""
,"_METAUSER": "sasdemo@SAS"
,"_METAPERSON": "sasdemo"
,"_PROGRAM" : "/Projects/app/dc/services/usernav/usergroupsbymember"
,"AUTOEXEC" : "D%3A%5Copt%5Csasinside%5CConfig%5CLev1%5CSASApp%5CStoredProcessServer%5Cautoexec.sas"
,"MF_GETUSER" : "sasdemo"
,"SYSCC" : "0"
,"SYSENCODING" : "wlatin1"
,"SYSERRORTEXT" : ""
,"SYSHOSTNAME" : "SAS"
,"SYSPROCESSID" : "41DD8057E09DF3B640C28A0000000000"
,"SYSPROCESSMODE" : "SAS Stored Process Server"
,"SYSPROCESSNAME" : ""
,"SYSJOBID" : "27448"
,"SYSSCPL" : "X64_DSRV16"
,"SYSSITE" : "123"
,"SYSUSERID" : "sassrv"
,"SYSVLONG" : "9.04.01M7P080520"
,"SYSWARNINGTEXT" : "ENCODING option ignored for files opened with RECFM=N."
,"END_DTTM" : "2022-09-26T08:51:15.304000"
,"MEMSIZE" : "46GB"
}`
