_webout=`{"SYSDATE" : "26SEP22"
,"SYSTIME" : "08:51"
, "sasmembers":
[
{
"URIMEM": "OMSOBJ:Person\A59LNVZG.AP000002",
"GROUPID": "A59LNVZG.A500001C",
"GROUPNAME": "BI Dashboard Administrators",
"GROUP_OR_ROLE": "UserGroup",
"MEMBERNAME": "sastrust",
"MEMBERTYPE": "User",
"MEMBERUPDATED": "19Aug2020:06:21:44",
"MEMBERCREATED": "19Aug2020:06:21:44",
"EMAILURI": "",
"GROUPDESC": "The members of this group are allowed to administer content for the SAS BI Dashboard product. The SAS Trusted User is made a member during initial deployment.",
"EMAIL": "",
"EMAILRC": -4
},
{
"URIMEM": "OMSOBJ:Person\A59LNVZG.AP000003",
"GROUPID": "A59LNVZG.A500001C",
"GROUPNAME": "BI Dashboard Administrators",
"GROUP_OR_ROLE": "UserGroup",
"MEMBERNAME": "sasdemo",
"MEMBERTYPE": "User",
"MEMBERUPDATED": "19Aug2020:06:22:00",
"MEMBERCREATED": "19Aug2020:06:22:00",
"EMAILURI": "",
"GROUPDESC": "The members of this group are allowed to administer content for the SAS BI Dashboard product. The SAS Trusted User is made a member during initial deployment.",
"EMAIL": "",
"EMAILRC": -4
}
]
,"_DEBUG" : ""
,"_METAUSER": "sasdemo@SAS"
,"_METAPERSON": "sasdemo"
,"_PROGRAM" : "/Projects/app/dc/services/usernav/usermembersbygroup"
,"AUTOEXEC" : "D%3A%5Copt%5Csasinside%5CConfig%5CLev1%5CSASApp%5CStoredProcessServer%5Cautoexec.sas"
,"MF_GETUSER" : "sasdemo"
,"SYSCC" : "0"
,"SYSENCODING" : "wlatin1"
,"SYSERRORTEXT" : ""
,"SYSHOSTNAME" : "SAS"
,"SYSPROCESSID" : "41DD8057E17EB85240C2E28000000000"
,"SYSPROCESSMODE" : "SAS Stored Process Server"
,"SYSPROCESSNAME" : ""
,"SYSJOBID" : "27448"
,"SYSSCPL" : "X64_DSRV16"
,"SYSSITE" : "123"
,"SYSUSERID" : "sassrv"
,"SYSVLONG" : "9.04.01M7P080520"
,"SYSWARNINGTEXT" : "ENCODING option ignored for files opened with RECFM=N."
,"END_DTTM" : "2022-09-26T08:51:18.468000"
,"MEMSIZE" : "46GB"
}`
