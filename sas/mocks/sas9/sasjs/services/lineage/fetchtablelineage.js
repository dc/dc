_webout=`{"SYSDATE" : "29MAR23"
,"SYSTIME" : "09:26"
, "finalfinal":
[
  {
    "LINE": "strict digraph \\"nolib.WEBTEST4\\" {"
  },
  {
    "LINE": "rankdir=LR; nodesep=0.5; node [shape = octagon];"
  },
  {
    "LINE": "subgraph cluster_libs { label=\\"Libraries\\";"
  },
  {
    "LINE": "SANKEY [label=\\"SANKEY\\"; style=\\"filled\\"; color=\\"#e6194b\\", shape = Mrecord, fontcolor=white]"
  },
  {
    "LINE": "nolib [label=\\"nolib\\"; style=\\"filled\\"; color=\\"#3cb44b\\", shape = Mrecord, fontcolor=white]"
  },
  {
    "LINE": "}"
  },
  {
    "LINE": "\\"A59LNVZG.BR000023\\" [label=\\"WEBTEST4\\", color=\\"#e6194b\\", shape=cylinder,style=filled,fontcolor=white];"
  },
  {
    "LINE": "\\"A59LNVZG.BR000024\\" [label=\\"WEBTEST4\\", color=\\"#3cb44b\\", shape=cylinder,style=filled,fontcolor=white];"
  },
  {
    "LINE": "\\"A59LNVZG.BX00000E\\" [label=\\"WEBTEST4 - LOAD JOB 0109151127AM\\"];"
  },
  {
    "LINE": "\\"A59LNVZG.BR000023\\" -> \\"A59LNVZG.BX00000E\\" [color=\\"#e6194b\\"];"
  },
  {
    "LINE": "\\"A59LNVZG.BR000024\\" -> \\"A59LNVZG.BX00000E\\" [color=\\"#3cb44b\\"];"
  },
  {
    "LINE": "\\"A59LNVZG.BX00000E\\" -> \\"A59LNVZG.BR000024\\" [color=\\"#3cb44b\\"];"
  },
  {
    "LINE": "}"
  }
]
, "info":
[
  {
    "TABLEID": "A59LNVZG.BR000024",
    "TABLENAME": "WEBTEST4",
    "LIBURI": "OMSOBJ:DatabaseSchema\A59LNVZG.BJ000001",
    "LIBREF": "nolib"
  }
]
, "flatdata":
[
  {
    "JOBID": "A59LNVZG.BX00000E",
    "SRCTABLEID": "A59LNVZG.BR000023",
    "TGTTABLEID": "A59LNVZG.BR000024",
    "JOBNAME": "WEBTEST4 - LOAD JOB 0109151127AM",
    "SRCTABLETYPE": "PhysicalTable",
    "SRCTABLENAME": "WEBTEST4",
    "SRCLIBREF": "SANKEY",
    "TGTTABLETYPE": "PhysicalTable",
    "TGTTABLENAME": "WEBTEST4",
    "TGTLIBREF": "nolib"
  },
  {
    "JOBID": "A59LNVZG.BX00000E",
    "SRCTABLEID": "A59LNVZG.BR000024",
    "TGTTABLEID": "A59LNVZG.BR000024",
    "JOBNAME": "WEBTEST4 - LOAD JOB 0109151127AM",
    "SRCTABLETYPE": "PhysicalTable",
    "SRCTABLENAME": "WEBTEST4",
    "SRCLIBREF": "nolib",
    "TGTTABLETYPE": "PhysicalTable",
    "TGTTABLENAME": "WEBTEST4",
    "TGTLIBREF": "nolib"
  }
]
, "idlookup":
[
  {
    "METAID": "A59LNVZG.BR000023",
    "METATYPE": "TABLE",
    "METANAME": "SANKEY.WEBTEST4"
  },
  {
    "METAID": "A59LNVZG.BR000024",
    "METATYPE": "TABLE",
    "METANAME": "nolib.WEBTEST4"
  },
  {
    "METAID": "A59LNVZG.BX00000E",
    "METATYPE": "JOB",
    "METANAME": "WEBTEST4 - LOAD JOB 0109151127AM"
  }
]
,"_DEBUG" : ""
,"_METAUSER": "sasdemo@SAS-AAP"
,"_METAPERSON": "sasdemo"
,"_PROGRAM" : "/Projects/app/dc2/services/lineage/fetchtablelineage"
,"AUTOEXEC" : "D%3A%5Copt%5Csasinside%5CConfig%5CLev1%5CSASApp%5CStoredProcessServer%5Cautoexec.sas"
,"MF_GETUSER" : "sasdemo"
,"SYSCC" : "0"
,"SYSENCODING" : "wlatin1"
,"SYSERRORTEXT" : ""
,"SYSHOSTNAME" : "SAS-AAP"
,"SYSPROCESSID" : "41DDBCFEEF35C28F40A7840000000000"
,"SYSPROCESSMODE" : "SAS Stored Process Server"
,"SYSPROCESSNAME" : ""
,"SYSJOBID" : "25672"
,"SYSSCPL" : "X64_DSRV16"
,"SYSSITE" : "70068130"
,"SYSUSERID" : "sassrv"
,"SYSVLONG" : "9.04.01M8P011823"
,"SYSWARNINGTEXT" : "ENCODING option ignored for files opened with RECFM=N."
,"END_DTTM" : "2023-03-29T09:26:22.584000"
,"MEMSIZE" : "44GB"
}
`