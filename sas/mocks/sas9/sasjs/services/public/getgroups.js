_webout=`{"SYSDATE" : "26SEP22"
,"SYSTIME" : "08:51"
, "groups":
[
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001C",
"GROUPNAME": "BI Dashboard Administrators",
"GROUPDESC": "The members of this group are allowed to administer content for the SAS BI Dashboard product. The SAS Trusted User is made a member during initial deployment."
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001D",
"GROUPNAME": "BI Dashboard Users",
"GROUPDESC": "The members of this group are allowed general viewing access of content for the SAS BI Dashboard product."
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000P",
"GROUPNAME": "BI Web Services Users",
"GROUPDESC": "Allows members to create and delete SAS BI Web Services."
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000T",
"GROUPNAME": "Data Management Administrators",
"GROUPDESC": "Administrative users of the data management environment."
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000W",
"GROUPNAME": "Data Management Business Approvers",
"GROUPDESC": "Business approvers of the data management environment."
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000V",
"GROUPNAME": "Data Management Business Users",
"GROUPDESC": "Business users of the data management environment."
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000Y",
"GROUPNAME": "Data Management Executives",
"GROUPDESC": "Executive users of the data management environment."
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000X",
"GROUPNAME": "Data Management Power Users",
"GROUPDESC": "Power users of the data management environment."
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000U",
"GROUPNAME": "Data Management Stewards",
"GROUPDESC": "Steward users of the data management environment."
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001F",
"GROUPNAME": "Decision Manager Common Administrators",
"GROUPDESC": "Decision Manager Administrative Group"
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001G",
"GROUPNAME": "Decision Manager Users",
"GROUPDESC": "Decision Manager Users Group"
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A5000021",
"GROUPNAME": "Factory Miner Database Users",
"GROUPDESC": ""
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A5000002",
"GROUPNAME": "PUBLIC",
"GROUPDESC": "Everyone who can access the metadata server."
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A5000008",
"GROUPNAME": "SAS General Servers",
"GROUPDESC": "Allows members to be used for launching stored process servers and pooled workspace servers."
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A5000007",
"GROUPNAME": "SAS System Services",
"GROUPDESC": "Service identities that need access to server definitions or other system resources."
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A5000003",
"GROUPNAME": "SASAdministrators",
"GROUPDESC": "Users who perform metadata administrative tasks."
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A5000001",
"GROUPNAME": "SASUSERS",
"GROUPDESC": "Everyone who has a metadata identity. SASUSERS is a subset of PUBLIC."
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001W",
"GROUPNAME": "SAS_EV_AppServer_Tier",
"GROUPDESC": "SAS Environment Manager App Server Tier Users"
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001V",
"GROUPNAME": "SAS_EV_Guest",
"GROUPDESC": "SAS Environment Manager Guests"
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001U",
"GROUPNAME": "SAS_EV_Super_User",
"GROUPDESC": "SAS Environment Manager Super Users"
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000Q",
"GROUPNAME": "ThemeDesigner Administrators",
"GROUPDESC": ""
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000I",
"GROUPNAME": "Visual Analytics Data Administrators",
"GROUPDESC": "Visual Analytics Data Administrators"
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A5000013",
"GROUPNAME": "Visual Analytics Users",
"GROUPDESC": "Registered users of SAS Visual Analytics."
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500000J",
"GROUPNAME": "Visual Data Builder Administrators",
"GROUPDESC": "Visual Data Builder Administrators"
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001O",
"GROUPNAME": "mdlmgradminusers",
"GROUPDESC": "Administrative Users for Model Manager"
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001P",
"GROUPNAME": "mdlmgradvusers",
"GROUPDESC": "Advanced Users for Model Manager"
},
{
"GROUPURI": "OMSOBJ:IdentityGroup\A59LNVZG.A500001Q",
"GROUPNAME": "mdlmgrusers",
"GROUPDESC": "Limited write access users of Model Manager"
}
]
,"_DEBUG" : ""
,"_METAUSER": "sasdemo@SAS"
,"_METAPERSON": "sasdemo"
,"_PROGRAM" : "/Projects/app/dc/services/public/getgroups"
,"AUTOEXEC" : "D%3A%5Copt%5Csasinside%5CConfig%5CLev1%5CSASApp%5CStoredProcessServer%5Cautoexec.sas"
,"MF_GETUSER" : "sasdemo"
,"SYSCC" : "0"
,"SYSENCODING" : "wlatin1"
,"SYSERRORTEXT" : ""
,"SYSHOSTNAME" : "SAS"
,"SYSPROCESSID" : "41DD8057DE0624DD40C2100000000000"
,"SYSPROCESSMODE" : "SAS Stored Process Server"
,"SYSPROCESSNAME" : ""
,"SYSJOBID" : "27448"
,"SYSSCPL" : "X64_DSRV16"
,"SYSSITE" : "123"
,"SYSUSERID" : "sassrv"
,"SYSVLONG" : "9.04.01M7P080520"
,"SYSWARNINGTEXT" : "ENCODING option ignored for files opened with RECFM=N."
,"END_DTTM" : "2022-09-26T08:51:04.736000"
,"MEMSIZE" : "46GB"
}`
