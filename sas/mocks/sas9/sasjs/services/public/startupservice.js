_webout=`{"SYSDATE" : "26SEP22"
,"SYSTIME" : "08:29"
, "sasdatasets":
[
{
"LIBREF": "DC996664",
"DSN": "MPE_ALERTS"
},
{
"LIBREF": "DC996664",
"DSN": "MPE_COLUMN_LEVEL_SECURITY"
},
{
"LIBREF": "DC996664",
"DSN": "MPE_CONFIG"
},
{
"LIBREF": "DC996664",
"DSN": "MPE_DATADICTIONARY"
},
{
"LIBREF": "DC996664",
"DSN": "MPE_EMAILS"
},
{
"LIBREF": "DC996664",
"DSN": "MPE_EXCEL_CONFIG"
},
{
"LIBREF": "DC996664",
"DSN": "MPE_GROUPS"
},
{
"LIBREF": "DC996664",
"DSN": "MPE_LOCKANYTABLE"
},
{
"LIBREF": "DC996664",
"DSN": "MPE_ROW_LEVEL_SECURITY"
},
{
"LIBREF": "DC996664",
"DSN": "MPE_SECURITY"
},
{
"LIBREF": "DC996664",
"DSN": "MPE_SELECTBOX"
},
{
"LIBREF": "DC996664",
"DSN": "MPE_TABLES"
},
{
"LIBREF": "DC996664",
"DSN": "MPE_VALIDATIONS"
},
{
"LIBREF": "DC996664",
"DSN": "MPE_X_TEST"
}
]
, "saslibs":
[
{
"LIBREF": "DC996664"
}
]
, "globvars":
[
{
"DCLIB": "DC996664",
"SAS9LINEAGE_ENABLED": 1,
"ISREGISTERED": 1,
"REGISTERCOUNT": 1,
"DC_ADMIN_GROUP": "Data Management Business Approvers",
"LICENCE_KEY": "",
"ACTIVATION_KEY": "",
"DC_RESTRICT_EDITRECORD": "NO"
}
]
,"xlmaps": [
  [
      "BASEL-CR2",
      "",
      "DC866788.MPE_XLMAP_DATA"
  ],
  [
      "BASEL-KM1",
      "Basel 3 Key Metrics report",
      "DC866788.MPE_XLMAP_DATA"
  ],
  [
      "SAMPLE",
      "",
      "DC866788.MPE_XLMAP_DATA"
  ]
]
,"_DEBUG" : ""
,"_METAUSER": "sasdemo@SAS"
,"_METAPERSON": "sasdemo"
,"_PROGRAM" : "/Projects/app/dc/services/public/startupservice"
,"AUTOEXEC" : "D%3A%5Copt%5Csasinside%5CConfig%5CLev1%5CSASApp%5CStoredProcessServer%5Cautoexec.sas"
,"MF_GETUSER" : "sasdemo"
,"SYSCC" : "0"
,"SYSENCODING" : "wlatin1"
,"SYSERRORTEXT" : ""
,"SYSHOSTNAME" : "SAS"
,"SYSPROCESSID" : "41DD8056944A8F5C409C500000000000"
,"SYSPROCESSMODE" : "SAS Stored Process Server"
,"SYSPROCESSNAME" : ""
,"SYSJOBID" : "27448"
,"SYSSCPL" : "X64_DSRV16"
,"SYSSITE" : "123"
,"SYSUSERID" : "sassrv"
,"SYSVLONG" : "9.04.01M7P080520"
,"SYSWARNINGTEXT" : "ENCODING option ignored for files opened with RECFM=N."
,"END_DTTM" : "2022-09-26T08:29:06.092000"
,"MEMSIZE" : "46GB"
}`
