const path = require('path')

let iwantFileText = ''

let appLoc = path.join(..._program.split('services')[0].split('/'))
const sessionStoragePath = path.resolve(__dirname, '..', '..', 'drive', 'files', appLoc, 'mock-storage')

if (!fs.existsSync(sessionStoragePath)){
    fs.mkdirSync(sessionStoragePath);
}

if (_WEBIN_FILENAME1.includes('filterquery')) iwantFileText = _WEBIN_FILEREF1.toString()
if (_WEBIN_FILENAME2.includes('filterquery')) iwantFileText = _WEBIN_FILEREF2.toString()

if (iwantFileText.length > 0) {
    const filterStore = path.resolve(sessionStoragePath, 'filter.txt')
    const filterText = iwantFileText.split('\n')[1]

    try {
        fs.writeFileSync(filterStore, filterText)
    } catch (err) {}
}

_webout = `{"SYSDATE" : "26SEP22"
,"SYSTIME" : "08:49"
, "result":
[
{
"FILTER_RK": 1,
"FILTER_HASH": "FFE9C1E5F7AEC3B71F315EF2FEFC2296",
"FILTER_TABLE": "DC996664.MPE_X_TEST"
}
]
,"_DEBUG" : ""
,"_METAUSER": "sasdemo@SAS"
,"_METAPERSON": "sasdemo"
,"_PROGRAM" : "/Projects/app/dc/services/public/validatefilter"
,"AUTOEXEC" : "D%3A%5Copt%5Csasinside%5CConfig%5CLev1%5CSASApp%5CStoredProcessServer%5Cautoexec.sas"
,"MF_GETUSER" : "sasdemo"
,"SYSCC" : "0"
,"SYSENCODING" : "wlatin1"
,"SYSERRORTEXT" : ""
,"SYSHOSTNAME" : "SAS"
,"SYSPROCESSID" : "41DD8057CAEDE35440BF960000000000"
,"SYSPROCESSMODE" : "SAS Stored Process Server"
,"SYSPROCESSNAME" : ""
,"SYSJOBID" : "27448"
,"SYSSCPL" : "Linunx"
,"SYSSITE" : "123"
,"SYSUSERID" : "sassrv"
,"SYSVLONG" : "9.04.01M7P080520"
,"SYSWARNINGTEXT" : "ENCODING option ignored for files opened with RECFM=N."
,"END_DTTM" : "2022-09-26T08:49:48.696000"
,"MEMSIZE" : "46GB"
}
`