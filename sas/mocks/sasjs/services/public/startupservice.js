const path = require('path')

let appLoc = path.join(..._program.split('services')[0].split('/'))
const sessionStoragePath = path.resolve(__dirname, '..', '..', 'drive', 'files', appLoc, 'mock-storage')
const licenceStore = path.resolve(sessionStoragePath, 'licence.json')
const usersStore = path.resolve(sessionStoragePath, 'users.json')

let licence = {}
let users = {}

licence = {
    activationKey: '',
    licenceKey: ''
}

users = {
    REGISTERCOUNT: 1,
    ISREGISTERED: 1
}

try {
    const licenceRaw = fs.readFileSync(licenceStore, {encoding:'utf8'}).toString()
    licence = JSON.parse(licenceRaw)
} catch(err) {

}

try {
    const usersRaw = fs.readFileSync(usersStore, {encoding:'utf8'}).toString()
    users = JSON.parse(usersRaw)

    if (users.ISREGISTERED === 0) {
        const newUsers = {
            REGISTERCOUNT: users.REGISTERCOUNT,
            ISREGISTERED: 1
        }

        try {
            fs.writeFileSync(usersStore, JSON.stringify(newUsers))
        } catch (err) {}
    }
} catch(err) {

}

_webout = `{"SYSDATE" : "26SEP22"
,"SYSTIME" : "08:29"
, "sasdatasets":
[
{
"LIBREF": "DC996664",
"DSN": "MPE_X_TEST"
},
{
  "LIBREF": "DC996664",
  "DSN": "MPE_DATADICTIONARY"
},
{
  "LIBREF": "DC996664",
  "DSN": "MPE_USERS"
},
{
    "LIBREF": "DC996664",
    "DSN": "MPE_TABLES"
}
]
, "saslibs":
[
{
"LIBREF": "DC996664"
}
]
, "globvars":
[
{
"DCLIB": "DC996664",
"SAS9LINEAGE_ENABLED": 1,
"ISREGISTERED": ${users.ISREGISTERED},
"REGISTERCOUNT": ${users.REGISTERCOUNT},
"DC_ADMIN_GROUP": "Data Management Business Approvers",
"LICENCE_KEY": "${licence.licenceKey}",
"ACTIVATION_KEY": "${licence.activationKey}",
"DC_RESTRICT_EDITRECORD": "NO"
}
]
,"xlmaps":
[
["BASEL-CR2" ,"" ,"DC695588.MPE_XLMAP_DATA" ]
,["BASEL-KM1" ,"Basel 3 Key Metrics report" ,"DC695588.MPE_XLMAP_DATA" ]
,["SAMPLE" ,"" ,"DC695588.MPE_XLMAP_DATA" ]
]
,"_DEBUG" : ""
,"_METAUSER": "sasdemo@SAS"
,"_METAPERSON": "sasdemo"
,"_PROGRAM" : "/Projects/app/dc/services/public/startupservice"
,"AUTOEXEC" : "D%3A%5Copt%5Csasinside%5CConfig%5CLev1%5CSASApp%5CStoredProcessServer%5Cautoexec.sas"
,"MF_GETUSER" : "sasdemo"
,"SYSCC" : "0"
,"SYSENCODING" : "wlatin1"
,"SYSERRORTEXT" : ""
,"SYSHOSTNAME" : "SAS"
,"SYSPROCESSID" : "41DD8056944A8F5C409C500000000000"
,"SYSPROCESSMODE" : "SAS Stored Process Server"
,"SYSPROCESSNAME" : ""
,"SYSJOBID" : "27448"
,"SYSSCPL" : "Linunx"
,"SYSSITE" : "123"
,"SYSUSERID" : "sassrv"
,"SYSVLONG" : "9.04.01M7P080520"
,"SYSWARNINGTEXT" : "ENCODING option ignored for files opened with RECFM=N."
,"END_DTTM" : "2022-09-26T08:29:06.092000"
,"MEMSIZE" : "46GB"
}`