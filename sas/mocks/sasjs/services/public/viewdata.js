const path = require('path')

let appLoc = path.join(..._program.split('services')[0].split('/'))
const sessionStoragePath = path.resolve(__dirname, '..', '..', 'drive', 'files', appLoc, 'mock-storage')

let filterText = ''

if (fs.existsSync(sessionStoragePath)){
    const filterStore = path.resolve(sessionStoragePath, 'filter.txt')

    try {
        filterText = fs.readFileSync(filterStore).toString()
    } catch (err) {}
}

let webouts = {
    MPE_X_TEST: `{"SYSDATE" : "26SEP22"
    ,"SYSTIME" : "08:48"
    , "cls_rules":
    [

    ]
    , "cols":
    [
    {
    "NAME": "PRIMARY_KEY_FIELD",
    "LENGTH": 8,
    "VARNUM": 1,
    "LABEL": "PRIMARY_KEY_FIELD",
    "FMTNAME": "",
    "FORMAT": "8.",
    "TYPE": "N",
    "DDTYPE": "NUMERIC"
    },
    {
    "NAME": "SOME_BESTNUM",
    "LENGTH": 8,
    "VARNUM": 9,
    "LABEL": "SOME_BESTNUM",
    "FMTNAME": "BEST",
    "FORMAT": "BEST.",
    "TYPE": "N",
    "DDTYPE": "NUMERIC"
    },
    {
    "NAME": "SOME_CHAR",
    "LENGTH": 32767,
    "VARNUM": 2,
    "LABEL": "SOME_CHAR",
    "FMTNAME": "",
    "FORMAT": "$32767.",
    "TYPE": "C",
    "DDTYPE": "CHARACTER"
    },
    {
    "NAME": "SOME_DATE",
    "LENGTH": 8,
    "VARNUM": 5,
    "LABEL": "SOME_DATE",
    "FMTNAME": "DATE",
    "FORMAT": "DATE9.",
    "TYPE": "N",
    "DDTYPE": "DATE"
    },
    {
    "NAME": "SOME_DATETIME",
    "LENGTH": 8,
    "VARNUM": 6,
    "LABEL": "SOME_DATETIME",
    "FMTNAME": "DATETIME",
    "FORMAT": "DATETIME19.",
    "TYPE": "N",
    "DDTYPE": "DATETIME"
    },
    {
    "NAME": "SOME_DROPDOWN",
    "LENGTH": 128,
    "VARNUM": 3,
    "LABEL": "SOME_DROPDOWN",
    "FMTNAME": "",
    "FORMAT": "$128.",
    "TYPE": "C",
    "DDTYPE": "CHARACTER"
    },
    {
    "NAME": "SOME_NUM",
    "LENGTH": 8,
    "VARNUM": 4,
    "LABEL": "SOME_NUM",
    "FMTNAME": "",
    "FORMAT": "8.",
    "TYPE": "N",
    "DDTYPE": "NUMERIC"
    },
    {
    "NAME": "SOME_SHORTNUM",
    "LENGTH": 4,
    "VARNUM": 8,
    "LABEL": "SOME_SHORTNUM",
    "FMTNAME": "",
    "FORMAT": "4.",
    "TYPE": "N",
    "DDTYPE": "NUMERIC"
    },
    {
    "NAME": "SOME_TIME",
    "LENGTH": 8,
    "VARNUM": 7,
    "LABEL": "SOME_TIME",
    "FMTNAME": "TIME",
    "FORMAT": "TIME8.",
    "TYPE": "N",
    "DDTYPE": "TIME"
    }
    ]
    , "dsmeta":
    [
    {
    "ODS_TABLE": "ATTRIBUTES",
    "NAME": "Data Set Name",
    "VALUE": "DC996664.MPE_X_TEST"
    },
    {
    "ODS_TABLE": "ATTRIBUTES",
    "NAME": "Observations",
    "VALUE": "496"
    },
    {
    "ODS_TABLE": "ATTRIBUTES",
    "NAME": "Member Type",
    "VALUE": "DATA"
    },
    {
    "ODS_TABLE": "ATTRIBUTES",
    "NAME": "Variables",
    "VALUE": "9"
    },
    {
    "ODS_TABLE": "ATTRIBUTES",
    "NAME": "Engine",
    "VALUE": "V9"
    },
    {
    "ODS_TABLE": "ATTRIBUTES",
    "NAME": "Indexes",
    "VALUE": "1"
    },
    {
    "ODS_TABLE": "ATTRIBUTES",
    "NAME": "Created",
    "VALUE": "09/26/2022 08:24:39"
    },
    {
    "ODS_TABLE": "ATTRIBUTES",
    "NAME": "Integrity Constraints",
    "VALUE": "1"
    },
    {
    "ODS_TABLE": "ATTRIBUTES",
    "NAME": "Last Modified",
    "VALUE": "09/26/2022 08:47:46"
    },
    {
    "ODS_TABLE": "ATTRIBUTES",
    "NAME": "Observation Length",
    "VALUE": "32947"
    },
    {
    "ODS_TABLE": "ATTRIBUTES",
    "NAME": "Protection",
    "VALUE": " ."
    },
    {
    "ODS_TABLE": "ATTRIBUTES",
    "NAME": "Deleted Observations",
    "VALUE": "2"
    },
    {
    "ODS_TABLE": "ATTRIBUTES",
    "NAME": "Data Set Type",
    "VALUE": " ."
    },
    {
    "ODS_TABLE": "ATTRIBUTES",
    "NAME": "Compressed",
    "VALUE": "CHAR"
    },
    {
    "ODS_TABLE": "ATTRIBUTES",
    "NAME": "Label",
    "VALUE": " ."
    },
    {
    "ODS_TABLE": "ATTRIBUTES",
    "NAME": "Reuse Space",
    "VALUE": "NO"
    },
    {
    "ODS_TABLE": "ATTRIBUTES",
    "NAME": "Data Representation",
    "VALUE": "WINDOWS_64"
    },
    {
    "ODS_TABLE": "ATTRIBUTES",
    "NAME": "Point to Observations",
    "VALUE": "YES"
    },
    {
    "ODS_TABLE": "ATTRIBUTES",
    "NAME": "Encoding",
    "VALUE": "wlatin1 Western (Windows)"
    },
    {
    "ODS_TABLE": "ATTRIBUTES",
    "NAME": "Sorted",
    "VALUE": "NO"
    },
    {
    "ODS_TABLE": "ENGINEHOST",
    "NAME": "Data Set Page Size",
    "VALUE": "262144"
    },
    {
    "ODS_TABLE": "ENGINEHOST",
    "NAME": "Number of Data Set Pages",
    "VALUE": "3"
    },
    {
    "ODS_TABLE": "ENGINEHOST",
    "NAME": "Index File Page Size",
    "VALUE": "4096"
    },
    {
    "ODS_TABLE": "ENGINEHOST",
    "NAME": "Number of Index File Pages",
    "VALUE": "4"
    },
    {
    "ODS_TABLE": "ENGINEHOST",
    "NAME": "Number of Data Set Repairs",
    "VALUE": "0"
    },
    {
    "ODS_TABLE": "ENGINEHOST",
    "NAME": "ExtendObsCounter",
    "VALUE": "YES"
    },
    {
    "ODS_TABLE": "ENGINEHOST",
    "NAME": "Filename",
    "VALUE": "C:\DataController\DC996664\mpe_x_test.sas7bdat"
    },
    {
    "ODS_TABLE": "ENGINEHOST",
    "NAME": "Release Created",
    "VALUE": "9.0401M7"
    },
    {
    "ODS_TABLE": "ENGINEHOST",
    "NAME": "Host Created",
    "VALUE": "Linunx"
    },
    {
    "ODS_TABLE": "ENGINEHOST",
    "NAME": "Owner Name",
    "VALUE": "BUILTIN\Administrators"
    },
    {
    "ODS_TABLE": "ENGINEHOST",
    "NAME": "File Size",
    "VALUE": " 1MB"
    },
    {
    "ODS_TABLE": "ENGINEHOST",
    "NAME": "File Size (bytes)",
    "VALUE": "1048576"
    }
    ]
    , "query":
    [

    ]
    , "sasparams":
    [
    {
    "TABLEURI": "OMSOBJ:PhysicalTable\A59LNVZG.BR000036",
    "TABLENAME": "MPE_X_TEST",
    "FILTER_TEXT": "${filterText.replace(/\"/gmi, '\\"')}",
    "PK_FIELDS": "PRIMARY_KEY_FIELD",
    "NOBS": 496,
    "VARS": 9,
    "MAXROWS": 250
    }
    ]
    , "viewdata":
    [
    {
    "PRIMARY_KEY_FIELD": 2,
    "SOME_CHAR": "even more dummy data",
    "SOME_DROPDOWN": "Option 3",
    "SOME_NUM": 42,
    "SOME_DATE": "12FEB1960",
    "SOME_DATETIME": " 01JAN1960:00:00:42",
    "SOME_TIME": " 0:02:22",
    "SOME_SHORTNUM": 3,
    "SOME_BESTNUM": 44
    },
    {
    "PRIMARY_KEY_FIELD": 3,
    "SOME_CHAR": "It was a dark and stormy night. The wind was blowing a gale! The captain said to his mate - mate, tell us a tale. And this, is the tale he told: It was a dark and stormy night. The wind was blowing a gale! The captain said to his mate - mate, tell us a tale. And this, is the tale he told: It was a dark and stormy night. The wind was blowing a gale! The captain said to his mate - mate, tell us a tale. And this, is the tale he told: It was a dark and stormy night. The wind was blowing a gale! The captain said to his mate - mate, tell us a tale. And this, is the tale he told:",
    "SOME_DROPDOWN": "Option 2",
    "SOME_NUM": 1613.001,
    "SOME_DATE": "27FEB1961",
    "SOME_DATETIME": " 01JAN1960:00:07:03",
    "SOME_TIME": " 0:00:44",
    "SOME_SHORTNUM": 3,
    "SOME_BESTNUM": 44
    },
    {
    "PRIMARY_KEY_FIELD": 4,
    "SOME_CHAR": "if you can fill the unforgiving minute",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 1613.00112346,
    "SOME_DATE": "02AUG1971",
    "SOME_DATETIME": " 29MAY1973:06:12:03",
    "SOME_TIME": " 0:06:52",
    "SOME_SHORTNUM": 3,
    "SOME_BESTNUM": 44
    },
    {
    "PRIMARY_KEY_FIELD": 1010,
    "SOME_CHAR": "10 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.367786711253,
    "SOME_DATE": "05MAR1961",
    "SOME_DATETIME": " 01JAN1960:08:16:44",
    "SOME_TIME": " 0:00:35",
    "SOME_SHORTNUM": 1,
    "SOME_BESTNUM": 72
    },
    {
    "PRIMARY_KEY_FIELD": 1011,
    "SOME_CHAR": "11 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.869333049687,
    "SOME_DATE": "20JAN1961",
    "SOME_DATETIME": " 01JAN1960:01:25:19",
    "SOME_TIME": " 0:00:01",
    "SOME_SHORTNUM": 6,
    "SOME_BESTNUM": 54
    },
    {
    "PRIMARY_KEY_FIELD": 1012,
    "SOME_CHAR": "12 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.543277906507,
    "SOME_DATE": "06OCT1961",
    "SOME_DATETIME": " 01JAN1960:02:57:35",
    "SOME_TIME": " 0:00:35",
    "SOME_SHORTNUM": 54,
    "SOME_BESTNUM": 62
    },
    {
    "PRIMARY_KEY_FIELD": 1013,
    "SOME_CHAR": "13 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.5051939867,
    "SOME_DATE": "20FEB1962",
    "SOME_DATETIME": " 01JAN1960:06:47:55",
    "SOME_TIME": " 0:00:41",
    "SOME_SHORTNUM": 38,
    "SOME_BESTNUM": 4
    },
    {
    "PRIMARY_KEY_FIELD": 1014,
    "SOME_CHAR": "14 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.0130502507151,
    "SOME_DATE": "13JAN1960",
    "SOME_DATETIME": " 01JAN1960:03:48:13",
    "SOME_TIME": " 0:00:14",
    "SOME_SHORTNUM": 92,
    "SOME_BESTNUM": 57
    },
    {
    "PRIMARY_KEY_FIELD": 1015,
    "SOME_CHAR": "15 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.582270800873,
    "SOME_DATE": "12JUL1962",
    "SOME_DATETIME": " 01JAN1960:12:05:18",
    "SOME_TIME": " 0:00:54",
    "SOME_SHORTNUM": 92,
    "SOME_BESTNUM": 80
    },
    {
    "PRIMARY_KEY_FIELD": 1016,
    "SOME_CHAR": "16 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.138272497867,
    "SOME_DATE": "29AUG1960",
    "SOME_DATETIME": " 01JAN1960:02:48:01",
    "SOME_TIME": " 0:00:01",
    "SOME_SHORTNUM": 28,
    "SOME_BESTNUM": 91
    },
    {
    "PRIMARY_KEY_FIELD": 1017,
    "SOME_CHAR": "17 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.892701324025,
    "SOME_DATE": "14SEP1961",
    "SOME_DATETIME": " 01JAN1960:07:03:58",
    "SOME_TIME": " 0:01:37",
    "SOME_SHORTNUM": 91,
    "SOME_BESTNUM": 72
    },
    {
    "PRIMARY_KEY_FIELD": 1018,
    "SOME_CHAR": "18 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.185278856747,
    "SOME_DATE": "08MAR1961",
    "SOME_DATETIME": " 01JAN1960:00:22:48",
    "SOME_TIME": " 0:00:32",
    "SOME_SHORTNUM": 93,
    "SOME_BESTNUM": 79
    },
    {
    "PRIMARY_KEY_FIELD": 1019,
    "SOME_CHAR": "19 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.0737551018008,
    "SOME_DATE": "24JAN1961",
    "SOME_DATETIME": " 01JAN1960:03:14:33",
    "SOME_TIME": " 0:00:21",
    "SOME_SHORTNUM": 22,
    "SOME_BESTNUM": 90
    },
    {
    "PRIMARY_KEY_FIELD": 1020,
    "SOME_CHAR": "20 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.712856993877,
    "SOME_DATE": "08FEB1961",
    "SOME_DATETIME": " 01JAN1960:01:50:23",
    "SOME_TIME": " 0:01:40",
    "SOME_SHORTNUM": 65,
    "SOME_BESTNUM": 34
    },
    {
    "PRIMARY_KEY_FIELD": 1021,
    "SOME_CHAR": "21 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.67061384426,
    "SOME_DATE": "09MAR1961",
    "SOME_DATETIME": " 01JAN1960:04:52:55",
    "SOME_TIME": " 0:00:13",
    "SOME_SHORTNUM": 44,
    "SOME_BESTNUM": 97
    },
    {
    "PRIMARY_KEY_FIELD": 1022,
    "SOME_CHAR": "22 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.142321579225,
    "SOME_DATE": "22JUL1962",
    "SOME_DATETIME": " 01JAN1960:07:25:01",
    "SOME_TIME": " 0:01:10",
    "SOME_SHORTNUM": 66,
    "SOME_BESTNUM": 98
    },
    {
    "PRIMARY_KEY_FIELD": 1023,
    "SOME_CHAR": "23 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.125984806626,
    "SOME_DATE": "01SEP1962",
    "SOME_DATETIME": " 01JAN1960:09:32:34",
    "SOME_TIME": " 0:01:16",
    "SOME_SHORTNUM": 44,
    "SOME_BESTNUM": 98
    },
    {
    "PRIMARY_KEY_FIELD": 1024,
    "SOME_CHAR": "24 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.389946863702,
    "SOME_DATE": "06DEC1961",
    "SOME_DATETIME": " 01JAN1960:06:53:51",
    "SOME_TIME": " 0:00:33",
    "SOME_SHORTNUM": 30,
    "SOME_BESTNUM": 90
    },
    {
    "PRIMARY_KEY_FIELD": 1025,
    "SOME_CHAR": "25 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.0310356193367,
    "SOME_DATE": "01MAR1960",
    "SOME_DATETIME": " 01JAN1960:02:58:07",
    "SOME_TIME": " 0:00:27",
    "SOME_SHORTNUM": 73,
    "SOME_BESTNUM": 59
    },
    {
    "PRIMARY_KEY_FIELD": 1026,
    "SOME_CHAR": "26 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.905788423915,
    "SOME_DATE": "04OCT1960",
    "SOME_DATETIME": " 01JAN1960:11:17:28",
    "SOME_TIME": " 0:00:41",
    "SOME_SHORTNUM": 82,
    "SOME_BESTNUM": 46
    },
    {
    "PRIMARY_KEY_FIELD": 1027,
    "SOME_CHAR": "27 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.592067585602,
    "SOME_DATE": "15JUL1962",
    "SOME_DATETIME": " 01JAN1960:03:35:41",
    "SOME_TIME": " 0:00:22",
    "SOME_SHORTNUM": 46,
    "SOME_BESTNUM": 73
    },
    {
    "PRIMARY_KEY_FIELD": 1028,
    "SOME_CHAR": "28 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.658003004574,
    "SOME_DATE": "08OCT1960",
    "SOME_DATETIME": " 01JAN1960:13:13:30",
    "SOME_TIME": " 0:00:40",
    "SOME_SHORTNUM": 35,
    "SOME_BESTNUM": 40
    },
    {
    "PRIMARY_KEY_FIELD": 1029,
    "SOME_CHAR": "29 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.807042593978,
    "SOME_DATE": "26DEC1960",
    "SOME_DATETIME": " 01JAN1960:11:57:14",
    "SOME_TIME": " 0:00:19",
    "SOME_SHORTNUM": 80,
    "SOME_BESTNUM": 12
    },
    {
    "PRIMARY_KEY_FIELD": 1030,
    "SOME_CHAR": "30 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.880145040751,
    "SOME_DATE": "15MAY1961",
    "SOME_DATETIME": " 01JAN1960:10:11:05",
    "SOME_TIME": " 0:00:25",
    "SOME_SHORTNUM": 70,
    "SOME_BESTNUM": 19
    },
    {
    "PRIMARY_KEY_FIELD": 1031,
    "SOME_CHAR": "31 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.41501947046,
    "SOME_DATE": "27JAN1962",
    "SOME_DATETIME": " 01JAN1960:11:27:09",
    "SOME_TIME": " 0:01:04",
    "SOME_SHORTNUM": 94,
    "SOME_BESTNUM": 48
    },
    {
    "PRIMARY_KEY_FIELD": 1032,
    "SOME_CHAR": "32 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.974340120319,
    "SOME_DATE": "09JAN1962",
    "SOME_DATETIME": " 01JAN1960:07:44:35",
    "SOME_TIME": " 0:01:07",
    "SOME_SHORTNUM": 43,
    "SOME_BESTNUM": 3
    },
    {
    "PRIMARY_KEY_FIELD": 1033,
    "SOME_CHAR": "33 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.203559569178,
    "SOME_DATE": "07SEP1960",
    "SOME_DATETIME": " 01JAN1960:11:52:19",
    "SOME_TIME": " 0:00:42",
    "SOME_SHORTNUM": 29,
    "SOME_BESTNUM": 56
    },
    {
    "PRIMARY_KEY_FIELD": 1034,
    "SOME_CHAR": "34 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.679243555609,
    "SOME_DATE": "21APR1960",
    "SOME_DATETIME": " 01JAN1960:07:17:04",
    "SOME_TIME": " 0:01:14",
    "SOME_SHORTNUM": 68,
    "SOME_BESTNUM": 9
    },
    {
    "PRIMARY_KEY_FIELD": 1035,
    "SOME_CHAR": "35 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.949411697197,
    "SOME_DATE": "19JAN1960",
    "SOME_DATETIME": " 01JAN1960:10:15:38",
    "SOME_TIME": " 0:01:16",
    "SOME_SHORTNUM": 91,
    "SOME_BESTNUM": 10
    },
    {
    "PRIMARY_KEY_FIELD": 1036,
    "SOME_CHAR": "36 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.544613491066,
    "SOME_DATE": "26OCT1960",
    "SOME_DATETIME": " 01JAN1960:03:55:27",
    "SOME_TIME": " 0:01:24",
    "SOME_SHORTNUM": 72,
    "SOME_BESTNUM": 36
    },
    {
    "PRIMARY_KEY_FIELD": 1037,
    "SOME_CHAR": "37 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.458775893999,
    "SOME_DATE": "21NOV1960",
    "SOME_DATETIME": " 01JAN1960:13:34:37",
    "SOME_TIME": " 0:01:35",
    "SOME_SHORTNUM": 97,
    "SOME_BESTNUM": 32
    },
    {
    "PRIMARY_KEY_FIELD": 1038,
    "SOME_CHAR": "38 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.153719423876,
    "SOME_DATE": "06MAY1961",
    "SOME_DATETIME": " 01JAN1960:06:14:13",
    "SOME_TIME": " 0:00:29",
    "SOME_SHORTNUM": 60,
    "SOME_BESTNUM": 98
    },
    {
    "PRIMARY_KEY_FIELD": 1039,
    "SOME_CHAR": "39 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.493500256209,
    "SOME_DATE": "05JUN1960",
    "SOME_DATETIME": " 01JAN1960:06:59:42",
    "SOME_TIME": " 0:00:45",
    "SOME_SHORTNUM": 95,
    "SOME_BESTNUM": 55
    },
    {
    "PRIMARY_KEY_FIELD": 1040,
    "SOME_CHAR": "40 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.124728858995,
    "SOME_DATE": "09MAR1961",
    "SOME_DATETIME": " 01JAN1960:03:03:06",
    "SOME_TIME": " 0:01:23",
    "SOME_SHORTNUM": 35,
    "SOME_BESTNUM": 79
    },
    {
    "PRIMARY_KEY_FIELD": 1041,
    "SOME_CHAR": "41 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.279442200102,
    "SOME_DATE": "06JUL1962",
    "SOME_DATETIME": " 01JAN1960:05:29:26",
    "SOME_TIME": " 0:00:51",
    "SOME_SHORTNUM": 86,
    "SOME_BESTNUM": 66
    },
    {
    "PRIMARY_KEY_FIELD": 1042,
    "SOME_CHAR": "42 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.703077549908,
    "SOME_DATE": "11AUG1960",
    "SOME_DATETIME": " 01JAN1960:12:11:24",
    "SOME_TIME": " 0:00:38",
    "SOME_SHORTNUM": 86,
    "SOME_BESTNUM": 97
    },
    {
    "PRIMARY_KEY_FIELD": 1043,
    "SOME_CHAR": "43 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.0701107536769,
    "SOME_DATE": "29JAN1961",
    "SOME_DATETIME": " 01JAN1960:03:44:09",
    "SOME_TIME": " 0:00:03",
    "SOME_SHORTNUM": 25,
    "SOME_BESTNUM": 8
    },
    {
    "PRIMARY_KEY_FIELD": 1044,
    "SOME_CHAR": "44 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.642329292671,
    "SOME_DATE": "15JAN1962",
    "SOME_DATETIME": " 01JAN1960:00:57:07",
    "SOME_TIME": " 0:00:09",
    "SOME_SHORTNUM": 97,
    "SOME_BESTNUM": 37
    },
    {
    "PRIMARY_KEY_FIELD": 1045,
    "SOME_CHAR": "45 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.720644774251,
    "SOME_DATE": "14OCT1961",
    "SOME_DATETIME": " 01JAN1960:12:25:32",
    "SOME_TIME": " 0:00:07",
    "SOME_SHORTNUM": 58,
    "SOME_BESTNUM": 58
    },
    {
    "PRIMARY_KEY_FIELD": 1046,
    "SOME_CHAR": "46 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.0431997366451,
    "SOME_DATE": "12SEP1960",
    "SOME_DATETIME": " 01JAN1960:05:12:57",
    "SOME_TIME": " 0:01:35",
    "SOME_SHORTNUM": 17,
    "SOME_BESTNUM": 8
    },
    {
    "PRIMARY_KEY_FIELD": 1047,
    "SOME_CHAR": "47 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.370407136795,
    "SOME_DATE": "01JUL1960",
    "SOME_DATETIME": " 01JAN1960:02:44:37",
    "SOME_TIME": " 0:00:06",
    "SOME_SHORTNUM": 45,
    "SOME_BESTNUM": 26
    },
    {
    "PRIMARY_KEY_FIELD": 1048,
    "SOME_CHAR": "48 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.654417035009,
    "SOME_DATE": "04MAY1961",
    "SOME_DATETIME": " 01JAN1960:01:23:07",
    "SOME_TIME": " 0:01:38",
    "SOME_SHORTNUM": 41,
    "SOME_BESTNUM": 13
    },
    {
    "PRIMARY_KEY_FIELD": 1049,
    "SOME_CHAR": "49 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.130021256455,
    "SOME_DATE": "06JAN1961",
    "SOME_DATETIME": " 01JAN1960:05:27:29",
    "SOME_TIME": " 0:01:21",
    "SOME_SHORTNUM": 37,
    "SOME_BESTNUM": 66
    },
    {
    "PRIMARY_KEY_FIELD": 1050,
    "SOME_CHAR": "50 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.00584097253431,
    "SOME_DATE": "23JUL1960",
    "SOME_DATETIME": " 01JAN1960:00:04:24",
    "SOME_TIME": " 0:00:40",
    "SOME_SHORTNUM": 15,
    "SOME_BESTNUM": 32
    },
    {
    "PRIMARY_KEY_FIELD": 1051,
    "SOME_CHAR": "51 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.723938258702,
    "SOME_DATE": "09JUN1960",
    "SOME_DATETIME": " 01JAN1960:03:15:09",
    "SOME_TIME": " 0:00:04",
    "SOME_SHORTNUM": 42,
    "SOME_BESTNUM": 82
    },
    {
    "PRIMARY_KEY_FIELD": 1052,
    "SOME_CHAR": "52 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.83190037116,
    "SOME_DATE": "13AUG1960",
    "SOME_DATETIME": " 01JAN1960:07:38:35",
    "SOME_TIME": " 0:00:36",
    "SOME_SHORTNUM": 69,
    "SOME_BESTNUM": 81
    },
    {
    "PRIMARY_KEY_FIELD": 1053,
    "SOME_CHAR": "53 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.503082887504,
    "SOME_DATE": "22JUN1961",
    "SOME_DATETIME": " 01JAN1960:11:25:29",
    "SOME_TIME": " 0:00:53",
    "SOME_SHORTNUM": 39,
    "SOME_BESTNUM": 75
    },
    {
    "PRIMARY_KEY_FIELD": 1054,
    "SOME_CHAR": "54 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.714804551431,
    "SOME_DATE": "26AUG1960",
    "SOME_DATETIME": " 01JAN1960:10:10:09",
    "SOME_TIME": " 0:00:39",
    "SOME_SHORTNUM": 6,
    "SOME_BESTNUM": 4
    },
    {
    "PRIMARY_KEY_FIELD": 1055,
    "SOME_CHAR": "55 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.855794578724,
    "SOME_DATE": "19OCT1960",
    "SOME_DATETIME": " 01JAN1960:02:17:32",
    "SOME_TIME": " 0:00:08",
    "SOME_SHORTNUM": 93,
    "SOME_BESTNUM": 36
    },
    {
    "PRIMARY_KEY_FIELD": 1056,
    "SOME_CHAR": "56 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.970046330695,
    "SOME_DATE": "11JUL1962",
    "SOME_DATETIME": " 01JAN1960:11:18:41",
    "SOME_TIME": " 0:00:51",
    "SOME_SHORTNUM": 25,
    "SOME_BESTNUM": 35
    },
    {
    "PRIMARY_KEY_FIELD": 1057,
    "SOME_CHAR": "57 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.938039942616,
    "SOME_DATE": "26JUN1961",
    "SOME_DATETIME": " 01JAN1960:13:15:13",
    "SOME_TIME": " 0:00:52",
    "SOME_SHORTNUM": 57,
    "SOME_BESTNUM": 66
    },
    {
    "PRIMARY_KEY_FIELD": 1058,
    "SOME_CHAR": "58 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.848449948639,
    "SOME_DATE": "02JUN1960",
    "SOME_DATETIME": " 01JAN1960:01:14:51",
    "SOME_TIME": " 0:00:00",
    "SOME_SHORTNUM": 80,
    "SOME_BESTNUM": 58
    },
    {
    "PRIMARY_KEY_FIELD": 1059,
    "SOME_CHAR": "59 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.141570762797,
    "SOME_DATE": "28JUL1961",
    "SOME_DATETIME": " 01JAN1960:06:33:16",
    "SOME_TIME": " 0:00:58",
    "SOME_SHORTNUM": 11,
    "SOME_BESTNUM": 32
    },
    {
    "PRIMARY_KEY_FIELD": 1060,
    "SOME_CHAR": "60 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.282674512958,
    "SOME_DATE": "27MAR1962",
    "SOME_DATETIME": " 01JAN1960:00:25:37",
    "SOME_TIME": " 0:00:56",
    "SOME_SHORTNUM": 79,
    "SOME_BESTNUM": 58
    },
    {
    "PRIMARY_KEY_FIELD": 1061,
    "SOME_CHAR": "61 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.372728008019,
    "SOME_DATE": "04JAN1962",
    "SOME_DATETIME": " 01JAN1960:05:07:43",
    "SOME_TIME": " 0:01:00",
    "SOME_SHORTNUM": 86,
    "SOME_BESTNUM": 92
    },
    {
    "PRIMARY_KEY_FIELD": 1062,
    "SOME_CHAR": "62 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.951733731642,
    "SOME_DATE": "29AUG1961",
    "SOME_DATETIME": " 01JAN1960:02:40:05",
    "SOME_TIME": " 0:00:05",
    "SOME_SHORTNUM": 30,
    "SOME_BESTNUM": 93
    },
    {
    "PRIMARY_KEY_FIELD": 1063,
    "SOME_CHAR": "63 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.096749868289,
    "SOME_DATE": "17FEB1962",
    "SOME_DATETIME": " 01JAN1960:07:30:41",
    "SOME_TIME": " 0:00:29",
    "SOME_SHORTNUM": 90,
    "SOME_BESTNUM": 82
    },
    {
    "PRIMARY_KEY_FIELD": 1064,
    "SOME_CHAR": "64 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.054067135348,
    "SOME_DATE": "26MAY1961",
    "SOME_DATETIME": " 01JAN1960:13:13:43",
    "SOME_TIME": " 0:00:08",
    "SOME_SHORTNUM": 88,
    "SOME_BESTNUM": 45
    },
    {
    "PRIMARY_KEY_FIELD": 1065,
    "SOME_CHAR": "65 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.646163646433,
    "SOME_DATE": "27JAN1962",
    "SOME_DATETIME": " 01JAN1960:02:56:41",
    "SOME_TIME": " 0:00:19",
    "SOME_SHORTNUM": 41,
    "SOME_BESTNUM": 38
    },
    {
    "PRIMARY_KEY_FIELD": 1066,
    "SOME_CHAR": "66 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.905301198319,
    "SOME_DATE": "02OCT1960",
    "SOME_DATETIME": " 01JAN1960:03:35:49",
    "SOME_TIME": " 0:01:04",
    "SOME_SHORTNUM": 68,
    "SOME_BESTNUM": 39
    },
    {
    "PRIMARY_KEY_FIELD": 1067,
    "SOME_CHAR": "67 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.977525881015,
    "SOME_DATE": "19JUL1962",
    "SOME_DATETIME": " 01JAN1960:05:53:20",
    "SOME_TIME": " 0:00:28",
    "SOME_SHORTNUM": 28,
    "SOME_BESTNUM": 34
    },
    {
    "PRIMARY_KEY_FIELD": 1068,
    "SOME_CHAR": "68 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.216555316102,
    "SOME_DATE": "13MAY1960",
    "SOME_DATETIME": " 01JAN1960:01:44:02",
    "SOME_TIME": " 0:01:12",
    "SOME_SHORTNUM": 63,
    "SOME_BESTNUM": 23
    },
    {
    "PRIMARY_KEY_FIELD": 1069,
    "SOME_CHAR": "69 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.224835279502,
    "SOME_DATE": "09MAY1961",
    "SOME_DATETIME": " 01JAN1960:00:04:33",
    "SOME_TIME": " 0:00:09",
    "SOME_SHORTNUM": 26,
    "SOME_BESTNUM": 93
    },
    {
    "PRIMARY_KEY_FIELD": 1070,
    "SOME_CHAR": "70 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.138628336666,
    "SOME_DATE": "18MAY1962",
    "SOME_DATETIME": " 01JAN1960:03:32:00",
    "SOME_TIME": " 0:01:36",
    "SOME_SHORTNUM": 83,
    "SOME_BESTNUM": 89
    },
    {
    "PRIMARY_KEY_FIELD": 1071,
    "SOME_CHAR": "71 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.933733141485,
    "SOME_DATE": "16MAY1961",
    "SOME_DATETIME": " 01JAN1960:13:46:54",
    "SOME_TIME": " 0:00:47",
    "SOME_SHORTNUM": 27,
    "SOME_BESTNUM": 56
    },
    {
    "PRIMARY_KEY_FIELD": 1072,
    "SOME_CHAR": "72 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.0352235506453,
    "SOME_DATE": "06JUN1961",
    "SOME_DATETIME": " 01JAN1960:09:09:20",
    "SOME_TIME": " 0:01:16",
    "SOME_SHORTNUM": 7,
    "SOME_BESTNUM": 27
    },
    {
    "PRIMARY_KEY_FIELD": 1073,
    "SOME_CHAR": "73 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.320666269549,
    "SOME_DATE": "13MAR1960",
    "SOME_DATETIME": " 01JAN1960:10:38:11",
    "SOME_TIME": " 0:01:08",
    "SOME_SHORTNUM": 3,
    "SOME_BESTNUM": 50
    },
    {
    "PRIMARY_KEY_FIELD": 1074,
    "SOME_CHAR": "74 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.461086170497,
    "SOME_DATE": "31AUG1961",
    "SOME_DATETIME": " 01JAN1960:09:35:41",
    "SOME_TIME": " 0:01:08",
    "SOME_SHORTNUM": 54,
    "SOME_BESTNUM": 68
    },
    {
    "PRIMARY_KEY_FIELD": 1075,
    "SOME_CHAR": "75 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.452774562152,
    "SOME_DATE": "16JAN1962",
    "SOME_DATETIME": " 01JAN1960:06:49:27",
    "SOME_TIME": " 0:00:45",
    "SOME_SHORTNUM": 96,
    "SOME_BESTNUM": 63
    },
    {
    "PRIMARY_KEY_FIELD": 1076,
    "SOME_CHAR": "76 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.358124405778,
    "SOME_DATE": "16MAY1960",
    "SOME_DATETIME": " 01JAN1960:00:56:40",
    "SOME_TIME": " 0:01:13",
    "SOME_SHORTNUM": 72,
    "SOME_BESTNUM": 24
    },
    {
    "PRIMARY_KEY_FIELD": 1077,
    "SOME_CHAR": "77 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.893992133389,
    "SOME_DATE": "21JAN1961",
    "SOME_DATETIME": " 01JAN1960:09:16:31",
    "SOME_TIME": " 0:01:15",
    "SOME_SHORTNUM": 88,
    "SOME_BESTNUM": 69
    },
    {
    "PRIMARY_KEY_FIELD": 1078,
    "SOME_CHAR": "78 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.244572706634,
    "SOME_DATE": "22DEC1960",
    "SOME_DATETIME": " 01JAN1960:03:11:14",
    "SOME_TIME": " 0:01:37",
    "SOME_SHORTNUM": 88,
    "SOME_BESTNUM": 32
    },
    {
    "PRIMARY_KEY_FIELD": 1079,
    "SOME_CHAR": "79 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.968302946523,
    "SOME_DATE": "14AUG1961",
    "SOME_DATETIME": " 01JAN1960:04:45:43",
    "SOME_TIME": " 0:01:09",
    "SOME_SHORTNUM": 51,
    "SOME_BESTNUM": 60
    },
    {
    "PRIMARY_KEY_FIELD": 1080,
    "SOME_CHAR": "80 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.130354136755,
    "SOME_DATE": "28FEB1962",
    "SOME_DATETIME": " 01JAN1960:02:14:50",
    "SOME_TIME": " 0:00:21",
    "SOME_SHORTNUM": 79,
    "SOME_BESTNUM": 87
    },
    {
    "PRIMARY_KEY_FIELD": 1081,
    "SOME_CHAR": "81 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.765697965289,
    "SOME_DATE": "03AUG1961",
    "SOME_DATETIME": " 01JAN1960:06:49:50",
    "SOME_TIME": " 0:01:31",
    "SOME_SHORTNUM": 58,
    "SOME_BESTNUM": 30
    },
    {
    "PRIMARY_KEY_FIELD": 1082,
    "SOME_CHAR": "82 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.185562967409,
    "SOME_DATE": "16DEC1960",
    "SOME_DATETIME": " 01JAN1960:06:27:21",
    "SOME_TIME": " 0:00:33",
    "SOME_SHORTNUM": 1,
    "SOME_BESTNUM": 72
    },
    {
    "PRIMARY_KEY_FIELD": 1083,
    "SOME_CHAR": "83 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.478217864166,
    "SOME_DATE": "16APR1961",
    "SOME_DATETIME": " 01JAN1960:08:05:23",
    "SOME_TIME": " 0:01:10",
    "SOME_SHORTNUM": 0,
    "SOME_BESTNUM": 1
    },
    {
    "PRIMARY_KEY_FIELD": 1084,
    "SOME_CHAR": "84 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.167027213223,
    "SOME_DATE": "21JUN1962",
    "SOME_DATETIME": " 01JAN1960:13:43:20",
    "SOME_TIME": " 0:00:27",
    "SOME_SHORTNUM": 53,
    "SOME_BESTNUM": 6
    },
    {
    "PRIMARY_KEY_FIELD": 1085,
    "SOME_CHAR": "85 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.606824918933,
    "SOME_DATE": "21MAY1960",
    "SOME_DATETIME": " 01JAN1960:11:05:11",
    "SOME_TIME": " 0:00:08",
    "SOME_SHORTNUM": 17,
    "SOME_BESTNUM": 68
    },
    {
    "PRIMARY_KEY_FIELD": 1086,
    "SOME_CHAR": "86 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.0936049917217,
    "SOME_DATE": "20JUL1962",
    "SOME_DATETIME": " 01JAN1960:07:16:09",
    "SOME_TIME": " 0:00:46",
    "SOME_SHORTNUM": 73,
    "SOME_BESTNUM": 37
    },
    {
    "PRIMARY_KEY_FIELD": 1087,
    "SOME_CHAR": "87 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.653824917811,
    "SOME_DATE": "24APR1960",
    "SOME_DATETIME": " 01JAN1960:02:06:54",
    "SOME_TIME": " 0:00:59",
    "SOME_SHORTNUM": 95,
    "SOME_BESTNUM": 32
    },
    {
    "PRIMARY_KEY_FIELD": 1088,
    "SOME_CHAR": "88 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.884615856169,
    "SOME_DATE": "19NOV1961",
    "SOME_DATETIME": " 01JAN1960:05:35:27",
    "SOME_TIME": " 0:01:01",
    "SOME_SHORTNUM": 87,
    "SOME_BESTNUM": 30
    },
    {
    "PRIMARY_KEY_FIELD": 1089,
    "SOME_CHAR": "89 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.157820831592,
    "SOME_DATE": "03MAR1961",
    "SOME_DATETIME": " 01JAN1960:09:02:02",
    "SOME_TIME": " 0:00:23",
    "SOME_SHORTNUM": 60,
    "SOME_BESTNUM": 53
    },
    {
    "PRIMARY_KEY_FIELD": 1090,
    "SOME_CHAR": "90 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.422575375262,
    "SOME_DATE": "19MAR1960",
    "SOME_DATETIME": " 01JAN1960:12:14:04",
    "SOME_TIME": " 0:01:00",
    "SOME_SHORTNUM": 57,
    "SOME_BESTNUM": 64
    },
    {
    "PRIMARY_KEY_FIELD": 1091,
    "SOME_CHAR": "91 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.659894335391,
    "SOME_DATE": "17SEP1961",
    "SOME_DATETIME": " 01JAN1960:03:03:13",
    "SOME_TIME": " 0:01:00",
    "SOME_SHORTNUM": 41,
    "SOME_BESTNUM": 28
    },
    {
    "PRIMARY_KEY_FIELD": 1092,
    "SOME_CHAR": "92 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.629350168924,
    "SOME_DATE": "18OCT1961",
    "SOME_DATETIME": " 01JAN1960:00:21:13",
    "SOME_TIME": " 0:01:11",
    "SOME_SHORTNUM": 64,
    "SOME_BESTNUM": 7
    },
    {
    "PRIMARY_KEY_FIELD": 1093,
    "SOME_CHAR": "93 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.437884498591,
    "SOME_DATE": "24JUN1961",
    "SOME_DATETIME": " 01JAN1960:10:20:39",
    "SOME_TIME": " 0:00:27",
    "SOME_SHORTNUM": 30,
    "SOME_BESTNUM": 78
    },
    {
    "PRIMARY_KEY_FIELD": 1094,
    "SOME_CHAR": "94 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.983858496875,
    "SOME_DATE": "25MAY1962",
    "SOME_DATETIME": " 01JAN1960:02:59:06",
    "SOME_TIME": " 0:00:59",
    "SOME_SHORTNUM": 48,
    "SOME_BESTNUM": 98
    },
    {
    "PRIMARY_KEY_FIELD": 1095,
    "SOME_CHAR": "95 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.0892523769705,
    "SOME_DATE": "16JUN1961",
    "SOME_DATETIME": " 01JAN1960:04:54:20",
    "SOME_TIME": " 0:00:10",
    "SOME_SHORTNUM": 75,
    "SOME_BESTNUM": 33
    },
    {
    "PRIMARY_KEY_FIELD": 1096,
    "SOME_CHAR": "96 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.457820515362,
    "SOME_DATE": "20JAN1960",
    "SOME_DATETIME": " 01JAN1960:10:36:00",
    "SOME_TIME": " 0:00:41",
    "SOME_SHORTNUM": 14,
    "SOME_BESTNUM": 17
    },
    {
    "PRIMARY_KEY_FIELD": 1097,
    "SOME_CHAR": "97 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.586327158653,
    "SOME_DATE": "20APR1962",
    "SOME_DATETIME": " 01JAN1960:11:14:11",
    "SOME_TIME": " 0:01:28",
    "SOME_SHORTNUM": 66,
    "SOME_BESTNUM": 84
    },
    {
    "PRIMARY_KEY_FIELD": 1098,
    "SOME_CHAR": "98 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.299423205806,
    "SOME_DATE": "04JUL1960",
    "SOME_DATETIME": " 01JAN1960:08:15:41",
    "SOME_TIME": " 0:01:28",
    "SOME_SHORTNUM": 99,
    "SOME_BESTNUM": 85
    },
    {
    "PRIMARY_KEY_FIELD": 1099,
    "SOME_CHAR": "99 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.0981378052841,
    "SOME_DATE": "05FEB1960",
    "SOME_DATETIME": " 01JAN1960:11:10:11",
    "SOME_TIME": " 0:00:43",
    "SOME_SHORTNUM": 23,
    "SOME_BESTNUM": 65
    },
    {
    "PRIMARY_KEY_FIELD": 10100,
    "SOME_CHAR": "100 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.982972265213,
    "SOME_DATE": "01FEB1960",
    "SOME_DATETIME": " 01JAN1960:05:45:06",
    "SOME_TIME": " 0:01:16",
    "SOME_SHORTNUM": 28,
    "SOME_BESTNUM": 44
    },
    {
    "PRIMARY_KEY_FIELD": 10101,
    "SOME_CHAR": "101 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.454079491298,
    "SOME_DATE": "03AUG1962",
    "SOME_DATETIME": " 01JAN1960:09:27:03",
    "SOME_TIME": " 0:01:10",
    "SOME_SHORTNUM": 42,
    "SOME_BESTNUM": 44
    },
    {
    "PRIMARY_KEY_FIELD": 10102,
    "SOME_CHAR": "102 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.845217436946,
    "SOME_DATE": "02OCT1960",
    "SOME_DATETIME": " 01JAN1960:03:08:47",
    "SOME_TIME": " 0:00:23",
    "SOME_SHORTNUM": 10,
    "SOME_BESTNUM": 14
    },
    {
    "PRIMARY_KEY_FIELD": 10103,
    "SOME_CHAR": "103 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.590491960566,
    "SOME_DATE": "30JUL1960",
    "SOME_DATETIME": " 01JAN1960:13:09:58",
    "SOME_TIME": " 0:00:10",
    "SOME_SHORTNUM": 68,
    "SOME_BESTNUM": 53
    },
    {
    "PRIMARY_KEY_FIELD": 10104,
    "SOME_CHAR": "104 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.708338867737,
    "SOME_DATE": "24JUL1960",
    "SOME_DATETIME": " 01JAN1960:05:49:50",
    "SOME_TIME": " 0:01:29",
    "SOME_SHORTNUM": 84,
    "SOME_BESTNUM": 33
    },
    {
    "PRIMARY_KEY_FIELD": 10105,
    "SOME_CHAR": "105 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.318355025872,
    "SOME_DATE": "14SEP1961",
    "SOME_DATETIME": " 01JAN1960:02:28:34",
    "SOME_TIME": " 0:00:51",
    "SOME_SHORTNUM": 69,
    "SOME_BESTNUM": 97
    },
    {
    "PRIMARY_KEY_FIELD": 10106,
    "SOME_CHAR": "106 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.972975597239,
    "SOME_DATE": "08AUG1961",
    "SOME_DATETIME": " 01JAN1960:04:30:41",
    "SOME_TIME": " 0:00:10",
    "SOME_SHORTNUM": 91,
    "SOME_BESTNUM": 29
    },
    {
    "PRIMARY_KEY_FIELD": 10107,
    "SOME_CHAR": "107 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.509077321509,
    "SOME_DATE": "23SEP1960",
    "SOME_DATETIME": " 01JAN1960:05:20:31",
    "SOME_TIME": " 0:00:43",
    "SOME_SHORTNUM": 92,
    "SOME_BESTNUM": 73
    },
    {
    "PRIMARY_KEY_FIELD": 10108,
    "SOME_CHAR": "108 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.605017692598,
    "SOME_DATE": "18MAR1961",
    "SOME_DATETIME": " 01JAN1960:02:16:28",
    "SOME_TIME": " 0:00:35",
    "SOME_SHORTNUM": 88,
    "SOME_BESTNUM": 21
    },
    {
    "PRIMARY_KEY_FIELD": 10109,
    "SOME_CHAR": "109 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.361268181522,
    "SOME_DATE": "02JAN1962",
    "SOME_DATETIME": " 01JAN1960:11:03:06",
    "SOME_TIME": " 0:00:39",
    "SOME_SHORTNUM": 67,
    "SOME_BESTNUM": 1
    },
    {
    "PRIMARY_KEY_FIELD": 10110,
    "SOME_CHAR": "110 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.92257953711,
    "SOME_DATE": "30JUN1960",
    "SOME_DATETIME": " 01JAN1960:07:37:19",
    "SOME_TIME": " 0:00:53",
    "SOME_SHORTNUM": 45,
    "SOME_BESTNUM": 96
    },
    {
    "PRIMARY_KEY_FIELD": 10111,
    "SOME_CHAR": "111 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.576207744226,
    "SOME_DATE": "02JAN1960",
    "SOME_DATETIME": " 01JAN1960:02:58:52",
    "SOME_TIME": " 0:00:13",
    "SOME_SHORTNUM": 85,
    "SOME_BESTNUM": 94
    },
    {
    "PRIMARY_KEY_FIELD": 10112,
    "SOME_CHAR": "112 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.722135797945,
    "SOME_DATE": "03JAN1961",
    "SOME_DATETIME": " 01JAN1960:06:46:21",
    "SOME_TIME": " 0:00:11",
    "SOME_SHORTNUM": 81,
    "SOME_BESTNUM": 4
    },
    {
    "PRIMARY_KEY_FIELD": 10113,
    "SOME_CHAR": "113 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.683960923312,
    "SOME_DATE": "26MAY1962",
    "SOME_DATETIME": " 01JAN1960:02:09:46",
    "SOME_TIME": " 0:01:28",
    "SOME_SHORTNUM": 88,
    "SOME_BESTNUM": 35
    },
    {
    "PRIMARY_KEY_FIELD": 10114,
    "SOME_CHAR": "114 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.0627542203584,
    "SOME_DATE": "30AUG1960",
    "SOME_DATETIME": " 01JAN1960:10:55:01",
    "SOME_TIME": " 0:01:05",
    "SOME_SHORTNUM": 37,
    "SOME_BESTNUM": 30
    },
    {
    "PRIMARY_KEY_FIELD": 10115,
    "SOME_CHAR": "115 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.146239363191,
    "SOME_DATE": "02FEB1962",
    "SOME_DATETIME": " 01JAN1960:08:38:32",
    "SOME_TIME": " 0:00:32",
    "SOME_SHORTNUM": 26,
    "SOME_BESTNUM": 92
    },
    {
    "PRIMARY_KEY_FIELD": 10116,
    "SOME_CHAR": "116 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.88700955449,
    "SOME_DATE": "06APR1961",
    "SOME_DATETIME": " 01JAN1960:08:19:07",
    "SOME_TIME": " 0:01:07",
    "SOME_SHORTNUM": 93,
    "SOME_BESTNUM": 100
    },
    {
    "PRIMARY_KEY_FIELD": 10117,
    "SOME_CHAR": "117 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.586251998128,
    "SOME_DATE": "11FEB1962",
    "SOME_DATETIME": " 01JAN1960:09:18:09",
    "SOME_TIME": " 0:01:24",
    "SOME_SHORTNUM": 17,
    "SOME_BESTNUM": 65
    },
    {
    "PRIMARY_KEY_FIELD": 10118,
    "SOME_CHAR": "118 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.119509128444,
    "SOME_DATE": "29MAR1960",
    "SOME_DATETIME": " 01JAN1960:05:00:30",
    "SOME_TIME": " 0:01:23",
    "SOME_SHORTNUM": 74,
    "SOME_BESTNUM": 91
    },
    {
    "PRIMARY_KEY_FIELD": 10119,
    "SOME_CHAR": "119 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.25274958287,
    "SOME_DATE": "14MAR1960",
    "SOME_DATETIME": " 01JAN1960:11:15:20",
    "SOME_TIME": " 0:00:03",
    "SOME_SHORTNUM": 35,
    "SOME_BESTNUM": 48
    },
    {
    "PRIMARY_KEY_FIELD": 10120,
    "SOME_CHAR": "120 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.200962597132,
    "SOME_DATE": "18NOV1960",
    "SOME_DATETIME": " 01JAN1960:10:17:59",
    "SOME_TIME": " 0:00:25",
    "SOME_SHORTNUM": 72,
    "SOME_BESTNUM": 46
    },
    {
    "PRIMARY_KEY_FIELD": 10121,
    "SOME_CHAR": "121 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.517157181407,
    "SOME_DATE": "27NOV1961",
    "SOME_DATETIME": " 01JAN1960:09:53:43",
    "SOME_TIME": " 0:01:10",
    "SOME_SHORTNUM": 2,
    "SOME_BESTNUM": 29
    },
    {
    "PRIMARY_KEY_FIELD": 10122,
    "SOME_CHAR": "122 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.625352653966,
    "SOME_DATE": "15DEC1960",
    "SOME_DATETIME": " 01JAN1960:05:14:23",
    "SOME_TIME": " 0:01:17",
    "SOME_SHORTNUM": 55,
    "SOME_BESTNUM": 42
    },
    {
    "PRIMARY_KEY_FIELD": 10123,
    "SOME_CHAR": "123 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.0397578571177,
    "SOME_DATE": "08SEP1961",
    "SOME_DATETIME": " 01JAN1960:00:37:02",
    "SOME_TIME": " 0:00:57",
    "SOME_SHORTNUM": 21,
    "SOME_BESTNUM": 21
    },
    {
    "PRIMARY_KEY_FIELD": 10124,
    "SOME_CHAR": "124 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.152996921983,
    "SOME_DATE": "20FEB1962",
    "SOME_DATETIME": " 01JAN1960:08:46:06",
    "SOME_TIME": " 0:00:49",
    "SOME_SHORTNUM": 77,
    "SOME_BESTNUM": 36
    },
    {
    "PRIMARY_KEY_FIELD": 10125,
    "SOME_CHAR": "125 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.55944163518,
    "SOME_DATE": "28APR1962",
    "SOME_DATETIME": " 01JAN1960:05:02:05",
    "SOME_TIME": " 0:01:38",
    "SOME_SHORTNUM": 89,
    "SOME_BESTNUM": 81
    },
    {
    "PRIMARY_KEY_FIELD": 10126,
    "SOME_CHAR": "126 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.0545679247261,
    "SOME_DATE": "12APR1960",
    "SOME_DATETIME": " 01JAN1960:03:28:48",
    "SOME_TIME": " 0:01:33",
    "SOME_SHORTNUM": 78,
    "SOME_BESTNUM": 97
    },
    {
    "PRIMARY_KEY_FIELD": 10127,
    "SOME_CHAR": "127 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.371348928367,
    "SOME_DATE": "12OCT1961",
    "SOME_DATETIME": " 01JAN1960:02:16:07",
    "SOME_TIME": " 0:00:52",
    "SOME_SHORTNUM": 54,
    "SOME_BESTNUM": 88
    },
    {
    "PRIMARY_KEY_FIELD": 10128,
    "SOME_CHAR": "128 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.647298235282,
    "SOME_DATE": "03APR1960",
    "SOME_DATETIME": " 01JAN1960:00:32:14",
    "SOME_TIME": " 0:00:51",
    "SOME_SHORTNUM": 6,
    "SOME_BESTNUM": 19
    },
    {
    "PRIMARY_KEY_FIELD": 10129,
    "SOME_CHAR": "129 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.830022884919,
    "SOME_DATE": "05JAN1960",
    "SOME_DATETIME": " 01JAN1960:13:45:11",
    "SOME_TIME": " 0:00:33",
    "SOME_SHORTNUM": 81,
    "SOME_BESTNUM": 10
    },
    {
    "PRIMARY_KEY_FIELD": 10130,
    "SOME_CHAR": "130 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.265660790385,
    "SOME_DATE": "10JUL1961",
    "SOME_DATETIME": " 01JAN1960:05:20:20",
    "SOME_TIME": " 0:00:44",
    "SOME_SHORTNUM": 64,
    "SOME_BESTNUM": 96
    },
    {
    "PRIMARY_KEY_FIELD": 10131,
    "SOME_CHAR": "131 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.351538887877,
    "SOME_DATE": "10APR1961",
    "SOME_DATETIME": " 01JAN1960:02:34:28",
    "SOME_TIME": " 0:01:05",
    "SOME_SHORTNUM": 33,
    "SOME_BESTNUM": 47
    },
    {
    "PRIMARY_KEY_FIELD": 10132,
    "SOME_CHAR": "132 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.584812691708,
    "SOME_DATE": "04JAN1961",
    "SOME_DATETIME": " 01JAN1960:00:58:07",
    "SOME_TIME": " 0:00:09",
    "SOME_SHORTNUM": 27,
    "SOME_BESTNUM": 23
    },
    {
    "PRIMARY_KEY_FIELD": 10133,
    "SOME_CHAR": "133 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.359766102563,
    "SOME_DATE": "01APR1962",
    "SOME_DATETIME": " 01JAN1960:04:32:07",
    "SOME_TIME": " 0:00:12",
    "SOME_SHORTNUM": 6,
    "SOME_BESTNUM": 79
    },
    {
    "PRIMARY_KEY_FIELD": 10134,
    "SOME_CHAR": "134 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.668257887321,
    "SOME_DATE": "23NOV1961",
    "SOME_DATETIME": " 01JAN1960:12:09:29",
    "SOME_TIME": " 0:00:43",
    "SOME_SHORTNUM": 39,
    "SOME_BESTNUM": 24
    },
    {
    "PRIMARY_KEY_FIELD": 10135,
    "SOME_CHAR": "135 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.673810465575,
    "SOME_DATE": "21MAY1961",
    "SOME_DATETIME": " 01JAN1960:08:17:36",
    "SOME_TIME": " 0:01:25",
    "SOME_SHORTNUM": 25,
    "SOME_BESTNUM": 81
    },
    {
    "PRIMARY_KEY_FIELD": 10136,
    "SOME_CHAR": "136 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.610945592919,
    "SOME_DATE": "20DEC1961",
    "SOME_DATETIME": " 01JAN1960:06:15:06",
    "SOME_TIME": " 0:00:35",
    "SOME_SHORTNUM": 54,
    "SOME_BESTNUM": 62
    },
    {
    "PRIMARY_KEY_FIELD": 10137,
    "SOME_CHAR": "137 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.903748751573,
    "SOME_DATE": "13MAR1960",
    "SOME_DATETIME": " 01JAN1960:01:34:57",
    "SOME_TIME": " 0:01:18",
    "SOME_SHORTNUM": 47,
    "SOME_BESTNUM": 95
    },
    {
    "PRIMARY_KEY_FIELD": 10138,
    "SOME_CHAR": "138 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.807945651844,
    "SOME_DATE": "04OCT1961",
    "SOME_DATETIME": " 01JAN1960:11:45:56",
    "SOME_TIME": " 0:00:27",
    "SOME_SHORTNUM": 12,
    "SOME_BESTNUM": 85
    },
    {
    "PRIMARY_KEY_FIELD": 10139,
    "SOME_CHAR": "139 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.383073866546,
    "SOME_DATE": "06APR1960",
    "SOME_DATETIME": " 01JAN1960:07:42:17",
    "SOME_TIME": " 0:01:31",
    "SOME_SHORTNUM": 35,
    "SOME_BESTNUM": 33
    },
    {
    "PRIMARY_KEY_FIELD": 10140,
    "SOME_CHAR": "140 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.56504530905,
    "SOME_DATE": "20FEB1960",
    "SOME_DATETIME": " 01JAN1960:00:07:16",
    "SOME_TIME": " 0:01:32",
    "SOME_SHORTNUM": 79,
    "SOME_BESTNUM": 7
    },
    {
    "PRIMARY_KEY_FIELD": 10141,
    "SOME_CHAR": "141 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.887459293887,
    "SOME_DATE": "01MAR1962",
    "SOME_DATETIME": " 01JAN1960:00:34:22",
    "SOME_TIME": " 0:00:19",
    "SOME_SHORTNUM": 22,
    "SOME_BESTNUM": 14
    },
    {
    "PRIMARY_KEY_FIELD": 10142,
    "SOME_CHAR": "142 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.404861313945,
    "SOME_DATE": "05FEB1961",
    "SOME_DATETIME": " 01JAN1960:10:57:41",
    "SOME_TIME": " 0:00:31",
    "SOME_SHORTNUM": 67,
    "SOME_BESTNUM": 8
    },
    {
    "PRIMARY_KEY_FIELD": 10143,
    "SOME_CHAR": "143 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.460108042443,
    "SOME_DATE": "21MAY1960",
    "SOME_DATETIME": " 01JAN1960:10:35:18",
    "SOME_TIME": " 0:00:13",
    "SOME_SHORTNUM": 5,
    "SOME_BESTNUM": 56
    },
    {
    "PRIMARY_KEY_FIELD": 10144,
    "SOME_CHAR": "144 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.809737510891,
    "SOME_DATE": "26JAN1961",
    "SOME_DATETIME": " 01JAN1960:08:06:34",
    "SOME_TIME": " 0:00:40",
    "SOME_SHORTNUM": 99,
    "SOME_BESTNUM": 51
    },
    {
    "PRIMARY_KEY_FIELD": 10145,
    "SOME_CHAR": "145 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.993591509756,
    "SOME_DATE": "15MAR1961",
    "SOME_DATETIME": " 01JAN1960:12:49:53",
    "SOME_TIME": " 0:01:12",
    "SOME_SHORTNUM": 64,
    "SOME_BESTNUM": 36
    },
    {
    "PRIMARY_KEY_FIELD": 10146,
    "SOME_CHAR": "146 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.0452116918961,
    "SOME_DATE": "28APR1960",
    "SOME_DATETIME": " 01JAN1960:06:18:21",
    "SOME_TIME": " 0:00:24",
    "SOME_SHORTNUM": 60,
    "SOME_BESTNUM": 96
    },
    {
    "PRIMARY_KEY_FIELD": 10147,
    "SOME_CHAR": "147 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.828117129779,
    "SOME_DATE": "17SEP1960",
    "SOME_DATETIME": " 01JAN1960:03:32:05",
    "SOME_TIME": " 0:00:17",
    "SOME_SHORTNUM": 62,
    "SOME_BESTNUM": 66
    },
    {
    "PRIMARY_KEY_FIELD": 10148,
    "SOME_CHAR": "148 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.108167296326,
    "SOME_DATE": "27JUL1962",
    "SOME_DATETIME": " 01JAN1960:07:27:20",
    "SOME_TIME": " 0:00:50",
    "SOME_SHORTNUM": 84,
    "SOME_BESTNUM": 81
    },
    {
    "PRIMARY_KEY_FIELD": 10149,
    "SOME_CHAR": "149 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.380526406868,
    "SOME_DATE": "14NOV1961",
    "SOME_DATETIME": " 01JAN1960:00:53:41",
    "SOME_TIME": " 0:01:26",
    "SOME_SHORTNUM": 85,
    "SOME_BESTNUM": 46
    },
    {
    "PRIMARY_KEY_FIELD": 10150,
    "SOME_CHAR": "150 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.755726019738,
    "SOME_DATE": "09SEP1962",
    "SOME_DATETIME": " 01JAN1960:05:18:45",
    "SOME_TIME": " 0:00:32",
    "SOME_SHORTNUM": 95,
    "SOME_BESTNUM": 95
    },
    {
    "PRIMARY_KEY_FIELD": 10151,
    "SOME_CHAR": "151 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.144336483043,
    "SOME_DATE": "05SEP1962",
    "SOME_DATETIME": " 01JAN1960:02:58:32",
    "SOME_TIME": " 0:01:07",
    "SOME_SHORTNUM": 60,
    "SOME_BESTNUM": 46
    },
    {
    "PRIMARY_KEY_FIELD": 10152,
    "SOME_CHAR": "152 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.0478734295107,
    "SOME_DATE": "14JUL1960",
    "SOME_DATETIME": " 01JAN1960:05:46:43",
    "SOME_TIME": " 0:00:16",
    "SOME_SHORTNUM": 11,
    "SOME_BESTNUM": 31
    },
    {
    "PRIMARY_KEY_FIELD": 10153,
    "SOME_CHAR": "153 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.99183906661,
    "SOME_DATE": "26APR1962",
    "SOME_DATETIME": " 01JAN1960:04:48:41",
    "SOME_TIME": " 0:01:12",
    "SOME_SHORTNUM": 56,
    "SOME_BESTNUM": 75
    },
    {
    "PRIMARY_KEY_FIELD": 10154,
    "SOME_CHAR": "154 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.915963809898,
    "SOME_DATE": "04SEP1960",
    "SOME_DATETIME": " 01JAN1960:01:48:59",
    "SOME_TIME": " 0:00:24",
    "SOME_SHORTNUM": 83,
    "SOME_BESTNUM": 15
    },
    {
    "PRIMARY_KEY_FIELD": 10155,
    "SOME_CHAR": "155 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.368240488865,
    "SOME_DATE": "24JAN1962",
    "SOME_DATETIME": " 01JAN1960:08:59:46",
    "SOME_TIME": " 0:00:14",
    "SOME_SHORTNUM": 80,
    "SOME_BESTNUM": 100
    },
    {
    "PRIMARY_KEY_FIELD": 10156,
    "SOME_CHAR": "156 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.261119415174,
    "SOME_DATE": "31DEC1961",
    "SOME_DATETIME": " 01JAN1960:00:43:42",
    "SOME_TIME": " 0:00:30",
    "SOME_SHORTNUM": 83,
    "SOME_BESTNUM": 10
    },
    {
    "PRIMARY_KEY_FIELD": 10157,
    "SOME_CHAR": "157 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.889038999513,
    "SOME_DATE": "28NOV1960",
    "SOME_DATETIME": " 01JAN1960:03:19:35",
    "SOME_TIME": " 0:01:36",
    "SOME_SHORTNUM": 11,
    "SOME_BESTNUM": 35
    },
    {
    "PRIMARY_KEY_FIELD": 10158,
    "SOME_CHAR": "158 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.131941327887,
    "SOME_DATE": "27AUG1961",
    "SOME_DATETIME": " 01JAN1960:07:26:49",
    "SOME_TIME": " 0:00:16",
    "SOME_SHORTNUM": 51,
    "SOME_BESTNUM": 85
    },
    {
    "PRIMARY_KEY_FIELD": 10159,
    "SOME_CHAR": "159 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.626175243233,
    "SOME_DATE": "23JUN1960",
    "SOME_DATETIME": " 01JAN1960:02:32:40",
    "SOME_TIME": " 0:00:21",
    "SOME_SHORTNUM": 48,
    "SOME_BESTNUM": 61
    },
    {
    "PRIMARY_KEY_FIELD": 10160,
    "SOME_CHAR": "160 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.570262782076,
    "SOME_DATE": "27NOV1961",
    "SOME_DATETIME": " 01JAN1960:12:11:27",
    "SOME_TIME": " 0:00:57",
    "SOME_SHORTNUM": 83,
    "SOME_BESTNUM": 1
    },
    {
    "PRIMARY_KEY_FIELD": 10161,
    "SOME_CHAR": "161 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.85756031324,
    "SOME_DATE": "28SEP1960",
    "SOME_DATETIME": " 01JAN1960:13:40:34",
    "SOME_TIME": " 0:00:27",
    "SOME_SHORTNUM": 2,
    "SOME_BESTNUM": 27
    },
    {
    "PRIMARY_KEY_FIELD": 10162,
    "SOME_CHAR": "162 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.182688185099,
    "SOME_DATE": "17FEB1960",
    "SOME_DATETIME": " 01JAN1960:02:58:44",
    "SOME_TIME": " 0:01:18",
    "SOME_SHORTNUM": 2,
    "SOME_BESTNUM": 97
    },
    {
    "PRIMARY_KEY_FIELD": 10163,
    "SOME_CHAR": "163 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.234210872666,
    "SOME_DATE": "27APR1961",
    "SOME_DATETIME": " 01JAN1960:02:55:22",
    "SOME_TIME": " 0:01:28",
    "SOME_SHORTNUM": 20,
    "SOME_BESTNUM": 48
    },
    {
    "PRIMARY_KEY_FIELD": 10164,
    "SOME_CHAR": "164 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.356953425965,
    "SOME_DATE": "10JUN1960",
    "SOME_DATETIME": " 01JAN1960:10:48:35",
    "SOME_TIME": " 0:00:54",
    "SOME_SHORTNUM": 32,
    "SOME_BESTNUM": 10
    },
    {
    "PRIMARY_KEY_FIELD": 10165,
    "SOME_CHAR": "165 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.913664674812,
    "SOME_DATE": "13JAN1961",
    "SOME_DATETIME": " 01JAN1960:10:33:26",
    "SOME_TIME": " 0:01:10",
    "SOME_SHORTNUM": 63,
    "SOME_BESTNUM": 6
    },
    {
    "PRIMARY_KEY_FIELD": 10166,
    "SOME_CHAR": "166 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.999055517837,
    "SOME_DATE": "29MAR1962",
    "SOME_DATETIME": " 01JAN1960:11:58:00",
    "SOME_TIME": " 0:00:35",
    "SOME_SHORTNUM": 10,
    "SOME_BESTNUM": 36
    },
    {
    "PRIMARY_KEY_FIELD": 10167,
    "SOME_CHAR": "167 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.522591932454,
    "SOME_DATE": "03MAR1960",
    "SOME_DATETIME": " 01JAN1960:13:08:34",
    "SOME_TIME": " 0:01:17",
    "SOME_SHORTNUM": 73,
    "SOME_BESTNUM": 92
    },
    {
    "PRIMARY_KEY_FIELD": 10168,
    "SOME_CHAR": "168 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.376125323761,
    "SOME_DATE": "31MAR1961",
    "SOME_DATETIME": " 01JAN1960:02:55:13",
    "SOME_TIME": " 0:01:19",
    "SOME_SHORTNUM": 37,
    "SOME_BESTNUM": 41
    },
    {
    "PRIMARY_KEY_FIELD": 10169,
    "SOME_CHAR": "169 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.326772676467,
    "SOME_DATE": "19JUN1962",
    "SOME_DATETIME": " 01JAN1960:11:57:02",
    "SOME_TIME": " 0:00:33",
    "SOME_SHORTNUM": 56,
    "SOME_BESTNUM": 4
    },
    {
    "PRIMARY_KEY_FIELD": 10170,
    "SOME_CHAR": "170 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.782820686597,
    "SOME_DATE": "07AUG1961",
    "SOME_DATETIME": " 01JAN1960:09:55:03",
    "SOME_TIME": " 0:00:11",
    "SOME_SHORTNUM": 96,
    "SOME_BESTNUM": 19
    },
    {
    "PRIMARY_KEY_FIELD": 10171,
    "SOME_CHAR": "171 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.863840596221,
    "SOME_DATE": "18JAN1961",
    "SOME_DATETIME": " 01JAN1960:04:31:08",
    "SOME_TIME": " 0:01:12",
    "SOME_SHORTNUM": 97,
    "SOME_BESTNUM": 52
    },
    {
    "PRIMARY_KEY_FIELD": 10172,
    "SOME_CHAR": "172 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.789710712987,
    "SOME_DATE": "01OCT1960",
    "SOME_DATETIME": " 01JAN1960:11:06:53",
    "SOME_TIME": " 0:00:11",
    "SOME_SHORTNUM": 12,
    "SOME_BESTNUM": 77
    },
    {
    "PRIMARY_KEY_FIELD": 10173,
    "SOME_CHAR": "173 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.802511338518,
    "SOME_DATE": "21MAY1960",
    "SOME_DATETIME": " 01JAN1960:06:53:54",
    "SOME_TIME": " 0:00:03",
    "SOME_SHORTNUM": 24,
    "SOME_BESTNUM": 52
    },
    {
    "PRIMARY_KEY_FIELD": 10174,
    "SOME_CHAR": "174 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.753009963666,
    "SOME_DATE": "26JAN1961",
    "SOME_DATETIME": " 01JAN1960:07:06:38",
    "SOME_TIME": " 0:01:09",
    "SOME_SHORTNUM": 10,
    "SOME_BESTNUM": 50
    },
    {
    "PRIMARY_KEY_FIELD": 10175,
    "SOME_CHAR": "175 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.716320670543,
    "SOME_DATE": "15AUG1962",
    "SOME_DATETIME": " 01JAN1960:04:48:32",
    "SOME_TIME": " 0:01:15",
    "SOME_SHORTNUM": 80,
    "SOME_BESTNUM": 50
    },
    {
    "PRIMARY_KEY_FIELD": 10176,
    "SOME_CHAR": "176 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.791488602195,
    "SOME_DATE": "26MAY1960",
    "SOME_DATETIME": " 01JAN1960:12:09:38",
    "SOME_TIME": " 0:00:51",
    "SOME_SHORTNUM": 4,
    "SOME_BESTNUM": 35
    },
    {
    "PRIMARY_KEY_FIELD": 10177,
    "SOME_CHAR": "177 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.0925364206045,
    "SOME_DATE": "18APR1960",
    "SOME_DATETIME": " 01JAN1960:01:35:06",
    "SOME_TIME": " 0:00:03",
    "SOME_SHORTNUM": 54,
    "SOME_BESTNUM": 99
    },
    {
    "PRIMARY_KEY_FIELD": 10178,
    "SOME_CHAR": "178 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.117532277069,
    "SOME_DATE": "21SEP1961",
    "SOME_DATETIME": " 01JAN1960:05:49:58",
    "SOME_TIME": " 0:00:30",
    "SOME_SHORTNUM": 9,
    "SOME_BESTNUM": 4
    },
    {
    "PRIMARY_KEY_FIELD": 10179,
    "SOME_CHAR": "179 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.768050647233,
    "SOME_DATE": "25APR1961",
    "SOME_DATETIME": " 01JAN1960:04:38:18",
    "SOME_TIME": " 0:01:04",
    "SOME_SHORTNUM": 29,
    "SOME_BESTNUM": 40
    },
    {
    "PRIMARY_KEY_FIELD": 10180,
    "SOME_CHAR": "180 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.726473274979,
    "SOME_DATE": "04JAN1960",
    "SOME_DATETIME": " 01JAN1960:00:11:32",
    "SOME_TIME": " 0:01:38",
    "SOME_SHORTNUM": 26,
    "SOME_BESTNUM": 63
    },
    {
    "PRIMARY_KEY_FIELD": 10181,
    "SOME_CHAR": "181 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.611879586527,
    "SOME_DATE": "15MAR1962",
    "SOME_DATETIME": " 01JAN1960:04:18:35",
    "SOME_TIME": " 0:00:35",
    "SOME_SHORTNUM": 74,
    "SOME_BESTNUM": 9
    },
    {
    "PRIMARY_KEY_FIELD": 10182,
    "SOME_CHAR": "182 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.378131004226,
    "SOME_DATE": "05AUG1962",
    "SOME_DATETIME": " 01JAN1960:04:46:05",
    "SOME_TIME": " 0:00:45",
    "SOME_SHORTNUM": 40,
    "SOME_BESTNUM": 70
    },
    {
    "PRIMARY_KEY_FIELD": 10183,
    "SOME_CHAR": "183 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.772194760745,
    "SOME_DATE": "29NOV1960",
    "SOME_DATETIME": " 01JAN1960:05:09:32",
    "SOME_TIME": " 0:00:47",
    "SOME_SHORTNUM": 75,
    "SOME_BESTNUM": 13
    },
    {
    "PRIMARY_KEY_FIELD": 10184,
    "SOME_CHAR": "184 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.0812941533892,
    "SOME_DATE": "28JUN1961",
    "SOME_DATETIME": " 01JAN1960:11:39:08",
    "SOME_TIME": " 0:01:13",
    "SOME_SHORTNUM": 69,
    "SOME_BESTNUM": 42
    },
    {
    "PRIMARY_KEY_FIELD": 10185,
    "SOME_CHAR": "185 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.312568983209,
    "SOME_DATE": "27FEB1962",
    "SOME_DATETIME": " 01JAN1960:11:13:19",
    "SOME_TIME": " 0:01:19",
    "SOME_SHORTNUM": 46,
    "SOME_BESTNUM": 81
    },
    {
    "PRIMARY_KEY_FIELD": 10186,
    "SOME_CHAR": "186 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.17449143863,
    "SOME_DATE": "03MAR1962",
    "SOME_DATETIME": " 01JAN1960:13:34:59",
    "SOME_TIME": " 0:01:22",
    "SOME_SHORTNUM": 75,
    "SOME_BESTNUM": 11
    },
    {
    "PRIMARY_KEY_FIELD": 10187,
    "SOME_CHAR": "187 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.967759952865,
    "SOME_DATE": "14OCT1960",
    "SOME_DATETIME": " 01JAN1960:13:48:30",
    "SOME_TIME": " 0:00:56",
    "SOME_SHORTNUM": 53,
    "SOME_BESTNUM": 48
    },
    {
    "PRIMARY_KEY_FIELD": 10188,
    "SOME_CHAR": "188 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.891128778407,
    "SOME_DATE": "05MAR1960",
    "SOME_DATETIME": " 01JAN1960:05:28:49",
    "SOME_TIME": " 0:01:29",
    "SOME_SHORTNUM": 53,
    "SOME_BESTNUM": 7
    },
    {
    "PRIMARY_KEY_FIELD": 10189,
    "SOME_CHAR": "189 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.0667286301342,
    "SOME_DATE": "17MAR1960",
    "SOME_DATETIME": " 01JAN1960:07:56:56",
    "SOME_TIME": " 0:00:10",
    "SOME_SHORTNUM": 62,
    "SOME_BESTNUM": 87
    },
    {
    "PRIMARY_KEY_FIELD": 10190,
    "SOME_CHAR": "190 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.940826008069,
    "SOME_DATE": "27MAY1960",
    "SOME_DATETIME": " 01JAN1960:11:42:13",
    "SOME_TIME": " 0:00:34",
    "SOME_SHORTNUM": 45,
    "SOME_BESTNUM": 53
    },
    {
    "PRIMARY_KEY_FIELD": 10191,
    "SOME_CHAR": "191 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.310336881462,
    "SOME_DATE": "16APR1962",
    "SOME_DATETIME": " 01JAN1960:08:35:10",
    "SOME_TIME": " 0:00:16",
    "SOME_SHORTNUM": 47,
    "SOME_BESTNUM": 17
    },
    {
    "PRIMARY_KEY_FIELD": 10192,
    "SOME_CHAR": "192 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.187635322654,
    "SOME_DATE": "03DEC1960",
    "SOME_DATETIME": " 01JAN1960:11:15:21",
    "SOME_TIME": " 0:00:03",
    "SOME_SHORTNUM": 51,
    "SOME_BESTNUM": 83
    },
    {
    "PRIMARY_KEY_FIELD": 10193,
    "SOME_CHAR": "193 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.962409494427,
    "SOME_DATE": "18OCT1960",
    "SOME_DATETIME": " 01JAN1960:04:17:51",
    "SOME_TIME": " 0:00:49",
    "SOME_SHORTNUM": 39,
    "SOME_BESTNUM": 59
    },
    {
    "PRIMARY_KEY_FIELD": 10194,
    "SOME_CHAR": "194 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.818376173181,
    "SOME_DATE": "23FEB1961",
    "SOME_DATETIME": " 01JAN1960:11:08:28",
    "SOME_TIME": " 0:00:43",
    "SOME_SHORTNUM": 39,
    "SOME_BESTNUM": 67
    },
    {
    "PRIMARY_KEY_FIELD": 10195,
    "SOME_CHAR": "195 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.249898561393,
    "SOME_DATE": "01NOV1961",
    "SOME_DATETIME": " 01JAN1960:04:15:44",
    "SOME_TIME": " 0:01:32",
    "SOME_SHORTNUM": 43,
    "SOME_BESTNUM": 42
    },
    {
    "PRIMARY_KEY_FIELD": 10196,
    "SOME_CHAR": "196 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.303004053097,
    "SOME_DATE": "24JAN1961",
    "SOME_DATETIME": " 01JAN1960:06:32:50",
    "SOME_TIME": " 0:00:23",
    "SOME_SHORTNUM": 54,
    "SOME_BESTNUM": 49
    },
    {
    "PRIMARY_KEY_FIELD": 10197,
    "SOME_CHAR": "197 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.724311870394,
    "SOME_DATE": "10SEP1960",
    "SOME_DATETIME": " 01JAN1960:09:12:28",
    "SOME_TIME": " 0:00:08",
    "SOME_SHORTNUM": 44,
    "SOME_BESTNUM": 61
    },
    {
    "PRIMARY_KEY_FIELD": 10198,
    "SOME_CHAR": "198 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.281540174634,
    "SOME_DATE": "17SEP1962",
    "SOME_DATETIME": " 01JAN1960:01:33:10",
    "SOME_TIME": " 0:00:35",
    "SOME_SHORTNUM": 70,
    "SOME_BESTNUM": 58
    },
    {
    "PRIMARY_KEY_FIELD": 10199,
    "SOME_CHAR": "199 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.919220488015,
    "SOME_DATE": "08MAY1960",
    "SOME_DATETIME": " 01JAN1960:09:33:28",
    "SOME_TIME": " 0:01:09",
    "SOME_SHORTNUM": 54,
    "SOME_BESTNUM": 0
    },
    {
    "PRIMARY_KEY_FIELD": 10200,
    "SOME_CHAR": "200 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.505939372585,
    "SOME_DATE": "17APR1960",
    "SOME_DATETIME": " 01JAN1960:05:14:10",
    "SOME_TIME": " 0:01:18",
    "SOME_SHORTNUM": 83,
    "SOME_BESTNUM": 97
    },
    {
    "PRIMARY_KEY_FIELD": 10201,
    "SOME_CHAR": "201 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.143907736123,
    "SOME_DATE": "04AUG1962",
    "SOME_DATETIME": " 01JAN1960:04:44:27",
    "SOME_TIME": " 0:01:04",
    "SOME_SHORTNUM": 26,
    "SOME_BESTNUM": 8
    },
    {
    "PRIMARY_KEY_FIELD": 10202,
    "SOME_CHAR": "202 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.221468285295,
    "SOME_DATE": "02SEP1961",
    "SOME_DATETIME": " 01JAN1960:06:49:19",
    "SOME_TIME": " 0:00:46",
    "SOME_SHORTNUM": 49,
    "SOME_BESTNUM": 59
    },
    {
    "PRIMARY_KEY_FIELD": 10203,
    "SOME_CHAR": "203 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.258218495761,
    "SOME_DATE": "25OCT1961",
    "SOME_DATETIME": " 01JAN1960:09:37:09",
    "SOME_TIME": " 0:00:01",
    "SOME_SHORTNUM": 6,
    "SOME_BESTNUM": 71
    },
    {
    "PRIMARY_KEY_FIELD": 10204,
    "SOME_CHAR": "204 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.0766677759013,
    "SOME_DATE": "11APR1961",
    "SOME_DATETIME": " 01JAN1960:07:32:18",
    "SOME_TIME": " 0:00:26",
    "SOME_SHORTNUM": 99,
    "SOME_BESTNUM": 30
    },
    {
    "PRIMARY_KEY_FIELD": 10205,
    "SOME_CHAR": "205 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.993368008171,
    "SOME_DATE": "25NOV1961",
    "SOME_DATETIME": " 01JAN1960:04:29:28",
    "SOME_TIME": " 0:01:05",
    "SOME_SHORTNUM": 11,
    "SOME_BESTNUM": 46
    },
    {
    "PRIMARY_KEY_FIELD": 10206,
    "SOME_CHAR": "206 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.872760605473,
    "SOME_DATE": "30JUL1961",
    "SOME_DATETIME": " 01JAN1960:07:00:29",
    "SOME_TIME": " 0:01:20",
    "SOME_SHORTNUM": 91,
    "SOME_BESTNUM": 90
    },
    {
    "PRIMARY_KEY_FIELD": 10207,
    "SOME_CHAR": "207 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.789409748646,
    "SOME_DATE": "07JAN1960",
    "SOME_DATETIME": " 01JAN1960:13:04:56",
    "SOME_TIME": " 0:00:26",
    "SOME_SHORTNUM": 63,
    "SOME_BESTNUM": 27
    },
    {
    "PRIMARY_KEY_FIELD": 10208,
    "SOME_CHAR": "208 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.34292900066,
    "SOME_DATE": "15JAN1960",
    "SOME_DATETIME": " 01JAN1960:08:44:18",
    "SOME_TIME": " 0:00:36",
    "SOME_SHORTNUM": 22,
    "SOME_BESTNUM": 82
    },
    {
    "PRIMARY_KEY_FIELD": 10209,
    "SOME_CHAR": "209 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.853056217475,
    "SOME_DATE": "20SEP1962",
    "SOME_DATETIME": " 01JAN1960:00:37:31",
    "SOME_TIME": " 0:01:26",
    "SOME_SHORTNUM": 89,
    "SOME_BESTNUM": 14
    },
    {
    "PRIMARY_KEY_FIELD": 10210,
    "SOME_CHAR": "210 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.1569773956,
    "SOME_DATE": "17JUL1960",
    "SOME_DATETIME": " 01JAN1960:12:40:44",
    "SOME_TIME": " 0:00:46",
    "SOME_SHORTNUM": 91,
    "SOME_BESTNUM": 4
    },
    {
    "PRIMARY_KEY_FIELD": 10211,
    "SOME_CHAR": "211 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.28367675109,
    "SOME_DATE": "25JUN1962",
    "SOME_DATETIME": " 01JAN1960:03:47:07",
    "SOME_TIME": " 0:00:42",
    "SOME_SHORTNUM": 90,
    "SOME_BESTNUM": 59
    },
    {
    "PRIMARY_KEY_FIELD": 10212,
    "SOME_CHAR": "212 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.319825820774,
    "SOME_DATE": "13JAN1961",
    "SOME_DATETIME": " 01JAN1960:02:02:49",
    "SOME_TIME": " 0:01:18",
    "SOME_SHORTNUM": 38,
    "SOME_BESTNUM": 8
    },
    {
    "PRIMARY_KEY_FIELD": 10213,
    "SOME_CHAR": "213 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.236386205645,
    "SOME_DATE": "10OCT1961",
    "SOME_DATETIME": " 01JAN1960:12:17:24",
    "SOME_TIME": " 0:01:33",
    "SOME_SHORTNUM": 90,
    "SOME_BESTNUM": 55
    },
    {
    "PRIMARY_KEY_FIELD": 10214,
    "SOME_CHAR": "214 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.742101271051,
    "SOME_DATE": "25JAN1960",
    "SOME_DATETIME": " 01JAN1960:05:39:56",
    "SOME_TIME": " 0:01:34",
    "SOME_SHORTNUM": 44,
    "SOME_BESTNUM": 25
    },
    {
    "PRIMARY_KEY_FIELD": 10215,
    "SOME_CHAR": "215 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.863742849726,
    "SOME_DATE": "15MAR1960",
    "SOME_DATETIME": " 01JAN1960:13:30:58",
    "SOME_TIME": " 0:01:40",
    "SOME_SHORTNUM": 86,
    "SOME_BESTNUM": 85
    },
    {
    "PRIMARY_KEY_FIELD": 10216,
    "SOME_CHAR": "216 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.750501612551,
    "SOME_DATE": "29FEB1960",
    "SOME_DATETIME": " 01JAN1960:12:48:41",
    "SOME_TIME": " 0:01:29",
    "SOME_SHORTNUM": 20,
    "SOME_BESTNUM": 42
    },
    {
    "PRIMARY_KEY_FIELD": 10217,
    "SOME_CHAR": "217 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.665220306099,
    "SOME_DATE": "21SEP1962",
    "SOME_DATETIME": " 01JAN1960:11:47:48",
    "SOME_TIME": " 0:00:11",
    "SOME_SHORTNUM": 56,
    "SOME_BESTNUM": 86
    },
    {
    "PRIMARY_KEY_FIELD": 10218,
    "SOME_CHAR": "218 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.314111363755,
    "SOME_DATE": "17OCT1961",
    "SOME_DATETIME": " 01JAN1960:07:34:23",
    "SOME_TIME": " 0:01:05",
    "SOME_SHORTNUM": 25,
    "SOME_BESTNUM": 96
    },
    {
    "PRIMARY_KEY_FIELD": 10219,
    "SOME_CHAR": "219 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.475602078007,
    "SOME_DATE": "14MAY1961",
    "SOME_DATETIME": " 01JAN1960:13:32:24",
    "SOME_TIME": " 0:00:43",
    "SOME_SHORTNUM": 95,
    "SOME_BESTNUM": 39
    },
    {
    "PRIMARY_KEY_FIELD": 10220,
    "SOME_CHAR": "220 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.292178807451,
    "SOME_DATE": "14MAY1961",
    "SOME_DATETIME": " 01JAN1960:03:16:05",
    "SOME_TIME": " 0:00:39",
    "SOME_SHORTNUM": 38,
    "SOME_BESTNUM": 34
    },
    {
    "PRIMARY_KEY_FIELD": 10221,
    "SOME_CHAR": "221 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.342086964446,
    "SOME_DATE": "21FEB1962",
    "SOME_DATETIME": " 01JAN1960:13:35:14",
    "SOME_TIME": " 0:00:46",
    "SOME_SHORTNUM": 28,
    "SOME_BESTNUM": 67
    },
    {
    "PRIMARY_KEY_FIELD": 10222,
    "SOME_CHAR": "222 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.361870947928,
    "SOME_DATE": "18JAN1960",
    "SOME_DATETIME": " 01JAN1960:09:45:01",
    "SOME_TIME": " 0:00:36",
    "SOME_SHORTNUM": 32,
    "SOME_BESTNUM": 96
    },
    {
    "PRIMARY_KEY_FIELD": 10223,
    "SOME_CHAR": "223 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.571696416741,
    "SOME_DATE": "12SEP1960",
    "SOME_DATETIME": " 01JAN1960:11:04:22",
    "SOME_TIME": " 0:00:46",
    "SOME_SHORTNUM": 25,
    "SOME_BESTNUM": 32
    },
    {
    "PRIMARY_KEY_FIELD": 10224,
    "SOME_CHAR": "224 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.819183359304,
    "SOME_DATE": "22FEB1960",
    "SOME_DATETIME": " 01JAN1960:00:11:18",
    "SOME_TIME": " 0:00:48",
    "SOME_SHORTNUM": 93,
    "SOME_BESTNUM": 9
    },
    {
    "PRIMARY_KEY_FIELD": 10225,
    "SOME_CHAR": "225 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.0242283884549,
    "SOME_DATE": "26MAR1960",
    "SOME_DATETIME": " 01JAN1960:11:41:08",
    "SOME_TIME": " 0:01:23",
    "SOME_SHORTNUM": 16,
    "SOME_BESTNUM": 80
    },
    {
    "PRIMARY_KEY_FIELD": 10226,
    "SOME_CHAR": "226 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.418458293387,
    "SOME_DATE": "29OCT1960",
    "SOME_DATETIME": " 01JAN1960:06:51:07",
    "SOME_TIME": " 0:00:57",
    "SOME_SHORTNUM": 21,
    "SOME_BESTNUM": 94
    },
    {
    "PRIMARY_KEY_FIELD": 10227,
    "SOME_CHAR": "227 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.855226562757,
    "SOME_DATE": "26JAN1960",
    "SOME_DATETIME": " 01JAN1960:01:22:42",
    "SOME_TIME": " 0:00:39",
    "SOME_SHORTNUM": 16,
    "SOME_BESTNUM": 92
    },
    {
    "PRIMARY_KEY_FIELD": 10228,
    "SOME_CHAR": "228 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.612432046613,
    "SOME_DATE": "30JUL1960",
    "SOME_DATETIME": " 01JAN1960:08:13:43",
    "SOME_TIME": " 0:00:51",
    "SOME_SHORTNUM": 3,
    "SOME_BESTNUM": 47
    },
    {
    "PRIMARY_KEY_FIELD": 10229,
    "SOME_CHAR": "229 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.0565994717444,
    "SOME_DATE": "14JUN1962",
    "SOME_DATETIME": " 01JAN1960:12:06:18",
    "SOME_TIME": " 0:00:03",
    "SOME_SHORTNUM": 89,
    "SOME_BESTNUM": 43
    },
    {
    "PRIMARY_KEY_FIELD": 10230,
    "SOME_CHAR": "230 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.234995038824,
    "SOME_DATE": "05MAY1961",
    "SOME_DATETIME": " 01JAN1960:01:11:48",
    "SOME_TIME": " 0:01:02",
    "SOME_SHORTNUM": 37,
    "SOME_BESTNUM": 48
    },
    {
    "PRIMARY_KEY_FIELD": 10231,
    "SOME_CHAR": "231 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.905498261054,
    "SOME_DATE": "04FEB1961",
    "SOME_DATETIME": " 01JAN1960:01:50:23",
    "SOME_TIME": " 0:01:29",
    "SOME_SHORTNUM": 33,
    "SOME_BESTNUM": 46
    },
    {
    "PRIMARY_KEY_FIELD": 10232,
    "SOME_CHAR": "232 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.489890096006,
    "SOME_DATE": "14JAN1962",
    "SOME_DATETIME": " 01JAN1960:07:07:05",
    "SOME_TIME": " 0:00:56",
    "SOME_SHORTNUM": 51,
    "SOME_BESTNUM": 95
    },
    {
    "PRIMARY_KEY_FIELD": 10233,
    "SOME_CHAR": "233 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.458665252877,
    "SOME_DATE": "06AUG1960",
    "SOME_DATETIME": " 01JAN1960:12:28:55",
    "SOME_TIME": " 0:01:09",
    "SOME_SHORTNUM": 85,
    "SOME_BESTNUM": 54
    },
    {
    "PRIMARY_KEY_FIELD": 10234,
    "SOME_CHAR": "234 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.85541833977,
    "SOME_DATE": "01OCT1961",
    "SOME_DATETIME": " 01JAN1960:06:53:06",
    "SOME_TIME": " 0:00:23",
    "SOME_SHORTNUM": 38,
    "SOME_BESTNUM": 89
    },
    {
    "PRIMARY_KEY_FIELD": 10235,
    "SOME_CHAR": "235 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.857599226226,
    "SOME_DATE": "30OCT1961",
    "SOME_DATETIME": " 01JAN1960:06:13:30",
    "SOME_TIME": " 0:00:58",
    "SOME_SHORTNUM": 67,
    "SOME_BESTNUM": 99
    },
    {
    "PRIMARY_KEY_FIELD": 10236,
    "SOME_CHAR": "236 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.844420214111,
    "SOME_DATE": "11APR1960",
    "SOME_DATETIME": " 01JAN1960:00:27:45",
    "SOME_TIME": " 0:01:22",
    "SOME_SHORTNUM": 65,
    "SOME_BESTNUM": 33
    },
    {
    "PRIMARY_KEY_FIELD": 10237,
    "SOME_CHAR": "237 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.474337428098,
    "SOME_DATE": "13JAN1961",
    "SOME_DATETIME": " 01JAN1960:06:48:00",
    "SOME_TIME": " 0:00:58",
    "SOME_SHORTNUM": 22,
    "SOME_BESTNUM": 29
    },
    {
    "PRIMARY_KEY_FIELD": 10238,
    "SOME_CHAR": "238 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.326561977773,
    "SOME_DATE": "31MAY1961",
    "SOME_DATETIME": " 01JAN1960:12:12:34",
    "SOME_TIME": " 0:00:03",
    "SOME_SHORTNUM": 94,
    "SOME_BESTNUM": 67
    },
    {
    "PRIMARY_KEY_FIELD": 10239,
    "SOME_CHAR": "239 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.921823682693,
    "SOME_DATE": "13DEC1961",
    "SOME_DATETIME": " 01JAN1960:01:20:43",
    "SOME_TIME": " 0:01:11",
    "SOME_SHORTNUM": 46,
    "SOME_BESTNUM": 48
    },
    {
    "PRIMARY_KEY_FIELD": 10240,
    "SOME_CHAR": "240 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.121250483264,
    "SOME_DATE": "18DEC1960",
    "SOME_DATETIME": " 01JAN1960:02:34:37",
    "SOME_TIME": " 0:00:22",
    "SOME_SHORTNUM": 2,
    "SOME_BESTNUM": 32
    },
    {
    "PRIMARY_KEY_FIELD": 10241,
    "SOME_CHAR": "241 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.949745169352,
    "SOME_DATE": "07JUN1961",
    "SOME_DATETIME": " 01JAN1960:06:48:37",
    "SOME_TIME": " 0:00:34",
    "SOME_SHORTNUM": 3,
    "SOME_BESTNUM": 67
    },
    {
    "PRIMARY_KEY_FIELD": 10242,
    "SOME_CHAR": "242 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.255982018661,
    "SOME_DATE": "13MAR1962",
    "SOME_DATETIME": " 01JAN1960:07:37:42",
    "SOME_TIME": " 0:01:04",
    "SOME_SHORTNUM": 76,
    "SOME_BESTNUM": 70
    },
    {
    "PRIMARY_KEY_FIELD": 10243,
    "SOME_CHAR": "243 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.759833389781,
    "SOME_DATE": "28JUN1960",
    "SOME_DATETIME": " 01JAN1960:08:19:49",
    "SOME_TIME": " 0:01:01",
    "SOME_SHORTNUM": 29,
    "SOME_BESTNUM": 71
    },
    {
    "PRIMARY_KEY_FIELD": 10244,
    "SOME_CHAR": "244 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.700597827183,
    "SOME_DATE": "24JUL1960",
    "SOME_DATETIME": " 01JAN1960:05:10:15",
    "SOME_TIME": " 0:00:38",
    "SOME_SHORTNUM": 53,
    "SOME_BESTNUM": 10
    },
    {
    "PRIMARY_KEY_FIELD": 10245,
    "SOME_CHAR": "245 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.221195794745,
    "SOME_DATE": "05SEP1960",
    "SOME_DATETIME": " 01JAN1960:08:59:36",
    "SOME_TIME": " 0:01:07",
    "SOME_SHORTNUM": 44,
    "SOME_BESTNUM": 33
    },
    {
    "PRIMARY_KEY_FIELD": 10246,
    "SOME_CHAR": "246 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.200741445739,
    "SOME_DATE": "24MAR1960",
    "SOME_DATETIME": " 01JAN1960:02:29:02",
    "SOME_TIME": " 0:00:09",
    "SOME_SHORTNUM": 37,
    "SOME_BESTNUM": 88
    },
    {
    "PRIMARY_KEY_FIELD": 10247,
    "SOME_CHAR": "247 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.287490293517,
    "SOME_DATE": "24JUL1961",
    "SOME_DATETIME": " 01JAN1960:02:55:57",
    "SOME_TIME": " 0:00:57",
    "SOME_SHORTNUM": 30,
    "SOME_BESTNUM": 33
    },
    {
    "PRIMARY_KEY_FIELD": 10248,
    "SOME_CHAR": "248 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.561167126783,
    "SOME_DATE": "25JUN1960",
    "SOME_DATETIME": " 01JAN1960:02:01:02",
    "SOME_TIME": " 0:01:36",
    "SOME_SHORTNUM": 49,
    "SOME_BESTNUM": 90
    },
    {
    "PRIMARY_KEY_FIELD": 10249,
    "SOME_CHAR": "249 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.349366858299,
    "SOME_DATE": "28FEB1961",
    "SOME_DATETIME": " 01JAN1960:04:26:52",
    "SOME_TIME": " 0:01:35",
    "SOME_SHORTNUM": 81,
    "SOME_BESTNUM": 25
    },
    {
    "PRIMARY_KEY_FIELD": 10250,
    "SOME_CHAR": "250 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.253183987575,
    "SOME_DATE": "04FEB1961",
    "SOME_DATETIME": " 01JAN1960:03:37:40",
    "SOME_TIME": " 0:01:33",
    "SOME_SHORTNUM": 38,
    "SOME_BESTNUM": 32
    },
    {
    "PRIMARY_KEY_FIELD": 10251,
    "SOME_CHAR": "251 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.747167228603,
    "SOME_DATE": "14APR1960",
    "SOME_DATETIME": " 01JAN1960:10:00:45",
    "SOME_TIME": " 0:01:35",
    "SOME_SHORTNUM": 12,
    "SOME_BESTNUM": 91
    },
    {
    "PRIMARY_KEY_FIELD": 10252,
    "SOME_CHAR": "252 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.27761360038,
    "SOME_DATE": "13SEP1961",
    "SOME_DATETIME": " 01JAN1960:09:09:05",
    "SOME_TIME": " 0:00:24",
    "SOME_SHORTNUM": 43,
    "SOME_BESTNUM": 56
    },
    {
    "PRIMARY_KEY_FIELD": 10253,
    "SOME_CHAR": "253 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.10966447094,
    "SOME_DATE": "04APR1962",
    "SOME_DATETIME": " 01JAN1960:00:26:22",
    "SOME_TIME": " 0:01:22",
    "SOME_SHORTNUM": 47,
    "SOME_BESTNUM": 71
    },
    {
    "PRIMARY_KEY_FIELD": 10254,
    "SOME_CHAR": "254 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.205800468198,
    "SOME_DATE": "30MAY1961",
    "SOME_DATETIME": " 01JAN1960:13:20:29",
    "SOME_TIME": " 0:00:52",
    "SOME_SHORTNUM": 84,
    "SOME_BESTNUM": 12
    },
    {
    "PRIMARY_KEY_FIELD": 10255,
    "SOME_CHAR": "255 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.253791187077,
    "SOME_DATE": "12JUN1961",
    "SOME_DATETIME": " 01JAN1960:05:43:23",
    "SOME_TIME": " 0:00:24",
    "SOME_SHORTNUM": 50,
    "SOME_BESTNUM": 93
    },
    {
    "PRIMARY_KEY_FIELD": 10256,
    "SOME_CHAR": "256 bottles of beer on the wall",
    "SOME_DROPDOWN": "Option 1",
    "SOME_NUM": 0.916653747632,
    "SOME_DATE": "06DEC1960",
    "SOME_DATETIME": " 01JAN1960:09:22:08",
    "SOME_TIME": " 0:01:32",
    "SOME_SHORTNUM": 65,
    "SOME_BESTNUM": 18
    }
    ]
    , "$viewdata":{"vars":{
    "PRIMARY_KEY_FIELD" :{"format":"best." ,"label":"PRIMARY_KEY_FIELD" ,"length":"8" ,"type":"num" }
    ,"SOME_CHAR" :{"format":"$32767." ,"label":"SOME_CHAR" ,"length":"32767" ,"type":"char" }
    ,"SOME_DROPDOWN" :{"format":"$128." ,"label":"SOME_DROPDOWN" ,"length":"128" ,"type":"char" }
    ,"SOME_NUM" :{"format":"best." ,"label":"SOME_NUM" ,"length":"8" ,"type":"num" }
    ,"SOME_DATE" :{"format":"DATE9." ,"label":"SOME_DATE" ,"length":"8" ,"type":"num" }
    ,"SOME_DATETIME" :{"format":"DATETIME19." ,"label":"SOME_DATETIME" ,"length":"8" ,"type":"num" }
    ,"SOME_TIME" :{"format":"TIME8." ,"label":"SOME_TIME" ,"length":"8" ,"type":"num" }
    ,"SOME_SHORTNUM" :{"format":"best." ,"label":"SOME_SHORTNUM" ,"length":"4" ,"type":"num" }
    ,"SOME_BESTNUM" :{"format":"BEST." ,"label":"SOME_BESTNUM" ,"length":"8" ,"type":"num" }
    }}
    ,"_DEBUG" : ""
    ,"_METAUSER": "sasdemo@SAS"
    ,"_METAPERSON": "sasdemo"
    ,"_PROGRAM" : "/Projects/app/dc/services/public/viewdata"
    ,"AUTOEXEC" : "D%3A%5Copt%5Csasinside%5CConfig%5CLev1%5CSASApp%5CStoredProcessServer%5Cautoexec.sas"
    ,"MF_GETUSER" : "sasdemo"
    ,"SYSCC" : "0"
    ,"SYSENCODING" : "wlatin1"
    ,"SYSERRORTEXT" : ""
    ,"SYSHOSTNAME" : "SAS"
    ,"SYSPROCESSID" : "41DD8057BBE1CAC140BA860000000000"
    ,"SYSPROCESSMODE" : "SAS Stored Process Server"
    ,"SYSPROCESSNAME" : ""
    ,"SYSJOBID" : "27448"
    ,"SYSSCPL" : "Linunx"
    ,"SYSSITE" : "123"
    ,"SYSUSERID" : "sassrv"
    ,"SYSVLONG" : "9.04.01M7P080520"
    ,"SYSWARNINGTEXT" : "ENCODING option ignored for files opened with RECFM=N."
    ,"END_DTTM" : "2022-09-26T08:48:49.746000"
    ,"MEMSIZE" : "46GB"
    }`,
    MPE_AUDIT: `{
        "SYSDATE": "17JUL23",
        "SYSTIME": "17:53",
        "cls_rules": [
        ],
        "cols": [
            {
                "NAME": "DSN",
                "LENGTH": 32,
                "VARNUM": 3,
                "LABEL": "Dataset Name (32 chars)",
                "FMTNAME": "",
                "FORMAT": "$32.",
                "TYPE": "C",
                "DDTYPE": "CHARACTER"
            },
            {
                "NAME": "IS_DIFF",
                "LENGTH": 8,
                "VARNUM": 9,
                "LABEL": "Did value change? (1/0/-1).  Always -1 for appends and deletes.",
                "FMTNAME": "",
                "FORMAT": "8.",
                "TYPE": "N",
                "DDTYPE": "NUMERIC"
            },
            {
                "NAME": "IS_PK",
                "LENGTH": 8,
                "VARNUM": 8,
                "LABEL": "Is Primary Key Field? (1/0)",
                "FMTNAME": "",
                "FORMAT": "8.",
                "TYPE": "N",
                "DDTYPE": "NUMERIC"
            },
            {
                "NAME": "KEY_HASH",
                "LENGTH": 32,
                "VARNUM": 4,
                "LABEL": "MD5 Hash of primary key values (pipe seperated)",
                "FMTNAME": "",
                "FORMAT": "$32.",
                "TYPE": "C",
                "DDTYPE": "CHARACTER"
            },
            {
                "NAME": "LIBREF",
                "LENGTH": 8,
                "VARNUM": 2,
                "LABEL": "Library Reference (8 chars)",
                "FMTNAME": "",
                "FORMAT": "$8.",
                "TYPE": "C",
                "DDTYPE": "CHARACTER"
            },
            {
                "NAME": "LOAD_REF",
                "LENGTH": 36,
                "VARNUM": 1,
                "LABEL": "unique load reference",
                "FMTNAME": "",
                "FORMAT": "$36.",
                "TYPE": "C",
                "DDTYPE": "CHARACTER"
            },
            {
                "NAME": "MOVE_TYPE",
                "LENGTH": 1,
                "VARNUM": 6,
                "LABEL": "Either (A)ppended, (D)eleted or (M)odified",
                "FMTNAME": "",
                "FORMAT": "$1.",
                "TYPE": "C",
                "DDTYPE": "CHARACTER"
            },
            {
                "NAME": "NEWVAL_CHAR",
                "LENGTH": 32765,
                "VARNUM": 14,
                "LABEL": "New (character) value",
                "FMTNAME": "",
                "FORMAT": "$32765.",
                "TYPE": "C",
                "DDTYPE": "CHARACTER"
            },
            {
                "NAME": "NEWVAL_NUM",
                "LENGTH": 8,
                "VARNUM": 12,
                "LABEL": "New (numeric) value",
                "FMTNAME": "BEST",
                "FORMAT": "BEST32.",
                "TYPE": "N",
                "DDTYPE": "NUMERIC"
            },
            {
                "NAME": "OLDVAL_CHAR",
                "LENGTH": 32765,
                "VARNUM": 13,
                "LABEL": "Old (character) value",
                "FMTNAME": "",
                "FORMAT": "$32765.",
                "TYPE": "C",
                "DDTYPE": "CHARACTER"
            },
            {
                "NAME": "OLDVAL_NUM",
                "LENGTH": 8,
                "VARNUM": 11,
                "LABEL": "Old (numeric) value",
                "FMTNAME": "BEST",
                "FORMAT": "BEST32.",
                "TYPE": "N",
                "DDTYPE": "NUMERIC"
            },
            {
                "NAME": "PROCESSED_DTTM",
                "LENGTH": 8,
                "VARNUM": 7,
                "LABEL": "Processed at timestamp",
                "FMTNAME": "E8601DT",
                "FORMAT": "E8601DT26.6",
                "TYPE": "N",
                "DDTYPE": "DATETIME"
            },
            {
                "NAME": "TGTVAR_NM",
                "LENGTH": 32,
                "VARNUM": 5,
                "LABEL": "Target variable name (32 chars)",
                "FMTNAME": "",
                "FORMAT": "$32.",
                "TYPE": "C",
                "DDTYPE": "CHARACTER"
            },
            {
                "NAME": "TGTVAR_TYPE",
                "LENGTH": 1,
                "VARNUM": 10,
                "LABEL": "Either (C)haracter or (N)umeric",
                "FMTNAME": "",
                "FORMAT": "$1.",
                "TYPE": "C",
                "DDTYPE": "CHARACTER"
            }
        ],
        "dsmeta": [
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Data Set Name",
                "VALUE": "DC406039.MPE_AUDIT"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Observations",
                "VALUE": "56"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Member Type",
                "VALUE": "DATA"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Variables",
                "VALUE": "14"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Engine",
                "VALUE": "V9"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Indexes",
                "VALUE": "1"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Created",
                "VALUE": "07/07/2023 15:19:25"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Observation Length",
                "VALUE": "65712"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Last Modified",
                "VALUE": "12/07/2023 09:56:36"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Deleted Observations",
                "VALUE": "0"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Protection",
                "VALUE": "."
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Compressed",
                "VALUE": "NO"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Data Set Type",
                "VALUE": "."
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Sorted",
                "VALUE": "NO"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Label",
                "VALUE": "."
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Data Representation",
                "VALUE": "SOLARIS_X86_64, LINUX_X86_64, ALPHA_TRU64, LINUX_IA64"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Encoding",
                "VALUE": "utf-8  Unicode (UTF-8)"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Data Set Page Size",
                "VALUE": "73728"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Number of Data Set Pages",
                "VALUE": "57"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "First Data Page",
                "VALUE": "1"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Max Obs per Page",
                "VALUE": "1"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Obs in First Data Page",
                "VALUE": "1"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Index File Page Size",
                "VALUE": "8192"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Number of Index File Pages",
                "VALUE": "4"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Number of Data Set Repairs",
                "VALUE": "0"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Filename",
                "VALUE": "/tmp/mihajlo/DC406039/mpe_audit.sas7bdat"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Release Created",
                "VALUE": "9.0401M7"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Host Created",
                "VALUE": "Linux"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Inode Number",
                "VALUE": "21373252"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Access Permission",
                "VALUE": "rw-rw-r--"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Owner Name",
                "VALUE": "sasjssrv"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "File Size",
                "VALUE": "4MB"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "File Size (bytes)",
                "VALUE": "4276224"
            }
        ],
        "query": [
        ],
        "sasparams": [
            {
                "TABLEURI": "",
                "TABLENAME": "MPE_AUDIT",
                "FILTER_TEXT": "${filterText.replace(/\"/gmi, '\\"')}",
                "PK_FIELDS": "LOAD_REF LIBREF DSN KEY_HASH TGTVAR_NM",
                "NOBS": 56,
                "VARS": 14,
                "MAXROWS": 250
            }
        ],
        "viewdata": [
            {
                "LOAD_REF": "DC20230711T092315732_387472_9241",
                "LIBREF": "DC406039",
                "DSN": "MPE_X_TEST",
                "KEY_HASH": "C81E728D9D4C2F636F067F89CC14862C",
                "TGTVAR_NM": "PRIMARY_KEY_FIELD",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-11T09:23:34.400000",
                "IS_PK": "1",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "N",
                "OLDVAL_NUM": "2",
                "NEWVAL_NUM": "2",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230711T092315732_387472_9241",
                "LIBREF": "DC406039",
                "DSN": "MPE_X_TEST",
                "KEY_HASH": "C81E728D9D4C2F636F067F89CC14862C",
                "TGTVAR_NM": "SOME_BESTNUM",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-11T09:23:34.400000",
                "IS_PK": "0",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "N",
                "OLDVAL_NUM": "44",
                "NEWVAL_NUM": "44",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230711T092315732_387472_9241",
                "LIBREF": "DC406039",
                "DSN": "MPE_X_TEST",
                "KEY_HASH": "C81E728D9D4C2F636F067F89CC14862C",
                "TGTVAR_NM": "SOME_CHAR",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-11T09:23:34.400000",
                "IS_PK": "0",
                "IS_DIFF": "1",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "even more dummy data",
                "NEWVAL_CHAR": "kkk"
            },
            {
                "LOAD_REF": "DC20230711T092315732_387472_9241",
                "LIBREF": "DC406039",
                "DSN": "MPE_X_TEST",
                "KEY_HASH": "C81E728D9D4C2F636F067F89CC14862C",
                "TGTVAR_NM": "SOME_DATE",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-11T09:23:34.400000",
                "IS_PK": "0",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "N",
                "OLDVAL_NUM": "42",
                "NEWVAL_NUM": "42",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230711T092315732_387472_9241",
                "LIBREF": "DC406039",
                "DSN": "MPE_X_TEST",
                "KEY_HASH": "C81E728D9D4C2F636F067F89CC14862C",
                "TGTVAR_NM": "SOME_DATETIME",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-11T09:23:34.400000",
                "IS_PK": "0",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "N",
                "OLDVAL_NUM": "42",
                "NEWVAL_NUM": "42",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230711T092315732_387472_9241",
                "LIBREF": "DC406039",
                "DSN": "MPE_X_TEST",
                "KEY_HASH": "C81E728D9D4C2F636F067F89CC14862C",
                "TGTVAR_NM": "SOME_DROPDOWN",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-11T09:23:34.400000",
                "IS_PK": "0",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "Option 3",
                "NEWVAL_CHAR": "Option 3"
            },
            {
                "LOAD_REF": "DC20230711T092315732_387472_9241",
                "LIBREF": "DC406039",
                "DSN": "MPE_X_TEST",
                "KEY_HASH": "C81E728D9D4C2F636F067F89CC14862C",
                "TGTVAR_NM": "SOME_NUM",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-11T09:23:34.400000",
                "IS_PK": "0",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "N",
                "OLDVAL_NUM": "42",
                "NEWVAL_NUM": "42",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230711T092315732_387472_9241",
                "LIBREF": "DC406039",
                "DSN": "MPE_X_TEST",
                "KEY_HASH": "C81E728D9D4C2F636F067F89CC14862C",
                "TGTVAR_NM": "SOME_SHORTNUM",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-11T09:23:34.400000",
                "IS_PK": "0",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "N",
                "OLDVAL_NUM": "3",
                "NEWVAL_NUM": "3",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230711T092315732_387472_9241",
                "LIBREF": "DC406039",
                "DSN": "MPE_X_TEST",
                "KEY_HASH": "C81E728D9D4C2F636F067F89CC14862C",
                "TGTVAR_NM": "SOME_TIME",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-11T09:23:34.400000",
                "IS_PK": "0",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "N",
                "OLDVAL_NUM": "142",
                "NEWVAL_NUM": "142",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095258550_351113_1353",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "DSN",
                "MOVE_TYPE": "A",
                "PROCESSED_DTTM": "2023-07-12T09:53:21.100000",
                "IS_PK": "1",
                "IS_DIFF": "-1",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": "MPE_USERS"
            },
            {
                "LOAD_REF": "DC20230712T095258550_351113_1353",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "LIBREF",
                "MOVE_TYPE": "A",
                "PROCESSED_DTTM": "2023-07-12T09:53:21.100000",
                "IS_PK": "1",
                "IS_DIFF": "-1",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": "DC406039"
            },
            {
                "LOAD_REF": "DC20230712T095258550_351113_1353",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "AUDIT_LIBDS",
                "MOVE_TYPE": "A",
                "PROCESSED_DTTM": "2023-07-12T09:53:21.100000",
                "IS_PK": "0",
                "IS_DIFF": "-1",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095258550_351113_1353",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "BUSKEY",
                "MOVE_TYPE": "A",
                "PROCESSED_DTTM": "2023-07-12T09:53:21.100000",
                "IS_PK": "0",
                "IS_DIFF": "-1",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": "USER_ID"
            },
            {
                "LOAD_REF": "DC20230712T095258550_351113_1353",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "CLOSE_VARS",
                "MOVE_TYPE": "A",
                "PROCESSED_DTTM": "2023-07-12T09:53:21.100000",
                "IS_PK": "0",
                "IS_DIFF": "-1",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095258550_351113_1353",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "LOADTYPE",
                "MOVE_TYPE": "A",
                "PROCESSED_DTTM": "2023-07-12T09:53:21.100000",
                "IS_PK": "0",
                "IS_DIFF": "-1",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": "UPDATE"
            },
            {
                "LOAD_REF": "DC20230712T095258550_351113_1353",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "NOTES",
                "MOVE_TYPE": "A",
                "PROCESSED_DTTM": "2023-07-12T09:53:21.100000",
                "IS_PK": "0",
                "IS_DIFF": "-1",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095258550_351113_1353",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "NUM_OF_APPROVALS_REQUIRED",
                "MOVE_TYPE": "A",
                "PROCESSED_DTTM": "2023-07-12T09:53:21.100000",
                "IS_PK": "0",
                "IS_DIFF": "-1",
                "TGTVAR_TYPE": "N",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": "1",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095258550_351113_1353",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "POST_APPROVE_HOOK",
                "MOVE_TYPE": "A",
                "PROCESSED_DTTM": "2023-07-12T09:53:21.100000",
                "IS_PK": "0",
                "IS_DIFF": "-1",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095258550_351113_1353",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "POST_EDIT_HOOK",
                "MOVE_TYPE": "A",
                "PROCESSED_DTTM": "2023-07-12T09:53:21.100000",
                "IS_PK": "0",
                "IS_DIFF": "-1",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095258550_351113_1353",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "PRE_APPROVE_HOOK",
                "MOVE_TYPE": "A",
                "PROCESSED_DTTM": "2023-07-12T09:53:21.100000",
                "IS_PK": "0",
                "IS_DIFF": "-1",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095258550_351113_1353",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "PRE_EDIT_HOOK",
                "MOVE_TYPE": "A",
                "PROCESSED_DTTM": "2023-07-12T09:53:21.100000",
                "IS_PK": "0",
                "IS_DIFF": "-1",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095258550_351113_1353",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "RK_UNDERLYING",
                "MOVE_TYPE": "A",
                "PROCESSED_DTTM": "2023-07-12T09:53:21.100000",
                "IS_PK": "0",
                "IS_DIFF": "-1",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095258550_351113_1353",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "SIGNOFF_COLS",
                "MOVE_TYPE": "A",
                "PROCESSED_DTTM": "2023-07-12T09:53:21.100000",
                "IS_PK": "0",
                "IS_DIFF": "-1",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095258550_351113_1353",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "SIGNOFF_HOOK",
                "MOVE_TYPE": "A",
                "PROCESSED_DTTM": "2023-07-12T09:53:21.100000",
                "IS_PK": "0",
                "IS_DIFF": "-1",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095258550_351113_1353",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "TX_FROM",
                "MOVE_TYPE": "A",
                "PROCESSED_DTTM": "2023-07-12T09:53:21.100000",
                "IS_PK": "0",
                "IS_DIFF": "-1",
                "TGTVAR_TYPE": "N",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": "2004774801.1",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095258550_351113_1353",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "TX_TO",
                "MOVE_TYPE": "A",
                "PROCESSED_DTTM": "2023-07-12T09:53:21.100000",
                "IS_PK": "0",
                "IS_DIFF": "-1",
                "TGTVAR_TYPE": "N",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": "253717747199",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095258550_351113_1353",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "VAR_BUSFROM",
                "MOVE_TYPE": "A",
                "PROCESSED_DTTM": "2023-07-12T09:53:21.100000",
                "IS_PK": "0",
                "IS_DIFF": "-1",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095258550_351113_1353",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "VAR_BUSTO",
                "MOVE_TYPE": "A",
                "PROCESSED_DTTM": "2023-07-12T09:53:21.100000",
                "IS_PK": "0",
                "IS_DIFF": "-1",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095258550_351113_1353",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "VAR_PROCESSED",
                "MOVE_TYPE": "A",
                "PROCESSED_DTTM": "2023-07-12T09:53:21.100000",
                "IS_PK": "0",
                "IS_DIFF": "-1",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095258550_351113_1353",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "VAR_TXFROM",
                "MOVE_TYPE": "A",
                "PROCESSED_DTTM": "2023-07-12T09:53:21.100000",
                "IS_PK": "0",
                "IS_DIFF": "-1",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": "REGISTERED_DT"
            },
            {
                "LOAD_REF": "DC20230712T095258550_351113_1353",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "VAR_TXTO",
                "MOVE_TYPE": "A",
                "PROCESSED_DTTM": "2023-07-12T09:53:21.100000",
                "IS_PK": "0",
                "IS_DIFF": "-1",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": "LAST_SEEN_DT"
            },
            {
                "LOAD_REF": "DC20230712T095507852_998478_1675",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "DSN",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-12T09:55:41.700000",
                "IS_PK": "1",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "MPE_USERS",
                "NEWVAL_CHAR": "MPE_USERS"
            },
            {
                "LOAD_REF": "DC20230712T095507852_998478_1675",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "LIBREF",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-12T09:55:41.700000",
                "IS_PK": "1",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "DC406039",
                "NEWVAL_CHAR": "DC406039"
            },
            {
                "LOAD_REF": "DC20230712T095507852_998478_1675",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "AUDIT_LIBDS",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-12T09:55:41.700000",
                "IS_PK": "0",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095507852_998478_1675",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "BUSKEY",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-12T09:55:41.700000",
                "IS_PK": "0",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "USER_ID",
                "NEWVAL_CHAR": "USER_ID"
            },
            {
                "LOAD_REF": "DC20230712T095507852_998478_1675",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "CLOSE_VARS",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-12T09:55:41.700000",
                "IS_PK": "0",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095507852_998478_1675",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "LOADTYPE",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-12T09:55:41.700000",
                "IS_PK": "0",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "UPDATE",
                "NEWVAL_CHAR": "UPDATE"
            },
            {
                "LOAD_REF": "DC20230712T095507852_998478_1675",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "NOTES",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-12T09:55:41.700000",
                "IS_PK": "0",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095507852_998478_1675",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "NUM_OF_APPROVALS_REQUIRED",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-12T09:55:41.700000",
                "IS_PK": "0",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "N",
                "OLDVAL_NUM": "1",
                "NEWVAL_NUM": "1",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095507852_998478_1675",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "POST_APPROVE_HOOK",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-12T09:55:41.700000",
                "IS_PK": "0",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095507852_998478_1675",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "POST_EDIT_HOOK",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-12T09:55:41.700000",
                "IS_PK": "0",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095507852_998478_1675",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "PRE_APPROVE_HOOK",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-12T09:55:41.700000",
                "IS_PK": "0",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095507852_998478_1675",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "PRE_EDIT_HOOK",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-12T09:55:41.700000",
                "IS_PK": "0",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095507852_998478_1675",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "RK_UNDERLYING",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-12T09:55:41.700000",
                "IS_PK": "0",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095507852_998478_1675",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "SIGNOFF_COLS",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-12T09:55:41.700000",
                "IS_PK": "0",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095507852_998478_1675",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "SIGNOFF_HOOK",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-12T09:55:41.700000",
                "IS_PK": "0",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095507852_998478_1675",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "TX_FROM",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-12T09:55:41.700000",
                "IS_PK": "0",
                "IS_DIFF": "1",
                "TGTVAR_TYPE": "N",
                "OLDVAL_NUM": "2004774801.1",
                "NEWVAL_NUM": "2004774941.7",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095507852_998478_1675",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "TX_TO",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-12T09:55:41.700000",
                "IS_PK": "0",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "N",
                "OLDVAL_NUM": "253717747199",
                "NEWVAL_NUM": "253717747199",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095507852_998478_1675",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "VAR_BUSFROM",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-12T09:55:41.700000",
                "IS_PK": "0",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095507852_998478_1675",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "VAR_BUSTO",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-12T09:55:41.700000",
                "IS_PK": "0",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095507852_998478_1675",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "VAR_PROCESSED",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-12T09:55:41.700000",
                "IS_PK": "0",
                "IS_DIFF": "0",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095507852_998478_1675",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "VAR_TXFROM",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-12T09:55:41.700000",
                "IS_PK": "0",
                "IS_DIFF": "1",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "REGISTERED_DT",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095507852_998478_1675",
                "LIBREF": "DC406039",
                "DSN": "MPE_TABLES",
                "KEY_HASH": "441C6BDDE1D6A5322A458F9BC6D91CF7",
                "TGTVAR_NM": "VAR_TXTO",
                "MOVE_TYPE": "M",
                "PROCESSED_DTTM": "2023-07-12T09:55:41.700000",
                "IS_PK": "0",
                "IS_DIFF": "1",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "LAST_SEEN_DT",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095617278_667889_1859",
                "LIBREF": "DC406039",
                "DSN": "MPE_USERS",
                "KEY_HASH": "20EB1B22A92B5C573DC1EB4331FC49EE",
                "TGTVAR_NM": "USER_ID",
                "MOVE_TYPE": "D",
                "PROCESSED_DTTM": "2023-07-12T09:56:35.500000",
                "IS_PK": "1",
                "IS_DIFF": "-1",
                "TGTVAR_TYPE": "C",
                "OLDVAL_NUM": ".",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "secretuser",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095617278_667889_1859",
                "LIBREF": "DC406039",
                "DSN": "MPE_USERS",
                "KEY_HASH": "20EB1B22A92B5C573DC1EB4331FC49EE",
                "TGTVAR_NM": "LAST_SEEN_DT",
                "MOVE_TYPE": "D",
                "PROCESSED_DTTM": "2023-07-12T09:56:35.500000",
                "IS_PK": "0",
                "IS_DIFF": "-1",
                "TGTVAR_TYPE": "N",
                "OLDVAL_NUM": "23203",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            },
            {
                "LOAD_REF": "DC20230712T095617278_667889_1859",
                "LIBREF": "DC406039",
                "DSN": "MPE_USERS",
                "KEY_HASH": "20EB1B22A92B5C573DC1EB4331FC49EE",
                "TGTVAR_NM": "REGISTERED_DT",
                "MOVE_TYPE": "D",
                "PROCESSED_DTTM": "2023-07-12T09:56:35.500000",
                "IS_PK": "0",
                "IS_DIFF": "-1",
                "TGTVAR_TYPE": "N",
                "OLDVAL_NUM": "23199",
                "NEWVAL_NUM": ".",
                "OLDVAL_CHAR": "",
                "NEWVAL_CHAR": ""
            }
        ],
        "$viewdata": {
            "vars": {
                "LOAD_REF": {
                    "format": "$36.",
                    "label": "unique load reference",
                    "length": "36",
                    "type": "char"
                },
                "LIBREF": {
                    "format": "$8.",
                    "label": "Library Reference (8 chars)",
                    "length": "8",
                    "type": "char"
                },
                "DSN": {
                    "format": "$32.",
                    "label": "Dataset Name (32 chars)",
                    "length": "32",
                    "type": "char"
                },
                "KEY_HASH": {
                    "format": "$32.",
                    "label": "MD5 Hash of primary key values (pipe seperated)",
                    "length": "32",
                    "type": "char"
                },
                "TGTVAR_NM": {
                    "format": "$32.",
                    "label": "Target variable name (32 chars)",
                    "length": "32",
                    "type": "char"
                },
                "MOVE_TYPE": {
                    "format": "$1.",
                    "label": "Either (A)ppended, (D)eleted or (M)odified",
                    "length": "1",
                    "type": "char"
                },
                "PROCESSED_DTTM": {
                    "format": "E8601DT26.6",
                    "label": "Processed at timestamp",
                    "length": "8",
                    "type": "num"
                },
                "IS_PK": {
                    "format": "best.",
                    "label": "Is Primary Key Field? (1/0)",
                    "length": "8",
                    "type": "num"
                },
                "IS_DIFF": {
                    "format": "best.",
                    "label": "Did value change? (1/0/-1).  Always -1 for appends and deletes.",
                    "length": "8",
                    "type": "num"
                },
                "TGTVAR_TYPE": {
                    "format": "$1.",
                    "label": "Either (C)haracter or (N)umeric",
                    "length": "1",
                    "type": "char"
                },
                "OLDVAL_NUM": {
                    "format": "BEST32.",
                    "label": "Old (numeric) value",
                    "length": "8",
                    "type": "num"
                },
                "NEWVAL_NUM": {
                    "format": "BEST32.",
                    "label": "New (numeric) value",
                    "length": "8",
                    "type": "num"
                },
                "OLDVAL_CHAR": {
                    "format": "$32765.",
                    "label": "Old (character) value",
                    "length": "32765",
                    "type": "char"
                },
                "NEWVAL_CHAR": {
                    "format": "$32765.",
                    "label": "New (character) value",
                    "length": "32765",
                    "type": "char"
                }
            }
        },
        "_DEBUG": "",
        "_PROGRAM": "/Public/app/dc/services/public/viewdata",
        "AUTOEXEC": "some.sas",
        "MF_GETUSER": "secretuser",
        "SYSCC": "0",
        "SYSENCODING": "utf-8",
        "SYSERRORTEXT": "",
        "SYSHOSTINFOLONG": "os",
        "SYSHOSTNAME": "sys",
        "SYSPROCESSID": "1111",
        "SYSPROCESSMODE": "SAS Batch Mode",
        "SYSPROCESSNAME": "",
        "SYSJOBID": "123",
        "SYSSCPL": "Linux",
        "SYSSITE": "123",
        "SYSTCPIPHOSTNAME": "sys",
        "SYSUSERID": "sasjssrv",
        "SYSVLONG": "",
        "SYSWARNINGTEXT": "ENCODING option ignored for files opened with RECFM=N.",
        "END_DTTM": "2023-07-17T17:53:35.282581",
        "MEMSIZE": "2GB"
    }`,
    MPE_ALERTS: `{"SYSDATE" : "18JUL23"
    ,"SYSTIME" : "09:22"
    , "cls_rules":
    [
    ]
    , "cols":
    [
    {"NAME":"ALERT_DS" ,"LENGTH":32 ,"VARNUM":4 ,"LABEL":"ALERT_DS" ,"FMTNAME":"" ,"FORMAT":"$32." ,"TYPE":"C" ,"DDTYPE":"CHARACTER" }
    ,{"NAME":"ALERT_EVENT" ,"LENGTH":20 ,"VARNUM":2 ,"LABEL":"ALERT_EVENT" ,"FMTNAME":"" ,"FORMAT":"$20." ,"TYPE":"C" ,"DDTYPE":"CHARACTER" }
    ,{"NAME":"ALERT_LIB" ,"LENGTH":8 ,"VARNUM":3 ,"LABEL":"ALERT_LIB" ,"FMTNAME":"" ,"FORMAT":"$8." ,"TYPE":"C" ,"DDTYPE":"CHARACTER" }
    ,{"NAME":"ALERT_USER" ,"LENGTH":100 ,"VARNUM":5 ,"LABEL":"ALERT_USER" ,"FMTNAME":"" ,"FORMAT":"$100." ,"TYPE":"C" ,"DDTYPE":"CHARACTER" }
    ,{"NAME":"TX_FROM" ,"LENGTH":8 ,"VARNUM":1 ,"LABEL":"TX_FROM" ,"FMTNAME":"DATETIME" ,"FORMAT":"DATETIME19.3" ,"TYPE":"N" ,"DDTYPE":"DATETIME" }
    ,{"NAME":"TX_TO" ,"LENGTH":8 ,"VARNUM":6 ,"LABEL":"TX_TO" ,"FMTNAME":"DATETIME" ,"FORMAT":"DATETIME19.3" ,"TYPE":"N" ,"DDTYPE":"DATETIME" }
    ]
    , "dsmeta":
    [
    {"ODS_TABLE":"ATTRIBUTES" ,"NAME":"Data Set Name" ,"VALUE":"DC406039.MPE_ALERTS" }
    ,{"ODS_TABLE":"ATTRIBUTES" ,"NAME":"Observations" ,"VALUE":"1" }
    ,{"ODS_TABLE":"ATTRIBUTES" ,"NAME":"Member Type" ,"VALUE":"DATA" }
    ,{"ODS_TABLE":"ATTRIBUTES" ,"NAME":"Variables" ,"VALUE":"6" }
    ,{"ODS_TABLE":"ATTRIBUTES" ,"NAME":"Engine" ,"VALUE":"V9" }
    ,{"ODS_TABLE":"ATTRIBUTES" ,"NAME":"Indexes" ,"VALUE":"1" }
    ,{"ODS_TABLE":"ATTRIBUTES" ,"NAME":"Created" ,"VALUE":"07/07/2023 15:19:25" }
    ,{"ODS_TABLE":"ATTRIBUTES" ,"NAME":"Integrity Constraints" ,"VALUE":"1" }
    ,{"ODS_TABLE":"ATTRIBUTES" ,"NAME":"Last Modified" ,"VALUE":"07/07/2023 15:19:25" }
    ,{"ODS_TABLE":"ATTRIBUTES" ,"NAME":"Observation Length" ,"VALUE":"176" }
    ,{"ODS_TABLE":"ATTRIBUTES" ,"NAME":"Protection" ,"VALUE":"." }
    ,{"ODS_TABLE":"ATTRIBUTES" ,"NAME":"Deleted Observations" ,"VALUE":"0" }
    ,{"ODS_TABLE":"ATTRIBUTES" ,"NAME":"Data Set Type" ,"VALUE":"." }
    ,{"ODS_TABLE":"ATTRIBUTES" ,"NAME":"Compressed" ,"VALUE":"NO" }
    ,{"ODS_TABLE":"ATTRIBUTES" ,"NAME":"Label" ,"VALUE":"." }
    ,{"ODS_TABLE":"ATTRIBUTES" ,"NAME":"Sorted" ,"VALUE":"NO" }
    ,{"ODS_TABLE":"ATTRIBUTES" ,"NAME":"Data Representation" ,"VALUE":"SOLARIS_X86_64, LINUX_X86_64, ALPHA_TRU64, LINUX_IA64" }
    ,{"ODS_TABLE":"ATTRIBUTES" ,"NAME":"Encoding" ,"VALUE":"utf-8  Unicode (UTF-8)" }
    ,{"ODS_TABLE":"ENGINEHOST" ,"NAME":"Data Set Page Size" ,"VALUE":"65536" }
    ,{"ODS_TABLE":"ENGINEHOST" ,"NAME":"Number of Data Set Pages" ,"VALUE":"2" }
    ,{"ODS_TABLE":"ENGINEHOST" ,"NAME":"First Data Page" ,"VALUE":"1" }
    ,{"ODS_TABLE":"ENGINEHOST" ,"NAME":"Max Obs per Page" ,"VALUE":"371" }
    ,{"ODS_TABLE":"ENGINEHOST" ,"NAME":"Obs in First Data Page" ,"VALUE":"1" }
    ,{"ODS_TABLE":"ENGINEHOST" ,"NAME":"Index File Page Size" ,"VALUE":"8192" }
    ,{"ODS_TABLE":"ENGINEHOST" ,"NAME":"Number of Index File Pages" ,"VALUE":"2" }
    ,{"ODS_TABLE":"ENGINEHOST" ,"NAME":"Number of Data Set Repairs" ,"VALUE":"0" }
    ,{"ODS_TABLE":"ENGINEHOST" ,"NAME":"Filename" ,"VALUE":"/tmp/mihajlo/DC406039/mpe_alerts.sas7bdat" }
    ,{"ODS_TABLE":"ENGINEHOST" ,"NAME":"Release Created" ,"VALUE":"9.0401M7" }
    ,{"ODS_TABLE":"ENGINEHOST" ,"NAME":"Host Created" ,"VALUE":"Linux" }
    ,{"ODS_TABLE":"ENGINEHOST" ,"NAME":"Inode Number" ,"VALUE":"21373250" }
    ,{"ODS_TABLE":"ENGINEHOST" ,"NAME":"Access Permission" ,"VALUE":"rw-rw-r--" }
    ,{"ODS_TABLE":"ENGINEHOST" ,"NAME":"Owner Name" ,"VALUE":"sasjssrv" }
    ,{"ODS_TABLE":"ENGINEHOST" ,"NAME":"File Size" ,"VALUE":"192KB" }
    ,{"ODS_TABLE":"ENGINEHOST" ,"NAME":"File Size (bytes)" ,"VALUE":"196608" }
    ]
    , "query":
    [
    ]
    , "sasparams":
    [
    {"TABLEURI":"" ,"TABLENAME":"MPE_ALERTS" ,"FILTER_TEXT":"${filterText.replace(/\"/gmi, '\\"')}" ,"PK_FIELDS":"TX_FROM ALERT_EVENT ALERT_LIB ALERT_DS ALERT_USER" ,"NOBS":1 ,"VARS":6 ,"MAXROWS":250 }
    ]
    , "viewdata":
    [
    {"TX_FROM":"01JAN60:00:00:00.00" ,"ALERT_EVENT":"*ALL*" ,"ALERT_LIB":"*ALL*" ,"ALERT_DS":"*ALL*" ,"ALERT_USER":"sasjssrv" ,"TX_TO":"31DEC99:23:59:59.00" }
    ]
    , "$viewdata":{"vars":{
    "TX_FROM" :{"format":"DATETIME19.3" ,"label":"TX_FROM" ,"length":"8" ,"type":"num" }
    ,"ALERT_EVENT" :{"format":"$20." ,"label":"ALERT_EVENT" ,"length":"20" ,"type":"char" }
    ,"ALERT_LIB" :{"format":"$8." ,"label":"ALERT_LIB" ,"length":"8" ,"type":"char" }
    ,"ALERT_DS" :{"format":"$32." ,"label":"ALERT_DS" ,"length":"32" ,"type":"char" }
    ,"ALERT_USER" :{"format":"$100." ,"label":"ALERT_USER" ,"length":"100" ,"type":"char" }
    ,"TX_TO" :{"format":"DATETIME19.3" ,"label":"TX_TO" ,"length":"8" ,"type":"num" }
    }}
    ,"_DEBUG" : ""
    ,"_PROGRAM" : "/Public/app/dc/services/public/viewdata"
    ,"AUTOEXEC" : "%2Fhome%2Fsasjssrv%2Fsasjs_root%2Fsessions%2F20230718072243-44039-1689664963717%2Fautoexec.sas"
    ,"MF_GETUSER" : "secretuser"
    ,"SYSCC" : "0"
    ,"SYSENCODING" : "utf-8"
    ,"SYSERRORTEXT" : ""
    ,"SYSHOSTINFOLONG" : "os"
    ,"SYSHOSTNAME" : "os"
    ,"SYSPROCESSID" : "111"
    ,"SYSPROCESSMODE" : "SAS Batch Mode"
    ,"SYSPROCESSNAME" : ""
    ,"SYSJOBID" : "11902"
    ,"SYSSCPL" : "Linux"
    ,"SYSSITE" : "123"
    ,"SYSTCPIPHOSTNAME" : "os"
    ,"SYSUSERID" : "sasjssrv"
    ,"SYSVLONG" : "9.04.01M7P080520"
    ,"SYSWARNINGTEXT" : "ENCODING option ignored for files opened with RECFM=N."
    ,"END_DTTM" : "2023-07-18T09:22:52.205170"
    ,"MEMSIZE" : "2GB"
    }`,
    MPE_VALIDATIONS: `{
        "SYSDATE": "18JUL23",
        "SYSTIME": "09:22",
        "cls_rules": [
        ],
        "cols": [
            {
                "NAME": "BASE_COL",
                "LENGTH": 32,
                "VARNUM": 4,
                "LABEL": "BASE_COL",
                "FMTNAME": "",
                "FORMAT": "$32.",
                "TYPE": "C",
                "DDTYPE": "CHARACTER"
            },
            {
                "NAME": "BASE_DS",
                "LENGTH": 32,
                "VARNUM": 3,
                "LABEL": "BASE_DS",
                "FMTNAME": "",
                "FORMAT": "$32.",
                "TYPE": "C",
                "DDTYPE": "CHARACTER"
            },
            {
                "NAME": "BASE_LIB",
                "LENGTH": 8,
                "VARNUM": 2,
                "LABEL": "BASE_LIB",
                "FMTNAME": "",
                "FORMAT": "$8.",
                "TYPE": "C",
                "DDTYPE": "CHARACTER"
            },
            {
                "NAME": "RULE_ACTIVE",
                "LENGTH": 8,
                "VARNUM": 7,
                "LABEL": "RULE_ACTIVE",
                "FMTNAME": "",
                "FORMAT": "8.",
                "TYPE": "N",
                "DDTYPE": "NUMERIC"
            },
            {
                "NAME": "RULE_TYPE",
                "LENGTH": 32,
                "VARNUM": 5,
                "LABEL": "RULE_TYPE",
                "FMTNAME": "",
                "FORMAT": "$32.",
                "TYPE": "C",
                "DDTYPE": "CHARACTER"
            },
            {
                "NAME": "RULE_VALUE",
                "LENGTH": 128,
                "VARNUM": 6,
                "LABEL": "RULE_VALUE",
                "FMTNAME": "",
                "FORMAT": "$128.",
                "TYPE": "C",
                "DDTYPE": "CHARACTER"
            },
            {
                "NAME": "TX_FROM",
                "LENGTH": 8,
                "VARNUM": 1,
                "LABEL": "TX_FROM",
                "FMTNAME": "DATETIME",
                "FORMAT": "DATETIME19.3",
                "TYPE": "N",
                "DDTYPE": "DATETIME"
            },
            {
                "NAME": "TX_TO",
                "LENGTH": 8,
                "VARNUM": 8,
                "LABEL": "TX_TO",
                "FMTNAME": "DATETIME",
                "FORMAT": "DATETIME19.3",
                "TYPE": "N",
                "DDTYPE": "DATETIME"
            }
        ],
        "dsmeta": [
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Data Set Name",
                "VALUE": "DC406039.MPE_VALIDATIONS"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Observations",
                "VALUE": "52"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Member Type",
                "VALUE": "DATA"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Variables",
                "VALUE": "8"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Engine",
                "VALUE": "V9"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Indexes",
                "VALUE": "1"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Created",
                "VALUE": "07/07/2023 15:19:25"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Integrity Constraints",
                "VALUE": "2"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Last Modified",
                "VALUE": "07/07/2023 15:19:25"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Observation Length",
                "VALUE": "256"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Protection",
                "VALUE": "."
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Deleted Observations",
                "VALUE": "0"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Data Set Type",
                "VALUE": "."
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Compressed",
                "VALUE": "NO"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Label",
                "VALUE": "."
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Sorted",
                "VALUE": "NO"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Data Representation",
                "VALUE": "SOLARIS_X86_64, LINUX_X86_64, ALPHA_TRU64, LINUX_IA64"
            },
            {
                "ODS_TABLE": "ATTRIBUTES",
                "NAME": "Encoding",
                "VALUE": "utf-8  Unicode (UTF-8)"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Data Set Page Size",
                "VALUE": "65536"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Number of Data Set Pages",
                "VALUE": "2"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "First Data Page",
                "VALUE": "1"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Max Obs per Page",
                "VALUE": "255"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Obs in First Data Page",
                "VALUE": "52"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Index File Page Size",
                "VALUE": "8192"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Number of Index File Pages",
                "VALUE": "2"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Number of Data Set Repairs",
                "VALUE": "0"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Filename",
                "VALUE": "/tmp/mihajlo/DC406039/mpe_validations.sas7bdat"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Release Created",
                "VALUE": "9.0401M7"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Host Created",
                "VALUE": "Linux"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Inode Number",
                "VALUE": "21373309"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Access Permission",
                "VALUE": "rw-rw-r--"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "Owner Name",
                "VALUE": "sasjssrv"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "File Size",
                "VALUE": "192KB"
            },
            {
                "ODS_TABLE": "ENGINEHOST",
                "NAME": "File Size (bytes)",
                "VALUE": "196608"
            }
        ],
        "query": [
        ],
        "sasparams": [
            {
                "TABLEURI": "",
                "TABLENAME": "MPE_VALIDATIONS",
                "FILTER_TEXT": "${filterText.replace(/\"/gmi, '\\"')}",
                "PK_FIELDS": "TX_FROM BASE_LIB BASE_DS BASE_COL RULE_TYPE",
                "NOBS": 52,
                "VARS": 8,
                "MAXROWS": 250
            }
        ],
        "viewdata": [
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_COLUMN_LEVEL_SECURITY",
                "BASE_COL": "CLS_SCOPE",
                "RULE_TYPE": "CASE",
                "RULE_VALUE": "UPCASE",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_COLUMN_LEVEL_SECURITY",
                "BASE_COL": "CLS_LIBREF",
                "RULE_TYPE": "CASE",
                "RULE_VALUE": "UPCASE",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_COLUMN_LEVEL_SECURITY",
                "BASE_COL": "CLS_LIBREF",
                "RULE_TYPE": "SOFTSELECT_HOOK",
                "RULE_VALUE": "services/validations/libraries_all",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_COLUMN_LEVEL_SECURITY",
                "BASE_COL": "CLS_TABLE",
                "RULE_TYPE": "CASE",
                "RULE_VALUE": "UPCASE",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_COLUMN_LEVEL_SECURITY",
                "BASE_COL": "CLS_TABLE",
                "RULE_TYPE": "SOFTSELECT_HOOK",
                "RULE_VALUE": "services/validations/tables_all",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_COLUMN_LEVEL_SECURITY",
                "BASE_COL": "CLS_VARIABLE_NM",
                "RULE_TYPE": "CASE",
                "RULE_VALUE": "UPCASE",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_COLUMN_LEVEL_SECURITY",
                "BASE_COL": "CLS_VARIABLE_NM",
                "RULE_TYPE": "SOFTSELECT_HOOK",
                "RULE_VALUE": "services/validations/columns_in_libds",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_COLUMN_LEVEL_SECURITY",
                "BASE_COL": "CLS_ACTIVE",
                "RULE_TYPE": "MAXVAL",
                "RULE_VALUE": "1",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_COLUMN_LEVEL_SECURITY",
                "BASE_COL": "CLS_HIDE",
                "RULE_TYPE": "MAXVAL",
                "RULE_VALUE": "1",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_COLUMN_LEVEL_SECURITY",
                "BASE_COL": "CLS_GROUP",
                "RULE_TYPE": "SOFTSELECT_HOOK",
                "RULE_VALUE": "services/validations/sas_groups",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_ALERTS",
                "BASE_COL": "ALERT_LIB",
                "RULE_TYPE": "HARDSELECT_HOOK",
                "RULE_VALUE": "services/validations/mpe_alerts.alert_lib",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_TABLES",
                "BASE_COL": "LIBREF",
                "RULE_TYPE": "CASE",
                "RULE_VALUE": "UPCASE",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_TABLES",
                "BASE_COL": "DSN",
                "RULE_TYPE": "CASE",
                "RULE_VALUE": "UPCASE",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_TABLES",
                "BASE_COL": "LIBREF",
                "RULE_TYPE": "NOTNULL",
                "RULE_VALUE": "",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_TABLES",
                "BASE_COL": "DSN",
                "RULE_TYPE": "NOTNULL",
                "RULE_VALUE": "",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_TABLES",
                "BASE_COL": "NUM_OF_APPROVALS_REQUIRED",
                "RULE_TYPE": "MINVAL",
                "RULE_VALUE": "1",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_TABLES",
                "BASE_COL": "BUSKEY",
                "RULE_TYPE": "CASE",
                "RULE_VALUE": "UPCASE",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_TABLES",
                "BASE_COL": "BUSKEY",
                "RULE_TYPE": "NOTNULL",
                "RULE_VALUE": "",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_TABLES",
                "BASE_COL": "VAR_TXFROM",
                "RULE_TYPE": "CASE",
                "RULE_VALUE": "UPCASE",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_TABLES",
                "BASE_COL": "VAR_TXTO",
                "RULE_TYPE": "CASE",
                "RULE_VALUE": "UPCASE",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_TABLES",
                "BASE_COL": "VAR_BUSFROM",
                "RULE_TYPE": "CASE",
                "RULE_VALUE": "UPCASE",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_TABLES",
                "BASE_COL": "VAR_BUSTO",
                "RULE_TYPE": "CASE",
                "RULE_VALUE": "UPCASE",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_TABLES",
                "BASE_COL": "VAR_PROCESSED",
                "RULE_TYPE": "CASE",
                "RULE_VALUE": "UPCASE",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_SECURITY",
                "BASE_COL": "LIBREF",
                "RULE_TYPE": "HARDSELECT",
                "RULE_VALUE": "DC406039.MPE_TABLES.LIBREF",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_SECURITY",
                "BASE_COL": "DSN",
                "RULE_TYPE": "SOFTSELECT",
                "RULE_VALUE": "DC406039.MPE_TABLES.DSN",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_SECURITY",
                "BASE_COL": "SAS_GROUP",
                "RULE_TYPE": "SOFTSELECT_HOOK",
                "RULE_VALUE": "services/validations/sas_groups",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_VALIDATIONS",
                "BASE_COL": "BASE_LIB",
                "RULE_TYPE": "SOFTSELECT_HOOK",
                "RULE_VALUE": "services/validations/libraries_editable",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_VALIDATIONS",
                "BASE_COL": "BASE_DS",
                "RULE_TYPE": "SOFTSELECT_HOOK",
                "RULE_VALUE": "services/validations/tables_editable",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_VALIDATIONS",
                "BASE_COL": "BASE_COL",
                "RULE_TYPE": "SOFTSELECT_HOOK",
                "RULE_VALUE": "services/validations/columns_in_libds",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_VALIDATIONS",
                "BASE_COL": "RULE_ACTIVE",
                "RULE_TYPE": "MINVAL",
                "RULE_VALUE": "0",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_VALIDATIONS",
                "BASE_COL": "RULE_ACTIVE",
                "RULE_TYPE": "MAXVAL",
                "RULE_VALUE": "1",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_EXCEL_CONFIG",
                "BASE_COL": "XL_LIBREF",
                "RULE_TYPE": "SOFTSELECT_HOOK",
                "RULE_VALUE": "services/validations/libraries_editable",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_EXCEL_CONFIG",
                "BASE_COL": "XL_TABLE",
                "RULE_TYPE": "SOFTSELECT_HOOK",
                "RULE_VALUE": "services/validations/tables_editable",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_EXCEL_CONFIG",
                "BASE_COL": "XL_COLUMN",
                "RULE_TYPE": "SOFTSELECT_HOOK",
                "RULE_VALUE": "services/validations/columns_in_libds",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_TABLES",
                "BASE_COL": "LIBREF",
                "RULE_TYPE": "SOFTSELECT_HOOK",
                "RULE_VALUE": "services/validations/libraries_all",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_TABLES",
                "BASE_COL": "DSN",
                "RULE_TYPE": "SOFTSELECT_HOOK",
                "RULE_VALUE": "services/validations/mpe_tables.dsn",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_TABLES",
                "BASE_COL": "VAR_TXFROM",
                "RULE_TYPE": "SOFTSELECT_HOOK",
                "RULE_VALUE": "services/validations/columns_in_libds",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_TABLES",
                "BASE_COL": "VAR_TXTO",
                "RULE_TYPE": "SOFTSELECT_HOOK",
                "RULE_VALUE": "services/validations/columns_in_libds",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_TABLES",
                "BASE_COL": "VAR_BUSFROM",
                "RULE_TYPE": "SOFTSELECT_HOOK",
                "RULE_VALUE": "services/validations/columns_in_libds",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_TABLES",
                "BASE_COL": "VAR_BUSTO",
                "RULE_TYPE": "SOFTSELECT_HOOK",
                "RULE_VALUE": "services/validations/columns_in_libds",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_TABLES",
                "BASE_COL": "VAR_PROCESSED",
                "RULE_TYPE": "SOFTSELECT_HOOK",
                "RULE_VALUE": "services/validations/columns_in_libds",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_SELECTBOX",
                "BASE_COL": "SELECT_LIB",
                "RULE_TYPE": "SOFTSELECT_HOOK",
                "RULE_VALUE": "services/validations/libraries_editable",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_SELECTBOX",
                "BASE_COL": "SELECT_DS",
                "RULE_TYPE": "SOFTSELECT_HOOK",
                "RULE_VALUE": "services/validations/tables_editable",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_SELECTBOX",
                "BASE_COL": "BASE_COLUMN",
                "RULE_TYPE": "SOFTSELECT_HOOK",
                "RULE_VALUE": "services/validations/columns_in_libds",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_ROW_LEVEL_SECURITY",
                "BASE_COL": "RLS_GROUP",
                "RULE_TYPE": "SOFTSELECT_HOOK",
                "RULE_VALUE": "services/validations/sas_groups",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_ROW_LEVEL_SECURITY",
                "BASE_COL": "RLS_LIBREF",
                "RULE_TYPE": "SOFTSELECT_HOOK",
                "RULE_VALUE": "services/validations/libraries_all",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_ROW_LEVEL_SECURITY",
                "BASE_COL": "RLS_TABLE",
                "RULE_TYPE": "SOFTSELECT_HOOK",
                "RULE_VALUE": "services/validations/tables_all",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_ROW_LEVEL_SECURITY",
                "BASE_COL": "RLS_SUBGROUP_ID",
                "RULE_TYPE": "MINVAL",
                "RULE_VALUE": "0",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_ROW_LEVEL_SECURITY",
                "BASE_COL": "RLS_VARIABLE_NM",
                "RULE_TYPE": "SOFTSELECT_HOOK",
                "RULE_VALUE": "services/validations/columns_in_libds",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_X_TEST",
                "BASE_COL": "SOME_NUM",
                "RULE_TYPE": "HARDSELECT_HOOK",
                "RULE_VALUE": "services/validations/mpe_x_test.some_num",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_EXCEL_CONFIG",
                "BASE_COL": "XL_ACTIVE",
                "RULE_TYPE": "MINVAL",
                "RULE_VALUE": "0",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            },
            {
                "TX_FROM": "01JAN60:00:00:00.00",
                "BASE_LIB": "DC406039",
                "BASE_DS": "MPE_EXCEL_CONFIG",
                "BASE_COL": "XL_ACTIVE",
                "RULE_TYPE": "MAXVAL",
                "RULE_VALUE": "1",
                "RULE_ACTIVE": "1",
                "TX_TO": "31DEC99:23:59:59.00"
            }
        ],
        "$viewdata": {
            "vars": {
                "TX_FROM": {
                    "format": "DATETIME19.3",
                    "label": "TX_FROM",
                    "length": "8",
                    "type": "num"
                },
                "BASE_LIB": {
                    "format": "$8.",
                    "label": "BASE_LIB",
                    "length": "8",
                    "type": "char"
                },
                "BASE_DS": {
                    "format": "$32.",
                    "label": "BASE_DS",
                    "length": "32",
                    "type": "char"
                },
                "BASE_COL": {
                    "format": "$32.",
                    "label": "BASE_COL",
                    "length": "32",
                    "type": "char"
                },
                "RULE_TYPE": {
                    "format": "$32.",
                    "label": "RULE_TYPE",
                    "length": "32",
                    "type": "char"
                },
                "RULE_VALUE": {
                    "format": "$128.",
                    "label": "RULE_VALUE",
                    "length": "128",
                    "type": "char"
                },
                "RULE_ACTIVE": {
                    "format": "best.",
                    "label": "RULE_ACTIVE",
                    "length": "8",
                    "type": "num"
                },
                "TX_TO": {
                    "format": "DATETIME19.3",
                    "label": "TX_TO",
                    "length": "8",
                    "type": "num"
                }
            }
        },
        "_DEBUG": "",
        "_PROGRAM": "/Public/app/dc/services/public/viewdata",
        "AUTOEXEC": "%2Fhome%2Fsasjssrv%2Fsasjs_root%2Fsessions%2F20230718072250-60048-1689664970690%2Fautoexec.sas",
        "MF_GETUSER": "secretuser",
        "SYSCC": "0",
        "SYSENCODING": "utf-8",
        "SYSERRORTEXT": "",
        "SYSHOSTINFOLONG": "os",
        "SYSHOSTNAME": "os",
        "SYSPROCESSID": "111",
        "SYSPROCESSMODE": "SAS Batch Mode",
        "SYSPROCESSNAME": "",
        "SYSJOBID": "12051",
        "SYSSCPL": "Linux",
        "SYSSITE": "123",
        "SYSTCPIPHOSTNAME": "os",
        "SYSUSERID": "sasjssrv",
        "SYSVLONG": "os",
        "SYSWARNINGTEXT": "ENCODING option ignored for files opened with RECFM=N.",
        "END_DTTM": "2023-07-18T09:27:47.018981",
        "MEMSIZE": "2GB"
    }`
}

let table = 'MPE_X_TEST'

if (_WEBIN_FILEREF1) {
    const file1 = _WEBIN_FILEREF1.toString()

    if (file1.includes('MPE_X_TEST')) {
        table = 'MPE_X_TEST'
    } else if (file1.includes('MPE_AUDIT')) {
        table = 'MPE_AUDIT'
    } else if (file1.includes('MPE_ALERTS')) {
        table = 'MPE_ALERTS'
    } else if (file1.includes('MPE_VALIDATIONS')) {
        table = 'MPE_VALIDATIONS'
    }
}

_webout = webouts[table]