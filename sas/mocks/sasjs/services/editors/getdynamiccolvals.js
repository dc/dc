let mpe_tables = `{"SYSDATE" : "10MAR23"
,"SYSTIME" : "12:56"
, "dynamic_values":
[
[1 ,"MPE_ALERTS" ,"MPE_ALERTS" ]
,[2 ,"MPE_AUDIT" ,"MPE_AUDIT" ]
,[3 ,"MPE_COLUMN_LEVEL_SECURITY" ,"MPE_COLUMN_LEVEL_SECURITY" ]
,[4 ,"MPE_CONFIG" ,"MPE_CONFIG" ]
,[5 ,"MPE_DATACATALOG_LIBS" ,"MPE_DATACATALOG_LIBS" ]
,[6 ,"MPE_DATACATALOG_TABS" ,"MPE_DATACATALOG_TABS" ]
,[7 ,"MPE_DATACATALOG_VARS" ,"MPE_DATACATALOG_VARS" ]
,[8 ,"MPE_DATADICTIONARY" ,"MPE_DATADICTIONARY" ]
,[9 ,"MPE_DATALOADS" ,"MPE_DATALOADS" ]
,[10 ,"MPE_DATASTATUS_LIBS" ,"MPE_DATASTATUS_LIBS" ]
,[11 ,"MPE_DATASTATUS_TABS" ,"MPE_DATASTATUS_TABS" ]
,[12 ,"MPE_EMAILS" ,"MPE_EMAILS" ]
,[13 ,"MPE_EXCEL_CONFIG" ,"MPE_EXCEL_CONFIG" ]
,[14 ,"MPE_FILTERANYTABLE" ,"MPE_FILTERANYTABLE" ]
,[15 ,"MPE_FILTERSOURCE" ,"MPE_FILTERSOURCE" ]
,[16 ,"MPE_GROUPS" ,"MPE_GROUPS" ]
,[17 ,"MPE_LINEAGE_COLS" ,"MPE_LINEAGE_COLS" ]
,[18 ,"MPE_LINEAGE_TABS" ,"MPE_LINEAGE_TABS" ]
,[19 ,"MPE_LOADS" ,"MPE_LOADS" ]
,[20 ,"MPE_LOCKANYTABLE" ,"MPE_LOCKANYTABLE" ]
,[21 ,"MPE_MAXKEYVALUES" ,"MPE_MAXKEYVALUES" ]
,[22 ,"MPE_REQUESTS" ,"MPE_REQUESTS" ]
,[23 ,"MPE_REVIEW" ,"MPE_REVIEW" ]
,[24 ,"MPE_ROW_LEVEL_SECURITY" ,"MPE_ROW_LEVEL_SECURITY" ]
,[25 ,"MPE_SECURITY" ,"MPE_SECURITY" ]
,[26 ,"MPE_SELECTBOX" ,"MPE_SELECTBOX" ]
,[27 ,"MPE_SIGNOFFS" ,"MPE_SIGNOFFS" ]
,[28 ,"MPE_SUBMIT" ,"MPE_SUBMIT" ]
,[29 ,"MPE_TABLES" ,"MPE_TABLES" ]
,[30 ,"MPE_USERS" ,"MPE_USERS" ]
,[31 ,"MPE_VALIDATIONS" ,"MPE_VALIDATIONS" ]
,[32 ,"MPE_X_TEST" ,"MPE_X_TEST" ]
]
, "dynamic_extended_values":
[
[1 ,"VAR_BUSFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[1 ,"VAR_BUSFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[2 ,"VAR_BUSFROM" ,"IS_DIFF" ,"C" ,null ,"IS_DIFF" ,0 ]
,[2 ,"VAR_BUSFROM" ,"IS_PK" ,"C" ,null ,"IS_PK" ,0 ]
,[2 ,"VAR_BUSFROM" ,"NEWVAL_NUM" ,"C" ,null ,"NEWVAL_NUM" ,0 ]
,[2 ,"VAR_BUSFROM" ,"OLDVAL_NUM" ,"C" ,null ,"OLDVAL_NUM" ,0 ]
,[2 ,"VAR_BUSFROM" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[3 ,"VAR_BUSFROM" ,"CLS_ACTIVE" ,"C" ,null ,"CLS_ACTIVE" ,0 ]
,[3 ,"VAR_BUSFROM" ,"CLS_HIDE" ,"C" ,null ,"CLS_HIDE" ,0 ]
,[3 ,"VAR_BUSFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[3 ,"VAR_BUSFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[4 ,"VAR_BUSFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[4 ,"VAR_BUSFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[4 ,"VAR_BUSFROM" ,"VAR_ACTIVE" ,"C" ,null ,"VAR_ACTIVE" ,0 ]
,[5 ,"VAR_BUSFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[5 ,"VAR_BUSFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[6 ,"VAR_BUSFROM" ,"NVAR" ,"C" ,null ,"NVAR" ,0 ]
,[6 ,"VAR_BUSFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[6 ,"VAR_BUSFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[7 ,"VAR_BUSFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[7 ,"VAR_BUSFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[7 ,"VAR_BUSFROM" ,"PK_IND" ,"C" ,null ,"PK_IND" ,0 ]
,[7 ,"VAR_BUSFROM" ,"VARNUM" ,"C" ,null ,"VARNUM" ,0 ]
,[7 ,"VAR_BUSFROM" ,"LENGTH" ,"C" ,null ,"LENGTH" ,0 ]
,[8 ,"VAR_BUSFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[8 ,"VAR_BUSFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[9 ,"VAR_BUSFROM" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[9 ,"VAR_BUSFROM" ,"DURATION" ,"C" ,null ,"DURATION" ,0 ]
,[9 ,"VAR_BUSFROM" ,"DELETED_RECORDS" ,"C" ,null ,"DELETED_RECORDS" ,0 ]
,[9 ,"VAR_BUSFROM" ,"NEW_RECORDS" ,"C" ,null ,"NEW_RECORDS" ,0 ]
,[9 ,"VAR_BUSFROM" ,"CHANGED_RECORDS" ,"C" ,null ,"CHANGED_RECORDS" ,0 ]
,[10 ,"VAR_BUSFROM" ,"TABLE_CNT" ,"C" ,null ,"TABLE_CNT" ,0 ]
,[10 ,"VAR_BUSFROM" ,"LIBSIZE" ,"C" ,null ,"LIBSIZE" ,0 ]
,[10 ,"VAR_BUSFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[10 ,"VAR_BUSFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[11 ,"VAR_BUSFROM" ,"NOBS" ,"C" ,null ,"NOBS" ,0 ]
,[11 ,"VAR_BUSFROM" ,"MODATE" ,"C" ,null ,"MODATE" ,0 ]
,[11 ,"VAR_BUSFROM" ,"CRDATE" ,"C" ,null ,"CRDATE" ,0 ]
,[11 ,"VAR_BUSFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[11 ,"VAR_BUSFROM" ,"FILESIZE" ,"C" ,null ,"FILESIZE" ,0 ]
,[11 ,"VAR_BUSFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[12 ,"VAR_BUSFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[12 ,"VAR_BUSFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[13 ,"VAR_BUSFROM" ,"XL_ACTIVE" ,"C" ,null ,"XL_ACTIVE" ,0 ]
,[13 ,"VAR_BUSFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[13 ,"VAR_BUSFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[14 ,"VAR_BUSFROM" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[14 ,"VAR_BUSFROM" ,"FILTER_RK" ,"C" ,null ,"FILTER_RK" ,0 ]
,[15 ,"VAR_BUSFROM" ,"SUBGROUP_ID" ,"C" ,null ,"SUBGROUP_ID" ,0 ]
,[15 ,"VAR_BUSFROM" ,"FILTER_LINE" ,"C" ,null ,"FILTER_LINE" ,0 ]
,[15 ,"VAR_BUSFROM" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[16 ,"VAR_BUSFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[16 ,"VAR_BUSFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[17 ,"VAR_BUSFROM" ,"MODIFIED_DTTM" ,"C" ,null ,"MODIFIED_DTTM" ,0 ]
,[17 ,"VAR_BUSFROM" ,"LEVEL" ,"C" ,null ,"LEVEL" ,0 ]
,[18 ,"VAR_BUSFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[18 ,"VAR_BUSFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[19 ,"VAR_BUSFROM" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[19 ,"VAR_BUSFROM" ,"DURATION" ,"C" ,null ,"DURATION" ,0 ]
,[20 ,"VAR_BUSFROM" ,"LOCK_END_DTTM" ,"C" ,null ,"LOCK_END_DTTM" ,0 ]
,[20 ,"VAR_BUSFROM" ,"LOCK_START_DTTM" ,"C" ,null ,"LOCK_START_DTTM" ,0 ]
,[21 ,"VAR_BUSFROM" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[21 ,"VAR_BUSFROM" ,"MAX_KEY" ,"C" ,null ,"MAX_KEY" ,0 ]
,[22 ,"VAR_BUSFROM" ,"REQUEST_DTTM" ,"C" ,null ,"REQUEST_DTTM" ,0 ]
,[23 ,"VAR_BUSFROM" ,"REVIEWED_ON_DTTM" ,"C" ,null ,"REVIEWED_ON_DTTM" ,0 ]
,[24 ,"VAR_BUSFROM" ,"RLS_ACTIVE" ,"C" ,null ,"RLS_ACTIVE" ,0 ]
,[24 ,"VAR_BUSFROM" ,"RLS_SUBGROUP_ID" ,"C" ,null ,"RLS_SUBGROUP_ID" ,0 ]
,[24 ,"VAR_BUSFROM" ,"RLS_RK" ,"C" ,null ,"RLS_RK" ,0 ]
,[24 ,"VAR_BUSFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[24 ,"VAR_BUSFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[25 ,"VAR_BUSFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[25 ,"VAR_BUSFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[26 ,"VAR_BUSFROM" ,"SELECTBOX_ORDER" ,"C" ,null ,"SELECTBOX_ORDER" ,0 ]
,[26 ,"VAR_BUSFROM" ,"SELECTBOX_RK" ,"C" ,null ,"SELECTBOX_RK" ,0 ]
,[26 ,"VAR_BUSFROM" ,"VER_TO_DTTM" ,"C" ,null ,"VER_TO_DTTM" ,0 ]
,[26 ,"VAR_BUSFROM" ,"VER_FROM_DTTM" ,"C" ,null ,"VER_FROM_DTTM" ,0 ]
,[27 ,"VAR_BUSFROM" ,"SIGNOFF_VERSION_RK" ,"C" ,null ,"SIGNOFF_VERSION_RK" ,0 ]
,[27 ,"VAR_BUSFROM" ,"SIGNOFF_SECTION_RK" ,"C" ,null ,"SIGNOFF_SECTION_RK" ,0 ]
,[27 ,"VAR_BUSFROM" ,"TECH_TO_DTTM" ,"C" ,null ,"TECH_TO_DTTM" ,0 ]
,[27 ,"VAR_BUSFROM" ,"TECH_FROM_DTTM" ,"C" ,null ,"TECH_FROM_DTTM" ,0 ]
,[28 ,"VAR_BUSFROM" ,"REVIEWED_ON_DTTM" ,"C" ,null ,"REVIEWED_ON_DTTM" ,0 ]
,[28 ,"VAR_BUSFROM" ,"NUM_OF_APPROVALS_REMAINING" ,"C" ,null ,"NUM_OF_APPROVALS_REMAINING" ,0 ]
,[28 ,"VAR_BUSFROM" ,"NUM_OF_APPROVALS_REQUIRED" ,"C" ,null ,"NUM_OF_APPROVALS_REQUIRED" ,0 ]
,[28 ,"VAR_BUSFROM" ,"INPUT_VARS" ,"C" ,null ,"INPUT_VARS" ,0 ]
,[28 ,"VAR_BUSFROM" ,"INPUT_OBS" ,"C" ,null ,"INPUT_OBS" ,0 ]
,[28 ,"VAR_BUSFROM" ,"SUBMITTED_ON_DTTM" ,"C" ,null ,"SUBMITTED_ON_DTTM" ,0 ]
,[29 ,"VAR_BUSFROM" ,"NUM_OF_APPROVALS_REQUIRED" ,"C" ,null ,"NUM_OF_APPROVALS_REQUIRED" ,0 ]
,[29 ,"VAR_BUSFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[29 ,"VAR_BUSFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[30 ,"VAR_BUSFROM" ,"REGISTERED_DT" ,"C" ,null ,"REGISTERED_DT" ,0 ]
,[30 ,"VAR_BUSFROM" ,"LAST_SEEN_DT" ,"C" ,null ,"LAST_SEEN_DT" ,0 ]
,[31 ,"VAR_BUSFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[31 ,"VAR_BUSFROM" ,"RULE_ACTIVE" ,"C" ,null ,"RULE_ACTIVE" ,0 ]
,[31 ,"VAR_BUSFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[32 ,"VAR_BUSFROM" ,"SOME_BESTNUM" ,"C" ,null ,"SOME_BESTNUM" ,0 ]
,[32 ,"VAR_BUSFROM" ,"SOME_SHORTNUM" ,"C" ,null ,"SOME_SHORTNUM" ,0 ]
,[32 ,"VAR_BUSFROM" ,"SOME_TIME" ,"C" ,null ,"SOME_TIME" ,0 ]
,[32 ,"VAR_BUSFROM" ,"SOME_DATETIME" ,"C" ,null ,"SOME_DATETIME" ,0 ]
,[32 ,"VAR_BUSFROM" ,"SOME_DATE" ,"C" ,null ,"SOME_DATE" ,0 ]
,[32 ,"VAR_BUSFROM" ,"SOME_NUM" ,"C" ,null ,"SOME_NUM" ,0 ]
,[32 ,"VAR_BUSFROM" ,"PRIMARY_KEY_FIELD" ,"C" ,null ,"PRIMARY_KEY_FIELD" ,0 ]
,[1 ,"VAR_BUSTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[1 ,"VAR_BUSTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[2 ,"VAR_BUSTO" ,"IS_DIFF" ,"C" ,null ,"IS_DIFF" ,0 ]
,[2 ,"VAR_BUSTO" ,"IS_PK" ,"C" ,null ,"IS_PK" ,0 ]
,[2 ,"VAR_BUSTO" ,"NEWVAL_NUM" ,"C" ,null ,"NEWVAL_NUM" ,0 ]
,[2 ,"VAR_BUSTO" ,"OLDVAL_NUM" ,"C" ,null ,"OLDVAL_NUM" ,0 ]
,[2 ,"VAR_BUSTO" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[3 ,"VAR_BUSTO" ,"CLS_ACTIVE" ,"C" ,null ,"CLS_ACTIVE" ,0 ]
,[3 ,"VAR_BUSTO" ,"CLS_HIDE" ,"C" ,null ,"CLS_HIDE" ,0 ]
,[3 ,"VAR_BUSTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[3 ,"VAR_BUSTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[4 ,"VAR_BUSTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[4 ,"VAR_BUSTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[4 ,"VAR_BUSTO" ,"VAR_ACTIVE" ,"C" ,null ,"VAR_ACTIVE" ,0 ]
,[5 ,"VAR_BUSTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[5 ,"VAR_BUSTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[6 ,"VAR_BUSTO" ,"NVAR" ,"C" ,null ,"NVAR" ,0 ]
,[6 ,"VAR_BUSTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[6 ,"VAR_BUSTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[7 ,"VAR_BUSTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[7 ,"VAR_BUSTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[7 ,"VAR_BUSTO" ,"PK_IND" ,"C" ,null ,"PK_IND" ,0 ]
,[7 ,"VAR_BUSTO" ,"VARNUM" ,"C" ,null ,"VARNUM" ,0 ]
,[7 ,"VAR_BUSTO" ,"LENGTH" ,"C" ,null ,"LENGTH" ,0 ]
,[8 ,"VAR_BUSTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[8 ,"VAR_BUSTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[9 ,"VAR_BUSTO" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[9 ,"VAR_BUSTO" ,"DURATION" ,"C" ,null ,"DURATION" ,0 ]
,[9 ,"VAR_BUSTO" ,"DELETED_RECORDS" ,"C" ,null ,"DELETED_RECORDS" ,0 ]
,[9 ,"VAR_BUSTO" ,"NEW_RECORDS" ,"C" ,null ,"NEW_RECORDS" ,0 ]
,[9 ,"VAR_BUSTO" ,"CHANGED_RECORDS" ,"C" ,null ,"CHANGED_RECORDS" ,0 ]
,[10 ,"VAR_BUSTO" ,"TABLE_CNT" ,"C" ,null ,"TABLE_CNT" ,0 ]
,[10 ,"VAR_BUSTO" ,"LIBSIZE" ,"C" ,null ,"LIBSIZE" ,0 ]
,[10 ,"VAR_BUSTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[10 ,"VAR_BUSTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[11 ,"VAR_BUSTO" ,"NOBS" ,"C" ,null ,"NOBS" ,0 ]
,[11 ,"VAR_BUSTO" ,"MODATE" ,"C" ,null ,"MODATE" ,0 ]
,[11 ,"VAR_BUSTO" ,"CRDATE" ,"C" ,null ,"CRDATE" ,0 ]
,[11 ,"VAR_BUSTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[11 ,"VAR_BUSTO" ,"FILESIZE" ,"C" ,null ,"FILESIZE" ,0 ]
,[11 ,"VAR_BUSTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[12 ,"VAR_BUSTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[12 ,"VAR_BUSTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[13 ,"VAR_BUSTO" ,"XL_ACTIVE" ,"C" ,null ,"XL_ACTIVE" ,0 ]
,[13 ,"VAR_BUSTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[13 ,"VAR_BUSTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[14 ,"VAR_BUSTO" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[14 ,"VAR_BUSTO" ,"FILTER_RK" ,"C" ,null ,"FILTER_RK" ,0 ]
,[15 ,"VAR_BUSTO" ,"SUBGROUP_ID" ,"C" ,null ,"SUBGROUP_ID" ,0 ]
,[15 ,"VAR_BUSTO" ,"FILTER_LINE" ,"C" ,null ,"FILTER_LINE" ,0 ]
,[15 ,"VAR_BUSTO" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[16 ,"VAR_BUSTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[16 ,"VAR_BUSTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[17 ,"VAR_BUSTO" ,"MODIFIED_DTTM" ,"C" ,null ,"MODIFIED_DTTM" ,0 ]
,[17 ,"VAR_BUSTO" ,"LEVEL" ,"C" ,null ,"LEVEL" ,0 ]
,[18 ,"VAR_BUSTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[18 ,"VAR_BUSTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[19 ,"VAR_BUSTO" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[19 ,"VAR_BUSTO" ,"DURATION" ,"C" ,null ,"DURATION" ,0 ]
,[20 ,"VAR_BUSTO" ,"LOCK_END_DTTM" ,"C" ,null ,"LOCK_END_DTTM" ,0 ]
,[20 ,"VAR_BUSTO" ,"LOCK_START_DTTM" ,"C" ,null ,"LOCK_START_DTTM" ,0 ]
,[21 ,"VAR_BUSTO" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[21 ,"VAR_BUSTO" ,"MAX_KEY" ,"C" ,null ,"MAX_KEY" ,0 ]
,[22 ,"VAR_BUSTO" ,"REQUEST_DTTM" ,"C" ,null ,"REQUEST_DTTM" ,0 ]
,[23 ,"VAR_BUSTO" ,"REVIEWED_ON_DTTM" ,"C" ,null ,"REVIEWED_ON_DTTM" ,0 ]
,[24 ,"VAR_BUSTO" ,"RLS_ACTIVE" ,"C" ,null ,"RLS_ACTIVE" ,0 ]
,[24 ,"VAR_BUSTO" ,"RLS_SUBGROUP_ID" ,"C" ,null ,"RLS_SUBGROUP_ID" ,0 ]
,[24 ,"VAR_BUSTO" ,"RLS_RK" ,"C" ,null ,"RLS_RK" ,0 ]
,[24 ,"VAR_BUSTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[24 ,"VAR_BUSTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[25 ,"VAR_BUSTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[25 ,"VAR_BUSTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[26 ,"VAR_BUSTO" ,"SELECTBOX_ORDER" ,"C" ,null ,"SELECTBOX_ORDER" ,0 ]
,[26 ,"VAR_BUSTO" ,"SELECTBOX_RK" ,"C" ,null ,"SELECTBOX_RK" ,0 ]
,[26 ,"VAR_BUSTO" ,"VER_TO_DTTM" ,"C" ,null ,"VER_TO_DTTM" ,0 ]
,[26 ,"VAR_BUSTO" ,"VER_FROM_DTTM" ,"C" ,null ,"VER_FROM_DTTM" ,0 ]
,[27 ,"VAR_BUSTO" ,"SIGNOFF_VERSION_RK" ,"C" ,null ,"SIGNOFF_VERSION_RK" ,0 ]
,[27 ,"VAR_BUSTO" ,"SIGNOFF_SECTION_RK" ,"C" ,null ,"SIGNOFF_SECTION_RK" ,0 ]
,[27 ,"VAR_BUSTO" ,"TECH_TO_DTTM" ,"C" ,null ,"TECH_TO_DTTM" ,0 ]
,[27 ,"VAR_BUSTO" ,"TECH_FROM_DTTM" ,"C" ,null ,"TECH_FROM_DTTM" ,0 ]
,[28 ,"VAR_BUSTO" ,"REVIEWED_ON_DTTM" ,"C" ,null ,"REVIEWED_ON_DTTM" ,0 ]
,[28 ,"VAR_BUSTO" ,"NUM_OF_APPROVALS_REMAINING" ,"C" ,null ,"NUM_OF_APPROVALS_REMAINING" ,0 ]
,[28 ,"VAR_BUSTO" ,"NUM_OF_APPROVALS_REQUIRED" ,"C" ,null ,"NUM_OF_APPROVALS_REQUIRED" ,0 ]
,[28 ,"VAR_BUSTO" ,"INPUT_VARS" ,"C" ,null ,"INPUT_VARS" ,0 ]
,[28 ,"VAR_BUSTO" ,"INPUT_OBS" ,"C" ,null ,"INPUT_OBS" ,0 ]
,[28 ,"VAR_BUSTO" ,"SUBMITTED_ON_DTTM" ,"C" ,null ,"SUBMITTED_ON_DTTM" ,0 ]
,[29 ,"VAR_BUSTO" ,"NUM_OF_APPROVALS_REQUIRED" ,"C" ,null ,"NUM_OF_APPROVALS_REQUIRED" ,0 ]
,[29 ,"VAR_BUSTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[29 ,"VAR_BUSTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[30 ,"VAR_BUSTO" ,"REGISTERED_DT" ,"C" ,null ,"REGISTERED_DT" ,0 ]
,[30 ,"VAR_BUSTO" ,"LAST_SEEN_DT" ,"C" ,null ,"LAST_SEEN_DT" ,0 ]
,[31 ,"VAR_BUSTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[31 ,"VAR_BUSTO" ,"RULE_ACTIVE" ,"C" ,null ,"RULE_ACTIVE" ,0 ]
,[31 ,"VAR_BUSTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[32 ,"VAR_BUSTO" ,"SOME_BESTNUM" ,"C" ,null ,"SOME_BESTNUM" ,0 ]
,[32 ,"VAR_BUSTO" ,"SOME_SHORTNUM" ,"C" ,null ,"SOME_SHORTNUM" ,0 ]
,[32 ,"VAR_BUSTO" ,"SOME_TIME" ,"C" ,null ,"SOME_TIME" ,0 ]
,[32 ,"VAR_BUSTO" ,"SOME_DATETIME" ,"C" ,null ,"SOME_DATETIME" ,0 ]
,[32 ,"VAR_BUSTO" ,"SOME_DATE" ,"C" ,null ,"SOME_DATE" ,0 ]
,[32 ,"VAR_BUSTO" ,"SOME_NUM" ,"C" ,null ,"SOME_NUM" ,0 ]
,[32 ,"VAR_BUSTO" ,"PRIMARY_KEY_FIELD" ,"C" ,null ,"PRIMARY_KEY_FIELD" ,0 ]
,[1 ,"VAR_PROCESSED" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[1 ,"VAR_PROCESSED" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[2 ,"VAR_PROCESSED" ,"IS_DIFF" ,"C" ,null ,"IS_DIFF" ,0 ]
,[2 ,"VAR_PROCESSED" ,"IS_PK" ,"C" ,null ,"IS_PK" ,0 ]
,[2 ,"VAR_PROCESSED" ,"NEWVAL_NUM" ,"C" ,null ,"NEWVAL_NUM" ,0 ]
,[2 ,"VAR_PROCESSED" ,"OLDVAL_NUM" ,"C" ,null ,"OLDVAL_NUM" ,0 ]
,[2 ,"VAR_PROCESSED" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[3 ,"VAR_PROCESSED" ,"CLS_ACTIVE" ,"C" ,null ,"CLS_ACTIVE" ,0 ]
,[3 ,"VAR_PROCESSED" ,"CLS_HIDE" ,"C" ,null ,"CLS_HIDE" ,0 ]
,[3 ,"VAR_PROCESSED" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[3 ,"VAR_PROCESSED" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[4 ,"VAR_PROCESSED" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[4 ,"VAR_PROCESSED" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[4 ,"VAR_PROCESSED" ,"VAR_ACTIVE" ,"C" ,null ,"VAR_ACTIVE" ,0 ]
,[5 ,"VAR_PROCESSED" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[5 ,"VAR_PROCESSED" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[6 ,"VAR_PROCESSED" ,"NVAR" ,"C" ,null ,"NVAR" ,0 ]
,[6 ,"VAR_PROCESSED" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[6 ,"VAR_PROCESSED" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[7 ,"VAR_PROCESSED" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[7 ,"VAR_PROCESSED" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[7 ,"VAR_PROCESSED" ,"PK_IND" ,"C" ,null ,"PK_IND" ,0 ]
,[7 ,"VAR_PROCESSED" ,"VARNUM" ,"C" ,null ,"VARNUM" ,0 ]
,[7 ,"VAR_PROCESSED" ,"LENGTH" ,"C" ,null ,"LENGTH" ,0 ]
,[8 ,"VAR_PROCESSED" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[8 ,"VAR_PROCESSED" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[9 ,"VAR_PROCESSED" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[9 ,"VAR_PROCESSED" ,"DURATION" ,"C" ,null ,"DURATION" ,0 ]
,[9 ,"VAR_PROCESSED" ,"DELETED_RECORDS" ,"C" ,null ,"DELETED_RECORDS" ,0 ]
,[9 ,"VAR_PROCESSED" ,"NEW_RECORDS" ,"C" ,null ,"NEW_RECORDS" ,0 ]
,[9 ,"VAR_PROCESSED" ,"CHANGED_RECORDS" ,"C" ,null ,"CHANGED_RECORDS" ,0 ]
,[10 ,"VAR_PROCESSED" ,"TABLE_CNT" ,"C" ,null ,"TABLE_CNT" ,0 ]
,[10 ,"VAR_PROCESSED" ,"LIBSIZE" ,"C" ,null ,"LIBSIZE" ,0 ]
,[10 ,"VAR_PROCESSED" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[10 ,"VAR_PROCESSED" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[11 ,"VAR_PROCESSED" ,"NOBS" ,"C" ,null ,"NOBS" ,0 ]
,[11 ,"VAR_PROCESSED" ,"MODATE" ,"C" ,null ,"MODATE" ,0 ]
,[11 ,"VAR_PROCESSED" ,"CRDATE" ,"C" ,null ,"CRDATE" ,0 ]
,[11 ,"VAR_PROCESSED" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[11 ,"VAR_PROCESSED" ,"FILESIZE" ,"C" ,null ,"FILESIZE" ,0 ]
,[11 ,"VAR_PROCESSED" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[12 ,"VAR_PROCESSED" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[12 ,"VAR_PROCESSED" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[13 ,"VAR_PROCESSED" ,"XL_ACTIVE" ,"C" ,null ,"XL_ACTIVE" ,0 ]
,[13 ,"VAR_PROCESSED" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[13 ,"VAR_PROCESSED" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[14 ,"VAR_PROCESSED" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[14 ,"VAR_PROCESSED" ,"FILTER_RK" ,"C" ,null ,"FILTER_RK" ,0 ]
,[15 ,"VAR_PROCESSED" ,"SUBGROUP_ID" ,"C" ,null ,"SUBGROUP_ID" ,0 ]
,[15 ,"VAR_PROCESSED" ,"FILTER_LINE" ,"C" ,null ,"FILTER_LINE" ,0 ]
,[15 ,"VAR_PROCESSED" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[16 ,"VAR_PROCESSED" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[16 ,"VAR_PROCESSED" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[17 ,"VAR_PROCESSED" ,"MODIFIED_DTTM" ,"C" ,null ,"MODIFIED_DTTM" ,0 ]
,[17 ,"VAR_PROCESSED" ,"LEVEL" ,"C" ,null ,"LEVEL" ,0 ]
,[18 ,"VAR_PROCESSED" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[18 ,"VAR_PROCESSED" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[19 ,"VAR_PROCESSED" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[19 ,"VAR_PROCESSED" ,"DURATION" ,"C" ,null ,"DURATION" ,0 ]
,[20 ,"VAR_PROCESSED" ,"LOCK_END_DTTM" ,"C" ,null ,"LOCK_END_DTTM" ,0 ]
,[20 ,"VAR_PROCESSED" ,"LOCK_START_DTTM" ,"C" ,null ,"LOCK_START_DTTM" ,0 ]
,[21 ,"VAR_PROCESSED" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[21 ,"VAR_PROCESSED" ,"MAX_KEY" ,"C" ,null ,"MAX_KEY" ,0 ]
,[22 ,"VAR_PROCESSED" ,"REQUEST_DTTM" ,"C" ,null ,"REQUEST_DTTM" ,0 ]
,[23 ,"VAR_PROCESSED" ,"REVIEWED_ON_DTTM" ,"C" ,null ,"REVIEWED_ON_DTTM" ,0 ]
,[24 ,"VAR_PROCESSED" ,"RLS_ACTIVE" ,"C" ,null ,"RLS_ACTIVE" ,0 ]
,[24 ,"VAR_PROCESSED" ,"RLS_SUBGROUP_ID" ,"C" ,null ,"RLS_SUBGROUP_ID" ,0 ]
,[24 ,"VAR_PROCESSED" ,"RLS_RK" ,"C" ,null ,"RLS_RK" ,0 ]
,[24 ,"VAR_PROCESSED" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[24 ,"VAR_PROCESSED" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[25 ,"VAR_PROCESSED" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[25 ,"VAR_PROCESSED" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[26 ,"VAR_PROCESSED" ,"SELECTBOX_ORDER" ,"C" ,null ,"SELECTBOX_ORDER" ,0 ]
,[26 ,"VAR_PROCESSED" ,"SELECTBOX_RK" ,"C" ,null ,"SELECTBOX_RK" ,0 ]
,[26 ,"VAR_PROCESSED" ,"VER_TO_DTTM" ,"C" ,null ,"VER_TO_DTTM" ,0 ]
,[26 ,"VAR_PROCESSED" ,"VER_FROM_DTTM" ,"C" ,null ,"VER_FROM_DTTM" ,0 ]
,[27 ,"VAR_PROCESSED" ,"SIGNOFF_VERSION_RK" ,"C" ,null ,"SIGNOFF_VERSION_RK" ,0 ]
,[27 ,"VAR_PROCESSED" ,"SIGNOFF_SECTION_RK" ,"C" ,null ,"SIGNOFF_SECTION_RK" ,0 ]
,[27 ,"VAR_PROCESSED" ,"TECH_TO_DTTM" ,"C" ,null ,"TECH_TO_DTTM" ,0 ]
,[27 ,"VAR_PROCESSED" ,"TECH_FROM_DTTM" ,"C" ,null ,"TECH_FROM_DTTM" ,0 ]
,[28 ,"VAR_PROCESSED" ,"REVIEWED_ON_DTTM" ,"C" ,null ,"REVIEWED_ON_DTTM" ,0 ]
,[28 ,"VAR_PROCESSED" ,"NUM_OF_APPROVALS_REMAINING" ,"C" ,null ,"NUM_OF_APPROVALS_REMAINING" ,0 ]
,[28 ,"VAR_PROCESSED" ,"NUM_OF_APPROVALS_REQUIRED" ,"C" ,null ,"NUM_OF_APPROVALS_REQUIRED" ,0 ]
,[28 ,"VAR_PROCESSED" ,"INPUT_VARS" ,"C" ,null ,"INPUT_VARS" ,0 ]
,[28 ,"VAR_PROCESSED" ,"INPUT_OBS" ,"C" ,null ,"INPUT_OBS" ,0 ]
,[28 ,"VAR_PROCESSED" ,"SUBMITTED_ON_DTTM" ,"C" ,null ,"SUBMITTED_ON_DTTM" ,0 ]
,[29 ,"VAR_PROCESSED" ,"NUM_OF_APPROVALS_REQUIRED" ,"C" ,null ,"NUM_OF_APPROVALS_REQUIRED" ,0 ]
,[29 ,"VAR_PROCESSED" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[29 ,"VAR_PROCESSED" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[30 ,"VAR_PROCESSED" ,"REGISTERED_DT" ,"C" ,null ,"REGISTERED_DT" ,0 ]
,[30 ,"VAR_PROCESSED" ,"LAST_SEEN_DT" ,"C" ,null ,"LAST_SEEN_DT" ,0 ]
,[31 ,"VAR_PROCESSED" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[31 ,"VAR_PROCESSED" ,"RULE_ACTIVE" ,"C" ,null ,"RULE_ACTIVE" ,0 ]
,[31 ,"VAR_PROCESSED" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[32 ,"VAR_PROCESSED" ,"SOME_BESTNUM" ,"C" ,null ,"SOME_BESTNUM" ,0 ]
,[32 ,"VAR_PROCESSED" ,"SOME_SHORTNUM" ,"C" ,null ,"SOME_SHORTNUM" ,0 ]
,[32 ,"VAR_PROCESSED" ,"SOME_TIME" ,"C" ,null ,"SOME_TIME" ,0 ]
,[32 ,"VAR_PROCESSED" ,"SOME_DATETIME" ,"C" ,null ,"SOME_DATETIME" ,0 ]
,[32 ,"VAR_PROCESSED" ,"SOME_DATE" ,"C" ,null ,"SOME_DATE" ,0 ]
,[32 ,"VAR_PROCESSED" ,"SOME_NUM" ,"C" ,null ,"SOME_NUM" ,0 ]
,[32 ,"VAR_PROCESSED" ,"PRIMARY_KEY_FIELD" ,"C" ,null ,"PRIMARY_KEY_FIELD" ,0 ]
,[1 ,"VAR_TXFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,1 ]
,[1 ,"VAR_TXFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[2 ,"VAR_TXFROM" ,"IS_DIFF" ,"C" ,null ,"IS_DIFF" ,0 ]
,[2 ,"VAR_TXFROM" ,"IS_PK" ,"C" ,null ,"IS_PK" ,0 ]
,[2 ,"VAR_TXFROM" ,"NEWVAL_NUM" ,"C" ,null ,"NEWVAL_NUM" ,0 ]
,[2 ,"VAR_TXFROM" ,"OLDVAL_NUM" ,"C" ,null ,"OLDVAL_NUM" ,0 ]
,[2 ,"VAR_TXFROM" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[3 ,"VAR_TXFROM" ,"CLS_ACTIVE" ,"C" ,null ,"CLS_ACTIVE" ,0 ]
,[3 ,"VAR_TXFROM" ,"CLS_HIDE" ,"C" ,null ,"CLS_HIDE" ,0 ]
,[3 ,"VAR_TXFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[3 ,"VAR_TXFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,1 ]
,[4 ,"VAR_TXFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[4 ,"VAR_TXFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,1 ]
,[4 ,"VAR_TXFROM" ,"VAR_ACTIVE" ,"C" ,null ,"VAR_ACTIVE" ,0 ]
,[5 ,"VAR_TXFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[5 ,"VAR_TXFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,1 ]
,[6 ,"VAR_TXFROM" ,"NVAR" ,"C" ,null ,"NVAR" ,0 ]
,[6 ,"VAR_TXFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[6 ,"VAR_TXFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,1 ]
,[7 ,"VAR_TXFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,1 ]
,[7 ,"VAR_TXFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[7 ,"VAR_TXFROM" ,"PK_IND" ,"C" ,null ,"PK_IND" ,0 ]
,[7 ,"VAR_TXFROM" ,"VARNUM" ,"C" ,null ,"VARNUM" ,0 ]
,[7 ,"VAR_TXFROM" ,"LENGTH" ,"C" ,null ,"LENGTH" ,0 ]
,[8 ,"VAR_TXFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,1 ]
,[8 ,"VAR_TXFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[9 ,"VAR_TXFROM" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[9 ,"VAR_TXFROM" ,"DURATION" ,"C" ,null ,"DURATION" ,0 ]
,[9 ,"VAR_TXFROM" ,"DELETED_RECORDS" ,"C" ,null ,"DELETED_RECORDS" ,0 ]
,[9 ,"VAR_TXFROM" ,"NEW_RECORDS" ,"C" ,null ,"NEW_RECORDS" ,0 ]
,[9 ,"VAR_TXFROM" ,"CHANGED_RECORDS" ,"C" ,null ,"CHANGED_RECORDS" ,0 ]
,[10 ,"VAR_TXFROM" ,"TABLE_CNT" ,"C" ,null ,"TABLE_CNT" ,0 ]
,[10 ,"VAR_TXFROM" ,"LIBSIZE" ,"C" ,null ,"LIBSIZE" ,0 ]
,[10 ,"VAR_TXFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[10 ,"VAR_TXFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,1 ]
,[11 ,"VAR_TXFROM" ,"NOBS" ,"C" ,null ,"NOBS" ,0 ]
,[11 ,"VAR_TXFROM" ,"MODATE" ,"C" ,null ,"MODATE" ,0 ]
,[11 ,"VAR_TXFROM" ,"CRDATE" ,"C" ,null ,"CRDATE" ,0 ]
,[11 ,"VAR_TXFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[11 ,"VAR_TXFROM" ,"FILESIZE" ,"C" ,null ,"FILESIZE" ,0 ]
,[11 ,"VAR_TXFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,1 ]
,[12 ,"VAR_TXFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[12 ,"VAR_TXFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,1 ]
,[13 ,"VAR_TXFROM" ,"XL_ACTIVE" ,"C" ,null ,"XL_ACTIVE" ,0 ]
,[13 ,"VAR_TXFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[13 ,"VAR_TXFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,1 ]
,[14 ,"VAR_TXFROM" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[14 ,"VAR_TXFROM" ,"FILTER_RK" ,"C" ,null ,"FILTER_RK" ,0 ]
,[15 ,"VAR_TXFROM" ,"SUBGROUP_ID" ,"C" ,null ,"SUBGROUP_ID" ,0 ]
,[15 ,"VAR_TXFROM" ,"FILTER_LINE" ,"C" ,null ,"FILTER_LINE" ,0 ]
,[15 ,"VAR_TXFROM" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[16 ,"VAR_TXFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[16 ,"VAR_TXFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,1 ]
,[17 ,"VAR_TXFROM" ,"MODIFIED_DTTM" ,"C" ,null ,"MODIFIED_DTTM" ,0 ]
,[17 ,"VAR_TXFROM" ,"LEVEL" ,"C" ,null ,"LEVEL" ,0 ]
,[18 ,"VAR_TXFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[18 ,"VAR_TXFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,1 ]
,[19 ,"VAR_TXFROM" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[19 ,"VAR_TXFROM" ,"DURATION" ,"C" ,null ,"DURATION" ,0 ]
,[20 ,"VAR_TXFROM" ,"LOCK_END_DTTM" ,"C" ,null ,"LOCK_END_DTTM" ,0 ]
,[20 ,"VAR_TXFROM" ,"LOCK_START_DTTM" ,"C" ,null ,"LOCK_START_DTTM" ,0 ]
,[21 ,"VAR_TXFROM" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[21 ,"VAR_TXFROM" ,"MAX_KEY" ,"C" ,null ,"MAX_KEY" ,0 ]
,[22 ,"VAR_TXFROM" ,"REQUEST_DTTM" ,"C" ,null ,"REQUEST_DTTM" ,0 ]
,[23 ,"VAR_TXFROM" ,"REVIEWED_ON_DTTM" ,"C" ,null ,"REVIEWED_ON_DTTM" ,0 ]
,[24 ,"VAR_TXFROM" ,"RLS_ACTIVE" ,"C" ,null ,"RLS_ACTIVE" ,0 ]
,[24 ,"VAR_TXFROM" ,"RLS_SUBGROUP_ID" ,"C" ,null ,"RLS_SUBGROUP_ID" ,0 ]
,[24 ,"VAR_TXFROM" ,"RLS_RK" ,"C" ,null ,"RLS_RK" ,0 ]
,[24 ,"VAR_TXFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[24 ,"VAR_TXFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,1 ]
,[25 ,"VAR_TXFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[25 ,"VAR_TXFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,1 ]
,[26 ,"VAR_TXFROM" ,"SELECTBOX_ORDER" ,"C" ,null ,"SELECTBOX_ORDER" ,0 ]
,[26 ,"VAR_TXFROM" ,"SELECTBOX_RK" ,"C" ,null ,"SELECTBOX_RK" ,0 ]
,[26 ,"VAR_TXFROM" ,"VER_TO_DTTM" ,"C" ,null ,"VER_TO_DTTM" ,0 ]
,[26 ,"VAR_TXFROM" ,"VER_FROM_DTTM" ,"C" ,null ,"VER_FROM_DTTM" ,0 ]
,[27 ,"VAR_TXFROM" ,"SIGNOFF_VERSION_RK" ,"C" ,null ,"SIGNOFF_VERSION_RK" ,0 ]
,[27 ,"VAR_TXFROM" ,"SIGNOFF_SECTION_RK" ,"C" ,null ,"SIGNOFF_SECTION_RK" ,0 ]
,[27 ,"VAR_TXFROM" ,"TECH_TO_DTTM" ,"C" ,null ,"TECH_TO_DTTM" ,0 ]
,[27 ,"VAR_TXFROM" ,"TECH_FROM_DTTM" ,"C" ,null ,"TECH_FROM_DTTM" ,0 ]
,[28 ,"VAR_TXFROM" ,"REVIEWED_ON_DTTM" ,"C" ,null ,"REVIEWED_ON_DTTM" ,0 ]
,[28 ,"VAR_TXFROM" ,"NUM_OF_APPROVALS_REMAINING" ,"C" ,null ,"NUM_OF_APPROVALS_REMAINING" ,0 ]
,[28 ,"VAR_TXFROM" ,"NUM_OF_APPROVALS_REQUIRED" ,"C" ,null ,"NUM_OF_APPROVALS_REQUIRED" ,0 ]
,[28 ,"VAR_TXFROM" ,"INPUT_VARS" ,"C" ,null ,"INPUT_VARS" ,0 ]
,[28 ,"VAR_TXFROM" ,"INPUT_OBS" ,"C" ,null ,"INPUT_OBS" ,0 ]
,[28 ,"VAR_TXFROM" ,"SUBMITTED_ON_DTTM" ,"C" ,null ,"SUBMITTED_ON_DTTM" ,0 ]
,[29 ,"VAR_TXFROM" ,"NUM_OF_APPROVALS_REQUIRED" ,"C" ,null ,"NUM_OF_APPROVALS_REQUIRED" ,0 ]
,[29 ,"VAR_TXFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[29 ,"VAR_TXFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,1 ]
,[30 ,"VAR_TXFROM" ,"REGISTERED_DT" ,"C" ,null ,"REGISTERED_DT" ,0 ]
,[30 ,"VAR_TXFROM" ,"LAST_SEEN_DT" ,"C" ,null ,"LAST_SEEN_DT" ,0 ]
,[31 ,"VAR_TXFROM" ,"TX_TO" ,"C" ,null ,"TX_TO" ,0 ]
,[31 ,"VAR_TXFROM" ,"RULE_ACTIVE" ,"C" ,null ,"RULE_ACTIVE" ,0 ]
,[31 ,"VAR_TXFROM" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,1 ]
,[32 ,"VAR_TXFROM" ,"SOME_BESTNUM" ,"C" ,null ,"SOME_BESTNUM" ,0 ]
,[32 ,"VAR_TXFROM" ,"SOME_SHORTNUM" ,"C" ,null ,"SOME_SHORTNUM" ,0 ]
,[32 ,"VAR_TXFROM" ,"SOME_TIME" ,"C" ,null ,"SOME_TIME" ,0 ]
,[32 ,"VAR_TXFROM" ,"SOME_DATETIME" ,"C" ,null ,"SOME_DATETIME" ,0 ]
,[32 ,"VAR_TXFROM" ,"SOME_DATE" ,"C" ,null ,"SOME_DATE" ,0 ]
,[32 ,"VAR_TXFROM" ,"SOME_NUM" ,"C" ,null ,"SOME_NUM" ,0 ]
,[32 ,"VAR_TXFROM" ,"PRIMARY_KEY_FIELD" ,"C" ,null ,"PRIMARY_KEY_FIELD" ,0 ]
,[1 ,"VAR_TXTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[1 ,"VAR_TXTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,1 ]
,[2 ,"VAR_TXTO" ,"IS_DIFF" ,"C" ,null ,"IS_DIFF" ,0 ]
,[2 ,"VAR_TXTO" ,"IS_PK" ,"C" ,null ,"IS_PK" ,0 ]
,[2 ,"VAR_TXTO" ,"NEWVAL_NUM" ,"C" ,null ,"NEWVAL_NUM" ,0 ]
,[2 ,"VAR_TXTO" ,"OLDVAL_NUM" ,"C" ,null ,"OLDVAL_NUM" ,0 ]
,[2 ,"VAR_TXTO" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[3 ,"VAR_TXTO" ,"CLS_ACTIVE" ,"C" ,null ,"CLS_ACTIVE" ,0 ]
,[3 ,"VAR_TXTO" ,"CLS_HIDE" ,"C" ,null ,"CLS_HIDE" ,0 ]
,[3 ,"VAR_TXTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,1 ]
,[3 ,"VAR_TXTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[4 ,"VAR_TXTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,1 ]
,[4 ,"VAR_TXTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[4 ,"VAR_TXTO" ,"VAR_ACTIVE" ,"C" ,null ,"VAR_ACTIVE" ,0 ]
,[5 ,"VAR_TXTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,1 ]
,[5 ,"VAR_TXTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[6 ,"VAR_TXTO" ,"NVAR" ,"C" ,null ,"NVAR" ,0 ]
,[6 ,"VAR_TXTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,1 ]
,[6 ,"VAR_TXTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[7 ,"VAR_TXTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[7 ,"VAR_TXTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,1 ]
,[7 ,"VAR_TXTO" ,"PK_IND" ,"C" ,null ,"PK_IND" ,0 ]
,[7 ,"VAR_TXTO" ,"VARNUM" ,"C" ,null ,"VARNUM" ,0 ]
,[7 ,"VAR_TXTO" ,"LENGTH" ,"C" ,null ,"LENGTH" ,0 ]
,[8 ,"VAR_TXTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[8 ,"VAR_TXTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,1 ]
,[9 ,"VAR_TXTO" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[9 ,"VAR_TXTO" ,"DURATION" ,"C" ,null ,"DURATION" ,0 ]
,[9 ,"VAR_TXTO" ,"DELETED_RECORDS" ,"C" ,null ,"DELETED_RECORDS" ,0 ]
,[9 ,"VAR_TXTO" ,"NEW_RECORDS" ,"C" ,null ,"NEW_RECORDS" ,0 ]
,[9 ,"VAR_TXTO" ,"CHANGED_RECORDS" ,"C" ,null ,"CHANGED_RECORDS" ,0 ]
,[10 ,"VAR_TXTO" ,"TABLE_CNT" ,"C" ,null ,"TABLE_CNT" ,0 ]
,[10 ,"VAR_TXTO" ,"LIBSIZE" ,"C" ,null ,"LIBSIZE" ,0 ]
,[10 ,"VAR_TXTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,1 ]
,[10 ,"VAR_TXTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[11 ,"VAR_TXTO" ,"NOBS" ,"C" ,null ,"NOBS" ,0 ]
,[11 ,"VAR_TXTO" ,"MODATE" ,"C" ,null ,"MODATE" ,0 ]
,[11 ,"VAR_TXTO" ,"CRDATE" ,"C" ,null ,"CRDATE" ,0 ]
,[11 ,"VAR_TXTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,1 ]
,[11 ,"VAR_TXTO" ,"FILESIZE" ,"C" ,null ,"FILESIZE" ,0 ]
,[11 ,"VAR_TXTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[12 ,"VAR_TXTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,1 ]
,[12 ,"VAR_TXTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[13 ,"VAR_TXTO" ,"XL_ACTIVE" ,"C" ,null ,"XL_ACTIVE" ,0 ]
,[13 ,"VAR_TXTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,1 ]
,[13 ,"VAR_TXTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[14 ,"VAR_TXTO" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[14 ,"VAR_TXTO" ,"FILTER_RK" ,"C" ,null ,"FILTER_RK" ,0 ]
,[15 ,"VAR_TXTO" ,"SUBGROUP_ID" ,"C" ,null ,"SUBGROUP_ID" ,0 ]
,[15 ,"VAR_TXTO" ,"FILTER_LINE" ,"C" ,null ,"FILTER_LINE" ,0 ]
,[15 ,"VAR_TXTO" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[16 ,"VAR_TXTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,1 ]
,[16 ,"VAR_TXTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[17 ,"VAR_TXTO" ,"MODIFIED_DTTM" ,"C" ,null ,"MODIFIED_DTTM" ,0 ]
,[17 ,"VAR_TXTO" ,"LEVEL" ,"C" ,null ,"LEVEL" ,0 ]
,[18 ,"VAR_TXTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,1 ]
,[18 ,"VAR_TXTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[19 ,"VAR_TXTO" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[19 ,"VAR_TXTO" ,"DURATION" ,"C" ,null ,"DURATION" ,0 ]
,[20 ,"VAR_TXTO" ,"LOCK_END_DTTM" ,"C" ,null ,"LOCK_END_DTTM" ,0 ]
,[20 ,"VAR_TXTO" ,"LOCK_START_DTTM" ,"C" ,null ,"LOCK_START_DTTM" ,0 ]
,[21 ,"VAR_TXTO" ,"PROCESSED_DTTM" ,"C" ,null ,"PROCESSED_DTTM" ,0 ]
,[21 ,"VAR_TXTO" ,"MAX_KEY" ,"C" ,null ,"MAX_KEY" ,0 ]
,[22 ,"VAR_TXTO" ,"REQUEST_DTTM" ,"C" ,null ,"REQUEST_DTTM" ,0 ]
,[23 ,"VAR_TXTO" ,"REVIEWED_ON_DTTM" ,"C" ,null ,"REVIEWED_ON_DTTM" ,0 ]
,[24 ,"VAR_TXTO" ,"RLS_ACTIVE" ,"C" ,null ,"RLS_ACTIVE" ,0 ]
,[24 ,"VAR_TXTO" ,"RLS_SUBGROUP_ID" ,"C" ,null ,"RLS_SUBGROUP_ID" ,0 ]
,[24 ,"VAR_TXTO" ,"RLS_RK" ,"C" ,null ,"RLS_RK" ,0 ]
,[24 ,"VAR_TXTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,1 ]
,[24 ,"VAR_TXTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[25 ,"VAR_TXTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,1 ]
,[25 ,"VAR_TXTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[26 ,"VAR_TXTO" ,"SELECTBOX_ORDER" ,"C" ,null ,"SELECTBOX_ORDER" ,0 ]
,[26 ,"VAR_TXTO" ,"SELECTBOX_RK" ,"C" ,null ,"SELECTBOX_RK" ,0 ]
,[26 ,"VAR_TXTO" ,"VER_TO_DTTM" ,"C" ,null ,"VER_TO_DTTM" ,0 ]
,[26 ,"VAR_TXTO" ,"VER_FROM_DTTM" ,"C" ,null ,"VER_FROM_DTTM" ,0 ]
,[27 ,"VAR_TXTO" ,"SIGNOFF_VERSION_RK" ,"C" ,null ,"SIGNOFF_VERSION_RK" ,0 ]
,[27 ,"VAR_TXTO" ,"SIGNOFF_SECTION_RK" ,"C" ,null ,"SIGNOFF_SECTION_RK" ,0 ]
,[27 ,"VAR_TXTO" ,"TECH_TO_DTTM" ,"C" ,null ,"TECH_TO_DTTM" ,0 ]
,[27 ,"VAR_TXTO" ,"TECH_FROM_DTTM" ,"C" ,null ,"TECH_FROM_DTTM" ,0 ]
,[28 ,"VAR_TXTO" ,"REVIEWED_ON_DTTM" ,"C" ,null ,"REVIEWED_ON_DTTM" ,0 ]
,[28 ,"VAR_TXTO" ,"NUM_OF_APPROVALS_REMAINING" ,"C" ,null ,"NUM_OF_APPROVALS_REMAINING" ,0 ]
,[28 ,"VAR_TXTO" ,"NUM_OF_APPROVALS_REQUIRED" ,"C" ,null ,"NUM_OF_APPROVALS_REQUIRED" ,0 ]
,[28 ,"VAR_TXTO" ,"INPUT_VARS" ,"C" ,null ,"INPUT_VARS" ,0 ]
,[28 ,"VAR_TXTO" ,"INPUT_OBS" ,"C" ,null ,"INPUT_OBS" ,0 ]
,[28 ,"VAR_TXTO" ,"SUBMITTED_ON_DTTM" ,"C" ,null ,"SUBMITTED_ON_DTTM" ,0 ]
,[29 ,"VAR_TXTO" ,"NUM_OF_APPROVALS_REQUIRED" ,"C" ,null ,"NUM_OF_APPROVALS_REQUIRED" ,0 ]
,[29 ,"VAR_TXTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,1 ]
,[29 ,"VAR_TXTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[30 ,"VAR_TXTO" ,"REGISTERED_DT" ,"C" ,null ,"REGISTERED_DT" ,0 ]
,[30 ,"VAR_TXTO" ,"LAST_SEEN_DT" ,"C" ,null ,"LAST_SEEN_DT" ,0 ]
,[31 ,"VAR_TXTO" ,"TX_TO" ,"C" ,null ,"TX_TO" ,1 ]
,[31 ,"VAR_TXTO" ,"RULE_ACTIVE" ,"C" ,null ,"RULE_ACTIVE" ,0 ]
,[31 ,"VAR_TXTO" ,"TX_FROM" ,"C" ,null ,"TX_FROM" ,0 ]
,[32 ,"VAR_TXTO" ,"SOME_BESTNUM" ,"C" ,null ,"SOME_BESTNUM" ,0 ]
,[32 ,"VAR_TXTO" ,"SOME_SHORTNUM" ,"C" ,null ,"SOME_SHORTNUM" ,0 ]
,[32 ,"VAR_TXTO" ,"SOME_TIME" ,"C" ,null ,"SOME_TIME" ,0 ]
,[32 ,"VAR_TXTO" ,"SOME_DATETIME" ,"C" ,null ,"SOME_DATETIME" ,0 ]
,[32 ,"VAR_TXTO" ,"SOME_DATE" ,"C" ,null ,"SOME_DATE" ,0 ]
,[32 ,"VAR_TXTO" ,"SOME_NUM" ,"C" ,null ,"SOME_NUM" ,0 ]
,[32 ,"VAR_TXTO" ,"PRIMARY_KEY_FIELD" ,"C" ,null ,"PRIMARY_KEY_FIELD" ,0 ]
]
,"_DEBUG" : ""
,"_PROGRAM" : "/Public/app/dc/services/editors/getdynamiccolvals"
,"AUTOEXEC" : "%2Fhome%2Fsasjssrv%2Fsasjs_root%2Fsessions%2F20230310115651-53694-1678449411618%2Fautoexec.sas"
,"MF_GETUSER" : "mihajlo"
,"SYSCC" : "0"
,"SYSENCODING" : "utf-8"
,"SYSERRORTEXT" : ""
,"SYSHOSTINFOLONG" : "Linux LIN X64 3.10.0-1160.76.1.el7.x86_64 #1 SMP Wed Aug 10 16:21:17 UTC 2022 x86_64 CentOS Linux release 7.9.2009 (Core)"
,"SYSHOSTNAME" : "CentOS-79-64-minimal"
,"SYSPROCESSID" : "41DDB6C824EC04924018000000000000"
,"SYSPROCESSMODE" : "SAS Batch Mode"
,"SYSPROCESSNAME" : ""
,"SYSJOBID" : "25810"
,"SYSSCPL" : "Linux"
,"SYSSITE" : "123"
,"SYSTCPIPHOSTNAME" : "CentOS-79-64-minimal"
,"SYSUSERID" : "sasjssrv"
,"SYSVLONG" : "9.04.01M7P080520"
,"SYSWARNINGTEXT" : ""
,"END_DTTM" : "2023-03-10T12:56:52.295466"
,"MEMSIZE" : "2GB"
}
`

let other = `{"SYSDATE" : "29SEP22"
,"SYSTIME" : "11:51"
, "dynamic_values":
[
  [
                                   1,
    "0.0010556476",
                    0.00105564761956
  ],
  [
                                   2,
    "0.0052189599",
                    0.00521895988156
  ],
  [
                                   3,
    "0.0058409725",
                     0.0058409725343
  ],
  [
                                   4,
    "0.006130504",
                    0.00613050395908
  ],
  [
                                   5,
    "0.0130502507",
                    0.01305025071513
  ],
  [
                                   6,
    "0.0144214248",
                    0.01442142483518
  ],
  [
                                   7,
    "0.0242283885",
                    0.02422838845487
  ],
  [
                                   8,
    "0.0256445333",
                     0.0256445333481
  ],
  [
                                   9,
    "0.0262452592",
                    0.02624525922641
  ],
  [
                                  10,
    "0.0289132246",
                    0.02891322459509
  ],
  [
                                  11,
    "0.029728665",
                    0.02972866503043
  ],
  [
                                  12,
    "0.0310356193",
                    0.03103561933666
  ],
  [
                                  13,
    "0.0315013211",
                    0.03150132113671
  ],
  [
                                  14,
    "0.0352235506",
                    0.03522355064527
  ],
  [
                                  15,
    "0.0352940625",
                    0.03529406247441
  ],
  [
                                  16,
    "0.0397578571",
                    0.03975785711768
  ],
  [
                                  17,
    "0.0431997366",
                    0.04319973664507
  ],
  [
                                  18,
    "0.0452116919",
                    0.04521169189606
  ],
  [
                                  19,
    "0.0478734295",
                    0.04787342951068
  ],
  [
                                  20,
    "0.0494814422",
                    0.04948144222119
  ],
  [
                                  21,
    "0.0509991274",
                    0.05099912735214
  ],
  [
                                  22,
    "0.0511230966",
                    0.05112309663143
  ],
  [
                                  23,
    "0.0540671353",
                    0.05406713534801
  ],
  [
                                  24,
    "0.0545679247",
                    0.05456792472608
  ],
  [
                                  25,
    "0.0560880252",
                    0.05608802524213
  ],
  [
                                  26,
    "0.0564905135",
                     0.0564905135224
  ],
  [
                                  27,
    "0.0565994717",
                    0.05659947174442
  ],
  [
                                  28,
    "0.0627542204",
                    0.06275422035844
  ],
  [
                                  29,
    "0.0628753263",
                    0.06287532628647
  ],
  [
                                  30,
    "0.0667286301",
                    0.06672863013424
  ],
  [
                                  31,
    "0.0701107537",
                     0.0701107536769
  ],
  [
                                  32,
    "0.0711784712",
                    0.07117847123703
  ],
  [
                                  33,
    "0.0737551018",
                    0.07375510180078
  ],
  [
                                  34,
    "0.0766677759",
                    0.07666777590134
  ],
  [
                                  35,
    "0.0812941534",
                    0.08129415338919
  ],
  [
                                  36,
    "0.0816029059",
                    0.08160290591493
  ],
  [
                                  37,
    "0.0871032426",
                     0.0871032425608
  ],
  [
                                  38,
    "0.0887682015",
                    0.08876820145583
  ],
  [
                                  39,
    "0.089252377",
                    0.08925237697048
  ],
  [
                                  40,
    "0.0913677868",
                    0.09136778679274
  ],
  [
                                  41,
    "0.0925364206",
                    0.09253642060446
  ],
  [
                                  42,
    "0.0933360565",
                     0.0933360564957
  ],
  [
                                  43,
    "0.0936049917",
                    0.09360499172173
  ],
  [
                                  44,
    "0.0967498683",
                    0.09674986828898
  ],
  [
                                  45,
    "0.0981378053",
                    0.09813780528406
  ],
  [
                                  46,
    "0.104656633",
                    0.10465663303837
  ],
  [
                                  47,
    "0.1081672963",
                    0.10816729632586
  ],
  [
                                  48,
    "0.1096644709",
                    0.10966447093974
  ],
  [
                                  49,
    "0.1156573501",
                    0.11565735010227
  ],
  [
                                  50,
    "0.1175322771",
                    0.11753227706883
  ],
  [
                                  51,
    "0.1195091284",
                    0.11950912844366
  ],
  [
                                  52,
    "0.1197331572",
                    0.11973315715777
  ],
  [
                                  53,
    "0.120146513",
                    0.12014651304117
  ],
  [
                                  54,
    "0.1212504833",
                    0.12125048326386
  ],
  [
                                  55,
    "0.124728859",
                    0.12472885899465
  ],
  [
                                  56,
    "0.1259848066",
                    0.12598480662609
  ],
  [
                                  57,
    "0.1289282409",
                    0.12892824091433
  ],
  [
                                  58,
    "0.1300212565",
                    0.13002125645523
  ],
  [
                                  59,
    "0.1303541368",
                    0.13035413675492
  ],
  [
                                  60,
    "0.1319413279",
                    0.13194132788662
  ],
  [
                                  61,
    "0.1323780209",
                    0.13237802085111
  ],
  [
                                  62,
    "0.1379828803",
                    0.13798288029524
  ],
  [
                                  63,
    "0.1382724979",
                    0.13827249786736
  ],
  [
                                  64,
    "0.1386283367",
                    0.13862833666551
  ],
  [
                                  65,
    "0.1415707628",
                    0.14157076279705
  ],
  [
                                  66,
    "0.1420672057",
                    0.14206720569267
  ],
  [
                                  67,
    "0.1423215792",
                    0.14232157922457
  ],
  [
                                  68,
    "0.1439077361",
                    0.14390773612256
  ],
  [
                                  69,
    "0.144336483",
                    0.14433648304284
  ],
  [
                                  70,
    "0.1449067272",
                    0.14490672719893
  ],
  [
                                  71,
    "0.1462393632",
                    0.14623936319082
  ],
  [
                                  72,
    "0.1462428794",
                    0.14624287939921
  ],
  [
                                  73,
    "0.1529210709",
                    0.15292107088161
  ],
  [
                                  74,
    "0.152996922",
                    0.15299692198308
  ],
  [
                                  75,
    "0.1537194239",
                    0.15371942387601
  ],
  [
                                  76,
    "0.1569773956",
                    0.15697739560016
  ],
  [
                                  77,
    "0.1578208316",
                    0.15782083159211
  ],
  [
                                  78,
    "0.1670272132",
                    0.16702721322282
  ],
  [
                                  79,
    "0.1693538531",
                     0.1693538530587
  ],
  [
                                  80,
    "0.1744914386",
                    0.17449143863026
  ],
  [
                                  81,
    "0.177699349",
                    0.17769934897203
  ],
  [
                                  82,
    "0.1820049724",
                    0.18200497244578
  ],
  [
                                  83,
    "0.1826881851",
                     0.1826881850989
  ],
  [
                                  84,
    "0.18323942",
                    0.18323942002991
  ],
  [
                                  85,
    "0.1852788567",
                    0.18527885674744
  ],
  [
                                  86,
    "0.1855629674",
                    0.18556296740917
  ],
  [
                                  87,
    "0.1876353227",
                    0.18763532265445
  ],
  [
                                  88,
    "0.1892374359",
                    0.18923743590211
  ],
  [
                                  89,
    "0.1941492833",
                    0.19414928331698
  ],
  [
                                  90,
    "0.1951624854",
                    0.19516248544452
  ],
  [
                                  91,
    "0.1962652361",
                     0.1962652361003
  ],
  [
                                  92,
    "0.2007414457",
                    0.20074144573916
  ],
  [
                                  93,
    "0.2009625971",
                    0.20096259713217
  ],
  [
                                  94,
    "0.2012765562",
                    0.20127655621677
  ],
  [
                                  95,
    "0.2035595692",
                    0.20355956917794
  ],
  [
                                  96,
    "0.2047977723",
                    0.20479777232035
  ],
  [
                                  97,
    "0.2058004682",
                    0.20580046819793
  ],
  [
                                  98,
    "0.2135380591",
                    0.21353805913288
  ],
  [
                                  99,
    "0.2165553161",
                    0.21655531610201
  ],
  [
                                 100,
    "0.2211957947",
                    0.22119579474497
  ],
  [
                                 101,
    "0.2214682853",
                    0.22146828529493
  ],
  [
                                 102,
    "0.2243504483",
                    0.22435044833661
  ],
  [
                                 103,
    "0.2248352795",
                    0.22483527950236
  ],
  [
                                 104,
    "0.2339701779",
                    0.23397017793449
  ],
  [
                                 105,
    "0.2342108727",
                    0.23421087266607
  ],
  [
                                 106,
    "0.2345068465",
                    0.23450684651476
  ],
  [
                                 107,
    "0.2349950388",
                    0.23499503882368
  ],
  [
                                 108,
    "0.2363862056",
                    0.23638620564545
  ],
  [
                                 109,
    "0.2381973272",
                    0.23819732723673
  ],
  [
                                 110,
    "0.2445727066",
                    0.24457270663444
  ],
  [
                                 111,
    "0.2472797596",
                    0.24727975961159
  ],
  [
                                 112,
    "0.2474831744",
                    0.24748317443182
  ],
  [
                                 113,
    "0.2498985614",
                    0.24989856139286
  ],
  [
                                 114,
    "0.2507105862",
                    0.25071058620266
  ],
  [
                                 115,
    "0.2527495829",
                    0.25274958287028
  ],
  [
                                 116,
    "0.2531839876",
                     0.2531839875752
  ],
  [
                                 117,
    "0.2537911871",
                    0.25379118707673
  ],
  [
                                 118,
    "0.2559820187",
                    0.25598201866074
  ],
  [
                                 119,
    "0.2582184958",
                    0.25821849576114
  ],
  [
                                 120,
    "0.2594222428",
                    0.25942224276225
  ],
  [
                                 121,
    "0.2611002812",
                    0.26110028115152
  ],
  [
                                 122,
    "0.2611194152",
                    0.26111941517382
  ],
  [
                                 123,
    "0.2656607904",
                    0.26566079038458
  ],
  [
                                 124,
    "0.2672296191",
                    0.26722961909474
  ],
  [
                                 125,
    "0.2704750268",
                    0.27047502681169
  ],
  [
                                 126,
    "0.2776136004",
                     0.2776136003796
  ],
  [
                                 127,
    "0.2794422001",
                    0.27944220010165
  ],
  [
                                 128,
    "0.2815401746",
                    0.28154017463398
  ],
  [
                                 129,
    "0.282674513",
                    0.28267451295753
  ],
  [
                                 130,
    "0.2827376883",
                    0.28273768829309
  ],
  [
                                 131,
    "0.2835610128",
                    0.28356101283969
  ],
  [
                                 132,
    "0.2836767511",
                    0.28367675109006
  ],
  [
                                 133,
    "0.2862014506",
                    0.28620145064136
  ],
  [
                                 134,
    "0.2874902935",
                    0.28749029351747
  ],
  [
                                 135,
    "0.2921020027",
                    0.29210200267476
  ],
  [
                                 136,
    "0.2921788075",
                    0.29217880745054
  ],
  [
                                 137,
    "0.2922141674",
                     0.2922141674404
  ],
  [
                                 138,
    "0.2959326474",
                    0.29593264744427
  ],
  [
                                 139,
    "0.2994232058",
                    0.29942320580567
  ],
  [
                                 140,
    "0.3030040531",
                    0.30300405309675
  ],
  [
                                 141,
    "0.3072776535",
                    0.30727765350941
  ],
  [
                                 142,
    "0.3083657815",
                    0.30836578146944
  ],
  [
                                 143,
    "0.3091963638",
                    0.30919636381286
  ],
  [
                                 144,
    "0.3103368815",
                    0.31033688146171
  ],
  [
                                 145,
    "0.3116543509",
                    0.31165435086547
  ],
  [
                                 146,
    "0.3119503438",
                    0.31195034380627
  ],
  [
                                 147,
    "0.3125689832",
                    0.31256898320865
  ],
  [
                                 148,
    "0.3128095354",
                    0.31280953544788
  ],
  [
                                 149,
    "0.3137571762",
                    0.31375717619143
  ],
  [
                                 150,
    "0.3141113638",
                    0.31411136375465
  ],
  [
                                 151,
    "0.3155595764",
                    0.31555957641245
  ],
  [
                                 152,
    "0.3183550259",
                    0.31835502587182
  ],
  [
                                 153,
    "0.319197504",
                     0.3191975039985
  ],
  [
                                 154,
    "0.3198258208",
                    0.31982582077375
  ],
  [
                                 155,
    "0.3202724426",
                    0.32027244256822
  ],
  [
                                 156,
    "0.3206662695",
                    0.32066626954854
  ],
  [
                                 157,
    "0.3246063573",
                    0.32460635729348
  ],
  [
                                 158,
    "0.3265619778",
                    0.32656197777323
  ],
  [
                                 159,
    "0.3267726765",
                    0.32677267646732
  ],
  [
                                 160,
    "0.3281118005",
                    0.32811180051793
  ],
  [
                                 161,
    "0.3300200963",
                    0.33002009630669
  ],
  [
                                 162,
    "0.3325596104",
                    0.33255961040619
  ],
  [
                                 163,
    "0.3383520904",
                      0.338352090371
  ],
  [
                                 164,
    "0.3418517105",
                    0.34185171050105
  ],
  [
                                 165,
    "0.3420869644",
                    0.34208696444616
  ],
  [
                                 166,
    "0.3429290007",
                    0.34292900066027
  ],
  [
                                 167,
    "0.3493668583",
                    0.34936685829859
  ],
  [
                                 168,
    "0.3515388879",
                     0.3515388878768
  ],
  [
                                 169,
    "0.3564928693",
                    0.35649286925629
  ],
  [
                                 170,
    "0.356953426",
                    0.35695342596478
  ],
  [
                                 171,
    "0.3581244058",
                    0.35812440577807
  ],
  [
                                 172,
    "0.3597661026",
                    0.35976610256348
  ],
  [
                                 173,
    "0.35980724",
                    0.35980724001294
  ],
  [
                                 174,
    "0.3612681815",
                    0.36126818152203
  ],
  [
                                 175,
    "0.361830231",
                    0.36183023097078
  ],
  [
                                 176,
    "0.3618709479",
                     0.3618709479281
  ],
  [
                                 177,
    "0.3677867113",
                    0.36778671125312
  ],
  [
                                 178,
    "0.3682404889",
                    0.36824048886459
  ],
  [
                                 179,
    "0.3704071368",
                     0.3704071367953
  ],
  [
                                 180,
    "0.3713489284",
                    0.37134892836741
  ],
  [
                                 181,
    "0.372728008",
                    0.37272800801914
  ],
  [
                                 182,
    "0.3738538126",
                    0.37385381263394
  ],
  [
                                 183,
    "0.3744010908",
                    0.37440109084099
  ],
  [
                                 184,
    "0.3761253238",
                    0.37612532376131
  ],
  [
                                 185,
    "0.3781310042",
                    0.37813100422645
  ],
  [
                                 186,
    "0.378220983",
                    0.37822098302572
  ],
  [
                                 187,
    "0.3783546949",
                    0.37835469487046
  ],
  [
                                 188,
    "0.3805264069",
                    0.38052640686767
  ],
  [
                                 189,
    "0.3830738665",
                    0.38307386654572
  ],
  [
                                 190,
    "0.383970143",
                    0.38397014298661
  ],
  [
                                 191,
    "0.3880386531",
                    0.38803865312972
  ],
  [
                                 192,
    "0.3893924595",
                    0.38939245948074
  ],
  [
                                 193,
    "0.3899468637",
                    0.38994686370247
  ],
  [
                                 194,
    "0.3976062771",
                    0.39760627709217
  ],
  [
                                 195,
    "0.4007478316",
                     0.4007478316318
  ],
  [
                                 196,
    "0.4041808939",
                     0.4041808938627
  ],
  [
                                 197,
    "0.4048613139",
                    0.40486131394508
  ],
  [
                                 198,
    "0.4063810457",
                    0.40638104565738
  ],
  [
                                 199,
    "0.4085182563",
                    0.40851825634414
  ],
  [
                                 200,
    "0.4089814818",
                    0.40898148175747
  ],
  [
                                 201,
    "0.4150194705",
                    0.41501947046025
  ],
  [
                                 202,
    "0.4162862289",
                    0.41628622888414
  ],
  [
                                 203,
    "0.4176403854",
                    0.41764038541244
  ],
  [
                                 204,
    "0.4184582934",
                    0.41845829338694
  ],
  [
                                 205,
    "0.4225753753",
                    0.42257537526198
  ],
  [
                                 206,
    "0.4236965931",
                     0.4236965931131
  ],
  [
                                 207,
    "0.4242641187",
                    0.42426411873859
  ],
  [
                                 208,
    "0.4249742881",
                    0.42497428805798
  ],
  [
                                 209,
    "0.4285296958",
                    0.42852969580727
  ],
  [
                                 210,
    "0.4358694048",
                    0.43586940478341
  ],
  [
                                 211,
    "0.4378844986",
                    0.43788449859148
  ],
  [
                                 212,
    "0.4383217452",
                    0.43832174522724
  ],
  [
                                 213,
    "0.4434180783",
                      0.443418078331
  ],
  [
                                 214,
    "0.4487700213",
                    0.44877002129739
  ],
  [
                                 215,
    "0.4527745622",
                    0.45277456215246
  ],
  [
                                 216,
    "0.4540794913",
                    0.45407949129775
  ],
  [
                                 217,
    "0.4578205154",
                    0.45782051536153
  ],
  [
                                 218,
    "0.4586652529",
                    0.45866525287677
  ],
  [
                                 219,
    "0.458775894",
                    0.45877589399869
  ],
  [
                                 220,
    "0.4601080424",
                    0.46010804244322
  ],
  [
                                 221,
    "0.4610861705",
                    0.46108617049692
  ],
  [
                                 222,
    "0.4633602372",
                    0.46336023717343
  ],
  [
                                 223,
    "0.4651395569",
                    0.46513955689274
  ],
  [
                                 224,
    "0.469918695",
                    0.46991869503162
  ],
  [
                                 225,
    "0.4712498465",
                    0.47124984649533
  ],
  [
                                 226,
    "0.4743374281",
                    0.47433742809777
  ],
  [
                                 227,
    "0.4754464964",
                    0.47544649638023
  ],
  [
                                 228,
    "0.475602078",
                    0.47560207800734
  ],
  [
                                 229,
    "0.4782178642",
                    0.47821786416611
  ],
  [
                                 230,
    "0.479468757",
                    0.47946875704427
  ],
  [
                                 231,
    "0.4819296959",
                    0.48192969592378
  ],
  [
                                 232,
    "0.4857948518",
                    0.48579485178263
  ],
  [
                                 233,
    "0.4860294845",
                    0.48602948453558
  ],
  [
                                 234,
    "0.489890096",
                    0.48989009600593
  ],
  [
                                 235,
    "0.4920524813",
                    0.49205248127321
  ],
  [
                                 236,
    "0.4932398379",
                    0.49323983792832
  ],
  [
                                 237,
    "0.4935002562",
                     0.4935002562094
  ],
  [
                                 238,
    "0.4964019216",
                    0.49640192161146
  ],
  [
                                 239,
    "0.496795092",
                    0.49679509200937
  ],
  [
                                 240,
    "0.499331618",
                    0.49933161796039
  ],
  [
                                 241,
    "0.5030828875",
                    0.50308288750382
  ],
  [
                                 242,
    "0.5033447056",
                    0.50334470556273
  ],
  [
                                 243,
    "0.5051939867",
                    0.50519398669954
  ],
  [
                                 244,
    "0.5059393726",
                    0.50593937258512
  ],
  [
                                 245,
    "0.5070585681",
                    0.50705856806927
  ],
  [
                                 246,
    "0.5090773215",
                     0.5090773215094
  ],
  [
                                 247,
    "0.5164117192",
                    0.51641171915289
  ],
  [
                                 248,
    "0.5166014081",
                    0.51660140814101
  ],
  [
                                 249,
    "0.5171571814",
                    0.51715718140693
  ],
  [
                                 250,
    "0.5225919325",
                    0.52259193245442
  ],
  [
                                 251,
    "0.5261947948",
                    0.52619479481419
  ],
  [
                                 252,
    "0.5275636159",
                    0.52756361594775
  ],
  [
                                 253,
    "0.5330098092",
                     0.5330098092244
  ],
  [
                                 254,
    "0.5344165617",
                    0.53441656172946
  ],
  [
                                 255,
    "0.53766182",
                    0.53766181996914
  ],
  [
                                 256,
    "0.5385407007",
                    0.53854070070131
  ],
  [
                                 257,
    "0.5418435021",
                    0.54184350210327
  ],
  [
                                 258,
    "0.5432779065",
                    0.54327790650691
  ],
  [
                                 259,
    "0.5446134911",
                    0.54461349106608
  ],
  [
                                 260,
    "0.5462143787",
                    0.54621437869324
  ],
  [
                                 261,
    "0.5482190571",
                    0.54821905705529
  ],
  [
                                 262,
    "0.5503190246",
                    0.55031902461793
  ],
  [
                                 263,
    "0.5555936049",
                    0.55559360494631
  ],
  [
                                 264,
    "0.5594416352",
                       0.55944163518
  ],
  [
                                 265,
    "0.5611671268",
                    0.56116712678278
  ],
  [
                                 266,
    "0.5650453091",
                    0.56504530905049
  ],
  [
                                 267,
    "0.565516237",
                    0.56551623696718
  ],
  [
                                 268,
    "0.5676668196",
                    0.56766681958346
  ],
  [
                                 269,
    "0.5678722689",
                    0.56787226887786
  ],
  [
                                 270,
    "0.5678896818",
                    0.56788968181604
  ],
  [
                                 271,
    "0.5701570546",
                    0.57015705461155
  ],
  [
                                 272,
    "0.5702627821",
                    0.57026278207556
  ],
  [
                                 273,
    "0.5716964167",
                    0.57169641674109
  ],
  [
                                 274,
    "0.5762077442",
                    0.57620774422595
  ],
  [
                                 275,
    "0.5822708009",
                    0.58227080087283
  ],
  [
                                 276,
    "0.5825441305",
                    0.58254413054443
  ],
  [
                                 277,
    "0.5826698898",
                     0.5826698898257
  ],
  [
                                 278,
    "0.5848126917",
                    0.58481269170754
  ],
  [
                                 279,
    "0.5856470697",
                    0.58564706965612
  ],
  [
                                 280,
    "0.5862519981",
                    0.58625199812755
  ],
  [
                                 281,
    "0.5863271587",
                    0.58632715865332
  ],
  [
                                 282,
    "0.5904919606",
                    0.59049196056578
  ],
  [
                                 283,
    "0.5920675856",
                    0.59206758560243
  ],
  [
                                 284,
    "0.5937247666",
                    0.59372476655697
  ],
  [
                                 285,
    "0.5968111752",
                    0.59681117515862
  ],
  [
                                 286,
    "0.6001805093",
                    0.60018050931402
  ],
  [
                                 287,
    "0.6050176926",
                    0.60501769259805
  ],
  [
                                 288,
    "0.6058371373",
                    0.60583713725481
  ],
  [
                                 289,
    "0.6068249189",
                    0.60682491893266
  ],
  [
                                 290,
    "0.6099196866",
                    0.60991968662008
  ],
  [
                                 291,
    "0.6109455929",
                    0.61094559291887
  ],
  [
                                 292,
    "0.6118795865",
                    0.61187958652706
  ],
  [
                                 293,
    "0.6124320466",
                    0.61243204661292
  ],
  [
                                 294,
    "0.6152676272",
                    0.61526762722771
  ],
  [
                                 295,
    "0.6196319371",
                    0.61963193706219
  ],
  [
                                 296,
    "0.6207422123",
                    0.62074221233871
  ],
  [
                                 297,
    "0.625352654",
                    0.62535265396598
  ],
  [
                                 298,
    "0.6261752432",
                    0.62617524323341
  ],
  [
                                 299,
    "0.6271740052",
                    0.62717400520442
  ],
  [
                                 300,
    "0.6283833318",
                    0.62838333175908
  ],
  [
                                 301,
    "0.6293501689",
                    0.62935016892354
  ],
  [
                                 302,
    "0.6339364614",
                    0.63393646135643
  ],
  [
                                 303,
    "0.6423292927",
                    0.64232929267097
  ],
  [
                                 304,
    "0.643764003",
                    0.64376400301408
  ],
  [
                                 305,
    "0.6438509126",
                     0.6438509126398
  ],
  [
                                 306,
    "0.6461636464",
                    0.64616364643264
  ],
  [
                                 307,
    "0.6471605886",
                    0.64716058859935
  ],
  [
                                 308,
    "0.6472982353",
                    0.64729823528197
  ],
  [
                                 309,
    "0.6481599396",
                    0.64815993963189
  ],
  [
                                 310,
    "0.6519840749",
                    0.65198407492227
  ],
  [
                                 311,
    "0.6538249178",
                    0.65382491781088
  ],
  [
                                 312,
    "0.654417035",
                    0.65441703500897
  ],
  [
                                 313,
    "0.6551565023",
                    0.65515650233959
  ],
  [
                                 314,
    "0.6580030046",
                    0.65800300457421
  ],
  [
                                 315,
    "0.6585909131",
                    0.65859091312558
  ],
  [
                                 316,
    "0.6598943354",
                    0.65989433539095
  ],
  [
                                 317,
    "0.6628110109",
                    0.66281101091895
  ],
  [
                                 318,
    "0.6652203061",
                    0.66522030609902
  ],
  [
                                 319,
    "0.6657726218",
                    0.66577262182988
  ],
  [
                                 320,
    "0.6672786696",
                    0.66727866961959
  ],
  [
                                 321,
    "0.6682578873",
                    0.66825788732071
  ],
  [
                                 322,
    "0.6706138443",
                    0.67061384425992
  ],
  [
                                 323,
    "0.6728389774",
                     0.6728389773857
  ],
  [
                                 324,
    "0.6738104656",
                     0.6738104655751
  ],
  [
                                 325,
    "0.6739414286",
                    0.67394142862127
  ],
  [
                                 326,
    "0.6789427873",
                    0.67894278731147
  ],
  [
                                 327,
    "0.6792435556",
                    0.67924355560878
  ],
  [
                                 328,
    "0.6839609233",
                    0.68396092331221
  ],
  [
                                 329,
    "0.6843712538",
                    0.68437125379423
  ],
  [
                                 330,
    "0.686917588",
                    0.68691758796894
  ],
  [
                                 331,
    "0.6886296313",
                    0.68862963127374
  ],
  [
                                 332,
    "0.6897600837",
                    0.68976008365385
  ],
  [
                                 333,
    "0.6902719348",
                     0.6902719348158
  ],
  [
                                 334,
    "0.6965584935",
                    0.69655849351387
  ],
  [
                                 335,
    "0.6982833937",
                    0.69828339372681
  ],
  [
                                 336,
    "0.6993319917",
                    0.69933199170014
  ],
  [
                                 337,
    "0.7005978272",
                    0.70059782718336
  ],
  [
                                 338,
    "0.7030775499",
                    0.70307754990788
  ],
  [
                                 339,
    "0.7083388677",
                    0.70833886773713
  ],
  [
                                 340,
    "0.7095440625",
                    0.70954406247918
  ],
  [
                                 341,
    "0.7128569939",
                    0.71285699387679
  ],
  [
                                 342,
    "0.7129457475",
                    0.71294574752121
  ],
  [
                                 343,
    "0.7148045514",
                    0.71480455143135
  ],
  [
                                 344,
    "0.7163206705",
                    0.71632067054338
  ],
  [
                                 345,
    "0.7194143854",
                    0.71941438537063
  ],
  [
                                 346,
    "0.7206447743",
                    0.72064477425098
  ],
  [
                                 347,
    "0.7221357979",
                    0.72213579794491
  ],
  [
                                 348,
    "0.7237609353",
                    0.72376093534927
  ],
  [
                                 349,
    "0.7239382587",
                     0.7239382587019
  ],
  [
                                 350,
    "0.7243118704",
                    0.72431187039441
  ],
  [
                                 351,
    "0.726473275",
                    0.72647327497902
  ],
  [
                                 352,
    "0.7296714185",
                    0.72967141854095
  ],
  [
                                 353,
    "0.7351543967",
                    0.73515439673101
  ],
  [
                                 354,
    "0.7355358548",
                    0.73553585481622
  ],
  [
                                 355,
    "0.7421012711",
                    0.74210127105102
  ],
  [
                                 356,
    "0.7431389316",
                    0.74313893157203
  ],
  [
                                 357,
    "0.7439721426",
                    0.74397214257343
  ],
  [
                                 358,
    "0.745112982",
                    0.74511298199422
  ],
  [
                                 359,
    "0.7457633935",
                    0.74576339346625
  ],
  [
                                 360,
    "0.7471672286",
                    0.74716722860334
  ],
  [
                                 361,
    "0.7481874147",
                    0.74818741471887
  ],
  [
                                 362,
    "0.7482518003",
                    0.74825180030812
  ],
  [
                                 363,
    "0.7485077012",
                    0.74850770120905
  ],
  [
                                 364,
    "0.7505016126",
                    0.75050161255081
  ],
  [
                                 365,
    "0.7530099637",
                    0.75300996366562
  ],
  [
                                 366,
    "0.754795242",
                    0.75479524198677
  ],
  [
                                 367,
    "0.7557260197",
                    0.75572601973811
  ],
  [
                                 368,
    "0.7562984739",
                    0.75629847392267
  ],
  [
                                 369,
    "0.7598333898",
                    0.75983338978133
  ],
  [
                                 370,
    "0.7600311771",
                     0.7600311770849
  ],
  [
                                 371,
    "0.7601212798",
                    0.76012127975007
  ],
  [
                                 372,
    "0.7640370269",
                    0.76403702691385
  ],
  [
                                 373,
    "0.7656979653",
                    0.76569796528932
  ],
  [
                                 374,
    "0.7679536002",
                    0.76795360016075
  ],
  [
                                 375,
    "0.7680506472",
                     0.7680506472327
  ],
  [
                                 376,
    "0.7683535613",
                    0.76835356129722
  ],
  [
                                 377,
    "0.7695589502",
                    0.76955895022002
  ],
  [
                                 378,
    "0.7719003185",
                    0.77190031845676
  ],
  [
                                 379,
    "0.7721947607",
                    0.77219476074548
  ],
  [
                                 380,
    "0.7730772094",
                    0.77307720937443
  ],
  [
                                 381,
    "0.7781522706",
                    0.77815227060492
  ],
  [
                                 382,
    "0.7822206192",
                    0.78222061916357
  ],
  [
                                 383,
    "0.7828206866",
                    0.78282068659682
  ],
  [
                                 384,
    "0.7894097486",
                    0.78940974864615
  ],
  [
                                 385,
    "0.789710713",
                    0.78971071298686
  ],
  [
                                 386,
    "0.7914886022",
                    0.79148860219469
  ],
  [
                                 387,
    "0.7948410128",
                    0.79484101282192
  ],
  [
                                 388,
    "0.8025113385",
                    0.80251133851823
  ],
  [
                                 389,
    "0.807042594",
                    0.80704259397789
  ],
  [
                                 390,
    "0.8079456518",
                    0.80794565184411
  ],
  [
                                 391,
    "0.8089167535",
                    0.80891675353465
  ],
  [
                                 392,
    "0.8097375109",
                    0.80973751089057
  ],
  [
                                 393,
    "0.8114332509",
                    0.81143325092803
  ],
  [
                                 394,
    "0.8127308357",
                    0.81273083566349
  ],
  [
                                 395,
    "0.8143003941",
                    0.81430039406488
  ],
  [
                                 396,
    "0.8183699259",
                    0.81836992586886
  ],
  [
                                 397,
    "0.8183761732",
                     0.8183761731807
  ],
  [
                                 398,
    "0.8191833593",
                    0.81918335930406
  ],
  [
                                 399,
    "0.8277966272",
                    0.82779662722153
  ],
  [
                                 400,
    "0.8281171298",
                    0.82811712977854
  ],
  [
                                 401,
    "0.8300228849",
                    0.83002288491931
  ],
  [
                                 402,
    "0.8319003712",
                    0.83190037116031
  ],
  [
                                 403,
    "0.8335338011",
                    0.83353380106088
  ],
  [
                                 404,
    "0.8382751536",
                    0.83827515358025
  ],
  [
                                 405,
    "0.8427027133",
                    0.84270271325609
  ],
  [
                                 406,
    "0.8444202141",
                    0.84442021411118
  ],
  [
                                 407,
    "0.8452174369",
                    0.84521743694563
  ],
  [
                                 408,
    "0.8468697895",
                    0.84686978945828
  ],
  [
                                 409,
    "0.8484499486",
                    0.84844994863888
  ],
  [
                                 410,
    "0.8530562175",
                    0.85305621747535
  ],
  [
                                 411,
    "0.8552265628",
                    0.85522656275668
  ],
  [
                                 412,
    "0.8554183398",
                    0.85541833977001
  ],
  [
                                 413,
    "0.8557945787",
                    0.85579457872351
  ],
  [
                                 414,
    "0.8560648569",
                    0.85606485691669
  ],
  [
                                 415,
    "0.8575603132",
                    0.85756031324042
  ],
  [
                                 416,
    "0.8575992262",
                    0.85759922622591
  ],
  [
                                 417,
    "0.8619446959",
                    0.86194469587036
  ],
  [
                                 418,
    "0.862746885",
                    0.86274688498244
  ],
  [
                                 419,
    "0.8637428497",
                    0.86374284972611
  ],
  [
                                 420,
    "0.8638405962",
                    0.86384059622131
  ],
  [
                                 421,
    "0.8667194014",
                    0.86671940137945
  ],
  [
                                 422,
    "0.8693330497",
                    0.86933304968724
  ],
  [
                                 423,
    "0.8706746715",
                      0.870674671545
  ],
  [
                                 424,
    "0.8726787776",
                    0.87267877760933
  ],
  [
                                 425,
    "0.8727606055",
                    0.87276060547342
  ],
  [
                                 426,
    "0.8727652737",
                    0.87276527372783
  ],
  [
                                 427,
    "0.8740768828",
                    0.87407688278429
  ],
  [
                                 428,
    "0.8801450408",
                    0.88014504075057
  ],
  [
                                 429,
    "0.8845914518",
                    0.88459145179232
  ],
  [
                                 430,
    "0.8846158562",
                    0.88461585616907
  ],
  [
                                 431,
    "0.8870095545",
                    0.88700955448998
  ],
  [
                                 432,
    "0.8874592939",
                    0.88745929388676
  ],
  [
                                 433,
    "0.8890389995",
                    0.88903899951327
  ],
  [
                                 434,
    "0.8911287784",
                    0.89112877840694
  ],
  [
                                 435,
    "0.892701324",
                    0.89270132402549
  ],
  [
                                 436,
    "0.893638656",
                    0.89363865595946
  ],
  [
                                 437,
    "0.8939921334",
                    0.89399213338922
  ],
  [
                                 438,
    "0.9002775321",
                    0.90027753212502
  ],
  [
                                 439,
    "0.9019281095",
                    0.90192810953684
  ],
  [
                                 440,
    "0.9037487516",
                    0.90374875157314
  ],
  [
                                 441,
    "0.9047026345",
                    0.90470263450625
  ],
  [
                                 442,
    "0.9050359586",
                    0.90503595858115
  ],
  [
                                 443,
    "0.9053011983",
                     0.9053011983192
  ],
  [
                                 444,
    "0.9054982611",
                    0.90549826105381
  ],
  [
                                 445,
    "0.9057884239",
                    0.90578842391529
  ],
  [
                                 446,
    "0.9078596592",
                    0.90785965924517
  ],
  [
                                 447,
    "0.9101276025",
                    0.91012760247575
  ],
  [
                                 448,
    "0.9110809797",
                     0.9110809797007
  ],
  [
                                 449,
    "0.9136646748",
                    0.91366467481184
  ],
  [
                                 450,
    "0.9159638099",
                     0.9159638098981
  ],
  [
                                 451,
    "0.9166537476",
                    0.91665374763154
  ],
  [
                                 452,
    "0.9186017285",
                     0.9186017284722
  ],
  [
                                 453,
    "0.919220488",
                    0.91922048801519
  ],
  [
                                 454,
    "0.9212328652",
                    0.92123286515531
  ],
  [
                                 455,
    "0.9217749945",
                    0.92177499454551
  ],
  [
                                 456,
    "0.9218236827",
                    0.92182368269275
  ],
  [
                                 457,
    "0.9225795371",
                    0.92257953710974
  ],
  [
                                 458,
    "0.923005158",
                    0.92300515804579
  ],
  [
                                 459,
    "0.9295114483",
                     0.9295114483356
  ],
  [
                                 460,
    "0.9334455854",
                    0.93344558539495
  ],
  [
                                 461,
    "0.9337331415",
                    0.93373314148454
  ],
  [
                                 462,
    "0.9358259239",
                    0.93582592389351
  ],
  [
                                 463,
    "0.9380399426",
                    0.93803994261568
  ],
  [
                                 464,
    "0.9392540212",
                    0.93925402124377
  ],
  [
                                 465,
    "0.9408260081",
                    0.94082600806878
  ],
  [
                                 466,
    "0.9412178755",
                    0.94121787554641
  ],
  [
                                 467,
    "0.9421049645",
                    0.94210496449009
  ],
  [
                                 468,
    "0.9443434551",
                     0.9443434551099
  ],
  [
                                 469,
    "0.947649973",
                     0.9476499729546
  ],
  [
                                 470,
    "0.9494116972",
                     0.9494116971965
  ],
  [
                                 471,
    "0.9497451694",
                    0.94974516935169
  ],
  [
                                 472,
    "0.9517337316",
                    0.95173373164224
  ],
  [
                                 473,
    "0.9577067233",
                    0.95770672334251
  ],
  [
                                 474,
    "0.9624094944",
                    0.96240949442722
  ],
  [
                                 475,
    "0.9642330622",
                    0.96423306221339
  ],
  [
                                 476,
    "0.9677599529",
                    0.96775995286542
  ],
  [
                                 477,
    "0.9683029465",
                    0.96830294652297
  ],
  [
                                 478,
    "0.9700463307",
                    0.97004633069506
  ],
  [
                                 479,
    "0.9729755972",
                    0.97297559723862
  ],
  [
                                 480,
    "0.9743401203",
                     0.9743401203185
  ],
  [
                                 481,
    "0.977525881",
                    0.97752588101547
  ],
  [
                                 482,
    "0.9788602395",
                    0.97886023948847
  ],
  [
                                 483,
    "0.9829722652",
                    0.98297226521324
  ],
  [
                                 484,
    "0.9838584969",
                     0.9838584968745
  ],
  [
                                 485,
    "0.986392145",
                    0.98639214503876
  ],
  [
                                 486,
    "0.9886501227",
                    0.98865012265213
  ],
  [
                                 487,
    "0.9908347088",
                    0.99083470878695
  ],
  [
                                 488,
    "0.9918390666",
                    0.99183906660966
  ],
  [
                                 489,
    "0.9933680082",
                    0.99336800817091
  ],
  [
                                 490,
    "0.9935915098",
                    0.99359150975644
  ],
  [
                                 491,
    "0.9990555178",
                    0.99905551783696
  ],
  [
                                 492,
    "1613.001",
                            1613.001
  ],
  [
                                 493,
    "1613.0011235",
                      1613.001123456
  ],
  [
                                 494,
    "42",
                                  42
  ]
]
, "dynamic_extended_values":
[
  [
    null,
    "",
    "",
    "",
    null,
    "",
    null
  ]
]
,"_DEBUG" : ""
,"_METAUSER": "sasdemo@SAS"
,"_METAPERSON": "sasdemo"
,"_PROGRAM" : "/Projects/app/dc/services/editors/getdynamiccolvals"
,"AUTOEXEC" : "D%3A%5Copt%5Csasinside%5CConfig%5CLev1%5CSASApp%5CStoredProcessServer%5Cautoexec.sas"
,"MF_GETUSER" : "sasdemo"
,"SYSCC" : "0"
,"SYSENCODING" : "wlatin1"
,"SYSERRORTEXT" : ""
,"SYSHOSTNAME" : "SAS"
,"SYSPROCESSID" : "41DD815F932A6E9840B6ED0000000000"
,"SYSPROCESSMODE" : "SAS Stored Process Server"
,"SYSPROCESSNAME" : ""
,"SYSJOBID" : "14432"
,"SYSSCPL" : "Linunx"
,"SYSSITE" : "123"
,"SYSUSERID" : "sassrv"
,"SYSVLONG" : "9.04.01M7P080520"
,"SYSWARNINGTEXT" : "ENCODING option ignored for files opened with RECFM=N."
,"END_DTTM" : "2022-09-29T11:51:41.486000"
,"MEMSIZE" : "93GB"
}
`

if (_WEBIN_FILENAME1.includes('SASControlTable')) jsDataFileText = _WEBIN_FILEREF1.toString()
if (_WEBIN_FILENAME2.includes('SASControlTable')) jsDataFileText = _WEBIN_FILEREF2.toString()

if (jsDataFileText.length > 0) {
    if (jsDataFileText.includes('MPE_TABLES')) {
        _webout = mpe_tables
    } else {
        _webout = other
    }
} else {
    console.log('Error reading jsdata input')
}