_webout = `{"SYSDATE" : "26SEP22"
,"SYSTIME" : "08:50"
, "types":
[
{
"ID": "AccessControlEntry",
"DESC": "Access control entry",
"HASSUBTYPES": "0"
},
{
"ID": "AccessControlTemplate",
"DESC": "Access control template",
"HASSUBTYPES": "0"
},
{
"ID": "Action",
"DESC": "Action",
"HASSUBTYPES": "0"
},
{
"ID": "AnalyticContext",
"DESC": "Analytic context",
"HASSUBTYPES": "0"
},
{
"ID": "ApplicationAction",
"DESC": "Application action",
"HASSUBTYPES": "0"
},
{
"ID": "AssociationProperty",
"DESC": "Association property",
"HASSUBTYPES": "0"
},
{
"ID": "AttributeProperty",
"DESC": "Attribute property",
"HASSUBTYPES": "0"
},
{
"ID": "AuthenticationDomain",
"DESC": "Authentication domain",
"HASSUBTYPES": "0"
},
{
"ID": "ClassifierMap",
"DESC": "Classifier map",
"HASSUBTYPES": "1"
},
{
"ID": "Column",
"DESC": "Column",
"HASSUBTYPES": "1"
},
{
"ID": "Condition",
"DESC": "Condition",
"HASSUBTYPES": "0"
},
{
"ID": "ConditionActionSet",
"DESC": "Condition action set",
"HASSUBTYPES": "0"
},
{
"ID": "ConfiguredComponent",
"DESC": "Configured component",
"HASSUBTYPES": "1"
},
{
"ID": "CustomAssociation",
"DESC": "Custom association",
"HASSUBTYPES": "0"
},
{
"ID": "DatabaseCatalog",
"DESC": "Database catalog",
"HASSUBTYPES": "0"
},
{
"ID": "DatabaseSchema",
"DESC": "Database schema",
"HASSUBTYPES": "0"
},
{
"ID": "DeployedComponent",
"DESC": "Deployed component",
"HASSUBTYPES": "1"
},
{
"ID": "Directory",
"DESC": "Directory",
"HASSUBTYPES": "1"
},
{
"ID": "Document",
"DESC": "Document",
"HASSUBTYPES": "0"
},
{
"ID": "Extension",
"DESC": "Extension",
"HASSUBTYPES": "0"
},
{
"ID": "ExternalIdentity",
"DESC": "External identity",
"HASSUBTYPES": "0"
},
{
"ID": "FavoritesContainer",
"DESC": "Favorites container",
"HASSUBTYPES": "0"
},
{
"ID": "File",
"DESC": "File",
"HASSUBTYPES": "1"
},
{
"ID": "Group",
"DESC": "Group",
"HASSUBTYPES": "1"
},
{
"ID": "ITChannel",
"DESC": "IT channel",
"HASSUBTYPES": "0"
},
{
"ID": "IdentityGroup",
"DESC": "Identity group",
"HASSUBTYPES": "0"
},
{
"ID": "Index",
"DESC": "Index",
"HASSUBTYPES": "0"
},
{
"ID": "InternalLogin",
"DESC": "Internal login",
"HASSUBTYPES": "0"
},
{
"ID": "Job",
"DESC": "Job",
"HASSUBTYPES": "0"
},
{
"ID": "Keyword",
"DESC": "Keyword",
"HASSUBTYPES": "0"
},
{
"ID": "LogicalServer",
"DESC": "Logical server",
"HASSUBTYPES": "0"
},
{
"ID": "Login",
"DESC": "Login",
"HASSUBTYPES": "0"
},
{
"ID": "Machine",
"DESC": "Machine",
"HASSUBTYPES": "0"
},
{
"ID": "NamedService",
"DESC": "Named service",
"HASSUBTYPES": "0"
},
{
"ID": "OLAPSchema",
"DESC": "OLAP schema",
"HASSUBTYPES": "0"
},
{
"ID": "PSColumnLayoutComponent",
"DESC": "PS column layout component",
"HASSUBTYPES": "0"
},
{
"ID": "PSPortalPage",
"DESC": "PS portal page",
"HASSUBTYPES": "0"
},
{
"ID": "PSPortlet",
"DESC": "PS portlet",
"HASSUBTYPES": "0"
},
{
"ID": "Permission",
"DESC": "Permission",
"HASSUBTYPES": "0"
},
{
"ID": "Person",
"DESC": "People",
"HASSUBTYPES": "0"
},
{
"ID": "PhysicalTable",
"DESC": "Physical table",
"HASSUBTYPES": "1"
},
{
"ID": "Prompt",
"DESC": "Prompt",
"HASSUBTYPES": "0"
},
{
"ID": "PromptGroup",
"DESC": "Prompt group",
"HASSUBTYPES": "0"
},
{
"ID": "Property",
"DESC": "Property",
"HASSUBTYPES": "0"
},
{
"ID": "PropertyGroup",
"DESC": "Property group",
"HASSUBTYPES": "0"
},
{
"ID": "PropertySet",
"DESC": "Property set",
"HASSUBTYPES": "0"
},
{
"ID": "PropertyType",
"DESC": "Property type",
"HASSUBTYPES": "0"
},
{
"ID": "Prototype",
"DESC": "Prototype",
"HASSUBTYPES": "0"
},
{
"ID": "Report",
"DESC": "Report",
"HASSUBTYPES": "0"
},
{
"ID": "ResponsibleParty",
"DESC": "Responsible party",
"HASSUBTYPES": "0"
},
{
"ID": "SASClientConnection",
"DESC": "SAS client connection",
"HASSUBTYPES": "0"
},
{
"ID": "SASLibrary",
"DESC": "SAS library",
"HASSUBTYPES": "1"
},
{
"ID": "SASPassword",
"DESC": "SAS password",
"HASSUBTYPES": "0"
},
{
"ID": "Search",
"DESC": "Search",
"HASSUBTYPES": "0"
},
{
"ID": "SecurityRuleScheme",
"DESC": "Security rule scheme",
"HASSUBTYPES": "0"
},
{
"ID": "SecurityTypeContainmentRule",
"DESC": "Security type containment rule",
"HASSUBTYPES": "0"
},
{
"ID": "ServerComponent",
"DESC": "Server component",
"HASSUBTYPES": "1"
},
{
"ID": "ServerContext",
"DESC": "Server context",
"HASSUBTYPES": "0"
},
{
"ID": "ServiceComponent",
"DESC": "Service component",
"HASSUBTYPES": "0"
},
{
"ID": "ServiceType",
"DESC": "Service type",
"HASSUBTYPES": "0"
},
{
"ID": "SoftwareComponent",
"DESC": "Software component",
"HASSUBTYPES": "1"
},
{
"ID": "Stream",
"DESC": "Stream",
"HASSUBTYPES": "0"
},
{
"ID": "TCPIPConnection",
"DESC": "TCPIP connection",
"HASSUBTYPES": "0"
},
{
"ID": "TextStore",
"DESC": "Text store",
"HASSUBTYPES": "0"
},
{
"ID": "Timestamp",
"DESC": "Timestamp",
"HASSUBTYPES": "0"
},
{
"ID": "Transformation",
"DESC": "Transformation",
"HASSUBTYPES": "1"
},
{
"ID": "TransformationActivity",
"DESC": "Transformation activity",
"HASSUBTYPES": "0"
},
{
"ID": "TransformationStep",
"DESC": "Transformation step",
"HASSUBTYPES": "1"
},
{
"ID": "Tree",
"DESC": "Metadata Trees",
"HASSUBTYPES": "0"
},
{
"ID": "TypeDefinition",
"DESC": "Type definition",
"HASSUBTYPES": "0"
},
{
"ID": "UniqueKey",
"DESC": "Unique key",
"HASSUBTYPES": "0"
}
]
,"_DEBUG" : ""
,"_METAUSER": "sasdemo@SAS"
,"_METAPERSON": "sasdemo"
,"_PROGRAM" : "/Projects/app/dc/services/metanav/metatypes"
,"AUTOEXEC" : "D%3A%5Copt%5Csasinside%5CConfig%5CLev1%5CSASApp%5CStoredProcessServer%5Cautoexec.sas"
,"MF_GETUSER" : "sasdemo"
,"SYSCC" : "0"
,"SYSENCODING" : "wlatin1"
,"SYSERRORTEXT" : ""
,"SYSHOSTNAME" : "SAS"
,"SYSPROCESSID" : "41DD8057D3E020C540C10F8000000000"
,"SYSPROCESSMODE" : "SAS Stored Process Server"
,"SYSPROCESSNAME" : ""
,"SYSJOBID" : "27448"
,"SYSSCPL" : "Linunx"
,"SYSSITE" : "123"
,"SYSUSERID" : "sassrv"
,"SYSVLONG" : "9.04.01M7P080520"
,"SYSWARNINGTEXT" : "ENCODING option ignored for files opened with RECFM=N."
,"END_DTTM" : "2022-09-26T08:50:23.928000"
,"MEMSIZE" : "46GB"
}
`