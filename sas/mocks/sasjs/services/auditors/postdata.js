_webout = `{"SYSDATE" : "07OCT22"
,"SYSTIME" : "12:25"
, "params":
[
{"TABLE_ID":"DC20221007T122326121_612316_7259" ,"SUBMIT_STATUS_CD":"SUBMITTED" ,"BASE_LIB":"DC988196" ,"BASE_DS":"MPE_X_TEST" ,"SUBMITTED_BY_NM":"mihajlo" ,"SUBMITTED_ON_DTTM":1980764606.22 ,"SUBMITTED_REASON_TXT":"" ,"INPUT_OBS":15 ,"INPUT_VARS":10 ,"NUM_OF_APPROVALS_REQUIRED":1 ,"NUM_OF_APPROVALS_REMAINING":1 ,"REVIEWED_BY_NM":"" ,"REVIEWED_ON_DTTM":null ,"LIBDS":"DC988196.MPE_X_TEST" ,"REVIEWED_ON":"." ,"DIFFTIME":"07OCT22:12:25:57.40" ,"DIFFS_CSV":"tempDiffs_20221007_122559.csv" ,"FILESIZE":"684.0KB" ,"FILESIZE_RAW":700416 ,"TRUNCATED":"NO" ,"NUM_ADDED":6 ,"NUM_DELETED":4 ,"NUM_UPDATED":5 ,"SUBMITTED_ON":"07OCT2022:12:23:26" ,"ISAPPROVER":"YES" }
]
, "cols":
[
{"NAME":"PRIMARY_KEY_FIELD" ,"TYPE":1 ,"LENGTH":8 ,"FORMAT":"" }
,{"NAME":"SOME_CHAR" ,"TYPE":2 ,"LENGTH":32767 ,"FORMAT":"" }
,{"NAME":"SOME_DROPDOWN" ,"TYPE":2 ,"LENGTH":128 ,"FORMAT":"" }
,{"NAME":"SOME_NUM" ,"TYPE":1 ,"LENGTH":8 ,"FORMAT":"" }
,{"NAME":"SOME_DATE" ,"TYPE":1 ,"LENGTH":8 ,"FORMAT":"DATE" }
,{"NAME":"SOME_DATETIME" ,"TYPE":1 ,"LENGTH":8 ,"FORMAT":"DATETIME" }
,{"NAME":"SOME_TIME" ,"TYPE":1 ,"LENGTH":8 ,"FORMAT":"TIME" }
,{"NAME":"SOME_SHORTNUM" ,"TYPE":1 ,"LENGTH":4 ,"FORMAT":"" }
,{"NAME":"SOME_BESTNUM" ,"TYPE":1 ,"LENGTH":8 ,"FORMAT":"BEST" }
]
, "submits":
[
{"TABLE_ID":"DC20221007T122119628_344949_7254" ,"SUBMIT_STATUS_CD":"SUBMITTED" ,"BASE_LIB":"DC988196" ,"BASE_DS":"MPE_X_TEST" ,"SUBMITTED_BY_NM":"mihajlo" ,"SUBMITTED_ON_DTTM":1980764479.74 ,"SUBMITTED_REASON_TXT":"" ,"INPUT_OBS":15 ,"INPUT_VARS":10 ,"NUM_OF_APPROVALS_REQUIRED":1 ,"NUM_OF_APPROVALS_REMAINING":1 ,"REVIEWED_BY_NM":"" ,"REVIEWED_ON_DTTM":null }
,{"TABLE_ID":"DC20221006T142649516_059582_7169" ,"SUBMIT_STATUS_CD":"SUBMITTED" ,"BASE_LIB":"DC988196" ,"BASE_DS":"MPE_X_TEST" ,"SUBMITTED_BY_NM":"mihajlo" ,"SUBMITTED_ON_DTTM":1980685609.6 ,"SUBMITTED_REASON_TXT":"" ,"INPUT_OBS":1 ,"INPUT_VARS":10 ,"NUM_OF_APPROVALS_REQUIRED":1 ,"NUM_OF_APPROVALS_REMAINING":1 ,"REVIEWED_BY_NM":"" ,"REVIEWED_ON_DTTM":null }
]
, "deleted":
[
{"PRIMARY_KEY_FIELD":1016 ,"SOME_CHAR":"16 bottles of beer on the wall" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":0.6963824517 ,"SOME_DATE":790 ,"SOME_DATETIME":null ,"SOME_TIME":60 ,"SOME_SHORTNUM":27 ,"SOME_BESTNUM":84 }
,{"PRIMARY_KEY_FIELD":1017 ,"SOME_CHAR":"17 bottles of beer on the wall" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":0.476432963 ,"SOME_DATE":432 ,"SOME_DATETIME":null ,"SOME_TIME":60 ,"SOME_SHORTNUM":6 ,"SOME_BESTNUM":93 }
,{"PRIMARY_KEY_FIELD":1018 ,"SOME_CHAR":"18 bottles of beer on the wall" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":0.498669565 ,"SOME_DATE":754 ,"SOME_DATETIME":null ,"SOME_TIME":0 ,"SOME_SHORTNUM":87 ,"SOME_BESTNUM":29 }
,{"PRIMARY_KEY_FIELD":1019 ,"SOME_CHAR":"19 bottles of beer on the wall" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":0.7999903256 ,"SOME_DATE":492 ,"SOME_DATETIME":null ,"SOME_TIME":60 ,"SOME_SHORTNUM":4 ,"SOME_BESTNUM":15 }
]
, "new":
[
{"PRIMARY_KEY_FIELD":10101234 ,"SOME_CHAR":"10 bottles of beer on the wall" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":0.3297298762 ,"SOME_DATE":728 ,"SOME_DATETIME":null ,"SOME_TIME":60 ,"SOME_SHORTNUM":52 ,"SOME_BESTNUM":23 }
,{"PRIMARY_KEY_FIELD":10101235 ,"SOME_CHAR":"11 bottles of beer on the wall" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":0.4249602912 ,"SOME_DATE":456 ,"SOME_DATETIME":null ,"SOME_TIME":60 ,"SOME_SHORTNUM":37 ,"SOME_BESTNUM":91 }
,{"PRIMARY_KEY_FIELD":10101236 ,"SOME_CHAR":"12 bottles of beer on the wall" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":0.5175091785 ,"SOME_DATE":383 ,"SOME_DATETIME":null ,"SOME_TIME":0 ,"SOME_SHORTNUM":53 ,"SOME_BESTNUM":5 }
,{"PRIMARY_KEY_FIELD":10101237 ,"SOME_CHAR":"13 bottles of beer on the wall" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":0.1282490171 ,"SOME_DATE":650 ,"SOME_DATETIME":20160 ,"SOME_TIME":0 ,"SOME_SHORTNUM":74 ,"SOME_BESTNUM":88 }
,{"PRIMARY_KEY_FIELD":10101238 ,"SOME_CHAR":"14 bottles of beer on the wall" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":0.9579004319 ,"SOME_DATE":203 ,"SOME_DATETIME":20160 ,"SOME_TIME":0 ,"SOME_SHORTNUM":51 ,"SOME_BESTNUM":65 }
,{"PRIMARY_KEY_FIELD":10101239 ,"SOME_CHAR":"15 bottles of beer on the wall" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":0.4804529285 ,"SOME_DATE":155 ,"SOME_DATETIME":23820 ,"SOME_TIME":0 ,"SOME_SHORTNUM":28 ,"SOME_BESTNUM":88 }
]
, "updates":
[
{"PRIMARY_KEY_FIELD":0 ,"SOME_CHAR":"updated row" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":42 ,"SOME_DATE":42 ,"SOME_DATETIME":null ,"SOME_TIME":0 ,"SOME_SHORTNUM":3 ,"SOME_BESTNUM":44 }
,{"PRIMARY_KEY_FIELD":1 ,"SOME_CHAR":"updated row" ,"SOME_DROPDOWN":"Option 2" ,"SOME_NUM":42 ,"SOME_DATE":42 ,"SOME_DATETIME":null ,"SOME_TIME":420 ,"SOME_SHORTNUM":3 ,"SOME_BESTNUM":44 }
,{"PRIMARY_KEY_FIELD":2 ,"SOME_CHAR":"updated row" ,"SOME_DROPDOWN":"Option 3" ,"SOME_NUM":42 ,"SOME_DATE":42 ,"SOME_DATETIME":null ,"SOME_TIME":120 ,"SOME_SHORTNUM":3 ,"SOME_BESTNUM":44 }
,{"PRIMARY_KEY_FIELD":3 ,"SOME_CHAR":"updated row" ,"SOME_DROPDOWN":"Option 2" ,"SOME_NUM":1613.001 ,"SOME_DATE":423 ,"SOME_DATETIME":null ,"SOME_TIME":0 ,"SOME_SHORTNUM":3 ,"SOME_BESTNUM":44 }
,{"PRIMARY_KEY_FIELD":4 ,"SOME_CHAR":"updated row" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":1613.0011235 ,"SOME_DATE":4231 ,"SOME_DATETIME":null ,"SOME_TIME":360 ,"SOME_SHORTNUM":3 ,"SOME_BESTNUM":44 }
]
, "originals":
[
{"PRIMARY_KEY_FIELD":0 ,"SOME_CHAR":"this is dummy data" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":42 ,"SOME_DATE":42 ,"SOME_DATETIME":42 ,"SOME_TIME":42 ,"SOME_SHORTNUM":3 ,"SOME_BESTNUM":44 }
,{"PRIMARY_KEY_FIELD":1 ,"SOME_CHAR":"more dummy data" ,"SOME_DROPDOWN":"Option 2" ,"SOME_NUM":42 ,"SOME_DATE":42 ,"SOME_DATETIME":42 ,"SOME_TIME":422 ,"SOME_SHORTNUM":3 ,"SOME_BESTNUM":44 }
,{"PRIMARY_KEY_FIELD":2 ,"SOME_CHAR":"even more dummy data" ,"SOME_DROPDOWN":"Option 3" ,"SOME_NUM":42 ,"SOME_DATE":42 ,"SOME_DATETIME":42 ,"SOME_TIME":142 ,"SOME_SHORTNUM":3 ,"SOME_BESTNUM":44 }
,{"PRIMARY_KEY_FIELD":3 ,"SOME_CHAR":"It was a dark and stormy night.  The wind was blowing a gale!  The captain said to his mate - mate, tell us a tale.  And this, is the tale he told: It was a dark and stormy night.  The wind was blowing a gale!  The captain said to his mate - mate, tell us a tale.  And this, is the tale he told: It was a dark and stormy night.  The wind was blowing a gale!  The captain said to his mate - mate, tell us a tale.  And this, is the tale he told: It was a dark and stormy night.  The wind was blowing a gale!  The captain said to his mate - mate, tell us a tale.  And this, is the tale he told:" ,"SOME_DROPDOWN":"Option 2" ,"SOME_NUM":1613.001 ,"SOME_DATE":423 ,"SOME_DATETIME":423 ,"SOME_TIME":44 ,"SOME_SHORTNUM":3 ,"SOME_BESTNUM":44 }
,{"PRIMARY_KEY_FIELD":4 ,"SOME_CHAR":"if you can fill the unforgiving minute" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":1613.001123456 ,"SOME_DATE":4231 ,"SOME_DATETIME":423123123 ,"SOME_TIME":412 ,"SOME_SHORTNUM":3 ,"SOME_BESTNUM":44 }
]
, "fmt_deleted":
[
{"PRIMARY_KEY_FIELD":"1016" ,"SOME_CHAR":"16 bottles of beer on the wall" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":"0.6963824517" ,"SOME_DATE":"01MAR1962" ,"SOME_DATETIME":"." ,"SOME_TIME":"0:01:00" ,"SOME_SHORTNUM":"27" ,"SOME_BESTNUM":"84" }
,{"PRIMARY_KEY_FIELD":"1017" ,"SOME_CHAR":"17 bottles of beer on the wall" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":"0.476432963" ,"SOME_DATE":"08MAR1961" ,"SOME_DATETIME":"." ,"SOME_TIME":"0:01:00" ,"SOME_SHORTNUM":"6" ,"SOME_BESTNUM":"93" }
,{"PRIMARY_KEY_FIELD":"1018" ,"SOME_CHAR":"18 bottles of beer on the wall" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":"0.498669565" ,"SOME_DATE":"24JAN1962" ,"SOME_DATETIME":"." ,"SOME_TIME":"0:00:00" ,"SOME_SHORTNUM":"87" ,"SOME_BESTNUM":"29" }
,{"PRIMARY_KEY_FIELD":"1019" ,"SOME_CHAR":"19 bottles of beer on the wall" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":"0.7999903256" ,"SOME_DATE":"07MAY1961" ,"SOME_DATETIME":"." ,"SOME_TIME":"0:01:00" ,"SOME_SHORTNUM":"4" ,"SOME_BESTNUM":"15" }
]
, "fmt_new":
[
{"PRIMARY_KEY_FIELD":"10101234" ,"SOME_CHAR":"10 bottles of beer on the wall" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":"0.3297298762" ,"SOME_DATE":"29DEC1961" ,"SOME_DATETIME":"." ,"SOME_TIME":"0:01:00" ,"SOME_SHORTNUM":"52" ,"SOME_BESTNUM":"23" }
,{"PRIMARY_KEY_FIELD":"10101235" ,"SOME_CHAR":"11 bottles of beer on the wall" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":"0.4249602912" ,"SOME_DATE":"01APR1961" ,"SOME_DATETIME":"." ,"SOME_TIME":"0:01:00" ,"SOME_SHORTNUM":"37" ,"SOME_BESTNUM":"91" }
,{"PRIMARY_KEY_FIELD":"10101236" ,"SOME_CHAR":"12 bottles of beer on the wall" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":"0.5175091785" ,"SOME_DATE":"18JAN1961" ,"SOME_DATETIME":"." ,"SOME_TIME":"0:00:00" ,"SOME_SHORTNUM":"53" ,"SOME_BESTNUM":"5" }
,{"PRIMARY_KEY_FIELD":"10101237" ,"SOME_CHAR":"13 bottles of beer on the wall" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":"0.1282490171" ,"SOME_DATE":"12OCT1961" ,"SOME_DATETIME":"01JAN1960:05:36:00" ,"SOME_TIME":"0:00:00" ,"SOME_SHORTNUM":"74" ,"SOME_BESTNUM":"88" }
,{"PRIMARY_KEY_FIELD":"10101238" ,"SOME_CHAR":"14 bottles of beer on the wall" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":"0.9579004319" ,"SOME_DATE":"22JUL1960" ,"SOME_DATETIME":"01JAN1960:05:36:00" ,"SOME_TIME":"0:00:00" ,"SOME_SHORTNUM":"51" ,"SOME_BESTNUM":"65" }
,{"PRIMARY_KEY_FIELD":"10101239" ,"SOME_CHAR":"15 bottles of beer on the wall" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":"0.4804529285" ,"SOME_DATE":"04JUN1960" ,"SOME_DATETIME":"01JAN1960:06:37:00" ,"SOME_TIME":"0:00:00" ,"SOME_SHORTNUM":"28" ,"SOME_BESTNUM":"88" }
]
, "fmt_updates":
[
{"PRIMARY_KEY_FIELD":"0" ,"SOME_CHAR":"updated row" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":"42" ,"SOME_DATE":"12FEB1960" ,"SOME_DATETIME":"." ,"SOME_TIME":"0:00:00" ,"SOME_SHORTNUM":"3" ,"SOME_BESTNUM":"44" }
,{"PRIMARY_KEY_FIELD":"1" ,"SOME_CHAR":"updated row" ,"SOME_DROPDOWN":"Option 2" ,"SOME_NUM":"42" ,"SOME_DATE":"12FEB1960" ,"SOME_DATETIME":"." ,"SOME_TIME":"0:07:00" ,"SOME_SHORTNUM":"3" ,"SOME_BESTNUM":"44" }
,{"PRIMARY_KEY_FIELD":"2" ,"SOME_CHAR":"updated row" ,"SOME_DROPDOWN":"Option 3" ,"SOME_NUM":"42" ,"SOME_DATE":"12FEB1960" ,"SOME_DATETIME":"." ,"SOME_TIME":"0:02:00" ,"SOME_SHORTNUM":"3" ,"SOME_BESTNUM":"44" }
,{"PRIMARY_KEY_FIELD":"3" ,"SOME_CHAR":"updated row" ,"SOME_DROPDOWN":"Option 2" ,"SOME_NUM":"1613.001" ,"SOME_DATE":"27FEB1961" ,"SOME_DATETIME":"." ,"SOME_TIME":"0:00:00" ,"SOME_SHORTNUM":"3" ,"SOME_BESTNUM":"44" }
,{"PRIMARY_KEY_FIELD":"4" ,"SOME_CHAR":"updated row" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":"1613.0011235" ,"SOME_DATE":"02AUG1971" ,"SOME_DATETIME":"." ,"SOME_TIME":"0:06:00" ,"SOME_SHORTNUM":"3" ,"SOME_BESTNUM":"44" }
]
, "fmt_originals":
[
{"PRIMARY_KEY_FIELD":"0" ,"SOME_CHAR":"this is dummy data" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":"42" ,"SOME_DATE":"12FEB1960" ,"SOME_DATETIME":"01JAN1960:00:00:42" ,"SOME_TIME":"0:00:42" ,"SOME_SHORTNUM":"3" ,"SOME_BESTNUM":"44" }
,{"PRIMARY_KEY_FIELD":"1" ,"SOME_CHAR":"more dummy data" ,"SOME_DROPDOWN":"Option 2" ,"SOME_NUM":"42" ,"SOME_DATE":"12FEB1960" ,"SOME_DATETIME":"01JAN1960:00:00:42" ,"SOME_TIME":"0:07:02" ,"SOME_SHORTNUM":"3" ,"SOME_BESTNUM":"44" }
,{"PRIMARY_KEY_FIELD":"2" ,"SOME_CHAR":"even more dummy data" ,"SOME_DROPDOWN":"Option 3" ,"SOME_NUM":"42" ,"SOME_DATE":"12FEB1960" ,"SOME_DATETIME":"01JAN1960:00:00:42" ,"SOME_TIME":"0:02:22" ,"SOME_SHORTNUM":"3" ,"SOME_BESTNUM":"44" }
,{"PRIMARY_KEY_FIELD":"3" ,"SOME_CHAR":"It was a dark and stormy night.  The wind was blowing a gale!  The captain said to his mate - mate, tell us a tale.  And this, is the tale he told: It was a dark and stormy night.  The wind was blowing a gale!  The captain said to his mate - mate, tell us a tale.  And this, is the tale he told: It was a dark and stormy night.  The wind was blowing a gale!  The captain said to his mate - mate, tell us a tale.  And this, is the tale he told: It was a dark and stormy night.  The wind was blowing a gale!  The captain said to his mate - mate, tell us a tale.  And this, is the tale he told:" ,"SOME_DROPDOWN":"Option 2" ,"SOME_NUM":"1613.001" ,"SOME_DATE":"27FEB1961" ,"SOME_DATETIME":"01JAN1960:00:07:03" ,"SOME_TIME":"0:00:44" ,"SOME_SHORTNUM":"3" ,"SOME_BESTNUM":"44" }
,{"PRIMARY_KEY_FIELD":"4" ,"SOME_CHAR":"if you can fill the unforgiving minute" ,"SOME_DROPDOWN":"Option 1" ,"SOME_NUM":"1613.0011235" ,"SOME_DATE":"02AUG1971" ,"SOME_DATETIME":"29MAY1973:06:12:03" ,"SOME_TIME":"0:06:52" ,"SOME_SHORTNUM":"3" ,"SOME_BESTNUM":"44" }
]
,"_DEBUG" : ""
,"_PROGRAM" : "/30.SASApps/app/mihajlo/services/auditors/postdata"
,"AUTOEXEC" : "%2Fhome%2Fmihajlo%2Fsasjs_root%2Fsessions%2F20221007122541-37053-1665145541605%2Fautoexec.sas"
,"MF_GETUSER" : "mihajlo"
,"SYSCC" : "0"
,"SYSENCODING" : "utf-8"
,"SYSERRORTEXT" : ""
,"SYSHOSTINFOLONG" : ""
,"SYSHOSTNAME" : "sas.4gl.io"
,"SYSPROCESSID" : "41DD84049178EB530000000000000000"
,"SYSPROCESSMODE" : "Stored Program"
,"SYSPROCESSNAME" : ""
,"SYSJOBID" : "726676"
,"SYSSCPL" : "LINUX"
,"syssite" : "123"
,"SYSTCPIPHOSTNAME" : "https://sas.4gl.io:5002"
,"SYSUSERID" : "mihajlo"
,"SYSVLONG" : "05.00.00.02.001146"
,"SYSWARNINGTEXT" : ""
,"END_DTTM" : "2022-10-07T12:26:00.209420"
,"MEMSIZE" : "0KB"
}
`