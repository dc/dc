## [6.14.1](https://git.datacontroller.io/dc/dc/compare/v6.14.0...v6.14.1) (2025-03-05)


### Bug Fixes

* handle national language datetime formats ([149e318](https://git.datacontroller.io/dc/dc/commit/149e318a8787be0109f25aeec3a1270ea75a97b2))
* updating logic to use NLDAT formats ([95289aa](https://git.datacontroller.io/dc/dc/commit/95289aa9524d3cb2b1c248cfb84f6b0d0a490c32))

# [6.14.0](https://git.datacontroller.io/dc/dc/compare/v6.13.2...v6.14.0) (2025-02-26)


### Features

* uses SORTSEQ=LINGUISTIC for the services/metanav/metadetails service ([a45f5bb](https://git.datacontroller.io/dc/dc/commit/a45f5bb3b27a86da5f55ff28c9c7669956484ddf))

## [6.13.2](https://git.datacontroller.io/dc/dc/compare/v6.13.1...v6.13.2) (2025-02-26)


### Bug Fixes

* get metadata email if exists ([1bd0eef](https://git.datacontroller.io/dc/dc/commit/1bd0eef913593579771c647d80010ce628c00819))

## [6.13.1](https://git.datacontroller.io/dc/dc/compare/v6.13.0...v6.13.1) (2025-02-18)


### Bug Fixes

* Avoiding LATIN1 unprintables in various UI locations ([bce1fd5](https://git.datacontroller.io/dc/dc/commit/bce1fd57ef397cfdd030552c3f424a8407174729))
* updated @sasjs/adapter, crypto-browserify ([4e64f28](https://git.datacontroller.io/dc/dc/commit/4e64f28732868b07ec2ab403912bd384c62c7e1d))

# [6.13.0](https://git.datacontroller.io/dc/dc/compare/v6.12.3...v6.13.0) (2025-01-31)


### Bug Fixes

* editor page csv upload ([217220f](https://git.datacontroller.io/dc/dc/commit/217220ffaaf688133321cc68d770aaf1e50590a9))


### Features

* csv test ([c53ab85](https://git.datacontroller.io/dc/dc/commit/c53ab85107f10c023117a099cc06321afc3e1f03))

## [6.12.3](https://git.datacontroller.io/dc/dc/compare/v6.12.2...v6.12.3) (2025-01-27)


### Bug Fixes

* adding missing=STRING to three services ([30d5e51](https://git.datacontroller.io/dc/dc/commit/30d5e51d0b9cf27774038476bd90559600952304))

## [6.12.2](https://git.datacontroller.io/dc/dc/compare/v6.12.1...v6.12.2) (2025-01-27)


### Bug Fixes

* unnecessary zeros when adding new row (data schema default values) ([a1a9051](https://git.datacontroller.io/dc/dc/commit/a1a90519c535ca25e00822b4d3358c991ac9662e))

## [6.12.1](https://git.datacontroller.io/dc/dc/compare/v6.12.0...v6.12.1) (2024-12-31)


### Bug Fixes

* no upcase of pk fields in MPE_TABLES in delete scenario ([3de095f](https://git.datacontroller.io/dc/dc/commit/3de095fe7797cde60f0e232c188305fe423c27eb)), closes [#134](https://git.datacontroller.io/dc/dc/issues/134)
* reduce length of tmp table names.  Closes [#130](https://git.datacontroller.io/dc/dc/issues/130) ([f9c2491](https://git.datacontroller.io/dc/dc/commit/f9c2491ab6e7b528b7ffc011fd9e45c963c5f6bf))

# [6.12.0](https://git.datacontroller.io/dc/dc/compare/v6.11.1...v6.12.0) (2024-09-02)


### Bug Fixes

* added appLoc to the system page ([dd2138a](https://git.datacontroller.io/dc/dc/commit/dd2138ac5e6067de310e83d16fccc9b9764ba3ff))
* bumping core for passthrough fix, [#124](https://git.datacontroller.io/dc/dc/issues/124) ([caa9854](https://git.datacontroller.io/dc/dc/commit/caa9854ff0431ccbb6ff1d6d3509dc877362cceb))
* excel with password flow, introducing web worker for XLSX.read ([a3ce367](https://git.datacontroller.io/dc/dc/commit/a3ce36795007a4e3b6ac3499ffd119dc3758f387))
* implemented the new request wrapper usage, added XLSX read with a Web Worker, multi load preview data, full height ([4218da9](https://git.datacontroller.io/dc/dc/commit/4218da91cd193aa45346ad7e34ccc00ca89df4fb))
* **multi load:** xlsx read file ahead of time, while user choose datasets ([6547461](https://git.datacontroller.io/dc/dc/commit/65474616379e1dacc1329b3bdc5eb14f34428bb1))
* refactored adapter request wrapper function to return job log as well ([67436f4](https://git.datacontroller.io/dc/dc/commit/67436f4ff9bb4d77d5f897f47a3e3d472981f275))
* using temporary names for temporary tables ([ce50365](https://git.datacontroller.io/dc/dc/commit/ce503653cd9fc36f72fb172bd14816e07c792e14)), closes [#124](https://git.datacontroller.io/dc/dc/issues/124)


### Features

* searching data in excel files using new algorithm (massive performance improvement) ([bbb725c](https://git.datacontroller.io/dc/dc/commit/bbb725c64cc23ed701b189623992408c42fdde8f))

## [6.11.1](https://git.datacontroller.io/dc/dc/compare/v6.11.0...v6.11.1) (2024-07-02)


### Bug Fixes

* adding SYSSITE, part of [#116](https://git.datacontroller.io/dc/dc/issues/116) ([a156c01](https://git.datacontroller.io/dc/dc/commit/a156c0111b3de5e3744e38d377d6e9aa09915803))
* ensuring review_reason_txt in output.  Closes [#117](https://git.datacontroller.io/dc/dc/issues/117) ([e5d93fd](https://git.datacontroller.io/dc/dc/commit/e5d93fd7d6d86bc47ff56664bd812b4d9d0749a5))

# [6.11.0](https://git.datacontroller.io/dc/dc/compare/v6.10.1...v6.11.0) (2024-06-27)


### Bug Fixes

* addressing PR comments ([d94df7f](https://git.datacontroller.io/dc/dc/commit/d94df7f0ebae8feab5e1d5cf8011af8c8be2ca18))
* **multi load:** fixed parsing algorithm reused for the multi load, the fix affects the normal upload as well. ([d4fee79](https://git.datacontroller.io/dc/dc/commit/d4fee791a72021e449cf9680c3e3a525dce41ac1))
* **multi load:** label rename ([fa04d7b](https://git.datacontroller.io/dc/dc/commit/fa04d7bf4e5ba337146bdaa926c60488f8851449))


### Features

* **multi load:** added HOT for user datasets input ([18363bb](https://git.datacontroller.io/dc/dc/commit/18363bbbeb9cf96183ba4841da8134b2f66f735c))
* **multi load:** implemented matching libds and parsing of the multiple sheets ([efcdc69](https://git.datacontroller.io/dc/dc/commit/efcdc694dd275cdb9a4e19f26e5522b8dadc5fd9))
* **multi load:** licence submit limits ([cffeab8](https://git.datacontroller.io/dc/dc/commit/cffeab813d8d4b324f82710dfd73953d4cbf8ffe))
* **multi load:** multiple csv files ([4d27665](https://git.datacontroller.io/dc/dc/commit/4d276657b35a147a2233a03afcb1716348555f52))
* **multi load:** refactored range find function, unlocking excel with password is reusable ([eb7c443](https://git.datacontroller.io/dc/dc/commit/eb7c44333c865e7f7bbfb54dd7f73bfc110f86a7))
* **multi load:** submitting multiple found tables at once ([5deba44](https://git.datacontroller.io/dc/dc/commit/5deba44d2b7352866d821b70dbbfbbf54955dc47))

## [6.10.1](https://git.datacontroller.io/dc/dc/compare/v6.10.0...v6.10.1) (2024-06-07)


### Bug Fixes

* adding 60 more colours to crayons table. Closes [#112](https://git.datacontroller.io/dc/dc/issues/112) ([3521579](https://git.datacontroller.io/dc/dc/commit/3521579dead089eebf62455686be3aee88bde687))
* terms and conditions colours, editor on smaller screens show only icons ([e32d44b](https://git.datacontroller.io/dc/dc/commit/e32d44b1bcdfeea43d19b21ec0ddf4af1ce3992a))

# [6.10.0](https://git.datacontroller.io/dc/dc/compare/v6.9.0...v6.10.0) (2024-06-07)


### Features

* updated handsontable to v14 ([2f8d0b7](https://git.datacontroller.io/dc/dc/commit/2f8d0b764a957ad8c11cd1088fad5e0670aa1731))

# [6.9.0](https://git.datacontroller.io/dc/dc/compare/v6.8.5...v6.9.0) (2024-05-31)


### Bug Fixes

* added colors.scss file, start of a refactor ([110ad9a](https://git.datacontroller.io/dc/dc/commit/110ad9a6e9ed39bd5591ae65c2d0005ba47ca758))
* added stealFocus directive ([9a79f37](https://git.datacontroller.io/dc/dc/commit/9a79f37bf143a1e05df7407358e2687c678e3e68))


### Features

* added app settings service to handle theme persistance, fix: optimised dark mode contrast ([35844e0](https://git.datacontroller.io/dc/dc/commit/35844e0cf1a639553269f2ab0f8666a56ab5cc47))
* **dark mode:** clarity optimizations ([afa7e38](https://git.datacontroller.io/dc/dc/commit/afa7e380aa3bdabd380c038522b9d73d9a8a3b91))
* **dark mode:** lineage and metadata ([27907ed](https://git.datacontroller.io/dc/dc/commit/27907ed00fe81f4c752ffe99d2fb029d5c884f0a))
* **dark mode:** refactoring clarity to enable dark mode, added toggle button ([5564aea](https://git.datacontroller.io/dc/dc/commit/5564aea9c25f8e81ff85afa8352325b9992e4043))
* **dark mode:** removing custom css rules so clarity can handle dark/light modes. Handsontable css for dark mode ([2c0afd0](https://git.datacontroller.io/dc/dc/commit/2c0afd02684cdf3bda374731b0359665e00ed95d))

## [6.8.5](https://git.datacontroller.io/dc/dc/compare/v6.8.4...v6.8.5) (2024-05-23)


### Bug Fixes

* bitemporal load issue [#105](https://git.datacontroller.io/dc/dc/issues/105) ([967698e](https://git.datacontroller.io/dc/dc/commit/967698e4ce1e0abcbc6f0aff8a4be6c512dee93c))

## [6.8.4](https://git.datacontroller.io/dc/dc/compare/v6.8.3...v6.8.4) (2024-05-22)


### Bug Fixes

* new approach to fixing [#105](https://git.datacontroller.io/dc/dc/issues/105) ([c11bd9a](https://git.datacontroller.io/dc/dc/commit/c11bd9a2c55e49f10451962cb2e222c21206bce5))

## [6.8.3](https://git.datacontroller.io/dc/dc/compare/v6.8.2...v6.8.3) (2024-05-09)


### Bug Fixes

* updating core to increase filename length, closes [#103](https://git.datacontroller.io/dc/dc/issues/103) ([ee58fd5](https://git.datacontroller.io/dc/dc/commit/ee58fd5b4bc0dd3e3f232c4f26bb85b2e7fe2b54))

## [6.8.2](https://git.datacontroller.io/dc/dc/compare/v6.8.1...v6.8.2) (2024-05-03)


### Bug Fixes

* dc_request_logs option feature ([93758ef](https://git.datacontroller.io/dc/dc/commit/93758efb275966c181f1ee8b6c752010909a0282))
* release process ([c0dc919](https://git.datacontroller.io/dc/dc/commit/c0dc9191e3b95ea6f7e5021fc0bdbcab0af4cc64))

## [6.8.1](https://git.datacontroller.io/dc/dc/compare/v6.8.0...v6.8.1) (2024-05-02)


### Bug Fixes

* hide approve button when table revertable ([ec0f539](https://git.datacontroller.io/dc/dc/commit/ec0f539a337b176c83a661ff520a6892d47efa02))

# [6.8.0](https://git.datacontroller.io/dc/dc/compare/v6.7.0...v6.8.0) (2024-05-02)


### Bug Fixes

* ci sheet lib, submit message auto focus ([c5e4650](https://git.datacontroller.io/dc/dc/commit/c5e46503272f3f3d9cd83ac04225babf79d4de44))
* **clarity:** new version style issues ([8c7de5a](https://git.datacontroller.io/dc/dc/commit/8c7de5aad7e7e32a64769696af9b93eb9a6225d3))
* cypress tests ([3dd85cc](https://git.datacontroller.io/dc/dc/commit/3dd85cc60bd5ac99bc930b6b9c89a8e707e4d51d))
* ensuring that only restorable versions are restorable ([a402856](https://git.datacontroller.io/dc/dc/commit/a4028562ce91b32ff971ab9821328b97cd23f381))
* ensuring version history only includes loaded versions ([51ebd25](https://git.datacontroller.io/dc/dc/commit/51ebd25aa362aa8e66c83b29b2c64aa0f206f5bd))
* final testing on restore feature ([297a84d](https://git.datacontroller.io/dc/dc/commit/297a84d3a4ebb47bef7f3ca9758978d727afed8d))
* issue with multiple adds/deletes, [#84](https://git.datacontroller.io/dc/dc/issues/84) ([904ca30](https://git.datacontroller.io/dc/dc/commit/904ca30f918da085fa05dae066367b512933d1a9))
* load_ref var ([aaad9f7](https://git.datacontroller.io/dc/dc/commit/aaad9f7207115599a006980fff099d59738dd2cd))
* removing alerts dummy data, closes [#93](https://git.datacontroller.io/dc/dc/issues/93) ([eba21e9](https://git.datacontroller.io/dc/dc/commit/eba21e96b4fa34e63b4477281f47d9a01d621f2e))
* restore table version improvement ([549f357](https://git.datacontroller.io/dc/dc/commit/549f35766ba7b5bbe55694845e85bfefc4193375))
* **sas:** viewer versions fix ([c6595c1](https://git.datacontroller.io/dc/dc/commit/c6595c1f618803d9202cba1a1fe76986449cf2e2))
* stage and approve buttons renaming ([ef81e33](https://git.datacontroller.io/dc/dc/commit/ef81e33f704d0b4f99405d96498e1d29ac817982))
* supporting SCD2 data reversions ([fa8396f](https://git.datacontroller.io/dc/dc/commit/fa8396f0394cbddb6dbacb4b355de078fad49980))
* table info modal, versions - column names ([801c8c6](https://git.datacontroller.io/dc/dc/commit/801c8c6a9fb95388a06a6c6284fec4dc25bb77c5))
* **updates:** angular, clarity, resolved legacy-peer-deps ([c60dd65](https://git.datacontroller.io/dc/dc/commit/c60dd65a1637333f11a0c39ef697c7292a6ede07))


### Features

* backend to show in getchangeinfo whether a user is allowed to restore ([8769841](https://git.datacontroller.io/dc/dc/commit/8769841f08694f672ef7ae1a17beacd0dbedda52))
* list versions of target tables (backend) ([f8a14d4](https://git.datacontroller.io/dc/dc/commit/f8a14d4bdef055b99930491d1f6fabe55a50a497))
* restore ([604c2e7](https://git.datacontroller.io/dc/dc/commit/604c2e70bdeeeb1aa5bb18b94f525ebd049397fa))
* SAS services & tests for RESTORE, [#84](https://git.datacontroller.io/dc/dc/issues/84) ([9ad7ae4](https://git.datacontroller.io/dc/dc/commit/9ad7ae47b5e793ce68ab21c9eeb8dee6cb85e496))
* staging page, restore buttons ([02a8a1c](https://git.datacontroller.io/dc/dc/commit/02a8a1c5654350cafc53b749cceb686fc6848c33))
* table metadata modal, versions tab (and link) ([b27fea5](https://git.datacontroller.io/dc/dc/commit/b27fea5b91e33b4673a3a991aedae558e366ca29))
* **versions:** getting list of versions (plus test) ([8003da9](https://git.datacontroller.io/dc/dc/commit/8003da94e615463ed3ddfd60b0cbf2e58615eab1))

# [6.7.0](https://git.datacontroller.io/dc/dc/compare/v6.6.4...v6.7.0) (2024-04-01)


### Features

* numeric values in hot dropdown aligned right ([9635626](https://git.datacontroller.io/dc/dc/commit/963562621ddf0e8d24a29a8481c5e6da1b040708))

## [6.6.4](https://git.datacontroller.io/dc/dc/compare/v6.6.3...v6.6.4) (2024-04-01)


### Bug Fixes

* ordering SOFTSELECT numerically in dropdown ([f522038](https://git.datacontroller.io/dc/dc/commit/f522038b8ddb1da14b8adbf8346d0a4539a94cc8)), closes [#85](https://git.datacontroller.io/dc/dc/issues/85)
* reverting col ([fbbcf90](https://git.datacontroller.io/dc/dc/commit/fbbcf90956bf538b032b0107c07b8576d20353b9))
* typo ([31d4e5c](https://git.datacontroller.io/dc/dc/commit/31d4e5c727f790d428fb2ea8da60dca929561805))

## [6.6.3](https://git.datacontroller.io/dc/dc/compare/v6.6.2...v6.6.3) (2024-02-26)


### Bug Fixes

* allow empty clause value when NE or CONTAINS ([432450a](https://git.datacontroller.io/dc/dc/commit/432450a15b51a269821ba1d430854f5d1dd04703))

## [6.6.2](https://git.datacontroller.io/dc/dc/compare/v6.6.1...v6.6.2) (2024-02-22)


### Bug Fixes

* excel with commas getting wrapped in quotes ([3860134](https://git.datacontroller.io/dc/dc/commit/38601346a529cfe3787bb286a639e0293c365020))

## [6.6.1](https://git.datacontroller.io/dc/dc/compare/v6.6.0...v6.6.1) (2024-02-19)


### Bug Fixes

* **client:** bumped @sasjs/adapter with fixed redirected login ([eb1c09d](https://git.datacontroller.io/dc/dc/commit/eb1c09d7909ba07faf763da261545dc1efaec1b3))

# [6.6.0](https://git.datacontroller.io/dc/dc/compare/v6.5.2...v6.6.0) (2024-02-12)


### Bug Fixes

* adjust the col numbers in extracted data ([cff5989](https://git.datacontroller.io/dc/dc/commit/cff598955930d2581349e5c6e8b2dd3f9ac96b4c))


### Features

* extra table metadata for [#75](https://git.datacontroller.io/dc/dc/issues/75) ([837821f](https://git.datacontroller.io/dc/dc/commit/837821fd01477d340524dfdaf8dd3d3758cf3095))
* show dsnote on hover title ([6565834](https://git.datacontroller.io/dc/dc/commit/6565834ad4089ecf2de39967e6ed6f217ee4a0a5))

## [6.5.2](https://git.datacontroller.io/dc/dc/compare/v6.5.1...v6.5.2) (2024-02-06)


### Bug Fixes

* ordering mpe_selectbox data by the data values after selectbox_order ([2b54034](https://git.datacontroller.io/dc/dc/commit/2b5403497317632a4be8a00f21455c036f1e6461))

## [6.5.1](https://git.datacontroller.io/dc/dc/compare/v6.5.0...v6.5.1) (2024-02-02)


### Bug Fixes

* ensuring submitter email can be pulled from mpe_emails ([eac0104](https://git.datacontroller.io/dc/dc/commit/eac0104d7aebaf98ff1d1c504c1ce3b25d4a0ce8))

# [6.5.0](https://git.datacontroller.io/dc/dc/compare/v6.4.0...v6.5.0) (2024-01-26)


### Features

* filtering by reference to Variables as well as Values ([6eb1aa8](https://git.datacontroller.io/dc/dc/commit/6eb1aa85d29294d63e6af377e622fbed7fd1fab8))

# [6.4.0](https://git.datacontroller.io/dc/dc/compare/v6.3.1...v6.4.0) (2024-01-24)


### Bug Fixes

* add dcLib to globals ([5d93346](https://git.datacontroller.io/dc/dc/commit/5d93346b52eda27c2829770e96686a713296d373))
* add service to get xlmap rules and fixed interface name ([9ffa30a](https://git.datacontroller.io/dc/dc/commit/9ffa30ab747f5b62acbd452431a5e6e440afcb80))
* increasing length of mpe_excel_map cols to ([2d4d068](https://git.datacontroller.io/dc/dc/commit/2d4d068413dcdac98581f08939e74bde65b73428))
* providing info on mapids to FE ([fd94945](https://git.datacontroller.io/dc/dc/commit/fd94945466c1a797ddc89815258a65624a9cb0cf))
* removing tables from EDIT menu that are in xlmaps ([9550ae4](https://git.datacontroller.io/dc/dc/commit/9550ae4d1154a0272f8a2427ac9d2afdfd699c96))
* removing XLMAP_TARGETLIBDS from mpe_xlmaps_rules table ([93702c6](https://git.datacontroller.io/dc/dc/commit/93702c63dc280cdba1e46f0fd8fe0deaec879611))
* renaming TABLE macvar to LOAD_REF in postdata.sas ([01915a2](https://git.datacontroller.io/dc/dc/commit/01915a2db9a4dfb94e4e8213e2c32181da36d349))
* reverting xlmap in getdata change ([2d6e747](https://git.datacontroller.io/dc/dc/commit/2d6e747db9b84e9fb0dfcf9102a2f7dd2cb51891))
* update edit tab to load ([516e5a2](https://git.datacontroller.io/dc/dc/commit/516e5a206216f79ab1dce9f4eab0d31115743160))


### Features

* adding ability to define the target table for excel maps ([c86fba9](https://git.datacontroller.io/dc/dc/commit/c86fba9dc75ddc6033132f469ad1c31b9131b12e))
* adding ismap attribute to getdata response (and fixing test) ([2702bb3](https://git.datacontroller.io/dc/dc/commit/2702bb3c84c45903def1aa2b8cc20a6dd080281b))
* Complex Excel Uploads ([cf19381](https://git.datacontroller.io/dc/dc/commit/cf193810606f287b8d6f864c4eb64d43c5ab5f3c)), closes [#69](https://git.datacontroller.io/dc/dc/issues/69)
* Create Tables / Files dropdown under load tab ([b473b19](https://git.datacontroller.io/dc/dc/commit/b473b198a61f468dff74cd8e64692e7847084a80))
* display list of maps in sidebar ([5aec024](https://git.datacontroller.io/dc/dc/commit/5aec0242429942f8a989b5fb79f8d3865e9de01a))
* implemented the logic for xlmap component ([50696bb](https://git.datacontroller.io/dc/dc/commit/50696bb926dd00472db65a008771a4b6352871be))
* model changes for [#69](https://git.datacontroller.io/dc/dc/issues/69) ([271543a](https://git.datacontroller.io/dc/dc/commit/271543a446a2116718f99f0540e3cd911f9f5fe7))
* new getxlmaps service to return rules for a particular xlmap_id ([56264ec](https://git.datacontroller.io/dc/dc/commit/56264ecc6908bf6c8e3e666dfeba7068d6195df8))
* validating the excel map after stage (adding load-ref) ([a485c3b](https://git.datacontroller.io/dc/dc/commit/a485c3b78724a36f7bacb264fb02140cc62d6512))

## [6.3.1](https://git.datacontroller.io/dc/dc/compare/v6.3.0...v6.3.1) (2024-01-01)


### Bug Fixes

* enabling excel uploads to tables with retained keys, also adding more validation to MPE_TABLES updates ([3efccc4](https://git.datacontroller.io/dc/dc/commit/3efccc4cf3752763d049836724f2491c287f65db))

# [6.3.0](https://git.datacontroller.io/dc/dc/compare/v6.2.8...v6.3.0) (2023-12-04)


### Features

* viewer row handle ([dadac4f](https://git.datacontroller.io/dc/dc/commit/dadac4f13f85b5446198b6340cad28844defc94d))

## [6.2.8](https://git.datacontroller.io/dc/dc/compare/v6.2.7...v6.2.8) (2023-12-04)


### Bug Fixes

* bumping sasjs/core to fix mp_loadformat issue ([a1d308e](https://git.datacontroller.io/dc/dc/commit/a1d308ea078786b27bf7ec940d018fc657d4c398))
* new logic for -fc suffix.  Closes [#63](https://git.datacontroller.io/dc/dc/issues/63) ([5579db0](https://git.datacontroller.io/dc/dc/commit/5579db0eafc668b1bc310099b7cc3062e0598fc4))

## [6.2.7](https://git.datacontroller.io/dc/dc/compare/v6.2.6...v6.2.7) (2023-11-09)


### Bug Fixes

* **audit:** updated crypto-js (hashing rows in dynamic cell validation) ([a7aa42a](https://git.datacontroller.io/dc/dc/commit/a7aa42a59b71597399924b8d2d06010c806321f3))
* missing dependency and avoiding label length limit issue ([91f128c](https://git.datacontroller.io/dc/dc/commit/91f128c2fead1e4f72267d689e67f49ec9a2ab35))

## [6.2.6](https://git.datacontroller.io/dc/dc/compare/v6.2.5...v6.2.6) (2023-10-18)


### Bug Fixes

* bumping core to address mm_assigndirectlib issue ([c27cdab](https://git.datacontroller.io/dc/dc/commit/c27cdab3fccbde814a29424d0344173a73ea816c))

## [6.2.5](https://git.datacontroller.io/dc/dc/compare/v6.2.4...v6.2.5) (2023-10-17)


### Bug Fixes

* enabling AUTHDOMAIN in MM_ASSIGNDIRECTLIB ([008b45a](https://git.datacontroller.io/dc/dc/commit/008b45ad175ec0e6026f5ef3bc210470226e328f))

## [6.2.4](https://git.datacontroller.io/dc/dc/compare/v6.2.3...v6.2.4) (2023-10-16)


### Bug Fixes

* Enable display of metadata-only tables. Closes [#56](https://git.datacontroller.io/dc/dc/issues/56) ([f3e82b4](https://git.datacontroller.io/dc/dc/commit/f3e82b4ee2a9c1c851f812ac60e9eaf05f91a0f9))

## [6.2.3](https://git.datacontroller.io/dc/dc/compare/v6.2.2...v6.2.3) (2023-10-12)


### Bug Fixes

* bumping core library to avoid non-ascii char in mp_validatecols.sas. [#50](https://git.datacontroller.io/dc/dc/issues/50) ([11b06f6](https://git.datacontroller.io/dc/dc/commit/11b06f6416300b6d70b1570c415d5a5c004976db))
* removing copyright symbol from mpe_alerts macro. [#50](https://git.datacontroller.io/dc/dc/issues/50) ([adb7eb7](https://git.datacontroller.io/dc/dc/commit/adb7eb77550c68a2dab15a6ff358129820e9b612))

## [6.2.2](https://git.datacontroller.io/dc/dc/compare/v6.2.1...v6.2.2) (2023-10-09)


### Bug Fixes

* updated SheetJS (crypto) to the latest ([8bd0dd2](https://git.datacontroller.io/dc/dc/commit/8bd0dd22c258911672303869e4df893a98e93575))

## [6.2.1](https://git.datacontroller.io/dc/dc/compare/v6.2.0...v6.2.1) (2023-10-09)


### Bug Fixes

* approve, history and submit pages grouped in review module ([e056ece](https://git.datacontroller.io/dc/dc/commit/e056ece2234ef6aab050f6a5b1f8de633b163d91))
* closes [#39](https://git.datacontroller.io/dc/dc/issues/39) upcase issue in MPE_SECURITY ([a00d31c](https://git.datacontroller.io/dc/dc/commit/a00d31caf3c5634cd61a4700fb175e76856edbb6))
* handsontable v13 ([6f482ec](https://git.datacontroller.io/dc/dc/commit/6f482ec6d909907a304ef9975262889e2370035f))
* latest adapter ([5e30dc0](https://git.datacontroller.io/dc/dc/commit/5e30dc0f892fab2af41f4ea56e30f27ec3b3912e))
* sasjs/cli and sasjs/core updated to the latest ([8571e01](https://git.datacontroller.io/dc/dc/commit/8571e01e44a8cb6df9d150d271c34bb75bffdf31))
* updating editors/stagedata to address issues in particular viya configurations as described in issue [#33](https://git.datacontroller.io/dc/dc/issues/33) ([94ab949](https://git.datacontroller.io/dc/dc/commit/94ab949df8c75072525751a2156b7a32c2e641dc))
* updating logic for REPLACE loadtype ([1f2ce55](https://git.datacontroller.io/dc/dc/commit/1f2ce55f249f4af56f0cacdec47e69246cd47431))

# [6.2.0](https://git.datacontroller.io/dc/dc/compare/v6.1.0...v6.2.0) (2023-08-24)


### Bug Fixes

* re-enabling full REPLACE uploads ([08e39c4](https://git.datacontroller.io/dc/dc/commit/08e39c4fca570406f9aad3d907cb04596421d074))

### Features

* support for European numeric formats ([e48e47b](https://git.datacontroller.io/dc/dc/commit/e48e47bc635452b59e107b235e597c26e748875e))

# [6.1.0](https://git.datacontroller.io/dc/dc/compare/v6.0.0...v6.1.0) (2023-07-25)


### Bug Fixes

* missing mf_existds dependency in bitemporal_dataloader ([5ce1701](https://git.datacontroller.io/dc/dc/commit/5ce1701657136f2cf792441412230513ff52e7e8))
* reducing audit data volumes. Closes [#4](https://git.datacontroller.io/dc/dc/issues/4) ([54fe701](https://git.datacontroller.io/dc/dc/commit/54fe7013b1a25be228eeb2aba3553f6219952de6))
* release script, excel upload duplicate primary keys, cypress fix ([2f79487](https://git.datacontroller.io/dc/dc/commit/2f79487aeaf6268b027a8fa52fcdaae2b449de3d))


### Features

* full format deletion, closes [#2](https://git.datacontroller.io/dc/dc/issues/2) ([8dc40bd](https://git.datacontroller.io/dc/dc/commit/8dc40bdd4e3a7ad5c1e6582b4130f24bc445eb77))

# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [6.0.0](https://gitlab.com/macropeople/datacontroller/compare/v5.3.0...v6.0.0) (2023-06-27)


### ⚠ BREAKING CHANGES

* The updated model and behaviour of FORMATS is the breaking change in this release!  (but not this commit)

* removed gitpod.yml and moved CONTRIBUTING.md to root directory ([eb3e1be](https://gitlab.com/macropeople/datacontroller/commit/eb3e1be2e4697a90faa01155c53f2e664c63c3c6))


### Features

* adding isadmin to startupservice. Relates to [#460](https://gitlab.com/macropeople/datacontroller/issues/460) ([70a755c](https://gitlab.com/macropeople/datacontroller/commit/70a755ccace68d0d2eb4baf25f091300e7f49b75))
* admin page ([642b7d0](https://gitlab.com/macropeople/datacontroller/commit/642b7d042a34fc83f7871fd84e254490cea81232))
* angular 16 ([9805c09](https://gitlab.com/macropeople/datacontroller/commit/9805c094290f9d8f7012bb0316ef924865e6ac29))
* autocomplete load more ([f3df479](https://gitlab.com/macropeople/datacontroller/commit/f3df4790c8160c2f51c643419a24992e4171e5b7))
* autocomplete load more ([3160be4](https://gitlab.com/macropeople/datacontroller/commit/3160be4fe1f3f4196cf0fa418dee75acfe6a64f3))
* custom dropdown load more ([0a576fd](https://gitlab.com/macropeople/datacontroller/commit/0a576fddb9525ca41db4a31b7ad4aec6db420ca4))
* handsontable 12.4.0 ([17d1c23](https://gitlab.com/macropeople/datacontroller/commit/17d1c231b45e667442236f5475cb5d68a67ce7d3))
* history paging (load more) ([d4381da](https://gitlab.com/macropeople/datacontroller/commit/d4381da4776814419133b11b4c99f57faca62172))
* licence in a file ([2cc59ff](https://gitlab.com/macropeople/datacontroller/commit/2cc59ff1b392985b342b8491147bea1bf895d7e2))
* licensing refactor - free tier version ([a418cd6](https://gitlab.com/macropeople/datacontroller/commit/a418cd6085c16d62a2be7fc7f9a9943f9452f3a9))
* new licence service with encoded features ([fa8722f](https://gitlab.com/macropeople/datacontroller/commit/fa8722f4c5ba8e7334d478560a82d24d5cb7f79e))
* showing PK in VIEW menu for format catalogs ([fc64832](https://gitlab.com/macropeople/datacontroller/commit/fc6483229a63afbe2f3044e242079798110aef41))
* support for INFORMATS - also multilabel and notsorted formats ([34af333](https://gitlab.com/macropeople/datacontroller/commit/34af333e414ad965d94b875065fa2d6a7492ed8e))
* viya api explorer ([4c69487](https://gitlab.com/macropeople/datacontroller/commit/4c69487552612a35e010d5c8de34337b5f43c8ee))


### Bug Fixes

* adapter bump ([72843c2](https://gitlab.com/macropeople/datacontroller/commit/72843c27ed6638f5fca84c0f8d1b865121655876))
* adapter bump ([30da29f](https://gitlab.com/macropeople/datacontroller/commit/30da29ffa7a57f723d85ed653de41e158a9743d1))
* added component to add link to support email ([234a43d](https://gitlab.com/macropeople/datacontroller/commit/234a43dc3537506096fbb16b225c755e9581fd9c))
* added copy without headers ([32d5a4f](https://gitlab.com/macropeople/datacontroller/commit/32d5a4f6ec293b610e917c10c9719bb6a0334d1b))
* adding lock check on mpe_audit before dataload ([6ad5d61](https://gitlab.com/macropeople/datacontroller/commit/6ad5d615dfa0ad23f0eda2c986f0d600fb1bc3aa))
* adding nobs to gethistory ([2c15abb](https://gitlab.com/macropeople/datacontroller/commit/2c15abb22a2fcd31aefb20b15a200a271a42ef2b))
* angular bump 15.2.9 ([c7e21b9](https://gitlab.com/macropeople/datacontroller/commit/c7e21b97daaec85337459f43856eb8f8a446cd13))
* angular-eslint bump 15.2.1 ([305de83](https://gitlab.com/macropeople/datacontroller/commit/305de8368bd98486b280208ea95fe3538a1f792b))
* **approve:** bumping core to prevent unlocks when tables are not locked (issue [#339](https://gitlab.com/macropeople/datacontroller/issues/339) in core library) ([afc0517](https://gitlab.com/macropeople/datacontroller/commit/afc0517a7a152be46072d740e0c893d30b7bfa09))
* **approve:** When uploading ONLY deletes, ensure that records are added to the mpe_audit table ([2d404c0](https://gitlab.com/macropeople/datacontroller/commit/2d404c0992cc8c611bede64aa71b5d378a5e3032))
* backend for getcolvals extra rows ([4f50654](https://gitlab.com/macropeople/datacontroller/commit/4f50654c17018103b1b30ea08ae7b01c5afca419))
* bitemporal load support in audit table ([a63f7de](https://gitlab.com/macropeople/datacontroller/commit/a63f7de32eeb8a67296713271df40c83125b61f8))
* bump typescript 4.9.5 ([b0e0dfa](https://gitlab.com/macropeople/datacontroller/commit/b0e0dfac6430b476f69966695075cc77cbcf5ea8))
* d3 graphviz updated to fix vulnerabilities ([28b27ce](https://gitlab.com/macropeople/datacontroller/commit/28b27ce2aca7d2319ba8bc1c4bd4f42e0a976f90))
* default hot licence, editor rows limit notice on the bottom, showed only when more then limit submitted ([0db976a](https://gitlab.com/macropeople/datacontroller/commit/0db976a5b7c9f167d103461fb65297eb091e2b87))
* display special missings in VIEW mode ([8d1bfb5](https://gitlab.com/macropeople/datacontroller/commit/8d1bfb5a5d336f094aaa955a87c23bd6986d72f1))
* enabling configurable history at backend in approvers/gethistory. Addresses [#257](https://gitlab.com/macropeople/datacontroller/issues/257) and ([055a7ff](https://gitlab.com/macropeople/datacontroller/commit/055a7ff5615dc1a074b7aa327cfedb99cd35e572)), closes [#477](https://gitlab.com/macropeople/datacontroller/issues/477)
* enabling refresh of single library when invalid libraries are present ([9545ac7](https://gitlab.com/macropeople/datacontroller/commit/9545ac7a6b44d31a3d5a45bb9dd2168b39a97997))
* filter on download in history removed, rows and cols number on viewer and editor ([bab3dfa](https://gitlab.com/macropeople/datacontroller/commit/bab3dfa908422708e31b4441f90133873b23be52))
* firefox dropdown not sticking, load more is note present if nothing to load ([9c78d67](https://gitlab.com/macropeople/datacontroller/commit/9c78d678ef10809392ccc83bfbf1353701b059e2))
* index html inline style, sas9 metadata type ([8226413](https://gitlab.com/macropeople/datacontroller/commit/8226413c734dfc1f6ce453647f2fb3dc349cda5e))
* inserting row above/below was scrolling to the bottom ([c30e361](https://gitlab.com/macropeople/datacontroller/commit/c30e36160c4ab42b45a8ec4c13a6f58ff7d3b90f))
* licence expiration blocks the app (it is now reverted to free tier version) ([389a05b](https://gitlab.com/macropeople/datacontroller/commit/389a05b52ad93d36395b8061f51d07166efa347a))
* licence expired blocking app  (in progress) ([4601683](https://gitlab.com/macropeople/datacontroller/commit/4601683eab34feb5e5701b1efc90e1f678071bdf))
* licence expired error, added expiry date ([2c20c8d](https://gitlab.com/macropeople/datacontroller/commit/2c20c8dfe0b0ea37f59fcf73e7f803e5023bba87))
* load more in history to appear only when paging available ([8851ba7](https://gitlab.com/macropeople/datacontroller/commit/8851ba7ded6f2eefbc3598d048729f4b80e6f878))
* making HIST configurable with MPE_CONFIG ([215fa87](https://gitlab.com/macropeople/datacontroller/commit/215fa87bfd0ee39c4ce499a4bffe3d4e8aa05035))
* missing licence should not show error ([5a653ae](https://gitlab.com/macropeople/datacontroller/commit/5a653aef288d48f444924255a6328f7172c8fc58))
* new cypress config, added cypress reporter in results after test run ([8ea3ab8](https://gitlab.com/macropeople/datacontroller/commit/8ea3ab8b89344a2c148e9a97c991c7ccbc678a15))
* removed ngx-markdown ([cda97fc](https://gitlab.com/macropeople/datacontroller/commit/cda97fcaf7d727cf41093aa272087e21c266a4aa))
* removed ngx-markdown, added `marked` package, used raw ([43cd643](https://gitlab.com/macropeople/datacontroller/commit/43cd643f6aec475e53f0c798d064a57035f11fa9))
* submit table request error handling ([b8395be](https://gitlab.com/macropeople/datacontroller/commit/b8395be93382e7187cfb1a1909231ea3f89032c6))
* submitted page linking ([c18519e](https://gitlab.com/macropeople/datacontroller/commit/c18519e287ffa9eb62d43b7bd414cb1c41b2471a))
* submitted_dttm type fix and distinct for non admin gethistory ([ef3fa67](https://gitlab.com/macropeople/datacontroller/commit/ef3fa67b950cd626f955bfd79bbfec4a4f1b9bd3))
* supporting additional date formats.  Closes [#484](https://gitlab.com/macropeople/datacontroller/issues/484) ([38d8e10](https://gitlab.com/macropeople/datacontroller/commit/38d8e10dc299be4c7939603b1a7e84d573bde3bf))
* typo in DDL for MPE_SUBMIT ([f6ca05d](https://gitlab.com/macropeople/datacontroller/commit/f6ca05d733ed05167f15858eaf1e6cfee5aea182))
* unlocking audit table ([2a6884d](https://gitlab.com/macropeople/datacontroller/commit/2a6884dffcffce22607f7d221e7743135bf32f6e))
* upcase issue ([ccd9c4e](https://gitlab.com/macropeople/datacontroller/commit/ccd9c4eaf3f45306199dc831cb691401bf5077e8))
* update to angular v15 ([90a143f](https://gitlab.com/macropeople/datacontroller/commit/90a143fd1ab2b60fe7f48a0c1c582d2727c17757))
* updated [@angular-eslint](https://gitlab.com/angular-eslint) ([080b281](https://gitlab.com/macropeople/datacontroller/commit/080b281df048b6577ac32577b19c46d74fe92ea6))
* users allowed with demo key ([6a0fce8](https://gitlab.com/macropeople/datacontroller/commit/6a0fce880e7d3d30b37133bcb1c8e42dee3467fb))
* viewer rows limit notice position ([5ac227b](https://gitlab.com/macropeople/datacontroller/commit/5ac227b950b0ee4a88d243c0df24d3e5f5e2a6a6))
* viya profile picture url was broken when client at different domain ([a42cfc0](https://gitlab.com/macropeople/datacontroller/commit/a42cfc077a4a5922e019086ca41365d4779e7205))

## [5.3.0](https://github.com/datacontroller/datacontroller/compare/v5.2.0...v5.3.0) (2023-02-23)


### Features

* added file drop and file select directives ([3d29908](https://github.com/datacontroller/datacontroller/commit/3d299087971eea6b0e52886e3b0980bedcb209ca))
* column ordering stored to URL ([63f7354](https://github.com/datacontroller/datacontroller/commit/63f7354976ca6c5966dadae659065f98524609ba))
* config in sasjs html tag ([4c0f6ed](https://github.com/datacontroller/datacontroller/commit/4c0f6ed91df48a4bb733f3a6343b95b387499487))
* editor - viewboxes added ([860a7e9](https://github.com/datacontroller/datacontroller/commit/860a7e9659cd28326a2a0eba3d908ff0fde880a9))
* **viewboxes:** edit table in new tab ([8321263](https://github.com/datacontroller/datacontroller/commit/8321263b3c3076f317aff5b0228ca8423cc5b89b))
* **viewboxes:** full table search table ([e86b53a](https://github.com/datacontroller/datacontroller/commit/e86b53a2855bd18eaf91fad0ae8ce84ac4ea360e))
* **viewboxes:** integration with viewer, modularization ([507f4ae](https://github.com/datacontroller/datacontroller/commit/507f4aef1cab4e6bab9ed310458b8e0fb4f75a8a))
* **viewboxes:** snap to grid ([940634a](https://github.com/datacontroller/datacontroller/commit/940634a560de8caa08222d9d4f95e3bba8da7d1b))
* **viewboxes:** table filtering (in progress) ([2029a85](https://github.com/datacontroller/datacontroller/commit/2029a85d6cb3a0ee1f4ba22bd9061cc7a172bd04))


### Bug Fixes

* abort issue on SAS with ms_webout etc ([05d79cb](https://github.com/datacontroller/datacontroller/commit/05d79cb096adfdaa1dd831b12cb379636e726ebd))
* adapter config in index.html can be ommited now ([0058eb8](https://github.com/datacontroller/datacontroller/commit/0058eb8ffe481b76130d21837b55dbfb0c24dce2))
* adapter execute script breaking change ([fd98a2e](https://github.com/datacontroller/datacontroller/commit/fd98a2e68870e797ad40e2fb683e362b63bcccfa))
* adapter updated ([414bf8d](https://github.com/datacontroller/datacontroller/commit/414bf8d606640091a41c7c91ce1ef77d4c29e522))
* approve details too many changes JS error ([2efb49e](https://github.com/datacontroller/datacontroller/commit/2efb49ebfdd751c06fd542c9c0f7be08398e391c))
* bump prettier ([0ab0e84](https://github.com/datacontroller/datacontroller/commit/0ab0e844508a6afbc06d393ca82c44cdc864f04d))
* bumping core & cli ([c7ce70d](https://github.com/datacontroller/datacontroller/commit/c7ce70d72a1cef1d5249f09a758df56af50f0aa6))
* col lineage errors (not respecting max_depth) ([2fdc462](https://github.com/datacontroller/datacontroller/commit/2fdc462af270935e35cbc8f933ae753316676377))
* col lineage hanging issue ([da6536e](https://github.com/datacontroller/datacontroller/commit/da6536e5e508c81b69d4d3aa92df90e5bbd80aac))
* dynamic mocks for licencing tests ([f967058](https://github.com/datacontroller/datacontroller/commit/f9670583461115caa0dcad1c93bee19f2120c91b))
* filtering and licence testing, dynamic mocks ([e309029](https://github.com/datacontroller/datacontroller/commit/e30902947fc25ff27366ff77c23d5e9bed3757d3))
* improved changed rows notice position ([8c521c8](https://github.com/datacontroller/datacontroller/commit/8c521c8aa29c902ea85b7adcabcfdd132ba7d462))
* remove ng2-file-uploader ([4170546](https://github.com/datacontroller/datacontroller/commit/41705466f1ffc1a28be7b5de250299aafcf9e79b))
* rename label (dot depth) ([57fa379](https://github.com/datacontroller/datacontroller/commit/57fa37931be42edb7cbe724740a3b486296c072f))
* update handsontable to 12.2.0 ([a438671](https://github.com/datacontroller/datacontroller/commit/a4386717e8c0f38974297953a6cf805b012cb246))
* viewbox libraries paging ([4039973](https://github.com/datacontroller/datacontroller/commit/4039973af861146250893da4588de9d9be79a9e4))
* **viewboxes:** independent filter caching ([7f78ef0](https://github.com/datacontroller/datacontroller/commit/7f78ef0cd2a6967f0fe69a2d55fdd808b4b14211))
* **viewboxes:** main table filtering title, z index ([0801b5c](https://github.com/datacontroller/datacontroller/commit/0801b5c1fac147dc6ea21e0eeb3b64f514e7c6c1))
* **viewboxes:** url and keyboard selecting in tree double select ([80942cb](https://github.com/datacontroller/datacontroller/commit/80942cb1f68c6cadf2c026e761c331cb0d1660ac))

## [5.2.0](https://github.com/datacontroller/datacontroller/compare/v5.1.0...v5.2.0) (2022-11-15)


### Features

* limit lineage depth button ([cc901f1](https://github.com/datacontroller/datacontroller/commit/cc901f14d499452a19a666c032f72b44b810a4d4))


### Bug Fixes

* deps update ([8c9a14f](https://github.com/datacontroller/datacontroller/commit/8c9a14f2c7e6004ec62186e096c80f60002f8acd))
* dummy js file to force admin folder in demo.datacontroller.io ([07f3194](https://github.com/datacontroller/datacontroller/commit/07f319454ab776372e707e763eea34f423ff12bf))
* editor and viewer responsiveness, logo smaller ([e44ff33](https://github.com/datacontroller/datacontroller/commit/e44ff3389077642359e2fc33f1285d666cbf2c52))
* enable ampersands in library & table lineage ([078acf3](https://github.com/datacontroller/datacontroller/commit/078acf30e2ac4d8aebaff4a783b9800bf0f4af50))
* libinfo alignment ([c508f83](https://github.com/datacontroller/datacontroller/commit/c508f83ab25aaba406dfd03f866206909a588d80))
* licensing error details ([99bb81a](https://github.com/datacontroller/datacontroller/commit/99bb81a01ad021dbfbdfefdb2189eb568125c956))
* licensing error params ([9c4e1e6](https://github.com/datacontroller/datacontroller/commit/9c4e1e686e9f89380af79d2064abb1db9e14538a))
* lineage missmatch info not breaking error ([029fa9f](https://github.com/datacontroller/datacontroller/commit/029fa9f045a1a8f3529fd4b0ddf701275f42e779))
* lineage sidebar matching and error handling ([e26fb0c](https://github.com/datacontroller/datacontroller/commit/e26fb0cede2e05128395599a319bfe4aab2292d0))
* lineage table 'nolib' fix ([b74f0e0](https://github.com/datacontroller/datacontroller/commit/b74f0e0b9c1cdee17a98daba74ef9b0045765c62))
* max_depth not working on large lineage graphs ([1e066f1](https://github.com/datacontroller/datacontroller/commit/1e066f17cf9249bf25408ab8e2290c3aea5c6449))
* missing PK fields in dictionary table ([d40835b](https://github.com/datacontroller/datacontroller/commit/d40835b9da4d9d2750421e9b2232d0a9cea75921))
* viewer lib info styling on smaller screens ([c2c4424](https://github.com/datacontroller/datacontroller/commit/c2c44241a27aeccf8f3923fdc0dad2e8cbb6da23))

## [5.1.0](https://gitlab.com/macropeople/dcfrontend/compare/v5.0.0...v5.1.0) (2022-09-02)


### Features

* adding dsmeta info from mp_dsmeta macro ([2ac6659](https://gitlab.com/macropeople/dcfrontend/commit/2ac66592a27d2581fdbe31ea46f067f0ad42379f))
* adding sasdoc generation for all SAS Platforms ([56662ce](https://gitlab.com/macropeople/dcfrontend/commit/56662ce7b0bfdacda89621b759bb1df1e8ae83d4))
* dsmeta added to view and edit ([24e35dd](https://gitlab.com/macropeople/dcfrontend/commit/24e35dda6584a99734520152bce21b22cb90e6f6))
* edit record - forward backwards changing records ([b28b8f8](https://gitlab.com/macropeople/dcfrontend/commit/b28b8f87859da93105e15471ae7e1c1e6b3846fd))
* initial contents for https://code.datacontroller.io ([fdf8808](https://gitlab.com/macropeople/dcfrontend/commit/fdf88084384ef9fe4b4d90f8aa431f898114d5e5))
* library linking ([fa4aba8](https://gitlab.com/macropeople/dcfrontend/commit/fa4aba8bae442c2167611d96da83da8716ca18fa))
* new libinfo backend service ([0618e63](https://gitlab.com/macropeople/dcfrontend/commit/0618e6397e3d5c31bd2bd75d7346e2bed352b74e))
* refreshlib feature (and tests) backend ([3980774](https://gitlab.com/macropeople/dcfrontend/commit/39807741bc192d5fb24430598082d88aa45e040e))
* viewer library info page ([01b1148](https://gitlab.com/macropeople/dcfrontend/commit/01b114842fac6aa8ed4697778e5f4369bcb60e96))


### Bug Fixes

* adding libinfo in response from refreshlibinfo ([8810b2e](https://gitlab.com/macropeople/dcfrontend/commit/8810b2e459137fa796b8d7d99fa25a1b85a78cab))
* angular configuration fix ([a67e12e](https://gitlab.com/macropeople/dcfrontend/commit/a67e12e46b30ba6359969a591cf4a7336baf017d))
* avoid warning for empty library in viewtables ([abed1a9](https://gitlab.com/macropeople/dcfrontend/commit/abed1a97f23634ebe2d3f48228fb4ad678efe1bb))
* bump core to fix formats with incorrect length ([211f5dc](https://gitlab.com/macropeople/dcfrontend/commit/211f5dc83a4d7b3eca6358ebded4064cc8aa859b))
* bumping core ([6cb3216](https://gitlab.com/macropeople/dcfrontend/commit/6cb321694112afd54913ebd55f166ddfae57a6d7))
* cleanup, editor modularized, angualr 14 update ([558ea94](https://gitlab.com/macropeople/dcfrontend/commit/558ea94eb52133381bd92da68e0223a888b04ea9))
* css issues ([e0f6f33](https://gitlab.com/macropeople/dcfrontend/commit/e0f6f33986d8764839ccb53aba4be914947dc652))
* current row start from 1 ([05267e3](https://gitlab.com/macropeople/dcfrontend/commit/05267e3fefca6c80e6b364d4f0ccdc77980c48c2))
* custom autocomplete input component, fixed qutes around date and time in filtering ([985c45b](https://gitlab.com/macropeople/dcfrontend/commit/985c45bf76278959929cf0351ed2ea0f31686dd9))
* date time filtering with quotes ([78babd3](https://gitlab.com/macropeople/dcfrontend/commit/78babd3121c127095dd5f4711b54fa0e7efb1897))
* demo limits ([97ee3f7](https://gitlab.com/macropeople/dcfrontend/commit/97ee3f7384014ba0e1377b0ea70b8dd1dbe51071))
* dynamic cell validation suppress ABORT modals ([951a0d5](https://gitlab.com/macropeople/dcfrontend/commit/951a0d5cb90946e9ebf08644d1e55a0a60e24c03))
* dynamically obtaining serverContext when running makedata on SAS 9 ([22dec2d](https://gitlab.com/macropeople/dcfrontend/commit/22dec2d570e212804c49e7ee920dde8c8aaa5b1e))
* edit record separate component, modulization routihng ([0e2a57b](https://gitlab.com/macropeople/dcfrontend/commit/0e2a57b53c7124abe5d50476e80e1b3a833b56ea))
* editor multiple filterings ([20bb5cd](https://gitlab.com/macropeople/dcfrontend/commit/20bb5cdd7bae1813dfe29aa1aa3658aaf746aae8))
* empty library no tables, approve details screen break dots ([54ca1ef](https://gitlab.com/macropeople/dcfrontend/commit/54ca1efe3216526b6adc2b7e869ebc316ada3bfa))
* enabling consistent formats in the generated DIFF screen ([80ec246](https://gitlab.com/macropeople/dcfrontend/commit/80ec24667d8fa74b090bf95ed8bc00ae0d66843b))
* filtering IN did not append quotes ([62256a6](https://gitlab.com/macropeople/dcfrontend/commit/62256a6b11e2e7494d2f3b29870382a6f922d4a4))
* filtering Input typing, editor filtering quotes, update licence key placing ([1f2a516](https://gitlab.com/macropeople/dcfrontend/commit/1f2a516742119157b634e0c3cbd84d1abc721adf))
* filtering logic operator undefined ([5c8e951](https://gitlab.com/macropeople/dcfrontend/commit/5c8e951c89a1c73f166c856ae8fa3d3f977e1fa1))
* filtering OR not working ([98c43a5](https://gitlab.com/macropeople/dcfrontend/commit/98c43a518566043649fb06e39588f6d36d913d3c))
* going to libinfo from table view ([13c4add](https://gitlab.com/macropeople/dcfrontend/commit/13c4addeded5eb5bd678bcb3348c9590b052fe7b))
* half way fixed inline styles ([2f85090](https://gitlab.com/macropeople/dcfrontend/commit/2f8509062981330070b0817bca5dca30ee87b5c8))
* hardselect_hook on numeric was not strict ([abdea54](https://gitlab.com/macropeople/dcfrontend/commit/abdea5496d6f6a5fb7d364e716761e742d4d09fd))
* images in dist/images folder ([d187c5d](https://gitlab.com/macropeople/dcfrontend/commit/d187c5d1fa2050250926ebdc58056bee69478ce6))
* incorrect validation on format catalog load type ([b095926](https://gitlab.com/macropeople/dcfrontend/commit/b095926dd72c5b5182b978fe2b3b462d8176381c))
* libraries not refreshing on SAS 9 ([77e33df](https://gitlab.com/macropeople/dcfrontend/commit/77e33df9bce37f77f802b8e32a22dd3448c23cc6))
* licence note ([39d04bd](https://gitlab.com/macropeople/dcfrontend/commit/39d04bdc42102e3b3a9ceef528379e312f3db32a))
* mp_lockanytable error in rejecction ([195a350](https://gitlab.com/macropeople/dcfrontend/commit/195a3501d6d338bd270ba97f1224c6dbbff148c2))
* number of changed rows limit ([55ab7cf](https://gitlab.com/macropeople/dcfrontend/commit/55ab7cf9c5c0e3892c5cd89034cbb11d3dc0fb28))
* pasting NaN issue ([0b1ad3a](https://gitlab.com/macropeople/dcfrontend/commit/0b1ad3a1a81bc2927b89ed5fe45d994d25dbfa02))
* performance improvements with latest mp_jsonout ([5251f24](https://gitlab.com/macropeople/dcfrontend/commit/5251f2494946867111706758102875926168879e))
* requests modal ux, filtering reset kept old values ([dc866aa](https://gitlab.com/macropeople/dcfrontend/commit/dc866aacbe944ef1affddaaa7d3c671eebb860c4))
* softselect dropdown sticking ([b51afbb](https://gitlab.com/macropeople/dcfrontend/commit/b51afbb484dd9ab7ea9641dd185f70a256367ee4))
* typing in filtering `variable` call only on select from dropdown ([cfe9bf6](https://gitlab.com/macropeople/dcfrontend/commit/cfe9bf6c4ac48d4e5ea7e0f8fa3d4fbd8c949d69))
* validations failing in MPE_TABLES for column values due to initialisation of CLS_XX variables ([d65b609](https://gitlab.com/macropeople/dcfrontend/commit/d65b6092b4b39cc4b3043f24a2e89de2839343a9))
* **validations:** ensure LOADTYPE on MPE_TABLES has correct values ([536b176](https://gitlab.com/macropeople/dcfrontend/commit/536b17654d64eff57fe417ad1d830b55e10b046f))
* viya 4 compatibility ([9bb4b88](https://gitlab.com/macropeople/dcfrontend/commit/9bb4b886d08d81284e9fa60f1809184a0a22d2e2))
* when library info refreshed it was not stored ([ea50b31](https://gitlab.com/macropeople/dcfrontend/commit/ea50b31e72655d3f2cf7dd2579b753e3786d7fb4))
* when paste and edit record confirmation type conversion and special missing handling ([a352608](https://gitlab.com/macropeople/dcfrontend/commit/a35260861aaaa6f820c16c7d9c3664564426aad2))
* when pasting into grid types gets lost ([5e75a39](https://gitlab.com/macropeople/dcfrontend/commit/5e75a39f6b06eec3ce1704e0748fec298aebb733))

## [5.0.0](https://gitlab.com/macropeople/dcfrontend/compare/v4.2.2...v5.0.0) (2022-07-11)


### ⚠ BREAKING CHANGES

* mpe_approvals is now dropped and replaced by MPE_SUBMIT, which is unique on table_id.

### Features

* added demo config ([ea9cef0](https://gitlab.com/macropeople/dcfrontend/commit/ea9cef0995e36f7663d8979e5335c5b3f0f99cc8))
* additional tables added to export process.  Fixed a bug when staged data contains only deletions.  Also added CLS rules to COLS table in getdata response. ([66749f1](https://gitlab.com/macropeople/dcfrontend/commit/66749f16665f9c04b802a6ed4b0899f470866f3c))
* backend users (to be continued) ([ba487ba](https://gitlab.com/macropeople/dcfrontend/commit/ba487baa9c5d99ac33c59ed8decf3534de8c267a))
* Column Level Security ([7651fb8](https://gitlab.com/macropeople/dcfrontend/commit/7651fb8ece2a4b29b1e18ac8a447f034bae292a7))
* Column level securty EDITOR ([fdfe333](https://gitlab.com/macropeople/dcfrontend/commit/fdfe33371c83addd4da327ecd2f5b6f1d4602561))
* configurable audit (plus test fixes) ([8673d37](https://gitlab.com/macropeople/dcfrontend/commit/8673d37b7356c5efaa3f9ef5efc3f59f9a7ec671))
* demo max rows ([1b97255](https://gitlab.com/macropeople/dcfrontend/commit/1b97255978acaa8e72a336a65c72357ad371083c))
* dynamic validation features in edit record modal ([6740a5b](https://gitlab.com/macropeople/dcfrontend/commit/6740a5bf70d929e53d886132584f40d03cf43ecf))
* enabling backend primary key columns on VIEW tables ([1db832c](https://gitlab.com/macropeople/dcfrontend/commit/1db832cbc56a2d1d0622b5bd21dd02fb9602728d))
* enabling download as markdown feature ([9032cd1](https://gitlab.com/macropeople/dcfrontend/commit/9032cd18a491339060080c57e4134002cfa95a63))
* getgroups ([6247d6e](https://gitlab.com/macropeople/dcfrontend/commit/6247d6eb2bf35939a9a6b4caa04d2575bdbfe9c3))
* hardselect in record modal - numeric ([b88e622](https://gitlab.com/macropeople/dcfrontend/commit/b88e622a72569f75613700bdf7698146a49fe259))
* new deploy process for SAS 9 ([15e4936](https://gitlab.com/macropeople/dcfrontend/commit/15e4936ef577c199ee33d4ea1622ed8ab0c7cf0f))
* no key demo mode ([f76a0e7](https://gitlab.com/macropeople/dcfrontend/commit/f76a0e76193a8500081087e0305e755b24489df9))
* record modal dynamic cell validation started ([e537958](https://gitlab.com/macropeople/dcfrontend/commit/e5379585aa511b7e5764ec8410efe914d8406086))
* refactored DB for submit & approve ([a58151f](https://gitlab.com/macropeople/dcfrontend/commit/a58151f145dcc81eb7d0772e7b2a170ce861d899))
* restrict EDIT RECORD ([0e30649](https://gitlab.com/macropeople/dcfrontend/commit/0e306498dc4d57b0e02e33e75f25308dcd35c3bd))
* restrict editrecord ([5c8f013](https://gitlab.com/macropeople/dcfrontend/commit/5c8f013e2181e298419f963ac1e3aa5b5a87da64))
* sas9 configurator init ([40b70ab](https://gitlab.com/macropeople/dcfrontend/commit/40b70aba511cf0d815ddf0802344d34e7f3d208a))
* sasjs configurator ([f823276](https://gitlab.com/macropeople/dcfrontend/commit/f823276487524d60c8bec59793ae1cf261951f7c))
* sasjs configurator - on deploy page ([6511c19](https://gitlab.com/macropeople/dcfrontend/commit/6511c198a027226a27fc6d8e91c4c915807373c3))
* sasjs server - redirect to configurator if exits ([eb054cd](https://gitlab.com/macropeople/dcfrontend/commit/eb054cd33e772510cc96a1bfa35cced6497f7531))
* sasjs/server appstream compatibility - see server target ([cf17c90](https://gitlab.com/macropeople/dcfrontend/commit/cf17c9024171a19f2ccadec6c6616f472f6827da))
* show abort modal separate then show info modal ([033be9a](https://gitlab.com/macropeople/dcfrontend/commit/033be9a9ba919aa7bec7ce5de1af26941464b5ed))
* unlocking password protected excel ([ea93601](https://gitlab.com/macropeople/dcfrontend/commit/ea93601192dad275cd32f4647007cc7a2e85d578))


### Bug Fixes

* abort modal download log butto ([ca4631e](https://gitlab.com/macropeople/dcfrontend/commit/ca4631e34a2a0913c561e34cf71d15efa210fc69))
* adapter bump and viya usernav rename ([e98ae0e](https://gitlab.com/macropeople/dcfrontend/commit/e98ae0ee76a8a082b2c6cd13f0696bfcf1fd88f8))
* added open requests modal on sasjs abort ([ea78672](https://gitlab.com/macropeople/dcfrontend/commit/ea786721042e4f5c85e13f3a2886b6f031fe0ea3))
* adding makedataserver.json file ([0fa8680](https://gitlab.com/macropeople/dcfrontend/commit/0fa86801ef6cf5baea08c39e98d17d0c8f014908))
* adding missing statement for CSV uploads ([a8dc48f](https://gitlab.com/macropeople/dcfrontend/commit/a8dc48fc697f71ea8276fee28bafbac15e223b07))
* adding mp_init back ([dba9d7c](https://gitlab.com/macropeople/dcfrontend/commit/dba9d7c776f8f5ee28a7ab4c2cfa5b2f906f97ca))
* adding submit reason to approve pages ([cd903e3](https://gitlab.com/macropeople/dcfrontend/commit/cd903e3cbb5b939a6c2955d1983bc8a0e5bcc21a))
* ading dc-admin as default admin group ([fe5866c](https://gitlab.com/macropeople/dcfrontend/commit/fe5866c22e1144788a8a4c53e285d3018bf6de24))
* alias ([797e137](https://gitlab.com/macropeople/dcfrontend/commit/797e1370f15382c0926abd67363e8df210785792))
* approve details - formatted default on ([16e675c](https://gitlab.com/macropeople/dcfrontend/commit/16e675c4a223a7f759582a80e4a2e99194fcdda8))
* assets fix, logo ([667246f](https://gitlab.com/macropeople/dcfrontend/commit/667246f2d35fd9c64794a336fba5a1249e00a69c))
* avoiding error in mpe_filtermaster (read/write same dest) ([2f6a33e](https://gitlab.com/macropeople/dcfrontend/commit/2f6a33e8b47ddf3d8aeb8078580c7d77a06b607a))
* badly placed comma ([182a6b0](https://gitlab.com/macropeople/dcfrontend/commit/182a6b0ffabd50c3c5be46d68329a68c53cf1bde))
* bum core ([aa29837](https://gitlab.com/macropeople/dcfrontend/commit/aa298372e8b0b8b1bb8f08ea027637eddeae47a4))
* bump core and avoid mpe_groups warning ([bec7149](https://gitlab.com/macropeople/dcfrontend/commit/bec71497b7dca49c451267c581e17f061bcd0e6b))
* bump core, test setup ([d0efc3c](https://gitlab.com/macropeople/dcfrontend/commit/d0efc3c4b94e54967bf913ed6f149c6c6a7a6f69))
* bumping core ([5c0b7a9](https://gitlab.com/macropeople/dcfrontend/commit/5c0b7a90536109394522d8b4937b61d4c9b8f1b2))
* bumping core ([3c28c41](https://gitlab.com/macropeople/dcfrontend/commit/3c28c41237190d711fd4cad979d2e692f17b335b))
* bumping core and improving makedata response on SAS 9 ([79ab3b8](https://gitlab.com/macropeople/dcfrontend/commit/79ab3b8ce4905fdc636839d04b0f8401b5a65196))
* bumping core, missing comma in deploy ddl ([2114708](https://gitlab.com/macropeople/dcfrontend/commit/21147086e64cfb87b0328ad540528e7840551731))
* bumping core, removing licence key ([0a3e381](https://gitlab.com/macropeople/dcfrontend/commit/0a3e381c2fc6d0cba259dbd59583b230d6d37d1f))
* case when statement ([14304a0](https://gitlab.com/macropeople/dcfrontend/commit/14304a0736e68de97ba63673b5067b0694f06a64))
* char limit ([d9af43a](https://gitlab.com/macropeople/dcfrontend/commit/d9af43ab59a658b6e0b74674b8852589cd0ce259))
* checking makedata ([5f64079](https://gitlab.com/macropeople/dcfrontend/commit/5f64079ddd6dcfd021e8b889539eaf1a9b65c26b))
* clarity csp issues ([2ee3ee7](https://gitlab.com/macropeople/dcfrontend/commit/2ee3ee7272d7af0837dc3d42cddb106cc73492f2))
* cls issues revisit ([35b5fce](https://gitlab.com/macropeople/dcfrontend/commit/35b5fce8a68b37a04a8c99ef28311eea587b25b9))
* config ([1797964](https://gitlab.com/macropeople/dcfrontend/commit/1797964ecb1eb7f2bca79a6df2785f9468ed3a4d))
* configurator added group description and progress bar ([b72d27a](https://gitlab.com/macropeople/dcfrontend/commit/b72d27a927bac79cc1e075bed4103ba82aaf0a7c))
* configurator fixing ([10b3b60](https://gitlab.com/macropeople/dcfrontend/commit/10b3b60593a6435282ccf1a98ea27832123217c3))
* configurator groups dropdown, making json request viya specific ([8b1c2c9](https://gitlab.com/macropeople/dcfrontend/commit/8b1c2c915a92010175354301302b5f1e7596e23a))
* configurator html styling ([9d17ea7](https://gitlab.com/macropeople/dcfrontend/commit/9d17ea7778bc9bb0de5c97808f560be6f2f847dd))
* configurator redirect - using new api endpoint (list folder content) ([e32edbf](https://gitlab.com/macropeople/dcfrontend/commit/e32edbf2e47c1b212cfc4e07914bd0cfa2a1e1df))
* core bump ([3a29271](https://gitlab.com/macropeople/dcfrontend/commit/3a29271116cbb7610e9e5ab6ce4710724d85de0d))
* csp issues cwb, /deploy page force, bump up deps ([f8b8ceb](https://gitlab.com/macropeople/dcfrontend/commit/f8b8ceb9e2117acb02bf56841d27555277a78d96))
* CSV upload issue ([69f65de](https://gitlab.com/macropeople/dcfrontend/commit/69f65de59642d269fcfda80449f5cc539faced1d))
* cypress testing ([3f29f9b](https://gitlab.com/macropeople/dcfrontend/commit/3f29f9b429f9cc9101ef2fab237da8f31475b095))
* dc_dttmfmt macro usage (thanks to compatibility issue on M3 proc sql ([65aa595](https://gitlab.com/macropeople/dcfrontend/commit/65aa595fac58e6309814bf8f2bfd954b36a91b20))
* default appLoc ([5507841](https://gitlab.com/macropeople/dcfrontend/commit/550784102374e8e5c99c830fc51fc587ea03d648))
* demo limitations and file upload drop area ([bc4fb1b](https://gitlab.com/macropeople/dcfrontend/commit/bc4fb1b8f4a5209710aaa57e1935c1c4249d0469))
* demo limits refactored - improved ([b80817c](https://gitlab.com/macropeople/dcfrontend/commit/b80817c23ec1e18b9e81e62a8a7cc9fe1ec4d1fa))
* demoConfig rows allowed handling ([5254a6e](https://gitlab.com/macropeople/dcfrontend/commit/5254a6ec7ab67e232ef0698f5d07899b9c7c4dcc))
* deploy component refactor ([2d28d5d](https://gitlab.com/macropeople/dcfrontend/commit/2d28d5d49c93b80f3392fac71373cef4f6935bcf))
* deploy page autodeploy button to upload custom json file ([bee9274](https://gitlab.com/macropeople/dcfrontend/commit/bee9274153107f4e65989b7c3a9d5a6a730b99d0))
* deploy sas9 auto redirect to configurator ([f6598d2](https://gitlab.com/macropeople/dcfrontend/commit/f6598d273befde2982fcdea409f01130d12546b6))
* deprecated images as base64 - since csp can block them ([6c54100](https://gitlab.com/macropeople/dcfrontend/commit/6c54100220dd9be5ac9fe77164514802551cd573))
* doc updates ([aa36de3](https://gitlab.com/macropeople/dcfrontend/commit/aa36de3c5eeebe941c82e385947f5ef60d392c1b))
* drag n drop file fullscreen ([90f98c2](https://gitlab.com/macropeople/dcfrontend/commit/90f98c23f232f26d3b1e8a373b6abc6718aad110))
* EDIT record ([9987e72](https://gitlab.com/macropeople/dcfrontend/commit/9987e72f4a07999784bb94c2eec8c02a6a89d455))
* empty tables on SAS 9 ([4259866](https://gitlab.com/macropeople/dcfrontend/commit/42598669c67841337116621df96880340a1b1055))
* enabling deletes ([e5bbaa8](https://gitlab.com/macropeople/dcfrontend/commit/e5bbaa88558c1ec9195730121b4af39bb91174f3))
* excel upload NAN ([7c0dfda](https://gitlab.com/macropeople/dcfrontend/commit/7c0dfdafa992a6758d590297200eef5015ade512))
* explicit cols ([6e28174](https://gitlab.com/macropeople/dcfrontend/commit/6e281745de773895e8fafcd47a99c44d45ad1b36))
* filter values changed 200 limt to 2k ([ae64bb3](https://gitlab.com/macropeople/dcfrontend/commit/ae64bb3508f06319a3e7a8a77f6e4d78d4beb420))
* filtering added clear button ([981456b](https://gitlab.com/macropeople/dcfrontend/commit/981456b791e45da89bbe33204f1c4c5348101a71))
* filtering dropdowns showing formatted values and inputting unformatted ([beec1a2](https://gitlab.com/macropeople/dcfrontend/commit/beec1a2e3903792ee597a94a4b78e4dad81e3475))
* filtering selecting special missing from dropdown ([d8fde39](https://gitlab.com/macropeople/dcfrontend/commit/d8fde39c92c29f8f486c7f167eeb89dac3061cca))
* finishing up mpe_review + mpe_submit table refactor ([276669c](https://gitlab.com/macropeople/dcfrontend/commit/276669cc98077d019886c412d0d4bcb888190b5e))
* fixing specs ([829a42c](https://gitlab.com/macropeople/dcfrontend/commit/829a42ce34b12be5eb46fe201b8e24d5f628edc7))
* frontend ([3694f16](https://gitlab.com/macropeople/dcfrontend/commit/3694f16ddec5df0e3a295f3b35f4ac49d81951f9))
* frontend fix for startup service, and backend libref ([6dc06b8](https://gitlab.com/macropeople/dcfrontend/commit/6dc06b8dd9fa3e18e9ec142e5de8dfc0e64ffa1b))
* generic macro for hooks ([0fe00fd](https://gitlab.com/macropeople/dcfrontend/commit/0fe00fd2466ece1f7c3f6371c0834d5d8315e967))
* group rename ([514146e](https://gitlab.com/macropeople/dcfrontend/commit/514146eb88a550bc5693a00f27405ffb45769edb))
* hide in edit mode ([fc55fa9](https://gitlab.com/macropeople/dcfrontend/commit/fc55fa97884aa7d79f291a6edc89d4a8b0b4aef4))
* history tab with submit reason ([592078f](https://gitlab.com/macropeople/dcfrontend/commit/592078fb6c94161f0f991660bd2df41df9d3e4ae))
* hook scripts and desktop groups ([271987a](https://gitlab.com/macropeople/dcfrontend/commit/271987a02aac90cf7223d03454dee99730f2855c))
* hotTable height when add record button missing ([1df4bac](https://gitlab.com/macropeople/dcfrontend/commit/1df4bac6ac898c39d61ff923117818746e49a3c6))
* images to base64 ([1e1114b](https://gitlab.com/macropeople/dcfrontend/commit/1e1114b428c95a6e7a6e6f49c10d390d38f4c9b2))
* issue with isSpecialMissing function, sufficient spec files removed ([39412df](https://gitlab.com/macropeople/dcfrontend/commit/39412df367558ed4c06593f42ad40709e5587840))
* legacy peer deps in .npmrc ([14d5f2f](https://gitlab.com/macropeople/dcfrontend/commit/14d5f2f50b8ba683bfce677a68b7c7f7bf7ab8b9))
* length in meta lineage ([f1a262c](https://gitlab.com/macropeople/dcfrontend/commit/f1a262cdb8dfb1b5ffa2f0db81e951eba01676c6))
* lengths ([606611e](https://gitlab.com/macropeople/dcfrontend/commit/606611e11f729d2e862e93d26e72672cfb54546d))
* lineage bug ([be64516](https://gitlab.com/macropeople/dcfrontend/commit/be645166e74f322d69dd29ed55a5a1d73e849355))
* link and mpeinit ([e4b06c2](https://gitlab.com/macropeople/dcfrontend/commit/e4b06c2dd48188649e72f2b708aec74a252d1663))
* lint ([af15f94](https://gitlab.com/macropeople/dcfrontend/commit/af15f94031dc221f1022da6971b939a93430c0a4))
* lint and more approve group removal ([4079f57](https://gitlab.com/macropeople/dcfrontend/commit/4079f5790b0084e10c3289f14f94a453e3ed7ea7))
* lint and naming of sasjs adapter ([740d720](https://gitlab.com/macropeople/dcfrontend/commit/740d720e5295d4a073127c4dceac32b3344d2ccf))
* logout in configurator, filtering IN special missing ([43dc4d4](https://gitlab.com/macropeople/dcfrontend/commit/43dc4d4dde309d6e8d82a2165104f6ad25deca69))
* makedata on viya ([b4c7970](https://gitlab.com/macropeople/dcfrontend/commit/b4c79709bd54e57ffe542dd1d30b046c162a932e))
* making tests work ([5bcb955](https://gitlab.com/macropeople/dcfrontend/commit/5bcb955f753577b42dd6d7aed124a65227272b7c))
* making time format configurable ([202d45e](https://gitlab.com/macropeople/dcfrontend/commit/202d45ea0b17e8bbcfb2bac0c5def0d9a885216d))
* merge development ([5d0057d](https://gitlab.com/macropeople/dcfrontend/commit/5d0057d1eb21deca0bd6506a20c272676b0324c7))
* merge development ([434943c](https://gitlab.com/macropeople/dcfrontend/commit/434943cce4496c8fcc7aab0cc872773c030ec4ce))
* merging changes ([fbe5e14](https://gitlab.com/macropeople/dcfrontend/commit/fbe5e140e8560c7d81d5ce9af40c2d411da7801f))
* merging spec and cols ([7d6bb90](https://gitlab.com/macropeople/dcfrontend/commit/7d6bb90a3c44a106e3ae180e3bca42a15729bb50))
* model updates ([31cf930](https://gitlab.com/macropeople/dcfrontend/commit/31cf9305b6129edb8827d82cef8e7a8434d97ff0))
* more fixes | ([ddf94d5](https://gitlab.com/macropeople/dcfrontend/commit/ddf94d5b604c011eb7acd620706b14f7eb9350d3))
* mp_include ([d518367](https://gitlab.com/macropeople/dcfrontend/commit/d518367aca4a105bd8d210035ea810f548e393fb))
* mpe_tables hook script ([2ba8a37](https://gitlab.com/macropeople/dcfrontend/commit/2ba8a37532addca762c08bfe32eb05f53da7cbfe))
* newline ([285f017](https://gitlab.com/macropeople/dcfrontend/commit/285f017a412fce4accf50b9563d5d79007178d4f))
* no data rows excel upload ([a624f4e](https://gitlab.com/macropeople/dcfrontend/commit/a624f4e67e4be6b32b158f2c00edf9a81a1dcb16))
* nonote2err switch ([904bcdb](https://gitlab.com/macropeople/dcfrontend/commit/904bcdba6d0d435465b50c2dcaa123c5e6066131))
* optional userid as input ([33a6925](https://gitlab.com/macropeople/dcfrontend/commit/33a692569a7a0ecc0048c6363ef4643efa265f67))
* performance boost ([4abec25](https://gitlab.com/macropeople/dcfrontend/commit/4abec2567af55e0deeddd17b292bde5faf78e02d))
* prettier ([f3868f0](https://gitlab.com/macropeople/dcfrontend/commit/f3868f05b799d2fed8606599966b2f08323b6a55))
* preventing CLS being applied to admin users ([a414492](https://gitlab.com/macropeople/dcfrontend/commit/a414492210b4532ee344263594b58f6b1ef0571b))
* primary key always only readonly when cls active ([33ab65b](https://gitlab.com/macropeople/dcfrontend/commit/33ab65b33c95f8633ffc2f144322a0ac20669ddd))
* quoting ([7ca2f97](https://gitlab.com/macropeople/dcfrontend/commit/7ca2f975b89a0705dd79add7f46f69130de00e79))
* redirect streaming app ([9e795d0](https://gitlab.com/macropeople/dcfrontend/commit/9e795d0b828270594be8653de6f436afd455c241))
* reduce row limit in VIEW for performance (to 250 from 1000) ([9a28d6f](https://gitlab.com/macropeople/dcfrontend/commit/9a28d6fd5781352e23ea97df41adaf06899381d5))
* refactoring deploy component ([4f7a495](https://gitlab.com/macropeople/dcfrontend/commit/4f7a495466cb223f3114c6f221fc312123ffcd77))
* reload issue - first time loading screen was stuck ([80c5af8](https://gitlab.com/macropeople/dcfrontend/commit/80c5af8322d6dff1b999256d49f14f6af151d2ac))
* removed inline script, sasjsConfig  as html attributes ([fc6fb22](https://gitlab.com/macropeople/dcfrontend/commit/fc6fb22634447a544785b25e622d1323cd958fdc))
* removed sasjs sufficient elements from usernav ([1176655](https://gitlab.com/macropeople/dcfrontend/commit/1176655c917d36e2cd3360bb84c4a04db3d03a04))
* requests modal fixes ([6c720a8](https://gitlab.com/macropeople/dcfrontend/commit/6c720a85f8c986bbde8d531dba07099e2e10a84c))
* requests modal undefined handling ([aca8bfb](https://gitlab.com/macropeople/dcfrontend/commit/aca8bfbd330287557514291cee38f199fe55fdd1))
* sajs server mode - login ([5f589e5](https://gitlab.com/macropeople/dcfrontend/commit/5f589e5375b36355eb7eff514f25dee304708c4e))
* sas9 and user fixes: ([b42b47f](https://gitlab.com/macropeople/dcfrontend/commit/b42b47fefb7cb525780d1f887bb8e68b6c609d27))
* sas9 configurator fixes ([4c83a34](https://gitlab.com/macropeople/dcfrontend/commit/4c83a342b72c126bcdde9bf464171a5c3a122d27))
* sasjs configurator finish - get groups request ([51df692](https://gitlab.com/macropeople/dcfrontend/commit/51df692a6b9c80da0230e41eb9670a6171e411d4))
* sasjs server fixes ([dd1fe5f](https://gitlab.com/macropeople/dcfrontend/commit/dd1fe5f4e36152d5714bbeb7e09fbc8a320184a1))
* sasjs server fixes and sasjsconfig update ([b43e5e5](https://gitlab.com/macropeople/dcfrontend/commit/b43e5e5a01a7f3575b528f9f410f9a2c2192de3d))
* sasjs server url for downloads ([083bca1](https://gitlab.com/macropeople/dcfrontend/commit/083bca10246d4c0ce66ea661c24da49d8faa5e0b))
* sasjs serverType - usernav ([aeca2a6](https://gitlab.com/macropeople/dcfrontend/commit/aeca2a6a26bfbca7adbe2d7fe3fa4f38bab55bc3))
* save password prompt ([49f05b2](https://gitlab.com/macropeople/dcfrontend/commit/49f05b2b748fc8760bf416dacd14952240d5e9ad))
* sending favicon as base64 ([b6a34ed](https://gitlab.com/macropeople/dcfrontend/commit/b6a34edfdb0dc8c6bf55c90d73ed9fb9e13f29b9))
* server favicon ([5afd806](https://gitlab.com/macropeople/dcfrontend/commit/5afd8061e22145ee8f8d6fda1c363fa25df97b87))
* test for OS at backend ([c062b00](https://gitlab.com/macropeople/dcfrontend/commit/c062b00a94d60c2d3a0537f0596fbd12ec9594a8))
* testing updates ([8b16282](https://gitlab.com/macropeople/dcfrontend/commit/8b16282a4973453f94229edc9c0657bdd1da3507))
* tests and bumping core ([336f54a](https://gitlab.com/macropeople/dcfrontend/commit/336f54aa22fdf1bece991652c601cf519b8ec95b))
* timestamping issues ([082f0cd](https://gitlab.com/macropeople/dcfrontend/commit/082f0cd05c480e78f7c5e01e06885702f3800ff2))
* tree navigation search UX ([a17f253](https://gitlab.com/macropeople/dcfrontend/commit/a17f253c87ff9002c5e4ecd9f1e60ee6d4dd681c))
* update to improve user experience with sorted data ([8cb1b5b](https://gitlab.com/macropeople/dcfrontend/commit/8cb1b5b29412ef0d6c061d37b39a81d2aebc76bd))
* updating licence key and filter clause ([c87c3d8](https://gitlab.com/macropeople/dcfrontend/commit/c87c3d8e98f6ec2fc31217b57e4e4850f3cf4f78))
* usergroups updates ([83cd292](https://gitlab.com/macropeople/dcfrontend/commit/83cd292f8cf24533cda5dcbc509d0da84e22434f))
* username / group updates ([229886d](https://gitlab.com/macropeople/dcfrontend/commit/229886d565ddf8d626ce9a1ef27bdd35ec33e8b4))
* usernav linking and group name ([02c8f60](https://gitlab.com/macropeople/dcfrontend/commit/02c8f608bffc9a37122dfad417f5130358969c7d))
* users & groups on sasjs ([8886905](https://gitlab.com/macropeople/dcfrontend/commit/888690536d06e1bcca9071d5cdbcdd29b1dc2ad5))
* using conditional logic on mpe_refreshtables ([f71f18f](https://gitlab.com/macropeople/dcfrontend/commit/f71f18f29dfe312cc733fd3b6755fbbd3b49d0bd))
* using mf_fmtdttm() ([34ad201](https://gitlab.com/macropeople/dcfrontend/commit/34ad201baebf8003fdb6cc235436d6e47893727d))
* using usernav instead of viya_users ([3d50bbd](https://gitlab.com/macropeople/dcfrontend/commit/3d50bbda18251f3625163bf452f37ca96e0f40d3))
* UX improvements, cols, spec cleanup, dropdown empty record issue ([9a7c3e9](https://gitlab.com/macropeople/dcfrontend/commit/9a7c3e95dce762e50d126b2d9ceab53ca55c9c4a))
* viewdata for just meta only definition ([65ae495](https://gitlab.com/macropeople/dcfrontend/commit/65ae4950ca206698b7c0a2e3d890e445a60048d2))
* viewer error handling ([3c54689](https://gitlab.com/macropeople/dcfrontend/commit/3c546890d98df730273ffad6b0a4cdfb2ff3214e))
* viewer PK coloring, search missing when no data ([f10b8c7](https://gitlab.com/macropeople/dcfrontend/commit/f10b8c71a893a3339deac0129e270f486acb86be))
* viewer reset filter ([0ac5972](https://gitlab.com/macropeople/dcfrontend/commit/0ac5972b7103b593aa5d8736591a8bcbf915dc41))
* when file upload error (csv) file is discarded ([7653fff](https://gitlab.com/macropeople/dcfrontend/commit/7653fffb3a697897e51d5c70a2855dfebc2078dd))
* wps compatibilities ([79c5223](https://gitlab.com/macropeople/dcfrontend/commit/79c52231cd063de2c87b1c351761c85786d7381e))
* wps fix for mpeterm() ([2473364](https://gitlab.com/macropeople/dcfrontend/commit/2473364356d611e54378caad76d02e0beb1566e5))
* wps updates ([4c85101](https://gitlab.com/macropeople/dcfrontend/commit/4c85101bd910ece6829727cb1d7212999b3fac77))

### [4.2.2](https://gitlab.com/macropeople/dcfrontend/compare/v4.2.0...v4.2.2) (2022-03-21)


### Bug Fixes

* friendly labels on sas 9 invalid view libs (and other ci/cid updates) ([9014f28](https://gitlab.com/macropeople/dcfrontend/commit/9014f28193ad118c28667c1f606b61f8fd61cc68))
* sajs server ([0353ba1](https://gitlab.com/macropeople/dcfrontend/commit/0353ba1ed93efb74fd22c601115079154cfde104))

### [4.2.1](https://gitlab.com/macropeople/dcfrontend/compare/v4.2.0...v4.2.1) (2022-03-21)


### Bug Fixes

* friendly labels on sas 9 invalid view libs (and other ci/cid updates) ([9014f28](https://gitlab.com/macropeople/dcfrontend/commit/9014f28193ad118c28667c1f606b61f8fd61cc68))
* sajs server ([0353ba1](https://gitlab.com/macropeople/dcfrontend/commit/0353ba1ed93efb74fd22c601115079154cfde104))

## [4.2.0](https://gitlab.com/macropeople/dcfrontend/compare/v4.1.0...v4.2.0) (2022-03-17)


### Features

* requests modal dowanlod logs ([bd9e759](https://gitlab.com/macropeople/dcfrontend/commit/bd9e75917f7e1f2a8459488b9030886dba0e69d0))


### Bug Fixes

* bump core ([a664573](https://gitlab.com/macropeople/dcfrontend/commit/a664573634046d2e27ab8f321d1e5f9153362805))
* config ([fecdaee](https://gitlab.com/macropeople/dcfrontend/commit/fecdaeea2d9ba818f7df7c227cac669729f60de4))
* filtering pickers checkbox ([8327421](https://gitlab.com/macropeople/dcfrontend/commit/83274213d32c46e28c88b8ddfb38c1a2a5eabfa0))
* getting sas 9 tests to work ([d83b22d](https://gitlab.com/macropeople/dcfrontend/commit/d83b22d1664ce7266d443ea25f10ba139a5ac394))
* missmatch fix ([b33b520](https://gitlab.com/macropeople/dcfrontend/commit/b33b52026b0bb3d51964f998c32047c8c757d370))
* scss ([443c3d7](https://gitlab.com/macropeople/dcfrontend/commit/443c3d785c93d1e93d18a691161e1b57398a58a1))
* validator length, abort modal MAC ([c6b3d1f](https://gitlab.com/macropeople/dcfrontend/commit/c6b3d1faa8ec70fbdb17ac00000a02b0c3073e5e))

## [4.1.0](https://gitlab.com/macropeople/dcfrontend/compare/v4.0.1...v4.1.0) (2022-03-15)


### Features

* server configurator ([df6cc6c](https://gitlab.com/macropeople/dcfrontend/commit/df6cc6cb8c575be4c83c321f7757719a124dd7ac))


### Bug Fixes

* audit service issues ([c6e1452](https://gitlab.com/macropeople/dcfrontend/commit/c6e14524aa43dd797d410ce072b989c15a8fe91d))
* bump core and sasjs/server makedata update ([590710b](https://gitlab.com/macropeople/dcfrontend/commit/590710bffc95f047da06eff5a33cfe40bc8196e6))
* refactor sas 9 deploy ([4458385](https://gitlab.com/macropeople/dcfrontend/commit/445838535f3d5fbed11bea3d5d0e92c8577d531a))
* sas and app services timing ([7828d04](https://gitlab.com/macropeople/dcfrontend/commit/7828d04f49667f26a229a2c58e0b03e96a6bccdc))
* sasjs server ([c999716](https://gitlab.com/macropeople/dcfrontend/commit/c999716a5fe494c882bc017b474208447d906738))
* sasjs/server updates ([2cd0135](https://gitlab.com/macropeople/dcfrontend/commit/2cd0135d0da32da546990659a60650944509d04e))
* update db for constraint support ([cc1e171](https://gitlab.com/macropeople/dcfrontend/commit/cc1e1711afd246cca285aa4850f339051f7637bb))
* updates to deploy process ([f22556c](https://gitlab.com/macropeople/dcfrontend/commit/f22556c4cb1d4568779004bbd34113c688fd2845))

### [4.0.1](https://gitlab.com/macropeople/dcfrontend/compare/v4.0.0...v4.0.1) (2022-03-10)


### Bug Fixes

* approve details UI ([4e058e9](https://gitlab.com/macropeople/dcfrontend/commit/4e058e90476a3130d877f56475dc52bd7be6d1a0))
* bumping core ([70b5359](https://gitlab.com/macropeople/dcfrontend/commit/70b53591b1fed91f02bb7aa7a106259231488c8f))
* missings validation ([0745486](https://gitlab.com/macropeople/dcfrontend/commit/074548695ff2865ab32fdb7f9e3d5291d96cdc47))
* sas tests ([30b4b48](https://gitlab.com/macropeople/dcfrontend/commit/30b4b4832ab0cb24cc57c6e05858f49088b7b978))
* special missing period prepend validation, replacing period in numeric with null ([9de4197](https://gitlab.com/macropeople/dcfrontend/commit/9de4197d92bcb8c01ff7caa59543f2480497dc9e))
* testing issues and ability to upload excel for formats ([0e84e75](https://gitlab.com/macropeople/dcfrontend/commit/0e84e75086e2f438892f014ed363a967846aaa1d))
* tests etc ([435452b](https://gitlab.com/macropeople/dcfrontend/commit/435452b0c7acd2fe14900388567821cf4c64f22c))
* validation for numeric length ([0f9614b](https://gitlab.com/macropeople/dcfrontend/commit/0f9614bf9f9165dad5475c9262f6c9c76c27dba2))

## [4.0.0](https://gitlab.com/macropeople/dcfrontend/compare/v3.13.3...v4.0.0) (2022-03-03)


### ⚠ BREAKING CHANGES

* The filter_text variable is now dropped from mpe_filteranytable

### Features

* adding mp_init to ensure strict compatibilities.  Also removing the word 'error' for better log scanning. ([f539474](https://gitlab.com/macropeople/dcfrontend/commit/f539474557f17d984e82e79f119d7c05b5bc0dba))
* approve details - formatted values ([2d53b46](https://gitlab.com/macropeople/dcfrontend/commit/2d53b464a2d533e6ceb12883908b40c4bfc8bb1e))
* col info in header menu dropdown, added sheetjs pro tarball backup ([6ed88f8](https://gitlab.com/macropeople/dcfrontend/commit/6ed88f8283647f8c890233bf59ee12c8d0f935db))
* dc-validator module | shared components refactor ([271cc25](https://gitlab.com/macropeople/dcfrontend/commit/271cc256bb86e109f9dc03882081e55c696d78b1))
* excel upload delete column apply ([d908126](https://gitlab.com/macropeople/dcfrontend/commit/d90812633949c8ba8bd6727cdbee28972809c06b))
* format mods! ([6c6f3c5](https://gitlab.com/macropeople/dcfrontend/commit/6c6f3c5ca3b3547fdf32e3e8ef1686c176e48622))
* formats catalogs ([5e233cf](https://gitlab.com/macropeople/dcfrontend/commit/5e233cff2511b5bd4bd207e65c4a6b7e3197d0c8))
* formatted values for GET_DIFFS (backend). Relates to [#398](https://gitlab.com/macropeople/dcfrontend/issues/398) ([585ddb7](https://gitlab.com/macropeople/dcfrontend/commit/585ddb749da8eb48cbf1a1565fba738e07097baf))
* mpe_audit table now updated with every load ([b48a762](https://gitlab.com/macropeople/dcfrontend/commit/b48a7620a6406c321f70c53a3d8d86f98854a0de))
* primary key headers different color ([f7aa07c](https://gitlab.com/macropeople/dcfrontend/commit/f7aa07cf8aff6334cb8ff26941577fcb3a5a9a90))
* sasjs/server support ([63c9dd4](https://gitlab.com/macropeople/dcfrontend/commit/63c9dd45f497817d982b94f7f1ea2712fa820dd3))
* uploading excel with _delete_ column ([4b273e5](https://gitlab.com/macropeople/dcfrontend/commit/4b273e578141f4e5d63b6f7136a9dc23cbed43ee))
* viewer - column context details ([0bf4c48](https://gitlab.com/macropeople/dcfrontend/commit/0bf4c48d356c5caea161d869afcd7d7613d0de4d))


### Bug Fixes

* abort modal refactor ([82a29a3](https://gitlab.com/macropeople/dcfrontend/commit/82a29a345f2b6e4e48094f1bac1f69e7380f416b))
* adding .SSS to datetime and time formats ([af453e8](https://gitlab.com/macropeople/dcfrontend/commit/af453e87b7e21e253b0d89d0dc36e4d9fdb5c7f3))
* adding catalog support for validatefilter and corresponding test ([8104fd9](https://gitlab.com/macropeople/dcfrontend/commit/8104fd9675e06454f5170c368af452189e1bd711))
* addressing comments ([111473f](https://gitlab.com/macropeople/dcfrontend/commit/111473f870ff7a081df34c26270385590e8ad15a))
* addressing yurys comments ([8a84fe7](https://gitlab.com/macropeople/dcfrontend/commit/8a84fe7e5ef947f43d5fbbdc5c8f626858fcb36e))
* angular build ([f6c850c](https://gitlab.com/macropeople/dcfrontend/commit/f6c850c8c534655008b036800322e69501ebdd8e))
* angular new config structure, fixing ts lint errors ([997e8b8](https://gitlab.com/macropeople/dcfrontend/commit/997e8b8ed8258f7cceb024d46ba3da217f99ed74))
* approve details abort modal refactor ([8aec836](https://gitlab.com/macropeople/dcfrontend/commit/8aec83692a62b95a284c26ce18a735ddb5f49176))
* approve details screen more compact ([05bfa52](https://gitlab.com/macropeople/dcfrontend/commit/05bfa52fd9fac93b60cc398ffcf15e55db750646))
* changelog ([14b369a](https://gitlab.com/macropeople/dcfrontend/commit/14b369aa2e7ac3aee5d8b23e9721d434b287e54b))
* conflicts: ([5622d01](https://gitlab.com/macropeople/dcfrontend/commit/5622d018d93ba358dc01f12e2ae387c9f105a075))
* dropdown filtering UX optimization ([bf75a4a](https://gitlab.com/macropeople/dcfrontend/commit/bf75a4a2b9f8c7a50ff968b07eb96fd4cf874e6e))
* dwp issues - bolt icon on viewer, routing user back to viewer issue ([53e8b64](https://gitlab.com/macropeople/dcfrontend/commit/53e8b647594301def750b88437a93719bd80958d))
* enabling formats on getdata ([d1ae3df](https://gitlab.com/macropeople/dcfrontend/commit/d1ae3df2813cb28ff474906175974c548d96e161))
* excel file `delete` column parsing ([1d1635d](https://gitlab.com/macropeople/dcfrontend/commit/1d1635d675599e30493e36c7f8be9c1d1409fa72))
* executeValidator generic function, fixed tests ([1bd76a1](https://gitlab.com/macropeople/dcfrontend/commit/1bd76a1efeba60441fb8dca34e5bf4081426fd0f))
* filtering datetime decimals cut, getcolvals types ([bf8d059](https://gitlab.com/macropeople/dcfrontend/commit/bf8d05942955a3743da81459f9000f60481ea825))
* finally getting decimals to work both for time and timestamp to 5dp ([6062444](https://gitlab.com/macropeople/dcfrontend/commit/60624442b1fbf877de229709b1c9b67cf08e41b9))
* format support for getddl service ([5f5a224](https://gitlab.com/macropeople/dcfrontend/commit/5f5a22402a38798e3ef80a00a86e629fd7044ef4))
* format support in getrawdata service ([993e208](https://gitlab.com/macropeople/dcfrontend/commit/993e2084c0537d94b481be3b79f57d6040221546))
* format-cat entry ([2c0ebf5](https://gitlab.com/macropeople/dcfrontend/commit/2c0ebf575cd18c2467e5a3944877974b51255e7e))
* **formats:** adding formats to viewdata  service (and fixing test issues) ([d3ea3f0](https://gitlab.com/macropeople/dcfrontend/commit/d3ea3f0cc0aa80d299eb1f9a8487e67242782340))
* hot assert error appearing (blocking the app) ([c03b5fc](https://gitlab.com/macropeople/dcfrontend/commit/c03b5fc0cfcacbf4593954b73103a005a92a2b9a))
* hot dynamic validation adding source requires adding cell editor of autocomplete ([263fe95](https://gitlab.com/macropeople/dcfrontend/commit/263fe95ecefb29010d0b6725af59de544abff226))
* hot freeze issue (assertion error) ([c91aa0c](https://gitlab.com/macropeople/dcfrontend/commit/c91aa0c15fec121fc4dacbd8ad677dead10d0194))
* hot height ([6c81769](https://gitlab.com/macropeople/dcfrontend/commit/6c81769d6af5886b2dd07379b188e545e4416e78))
* hot, deploy abort modal refactor ([536e489](https://gitlab.com/macropeople/dcfrontend/commit/536e48905c4167e33afc561b1dc39c6c454b173b))
* include decimal part for datetime and time variables ([9ff05ce](https://gitlab.com/macropeople/dcfrontend/commit/9ff05cecf5ad0d148b8cdf3cb6e4eb084b373917))
* initialised variables fixes ([ad35a49](https://gitlab.com/macropeople/dcfrontend/commit/ad35a49029100626a087703d2f5892797903b000))
* leading char support (and test) ([306bc42](https://gitlab.com/macropeople/dcfrontend/commit/306bc421643426fc5fdbe2faf1011084ca623241))
* licensing abort modal refactor ([cd127d7](https://gitlab.com/macropeople/dcfrontend/commit/cd127d750b43695b9977f90e9ee0a10c33c46ca3))
* licensing with multiple syssite ids ([cb31af0](https://gitlab.com/macropeople/dcfrontend/commit/cb31af0063074e968717ac337bef29102283f8f5))
* macvar in buildinit and getting delete-folder script to work from local ([3280d8b](https://gitlab.com/macropeople/dcfrontend/commit/3280d8b0906ac7bb731fbb47769ad8405b08681c))
* make Lato font bundled ([4f20403](https://gitlab.com/macropeople/dcfrontend/commit/4f20403f691cf3e79cbab30b74bb378d233cb0a5))
* moving the checklock macros to the core library ([f332bd5](https://gitlab.com/macropeople/dcfrontend/commit/f332bd556ee6aa1e996e79507aab81ff275e78b0))
* NodeJS issue ([5e282e8](https://gitlab.com/macropeople/dcfrontend/commit/5e282e86e3f771659f01793c527b259bbc133c55))
* numeric validation length in bytes ([cd833e9](https://gitlab.com/macropeople/dcfrontend/commit/cd833e9ddf0dc355c34d99da509d66ac86841e55))
* paths ([eb06998](https://gitlab.com/macropeople/dcfrontend/commit/eb0699829363085bf6f91f90d7e2412a9723e180))
* postdata ([8d9d141](https://gitlab.com/macropeople/dcfrontend/commit/8d9d14171ba587111e9e8a37b16f21ef963d480b))
* record modal: dc validator refactor implementation ([5e25805](https://gitlab.com/macropeople/dcfrontend/commit/5e25805ce05062ec9648af4594d1d40ff3899805))
* refactoring retained key generation, migrating core component to sasjs/core library ([8a74311](https://gitlab.com/macropeople/dcfrontend/commit/8a74311aa22800e85e5c6aca9c8157aa3d073d80))
* remove processed_dttm from result ([2117727](https://gitlab.com/macropeople/dcfrontend/commit/2117727ac9a8d3a7fc4b437047da6f7c090d9c62))
* removing processed_dttm from validatefilter ([e848c3b](https://gitlab.com/macropeople/dcfrontend/commit/e848c3bf98ed6a3762bcce8153baa68d0c2488ff))
* removing unnecessary colvals col, fixing unfiltered rawdata download, showing batch code on parallel approve abort ([b736861](https://gitlab.com/macropeople/dcfrontend/commit/b7368615b06beb9a29d25a10a9823bb48e07b3d8))
* renaming hot into editor component ([43178e6](https://gitlab.com/macropeople/dcfrontend/commit/43178e66a7c09762b47c24f7a525308bf39f1499))
* replacing Macro People with 4GL Apps ([c43225b](https://gitlab.com/macropeople/dcfrontend/commit/c43225b3f85d1993c106a601db8f06a8ec76f91e))
* sas9 deploy work ([b822a14](https://gitlab.com/macropeople/dcfrontend/commit/b822a1458113fb59a2be4783f3b458875c1cad95))
* sasjs bumps ([0194cca](https://gitlab.com/macropeople/dcfrontend/commit/0194cca61f35ad8aff534e73c709cb707e657581))
* sasjsAbort refactor: viewer component ([290a5d0](https://gitlab.com/macropeople/dcfrontend/commit/290a5d0715cc15ff9990ba81bcb09c8a5f196560))
* servertype SASJS ([60f733c](https://gitlab.com/macropeople/dcfrontend/commit/60f733cd7cd54bed17959222ed6f6343ab915b1f))
* special missing support ([4e739ee](https://gitlab.com/macropeople/dcfrontend/commit/4e739ee2d72778689e842a0b931f35bff52a378a))
* strict mode enablement ([f226fc2](https://gitlab.com/macropeople/dcfrontend/commit/f226fc23e884ca7e9dd80ca9d283de1098f7813a))
* supporting fmt catalog in getcolvals ([92d6e8a](https://gitlab.com/macropeople/dcfrontend/commit/92d6e8afc7c12e6535b3372558ff72a05e40dfcb))
* surrounded data with all empty cells ([e2b3a76](https://gitlab.com/macropeople/dcfrontend/commit/e2b3a76c3128d870e6858c1e475c0a1a7fd373a6))
* terms abort modal refactor ([b7ae9f8](https://gitlab.com/macropeople/dcfrontend/commit/b7ae9f865a38c928471bfc5317a0599f4d879048))
* test fixed, dc validator utils ([f163d75](https://gitlab.com/macropeople/dcfrontend/commit/f163d75864cde7baa91065ea013abf5c7e996470))
* test setup for viewtables with format catalog ([4080459](https://gitlab.com/macropeople/dcfrontend/commit/4080459d707e8f7ca2503755e2d3839ae6137d65))
* types fixing ([3cfcb2e](https://gitlab.com/macropeople/dcfrontend/commit/3cfcb2ecb07c21a077e1ff9d191235cf04771e82))
* uninitialised variable ([65c1f51](https://gitlab.com/macropeople/dcfrontend/commit/65c1f51268be0cdaf593f1a0c429b8fc2333571c))
* updates following strict mode ([94fa05d](https://gitlab.com/macropeople/dcfrontend/commit/94fa05d1b4dc8f8b0045035acb4b42d5456fca98))
* updating analytium email addresses to data controller ([a2f2783](https://gitlab.com/macropeople/dcfrontend/commit/a2f27832f7d0369b0fa4de307988c2393634c87b))
* updating tests, bumping core, new postedit process to check MPE_VALIDATIONS ([a7d49b8](https://gitlab.com/macropeople/dcfrontend/commit/a7d49b8996603af226873f475d33902ab9e047d9))
* upgrading the filter process to work with mp_filtestore.sas ([354ecfa](https://gitlab.com/macropeople/dcfrontend/commit/354ecfa53e54d3ac1aaf0a10b0f221afad5c44fe))
* validation hook append to cell source if any ([d2a22b6](https://gitlab.com/macropeople/dcfrontend/commit/d2a22b65b501ce222526e830a5279ecaca68ac53))
* validations, undefined,null ([6fe5f73](https://gitlab.com/macropeople/dcfrontend/commit/6fe5f73889703829fa675be4ef52570cccda36ce))
* validator - editor implementation ([782e6d0](https://gitlab.com/macropeople/dcfrontend/commit/782e6d0b4c9cde390d907d6318a11fe6d982a0b1))
* validator check ([3fe5b32](https://gitlab.com/macropeople/dcfrontend/commit/3fe5b3269cab99e2d84827b0e1450bfb6a55b7cd))
* **validator:** yury's suggestions ([eae158d](https://gitlab.com/macropeople/dcfrontend/commit/eae158d052255eaa683ceaee71969da9f8b27722))
* versioning structure, moved to root ([e9da745](https://gitlab.com/macropeople/dcfrontend/commit/e9da745c9f32f4308230488e547343288c64949b))
* viewer errors ([dd8b115](https://gitlab.com/macropeople/dcfrontend/commit/dd8b1151209dbf650b4c0eb38f307d4e5e41e67a))
* viewer hot error in console ([e5775cf](https://gitlab.com/macropeople/dcfrontend/commit/e5775cfb37711a2684c267f660e5c96c8c71aff5))
* wording ([caa372f](https://gitlab.com/macropeople/dcfrontend/commit/caa372f56763d76918b3d4e8153cffeb73dd14aa))

## [3.14.0](https://gitlab.com/macropeople/dcfrontend/compare/v3.13.3...v3.14.0) (2021-12-08)


### Features

* sasjs/server support ([63c9dd4](https://gitlab.com/macropeople/dcfrontend/commit/63c9dd45f497817d982b94f7f1ea2712fa820dd3))


### Bug Fixes

* changelog ([14b369a](https://gitlab.com/macropeople/dcfrontend/commit/14b369aa2e7ac3aee5d8b23e9721d434b287e54b))
* licensing with multiple syssite ids ([cb31af0](https://gitlab.com/macropeople/dcfrontend/commit/cb31af0063074e968717ac337bef29102283f8f5))
* moving the checklock macros to the core library ([f332bd5](https://gitlab.com/macropeople/dcfrontend/commit/f332bd556ee6aa1e996e79507aab81ff275e78b0))
* paths ([eb06998](https://gitlab.com/macropeople/dcfrontend/commit/eb0699829363085bf6f91f90d7e2412a9723e180))
* sas9 deploy work ([b822a14](https://gitlab.com/macropeople/dcfrontend/commit/b822a1458113fb59a2be4783f3b458875c1cad95))
* servertype SASJS ([60f733c](https://gitlab.com/macropeople/dcfrontend/commit/60f733cd7cd54bed17959222ed6f6343ab915b1f))
* versioning structure, moved to root ([e9da745](https://gitlab.com/macropeople/dcfrontend/commit/e9da745c9f32f4308230488e547343288c64949b))

### [3.13.3](https://gitlab.com/macropeople/dcfrontend/commit/compare/v3.13.2...v3.13.3) (2021-11-10)

### Bug Fixes

* adapter login error in console ([06b8842](https://gitlab.com/macropeople/dcfrontend/commit/06b884250953dccfec007a6ba2066a6d73685168))
* adding comments in index.html, ensuring hard stop on table lock ([bebd9d5](https://gitlab.com/macropeople/dcfrontend/commit/bebd9d51c61afc4a34ef91cd1d980a81ea9dee01))
* cell validation - arrows not appearing on render ([7e879b9](https://gitlab.com/macropeople/dcfrontend/commit/7e879b9e39de5a7f899dc2495a5da2d6471574ad))
* cell validation - empty cell triggers forced values ([540297d](https://gitlab.com/macropeople/dcfrontend/commit/540297d16fd7f176a86112ef058d0cfbe6ec143a))
* cell validation - force values are set on all rows ([1b0c31c](https://gitlab.com/macropeople/dcfrontend/commit/1b0c31ce08ae0427f65d6f646729a2a8de0c6048))
* closes [#373](https://gitlab.analytium.co.uk//undefined/issues/373) ([209130d](https://gitlab.com/macropeople/dcfrontend/commit/209130db4c4330152ba6176fc79d562a4075b9d6))
* display modal with sheet name where the data is found when doing excel upload ([351d511](https://gitlab.com/macropeople/dcfrontend/commit/351d5118d0d94680b39e0411bfa6390af7f52591))
* duplicate *ALL* values removal ([fdc5b6f](https://gitlab.com/macropeople/dcfrontend/commit/fdc5b6f1a520fd3366ab38d6903b30cd9130f52d))
* excel fail with missing first row ([e508a54](https://gitlab.com/macropeople/dcfrontend/commit/e508a543ea6ba0e76b7603e4a9b3358596ad8a19))
* rejection reason not appearing in the email ([0476090](https://gitlab.com/macropeople/dcfrontend/commit/04760903836e87892d79caee3628c911ba787b86))
* upload excel faster validation ([316551b](https://gitlab.com/macropeople/dcfrontend/commit/316551b2f800d1fbdd3f418d40ce2330ff1742eb))
* upload validation speed up ([4277fef](https://gitlab.com/macropeople/dcfrontend/commit/4277fefeb4695ee7cbf5c588ceac654d5d32c9dc))

### [3.13.2](https://gitlab.com/macropeople/dcfrontend/compare/v3.13.1...v3.13.2) (2021-09-27)


### Features

* licencing - multiple site_id values support ([8e98dd5](https://gitlab.com/macropeople/dcfrontend/commit/8e98dd58cdaa10fd6e11b499f16b80c4627c183b))


### Bug Fixes

* better UX when doing quick reject ([0e35427](https://gitlab.com/macropeople/dcfrontend/commit/0e35427a5f66de4aa98128470c1c0e28db4b0e9e))
* deprecating mpe-getroot() ([a6bc6e7](https://gitlab.com/macropeople/dcfrontend/commit/a6bc6e7423d10b62a7512b787c3d3dcf7e3e580b))
* excel empty numerics parsed incorectly ([0d0e3eb](https://gitlab.com/macropeople/dcfrontend/commit/0d0e3ebff6f88faaef502b881ae0176e3dc7b6d6))
* invalid casting ([e086fb3](https://gitlab.com/macropeople/dcfrontend/commit/e086fb317ca4a094223402b46806a5d7ae418372))
* switching to sasjs/core version of mp_getcols ([616fc80](https://gitlab.com/macropeople/dcfrontend/commit/616fc80c4dbd1c89efd0d48cce027edd53019ef3))
* viewer - handsontable onDestroy of undefined error ([7832e0e](https://gitlab.com/macropeople/dcfrontend/commit/7832e0eb664c85df66b2580982a49d4c4aeec060))

### [3.13.1](https://gitlab.com/macropeople/dcfrontend/compare/v3.13.0...v3.13.1) (2021-09-06)


### Features

* dynamic cell validation force value flag ([016377f](https://gitlab.com/macropeople/dcfrontend/commit/016377f4f2e34c3ad45850b0aee982b2c49ccd36))
* enabling force flag at backend ([1d9ab78](https://gitlab.com/macropeople/dcfrontend/commit/1d9ab783ec30eaa815b5cf752463fbb6d3d814b3))
* hot.batch() improvement, loading spinner on error stop and presenting exclamation mark ([66d6c6b](https://gitlab.com/macropeople/dcfrontend/commit/66d6c6bd6c92bbe49acc8d88dc2f222f3f4b9be3))


### Bug Fixes

* adding test trigger ([9391796](https://gitlab.com/macropeople/dcfrontend/commit/93917961412d1d9a81647ccf4745df6acd1c2f99))
* enabling mp_include for error handling, and adding force flag on mpe_tables validation ([f1163e1](https://gitlab.com/macropeople/dcfrontend/commit/f1163e1fa7f34d94587c55dc306c86bc5039d0bc))
* enabling native PG for key_seq incrementer ([f81505f](https://gitlab.com/macropeople/dcfrontend/commit/f81505faf934b125e4d46c493ee66af0d9183b83))
* excel with additional columns issue ([b7d017d](https://gitlab.com/macropeople/dcfrontend/commit/b7d017dff02921abfb9e774652abf7bb0163225c))
* filter message should be removed when excel is uploaded ([3350da9](https://gitlab.com/macropeople/dcfrontend/commit/3350da96b60216493c32eac84228145cc6f3239b))
* filter RK bug ([b61c93d](https://gitlab.com/macropeople/dcfrontend/commit/b61c93d893fad5acc8b1f104dd2184a880e7a767))
* handsontable instance warnings, improved singleton when calling hot ([faccc11](https://gitlab.com/macropeople/dcfrontend/commit/faccc11eb452cc1b5c2497e1ad6c274889cb9755))
* more PG tests ([48836a0](https://gitlab.com/macropeople/dcfrontend/commit/48836a01ce3128131f1a01ff927cc021eb3669a4))
* pg direct - working branch ([0d46dd9](https://gitlab.com/macropeople/dcfrontend/commit/0d46dd98387baada4347a5c0d698079524e97f16))
* PG updates ([f41eefd](https://gitlab.com/macropeople/dcfrontend/commit/f41eefd8ea2615ad73510fa577274b22e4377e24))
* timestamps for PG compatibility ([aa321c2](https://gitlab.com/macropeople/dcfrontend/commit/aa321c295f05c881ec75dc6c40413ceb208778d5))
* using db compatible date system ([95a564c](https://gitlab.com/macropeople/dcfrontend/commit/95a564ce106142480f82321f201c6ae628e7fc1e))
* when no changes on approval screen state as such ([55a24db](https://gitlab.com/macropeople/dcfrontend/commit/55a24dbf322f110d567792b1cd24e7c612d7f493))

## [3.13.0](https://gitlab.com/macropeople/dcfrontend/commit/compare/v3.12.0...v3.13.0) (2021-08-25)


### Features

* backend for dynamic extended values, docs updated and tests written.  Available on MPE_TABLES.DSN column ([10fe4ba](https://gitlab.com/macropeople/dcfrontend/commit/10fe4bae537adf76e524b228d466c9bbe9d8ed81))
* date picker dropdown with values ([2be1ad7](https://gitlab.com/macropeople/dcfrontend/commit/2be1ad777254d69966b8b54b9b3abc719227a9c6))
* dynamic cell validation loading spinner ([e415476](https://gitlab.com/macropeople/dcfrontend/commit/e4154762b0d75f25be4727b566e012c860fb61a9))
* excel upload stater ([6aa35e1](https://gitlab.com/macropeople/dcfrontend/commit/6aa35e184e90b2d821031aea686f40e592e92f84))
* logger service, displaying console log when debug is on ([35992e0](https://gitlab.com/macropeople/dcfrontend/commit/35992e08d9b2db1b2133d2c1e23ce08686a62cd1))
* upload excel states and remaining time calculation ([7529d40](https://gitlab.com/macropeople/dcfrontend/commit/7529d408f0851318d099c5c438b02c76259713e7))
* **in progress:** extended values cell validation ([e44dc9a](https://gitlab.com/macropeople/dcfrontend/commit/e44dc9a065dbaa534a38f4edf5f6e097c5cd9413))
* row headers added ([55d5b24](https://gitlab.com/macropeople/dcfrontend/commit/55d5b24acb31c402c9d47499acff046c0605ba0f))
* **k8s:** dockerized frontend app ([5c9176c](https://gitlab.com/macropeople/dcfrontend/commit/5c9176c137041a862986217711304ecb3a67b793))
* **k8s:** set up k8s node with dcfront image ([b234b28](https://gitlab.com/macropeople/dcfrontend/commit/b234b283eeca353adb41aba2837539e3b1f75aa7))
* **server:** enabled typeScript ([2ac724e](https://gitlab.com/macropeople/dcfrontend/commit/2ac724e3e57700b9e5f291ff934097fa6fb6bbb2))
* **server:** initial commit ([8f2a0a5](https://gitlab.com/macropeople/dcfrontend/commit/8f2a0a5020b28866d2c35b3a8b22842dd3e89785))
* added test case to test the validity of uploaded values in xls extension ([7f7909b](https://gitlab.com/macropeople/dcfrontend/commit/7f7909bb62213cfd58eed047d15a91552723e4c2))
* adding datalist dropdown to time and date picker ([caaf5f9](https://gitlab.com/macropeople/dcfrontend/commit/caaf5f915228cd387b9711a0e9f2cd71d13c01ac))


### Bug Fixes

* adding demo key and fixing build process ([9298ef0](https://gitlab.com/macropeople/dcfrontend/commit/9298ef07f7ad038dcb9a0ed7d78dc8fe8a7a39bf))
* agree checkbox flow, open table from tree stability, library to open in cypress json ([1a3dcf2](https://gitlab.com/macropeople/dcfrontend/commit/1a3dcf229861fa9252d58196a93f61d572fe441a))
* backend updates ([1b3a61a](https://gitlab.com/macropeople/dcfrontend/commit/1b3a61ae1160b95044e8c3ccca1dcb5cbfab0f86))
* batch script updates, adding mpe_users to mpe_tables ([b5659d4](https://gitlab.com/macropeople/dcfrontend/commit/b5659d47e22f62483921ba460a7b59a44d98cc07))
* bumping core ([6034d12](https://gitlab.com/macropeople/dcfrontend/commit/6034d12a0c7d630015e3de34080cce07aea188b4))
* buttons focus outline remove ([c0348e3](https://gitlab.com/macropeople/dcfrontend/commit/c0348e3c56abf4476e3683f3cf91900ddab4a10e))
* created and implemented sheetInfo interface ([7408ad7](https://gitlab.com/macropeople/dcfrontend/commit/7408ad746cd83256ba20f496751f44d6fbe591db))
* date picker datalist arrow overlap with calendar. On loading screen abort modal not visible ([f426536](https://gitlab.com/macropeople/dcfrontend/commit/f4265365dc9fbe8548fb28a1f1d648e6aba16106))
* decoding licence key on http faling with invalid base64 ([5220cdb](https://gitlab.com/macropeople/dcfrontend/commit/5220cdb6d4d9ac86143454920b1d6eecb4d1a333))
* der touristik issue with error on table due to DQ check ([641a15c](https://gitlab.com/macropeople/dcfrontend/commit/641a15c0f723bb19937e38dbe278de5f708c1dc1))
* displaying the error message that comes in response from adapter ([43e5e31](https://gitlab.com/macropeople/dcfrontend/commit/43e5e31fdafa1c4cbb0af60c467407c147d5f1f5))
* dynamic cell validation on numeric cells, numeric dropdown with sofselect issues ([4c8a68a](https://gitlab.com/macropeople/dcfrontend/commit/4c8a68aa5ede968435d47031b6d3f6e5c0731f52))
* empty sheets triggering error (update range function) ([2caa513](https://gitlab.com/macropeople/dcfrontend/commit/2caa51354a8018d03297a4bb455ab1a5ff4bac6a))
* enable support for backend rls checks ([0422c39](https://gitlab.com/macropeople/dcfrontend/commit/0422c39c1450b65c2c678d2305647165acf93db8))
* enabling hook when submitting RLS and adding query object to VIEWdata response ([6023e8b](https://gitlab.com/macropeople/dcfrontend/commit/6023e8bf4e54feae68ad9c0312dd5ec43aea5084))
* enabling post edit hook for mpe_validations to check each table query seperately ([3906316](https://gitlab.com/macropeople/dcfrontend/commit/3906316f805758d5daee38c356447c3a67fd4263))
* excel finding data in file issue ([7d17685](https://gitlab.com/macropeople/dcfrontend/commit/7d17685e5c1eee35458d35e9979968778f072aa9))
* excel tests fixing ([a27066e](https://gitlab.com/macropeople/dcfrontend/commit/a27066e2b0ac9ebde3bff7c57240b9885dd47b72))
* excel upload with missing, duplicate and blank columns ([038b32f](https://gitlab.com/macropeople/dcfrontend/commit/038b32f6517cc5595a94fa664afffbaedd7ac340))
* excel with missingg first row upload fail ([839f34a](https://gitlab.com/macropeople/dcfrontend/commit/839f34a70481f6b35e172392a52d3d43ef9addc6))
* handle the string which contains apostrophe ([fdde83f](https://gitlab.com/macropeople/dcfrontend/commit/fdde83f51468bafce73d10203f2f19b00fc952ba))
* hardselect_hook prevent invalid submit ([127ff70](https://gitlab.com/macropeople/dcfrontend/commit/127ff7056f92a4bac1117f319e79b1331b55a3f3))
* history download context passed ([7fc3637](https://gitlab.com/macropeople/dcfrontend/commit/7fc36377d236537eb2aaa729cc3e39c5bda7da6c))
* immediately remove approve list item after clicking on reject button ([5004988](https://gitlab.com/macropeople/dcfrontend/commit/5004988132b03b29403e1b56b65423a016c226c0))
* incorrect group logic ([0be9e18](https://gitlab.com/macropeople/dcfrontend/commit/0be9e18b3395d8f6df1351d07e5167d90e7234f3))
* issue with filters in different tables being combined ([51d3c98](https://gitlab.com/macropeople/dcfrontend/commit/51d3c98ca8dc065e16bfa4e645f54f28a3701fae))
* lineage double request ([5a78a30](https://gitlab.com/macropeople/dcfrontend/commit/5a78a30dc8830ea81dc33ee90d3115513c13926c))
* made some attributes optional ([9879633](https://gitlab.com/macropeople/dcfrontend/commit/9879633d67f84ab9c4943efa564453aeffb18580))
* merge conflict ([957f4f6](https://gitlab.com/macropeople/dcfrontend/commit/957f4f635d5ef2de1980006987939c45f3835939))
* message typo ([b231651](https://gitlab.com/macropeople/dcfrontend/commit/b23165171a728eec59c0c18c1225e1f17a7f1a57))
* metadata aligment and loading spinner alignment ([385da0e](https://gitlab.com/macropeople/dcfrontend/commit/385da0e2df3964d2e51ab1a4beb711d1f697ec2e))
* metadata object not sending request ([a616a42](https://gitlab.com/macropeople/dcfrontend/commit/a616a42e1dc3dc2e337c2db27e11ad7468e5a779))
* metanav search issue ([e6a727a](https://gitlab.com/macropeople/dcfrontend/commit/e6a727a32e56fa7e08d9559b5bbf230f1713b4b9))
* new clarity styling issues ([50c9a23](https://gitlab.com/macropeople/dcfrontend/commit/50c9a23e6464d381382b197fabae5d1830af088c))
* nobs value incorrect in filtered view on VIEW table ([23fa2ce](https://gitlab.com/macropeople/dcfrontend/commit/23fa2ceaab050eb6e6a5503cc72d94d7762c9225))
* non picker input dropdown not visible ([4de8726](https://gitlab.com/macropeople/dcfrontend/commit/4de8726c5f660b4763dee4f03f7dc471c40848b9))
* not null validations not applied ([eccc28e](https://gitlab.com/macropeople/dcfrontend/commit/eccc28ee877d9c57294cb5d72a315fdfbfe51541))
* numeric column freeze when put char value ([d2cebc5](https://gitlab.com/macropeople/dcfrontend/commit/d2cebc50b3ccbd04f0a609c9299b874f0f334a14))
* pass debug parameter in upload file request when deug mode is enabled ([ab8ab28](https://gitlab.com/macropeople/dcfrontend/commit/ab8ab2844e2fc390bfdd2e35803627724cb3ed0e))
* postgres compatibility ([700fa74](https://gitlab.com/macropeople/dcfrontend/commit/700fa74fe223b08de627569c8739406e92d7b6c2))
* Primary key value lost when uploading excel file fixed ([0672a55](https://gitlab.com/macropeople/dcfrontend/commit/0672a556d59a9593b5109def961cae516dd1c2d8))
* reproduce filter for viewer component ([6edb0aa](https://gitlab.com/macropeople/dcfrontend/commit/6edb0aa410aadc1fc067f7b49c58fa93df283e8f))
* RLS lib assign ([6b274eb](https://gitlab.com/macropeople/dcfrontend/commit/6b274eb1f75c226ad8619bccd30ddb9cf4adb4d0))
* sas fixes ([0f4a1bb](https://gitlab.com/macropeople/dcfrontend/commit/0f4a1bbbd99afd4fe769b796d0428c066801166e))
* updates for PG compatibility and to improve logging in case of hard exit ([6f43756](https://gitlab.com/macropeople/dcfrontend/commit/6f437563b5f814e15f93b56ddf3d9d41b54640c5))
* updating test and config to use correct context, also updating tests so that they actually run with the designated context ([96aadb1](https://gitlab.com/macropeople/dcfrontend/commit/96aadb1b056b44bbc3dcc0b82a0504a6b90840de))
* validatefilter not handling sasjsAbort ([ee444bb](https://gitlab.com/macropeople/dcfrontend/commit/ee444bb11a566f9b3dee1ff91bb97017856d4a39))
* restructured repository ([b0accc5](https://gitlab.com/macropeople/dcfrontend/commit/b0accc5509f8dd3946763c8b4fddbf3691c98fff))
* restructured repository ([64530a6](https://gitlab.com/macropeople/dcfrontend/commit/64530a664780264b4b9264897d4c70d2efc54237))
* **ci:** fixed merge request CI step ([687785c](https://gitlab.com/macropeople/dcfrontend/commit/687785cb09bc8c271705b4fc9daae90f5ec052f6))
* **CI:** fixed gitlab-ci ([84bc6ba](https://gitlab.com/macropeople/dcfrontend/commit/84bc6ba9fa9508324ab5fc7eba2ad4fdf445c861))
* **k8s:** fixed ingress service ([6a71df7](https://gitlab.com/macropeople/dcfrontend/commit/6a71df7c8a1602ff8972ebad905886707c552779))
* timeout set globally ([79c0250](https://gitlab.com/macropeople/dcfrontend/commit/79c0250f06ab5fc2dfb3f290456cd266dc517729))
* validation for numeric cells ([6b9dba5](https://gitlab.com/macropeople/dcfrontend/commit/6b9dba542d2ce2e94c4368cce665b351e26a332f))
* when user open filter url show him filter clauses in filter dialog box ([b800672](https://gitlab.com/macropeople/dcfrontend/commit/b800672ad51a5443eca76cf86d468c5c0443be6a))

## [3.12.0](https://gitlab.com/macropeople/dcfrontend/compare/v3.11.0...v3.12.0) (2021-05-11)


### Features

* adding dynamic filter query to public/getcolvals service along with corresponding tests (and updates to test config) ([df86c39](https://gitlab.com/macropeople/dcfrontend/commit/df86c39aabf264236580c283a56e378a106dc6d6))
* backend logic for formula validation ([7d17aca](https://gitlab.com/macropeople/dcfrontend/commit/7d17aca7b982749f42303d51ecdd9cb549e73e9b))
* check for invalid clauses when adding new group clause ([085ede6](https://gitlab.com/macropeople/dcfrontend/commit/085ede69cb4056e598460150a0b5443398dedb94))
* checkbox values moved in modal. fix: selecting IN operator before getValues is finished did not populate values. ([831f6b7](https://gitlab.com/macropeople/dcfrontend/commit/831f6b794e8f3477ff2c3e1b49845540dba8425c))
* creating dynamic where clause string and passing ([2724383](https://gitlab.com/macropeople/dcfrontend/commit/272438366702a2ac7d333b0e6e9ae0a97c1f8e2a))
* getdynamiccolvals service (and test) ([c5a8df8](https://gitlab.com/macropeople/dcfrontend/commit/c5a8df870a83d460840f220ce3a33c1fb55a22f4))
* getdynamiccolvals v0 ([44be1de](https://gitlab.com/macropeople/dcfrontend/commit/44be1de57ceec597d14acc6e8e0a86c1f8d2f42f))
* lineage depth level pick ([46e4601](https://gitlab.com/macropeople/dcfrontend/commit/46e4601a1c9eaaa907c60adf16cccb7a02bb30bd))
* max depth lineage limit ([569a560](https://gitlab.com/macropeople/dcfrontend/commit/569a5606d8d743a1ba9ac19e388a960b0f320409))
* mpe_filtermaster macro to handle all filter generation (and corresponding tests) ([ddf88f0](https://gitlab.com/macropeople/dcfrontend/commit/ddf88f027bf23d64857628b040fb1419ba9e8c19))
* new validatefilter service (replaces whereclausevalidator) ([909ba02](https://gitlab.com/macropeople/dcfrontend/commit/909ba02b4288e2d4c3c266389ba6432fd3f85aac))
* reset filter button, improved datalist elements ([bd42094](https://gitlab.com/macropeople/dcfrontend/commit/bd42094919550752c53927d95f5c855d851e131a))
* Row Level Security (and test) ([5e0fe2f](https://gitlab.com/macropeople/dcfrontend/commit/5e0fe2f80223966a65e5878dbc250eefa61d995b))
* where clause check on adding new ([82410d2](https://gitlab.com/macropeople/dcfrontend/commit/82410d2cd4e011406e3d866571f703fb01bf708c))
* while uploding EXCEL parse formulas and formatted values ([5008010](https://gitlab.com/macropeople/dcfrontend/commit/500801042103480e034cd489d3a64d196a780432))


### Bug Fixes

* 0 rows on filter, no changes modal improved ([0318d86](https://gitlab.com/macropeople/dcfrontend/commit/0318d864459e1c46b3e2be919c48d7602cb77872))
* add record edits newly added row ([91328ea](https://gitlab.com/macropeople/dcfrontend/commit/91328eab83e5000b8d2a7038de230b0cb16e3472))
* adding default hook for mpe_security ([d282fd2](https://gitlab.com/macropeople/dcfrontend/commit/d282fd27a602b8576f5c7d63b6781c350a50d058))
* adding header to getdynamiccolvals ([c8f054a](https://gitlab.com/macropeople/dcfrontend/commit/c8f054af26cdc90afdd3737b3ef2fe6470d23ee7))
* adding outds so can send back to frontend ([6151391](https://gitlab.com/macropeople/dcfrontend/commit/6151391443293242c320e3b94181d5a107ed0934))
* adding stub for service ([0a32bfe](https://gitlab.com/macropeople/dcfrontend/commit/0a32bfe6d380c392759412fb2b57b1c99b71abe9))
* adding tests and new macro for fetching test response ([0fd4e35](https://gitlab.com/macropeople/dcfrontend/commit/0fd4e358f0323b0137e3f3ed36819b432992fa84))
* app break on lineage error when debug off ([f4f1fae](https://gitlab.com/macropeople/dcfrontend/commit/f4f1faea56742f8236d3d4837ac09d185072054b))
* appLoc check support for old adapter ([4d6fc29](https://gitlab.com/macropeople/dcfrontend/commit/4d6fc29a61cee1282286f40ce1874dcf6fd05da0))
* appLoc check, with adapter function ([2fa0de9](https://gitlab.com/macropeople/dcfrontend/commit/2fa0de946cce7ba4288a1ab645cfb80ea1839d35))
* approver list for *ALL* listings ([54a4fdf](https://gitlab.com/macropeople/dcfrontend/commit/54a4fdf388cebbe51e95ea2c60d8c605b3507145))
* auto deploy close modal button ([3657ae1](https://gitlab.com/macropeople/dcfrontend/commit/3657ae1448ae954145cd6ee8a9bf08fb5d463454))
* bumping adapter and closing [#305](https://gitlab.com/macropeople/dcfrontend/issues/305) ([2f3d49c](https://gitlab.com/macropeople/dcfrontend/commit/2f3d49cb529ca95a71bd4a58115edb586a98bb0b))
* bumping sasjs/core ([3b0896d](https://gitlab.com/macropeople/dcfrontend/commit/3b0896d7be0ba403a8701a96bc6e662fa1f09947))
* changes required by axios-adapter ([eac1bb5](https://gitlab.com/macropeople/dcfrontend/commit/eac1bb566d9927782ae372af51dd400657d347b0))
* check to ensure table exists.  Closes [#223](https://gitlab.com/macropeople/dcfrontend/issues/223) ([ee5b21a](https://gitlab.com/macropeople/dcfrontend/commit/ee5b21a2bd4ce1743ed550011577c06ac9041953))
* conflicts: ([92d6b4d](https://gitlab.com/macropeople/dcfrontend/commit/92d6b4d0c772f1dda7e548fb69a4e58624542fbc))
* datalist improving UX (show all values on click) ([99f298d](https://gitlab.com/macropeople/dcfrontend/commit/99f298d5abd3243c6b0070f8bb3c1ba95f52eb48))
* dependencies ([42f23d4](https://gitlab.com/macropeople/dcfrontend/commit/42f23d43691fa7e41f64e9d87dd50695934d8256))
* deploy page ([0c1c312](https://gitlab.com/macropeople/dcfrontend/commit/0c1c312ce7c205d468909d736bea0a1cb49842e2))
* double click opens empty dropdown ([c751295](https://gitlab.com/macropeople/dcfrontend/commit/c75129559fb26f94dc4f86b5f5781844f335f50f))
* download audit file error ([8a92657](https://gitlab.com/macropeople/dcfrontend/commit/8a9265779481a8955185e564c57297243366c8e7))
* dynamic filtering is true by default ([47d6c37](https://gitlab.com/macropeople/dcfrontend/commit/47d6c37127439bd21def7415073893bb0a98eabc))
* dynamic where clause was sending null values ([aad3d94](https://gitlab.com/macropeople/dcfrontend/commit/aad3d948895d89905e80d1d8a3751d8f551a70fb))
* editor breaks if submit service fails ([eef29ba](https://gitlab.com/macropeople/dcfrontend/commit/eef29ba679cfe0b67f5a58216f6d6fb2fc38a97c))
* editor reset filter not working ([9b2e5a9](https://gitlab.com/macropeople/dcfrontend/commit/9b2e5a9b4f7c44b9fc04a96d5376fc22c896f9fe))
* enabling excel export for non PC files sites, also updating tests ([35a36e7](https://gitlab.com/macropeople/dcfrontend/commit/35a36e705c17b5978884c53912d4a673400bf670))
* enabling filtering for getdata service, plus a test, plus fixes to make the test work ([69c6dde](https://gitlab.com/macropeople/dcfrontend/commit/69c6dde4322e096c12cc312a3e3a1e4d5bb889ca))
* enabling uploads in excel for non admins with *ALL* security settings ([46a083a](https://gitlab.com/macropeople/dcfrontend/commit/46a083a9e6453873933e57118fe6e3bfc66ee3b6))
* ensuring selectbox order is honoured.  closes [#283](https://gitlab.com/macropeople/dcfrontend/issues/283) ([124c1d3](https://gitlab.com/macropeople/dcfrontend/commit/124c1d3d9372dc37f7fcae0e375577eedd77f942))
* error on compilation ([574d1ca](https://gitlab.com/macropeople/dcfrontend/commit/574d1ca615458f8447ee0d9c76c63f145f7def6c))
* extending viz memory, added rendering spinner, added download png and svg buttons in the modal ([d6f674f](https://gitlab.com/macropeople/dcfrontend/commit/d6f674f56e1342c7069194b40a22a2df6261f9bc))
* filter text removeal ([08bcc4a](https://gitlab.com/macropeople/dcfrontend/commit/08bcc4a858dff402dd49ee8714f1f1b0bf341a73))
* filter_pk numeric ([e5a7728](https://gitlab.com/macropeople/dcfrontend/commit/e5a77288be0077355449d7dd520a7c4aea42d2d3))
* filter_pk numeric ([f0a1d7b](https://gitlab.com/macropeople/dcfrontend/commit/f0a1d7bec28f9b2847640d129a770eca4362ffa2))
* filtering datetime wrong time on some months ([00c2d4e](https://gitlab.com/macropeople/dcfrontend/commit/00c2d4ea92afadb9ab47cf1d2c9de82827fed0b8))
* filterquery for numeric and string ([29d4967](https://gitlab.com/macropeople/dcfrontend/commit/29d49679b668d1d41968a9d1fc3fdc172112415f))
* filterTable params fixed for getColVal request ([f6b43e2](https://gitlab.com/macropeople/dcfrontend/commit/f6b43e20b1b2792968d4c661a35be2018f25e151))
* fixing dates on fronted.   REquires an adapter update. ([544124b](https://gitlab.com/macropeople/dcfrontend/commit/544124b3914939b603ddab97b709adb8c187ce45))
* getcolvals breaking filtering ([086cd64](https://gitlab.com/macropeople/dcfrontend/commit/086cd64bdb8b985317ae5c40cdfa95d2161c7e59))
* getting tests to (finally) work ([caeea9a](https://gitlab.com/macropeople/dcfrontend/commit/caeea9a72e7400d8a6ae6ce4b6bbb0666f968193))
* getting viewdata to work ([d78cfa5](https://gitlab.com/macropeople/dcfrontend/commit/d78cfa56cbcc363eb1ba35c6bfd38080d3141126))
* http key support ([a166665](https://gitlab.com/macropeople/dcfrontend/commit/a1666658ceabe5c2094f1b33f86034ca445ebafc))
* I ran npm run format:fix ([7c5814f](https://gitlab.com/macropeople/dcfrontend/commit/7c5814fd729f04acc78d77c217e06611832552e8))
* improved progress bar, passing dynamic clause object to server ([c0a37a4](https://gitlab.com/macropeople/dcfrontend/commit/c0a37a4b8941fc5253d2462766c8be053febd6b0))
* large lineage handling improved UX ([316af47](https://gitlab.com/macropeople/dcfrontend/commit/316af476580a774538615bdb318a30310efec187))
* last minute fixes ([4399118](https://gitlab.com/macropeople/dcfrontend/commit/4399118d92941baf92de32c852c3a8ec6ab1e54c))
* last minute updates ([d58dc0a](https://gitlab.com/macropeople/dcfrontend/commit/d58dc0a3fe061be53163cd4a521a4d72ee8ebe8d))
* line length ([15e596a](https://gitlab.com/macropeople/dcfrontend/commit/15e596a1cd009d807d82ab9890bd7e7e506555b0))
* lineage not rendering issue ([ba932f4](https://gitlab.com/macropeople/dcfrontend/commit/ba932f4c43fd6c3762e773143e2060eda6b4e867))
* lint fixes ([fa6fbcc](https://gitlab.com/macropeople/dcfrontend/commit/fa6fbcc3bd45b504f8242a3b8924d5d32f7f456e))
* max_depth ([b90efe0](https://gitlab.com/macropeople/dcfrontend/commit/b90efe01b5ffa5d5761d01a1f5839f9cabd0cdfd))
* merge ([3f4db37](https://gitlab.com/macropeople/dcfrontend/commit/3f4db379b5c3ada8a29a8221ea810e56f2299cc7))
* merged cells excel upload fails ([df5e6d7](https://gitlab.com/macropeople/dcfrontend/commit/df5e6d7848e1864554bdc98daf6d9215f2caa40d))
* migration DDL ([63b7dd9](https://gitlab.com/macropeople/dcfrontend/commit/63b7dd988a61467b359eb285ce4e9976a5c2c2e5))
* minval validation broken ([ce1db82](https://gitlab.com/macropeople/dcfrontend/commit/ce1db828662185e2c58410d3c5f45eae84f9a154))
* missing changes and improvements ([d463d32](https://gitlab.com/macropeople/dcfrontend/commit/d463d32e18da4093e55ed64c4fedba40eca02b9e))
* more debugging, closes [#282](https://gitlab.com/macropeople/dcfrontend/issues/282) ([eb26f89](https://gitlab.com/macropeople/dcfrontend/commit/eb26f89e045d676dca5c1868dc491685e0289b4a))
* moving meta_mapper into designated folder, so it can now be overridden by the end user ([595f08d](https://gitlab.com/macropeople/dcfrontend/commit/595f08dc9490d5424c60e2d2ae053d523cc8265d))
* moving tests into same folders as services ([19ada72](https://gitlab.com/macropeople/dcfrontend/commit/19ada72845725722266827beff872e1c24aed62e))
* mpe_config issues ([315ed39](https://gitlab.com/macropeople/dcfrontend/commit/315ed39fb7d751c800ecc8436d079bd34a0e99ae))
* mpeinit ([b541f58](https://gitlab.com/macropeople/dcfrontend/commit/b541f58788925c772be79f5888e0caff0070c26a))
* mpeinit failure on sas 9 configurator ([9ace8e1](https://gitlab.com/macropeople/dcfrontend/commit/9ace8e172ff8059cf3ec0e64a8b3233276e21c96))
* one clause in group subgroup logic is undefined ([7cca83b](https://gitlab.com/macropeople/dcfrontend/commit/7cca83b2d0c02231aa4c22721592f061b5a1f6d4))
* ran npm run format:fix ([eda5785](https://gitlab.com/macropeople/dcfrontend/commit/eda578585fa174c529a8c866e8e112126478b22b))
* reducing rowcount to 100 for performance, also updating sasjs doc ([a612d33](https://gitlab.com/macropeople/dcfrontend/commit/a612d33c31c022dac6b8e9b1e334fe9b77313e47))
* removed services from appLoc ([4d390df](https://gitlab.com/macropeople/dcfrontend/commit/4d390df35a2b771acc7c7fc5b8b3d1276e8d421f))
* removing FILTER_JSON ([4e7b549](https://gitlab.com/macropeople/dcfrontend/commit/4e7b549b3a9f45b52eb7e0da911da8e35fdf44c1))
* removing nested macro ([2641e41](https://gitlab.com/macropeople/dcfrontend/commit/2641e415eab27ac1c1cd33004aea856538021ba2))
* removing nested macros ([70f25d2](https://gitlab.com/macropeople/dcfrontend/commit/70f25d217ca3dcb12b4f4422284451e7127d8086))
* replaced NOT EQUAL with NE ([696348a](https://gitlab.com/macropeople/dcfrontend/commit/696348aadd5e48d1bf08aeee2a5cbe9d9aba2e54))
* RLS hook, max-depth, mor eRLS values ([d2e9fa3](https://gitlab.com/macropeople/dcfrontend/commit/d2e9fa3552e409fb2cc48401a6f4eb013c5e3d4a))
* sanitising inputs ([04855b9](https://gitlab.com/macropeople/dcfrontend/commit/04855b925ce64c2c84e6434b05be5a22a741b378))
* sasjs/core bump ([2b57031](https://gitlab.com/macropeople/dcfrontend/commit/2b570317f00573e24ec679d54e2b2d21bb37db0d))
* shortening token ([49f17e7](https://gitlab.com/macropeople/dcfrontend/commit/49f17e755c32ed01531f9953ad73d9c2c2e15d12))
* small performance tweak in filtering ([672d51c](https://gitlab.com/macropeople/dcfrontend/commit/672d51c3da0db809b5712e7fb67b41e718ed181b))
* soft select in edit modal is working now ([98fa2d9](https://gitlab.com/macropeople/dcfrontend/commit/98fa2d9b84fc1e844a1521fdc8cb348f51eeaef8))
* sql update for history ([e1a3511](https://gitlab.com/macropeople/dcfrontend/commit/e1a3511a8768195f666254225e23965f1984a5af))
* styling issues ([720adb2](https://gitlab.com/macropeople/dcfrontend/commit/720adb222cdcf07cedb640d97bb534d2252b1a89))
* table max depth ([352e2c0](https://gitlab.com/macropeople/dcfrontend/commit/352e2c0ac8635cbc52fedd3ac3aa954ed766c95f))
* table on click scroll into view ([87f72be](https://gitlab.com/macropeople/dcfrontend/commit/87f72beba9d345c1635769695c466af1a209e363))
* test (and fix) for etlsource, upcasing returned cols & tables ([77ddff5](https://gitlab.com/macropeople/dcfrontend/commit/77ddff51cd07bed962d39de9748728401933e7aa))
* tests ([8c0eb1b](https://gitlab.com/macropeople/dcfrontend/commit/8c0eb1b1c6f1f2464c4f4cca3b9d18cae80ef917))
* tests and extra validations routines ([3dbfad8](https://gitlab.com/macropeople/dcfrontend/commit/3dbfad84a9b249b536d2041b4ee1863a412f8c9e))
* tidy up sasjsconfig.json ([4ed469b](https://gitlab.com/macropeople/dcfrontend/commit/4ed469b3ea9edadf04ca2b70eaf29b5ddf0dfeae))
* tree on generating graph scrolls to top ([ccaac6f](https://gitlab.com/macropeople/dcfrontend/commit/ccaac6f8a60b8968d5c05396b823b5af6deb3b1a))
* typo and fixes ([b258426](https://gitlab.com/macropeople/dcfrontend/commit/b2584266de6eb2b96a8cc61b4927e1811c64a75c))
* uknown error to SAS Service Error ([564e893](https://gitlab.com/macropeople/dcfrontend/commit/564e8934388ba135acc89417a7a1ac31798dd852))
* updated upload excel with formula test to check parsed values ([db58aa0](https://gitlab.com/macropeople/dcfrontend/commit/db58aa085dee3a616f4e1015868fbb5bf9a9be3c))
* updates for viya testing ([637c1e1](https://gitlab.com/macropeople/dcfrontend/commit/637c1e16390e8c72bce9bd6d907e70b7e8366f02))
* updates to make sasjs lint work ([fc5fe54](https://gitlab.com/macropeople/dcfrontend/commit/fc5fe54a9ac8c4716d4a4ceb58dd583918ae47ac))
* updating dc tests for SAS 9 with macro ([f42012f](https://gitlab.com/macropeople/dcfrontend/commit/f42012f4d368b8043e227ca25fdcb3032bc58887))
* url issues in deployment ([f6f933d](https://gitlab.com/macropeople/dcfrontend/commit/f6f933dbc6e465963474accbcc49cadfd83ce70f))
* viya users limit ([d6961f4](https://gitlab.com/macropeople/dcfrontend/commit/d6961f415b0826be75dd77999459583da320419e))
* warning on large dot files, hot dropdown style fix ([eb17798](https://gitlab.com/macropeople/dcfrontend/commit/eb177984387368af1d2454f20b73a6720e3cc040))

## [3.11.0](https://gitlab.com/macropeople/dcfrontend/compare/v3.10.0...v3.11.0) (2021-02-14)


### Features

* Adding DC_LOCALE option - will override locale when set to anything other than SYSTEM.  Closes [#237](https://gitlab.com/macropeople/dcfrontend/issues/237) ([419f022](https://gitlab.com/macropeople/dcfrontend/commit/419f022fd13e466058d2de017e85a8a9205400e1))
* dragging file over table opens drop dialog ([9d8ecc8](https://gitlab.com/macropeople/dcfrontend/commit/9d8ecc8a87ed5367c1cabc28b3d7c1e5a5ed66f7))
* enabling column level lineage to 'pass through' proc transpose ([66c911e](https://gitlab.com/macropeople/dcfrontend/commit/66c911ea96e97f573d2834bdb2a65755e65a145c))
* enabling native REDSHIFT access with incremental loads ([1532305](https://gitlab.com/macropeople/dcfrontend/commit/15323052086b13b2a57dd4e0b43bfe76ac47ff03))
* extra validations for MPE_TABLES cols (upcase for VAR_TXFROM, VAR_TXTO, VAR_BUSFROM, VAR_BUSTO and VAR_PROCESSED) ([5cd3434](https://gitlab.com/macropeople/dcfrontend/commit/5cd34345ecc992ea3401d1f590837b3fa9c9ec46))
* rows number thousand separator (space) ([3644f35](https://gitlab.com/macropeople/dcfrontend/commit/3644f3502f92b87d91fc0b1f3b4c1a3758247ff9))
* **licensing:** 2 more tests added for no of users reached/exceeded ([d615eea](https://gitlab.com/macropeople/dcfrontend/commit/d615eea5f2683b6384537cc164d33c9fda941197))
* **licensing:** activation logic added, refactor of terms agreement, license events interconnected ([89f20ae](https://gitlab.com/macropeople/dcfrontend/commit/89f20ae9bc115f721d92828e0d386dc162a8d6bc))
* **licensing:** added site_id check, added more error handling for decrypt function ([c54074c](https://gitlab.com/macropeople/dcfrontend/commit/c54074c5330e6fe720b68ddb21db2c0296857792))
* **licensing:** fixes ([acbc326](https://gitlab.com/macropeople/dcfrontend/commit/acbc3269aa9b3f4554c22509f1d6e0a69ec9f366))
* **licensing:** fixes ([eea2c96](https://gitlab.com/macropeople/dcfrontend/commit/eea2c96204a6232581eba6d44ff3da1987b3ea9a))
* **licensing:** licensing/update path to update the license key, when app is activated ([7145150](https://gitlab.com/macropeople/dcfrontend/commit/71451505f7d8509e9cd174fd6168621bb726b944))
* **licensing:** register key function ([ccf8e50](https://gitlab.com/macropeople/dcfrontend/commit/ccf8e50ba169851e15e1aef3527ebff27b97b6cc))
* added colored logs in console for matching errors with tests ([2e55a59](https://gitlab.com/macropeople/dcfrontend/commit/2e55a59dde805c5b8eb173af052dc0d20d7e33d2))
* admin/registerkey service.  Expects an input table called keyupload, with 2 variables - activation_key and licence_key ([3bad92f](https://gitlab.com/macropeople/dcfrontend/commit/3bad92fdf7a5dca3111ca77a701ccfdb5a3c0319))
* auto deploy improvements ([d290a96](https://gitlab.com/macropeople/dcfrontend/commit/d290a962cd03c70d1efbd69a5dbf5fb2a03aebe3))
* backend service registration code@ ([e1c7ac8](https://gitlab.com/macropeople/dcfrontend/commit/e1c7ac86165080da4097ea40fffebdb0c3910518))
* creating context for auto deploy ([6811828](https://gitlab.com/macropeople/dcfrontend/commit/6811828e9b12af2923a79822d701184e142ac8a4))
* cypress UI testing, started ([8ef54c4](https://gitlab.com/macropeople/dcfrontend/commit/8ef54c43e570bea355c4f84bbc346dc328826ebb))
* deploying services as part of CI ([e981ec0](https://gitlab.com/macropeople/dcfrontend/commit/e981ec04aaa4ac998eb1c207da5e20e9ff3dcbb5))
* excel file discard button and UX fixes ([91f4dc3](https://gitlab.com/macropeople/dcfrontend/commit/91f4dc3c2551e35b73342ab685c2b14dafb0681d))
* handling the service not found error ([402e4ec](https://gitlab.com/macropeople/dcfrontend/commit/402e4ec1bc950f795a1c8251c973b85d07461580))
* licensing and user registration ([0f9e114](https://gitlab.com/macropeople/dcfrontend/commit/0f9e114080e4f5e09487f4144f24cbafc05df1b3))
* licensing proof of concept ([80a7ebe](https://gitlab.com/macropeople/dcfrontend/commit/80a7ebe3acc9241b53c42fd3c640585b1061abe5))
* push on dfevelopment deploy SAS services ([6815ab8](https://gitlab.com/macropeople/dcfrontend/commit/6815ab8961615ef5263ebbc200d84b354433f856))
* regular excel upload test ([a07c739](https://gitlab.com/macropeople/dcfrontend/commit/a07c7395e2aded774632dd55f0f8a3c40e88b488))
* requests modal on autodeploy ([62bc1bd](https://gitlab.com/macropeople/dcfrontend/commit/62bc1bd9d8304e74d3bc9cf03f747cbbab907082))
* updating context with makedata response ([cf9109a](https://gitlab.com/macropeople/dcfrontend/commit/cf9109a0a3f2e6062f28aa858f4ac8b6f3873d67))
* user registartion. Fixed demo expiry on production issue. Added evaluation agreement. ([a42fcc1](https://gitlab.com/macropeople/dcfrontend/commit/a42fcc15035f74861fa777603734e0ccdca40be7))


### Bug Fixes

* `trim` error when chaning values in filter while variable not set ([a8019ce](https://gitlab.com/macropeople/dcfrontend/commit/a8019cee5f957573b54932c5d926af6f8006ab5c))
* abortModal not working, hashing disabled (it was bloating the server), testing fixes ([9f7aa8f](https://gitlab.com/macropeople/dcfrontend/commit/9f7aa8fb84fba86be174df62f5140257bf03a648))
* activation key in startup service ([4fe5d53](https://gitlab.com/macropeople/dcfrontend/commit/4fe5d53ac8e821657fdb0efd1c757c9480eec3e6))
* add dependency@ ([5127159](https://gitlab.com/macropeople/dcfrontend/commit/51271596bb9de8fe449091647533607982e4d031))
* added contextname to viewer download ([f1d9c35](https://gitlab.com/macropeople/dcfrontend/commit/f1d9c35925c30b3bec773df19a1161b670c21fcf))
* adding editors to history, closes ([c464ee8](https://gitlab.com/macropeople/dcfrontend/commit/c464ee86d0581ada4ae0f11a85ff413dacef9d04)), closes [#233](https://gitlab.com/macropeople/dcfrontend/issues/233)
* adding engine ([0309dfa](https://gitlab.com/macropeople/dcfrontend/commit/0309dfa63c377d8f9e7602f1dcbb83a4424df70f))
* adding licencekey to startupservice ([83b21c9](https://gitlab.com/macropeople/dcfrontend/commit/83b21c93d7fdcf44966c4f67364a5994bd678e7d))
* adding reason message to emails ([48dfb43](https://gitlab.com/macropeople/dcfrontend/commit/48dfb4370dfdbbab3dfd9e903cf99d8b4b7cea0c))
* alignemnt button not applying changes ([af7cbda](https://gitlab.com/macropeople/dcfrontend/commit/af7cbda41bff4e9564c466a5b73478dbd6e8a38d))
* app crashes if activationkey is invalid string ([e1dacd8](https://gitlab.com/macropeople/dcfrontend/commit/e1dacd81d3ac4edbe8d1c796bb8165654cc54dd2))
* appLoc check before running startupservice ([802b178](https://gitlab.com/macropeople/dcfrontend/commit/802b17893e36e228210199d9229b8b65a9f25884))
* approver name ([8b79eee](https://gitlab.com/macropeople/dcfrontend/commit/8b79eeee1c235cdf93cb6dbc1eeb65eaada1c2e6))
* bitemporal loader error (causing lock to be left behind) ([8a53226](https://gitlab.com/macropeople/dcfrontend/commit/8a53226dadd273b7ba9620ac2f03c00c76f1264c))
* bump sasjs/core ([4d76965](https://gitlab.com/macropeople/dcfrontend/commit/4d76965ea0126d5701fe6a5d5afd3ce8940d4cc6))
* cannot change column width while filtering ([7672561](https://gitlab.com/macropeople/dcfrontend/commit/767256128d3e7c2441741d749d401956459e2b11))
* catchphrase fixed ([c5af7a5](https://gitlab.com/macropeople/dcfrontend/commit/c5af7a505331c65128a4b98f87372f9c3039f357))
* checks for approval, closes [#234](https://gitlab.com/macropeople/dcfrontend/issues/234) ([4f995ff](https://gitlab.com/macropeople/dcfrontend/commit/4f995ffcfc9700add88ac9ac787b37eb9ed1d702))
* CI script updated to work with newest CLI ([f336ee6](https://gitlab.com/macropeople/dcfrontend/commit/f336ee6c22a93930c509c9c0bb829d1282f9870e))
* cli v2 ci script, refresh viewer table, autofocus tree search for libraries, apply keys redirection wrong ([049e872](https://gitlab.com/macropeople/dcfrontend/commit/049e87214beb559c5deadb25669fc34ca7237922))
* col names, debugging, and ddl file ([5876d22](https://gitlab.com/macropeople/dcfrontend/commit/5876d22348b1b8ff3d3e344b85f25aee9c3dd811))
* config ([08d6be5](https://gitlab.com/macropeople/dcfrontend/commit/08d6be53ac7d900b2ae08bc084b75c2bbf393a2e))
* config update ([b2e5e01](https://gitlab.com/macropeople/dcfrontend/commit/b2e5e01eabc898056d659eb683fa2820b940eda5))
* converting uploaded data to correct types ([ce7b6d7](https://gitlab.com/macropeople/dcfrontend/commit/ce7b6d7cd2c3e67c70d772f1e69891f7961fd992))
* copying primary key field with dragging ([7105a7a](https://gitlab.com/macropeople/dcfrontend/commit/7105a7a0b4b86072466696a50f7f5e3f3d9b4a1d))
* cypress new clarity and angular elements getter fix ([15540db](https://gitlab.com/macropeople/dcfrontend/commit/15540dbba1c8059c483857722c66fd11517945b1))
* ddtype field for [#231](https://gitlab.com/macropeople/dcfrontend/issues/231) ([90ae7f5](https://gitlab.com/macropeople/dcfrontend/commit/90ae7f5c4abaae55f90257d7f14698fcdea114f9))
* ddtype, [#231](https://gitlab.com/macropeople/dcfrontend/issues/231) ([c4d32b8](https://gitlab.com/macropeople/dcfrontend/commit/c4d32b8059abe89c8f35815fc90d1aa6c12ab8df))
* delete this record ordering ([cacbf46](https://gitlab.com/macropeople/dcfrontend/commit/cacbf4612908bf06d490adc05d971f3bb2fcaf5e))
* deploy db recreate checkbox and UI fixes ([fd6a13a](https://gitlab.com/macropeople/dcfrontend/commit/fd6a13a6e837315beed789155322a0451806a881))
* deploy error handling improved ([e681d9a](https://gitlab.com/macropeople/dcfrontend/commit/e681d9a82b138fcfd8ad2478955ebcf8266ae3c0))
* deploy makedata use original web approach ([dff12bd](https://gitlab.com/macropeople/dcfrontend/commit/dff12bda82ddd37a331245600c978c0605445d6c))
* deploy testing and key generator ([9a0ce35](https://gitlab.com/macropeople/dcfrontend/commit/9a0ce355edc6b2a50f8e39a84f01d78977f81efb))
* disable accept/reject buttons if user is not approver ([8971999](https://gitlab.com/macropeople/dcfrontend/commit/8971999f5c9a788b588a65b606c216385f213127))
* disable deploy button if json file not present ([a93736a](https://gitlab.com/macropeople/dcfrontend/commit/a93736a15cca6507a7a0f726ebc51e3e4e8f536c))
* duplicate keys, removing duplicated indexes ([3c2dccf](https://gitlab.com/macropeople/dcfrontend/commit/3c2dccf4d9283bb19bb83845f003a7b5f5e53e98))
* duplicate primary keys modal improvements ([a68e2d6](https://gitlab.com/macropeople/dcfrontend/commit/a68e2d6334657367c2d75c9e9de18c7a3bfb3226))
* empty object error ([ec90456](https://gitlab.com/macropeople/dcfrontend/commit/ec90456859ce432e775eb83bab2acdc6eb4bae38))
* empty table in EDIT menu not shown ([b2ca2c6](https://gitlab.com/macropeople/dcfrontend/commit/b2ca2c6b09f45a6fc2b88c4e0da5cd5080df2344))
* empty table upload ([4a5204e](https://gitlab.com/macropeople/dcfrontend/commit/4a5204ee0f807e6607286820e11ee436c2403a0b))
* error message when no root folder available ([e3ec4f9](https://gitlab.com/macropeople/dcfrontend/commit/e3ec4f99eed8ae6569cb8967750d79bc239b48a3))
* error when createing data dictionary in viya ([6deb93d](https://gitlab.com/macropeople/dcfrontend/commit/6deb93d2faa5db2f9458d1066d60da3cba2ec400))
* excel time exponential value ([4b16c93](https://gitlab.com/macropeople/dcfrontend/commit/4b16c937305beef553b8bb7269590ed64d9d10ce))
* excel upload with no data rows (js error was triggered, uncaught) ([cdc8784](https://gitlab.com/macropeople/dcfrontend/commit/cdc8784d1886562caa112cec6d2df106bfc7e737))
* exist typo ([2b13d75](https://gitlab.com/macropeople/dcfrontend/commit/2b13d75022c1d402600d8b39888fdf1f4c5adca6))
* extended test on both viya and v9. CI separated viya and v9 test jobs ([d41ff50](https://gitlab.com/macropeople/dcfrontend/commit/d41ff508a7d69b2fa5f59a845293a2a4dd52750e))
* filter operators not in sync with selected variable ([9e714ab](https://gitlab.com/macropeople/dcfrontend/commit/9e714abc1c9bd1f75f31bc17f9ac77d3d389ada6))
* filtering modal datepicker (in progress) ([3f1b4a2](https://gitlab.com/macropeople/dcfrontend/commit/3f1b4a25bf9d95035073fd91df552f09ed34ad93))
* handling of getdata error in response from SAS. Fixed abort modal style. ([e4c8cc6](https://gitlab.com/macropeople/dcfrontend/commit/e4c8cc647886c1609ae88a1b48a1eae8d3fe9b0e))
* handling trailing slash in dcPath ([8808227](https://gitlab.com/macropeople/dcfrontend/commit/880822744407e0a52b690bee28d3d2188017ab5a))
* hans-jeurgen processed issue ([2f4e5da](https://gitlab.com/macropeople/dcfrontend/commit/2f4e5da9b5f1df08b71813a6664b562883cb2a04))
* hot dropdown has no arrow ([b9ead6a](https://gitlab.com/macropeople/dcfrontend/commit/b9ead6af4502a5158f363cf6b23a46db5a82a211))
* if logged out in filter view, cannot log in ([4d682d3](https://gitlab.com/macropeople/dcfrontend/commit/4d682d38f2f2c2cee18d70976c08b9db9142228c))
* improved upload and submit UX ([e6156dc](https://gitlab.com/macropeople/dcfrontend/commit/e6156dc55bb58bb56c0b84338267d827f6fd0f29))
* improved UX, search dropdown of variable ([7a7504b](https://gitlab.com/macropeople/dcfrontend/commit/7a7504bac030f208111f76255eac3a27dc7cef8c))
* keep list ([1527248](https://gitlab.com/macropeople/dcfrontend/commit/1527248167bdb110bbf9f848590972d71fab0250))
* layout issue, in/not in filter checking errors ([d68b342](https://gitlab.com/macropeople/dcfrontend/commit/d68b342fcba3d89c15ff431339f6901e01823319))
* length ([48dfd3f](https://gitlab.com/macropeople/dcfrontend/commit/48dfd3f86c63b18e58fb12e6a6c1b00e063c3732))
* length issue ([37c39cf](https://gitlab.com/macropeople/dcfrontend/commit/37c39cf99c9e82058bcf3d2e37bb8bc5ed6040ad))
* lineage issues ([b4b1680](https://gitlab.com/macropeople/dcfrontend/commit/b4b16801afbe99814732b02c734741ed1826a9a7))
* liveness test requires submitting table to pass ([1b43a63](https://gitlab.com/macropeople/dcfrontend/commit/1b43a6394848000705ba532cf4e3d7cf7ecccfbd))
* makeData error detect ([b977b2a](https://gitlab.com/macropeople/dcfrontend/commit/b977b2a5c2659ab9fcfb6b0dd793571eb5a7b6e0))
* migration for incorrect datetime format ([8aefd1b](https://gitlab.com/macropeople/dcfrontend/commit/8aefd1b74ddfaba21b773259afa9ccf293597dd4))
* missing value ([2c69aad](https://gitlab.com/macropeople/dcfrontend/commit/2c69aad426ae4081cd1b751aa1eda8db474f6a65))
* older versions of edge licensing ([d1eea5c](https://gitlab.com/macropeople/dcfrontend/commit/d1eea5c66e816080fe2ae342838f48594d7cab74))
* optional viya call ([8e1537e](https://gitlab.com/macropeople/dcfrontend/commit/8e1537e24edbcac1795608197987807446b27ec1))
* pages are getting wrong details on pagination ([615af15](https://gitlab.com/macropeople/dcfrontend/commit/615af159bd1f5e1efbfb7514362392e1e26d135e))
* processed_dttm was not dynamic ([04d17eb](https://gitlab.com/macropeople/dcfrontend/commit/04d17eb348bbcb2321bb840fd04f54f0261a5868))
* refreshcatalog ([eae6bfd](https://gitlab.com/macropeople/dcfrontend/commit/eae6bfdfedfde33dd18d9723eb2c1e78ce0665cb))
* removing old package to remove npm warning ([bc99db0](https://gitlab.com/macropeople/dcfrontend/commit/bc99db0bb9b36d276da7cd670ffd814714c1a44c))
* removing tabs ([4742842](https://gitlab.com/macropeople/dcfrontend/commit/47428423cd6746257f3ce1e11a63a68222639b20))
* renegade %end ([a4a74ac](https://gitlab.com/macropeople/dcfrontend/commit/a4a74ac54c1483da4e33d26a5eedab679d844b03))
* replace dependencies with sas macros in header ([3acd92b](https://gitlab.com/macropeople/dcfrontend/commit/3acd92b236055fc936638fc399b1b2dda7e727dd))
* requests modal not working if server error ([cdcd578](https://gitlab.com/macropeople/dcfrontend/commit/cdcd5781f1c5a9a8cd72e167f47d36f848ad38a3))
* return from service ([4d9a8e5](https://gitlab.com/macropeople/dcfrontend/commit/4d9a8e55b08463e82fe343f6b41fae0b4a3291c7))
* rows separator is comma ([060ed48](https://gitlab.com/macropeople/dcfrontend/commit/060ed484a2ab10d9d6039a57030c755aed43ee9b))
* rxjs imports (production build error) ([83d0859](https://gitlab.com/macropeople/dcfrontend/commit/83d085916326aecfeb0c8329397e2af37a5e370a))
* sas double slash, new loading bar ([5fe053e](https://gitlab.com/macropeople/dcfrontend/commit/5fe053ecdcbe6e8487f6fe670d50545dffae03a6))
* sas9 metanav broken ([0503e4b](https://gitlab.com/macropeople/dcfrontend/commit/0503e4b19bd15e3f4f20a2b442576de37238624b))
* sas9 startupservice not executed ([0914007](https://gitlab.com/macropeople/dcfrontend/commit/09140074f9564b894878fce5f47c89544fd21d58))
* stage component error handling ([2d9f417](https://gitlab.com/macropeople/dcfrontend/commit/2d9f417913cdf14593292a267f264caadb36036f))
* startupservice failure handling ([d8c3380](https://gitlab.com/macropeople/dcfrontend/commit/d8c3380b861386c1b5357d40ade549fdb4b0756b))
* startupservice sasjsAbort catch ([9f30bbe](https://gitlab.com/macropeople/dcfrontend/commit/9f30bbe7b3c4b5d73908fa0329f37f4b0a5230c7))
* **licensing:** registerkey sasjsAbort fix ([a615e12](https://gitlab.com/macropeople/dcfrontend/commit/a615e12a31a293a36600912623e328b1fe244559))
* **tests:** licensing test fixed unstable behavior ([011fcb6](https://gitlab.com/macropeople/dcfrontend/commit/011fcb662c92a7ee8a8ecf09c540d45e95432785))
* **tests:** licensing tests shifted all to quickKey update ([6cc0128](https://gitlab.com/macropeople/dcfrontend/commit/6cc01281d6f5b957904a5e341be2eb2d88c2bbdb))
* Subject not constructor error ([e22afc7](https://gitlab.com/macropeople/dcfrontend/commit/e22afc72ac190f28385d709a062a7d98be34103b))
* surrounded data excel upload ([612729b](https://gitlab.com/macropeople/dcfrontend/commit/612729b0467c76371de3cd1784af275ef935adbb))
* switch to new SAS 9 folder ([41ee6c4](https://gitlab.com/macropeople/dcfrontend/commit/41ee6c43f56835f18b72bfef6776e49acce200be))
* syssite added to licensing page ([009618e](https://gitlab.com/macropeople/dcfrontend/commit/009618e47d6e6a7161bfecfd23e7fb2566d011f0))
* tests fail when switching between viya and 9 ([c315d40](https://gitlab.com/macropeople/dcfrontend/commit/c315d40890cbf21a3fe6476b548048ac9c23d6cb))
* typos and length checks ([69beff1](https://gitlab.com/macropeople/dcfrontend/commit/69beff1edd49e51d00cb193c35f460bc9b906425))
* unable to VIEW tables in der touristik due to lowercase librefs ([945d5ac](https://gitlab.com/macropeople/dcfrontend/commit/945d5ac77e1afad51e139e70f4c315a005124eea))
* updates to urls to support /services/ in program path ([c978f25](https://gitlab.com/macropeople/dcfrontend/commit/c978f25a29b32360083426d492cd2ee30f1f4595))
* updating sas9 config ([54ba22d](https://gitlab.com/macropeople/dcfrontend/commit/54ba22dee71947d372a0c936f7dca8b4124b6f4b))
* uploading video from ci tests ([14868e0](https://gitlab.com/macropeople/dcfrontend/commit/14868e0511606ef39f95ea3c4b861a433ce6f804))
* user menu dropdown was not closing when you click on hot table area. ([000e39e](https://gitlab.com/macropeople/dcfrontend/commit/000e39ea94388e53dbf971db86dcb8fa244df368))
* user nav style fix ([2a6af21](https://gitlab.com/macropeople/dcfrontend/commit/2a6af214435c905217d4188123cad08a96df5992))
* usernav issues ([b4bc47d](https://gitlab.com/macropeople/dcfrontend/commit/b4bc47d31b043acbb4f7a4bd4e675d7f38d11f62))
* using more robust functions for encoding and decoding base64 ([899ce20](https://gitlab.com/macropeople/dcfrontend/commit/899ce2007c748691271c75f326c5da8161351df5))
* var_active addition to mpe_config, moving redshift setup there ([35fc30e](https://gitlab.com/macropeople/dcfrontend/commit/35fc30e0a4a65b9c5e5ab12de4bccc1b50d00ef4))
* viewer if table eixsts undefined ([cbc8cc8](https://gitlab.com/macropeople/dcfrontend/commit/cbc8cc8281cf978722c3213c5138f99a6efae872))
* viewer library collapse after choosing a table ([84c6cd6](https://gitlab.com/macropeople/dcfrontend/commit/84c6cd65e9dddeec5a807bfc36b55cbbc40b3636))
* viewer rendering offset is to small ([52985fb](https://gitlab.com/macropeople/dcfrontend/commit/52985fbde43bff97d0d60649bfef02b0b3434831))
* viewer table search abort modal ([b6388da](https://gitlab.com/macropeople/dcfrontend/commit/b6388da53cce3b3e2fa5267610a4992ea49e5ab8))
* viewing non lineage tables broken in sas9 ([fee51ed](https://gitlab.com/macropeople/dcfrontend/commit/fee51edcf3167c0649b645cd77bfb772a1b36abc))
* warning ([0925733](https://gitlab.com/macropeople/dcfrontend/commit/0925733139d417fec36470475486c5af4737f98f))
* web streaming & deployment for SAS 9 ([3a36280](https://gitlab.com/macropeople/dcfrontend/commit/3a362800cbf17e65fb5da368641a668b4473f086))
* whereClause display wrap/styling ([cca4c9e](https://gitlab.com/macropeople/dcfrontend/commit/cca4c9e2d0126b5ad98ef1704bd8c94a98a833b9))
* wording of EULA ([64d9f1c](https://gitlab.com/macropeople/dcfrontend/commit/64d9f1cbb6c5880da332caf8bd4ef4e380262585))
* **service-paths:** use relative paths without leading slash ([bf0b7c1](https://gitlab.com/macropeople/dcfrontend/commit/bf0b7c115f071255d704b060256914d75d1703ba))
* **updated:** angular 10, clarity 3, optimized ([5031ed2](https://gitlab.com/macropeople/dcfrontend/commit/5031ed210a3e493f2652db5cb90e0b396923c38d))

## [3.10.0](https://gitlab.com/macropeople/dcfrontend/compare/v3.9.0...v3.10.0) (2020-08-18)


### Features

* abort modal complete refactor, removing duplicated code for abort, error handling improved ([3175654](https://gitlab.com/macropeople/dcfrontend/commit/317565491339c5697de2fdce65796920905d711a))
* adapter compute api integration ([22dad29](https://gitlab.com/macropeople/dcfrontend/commit/22dad297d92ea2e28c2d28db8b721f350058ca8c))
* added typedoc ([12480c4](https://gitlab.com/macropeople/dcfrontend/commit/12480c410a1dc7a852a468973119a9aa292f53b4))
* file upload using adapter function ([eb0dea9](https://gitlab.com/macropeople/dcfrontend/commit/eb0dea9fe509439899f2de2c144ddedd56cb88b3))
* handsontable v8, removed edit icons - contextMenu edit button ([1092e5c](https://gitlab.com/macropeople/dcfrontend/commit/1092e5c51fd50a050b1a5adcf285c5693a14d8b3))
* jobs error catching ([6c01e03](https://gitlab.com/macropeople/dcfrontend/commit/6c01e033d3c2d788308b545b438450d8da356d87))
* odd rows different shade ([ed336da](https://gitlab.com/macropeople/dcfrontend/commit/ed336daeaf7459637b321b1c9c4e83921f96720e))
* pgsql download ([a617d17](https://gitlab.com/macropeople/dcfrontend/commit/a617d1742e4d942f8245eca07b71285f8432a0a9))
* sasjsAbort uses new abort-modal component ([2bc300b](https://gitlab.com/macropeople/dcfrontend/commit/2bc300b621438b73a5401ee9c2696f263c180047))
* started catching of adapter response error, showing full log in abort modal ([e781703](https://gitlab.com/macropeople/dcfrontend/commit/e78170324df8573c296463dcb7ddfc9e9df36e3b))
* viewer lineage linking ([911d0c7](https://gitlab.com/macropeople/dcfrontend/commit/911d0c711481f56af483b82683b62e4bec3b5497))


### Bug Fixes

* abort modal added syserrortext syswarningtext ([6d6ccaf](https://gitlab.com/macropeople/dcfrontend/commit/6d6ccafe1695f0fe09164c7f701e45658b28ccf2))
* abort modal customizable title ([c222f55](https://gitlab.com/macropeople/dcfrontend/commit/c222f55cb4f885824f5310d5510e4474acf89981))
* abort modal styling ([e753d9a](https://gitlab.com/macropeople/dcfrontend/commit/e753d9af0cff0de6ad3972ee414458cfbc60b087))
* abort modal warning and error text style ([abaaff8](https://gitlab.com/macropeople/dcfrontend/commit/abaaff8df856a643a87692afe375c7f4fc70ff77))
* abot leftovers in htmls ([8de9713](https://gitlab.com/macropeople/dcfrontend/commit/8de971329894d6e3ed74ebc97b437a957e750d47))
* add record button, modal loading spinner ([8527df9](https://gitlab.com/macropeople/dcfrontend/commit/8527df9608fbc9c444bf8cce6e0ac5f13cda33ee))
* adding uri to enable lineage linking ([2b12f8c](https://gitlab.com/macropeople/dcfrontend/commit/2b12f8c559a3ee163e5890e801afedc128be3d9b))
* appLoc checking ([7141efd](https://gitlab.com/macropeople/dcfrontend/commit/7141efd683c8a067063db3bfb5041b61d04a2105))
* appLocCheck for viya only ([620b94d](https://gitlab.com/macropeople/dcfrontend/commit/620b94dcedd076e2225f88e4df122d324dc5fbb9))
* approve page vlaues overlapping ([b1d182d](https://gitlab.com/macropeople/dcfrontend/commit/b1d182d8b1109bdbe27f243ece35ab2a81485607))
* approvers casing issue ([d4d1112](https://gitlab.com/macropeople/dcfrontend/commit/d4d1112d9480a7a47ba2f25bfea3ef1a19546de9))
* autoselect/autodocus ([a93bf8d](https://gitlab.com/macropeople/dcfrontend/commit/a93bf8dbd274265edaba48ef30fb2830d7064a65))
* bump sasjs/core ([5617ced](https://gitlab.com/macropeople/dcfrontend/commit/5617cedd76a6a24b935b9c4e035ceffd88f6fea6))
* bumping core ([f5fa141](https://gitlab.com/macropeople/dcfrontend/commit/f5fa141efba99088d8ee938e7f4d612b4a287965))
* contextMenu enabled only on edit ([e8b4d30](https://gitlab.com/macropeople/dcfrontend/commit/e8b4d309fb7667920004cc2b68075d375edaff46))
* data creation refactor ([fb0e343](https://gitlab.com/macropeople/dcfrontend/commit/fb0e343c345fc4cbb23990bed976f20db651c1e6))
* demo mode add record button is off screen ([993b1a6](https://gitlab.com/macropeople/dcfrontend/commit/993b1a6c898f83f11fa42c69eafeb5276610c26e))
* deploy compute api fixes ([2b6e989](https://gitlab.com/macropeople/dcfrontend/commit/2b6e98995f7a02e693580f193ee3e66c01163859))
* duplicate key pipe ([0116304](https://gitlab.com/macropeople/dcfrontend/commit/0116304aa46cdec4ee1b831968562e5a2f670c2d))
* edit after invalid submit not applied ([be350d9](https://gitlab.com/macropeople/dcfrontend/commit/be350d9d21ee2180f95ace03e86a21adc55a1557))
* edit from viewer, submit error ([e0f2d56](https://gitlab.com/macropeople/dcfrontend/commit/e0f2d567245cfb6b9722e0cc4ba24fbcf7dca3ae))
* enabling makedata to run both as url and request format ([5989992](https://gitlab.com/macropeople/dcfrontend/commit/59899921a8279ef2fedf24b5f9a20e9f4a399c91))
* excel submit, UX fixes ([5de9c1d](https://gitlab.com/macropeople/dcfrontend/commit/5de9c1d105fc6a4540c5a33400a77a79c78fe6f0))
* home component cleanup ([9416f75](https://gitlab.com/macropeople/dcfrontend/commit/9416f75d85a1a384ec2eab8a90647b3ef839c931))
* lineage download full graph ([4d8d10b](https://gitlab.com/macropeople/dcfrontend/commit/4d8d10b99321b6de5e41a4371bffb38e6a6a1d0e))
* loading spinner on submit ([6adc797](https://gitlab.com/macropeople/dcfrontend/commit/6adc79795d3a155e2e0013d55db7a3ac946bb05b))
* makedata seperation ([32f9cd6](https://gitlab.com/macropeople/dcfrontend/commit/32f9cd6826c3ca87c40b31e3456d848eae24620f))
* missing dependency ([f5debd8](https://gitlab.com/macropeople/dcfrontend/commit/f5debd86ba67328f232486977fe5aa8ae537c69d))
* model updates and build environment updates ([528ed9c](https://gitlab.com/macropeople/dcfrontend/commit/528ed9ce19830300c49d4ca6fc9bb662b6a282dc))
* no editable tables configured ([7e344df](https://gitlab.com/macropeople/dcfrontend/commit/7e344df7f6a3ab52bca9934c975f1da75e845a3e))
* options button styling, sasjs adapter & core bumps, excel export fix ([2752c5d](https://gitlab.com/macropeople/dcfrontend/commit/2752c5da44a7d994a4099177c768c92562b8e25d))
* record modal linking UX ([94981cf](https://gitlab.com/macropeople/dcfrontend/commit/94981cf2abc1541c017aea0192ca1a5ff87745a8))
* Remove the auto-hide of the sidebar above a certain screen size ([d841ec6](https://gitlab.com/macropeople/dcfrontend/commit/d841ec6839d44e34014252fb9ca605d79e8ec88d))
* table sizing when no demo banner present ([e00533d](https://gitlab.com/macropeople/dcfrontend/commit/e00533d35f9dca2a9ecdb13eade7467245f52f56))
* to avoid circular reference ([c715aed](https://gitlab.com/macropeople/dcfrontend/commit/c715aedecf7e1326ed65260bcf7cc363e7d54125))
* uploader multiple files ([35e6cfd](https://gitlab.com/macropeople/dcfrontend/commit/35e6cfd84fb96bbdc919268495af00eb20160ba3))
* viewer options icon gap ([2d7199b](https://gitlab.com/macropeople/dcfrontend/commit/2d7199b82a63b82e04a9e0e7e2dc7427337a6d21))
* web query url ([ed85e0b](https://gitlab.com/macropeople/dcfrontend/commit/ed85e0ba06f5dd89a4ffbb3e31d7036fcfaa5fca))

<a name="3.9.0"></a>
# [3.9.0](https://gitlab.com/macropeople/dcfrontend/compare/v3.8.0...v3.9.0) (2020-07-14)


### Bug Fixes

*  deployServicePack removed ([01e3473](https://gitlab.com/macropeople/dcfrontend/commit/01e3473))
* _program conflict ([50dfa2b](https://gitlab.com/macropeople/dcfrontend/commit/50dfa2b))
* adapting to sasjs, removed token functions ([706eb3a](https://gitlab.com/macropeople/dcfrontend/commit/706eb3a))
* adding maxlen ([a1a36e4](https://gitlab.com/macropeople/dcfrontend/commit/a1a36e4))
* adding seperator ([d262685](https://gitlab.com/macropeople/dcfrontend/commit/d262685))
* autodeploy infos p instead of input ([fab1294](https://gitlab.com/macropeople/dcfrontend/commit/fab1294))
* bump macrocore, default value for _program ([f3d3e5e](https://gitlab.com/macropeople/dcfrontend/commit/f3d3e5e))
* bump MC and enable public services ([5319005](https://gitlab.com/macropeople/dcfrontend/commit/5319005))
* bump sasjs ([582c9e3](https://gitlab.com/macropeople/dcfrontend/commit/582c9e3))
* bump sasjs ([4b5a167](https://gitlab.com/macropeople/dcfrontend/commit/4b5a167))
* bumping sasjs and missing puri in lineage generator ([5f25bd6](https://gitlab.com/macropeople/dcfrontend/commit/5f25bd6))
* clicking icon in viewer menu does not expand menu ([3941635](https://gitlab.com/macropeople/dcfrontend/commit/3941635))
* client token removal ([af14883](https://gitlab.com/macropeople/dcfrontend/commit/af14883))
* data model update ([13a53f1](https://gitlab.com/macropeople/dcfrontend/commit/13a53f1))
* dcpath in deploy ([a157f05](https://gitlab.com/macropeople/dcfrontend/commit/a157f05))
* demo expired styling ([61336b0](https://gitlab.com/macropeople/dcfrontend/commit/61336b0))
* deploy and token imporvements ([9d05839](https://gitlab.com/macropeople/dcfrontend/commit/9d05839))
* deploy buttons position ([b06ad5d](https://gitlab.com/macropeople/dcfrontend/commit/b06ad5d))
* deploy defaults set ([a074787](https://gitlab.com/macropeople/dcfrontend/commit/a074787))
* deploy navigate to home ([17ed9b0](https://gitlab.com/macropeople/dcfrontend/commit/17ed9b0))
* deploy page fixes ([3f7419f](https://gitlab.com/macropeople/dcfrontend/commit/3f7419f))
* deploy removed client and secret, debug was not switched on ([09475f2](https://gitlab.com/macropeople/dcfrontend/commit/09475f2))
* deploy updates ([43083b4](https://gitlab.com/macropeople/dcfrontend/commit/43083b4))
* dynamic build of getviyaclient ([21e7b9c](https://gitlab.com/macropeople/dcfrontend/commit/21e7b9c))
* edit table styling issue ([d408a60](https://gitlab.com/macropeople/dcfrontend/commit/d408a60))
* editor title link ([a7edd3c](https://gitlab.com/macropeople/dcfrontend/commit/a7edd3c))
* expired token shows login page ([d1ad578](https://gitlab.com/macropeople/dcfrontend/commit/d1ad578))
* filter issues ([ccf9ea8](https://gitlab.com/macropeople/dcfrontend/commit/ccf9ea8))
* forbid unchanged submit ([0ec1c52](https://gitlab.com/macropeople/dcfrontend/commit/0ec1c52))
* get all contexts, deploy ([dae7ee1](https://gitlab.com/macropeople/dcfrontend/commit/dae7ee1))
* get executable contexts on deploy page ([d51c0d0](https://gitlab.com/macropeople/dcfrontend/commit/d51c0d0))
* header invisible when demo banner removed ([7216154](https://gitlab.com/macropeople/dcfrontend/commit/7216154))
* hide demo banner when license key present ([634e14e](https://gitlab.com/macropeople/dcfrontend/commit/634e14e))
* hitting accept before diff load causes error ([dbc9fbd](https://gitlab.com/macropeople/dcfrontend/commit/dbc9fbd))
* jobs type in idlookup ([3a138c6](https://gitlab.com/macropeople/dcfrontend/commit/3a138c6))
* lineage column level linking ([bf04597](https://gitlab.com/macropeople/dcfrontend/commit/bf04597))
* lineage is depending only on 'fetchlineage' service, not any other ([6631fd9](https://gitlab.com/macropeople/dcfrontend/commit/6631fd9))
* lineage libraryid of undefined, cases covered ([f44b75a](https://gitlab.com/macropeople/dcfrontend/commit/f44b75a))
* lineage multiple requests ([45350a0](https://gitlab.com/macropeople/dcfrontend/commit/45350a0))
* lineage table url access fix ([ffb2477](https://gitlab.com/macropeople/dcfrontend/commit/ffb2477))
* macro invoke ([f9b91dc](https://gitlab.com/macropeople/dcfrontend/commit/f9b91dc))
* macrocore ([50bd41b](https://gitlab.com/macropeople/dcfrontend/commit/50bd41b))
* macrocore bump ([778a891](https://gitlab.com/macropeople/dcfrontend/commit/778a891))
* makedata ([773a62b](https://gitlab.com/macropeople/dcfrontend/commit/773a62b))
* makedata updates ([2a6d6ae](https://gitlab.com/macropeople/dcfrontend/commit/2a6d6ae))
* metadata name from response ([642428c](https://gitlab.com/macropeople/dcfrontend/commit/642428c))
* metadata object name when linking ([62b6b6c](https://gitlab.com/macropeople/dcfrontend/commit/62b6b6c))
* metadata objectName param ignore ([56cfa9e](https://gitlab.com/macropeople/dcfrontend/commit/56cfa9e))
* migration for mpe_datadictionary ([b6a5edc](https://gitlab.com/macropeople/dcfrontend/commit/b6a5edc))
* more rendered rows, undefined celValidation ([43111bb](https://gitlab.com/macropeople/dcfrontend/commit/43111bb))
* pk duplicate check, removed cellValidtion on frontend ([6871191](https://gitlab.com/macropeople/dcfrontend/commit/6871191))
* pk dups check on first submit ([6e41613](https://gitlab.com/macropeople/dcfrontend/commit/6e41613))
* pk issue ([58e727b](https://gitlab.com/macropeople/dcfrontend/commit/58e727b))
* predefined colWidths ([98bb6e6](https://gitlab.com/macropeople/dcfrontend/commit/98bb6e6))
* refresh service ([6d1d489](https://gitlab.com/macropeople/dcfrontend/commit/6d1d489))
* removing 'error:' string ([049d04a](https://gitlab.com/macropeople/dcfrontend/commit/049d04a))
* removing access tokens ([9031e9a](https://gitlab.com/macropeople/dcfrontend/commit/9031e9a))
* removing caslib assignment ([53396bb](https://gitlab.com/macropeople/dcfrontend/commit/53396bb))
* removing session table ([e50c996](https://gitlab.com/macropeople/dcfrontend/commit/e50c996))
* search data bug in viya ([3c54cbc](https://gitlab.com/macropeople/dcfrontend/commit/3c54cbc))
* siemens issues (lineage, exporter) ([7eed379](https://gitlab.com/macropeople/dcfrontend/commit/7eed379))
* sorting is lost after clicking edit/cancel edit ([65ef697](https://gitlab.com/macropeople/dcfrontend/commit/65ef697))
* startupservice ([367a194](https://gitlab.com/macropeople/dcfrontend/commit/367a194))
* startupservice abort in console log ([9a5e10f](https://gitlab.com/macropeople/dcfrontend/commit/9a5e10f))
* styling and manual deploy ([d683a70](https://gitlab.com/macropeople/dcfrontend/commit/d683a70))
* styling, admin groups limit and sort ([6c3bcad](https://gitlab.com/macropeople/dcfrontend/commit/6c3bcad))
* submit after error ([8949021](https://gitlab.com/macropeople/dcfrontend/commit/8949021))
* submit without changes modal ([04998e3](https://gitlab.com/macropeople/dcfrontend/commit/04998e3))
* syscc to 0 on startup ([145866a](https://gitlab.com/macropeople/dcfrontend/commit/145866a))
* table update ([69a6e70](https://gitlab.com/macropeople/dcfrontend/commit/69a6e70))
* test ([bb01cd9](https://gitlab.com/macropeople/dcfrontend/commit/bb01cd9))
* token expiration ([1e5fb39](https://gitlab.com/macropeople/dcfrontend/commit/1e5fb39))
* tokenauth management ([20ae61d](https://gitlab.com/macropeople/dcfrontend/commit/20ae61d))
* unable to edit and dropdown blank ([06f4aae](https://gitlab.com/macropeople/dcfrontend/commit/06f4aae))
* validate primary keys, editor performance ([aa66cbc](https://gitlab.com/macropeople/dcfrontend/commit/aa66cbc))
* viya auth rebuild ([f74a4da](https://gitlab.com/macropeople/dcfrontend/commit/f74a4da))
* viya fixes ([24c6603](https://gitlab.com/macropeople/dcfrontend/commit/24c6603))
* viya token global ([4b597f1](https://gitlab.com/macropeople/dcfrontend/commit/4b597f1))
* viya usernav with RESTApis ([7e12bd5](https://gitlab.com/macropeople/dcfrontend/commit/7e12bd5))
* viyainstall ([3384ee8](https://gitlab.com/macropeople/dcfrontend/commit/3384ee8))


### Features

* authentication process update ([045d6a2](https://gitlab.com/macropeople/dcfrontend/commit/045d6a2))
* auto deploy for viya, appLoc exist check ([c4466b3](https://gitlab.com/macropeople/dcfrontend/commit/c4466b3))
* auto deploy if startupservice not present ([635e58a](https://gitlab.com/macropeople/dcfrontend/commit/635e58a))
* auto include sasbuild/viya.json from dist folder ([ea5f5c0](https://gitlab.com/macropeople/dcfrontend/commit/ea5f5c0))
* dcdeploy, creating database ([f17919d](https://gitlab.com/macropeople/dcfrontend/commit/f17919d))
* deploy page collapsable and improvements ([e6fc907](https://gitlab.com/macropeople/dcfrontend/commit/e6fc907))
* deploy page json upload ([4bca65d](https://gitlab.com/macropeople/dcfrontend/commit/4bca65d))
* deploy page with presistent keys ([078ff27](https://gitlab.com/macropeople/dcfrontend/commit/078ff27))
* deployEnabled switch, removed buttons on deploy page ([36f2e5f](https://gitlab.com/macropeople/dcfrontend/commit/36f2e5f))
* file uplaod with token, synced with formdata token ([e67a841](https://gitlab.com/macropeople/dcfrontend/commit/e67a841))
* lineage svg linking ([e64c840](https://gitlab.com/macropeople/dcfrontend/commit/e64c840))
* usernav shift to RESTApis ([b70091e](https://gitlab.com/macropeople/dcfrontend/commit/b70091e))
* validate rows, pk duplicates in modal ([98ad374](https://gitlab.com/macropeople/dcfrontend/commit/98ad374))
* viya deploy page ([8f058d1](https://gitlab.com/macropeople/dcfrontend/commit/8f058d1))
* viya deploy without client & access_token ([f8c3bc5](https://gitlab.com/macropeople/dcfrontend/commit/f8c3bc5))
* viya including access token ([2d4a91f](https://gitlab.com/macropeople/dcfrontend/commit/2d4a91f))



<a name="3.8.0"></a>
# [3.8.0](https://gitlab.com/macropeople/dcfrontend/compare/v3.7.0...v3.8.0) (2020-05-31)


### Bug Fixes

* add record url rewrite ([8ab80e8](https://gitlab.com/macropeople/dcfrontend/commit/8ab80e8))
* finishing up TableLineage ([ff600aa](https://gitlab.com/macropeople/dcfrontend/commit/ff600aa))
* table lineage and post service macro append ([cee486e](https://gitlab.com/macropeople/dcfrontend/commit/cee486e))
* **lineage table:** libref and table name ([f87cf6b](https://gitlab.com/macropeople/dcfrontend/commit/f87cf6b))
* **table lineage:** csv download, table name ([e4580be](https://gitlab.com/macropeople/dcfrontend/commit/e4580be))
* metadataRoot to appLoc ([d437a6f](https://gitlab.com/macropeople/dcfrontend/commit/d437a6f))
* moment import ([718df90](https://gitlab.com/macropeople/dcfrontend/commit/718df90))
* using sasjs vars in viya build ([bd790b6](https://gitlab.com/macropeople/dcfrontend/commit/bd790b6))


### Features

* varchar support in Viya ([c954f65](https://gitlab.com/macropeople/dcfrontend/commit/c954f65))
* **lineage table:** title link to viewer ([2726d09](https://gitlab.com/macropeople/dcfrontend/commit/2726d09))
* table level lineage ([54f7731](https://gitlab.com/macropeople/dcfrontend/commit/54f7731))
* Table Level Lineage ([01256a9](https://gitlab.com/macropeople/dcfrontend/commit/01256a9))



<a name="3.7.0"></a>
# [3.7.0](https://gitlab.com/macropeople/dcfrontend/compare/v3.6.0...v3.7.0) (2020-05-22)


### Bug Fixes

* ensuring _debug is always present ([299a78d](https://gitlab.com/macropeople/dcfrontend/commit/299a78d))
* file upload debug param, staged data download package button ([3b62a5c](https://gitlab.com/macropeople/dcfrontend/commit/3b62a5c))
* secret path ([c628553](https://gitlab.com/macropeople/dcfrontend/commit/c628553))


### Features

* download lineage as png ([0f2af40](https://gitlab.com/macropeople/dcfrontend/commit/0f2af40))



<a name="3.6.0"></a>
# [3.6.0](https://gitlab.com/macropeople/dcfrontend/compare/v3.5.0...v3.6.0) (2020-05-20)


### Bug Fixes

* disable filter when editing table ([d411f9a](https://gitlab.com/macropeople/dcfrontend/commit/d411f9a))


### Features

* edit number of rows ([f82e15c](https://gitlab.com/macropeople/dcfrontend/commit/f82e15c))
* submit only modified rows, redirect to staged data ([b3846b6](https://gitlab.com/macropeople/dcfrontend/commit/b3846b6))
* submit statistics ([ac8e97b](https://gitlab.com/macropeople/dcfrontend/commit/ac8e97b))



<a name="3.5.0"></a>
# [3.5.0](https://gitlab.com/macropeople/dcfrontend/compare/v3.4.0...v3.5.0) (2020-05-20)


### Bug Fixes

* adding cols info to getdata services ([211e943](https://gitlab.com/macropeople/dcfrontend/commit/211e943))
* bumping sasjs version ([30aa4b2](https://gitlab.com/macropeople/dcfrontend/commit/30aa4b2))
* edit button on table viewer ([7fbf306](https://gitlab.com/macropeople/dcfrontend/commit/7fbf306))
* editor navigation highlight, saving space in viewer and editor ([31448a5](https://gitlab.com/macropeople/dcfrontend/commit/31448a5))
* file upload with token ([4c0e18d](https://gitlab.com/macropeople/dcfrontend/commit/4c0e18d))
* filter datetime dropdown to show raw value ([6a3008e](https://gitlab.com/macropeople/dcfrontend/commit/6a3008e))
* filter values dropdown ([cd1ae54](https://gitlab.com/macropeople/dcfrontend/commit/cd1ae54))
* hot filtering issues, submitting only filtered rows ([159bafc](https://gitlab.com/macropeople/dcfrontend/commit/159bafc))
* IE search styling ([b2d93ef](https://gitlab.com/macropeople/dcfrontend/commit/b2d93ef))
* loop in viewer libraries ([313495e](https://gitlab.com/macropeople/dcfrontend/commit/313495e))
* options dropdown position ([2ae7402](https://gitlab.com/macropeople/dcfrontend/commit/2ae7402))
* perms issues ([4953fae](https://gitlab.com/macropeople/dcfrontend/commit/4953fae))
* removing bottom bar, saving space ([bd9c35c](https://gitlab.com/macropeople/dcfrontend/commit/bd9c35c))
* request modal linking ([a3f581c](https://gitlab.com/macropeople/dcfrontend/commit/a3f581c))
* SCD2 now works with OR filter ops ([6745cb1](https://gitlab.com/macropeople/dcfrontend/commit/6745cb1))
* schemas in catalog, metarepo in metanav ([43d0109](https://gitlab.com/macropeople/dcfrontend/commit/43d0109))
* usernav version bar ([34c6ebb](https://gitlab.com/macropeople/dcfrontend/commit/34c6ebb))
* using existing cols data for filter ([3c6ba97](https://gitlab.com/macropeople/dcfrontend/commit/3c6ba97))
* viya group title ([d360ec1](https://gitlab.com/macropeople/dcfrontend/commit/d360ec1))
* viya loadfile updates ([a25d36a](https://gitlab.com/macropeople/dcfrontend/commit/a25d36a))
* web query url duplication and spaces ([c27fe3c](https://gitlab.com/macropeople/dcfrontend/commit/c27fe3c))


### Features

* ability to exclude certain libs from the catalog refresh.  Also a fix to the lineage process. ([f2e95ce](https://gitlab.com/macropeople/dcfrontend/commit/f2e95ce))
* datastatus_libs ([528b84b](https://gitlab.com/macropeople/dcfrontend/commit/528b84b))
* downlaod button on submiter and approval screen ([cfbba5a](https://gitlab.com/macropeople/dcfrontend/commit/cfbba5a))
* meta nav repository presistance ([ad46c61](https://gitlab.com/macropeople/dcfrontend/commit/ad46c61))
* rows to submit ([dc918dd](https://gitlab.com/macropeople/dcfrontend/commit/dc918dd))
* selection on view presisted ([962ceba](https://gitlab.com/macropeople/dcfrontend/commit/962ceba))
* sql server data dictionary upates (plus removal of error lines) ([d8e8586](https://gitlab.com/macropeople/dcfrontend/commit/d8e8586))
* sql server integration to dictionary tables ([8610d7a](https://gitlab.com/macropeople/dcfrontend/commit/8610d7a))
* writeback data in Viya ([da3d0ee](https://gitlab.com/macropeople/dcfrontend/commit/da3d0ee))



<a name="3.4.0"></a>
# [3.4.0](https://gitlab.com/macropeople/dcfrontend/compare/v3.3.1...v3.4.0) (2020-05-03)


### Features

* data catalog for libraries, tables and columns ([1865eb1](https://gitlab.com/macropeople/dcfrontend/commit/1865eb1))
* filter values editable ([ffa1065](https://gitlab.com/macropeople/dcfrontend/commit/ffa1065))
* viewer number of rows ([37fbf3d](https://gitlab.com/macropeople/dcfrontend/commit/37fbf3d))
* viewer search table ([ae65969](https://gitlab.com/macropeople/dcfrontend/commit/ae65969))



<a name="3.3.1"></a>
## [3.3.1](https://gitlab.com/macropeople/dcfrontend/compare/v3.3.0...v3.3.1) (2020-04-27)


### Bug Fixes

* ddl download ([ece1e79](https://gitlab.com/macropeople/dcfrontend/commit/ece1e79))
* submit bug, stage page license key ([388c067](https://gitlab.com/macropeople/dcfrontend/commit/388c067))



<a name="3.3.0"></a>
# [3.3.0](https://gitlab.com/macropeople/dcfrontend/compare/v3.2.0...v3.3.0) (2020-04-26)


### Bug Fixes

* backend changes to support frontend fixes ([2e7b6ef](https://gitlab.com/macropeople/dcfrontend/commit/2e7b6ef))
* dqdata update ([b8db8bf](https://gitlab.com/macropeople/dcfrontend/commit/b8db8bf))
* invalid values on submit, notnull validation ([4ac8088](https://gitlab.com/macropeople/dcfrontend/commit/4ac8088))
* parsing errors and warning improved, performance fix, icons added ([e2b8a24](https://gitlab.com/macropeople/dcfrontend/commit/e2b8a24))
* remove syscc=4 in approvals ([af24fd3](https://gitlab.com/macropeople/dcfrontend/commit/af24fd3))
* switch to mp_streamfile ([8cb4529](https://gitlab.com/macropeople/dcfrontend/commit/8cb4529))
* warning not linkable ([9491ccc](https://gitlab.com/macropeople/dcfrontend/commit/9491ccc))


### Features

* request modal error and warning linking ([ebcf2f4](https://gitlab.com/macropeople/dcfrontend/commit/ebcf2f4))



<a name="3.2.0"></a>
# [3.2.0](https://gitlab.com/macropeople/dcfrontend/compare/v3.1.0...v3.2.0) (2020-04-23)


### Bug Fixes

*  get username from  MF_GETUSER ([10bc04f](https://gitlab.com/macropeople/dcfrontend/commit/10bc04f))
* double scroll, add record button, add row scroll to bottom ([75fd508](https://gitlab.com/macropeople/dcfrontend/commit/75fd508))
* dropdowns ([4247114](https://gitlab.com/macropeople/dcfrontend/commit/4247114))
* editor page not working ([c7ae36f](https://gitlab.com/macropeople/dcfrontend/commit/c7ae36f))
* record modal not opening, paste trim trailings, 'edit' button performance ([26da7f5](https://gitlab.com/macropeople/dcfrontend/commit/26da7f5))
* WIP dialouge not opening for new row after cancel and reopen ([ad37092](https://gitlab.com/macropeople/dcfrontend/commit/ad37092))


### Features

* dqdata update ([ccbec0c](https://gitlab.com/macropeople/dcfrontend/commit/ccbec0c))
* exclude data lineage and metanav if serverType is viya ([56c0459](https://gitlab.com/macropeople/dcfrontend/commit/56c0459))
* metavav auto open on object url navigation ([ce49d75](https://gitlab.com/macropeople/dcfrontend/commit/ce49d75))
* usernav functionality for VIYA ([63fa9ae](https://gitlab.com/macropeople/dcfrontend/commit/63fa9ae))



<a name="3.1.0"></a>
# [3.1.0](https://gitlab.com/macropeople/dcfrontend/compare/v3.0.0...v3.1.0) (2020-04-19)


### Bug Fixes

*  sas path in url dynamically ([241911d](https://gitlab.com/macropeople/dcfrontend/commit/241911d))
* broken url ([397857b](https://gitlab.com/macropeople/dcfrontend/commit/397857b))
* char input length check ([69efa6e](https://gitlab.com/macropeople/dcfrontend/commit/69efa6e))
* incorrent url for download and upload ([2638d58](https://gitlab.com/macropeople/dcfrontend/commit/2638d58))
* link to this record ([970f952](https://gitlab.com/macropeople/dcfrontend/commit/970f952))
* metanav services and utf-8 issue ([3820e78](https://gitlab.com/macropeople/dcfrontend/commit/3820e78))
* modal upcase validation, copied to clipboard, number converted to char ([9bdcf50](https://gitlab.com/macropeople/dcfrontend/commit/9bdcf50))
* record modal original column order ([2595911](https://gitlab.com/macropeople/dcfrontend/commit/2595911))
* submit modal, record modal improving ([ec1eab2](https://gitlab.com/macropeople/dcfrontend/commit/ec1eab2))
* **excel-upload:** add polyfill for DateTimeFormat.formatToParts ([22ce409](https://gitlab.com/macropeople/dcfrontend/commit/22ce409))
* **sasjs:** upgrade adapter version to fix IE ([517fff5](https://gitlab.com/macropeople/dcfrontend/commit/517fff5))
* **url:** remove hardcoded path ([7071186](https://gitlab.com/macropeople/dcfrontend/commit/7071186))
* wrong navigation from approve/submit to view table ([d144a2e](https://gitlab.com/macropeople/dcfrontend/commit/d144a2e))


### Features

* add "NOT IN" option to filter ([d41217b](https://gitlab.com/macropeople/dcfrontend/commit/d41217b))
* add / edit record option ([e82b346](https://gitlab.com/macropeople/dcfrontend/commit/e82b346))
* autoselect / autofocus on edit submit ([8833e11](https://gitlab.com/macropeople/dcfrontend/commit/8833e11))
* metanav  attributes search, auto open object details ([9dfb112](https://gitlab.com/macropeople/dcfrontend/commit/9dfb112))
* record-edit linking ([14eedca](https://gitlab.com/macropeople/dcfrontend/commit/14eedca))
* viya support wrapup ([d211378](https://gitlab.com/macropeople/dcfrontend/commit/d211378))



<a name="3.0.0"></a>
# [3.0.0](https://gitlab.com/macropeople/dcfrontend/compare/v2.2.0...v3.0.0) (2020-04-13)


### Bug Fixes

* all servies converted to SASjs ([f649bf1](https://gitlab.com/macropeople/dcfrontend/commit/f649bf1))
* bitemporal loader error removal, build process switched to dcbuild.sh ([0ccc4bc](https://gitlab.com/macropeople/dcfrontend/commit/0ccc4bc))
* case sensitive librefs ([198a2de](https://gitlab.com/macropeople/dcfrontend/commit/198a2de)), closes [#86](https://gitlab.com/macropeople/dcfrontend/issues/86)
* configuration and startup service ([d4e1d7b](https://gitlab.com/macropeople/dcfrontend/commit/d4e1d7b))
* configurator ([f0f2224](https://gitlab.com/macropeople/dcfrontend/commit/f0f2224))
* configurator, and metanav services updated ([c626f08](https://gitlab.com/macropeople/dcfrontend/commit/c626f08))
* enabling sasjs build ([71d65e8](https://gitlab.com/macropeople/dcfrontend/commit/71d65e8))
* environtment verson path ([8636879](https://gitlab.com/macropeople/dcfrontend/commit/8636879))
* final build script ([9cd80cf](https://gitlab.com/macropeople/dcfrontend/commit/9cd80cf))
* final for viya ([920d9ae](https://gitlab.com/macropeople/dcfrontend/commit/920d9ae))
* getting to demo version ([9ffbee2](https://gitlab.com/macropeople/dcfrontend/commit/9ffbee2))
* incorrect validations after duplicate check ([24a417d](https://gitlab.com/macropeople/dcfrontend/commit/24a417d))
* lowercase response, approve route ([9e07fb1](https://gitlab.com/macropeople/dcfrontend/commit/9e07fb1))
* metanav SAS integration ([3fce458](https://gitlab.com/macropeople/dcfrontend/commit/3fce458))
* page not found on approve details ([08fea24](https://gitlab.com/macropeople/dcfrontend/commit/08fea24))
* precision ([2321892](https://gitlab.com/macropeople/dcfrontend/commit/2321892))
* preparing web target ([20aaf69](https://gitlab.com/macropeople/dcfrontend/commit/20aaf69))
* re-org of files for sasjs-cli 2.2 ([81c2094](https://gitlab.com/macropeople/dcfrontend/commit/81c2094))
* removing h54s and boemska ([120cd1b](https://gitlab.com/macropeople/dcfrontend/commit/120cd1b))
* response lowercase, debug switch ([7660cc6](https://gitlab.com/macropeople/dcfrontend/commit/7660cc6))
* sas errors in 9 ([5d1471c](https://gitlab.com/macropeople/dcfrontend/commit/5d1471c))
* sas9 fixes ([45b90dd](https://gitlab.com/macropeople/dcfrontend/commit/45b90dd))
* setting username on login and request ([8e1e0d1](https://gitlab.com/macropeople/dcfrontend/commit/8e1e0d1))
* splash page ([6a50a22](https://gitlab.com/macropeople/dcfrontend/commit/6a50a22))
* stagedata fref pickup ([55572cb](https://gitlab.com/macropeople/dcfrontend/commit/55572cb))
* stagedata table object, debug switch modal close, removing appLoc in request path ([c227a2c](https://gitlab.com/macropeople/dcfrontend/commit/c227a2c))
* switching mf_abort for mp_abort ([1ba790f](https://gitlab.com/macropeople/dcfrontend/commit/1ba790f))
* test updates (demo version) ([cbc2723](https://gitlab.com/macropeople/dcfrontend/commit/cbc2723))
* upgrade to latest sasjs-cli format ([8aae0b2](https://gitlab.com/macropeople/dcfrontend/commit/8aae0b2))
* upgraded sasjs ([fdd7c17](https://gitlab.com/macropeople/dcfrontend/commit/fdd7c17))
* uppercase issue ([08d65b8](https://gitlab.com/macropeople/dcfrontend/commit/08d65b8))
* user _METAUSER instead of sysuserid for SAS9 ([5c12760](https://gitlab.com/macropeople/dcfrontend/commit/5c12760))
* various, including bitemporal ([09d77ca](https://gitlab.com/macropeople/dcfrontend/commit/09d77ca))
* viya integration ([99a8993](https://gitlab.com/macropeople/dcfrontend/commit/99a8993))
* viya integration of getgroupmembers ([426bc88](https://gitlab.com/macropeople/dcfrontend/commit/426bc88))
* viya updates ([22d6e98](https://gitlab.com/macropeople/dcfrontend/commit/22d6e98))
* **dcbuild:** hostURL starts with // instead of http/https ([1f3d6e3](https://gitlab.com/macropeople/dcfrontend/commit/1f3d6e3))


### Chores

* readme update ([4ded32a](https://gitlab.com/macropeople/dcfrontend/commit/4ded32a))


### Features

*  viya macros ([bef208e](https://gitlab.com/macropeople/dcfrontend/commit/bef208e))
* data quality rules on edit mode ([65a20c5](https://gitlab.com/macropeople/dcfrontend/commit/65a20c5))
* enabling users & groups in Viya ([30bed3d](https://gitlab.com/macropeople/dcfrontend/commit/30bed3d))
* integrating sasjs (startupService, SAS Requests, Login) ([d7e20f9](https://gitlab.com/macropeople/dcfrontend/commit/d7e20f9))
* sas9 auto-build with latest sasjs-cli ([5c2f1e4](https://gitlab.com/macropeople/dcfrontend/commit/5c2f1e4))
* sasjs integration ([beb30d8](https://gitlab.com/macropeople/dcfrontend/commit/beb30d8))
* viya mods to enable first service ([a8c8932](https://gitlab.com/macropeople/dcfrontend/commit/a8c8932))


### BREAKING CHANGES

* switch to SASjsgit add README.md



<a name="2.2.0"></a>
# [2.2.0](https://gitlab.com/macropeople/dcfrontend/compare/v2.1.0...v2.2.0) (2020-03-28)


### Bug Fixes

* date capture ([c7c3d57](https://gitlab.com/macropeople/dcfrontend/commit/c7c3d57))
* DOWNCASE->LOWCASE ([aecba73](https://gitlab.com/macropeople/dcfrontend/commit/aecba73))
* hot license key ([3f5b636](https://gitlab.com/macropeople/dcfrontend/commit/3f5b636))


### Features

* apply dqrule in edit mode ([3b4afbd](https://gitlab.com/macropeople/dcfrontend/commit/3b4afbd))



<a name="2.1.0"></a>
# [2.1.0](https://gitlab.com/macropeople/dcfrontend/compare/v2.0.0...v2.1.0) (2020-03-26)


### Bug Fixes

* closes [#83](https://gitlab.com/macropeople/dcfrontend/issues/83) (decimal truncation issue) ([870bf5c](https://gitlab.com/macropeople/dcfrontend/commit/870bf5c))
* cypress ([56ddfdf](https://gitlab.com/macropeople/dcfrontend/commit/56ddfdf))
* debug switch wrong state ([00d9ad1](https://gitlab.com/macropeople/dcfrontend/commit/00d9ad1))
* double click triggers edit mode ([169b84d](https://gitlab.com/macropeople/dcfrontend/commit/169b84d))
* legacy support for x variable in post edit hook ([3777c33](https://gitlab.com/macropeople/dcfrontend/commit/3777c33))
* local vars ([ba8ce4c](https://gitlab.com/macropeople/dcfrontend/commit/ba8ce4c))
* migration ([81a5e0d](https://gitlab.com/macropeople/dcfrontend/commit/81a5e0d))
* test update, remove logging ([cae0735](https://gitlab.com/macropeople/dcfrontend/commit/cae0735))
* tests ([e28d872](https://gitlab.com/macropeople/dcfrontend/commit/e28d872))
* tidyup and enabling views in viewtables ([06639cb](https://gitlab.com/macropeople/dcfrontend/commit/06639cb))


### Features

* data dictionary and mpe_validations tables, and DQRULES now sent in getData service ([856b4f6](https://gitlab.com/macropeople/dcfrontend/commit/856b4f6))
* hardselect, closes [#84](https://gitlab.com/macropeople/dcfrontend/issues/84) ([859cfd1](https://gitlab.com/macropeople/dcfrontend/commit/859cfd1))
* user friendly display when library is empty ([cff71f2](https://gitlab.com/macropeople/dcfrontend/commit/cff71f2))



<a name="2.0.0"></a>
# [2.0.0](https://gitlab.com/macropeople/dcfrontend/compare/v1.3.3...v2.0.0) (2020-03-16)


### Bug Fixes

* backend can take either first or second file as CSV ([a3aaab6](https://gitlab.com/macropeople/dcfrontend/commit/a3aaab6))
* click twice download dialog, link to VIEW in EDIT ([9b47b75](https://gitlab.com/macropeople/dcfrontend/commit/9b47b75))
* enabling clicking of users in groups page ([ab2acc4](https://gitlab.com/macropeople/dcfrontend/commit/ab2acc4))
* moving to single directory for log / staging data capture. ([766e9fc](https://gitlab.com/macropeople/dcfrontend/commit/766e9fc))
* pagination of libraries in tree ([5e79ec3](https://gitlab.com/macropeople/dcfrontend/commit/5e79ec3))
* removing 'multiple file load' feature ([2f21cc4](https://gitlab.com/macropeople/dcfrontend/commit/2f21cc4))
* removing 'multiple file load' feature ([268fdc1](https://gitlab.com/macropeople/dcfrontend/commit/268fdc1))
* saving sub-page in navigation, startup service refactor, edit in VIEW dropdown ([98280a9](https://gitlab.com/macropeople/dcfrontend/commit/98280a9))
* typing on X and prettify JS ([9ab737d](https://gitlab.com/macropeople/dcfrontend/commit/9ab737d))


### Features

* backup Excel file ([7c5e57e](https://gitlab.com/macropeople/dcfrontend/commit/7c5e57e))
* **excelIntegration:** upload both csv and excel file ([d90bfdf](https://gitlab.com/macropeople/dcfrontend/commit/d90bfdf))
* cypress testcases migration ([4fa372c](https://gitlab.com/macropeople/dcfrontend/commit/4fa372c))
* cypress testcases migration ([e2d9998](https://gitlab.com/macropeople/dcfrontend/commit/e2d9998))
* cypress testcases migration ([8e6ccc4](https://gitlab.com/macropeople/dcfrontend/commit/8e6ccc4))
* **excelIntegration:** upload both csv and excel file ([57698b0](https://gitlab.com/macropeople/dcfrontend/commit/57698b0))


### BREAKING CHANGES

* the previous download links will not work following this upgrade



<a name="1.3.3"></a>
## [1.3.3](https://gitlab.com/macropeople/dcfrontend/compare/v1.3.2...v1.3.3) (2020-03-10)


### Bug Fixes

* dups in groups and missing members ([e06b269](https://gitlab.com/macropeople/dcfrontend/commit/e06b269))



<a name="1.3.2"></a>
## [1.3.2](https://gitlab.com/macropeople/dcfrontend/compare/v1.3.1...v1.3.2) (2020-03-09)


### Bug Fixes

* removing duplicates on groupnames ([8e8f99a](https://gitlab.com/macropeople/dcfrontend/commit/8e8f99a))



<a name="1.3.1"></a>
## [1.3.1](https://gitlab.com/macropeople/dcfrontend/compare/v1.3.0...v1.3.1) (2020-03-09)



<a name="1.3.0"></a>
# [1.3.0](https://gitlab.com/macropeople/dcfrontend/compare/v1.2.1...v1.3.0) (2020-03-09)


### Bug Fixes

* closes [#73](https://gitlab.com/macropeople/dcfrontend/issues/73), adding test data and faster viewLib service ([3fbfff2](https://gitlab.com/macropeople/dcfrontend/commit/3fbfff2))
* moving meta specific macros to macros_meta folder ([5bb6fb0](https://gitlab.com/macropeople/dcfrontend/commit/5bb6fb0))
* recursive join issue (was necessary to have a dependency) ([cf0c4bf](https://gitlab.com/macropeople/dcfrontend/commit/cf0c4bf))
* services in usernav ([cb6630d](https://gitlab.com/macropeople/dcfrontend/commit/cb6630d))


### Features

* **usernav:** adding remaining backend services ([2f8ccd2](https://gitlab.com/macropeople/dcfrontend/commit/2f8ccd2))
* **usernav:** sidebar integration ([60700fb](https://gitlab.com/macropeople/dcfrontend/commit/60700fb))
* **usernav:** User Navigation ([265c489](https://gitlab.com/macropeople/dcfrontend/commit/265c489))
* User Navigator services ([505b99b](https://gitlab.com/macropeople/dcfrontend/commit/505b99b))



<a name="1.2.1"></a>
## [1.2.1](https://gitlab.com/macropeople/dcfrontend/compare/v1.2.0...v1.2.1) (2020-02-29)


### Bug Fixes

* IE and Edge hidding libraries ([c59de7c](https://gitlab.com/macropeople/dcfrontend/commit/c59de7c))
* resizing sidebar improvments ([fe7b4bb](https://gitlab.com/macropeople/dcfrontend/commit/fe7b4bb))
* view new table, old removed, added column dropdown ([e8d254b](https://gitlab.com/macropeople/dcfrontend/commit/e8d254b))



<a name="1.2.0"></a>
# [1.2.0](https://gitlab.com/macropeople/dcfrontend/compare/v1.1.0...v1.2.0) (2020-02-27)


### Bug Fixes

* model change in selectbox table ([92fd9be](https://gitlab.com/macropeople/dcfrontend/commit/92fd9be))
* remove autologging and make debug mode false by default ([cc625f0](https://gitlab.com/macropeople/dcfrontend/commit/cc625f0))
* tree library leftover ([b2b9c07](https://gitlab.com/macropeople/dcfrontend/commit/b2b9c07))


### Features

* metadata navigator ([80c3fcc](https://gitlab.com/macropeople/dcfrontend/commit/80c3fcc))
* **metadata navigator:** routing and scroll bar on objects ([9a88e5f](https://gitlab.com/macropeople/dcfrontend/commit/9a88e5f))
* **metadata navigator:** routing update and repository addition ([571da8f](https://gitlab.com/macropeople/dcfrontend/commit/571da8f))
* **metadata navigator:** search bar on metatypes ([3b94c0d](https://gitlab.com/macropeople/dcfrontend/commit/3b94c0d))
* **metadata navigator:** search on metadata objects, design improvements ([1973147](https://gitlab.com/macropeople/dcfrontend/commit/1973147))



<a name="1.1.0"></a>
# [1.1.0](https://gitlab.com/macropeople/dcfrontend/compare/v1.0.2...v1.1.0) (2020-02-26)


### Bug Fixes

* **docs:** direct lib now in settings ([a572625](https://gitlab.com/macropeople/dcfrontend/commit/a572625))
* adding npm package ([cdb6c87](https://gitlab.com/macropeople/dcfrontend/commit/cdb6c87))
* dropzone buttons one row ([a630a69](https://gitlab.com/macropeople/dcfrontend/commit/a630a69))
* edit integration undefined table approval ([b382aa8](https://gitlab.com/macropeople/dcfrontend/commit/b382aa8))
* editor libraries bug closes [#57](https://gitlab.com/macropeople/dcfrontend/issues/57) ([80c7719](https://gitlab.com/macropeople/dcfrontend/commit/80c7719))
* header match case insensitivity ([63ed719](https://gitlab.com/macropeople/dcfrontend/commit/63ed719))
* IE Tree bug, resizing sidebar ([2e0db53](https://gitlab.com/macropeople/dcfrontend/commit/2e0db53))
* removing call to mm_getlibs and ensuring compatibility with multiple repositories ([2520ddd](https://gitlab.com/macropeople/dcfrontend/commit/2520ddd))
* this.table ([0877332](https://gitlab.com/macropeople/dcfrontend/commit/0877332))
* using meta libname in settings ([550b9ce](https://gitlab.com/macropeople/dcfrontend/commit/550b9ce))
* viewer caching issue ([bdeffe2](https://gitlab.com/macropeople/dcfrontend/commit/bdeffe2))


### Features

* **excel data integration:** missing columns addition ([5fdab7b](https://gitlab.com/macropeople/dcfrontend/commit/5fdab7b))
* **excel parsing:** order doesn't matter + wiscard extra columns if exists in between ([c0d0c9a](https://gitlab.com/macropeople/dcfrontend/commit/c0d0c9a))
* **excel_integration:** header selection method update ([be1e2f6](https://gitlab.com/macropeople/dcfrontend/commit/be1e2f6))
*  download DDL (#closes 43) ([4ff3398](https://gitlab.com/macropeople/dcfrontend/commit/4ff3398))
* download DDL ([9a70fff](https://gitlab.com/macropeople/dcfrontend/commit/9a70fff))
* excel data Integration ([f630f1d](https://gitlab.com/macropeople/dcfrontend/commit/f630f1d))
* **excelIntegration:** Date time header validations ([2213bee](https://gitlab.com/macropeople/dcfrontend/commit/2213bee))
* list of metadata repos (new service) ([7d5e17e](https://gitlab.com/macropeople/dcfrontend/commit/7d5e17e))
* usage tracking. closes [#45](https://gitlab.com/macropeople/dcfrontend/issues/45) ([dccd7fa](https://gitlab.com/macropeople/dcfrontend/commit/dccd7fa))
* wide drap/drop and auto upload on file drop ([b02cfc2](https://gitlab.com/macropeople/dcfrontend/commit/b02cfc2))



<a name="1.0.2"></a>
## [1.0.2](https://gitlab.com/macropeople/dcfrontend/compare/v1.0.1...v1.0.2) (2020-02-12)


### Bug Fixes

* approve page overflow ([711401a](https://gitlab.com/macropeople/dcfrontend/commit/711401a))
* editor page overlaying header bar ([a9cd778](https://gitlab.com/macropeople/dcfrontend/commit/a9cd778))
* moving JS to be exernal ([05c41ba](https://gitlab.com/macropeople/dcfrontend/commit/05c41ba))
* moving JS to be exernal ([5b85773](https://gitlab.com/macropeople/dcfrontend/commit/5b85773))
* perms ([6721a67](https://gitlab.com/macropeople/dcfrontend/commit/6721a67))
* sidebar duplicates and double selection ([edade84](https://gitlab.com/macropeople/dcfrontend/commit/edade84))



<a name="1.0.1"></a>
## [1.0.1](https://gitlab.com/macropeople/dcfrontend/compare/v1.0.0...v1.0.1) (2020-02-11)


### Bug Fixes

* migration mpe-security ([7cd7896](https://gitlab.com/macropeople/dcfrontend/commit/7cd7896))
* moving scripts.js to external site in demo  version ([49ad2c0](https://gitlab.com/macropeople/dcfrontend/commit/49ad2c0))



<a name="1.0.0"></a>
# [1.0.0](https://gitlab.com/macropeople/dcfrontend/compare/v0.12.1...v1.0.0) (2020-02-09)


### Chores

* remove whitespace ([89fc982](https://gitlab.com/macropeople/dcfrontend/commit/89fc982))


### BREAKING CHANGES

* seems this needs to be in the footer for it to work



<a name="0.12.1"></a>
## [0.12.1](https://gitlab.com/macropeople/dcfrontend/compare/v0.12.0...v0.12.1) (2020-02-09)



<a name="0.12.0"></a>
# [0.12.0](https://gitlab.com/macropeople/dcfrontend/compare/v0.11.2...v0.12.0) (2020-02-09)


### Bug Fixes

* **new design:** clarity new version fixed elements (inputs, selects...) ([755a41c](https://gitlab.com/macropeople/dcfrontend/commit/755a41c))
* **new design:** filter input elements (broken due new clr version) ([338faf8](https://gitlab.com/macropeople/dcfrontend/commit/338faf8))
* issue20 security updates ([f7168b7](https://gitlab.com/macropeople/dcfrontend/commit/f7168b7))
* new design, viewer sidebar optimizing ([bb29fb7](https://gitlab.com/macropeople/dcfrontend/commit/bb29fb7))


### Features

* **new design:** lineage component ([2e070c0](https://gitlab.com/macropeople/dcfrontend/commit/2e070c0))
* **new design:** sidebar searchboxes ([b6cca58](https://gitlab.com/macropeople/dcfrontend/commit/b6cca58))
* New Design ([a4f41da](https://gitlab.com/macropeople/dcfrontend/commit/a4f41da)), closes [#35](https://gitlab.com/macropeople/dcfrontend/issues/35)
* new design: lineage started ([6e1ead1](https://gitlab.com/macropeople/dcfrontend/commit/6e1ead1))
* selectively disable tables in VIEW ([44a418e](https://gitlab.com/macropeople/dcfrontend/commit/44a418e))
* TIME support (minus time picker).  closes #issue42 ([a16e240](https://gitlab.com/macropeople/dcfrontend/commit/a16e240)), closes [#issue42](https://gitlab.com/macropeople/dcfrontend/issues/issue42)



<a name="0.11.2"></a>
## [0.11.2](https://gitlab.com/macropeople/dcfrontend/compare/v0.11.1...v0.11.2) (2020-01-31)


### Bug Fixes

* missing macro in bitemporal loader ([7681d46](https://gitlab.com/macropeople/dcfrontend/commit/7681d46))



<a name="0.11.1"></a>
## [0.11.1](https://gitlab.com/macropeople/dcfrontend/compare/v0.11.0...v0.11.1) (2020-01-29)


### Bug Fixes

* enabling embedded carriage returns ([d9422dc](https://gitlab.com/macropeople/dcfrontend/commit/d9422dc))



<a name="0.11.0"></a>
# [0.11.0](https://gitlab.com/macropeople/dcfrontend/compare/v0.10.1...v0.11.0) (2020-01-28)


### Bug Fixes

* short numerics and best. format numerics now handled ([b7c1279](https://gitlab.com/macropeople/dcfrontend/commit/b7c1279))
* stp registration ([8afa679](https://gitlab.com/macropeople/dcfrontend/commit/8afa679))


### Features

* short numerics ([c840b79](https://gitlab.com/macropeople/dcfrontend/commit/c840b79))



<a name="0.10.1"></a>
## [0.10.1](https://gitlab.com/macropeople/dcfrontend/compare/v0.10.0...v0.10.1) (2020-01-28)


### Bug Fixes

* short numerics and numeric formats, #closes issue40 ([eb5893c](https://gitlab.com/macropeople/dcfrontend/commit/eb5893c))



<a name="0.10.0"></a>
# [0.10.0](https://gitlab.com/macropeople/dcfrontend/compare/v0.9.0...v0.10.0) (2020-01-25)


### Bug Fixes

* adding more sample data ([89b25d1](https://gitlab.com/macropeople/dcfrontend/commit/89b25d1))
* editor column max width ([8e803fc](https://gitlab.com/macropeople/dcfrontend/commit/8e803fc))
* editor file upload abort message ([10cad83](https://gitlab.com/macropeople/dcfrontend/commit/10cad83))
* lineage regenerate problem ([354d126](https://gitlab.com/macropeople/dcfrontend/commit/354d126))
* sample data ([b743892](https://gitlab.com/macropeople/dcfrontend/commit/b743892))
* viewer / editor column max width ([6c3abc7](https://gitlab.com/macropeople/dcfrontend/commit/6c3abc7))


### Features

* groups plus metadata refresh backend ([850f29a](https://gitlab.com/macropeople/dcfrontend/commit/850f29a))
* lineage refresh cache option ([35d1b6c](https://gitlab.com/macropeople/dcfrontend/commit/35d1b6c))
* max col width 500 plus custom groups feature ([5d5b55b](https://gitlab.com/macropeople/dcfrontend/commit/5d5b55b))
* metadata services ([697642c](https://gitlab.com/macropeople/dcfrontend/commit/697642c))



<a name="0.9.0"></a>
# [0.9.0](https://gitlab.com/macropeople/dcfrontend/compare/v0.8.0...v0.9.0) (2020-01-19)


### Bug Fixes

* maxdepth for lineage ([28cf028](https://gitlab.com/macropeople/dcfrontend/commit/28cf028))
* MAXOBS for editing data ([5ada758](https://gitlab.com/macropeople/dcfrontend/commit/5ada758))


### Features

*  9.3 compatibility (remove fcmp in windows, updating adapter) ([11e7692](https://gitlab.com/macropeople/dcfrontend/commit/11e7692))
* admin tools (config viewer and download) ([a95ed90](https://gitlab.com/macropeople/dcfrontend/commit/a95ed90))
* extra checking to ensure the install user has the correct metadata and OS permissions ([4ef83db](https://gitlab.com/macropeople/dcfrontend/commit/4ef83db))
* licence checker plus adding MPE_SELECTBOX to config download ([5109670](https://gitlab.com/macropeople/dcfrontend/commit/5109670))



<a name="0.8.0"></a>
# [0.8.0](https://gitlab.com/macropeople/dcfrontend/compare/v0.7.2...v0.8.0) (2020-01-13)


### Bug Fixes

* lineage table removal ([e28cb59](https://gitlab.com/macropeople/dcfrontend/commit/e28cb59))
* removing configurator after it is run (to prevent accidental overwrite once library is up and running) ([8759226](https://gitlab.com/macropeople/dcfrontend/commit/8759226))
* removing stp dependency in mpeinit ([44d3c63](https://gitlab.com/macropeople/dcfrontend/commit/44d3c63))


### Features

* lineage flatdata csv download ([fce560f](https://gitlab.com/macropeople/dcfrontend/commit/fce560f))



<a name="0.7.2"></a>
## [0.7.2](https://gitlab.com/macropeople/dcfrontend/compare/v0.7.1...v0.7.2) (2020-01-11)


### Bug Fixes

* empty lineage screen now displays ([9876ff9](https://gitlab.com/macropeople/dcfrontend/commit/9876ff9))



<a name="0.7.1"></a>
## [0.7.1](https://gitlab.com/macropeople/dcfrontend/compare/v0.7.0...v0.7.1) (2020-01-07)



<a name="0.7.0"></a>
# [0.7.0](https://gitlab.com/macropeople/dcfrontend/compare/v0.6.0...v0.7.0) (2020-01-02)


### Bug Fixes

* adding port to web query ([07e6a5d](https://gitlab.com/macropeople/dcfrontend/commit/07e6a5d))
* closes [#32](https://gitlab.com/macropeople/dcfrontend/issues/32) and enbles viewing of tables that only exist in metadata ([c942b21](https://gitlab.com/macropeople/dcfrontend/commit/c942b21))
* dropdown to download button inside popup ([ceb5b75](https://gitlab.com/macropeople/dcfrontend/commit/ceb5b75))
* dropdowns and buttons reposition, saving space for lineage graph ([e95a713](https://gitlab.com/macropeople/dcfrontend/commit/e95a713))
* editor table, dot check removed ([9612323](https://gitlab.com/macropeople/dcfrontend/commit/9612323))
* IE editor library dropdown, lineage selection width ([9a1c923](https://gitlab.com/macropeople/dcfrontend/commit/9a1c923))
* index.html metadata ([83fb505](https://gitlab.com/macropeople/dcfrontend/commit/83fb505))
* index.html metadata ([ff0c552](https://gitlab.com/macropeople/dcfrontend/commit/ff0c552))
* index.html metadata comment update ([c2e337f](https://gitlab.com/macropeople/dcfrontend/commit/c2e337f))
* lineage download buttons hidden ([9227ccc](https://gitlab.com/macropeople/dcfrontend/commit/9227ccc))
* lineage more space ([f2e6375](https://gitlab.com/macropeople/dcfrontend/commit/f2e6375))
* lineage popup remove, positions, linkable ([9d53f2a](https://gitlab.com/macropeople/dcfrontend/commit/9d53f2a))
* linking lineage ([4c2095b](https://gitlab.com/macropeople/dcfrontend/commit/4c2095b))
* padding issues, editor table loading fix ([bc0c2be](https://gitlab.com/macropeople/dcfrontend/commit/bc0c2be))


### Features

* IE compatibility ([e28ac50](https://gitlab.com/macropeople/dcfrontend/commit/e28ac50))
* linkable lineage ([d8ebc19](https://gitlab.com/macropeople/dcfrontend/commit/d8ebc19))



<a name="0.6.0"></a>
# [0.6.0](https://gitlab.com/macropeople/dcfrontend/compare/v0.5.0...v0.6.0) (2019-12-14)


### Bug Fixes

* package updates ([03ace1f](https://gitlab.com/macropeople/dcfrontend/commit/03ace1f))


### Features

*  lineage and metarepo option ([99090a1](https://gitlab.com/macropeople/dcfrontend/commit/99090a1))
* Add Data Lineage ([b21a644](https://gitlab.com/macropeople/dcfrontend/commit/b21a644))



<a name="0.5.0"></a>
# [0.5.0](https://gitlab.com/macropeople/dcfrontend/compare/v0.4.0...v0.5.0) (2019-09-20)


### Bug Fixes

* error message not surfaced on VIEW table, closes [#25](https://gitlab.com/macropeople/dcfrontend/issues/25) ([4687ec4](https://gitlab.com/macropeople/dcfrontend/commit/4687ec4))
* format length issue, special chars in usernames issue ([447f483](https://gitlab.com/macropeople/dcfrontend/commit/447f483))
* removing npm audit warnings with devkit update to 13.9 ([e1d28a9](https://gitlab.com/macropeople/dcfrontend/commit/e1d28a9))


### Features

*  improved deployment process for evaluation version ([3a9e846](https://gitlab.com/macropeople/dcfrontend/commit/3a9e846))
* Allow WLatin file upload ([1c02491](https://gitlab.com/macropeople/dcfrontend/commit/1c02491))
* auto config for eval version ([35ab667](https://gitlab.com/macropeople/dcfrontend/commit/35ab667))



<a name="0.4.0"></a>
# [0.4.0](https://gitlab.com/macropeople/dcfrontend/compare/v0.3.0...v0.4.0) (2019-05-27)


### Bug Fixes

* additional types added ([279fb6c](https://gitlab.com/macropeople/dcfrontend/commit/279fb6c))
* editor dropdown remember selections ([75391eb](https://gitlab.com/macropeople/dcfrontend/commit/75391eb))
* email fixes ([996b14b](https://gitlab.com/macropeople/dcfrontend/commit/996b14b))
* email issues ([c74f668](https://gitlab.com/macropeople/dcfrontend/commit/c74f668))
* filter remebering | cleanup ([bd59204](https://gitlab.com/macropeople/dcfrontend/commit/bd59204))
* issue 18, use strict ([a4ac751](https://gitlab.com/macropeople/dcfrontend/commit/a4ac751))
* issue 19 ([ca67123](https://gitlab.com/macropeople/dcfrontend/commit/ca67123))
* metadataRoot added back ([50243a3](https://gitlab.com/macropeople/dcfrontend/commit/50243a3))
* remember filter editor ([6fe1545](https://gitlab.com/macropeople/dcfrontend/commit/6fe1545))
* remember filter selections ([774a42e](https://gitlab.com/macropeople/dcfrontend/commit/774a42e))
* remember filter viewer ([de53590](https://gitlab.com/macropeople/dcfrontend/commit/de53590))
* remembering filter selections issue15 ([d86326a](https://gitlab.com/macropeople/dcfrontend/commit/d86326a))
* remembering filter without regenerating ([f5fa1dd](https://gitlab.com/macropeople/dcfrontend/commit/f5fa1dd))
* remembering selections | running service onliy on startup ([f8aeef5](https://gitlab.com/macropeople/dcfrontend/commit/f8aeef5))
* removed some settings ([222001a](https://gitlab.com/macropeople/dcfrontend/commit/222001a))
* removing col freeze as not ready yet ([a8ac2bc](https://gitlab.com/macropeople/dcfrontend/commit/a8ac2bc))
* removing not null constraints ([b4f4ba8](https://gitlab.com/macropeople/dcfrontend/commit/b4f4ba8))
* save selected library and table ([c98bdfd](https://gitlab.com/macropeople/dcfrontend/commit/c98bdfd))
* unique urls in prod build ([605de8a](https://gitlab.com/macropeople/dcfrontend/commit/605de8a))
* unique urls in prod build ([b749b35](https://gitlab.com/macropeople/dcfrontend/commit/b749b35))
* upgrading handsontable to 7.03 ([08200e0](https://gitlab.com/macropeople/dcfrontend/commit/08200e0))
* viewer dropdowns remember selections ([189304c](https://gitlab.com/macropeople/dcfrontend/commit/189304c))
* viewer dropdowns remember selections ([ae29b42](https://gitlab.com/macropeople/dcfrontend/commit/ae29b42))


### Features

*  adding email options in log in mpe_alerts ([e546d8e](https://gitlab.com/macropeople/dcfrontend/commit/e546d8e))
* adding dropdowns to mpe_alerts table by default ([f43db13](https://gitlab.com/macropeople/dcfrontend/commit/f43db13))
* adding frontend filtering, column freeze, and readonly column switch ([003373a](https://gitlab.com/macropeople/dcfrontend/commit/003373a))
* always on debug plus siteid linking for eval version ([fa0a418](https://gitlab.com/macropeople/dcfrontend/commit/fa0a418))
* locking down eval version to a siteid ([bfb6325](https://gitlab.com/macropeople/dcfrontend/commit/bfb6325))



<a name="0.3.0"></a>
# [0.3.0](https://gitlab.com/macropeople/dcfrontend/compare/v0.2.0...v0.3.0) (2019-04-11)


### Bug Fixes

* closes [#16](https://gitlab.com/macropeople/dcfrontend/issues/16), missing getAuditFile ([e6b6717](https://gitlab.com/macropeople/dcfrontend/commit/e6b6717))
* issue12 dev work ([8b547af](https://gitlab.com/macropeople/dcfrontend/commit/8b547af))
* missing macro in loadfile ([334b4a2](https://gitlab.com/macropeople/dcfrontend/commit/334b4a2))
* problem with file upload.  Also enabling licence key implementation on view & stage components. ([0f186e5](https://gitlab.com/macropeople/dcfrontend/commit/0f186e5))


### Features

* enabling multiColumnSort and manualColumnResize.  Closes [#1](https://gitlab.com/macropeople/dcfrontend/issues/1) and closes [#13](https://gitlab.com/macropeople/dcfrontend/issues/13). ([56dfc90](https://gitlab.com/macropeople/dcfrontend/commit/56dfc90))
* moving ALL config vars to MPE_CONFIG table. Closes [#11](https://gitlab.com/macropeople/dcfrontend/issues/11) ([6132793](https://gitlab.com/macropeople/dcfrontend/commit/6132793))



<a name="0.2.0"></a>
# [0.2.0](https://gitlab.com/macropeople/dcfrontend/compare/v0.1.0...v0.2.0) (2019-04-07)


### Bug Fixes

* failing file uploads in demo version of DC resolved.  closes [#9](https://gitlab.com/macropeople/dcfrontend/issues/9) ([f0a6aeb](https://gitlab.com/macropeople/dcfrontend/commit/f0a6aeb))
* missing mf_abort in getStageTable ([97f0c66](https://gitlab.com/macropeople/dcfrontend/commit/97f0c66))


### Features

*  switch settings to be an STP for easier modification ([a28057d](https://gitlab.com/macropeople/dcfrontend/commit/a28057d))
* adding switch for library checking, no by default to improve responsiveness.  Closes [#7](https://gitlab.com/macropeople/dcfrontend/issues/7) ([adcacf5](https://gitlab.com/macropeople/dcfrontend/commit/adcacf5))
* enabling emails on SUBMIT, APPROVE and REJECT.  Closes [#10](https://gitlab.com/macropeople/dcfrontend/issues/10) ([daec738](https://gitlab.com/macropeople/dcfrontend/commit/daec738))



<a name="0.1.0"></a>
# [0.1.0](https://gitlab.com/macropeople/dcfrontend/compare/v0.0.1...v0.1.0) (2019-04-03)


### Bug Fixes

* accidental deletion ([8236438](https://gitlab.com/macropeople/dcfrontend/commit/8236438))
* enabling build ([f5e8807](https://gitlab.com/macropeople/dcfrontend/commit/f5e8807))
* fixes to fix demo version ([6cf6c69](https://gitlab.com/macropeople/dcfrontend/commit/6cf6c69))
* removing console logs ([3d6e6f2](https://gitlab.com/macropeople/dcfrontend/commit/3d6e6f2))
* upgrade to avoid npm warnings ([d8f90c3](https://gitlab.com/macropeople/dcfrontend/commit/d8f90c3))


### Features

* **clarity:**  the styling now workscd .. ([28b2db5](https://gitlab.com/macropeople/dcfrontend/commit/28b2db5))
*  single SPK compilation ([b785d84](https://gitlab.com/macropeople/dcfrontend/commit/b785d84))
* removing app component replacing with document object, which is safer when importing SPKs over existing packages ([0ba14ed](https://gitlab.com/macropeople/dcfrontend/commit/0ba14ed))



<a name="0.0.1"></a>
## 0.0.1 (2019-01-27)


### Bug Fixes

* Adding new rows now works ([8685b4e](https://gitlab.com/macropeople/dcfrontend/commit/8685b4e))
* removing invalid json ([7dd24e0](https://gitlab.com/macropeople/dcfrontend/commit/7dd24e0))
* removing version.ts from source control ([186c669](https://gitlab.com/macropeople/dcfrontend/commit/186c669))
* removing version.ts from source control, now with gitignore ([2c8ccf8](https://gitlab.com/macropeople/dcfrontend/commit/2c8ccf8))


### Features

* removing  font ([325f193](https://gitlab.com/macropeople/dcfrontend/commit/325f193))
