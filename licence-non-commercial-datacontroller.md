
# Data Controller
_FREE FOR NON-COMMERCIAL LICENSE_

_VERSION 1.0 OF July 25th, 2023_

## 1. Copyright Notice
Copyright (c) Bowe IO Ltd, a UK Limited Company headquarted in 29 Oldfield Rd, Cumbria, registered by companies house under number 08777171, VAT number: 203914240

## 2. License Terms and Conditions
### 2.1. Grant of License.
Subject to the terms and conditions of this license, the licensor as indicated in the copyright notice above (“Licensor”) hereby grants you a perpetual, irrevocable except in the event of any breach of this license, worldwide, non-exclusive, no-charge, royalty-free, and limited license to:

(i) use Data Controller software (“Software”) and prepare original works of authorship based on or derived from the Software (“Derivative Works”); and

(ii) reproduce and distribute copies of the Software and Derivative Works (collectively,
“Deliverables”).

### 2.2. Restrictions.
Neither this license nor the Deliverables may be so used as to result in:

(i) your direct or indirect commercial advantage or monetary gains (“Commercial Purposes”) except only if such use is for the sole purpose of testing the suitability, performance, and usefulness of the Deliverables for your business needs; or

(ii) commercial, non-commercial, for profit or not for profit licensing, transfer or distribution of a Derivative Work that is competitive with the Software, contains the same or substantially similar functionality as the Software, or could likely result in third parties utilizing the Derivative Work in lieu of or in place of the Software (“Competitive Purposes”); or

(iii) both Commercial Purposes and Competitive Purposes.

### 2.3. Redistribution.
You may reproduce and distribute copies of the Deliverables in any medium, with or without modifications, and in source or object form, if you meet the following conditions:

(i) you must give any other recipients of the Deliverables a copy of this license; and

(ii) you must cause any modified files to carry prominent notices stating that you changed the files;
and

(iii) you must retain, in the source form of any Derivative Works that you distribute, all copyright, trademark, and attribution notices from the source form of the Software, excluding those notices that do not pertain to any part of the Derivative Works; and

(iv) if the Software includes a “notice” text file as part of its distribution, then any Derivative Works that you distribute must include a readable copy of the attribution notices contained within such notice file, excluding those notices that do not pertain to any part of the Derivative Works. The contents of the notice file are for informational purposes only and do not modify this license.

## 3. Miscellaneous
### 3.1. License Key.
To use the Deliverables, you shall apply a license key defined in the relevant Software documentation as from time to time amended.

### 3.2. Trademarks.
This license does not grant permission to use the trade names, trademarks, service marks, or product names of the Licensor, except as required for reasonable and customary use in describing the origin of the Deliverables and reproducing the content of the notice file.

## 4. Disclaimer and Limitation
### 4.1. Disclaimer of Warranty.
UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING, LICENSOR PROVIDES THE SOFTWARE ON AN “AS IS” BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, ANY WARRANTIES OR CONDITIONS OF TITLE, NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE. YOU ARE SOLELY RESPONSIBLE FOR DETERMINING THE APPROPRIATENESS OF USING THE DELIVERABLES AND ASSUME ANY RISKS ASSOCIATED WITH YOUR EXERCISE OF PERMISSIONS UNDER THIS LICENSE.

### 4.2. Limitation of Liability.
IN NO EVENT AND UNDER NO LEGAL THEORY, WHETHER IN TORT (INCLUDING NEGLIGENCE), CONTRACT, OR OTHERWISE, UNLESS REQUIRED BY APPLICABLE LAW (SUCH AS DELIBERATE AND GROSSLY NEGLIGENT ACTS) OR AGREED TO IN WRITING, SHALL LICENSOR BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY CHARACTER ARISING AS A RESULT OF THIS LICENSE OR OUT OF THE USE OR INABILITY TO USE THE DELIVERABLES (INCLUDING BUT NOT LIMITED TO DAMAGES FOR LOSS OF GOODWILL, WORK STOPPAGE, COMPUTER FAILURE OR MALFUNCTION, OR ANY AND ALL OTHER COMMERCIAL DAMAGES OR LOSSES), EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
