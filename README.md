# Data Controller for SAS

**Control your manual data modifications!**

_Alternatives to Data Controller include:_

* 💾 Developing / testing / deploying / scheduling overnight batch jobs to load files from shared drives
* 🔒 Opening (and locking) datasets in Enterprise Guide or SAS® Table Viewer to perform direct updates
* ❓ Asking a #DBA to run validated code after a change management process
* 🌐 Building & maintaining your own custom web application
* 🏃 Running #SAS or #SQL updates in production

_Problems with the above include:_

* Legacy 'black box' solutions with little to no testing, documentation or support
* End users requiring direct write access to critical data sources in production
* Upload routines that must be manually modified when the data model changes
* Breaches due to unnecessary parties having access to the data
* Inability to trace who made a change, when, and why
* Reliance on key individuals to perform updates
* Building bespoke ETL for every new data source
* High risk of manual error / data corruption

Data Controller for SAS® solves all these issues in a simple-to-install, user-friendly, secure, documented, battle-tested web application.  Available on Viya, SAS 9 EBI, and [SASjs Server](https://server.sasjs.io).

For more information:

* Main site:  https://datacontroller.io
* Docs:  https://docs.datacontroller.io
* Code: https://code.datacontroller.io

For support, contact support@4gl.io or reach out on [Matrix](https://matrix.to/#/#dc:4gl.io)!